# README #


## Overview

* This repository contains two sample applications, demonstrating the use of the real-time Windows extension Kithara for high speed image processing. Demonstrated functionality includes the ability to configure capture images using Kithara and running complex OpenCV based algorithms within the Kithara environment.
* This software is development code, representing proof-of-concept only. 
* To-date the software has been deployed on a development PC only, further configuration by a developer is likely to be necessary to enable deployment on other systems.  
* Further information on both applications is available in the supporting PDF documentation (see the root directory).

## Directories ##

* The following section describes the functionality of the individual code examples.

### Kithara camera data sharing 
	
* configuring a camera with specific GenICam settings;
* achieving high speed image capture from a USB 3.0 camera;
* creating a OpenCV based task running in kernel mode to capture images;
* performing real-time processing of captured images using OpenCV;
* transmitting captured images from the kernel to user mode via shared memory;
* displaying captured camera images within theuser mode.

### Kithara sorter ###
	
* creating a task in kernel mode to perform the sorter operation;
* loading 2000 test images into the kernel;
* running the sorter algorithm on all 2000 images, with and without displaying the results to the user;
* timing of these operations.

## Build Environment ##

* A Kithara Demo licence is required for the software to run.
* Software compilation has been tested using Visual Studio 2013 (64-bit) running on Windows 7 Professional.
* A dedicated USB 3.0 controller configured to use Kithara devices is required, to avoid conflict with Windows system drivers during operation.
* The software requires OpenCV and a directory of images to be supplied in specific locations on the c:\ drive to operate.