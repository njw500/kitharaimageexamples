Attribute VB_Name = "KrtsDemo"
' Copyright (c) 1999-2016 by Kithara Software GmbH. All rights reserved.

'###############################################################################################################
'
' File:             KrtsDemo.bas (v10.00d)
'
' Description:      Visual Basic API for Kithara �RealTime Suite�
'
' REV   DATE        LOG    DESCRIPTION
' ----- ----------  ------ ------------------------------------------------------------------------------------
' j.ant 1999-10-31  CREAT: Original - (file created)
' ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:01
'
'###############################################################################################################

'---------------------------------------------------------------------------------------------------------------
' KrtsDemo
'---------------------------------------------------------------------------------------------------------------

Option Explicit

'---------------------------------------------------------------------------------------------------------------
' Base Module
'---------------------------------------------------------------------------------------------------------------

'------ Common Constants ------
Public Const KS_INVALID_HANDLE                  = &H0

'------ Error Categories ------
' Operating system errors:
Public Const KSERROR_CATEGORY_OPERATING_SYSTEM  = &H0

' Categories 0x01000000 ... 0x0f000000 are free for customer usage!
Public Const KSERROR_CATEGORY_USER              = &HF000000
Public Const KSERROR_CATEGORY_BASE              = &H10000000
Public Const KSERROR_CATEGORY_DEAL              = &H11000000
Public Const KSERROR_CATEGORY_HANDLING          = &H17000000
Public Const KSERROR_CATEGORY_IOPORT            = &H14000000
Public Const KSERROR_CATEGORY_MEMORY            = &H12000000
Public Const KSERROR_CATEGORY_KERNEL            = &H18000000
Public Const KSERROR_CATEGORY_KEYBOARD          = &H1A000000
Public Const KSERROR_CATEGORY_INTERRUPT         = &H13000000
Public Const KSERROR_CATEGORY_TIMER             = &H16000000
Public Const KSERROR_CATEGORY_USB               = &H19000000
Public Const KSERROR_CATEGORY_IDT               = &H1B000000
Public Const KSERROR_CATEGORY_NODE              = &H1C000000
Public Const KSERROR_CATEGORY_SOCKET            = &H1D000000
Public Const KSERROR_CATEGORY_SYSTEM            = &H1E000000
Public Const KSERROR_CATEGORY_ETHERCAT          = &H1F000000
Public Const KSERROR_CATEGORY_MF                = &H20000000
Public Const KSERROR_CATEGORY_CAN               = &H21000000
Public Const KSERROR_CATEGORY_PROFIBUS          = &H22000000
Public Const KSERROR_CATEGORY_PLC               = &H23000000
Public Const KSERROR_CATEGORY_CANOPEN           = &H24000000
Public Const KSERROR_CATEGORY_FLEXRAY           = &H25000000
Public Const KSERROR_CATEGORY_PACKET            = &H26000000
Public Const KSERROR_CATEGORY_CAMERA            = &H27000000
Public Const KSERROR_CATEGORY_TASK              = &H28000000
Public Const KSERROR_CATEGORY_SPECIAL           = &H29000000
Public Const KSERROR_CATEGORY_XHCI              = &H2A000000
Public Const KSERROR_CATEGORY_DISK              = &H2B000000
Public Const KSERROR_CATEGORY_FILE              = &H2C000000
Public Const KSERROR_CATEGORY_NEXT              = &H2D000000
Public Const KSERROR_CATEGORY_EXCEPTION         = &H3F000000

'------ Error Codes ------
Public Const KSERROR_OPERATING_SYSTEM           = (KSERROR_CATEGORY_BASE+&H0)
Public Const KSERROR_UNKNOWN_ERROR_CODE         = (KSERROR_CATEGORY_BASE+&H10000)
Public Const KSERROR_UNKNOWN_ERROR_CATEGORY     = (KSERROR_CATEGORY_BASE+&H20000)
Public Const KSERROR_UNKNOWN_ERROR_LANGUAGE     = (KSERROR_CATEGORY_BASE+&H30000)
Public Const KSERROR_CANNOT_FIND_LIBRARY        = (KSERROR_CATEGORY_BASE+&H40000)
Public Const KSERROR_CANNOT_FIND_ADDRESS        = (KSERROR_CATEGORY_BASE+&H50000)
Public Const KSERROR_BAD_PARAM                  = (KSERROR_CATEGORY_BASE+&H60000)
Public Const KSERROR_BAD_VERSION                = (KSERROR_CATEGORY_BASE+&H70000)
Public Const KSERROR_INTERNAL                   = (KSERROR_CATEGORY_BASE+&H80000)
Public Const KSERROR_UNKNOWN                    = (KSERROR_CATEGORY_BASE+&H90000)
Public Const KSERROR_FUNCTION_NOT_AVAILABLE     = (KSERROR_CATEGORY_BASE+&HA0000)
Public Const KSERROR_DRIVER_NOT_OPENED          = (KSERROR_CATEGORY_BASE+&HB0000)
Public Const KSERROR_NOT_ENOUGH_MEMORY          = (KSERROR_CATEGORY_BASE+&HC0000)
Public Const KSERROR_CANNOT_OPEN_KERNEL         = (KSERROR_CATEGORY_BASE+&HD0000)
Public Const KSERROR_CANNOT_CLOSE_KERNEL        = (KSERROR_CATEGORY_BASE+&HE0000)
Public Const KSERROR_BAD_KERNEL_VERSION         = (KSERROR_CATEGORY_BASE+&HF0000)
Public Const KSERROR_CANNOT_FIND_KERNEL         = (KSERROR_CATEGORY_BASE+&H100000)
Public Const KSERROR_INVALID_HANDLE             = (KSERROR_CATEGORY_BASE+&H110000)
Public Const KSERROR_DEVICE_IO_FAILED           = (KSERROR_CATEGORY_BASE+&H120000)
Public Const KSERROR_REGISTRY_FAILURE           = (KSERROR_CATEGORY_BASE+&H130000)
Public Const KSERROR_CANNOT_START_KERNEL        = (KSERROR_CATEGORY_BASE+&H140000)
Public Const KSERROR_CANNOT_STOP_KERNEL         = (KSERROR_CATEGORY_BASE+&H150000)
Public Const KSERROR_ACCESS_DENIED              = (KSERROR_CATEGORY_BASE+&H160000)
Public Const KSERROR_DEVICE_NOT_OPENED          = (KSERROR_CATEGORY_BASE+&H170000)
Public Const KSERROR_FUNCTION_DENIED            = (KSERROR_CATEGORY_BASE+&H180000)
Public Const KSERROR_DEVICE_ALREADY_USED        = (KSERROR_CATEGORY_BASE+&H190000)
Public Const KSERROR_OUT_OF_MEMORY              = (KSERROR_CATEGORY_BASE+&H1A0000)
Public Const KSERROR_TOO_MANY_OPEN_PROCESSES    = (KSERROR_CATEGORY_BASE+&H1B0000)
Public Const KSERROR_SIGNAL_OVERFLOW            = (KSERROR_CATEGORY_BASE+&H1C0000)
Public Const KSERROR_CANNOT_SIGNAL              = (KSERROR_CATEGORY_BASE+&H1D0000)
Public Const KSERROR_FILE_NOT_FOUND             = (KSERROR_CATEGORY_BASE+&H1E0000)
Public Const KSERROR_DEVICE_NOT_FOUND           = (KSERROR_CATEGORY_BASE+&H1F0000)
Public Const KSERROR_ASSERTION_FAILED           = (KSERROR_CATEGORY_BASE+&H200000)
Public Const KSERROR_FUNCTION_FAILED            = (KSERROR_CATEGORY_BASE+&H210000)
Public Const KSERROR_OPERATION_ABORTED          = (KSERROR_CATEGORY_BASE+&H220000)
Public Const KSERROR_TIMEOUT                    = (KSERROR_CATEGORY_BASE+&H230000)
Public Const KSERROR_REBOOT_NECESSARY           = (KSERROR_CATEGORY_BASE+&H240000)
Public Const KSERROR_DEVICE_DISABLED            = (KSERROR_CATEGORY_BASE+&H250000)
Public Const KSERROR_ATTACH_FAILED              = (KSERROR_CATEGORY_BASE+&H260000)
Public Const KSERROR_DEVICE_REMOVED             = (KSERROR_CATEGORY_BASE+&H270000)
Public Const KSERROR_TYPE_MISMATCH              = (KSERROR_CATEGORY_BASE+&H280000)
Public Const KSERROR_FUNCTION_NOT_IMPLEMENTED   = (KSERROR_CATEGORY_BASE+&H290000)
Public Const KSERROR_COMMUNICATION_BROKEN       = (KSERROR_CATEGORY_BASE+&H2A0000)
Public Const KSERROR_DEVICE_NOT_READY           = (KSERROR_CATEGORY_BASE+&H370000)
Public Const KSERROR_NO_RESPONSE                = (KSERROR_CATEGORY_BASE+&H380000)
Public Const KSERROR_OPERATION_PENDING          = (KSERROR_CATEGORY_BASE+&H390000)
Public Const KSERROR_PORT_BUSY                  = (KSERROR_CATEGORY_BASE+&H3A0000)
Public Const KSERROR_UNKNOWN_SYSTEM             = (KSERROR_CATEGORY_BASE+&H3B0000)
Public Const KSERROR_BAD_CONTEXT                = (KSERROR_CATEGORY_BASE+&H3C0000)
Public Const KSERROR_END_OF_FILE                = (KSERROR_CATEGORY_BASE+&H3D0000)
Public Const KSERROR_INTERRUPT_ACTIVATION_FAILED 
                                                = (KSERROR_CATEGORY_BASE+&H3E0000)
Public Const KSERROR_INTERRUPT_IS_SHARED        = (KSERROR_CATEGORY_BASE+&H3F0000)
Public Const KSERROR_NO_REALTIME_ACCESS         = (KSERROR_CATEGORY_BASE+&H400000)
Public Const KSERROR_HARDWARE_NOT_SUPPORTED     = (KSERROR_CATEGORY_BASE+&H410000)
Public Const KSERROR_TIMER_ACTIVATION_FAILED    = (KSERROR_CATEGORY_BASE+&H420000)
Public Const KSERROR_SOCKET_FAILURE             = (KSERROR_CATEGORY_BASE+&H430000)
Public Const KSERROR_LINE_ERROR                 = (KSERROR_CATEGORY_BASE+&H440000)
Public Const KSERROR_STACK_CORRUPTION           = (KSERROR_CATEGORY_BASE+&H450000)
Public Const KSERROR_INVALID_ARGUMENT           = (KSERROR_CATEGORY_BASE+&H460000)
Public Const KSERROR_PARSE_ERROR                = (KSERROR_CATEGORY_BASE+&H470000)
Public Const KSERROR_INVALID_IDENTIFIER         = (KSERROR_CATEGORY_BASE+&H480000)
Public Const KSERROR_REALTIME_ALREADY_USED      = (KSERROR_CATEGORY_BASE+&H490000)
Public Const KSERROR_COMMUNICATION_FAILED       = (KSERROR_CATEGORY_BASE+&H4A0000)
Public Const KSERROR_INVALID_SIZE               = (KSERROR_CATEGORY_BASE+&H4B0000)
Public Const KSERROR_OBJECT_NOT_FOUND           = (KSERROR_CATEGORY_BASE+&H4C0000)

'------ Error codes of handling category ------
Public Const KSERROR_CANNOT_CREATE_EVENT        = (KSERROR_CATEGORY_HANDLING+&H0)
Public Const KSERROR_CANNOT_CREATE_THREAD       = (KSERROR_CATEGORY_HANDLING+&H10000)
Public Const KSERROR_CANNOT_BLOCK_THREAD        = (KSERROR_CATEGORY_HANDLING+&H20000)
Public Const KSERROR_CANNOT_SIGNAL_EVENT        = (KSERROR_CATEGORY_HANDLING+&H30000)
Public Const KSERROR_CANNOT_POST_MESSAGE        = (KSERROR_CATEGORY_HANDLING+&H40000)
Public Const KSERROR_CANNOT_CREATE_SHARED       = (KSERROR_CATEGORY_HANDLING+&H50000)
Public Const KSERROR_SHARED_DIFFERENT_SIZE      = (KSERROR_CATEGORY_HANDLING+&H60000)
Public Const KSERROR_BAD_SHARED_MEM             = (KSERROR_CATEGORY_HANDLING+&H70000)
Public Const KSERROR_WAIT_TIMEOUT               = (KSERROR_CATEGORY_HANDLING+&H80000)
Public Const KSERROR_EVENT_NOT_SIGNALED         = (KSERROR_CATEGORY_HANDLING+&H90000)
Public Const KSERROR_CANNOT_CREATE_CALLBACK     = (KSERROR_CATEGORY_HANDLING+&HA0000)
Public Const KSERROR_NO_DATA_AVAILABLE          = (KSERROR_CATEGORY_HANDLING+&HB0000)
Public Const KSERROR_REQUEST_NESTED             = (KSERROR_CATEGORY_HANDLING+&HC0000)

'------ Deal error codes ------
Public Const KSERROR_BAD_CUSTNUM                = (KSERROR_CATEGORY_DEAL+&H30000)
Public Const KSERROR_KEY_BAD_FORMAT             = (KSERROR_CATEGORY_DEAL+&H60000)
Public Const KSERROR_KEY_BAD_DATA               = (KSERROR_CATEGORY_DEAL+&H80000)
Public Const KSERROR_KEY_PERMISSION_EXPIRED     = (KSERROR_CATEGORY_DEAL+&H90000)
Public Const KSERROR_BAD_LICENCE                = (KSERROR_CATEGORY_DEAL+&HA0000)
Public Const KSERROR_CANNOT_LOAD_KEY            = (KSERROR_CATEGORY_DEAL+&HC0000)
Public Const KSERROR_BAD_NAME_OR_FIRM           = (KSERROR_CATEGORY_DEAL+&HF0000)
Public Const KSERROR_NO_LICENCES_FOUND          = (KSERROR_CATEGORY_DEAL+&H100000)
Public Const KSERROR_LICENCE_CAPACITY_EXHAUSTED = (KSERROR_CATEGORY_DEAL+&H110000)
Public Const KSERROR_FEATURE_NOT_LICENSED       = (KSERROR_CATEGORY_DEAL+&H120000)

'------ Exception Codes ------
Public Const KSERROR_DIVIDE_BY_ZERO             = (KSERROR_CATEGORY_EXCEPTION+&H0)
Public Const KSERROR_DEBUG_EXCEPTION            = (KSERROR_CATEGORY_EXCEPTION+&H10000)
Public Const KSERROR_NONMASKABLE_INTERRUPT      = (KSERROR_CATEGORY_EXCEPTION+&H20000)
Public Const KSERROR_BREAKPOINT                 = (KSERROR_CATEGORY_EXCEPTION+&H30000)
Public Const KSERROR_OVERFLOW                   = (KSERROR_CATEGORY_EXCEPTION+&H40000)
Public Const KSERROR_BOUND_RANGE                = (KSERROR_CATEGORY_EXCEPTION+&H50000)
Public Const KSERROR_INVALID_OPCODE             = (KSERROR_CATEGORY_EXCEPTION+&H60000)
Public Const KSERROR_NO_MATH                    = (KSERROR_CATEGORY_EXCEPTION+&H70000)
Public Const KSERROR_DOUBLE_FAULT               = (KSERROR_CATEGORY_EXCEPTION+&H80000)
Public Const KSERROR_COPROCESSOR_SEGMENT_OVERRUN 
                                                = (KSERROR_CATEGORY_EXCEPTION+&H90000)
Public Const KSERROR_INVALID_TSS                = (KSERROR_CATEGORY_EXCEPTION+&HA0000)
Public Const KSERROR_SEGMENT_NOT_PRESENT        = (KSERROR_CATEGORY_EXCEPTION+&HB0000)
Public Const KSERROR_STACK_FAULT                = (KSERROR_CATEGORY_EXCEPTION+&HC0000)
Public Const KSERROR_PROTECTION_FAULT           = (KSERROR_CATEGORY_EXCEPTION+&HD0000)
Public Const KSERROR_PAGE_FAULT                 = (KSERROR_CATEGORY_EXCEPTION+&HE0000)
Public Const KSERROR_FPU_FAULT                  = (KSERROR_CATEGORY_EXCEPTION+&H100000)
Public Const KSERROR_ALIGNMENT_CHECK            = (KSERROR_CATEGORY_EXCEPTION+&H110000)
Public Const KSERROR_MACHINE_CHECK              = (KSERROR_CATEGORY_EXCEPTION+&H120000)
Public Const KSERROR_SIMD_FAULT                 = (KSERROR_CATEGORY_EXCEPTION+&H130000)

'------ Flags ------
Public Const KSF_OPENED                         = &H1000000
Public Const KSF_ACTIVE                         = &H2000000
Public Const KSF_RUNNING                        = &H2000000
Public Const KSF_FINISHED                       = &H4000000
Public Const KSF_CANCELED                       = &H8000000
Public Const KSF_DISABLED                       = &H10000000
Public Const KSF_REQUESTED                      = &H20000000
Public Const KSF_TERMINATE                      = &H0
Public Const KSF_DONT_WAIT                      = &H1
Public Const KSF_KERNEL_EXEC                    = &H4
Public Const KSF_ASYNC_EXEC                     = &HC
Public Const KSF_DIRECT_EXEC                    = &HC
Public Const KSF_DONT_START                     = &H10
Public Const KSF_ONE_SHOT                       = &H20
Public Const KSF_SINGLE_SHOT                    = &H20
Public Const KSF_SAVE_FPU                       = &H40
Public Const KSF_USE_SSE                        = &H1
Public Const KSF_USER_EXEC                      = &H80
Public Const KSF_REALTIME_EXEC                  = &H100
Public Const KSF_WAIT                           = &H200
Public Const KSF_ACCEPT_INCOMPLETE              = &H200
Public Const KSF_REPORT_RESOURCE                = &H400
Public Const KSF_USE_TIMEOUTS                   = &H400
Public Const KSF_FORCE_OVERRIDE                 = &H1000
Public Const KSF_OPTION_ON                      = &H1000
Public Const KSF_OPTION_OFF                     = &H2000
Public Const KSF_FORCE_OPEN                     = &H2000
Public Const KSF_UNLIMITED                      = &H4000
Public Const KSF_DONT_USE_SMP                   = &H4000
Public Const KSF_NO_CONTEXT                     = &H8000
Public Const KSF_HIGH_PRECISION                 = &H8000
Public Const KSF_CONTINUOUS                     = &H10000
Public Const KSF_ZERO_INIT                      = &H20000
Public Const KSF_TX_PDO                         = &H10000
Public Const KSF_RX_PDO                         = &H20000
Public Const KSF_READ                           = &H40000
Public Const KSF_WRITE                          = &H80000
Public Const KSF_USE_SMP                        = &H80000
Public Const KSF_ALTERNATIVE                    = &H200000
Public Const KSF_PDO                            = &H40000
Public Const KSF_SDO                            = &H80000
Public Const KSF_IDN                            = &H100000
Public Const KSF_MANUAL_RESET                   = &H200000
Public Const KSF_RESET_STATE                    = &H800000
Public Const KSF_ISA_BUS                        = &H1
Public Const KSF_PCI_BUS                        = &H5
Public Const KSF_PRIO_NORMAL                    = &H0
Public Const KSF_PRIO_LOW                       = &H1
Public Const KSF_PRIO_LOWER                     = &H2
Public Const KSF_PRIO_LOWEST                    = &H3
Public Const KSF_MESSAGE_PIPE                   = &H1000
Public Const KSF_PIPE_NOTIFY_GET                = &H2000
Public Const KSF_PIPE_NOTIFY_PUT                = &H4000
Public Const KSF_ALL_CPUS                       = &H0
Public Const KSF_READ_ACCESS                    = &H1
Public Const KSF_WRITE_ACCESS                   = &H2
Public Const KSF_SIZE_8_BIT                     = &H10000
Public Const KSF_SIZE_16_BIT                    = &H20000
Public Const KSF_SIZE_32_BIT                    = &H40000

'------ Obsolete flags - do not use! ------
Public Const KSF_ACCEPT_PENDING                 = &H2
Public Const KSF_DONT_RAISE                     = &H200000

'------ Constant for returning no-error ------
Public Const KS_OK                              = &H0

'------ Language constants ------
Public Const KSLNG_DEFAULT                      = &H0

'------ Context type values ------
Public Const USER_CONTEXT                       = &H0
Public Const INTERRUPT_CONTEXT                  = &H100
Public Const TIMER_CONTEXT                      = &H200
Public Const KEYBOARD_CONTEXT                   = &H300
Public Const USB_BASE                           = &H506
Public Const PACKET_BASE                        = &H700
Public Const DEVICE_BASE                        = &H800
Public Const ETHERCAT_BASE                      = &H900
Public Const SOCKET_BASE                        = &HA00
Public Const TASK_BASE                          = &HB00
Public Const MULTIFUNCTION_BASE                 = &HC00
Public Const CAN_BASE                           = &HD00
Public Const PROFIBUS_BASE                      = &HE00
Public Const CANOPEN_BASE                       = &HF00
Public Const FLEXRAY_BASE                       = &H1000
Public Const XHCI_BASE                          = &H1100
Public Const V86_BASE                           = &H1200
Public Const PLC_BASE                           = &H2000

'------ Config code for "Driver" ------
Public Const KSCONFIG_DRIVER_NAME               = &H1
Public Const KSCONFIG_FUNCTION_LOGGING          = &H2

'------ Config code for "Base" ------
Public Const KSCONFIG_DISABLE_MESSAGE_LOGGING   = &H1
Public Const KSCONFIG_ENABLE_MESSAGE_LOGGING    = &H2
Public Const KSCONFIG_FLUSH_MESSAGE_PIPE        = &H3

'------ Config code for "Debug" ------

'------ Commonly used commands ------
Public Const KS_ABORT_XMIT_CMD                  = &H1
Public Const KS_ABORT_RECV_CMD                  = &H2
Public Const KS_FLUSH_XMIT_BUF                  = &H3
Public Const KS_FLUSH_RECV_BUF                  = &H9
Public Const KS_SET_BAUD_RATE                   = &HC

'------ Pipe contexts ------
Public Const PIPE_PUT_CONTEXT                   = (USER_CONTEXT+&H40)
Public Const PIPE_GET_CONTEXT                   = (USER_CONTEXT+&H41)

'------ Quick mutex levels ------
Public Const KS_APP_LEVEL                       = &H0
Public Const KS_DPC_LEVEL                       = &H1
Public Const KS_ISR_LEVEL                       = &H2
Public Const KS_RTX_LEVEL                       = &H3
Public Const KS_CPU_LEVEL                       = &H4

'------ Data types ------
Public Const KS_DATATYPE_UNKNOWN                = &H0
Public Const KS_DATATYPE_BOOLEAN                = &H1
Public Const KS_DATATYPE_INTEGER8               = &H2
Public Const KS_DATATYPE_UINTEGER8              = &H5
Public Const KS_DATATYPE_INTEGER16              = &H3
Public Const KS_DATATYPE_UINTEGER16             = &H6
Public Const KS_DATATYPE_INTEGER32              = &H4
Public Const KS_DATATYPE_UINTEGER32             = &H7
Public Const KS_DATATYPE_INTEGER64              = &H15
Public Const KS_DATATYPE_UNSIGNED64             = &H1B
Public Const KS_DATATYPE_REAL32                 = &H8
Public Const KS_DATATYPE_REAL64                 = &H11
Public Const KS_DATATYPE_TINY                   = &H2
Public Const KS_DATATYPE_BYTE                   = &H5
Public Const KS_DATATYPE_SHORT                  = &H3
Public Const KS_DATATYPE_USHORT                 = &H6
Public Const KS_DATATYPE_INT                    = &H4
Public Const KS_DATATYPE_UINT                   = &H7
Public Const KS_DATATYPE_LLONG                  = &H15
Public Const KS_DATATYPE_ULONG                  = &H1B
Public Const KS_DATATYPE_VISIBLE_STRING         = &H9
Public Const KS_DATATYPE_OCTET_STRING           = &HA
Public Const KS_DATATYPE_UNICODE_STRING         = &HB
Public Const KS_DATATYPE_TIME_OF_DAY            = &HC
Public Const KS_DATATYPE_TIME_DIFFERENCE        = &HD
Public Const KS_DATATYPE_DOMAIN                 = &HF
Public Const KS_DATATYPE_TIME_OF_DAY32          = &H1C
Public Const KS_DATATYPE_DATE32                 = &H1D
Public Const KS_DATATYPE_DATE_AND_TIME64        = &H1E
Public Const KS_DATATYPE_TIME_DIFFERENCE64      = &H1F
Public Const KS_DATATYPE_BIT1                   = &H30
Public Const KS_DATATYPE_BIT2                   = &H31
Public Const KS_DATATYPE_BIT3                   = &H32
Public Const KS_DATATYPE_BIT4                   = &H33
Public Const KS_DATATYPE_BIT5                   = &H34
Public Const KS_DATATYPE_BIT6                   = &H35
Public Const KS_DATATYPE_BIT7                   = &H36
Public Const KS_DATATYPE_BIT8                   = &H37
Public Const KS_DATATYPE_INTEGER24              = &H10
Public Const KS_DATATYPE_UNSIGNED24             = &H16
Public Const KS_DATATYPE_INTEGER40              = &H12
Public Const KS_DATATYPE_UNSIGNED40             = &H18
Public Const KS_DATATYPE_INTEGER48              = &H13
Public Const KS_DATATYPE_UNSIGNED48             = &H19
Public Const KS_DATATYPE_INTEGER56              = &H14
Public Const KS_DATATYPE_UNSIGNED56             = &H1A
Public Const KS_DATATYPE_KSHANDLE               = &H40

'------ DataObj type flags ------
Public Const KS_DATAOBJ_READABLE                = &H10000
Public Const KS_DATAOBJ_WRITEABLE               = &H20000
Public Const KS_DATAOBJ_ARRAY                   = &H1000000
Public Const KS_DATAOBJ_ENUM                    = &H2000000
Public Const KS_DATAOBJ_STRUCT                  = &H4000000

'------ Common types and structures ------
Public Type UInt64
  lo As Long
  hi As Long
End Type

Public Type HandlerState
  requested As Long
  performed As Long
  state As Long
  error As Long
End Type

Public Type KSTimeOut
  baseTime As Long
  charTime As Long
End Type

Public Type KSDeviceInfo
  structSize As Long
  pDeviceName(0 To 79) As Byte
  pDeviceDesc(0 To 79) As Byte
  pDeviceVendor(0 To 79) As Byte
  pFriendlyName(0 To 79) As Byte
  pHardwareId(0 To 79) As Byte
  pClassName(0 To 79) As Byte
  pInfPath(0 To 79) As Byte
  pDriverDesc(0 To 79) As Byte
  pDriverVendor(0 To 79) As Byte
  pDriverDate(0 To 79) As Byte
  pDriverVersion(0 To 79) As Byte
  pServiceName(0 To 79) As Byte
  pServiceDisp(0 To 79) As Byte
  pServiceDesc(0 To 79) As Byte
  pServicePath(0 To 79) As Byte
End Type

Public Type KSSystemInformation
  structSize As Long
  numberOfCPUs As Long
  numberOfSharedCPUs As Long
  kiloBytesOfRAM As Long
  isDll64Bit As Byte
  isSys64Bit As Byte
  isStableVersion As Byte
  isLogVersion As Byte
  dllVersion As Long
  dllBuildNumber As Long
  sysVersion As Long
  sysBuildNumber As Long
  systemVersion As Long
  pSystemName(0 To 63) As Byte
  pServicePack(0 To 63) As Byte
  pDllPath(0 To 255) As Byte
  pSysPath(0 To 255) As Byte
End Type

Public Type KSProcessorInformation
  structSize As Long
  index As Long
  group As Long
  number As Long
  currentTemperature As Long
  allowedTemperature As Long
End Type

Public Type KSSharedMemInfo
  structSize As Long
  hHandle As Long
  pName(0 To 255) As Byte
  size As Long
  pThisPtr As Long
End Type

'------ Context structures ------
Public Type PipeUserContext
  ctxType As Long
  hPipe As Long
End Type

'------ Common functions ------
Declare Function KS_openDriver Lib "KrtsDemo.dll" ( _
  ByVal customerNumber As String) As Long
Declare Function KS_closeDriver Lib "KrtsDemo.dll" ( _
  ) As Long
Declare Function KS_getDriverVersion Lib "KrtsDemo.dll" ( _
  ByRef pVersion As Long) As Long
Declare Function KS_getErrorString Lib "KrtsDemo.dll" ( _
  ByVal code As Long, ByVal msg As ???, ByVal language As Long) As Long
Declare Function KS_getErrorStringEx Lib "KrtsDemo.dll" ( _
  ByVal code As Long, ByVal pMsgBuf As String, ByVal language As Long) As Long
Declare Function KS_addErrorString Lib "KrtsDemo.dll" ( _
  ByVal code As Long, ByVal msg As String, ByVal language As Long) As Long
Declare Function KS_getDriverConfig Lib "KrtsDemo.dll" ( _
  ByVal module As String, ByVal configType As Long, ByRef pData As Any) As Long
Declare Function KS_setDriverConfig Lib "KrtsDemo.dll" ( _
  ByVal module As String, ByVal configType As Long, ByRef pData As Any) As Long
Declare Function KS_getSystemInformation Lib "KrtsDemo.dll" ( _
  ByRef pSystemInfo As KSSystemInformation, ByVal flags As Long) As Long
Declare Function KS_getProcessorInformation Lib "KrtsDemo.dll" ( _
  ByVal index As Long, ByRef pProcessorInfo As KSProcessorInformation, ByVal flags As Long) As Long
Declare Function KS_closeHandle Lib "KrtsDemo.dll" ( _
  ByVal handle As Long, ByVal flags As Long) As Long

'------ Debugging & test ------
Declare Function KS_showMessage Lib "KrtsDemo.dll" ( _
  ByVal ksError As Long, ByVal msg As String) As Long
Declare Function KS_logMessage Lib "KrtsDemo.dll" ( _
  ByVal msgType As Long, ByVal msgText As String) As Long
Declare Function KS_bufMessage Lib "KrtsDemo.dll" ( _
  ByVal msgType As Long, ByRef pBuffer As Any, ByVal length As Long, ByVal msgText As String) As Long
Declare Function KS_dbgMessage Lib "KrtsDemo.dll" ( _
  ByVal fileName As String, ByVal line As Long, ByVal msgText As String) As Long
Declare Function KS_beep Lib "KrtsDemo.dll" ( _
  ByVal freq As Long, ByVal duration As Long) As Long
Declare Function KS_throwException Lib "KrtsDemo.dll" ( _
  ByVal error As Long, ByVal pText As String, ByVal pFile As String, ByVal line As Long) As Long

'------ Device handling ------
Declare Function KS_enumDevices Lib "KrtsDemo.dll" ( _
  ByVal deviceType As String, ByVal index As Long, ByVal pDeviceNameBuf As String, ByVal flags As Long) As Long
Declare Function KS_enumDevicesEx Lib "KrtsDemo.dll" ( _
  ByVal className As String, ByVal busType As String, ByVal hEnumerator As Long, ByVal index As Long, ByVal pDeviceNameBuf As String, ByVal flags As Long) As Long
Declare Function KS_getDevices Lib "KrtsDemo.dll" ( _
  ByVal className As String, ByVal busType As String, ByVal hEnumerator As Long, ByRef pCount As Long, ByRef pBuf As Long, ByVal size As Long, ByVal flags As Long) As Long
Declare Function KS_getDeviceInfo Lib "KrtsDemo.dll" ( _
  ByVal deviceName As String, ByRef pDeviceInfo As KSDeviceInfo, ByVal flags As Long) As Long
Declare Function KS_updateDriver Lib "KrtsDemo.dll" ( _
  ByVal deviceName As String, ByVal fileName As String, ByVal flags As Long) As Long

'------ Threads & priorities ------
Declare Function KS_removeThread Lib "KrtsDemo.dll" ( _
  ) As Long
Declare Function KS_setThreadPrio Lib "KrtsDemo.dll" ( _
  ByVal prio As Long) As Long
Declare Function KS_getThreadPrio Lib "KrtsDemo.dll" ( _
  ByRef pPrio As Long) As Long

'------ Shared memory ------
Declare Function KS_createSharedMem Lib "KrtsDemo.dll" ( _
  ByRef ppAppPtr As Long, ByRef ppSysPtr As Long, ByVal name As String, ByVal size As Long, ByVal flags As Long) As Long
Declare Function KS_freeSharedMem Lib "KrtsDemo.dll" ( _
  ByVal pAppPtr As Long) As Long
Declare Function KS_getSharedMem Lib "KrtsDemo.dll" ( _
  ByRef ppPtr As Long, ByVal name As String) As Long
Declare Function KS_readMem Lib "KrtsDemo.dll" ( _
  ByRef pBuffer As Any, ByVal pAppPtr As Long, ByVal size As Long, ByVal offset As Long) As Long
Declare Function KS_writeMem Lib "KrtsDemo.dll" ( _
  ByRef pBuffer As Any, ByVal pAppPtr As Long, ByVal size As Long, ByVal offset As Long) As Long
Declare Function KS_createSharedMemEx Lib "KrtsDemo.dll" ( _
  ByRef phHandle As Long, ByVal name As String, ByVal size As Long, ByVal flags As Long) As Long
Declare Function KS_freeSharedMemEx Lib "KrtsDemo.dll" ( _
  ByVal hHandle As Long, ByVal flags As Long) As Long
Declare Function KS_getSharedMemEx Lib "KrtsDemo.dll" ( _
  ByVal hHandle As Long, ByRef pPtr As Long, ByVal flags As Long) As Long

'------ Events ------
Declare Function KS_createEvent Lib "KrtsDemo.dll" ( _
  ByRef phEvent As Long, ByVal name As String, ByVal flags As Long) As Long
Declare Function KS_closeEvent Lib "KrtsDemo.dll" ( _
  ByVal hEvent As Long) As Long
Declare Function KS_setEvent Lib "KrtsDemo.dll" ( _
  ByVal hEvent As Long) As Long
Declare Function KS_resetEvent Lib "KrtsDemo.dll" ( _
  ByVal hEvent As Long) As Long
Declare Function KS_pulseEvent Lib "KrtsDemo.dll" ( _
  ByVal hEvent As Long) As Long
Declare Function KS_waitForEvent Lib "KrtsDemo.dll" ( _
  ByVal hEvent As Long, ByVal flags As Long, ByVal timeout As Long) As Long
Declare Function KS_getEventState Lib "KrtsDemo.dll" ( _
  ByVal hEvent As Long, ByRef pState As Long) As Long
Declare Function KS_postMessage Lib "KrtsDemo.dll" ( _
  ByVal hWnd As Long, ByVal msg As Long, ByVal wP As Long, ByVal lP As Long) As Long

'------ CallBacks ------
Declare Function KS_removeCallBack Lib "KrtsDemo.dll" ( _
  ByVal hCallBack As Long) As Long
Declare Function KS_execCallBack Lib "KrtsDemo.dll" ( _
  ByVal hCallBack As Long, ByRef pContext As Any, ByVal flags As Long) As Long
Declare Function KS_getCallState Lib "KrtsDemo.dll" ( _
  ByVal hSignal As Long, ByRef pState As HandlerState) As Long
Declare Function KS_signalObject Lib "KrtsDemo.dll" ( _
  ByVal hObject As Long, ByRef pContext As Any, ByVal flags As Long) As Long

'------ Pipes ------
Declare Function KS_createPipe Lib "KrtsDemo.dll" ( _
  ByRef phPipe As Long, ByVal name As String, ByVal itemSize As Long, ByVal itemCount As Long, ByVal hNotify As Long, ByVal flags As Long) As Long
Declare Function KS_removePipe Lib "KrtsDemo.dll" ( _
  ByVal hPipe As Long) As Long
Declare Function KS_flushPipe Lib "KrtsDemo.dll" ( _
  ByVal hPipe As Long, ByVal flags As Long) As Long
Declare Function KS_getPipe Lib "KrtsDemo.dll" ( _
  ByVal hPipe As Long, ByRef pBuffer As Any, ByVal length As Long, ByRef pLength As Long, ByVal flags As Long) As Long
Declare Function KS_putPipe Lib "KrtsDemo.dll" ( _
  ByVal hPipe As Long, ByRef pBuffer As Any, ByVal length As Long, ByRef pLength As Long, ByVal flags As Long) As Long

'------ Quick mutexes ------
Declare Function KS_createQuickMutex Lib "KrtsDemo.dll" ( _
  ByRef phMutex As Long, ByVal level As Long, ByVal flags As Long) As Long
Declare Function KS_removeQuickMutex Lib "KrtsDemo.dll" ( _
  ByVal hMutex As Long) As Long
Declare Function KS_requestQuickMutex Lib "KrtsDemo.dll" ( _
  ByVal hMutex As Long) As Long
Declare Function KS_releaseQuickMutex Lib "KrtsDemo.dll" ( _
  ByVal hMutex As Long) As Long
Declare Function KS_createCallBack Lib "Kbeta.dll" Alias "KS_createCallBackEx" ( _
  ByRef phCallBack As Long, ByVal routine As Long, ByRef pArgs As Any, ByVal flags As Long, ByVal prio As Long ) As Long
Declare Function KS_createThread Lib "Kbeta.dll" Alias "KS_createThreadEx" ( _
  ByVal routine As Long, ByRef pArgs As Any, ByRef phThread As Long ) As Long

'---------------------------------------------------------------------------------------------------------------
' Kernel Module
'---------------------------------------------------------------------------------------------------------------

'------ Error Codes ------
Public Const KSERROR_RTX_TOO_MANY_FUNCTIONS     = (KSERROR_CATEGORY_KERNEL+&H0)
Public Const KSERROR_CANNOT_EXECUTE_RTX_CALL    = (KSERROR_CATEGORY_KERNEL+&H10000)
Public Const KSERROR_CANNOT_PREPARE_RTX         = (KSERROR_CATEGORY_KERNEL+&H20000)
Public Const KSERROR_RTX_BAD_SYSTEM_CALL        = (KSERROR_CATEGORY_KERNEL+&H30000)
Public Const KSERROR_CANNOT_LOAD_KERNEL         = (KSERROR_CATEGORY_KERNEL+&H40000)
Public Const KSERROR_CANNOT_RELOCATE_KERNEL     = (KSERROR_CATEGORY_KERNEL+&H50000)
Public Const KSERROR_KERNEL_ALREADY_LOADED      = (KSERROR_CATEGORY_KERNEL+&H60000)
Public Const KSERROR_KERNEL_NOT_LOADED          = (KSERROR_CATEGORY_KERNEL+&H70000)

'------ Flags ------
Public Const KSF_ANALYSE_ONLY                   = &H10000
Public Const KSF_FUNC_TABLE_GIVEN               = &H20000
Public Const KSF_FORCE_RELOC                    = &H40000
Public Const KSF_LOAD_DEPENDENCIES              = &H1000
Public Const KSF_EXEC_ENTRY_POINT               = &H2000

'------ Kernel commands ------
Public Const KS_GET_KERNEL_BASE_ADDRESS         = &H1

'------ Relocation by byte-wise instruction mapping ------

'------ Relocation by loading DLL into kernel-space ------
Declare Function KS_loadKernel Lib "KrtsDemo.dll" ( _
  ByRef phKernel As Long, ByVal dllName As String, ByVal initProcName As String, ByRef pArgs As Any, ByVal flags As Long) As Long
Declare Function KS_loadKernelFromBuffer Lib "KrtsDemo.dll" ( _
  ByRef phKernel As Long, ByVal dllName As String, ByVal pBuffer As Long, ByVal length As Long, ByVal initProcName As String, ByRef pArgs As Any, ByVal flags As Long) As Long
Declare Function KS_freeKernel Lib "KrtsDemo.dll" ( _
  ByVal hKernel As Long) As Long
Declare Function KS_execKernelFunction Lib "KrtsDemo.dll" ( _
  ByVal hKernel As Long, ByVal name As String, ByRef pArgs As Any, ByRef pContext As Any, ByVal flags As Long) As Long
Declare Function KS_getKernelFunction Lib "KrtsDemo.dll" ( _
  ByVal hKernel As Long, ByVal name As String, ByRef pRelocated As Long, ByVal flags As Long) As Long
Declare Function KS_enumKernelFunctions Lib "KrtsDemo.dll" ( _
  ByVal hKernel As Long, ByVal index As Long, ByVal pName As String, ByVal flags As Long) As Long
Declare Function KS_execKernelCommand Lib "KrtsDemo.dll" ( _
  ByVal hKernel As Long, ByVal command As Long, ByRef pParam As Any, ByVal flags As Long) As Long
Declare Function KS_createKernelCallBack Lib "KrtsDemo.dll" ( _
  ByRef phCallBack As Long, ByVal hKernel As Long, ByVal name As String, ByRef pArgs As Any, ByVal flags As Long, ByVal prio As Long) As Long

'------ Helper functions ------

'------ Memory functions ------
Declare Function KS_memCpy Lib "KrtsDemo.dll" ( _
  ByRef pDst As Any, ByRef pSrc As Any, ByVal size As Long, ByVal flags As Long) As Long
Declare Function KS_memMove Lib "KrtsDemo.dll" ( _
  ByRef pDst As Any, ByRef pSrc As Any, ByVal size As Long, ByVal flags As Long) As Long
Declare Function KS_memSet Lib "KrtsDemo.dll" ( _
  ByRef pMem As Any, ByVal value As Long, ByVal size As Long, ByVal flags As Long) As Long

'------ Interlocked functions ------

'---------------------------------------------------------------------------------------------------------------
' IoPort Module
'---------------------------------------------------------------------------------------------------------------

'------ Error Codes ------
Public Const KSERROR_CANNOT_ENABLE_IO_RANGE     = (KSERROR_CATEGORY_IOPORT+&H0)
Public Const KSERROR_BUS_NOT_FOUND              = (KSERROR_CATEGORY_IOPORT+&H10000)
Public Const KSERROR_BAD_SLOT_NUMBER            = (KSERROR_CATEGORY_IOPORT+&H20000)

'------ Number of address entries in PCI data ------
Public Const KS_PCI_TYPE0_ADDRESSES             = &H6
Public Const KS_PCI_TYPE1_ADDRESSES             = &H2
Public Const KS_PCI_TYPE2_ADDRESSES             = &H4

'------ Bit encodings for KSPciBusData.headerType ------
Public Const KSF_PCI_MULTIFUNCTION              = &H80
Public Const KSF_PCI_DEVICE_TYPE                = &H0
Public Const KSF_PCI_PCI_BRIDGE_TYPE            = &H1
Public Const KSF_PCI_CARDBUS_BRIDGE_TYPE        = &H2

'------ Bit encodings for KSPciBusData.command ------
Public Const KSF_PCI_ENABLE_IO_SPACE            = &H1
Public Const KSF_PCI_ENABLE_MEMORY_SPACE        = &H2
Public Const KSF_PCI_ENABLE_BUS_MASTER          = &H4
Public Const KSF_PCI_ENABLE_SPECIAL_CYCLES      = &H8
Public Const KSF_PCI_ENABLE_WRITE_AND_INVALIDATE 
                                                = &H10
Public Const KSF_PCI_ENABLE_VGA_COMPATIBLE_PALETTE 
                                                = &H20
Public Const KSF_PCI_ENABLE_PARITY              = &H40
Public Const KSF_PCI_ENABLE_WAIT_CYCLE          = &H80
Public Const KSF_PCI_ENABLE_SERR                = &H100
Public Const KSF_PCI_ENABLE_FAST_BACK_TO_BACK   = &H200

'------ Bit encodings for KSPciBusData.status ------
Public Const KSF_PCI_STATUS_CAPABILITIES_LIST   = &H10
Public Const KSF_PCI_STATUS_FAST_BACK_TO_BACK   = &H80
Public Const KSF_PCI_STATUS_DATA_PARITY_DETECTED 
                                                = &H100
Public Const KSF_PCI_STATUS_DEVSEL              = &H600
Public Const KSF_PCI_STATUS_SIGNALED_TARGET_ABORT 
                                                = &H800
Public Const KSF_PCI_STATUS_RECEIVED_TARGET_ABORT 
                                                = &H1000
Public Const KSF_PCI_STATUS_RECEIVED_MASTER_ABORT 
                                                = &H2000
Public Const KSF_PCI_STATUS_SIGNALED_SYSTEM_ERROR 
                                                = &H4000
Public Const KSF_PCI_STATUS_DETECTED_PARITY_ERROR 
                                                = &H8000

'------ Bit encodings for KSPciBusData.baseAddresses ------
Public Const KSF_PCI_ADDRESS_IO_SPACE           = &H1
Public Const KSF_PCI_ADDRESS_MEMORY_TYPE_MASK   = &H6
Public Const KSF_PCI_ADDRESS_MEMORY_PREFETCHABLE 
                                                = &H8

'------ PCI types ------
Public Const KSF_PCI_TYPE_32BIT                 = &H0
Public Const KSF_PCI_TYPE_20BIT                 = &H2
Public Const KSF_PCI_TYPE_64BIT                 = &H4

'------ Resource types ------
Public Const KSRESOURCE_TYPE_NONE               = &H0
Public Const KSRESOURCE_TYPE_PORT               = &H1
Public Const KSRESOURCE_TYPE_INTERRUPT          = &H2
Public Const KSRESOURCE_TYPE_MEMORY             = &H3
Public Const KSRESOURCE_TYPE_STRING             = &HFF

'------ KSRESOURCE_SHARE ------
Public Const KSRESOURCE_SHARE_UNDETERMINED      = &H0
Public Const KSRESOURCE_SHARE_DEVICEEXCLUSIVE   = &H1
Public Const KSRESOURCE_SHARE_DRIVEREXCLUSIVE   = &H2
Public Const KSRESOURCE_SHARE_SHARED            = &H3

'------ KSRESOURCE_MODE / KSRESOURCE_TYPE_PORT ------
Public Const KSRESOURCE_MODE_MEMORY             = &H0
Public Const KSRESOURCE_MODE_IO                 = &H1

'------ KSRESOURCE_MODE / KSRESOURCE_TYPE_INTERRUPT ------
Public Const KSRESOURCE_MODE_LEVELSENSITIVE     = &H0
Public Const KSRESOURCE_MODE_LATCHED            = &H1

'------ KSRESOURCE_MODE / KSRESOURCE_TYPE_MEMORY ------
Public Const KSRESOURCE_MODE_READ_WRITE         = &H0
Public Const KSRESOURCE_MODE_READ_ONLY          = &H1
Public Const KSRESOURCE_MODE_WRITE_ONLY         = &H2

'------ Types & Structures ------
Public Type KSPciBusData
  vendorID As Integer
  deviceID As Integer
  command As Integer
  status As Integer
  revisionID As Byte
  progIf As Byte
  subClass As Byte
  baseClass As Byte
  cacheLineSize As Byte
  latencyTimer As Byte
  headerType As Byte
  BIST As Byte
  pBaseAddresses(0 To 5) As Long
  CIS As Long
  subVendorID As Integer
  subSystemID As Integer
  baseROMAddress As Long
  capabilityList As Long
  reserved As Long
  interruptLine As Byte
  interruptPin As Byte
  minimumGrant As Byte
  maximumLatency As Byte
  pDeviceSpecific(0 To 191) As Byte
End Type

Public Type KSResourceItem
  flags As Long
  data0 As Long
  data1 As Long
  data2 As Long
End Type

Public Type KSResourceInfo
  busType As Long
  busNumber As Long
  version As Long
  itemCount As Long
  pItems(0 To 7) As KSResourceItem
End Type

Public Type KSRangeResourceItem
  flags As Long
  address As Long
  reserved As Long
  length As Long
End Type

Public Type KSInterruptResourceItem
  flags As Long
  level As Long
  vector As Long
  affinity As Long
End Type

Public Type KSResourceInfoEx
  busType As Long
  busNumber As Long
  version As Long
  itemCount As Long
  range0 As KSRangeResourceItem
  range1 As KSRangeResourceItem
  range2 As KSRangeResourceItem
  range3 As KSRangeResourceItem
  range4 As KSRangeResourceItem
  range5 As KSRangeResourceItem
  interruptItem As KSInterruptResourceItem
  reserved As KSResourceItem
End Type

'------ I/O port access functions ------
Declare Function KS_enableIoRange Lib "KrtsDemo.dll" ( _
  ByVal ioPortBase As Long, ByVal count As Long, ByVal flags As Long) As Long
Declare Function KS_readIoPort Lib "KrtsDemo.dll" ( _
  ByVal ioPort As Long, ByRef pValue As Long, ByVal flags As Long) As Long
Declare Function KS_writeIoPort Lib "KrtsDemo.dll" ( _
  ByVal ioPort As Long, ByVal value As Long, ByVal flags As Long) As Long
Declare Function KS_inpb Lib "KrtsDemo.dll" ( _
  ByVal ioPort As Long) As Long
Declare Function KS_inpw Lib "KrtsDemo.dll" ( _
  ByVal ioPort As Long) As Long
Declare Function KS_inpd Lib "KrtsDemo.dll" ( _
  ByVal ioPort As Long) As Long
Declare Sub KS_outpb Lib "KrtsDemo.dll" ( _
  ByVal ioPort As Long, ByVal value As Long)
Declare Sub KS_outpw Lib "KrtsDemo.dll" ( _
  ByVal ioPort As Long, ByVal value As Long)
Declare Sub KS_outpd Lib "KrtsDemo.dll" ( _
  ByVal ioPort As Long, ByVal value As Long)

'------ Bus data & resource info ------
Declare Function KS_getBusData Lib "KrtsDemo.dll" ( _
  ByRef pBusData As KSPciBusData, ByVal busNumber As Long, ByVal slotNumber As Long, ByVal flags As Long) As Long
Declare Function KS_setBusData Lib "KrtsDemo.dll" ( _
  ByRef pBusData As KSPciBusData, ByVal busNumber As Long, ByVal slotNumber As Long, ByVal flags As Long) As Long
' DO NOT USE! - OBSOLETE! - USE 'KS_getResourceInfoEx' instead!
Declare Function KS_getResourceInfo Lib "KrtsDemo.dll" ( _
  ByVal name As String, ByRef pInfo As KSResourceInfo) As Long
Declare Function KS_getResourceInfoEx Lib "KrtsDemo.dll" ( _
  ByVal name As String, ByRef pInfo As KSResourceInfoEx) As Long

'------ Quiet section (obsolete) ------

'---------------------------------------------------------------------------------------------------------------
' Memory Module
'---------------------------------------------------------------------------------------------------------------

'------ Error Codes ------
Public Const KSERROR_CANNOT_MAP_PHYSMEM         = (KSERROR_CATEGORY_MEMORY+&H0)
Public Const KSERROR_CANNOT_UNMAP_PHYSMEM       = (KSERROR_CATEGORY_MEMORY+&H10000)
Public Const KSERROR_PHYSMEM_DIFFERENT_SIZE     = (KSERROR_CATEGORY_MEMORY+&H20000)
Public Const KSERROR_CANNOT_ALLOC_PHYSMEM       = (KSERROR_CATEGORY_MEMORY+&H30000)
Public Const KSERROR_CANNOT_LOCK_MEM            = (KSERROR_CATEGORY_MEMORY+&H40000)
Public Const KSERROR_CANNOT_UNLOCK_MEM          = (KSERROR_CATEGORY_MEMORY+&H50000)

'------ Flags ------
Public Const KSF_MAP_INTERNAL                   = &H0

'------ Physical memory management functions ------
Declare Function KS_mapDeviceMem Lib "KrtsDemo.dll" ( _
  ByRef ppAppPtr As Long, ByRef ppSysPtr As Long, ByRef pInfo As KSResourceInfoEx, ByVal index As Long, ByVal flags As Long) As Long
Declare Function KS_mapPhysMem Lib "KrtsDemo.dll" ( _
  ByRef ppAppPtr As Long, ByRef ppSysPtr As Long, ByVal physAddr As Long, ByVal size As Long, ByVal flags As Long) As Long
Declare Function KS_unmapPhysMem Lib "KrtsDemo.dll" ( _
  ByVal pAppPtr As Long) As Long
Declare Function KS_allocPhysMem Lib "KrtsDemo.dll" ( _
  ByRef ppAppPtr As Long, ByRef ppSysPtr As Long, ByRef physAddr As Long, ByVal size As Long, ByVal flags As Long) As Long
Declare Function KS_freePhysMem Lib "KrtsDemo.dll" ( _
  ByVal pAppPtr As Long) As Long
Declare Function KS_copyPhysMem Lib "KrtsDemo.dll" ( _
  ByVal physAddr As Long, ByRef pBuffer As Any, ByVal size As Long, ByVal offset As Long, ByVal flags As Long) As Long

' ATTENTION: functions for getting shared memory: see 'Base Module'

'---------------------------------------------------------------------------------------------------------------
' Clock Module
'---------------------------------------------------------------------------------------------------------------

'------ Error Codes ------
Public Const KSERROR_NO_RDTSC_AVAILABLE         = (KSERROR_CATEGORY_SPECIAL+&H10000)

'------ Flags ------
Public Const KSF_USE_GIVEN_FREQ                 = &H10000
Public Const KSF_CLOCK_DELEGATE                 = &H1
Public Const KSF_CLOCK_CONVERT_ONLY             = &H2

'------ Clock source constants (for 'clockId') ------
Public Const KS_CLOCK_DEFAULT                   = &H0
Public Const KS_CLOCK_MEASURE_SYSTEM_TIMER      = &H1
Public Const KS_CLOCK_MEASURE_CPU_COUNTER       = &H2
Public Const KS_CLOCK_MEASURE_APIC_TIMER        = &H3
Public Const KS_CLOCK_MEASURE_PM_TIMER          = &H4
Public Const KS_CLOCK_MEASURE_PC_TIMER          = &H5
Public Const KS_CLOCK_MEASURE_INTERVAL_COUNTER  = &H6
Public Const KS_CLOCK_MEASURE_HPET              = &H7
Public Const KS_CLOCK_MEASURE_HIGHEST           = &H8
Public Const KS_CLOCK_CONVERT_HIGHEST           = &H9
Public Const KS_CLOCK_PC_TIMER                  = &HA
Public Const KS_CLOCK_MACHINE_TIME              = &HB
Public Const KS_CLOCK_ABSOLUTE_TIME             = &HC
Public Const KS_CLOCK_ABSOLUTE_TIME_LOCAL       = &HD
Public Const KS_CLOCK_MICROSECONDS              = &HE
Public Const KS_CLOCK_UNIX_TIME                 = &HF
Public Const KS_CLOCK_UNIX_TIME_LOCAL           = &H10
Public Const KS_CLOCK_MILLISECONDS              = &H11
Public Const KS_CLOCK_DOS_TIME                  = &H12
Public Const KS_CLOCK_DOS_TIME_LOCAL            = &H13
Public Const KS_CLOCK_IEEE1588_TIME             = &H14
Public Const KS_CLOCK_IEEE1588_CONVERT          = &H15
Public Const KS_CLOCK_TIME_OF_DAY               = &H16
Public Const KS_CLOCK_TIME_OF_DAY_LOCAL         = &H17
Public Const KS_CLOCK_FIRST_USER                = &H18

'------ Obsolete clock source constant (use KS_CLOCK_MACHINE_TIME instead!) ------
Public Const KS_CLOCK_SYSTEM_TIME               = &HB

'------ Types & Structures ------
Public Type KSClockSource
  structSize As Long
  flags As Long
  frequency As UInt64
  offset As UInt64
  baseClockId As Long
End Type

'------ Clock functions - Obsolete! Do not use! ------
Declare Function KS_calibrateMachineTime Lib "KrtsDemo.dll" ( _
  ByRef pFrequency As Long, ByVal flags As Long) As Long
Declare Function KS_getSystemTicks Lib "KrtsDemo.dll" ( _
  ByRef pSystemTicks As UInt64) As Long
Declare Function KS_getSystemTime Lib "KrtsDemo.dll" ( _
  ByRef pSystemTime As UInt64) As Long
Declare Function KS_getMachineTime Lib "KrtsDemo.dll" ( _
  ByRef pMachineTime As UInt64) As Long
Declare Function KS_getAbsTime Lib "KrtsDemo.dll" ( _
  ByRef pAbsTime As UInt64) As Long

'------ Clock functions ------
Declare Function KS_getClock Lib "KrtsDemo.dll" ( _
  ByRef pClock As UInt64, ByVal clockId As Long) As Long
Declare Function KS_getClockSource Lib "KrtsDemo.dll" ( _
  ByRef pClockSource As KSClockSource, ByVal clockId As Long) As Long
Declare Function KS_setClockSource Lib "KrtsDemo.dll" ( _
  ByRef pClockSource As KSClockSource, ByVal clockId As Long) As Long
Declare Function KS_microDelay Lib "KrtsDemo.dll" ( _
  ByVal delay As Long) As Long
Declare Function KS_convertClock Lib "KrtsDemo.dll" ( _
  ByRef pValue As UInt64, ByVal clockIdFrom As Long, ByVal clockIdTo As Long, ByVal flags As Long) As Long

'---------------------------------------------------------------------------------------------------------------
' Keyboard Module
'---------------------------------------------------------------------------------------------------------------

'------ Flags ------
Public Const KSF_SIGNAL_MAKE                    = &H1
Public Const KSF_SIGNAL_BREAK                   = &H2
Public Const KSF_IGNORE_KEY                     = &H10
Public Const KSF_MODIFY_KEY                     = &H20

'------ Key mask modifier ------
Public Const KSF_KEY_SHIFT                      = &H11
Public Const KSF_KEY_SHIFTR                     = &H1
Public Const KSF_KEY_SHIFTL                     = &H10
Public Const KSF_KEY_CTRL                       = &H22
Public Const KSF_KEY_CTRLR                      = &H2
Public Const KSF_KEY_CTRLL                      = &H20
Public Const KSF_KEY_ALT                        = &H44
Public Const KSF_KEY_ALTR                       = &H4
Public Const KSF_KEY_ALTL                       = &H40
Public Const KSF_KEY_WIN                        = &H88
Public Const KSF_KEY_WINR                       = &H8
Public Const KSF_KEY_WINL                       = &H80

'------ Key mask groups ------
Public Const KSF_KEY_ALPHA_NUM                  = &H100
Public Const KSF_KEY_NUM_PAD                    = &H200
Public Const KSF_KEY_CONTROL                    = &H400
Public Const KSF_KEY_FUNCTION                   = &H800

'------ Key mask combinations ------
Public Const KSF_KEY_CTRL_ALT_DEL               = &H10000
Public Const KSF_KEY_CTRL_ESC                   = &H20000
Public Const KSF_KEY_ALT_ESC                    = &H40000
Public Const KSF_KEY_ALT_TAB                    = &H80000
Public Const KSF_KEY_ALT_ENTER                  = &H100000
Public Const KSF_KEY_ALT_PRINT                  = &H200000
Public Const KSF_KEY_ALT_SPACE                  = &H400000
Public Const KSF_KEY_ESC                        = &H800000
Public Const KSF_KEY_TAB                        = &H1000000
Public Const KSF_KEY_ENTER                      = &H2000000
Public Const KSF_KEY_PRINT                      = &H4000000
Public Const KSF_KEY_SPACE                      = &H8000000
Public Const KSF_KEY_BACKSPACE                  = &H10000000
Public Const KSF_KEY_PAUSE                      = &H20000000
Public Const KSF_KEY_APP                        = &H40000000
Public Const KSF_KEY_ALL                        = &H7FFFFFFF

Public Const KSF_KEY_MODIFIER                   = KSF_KEY_CTRL Or KSF_KEY_ALT Or _
                                                  KSF_KEY_SHIFT Or KSF_KEY_WIN

Public Const KSF_KEY_SYSTEM                     = KSF_KEY_WIN Or KSF_KEY_CTRL_ALT_DEL Or _
                                                  KSF_KEY_CTRL_ESC Or KSF_KEY_ALT_ESC Or _
                                                  KSF_KEY_ALT_TAB

Public Const KSF_KEY_SYSTEM_DOS                 = KSF_KEY_SYSTEM Or KSF_KEY_ALT_ENTER Or _
                                                  KSF_KEY_ALT_PRINT Or KSF_KEY_ALT_SPACE Or _
                                                  KSF_KEY_PRINT

Public Const KSF_KEY_ALL_SINGLE                 = KSF_KEY_ALL And &H007f0000

'------ Keyboard events ------
Public Const KS_KEYBOARD_CONTEXT                = &H300

'------ Context structures ------
Public Type KeyboardUserContext
  ctxType As Long
  mask As Long
  modifier As Integer
  port As Integer
  state As Integer
  key As Integer
End Type

'------ Keyboard functions ------
Declare Function KS_createKeyHandler Lib "KrtsDemo.dll" ( _
  ByVal mask As Long, ByVal hSignal As Long, ByVal flags As Long) As Long
Declare Function KS_removeKeyHandler Lib "KrtsDemo.dll" ( _
  ByVal mask As Long) As Long
Declare Function KS_simulateKeyStroke Lib "KrtsDemo.dll" ( _
  ByRef pKeys As Long) As Long

'---------------------------------------------------------------------------------------------------------------
' Interrupt Module
'---------------------------------------------------------------------------------------------------------------

'------ Error Codes ------
Public Const KSERROR_IRQ_ALREADY_USED           = (KSERROR_CATEGORY_INTERRUPT+&H0)
Public Const KSERROR_CANNOT_INSTALL_HANDLER     = (KSERROR_CATEGORY_INTERRUPT+&H10000)
Public Const KSERROR_BAD_IRQ_INSTALLATION       = (KSERROR_CATEGORY_INTERRUPT+&H20000)
Public Const KSERROR_NO_HANDLER_INSTALLED       = (KSERROR_CATEGORY_INTERRUPT+&H30000)
Public Const KSERROR_NOBODY_IS_WAITING          = (KSERROR_CATEGORY_INTERRUPT+&H40000)
Public Const KSERROR_IRQ_DISABLED               = (KSERROR_CATEGORY_INTERRUPT+&H50000)
Public Const KSERROR_IRQ_ENABLED                = (KSERROR_CATEGORY_INTERRUPT+&H60000)
Public Const KSERROR_IRQ_IN_SERVICE             = (KSERROR_CATEGORY_INTERRUPT+&H70000)
Public Const KSERROR_IRQ_HANDLER_REMOVED        = (KSERROR_CATEGORY_INTERRUPT+&H80000)

'------ Flags ------
Public Const KSF_DONT_PASS_NEXT                 = &H20000
Public Const KSF_PCI_INTERRUPT                  = &H40000

'------ Interrupt events ------
Public Const KS_INTERRUPT_CONTEXT               = &H100

'------ Types & Structures ------
Public Type InterruptUserContext
  ctxType As Long
  hInterrupt As Long
  irq As Long
  consumed As Long
End Type

'------ Interrupt handling functions ------
Declare Function KS_createDeviceInterrupt Lib "KrtsDemo.dll" ( _
  ByRef phInterrupt As Long, ByRef pInfo As KSResourceInfoEx, ByVal hSignal As Long, ByVal flags As Long) As Long
Declare Function KS_createDeviceInterruptEx Lib "KrtsDemo.dll" ( _
  ByRef phInterrupt As Long, ByVal deviceName As String, ByVal hSignal As Long, ByVal flags As Long) As Long
Declare Function KS_createInterrupt Lib "KrtsDemo.dll" ( _
  ByRef phInterrupt As Long, ByVal irq As Long, ByVal hSignal As Long, ByVal flags As Long) As Long
Declare Function KS_removeInterrupt Lib "KrtsDemo.dll" ( _
  ByVal hInterrupt As Long) As Long
Declare Function KS_disableInterrupt Lib "KrtsDemo.dll" ( _
  ByVal hInterrupt As Long) As Long
Declare Function KS_enableInterrupt Lib "KrtsDemo.dll" ( _
  ByVal hInterrupt As Long) As Long
Declare Function KS_simulateInterrupt Lib "KrtsDemo.dll" ( _
  ByVal hInterrupt As Long) As Long
Declare Function KS_getInterruptState Lib "KrtsDemo.dll" ( _
  ByVal hInterrupt As Long, ByRef pState As HandlerState) As Long

'---------------------------------------------------------------------------------------------------------------
' UART Module
'---------------------------------------------------------------------------------------------------------------

'------ Flags ------
Public Const KSF_CHECK_LINE_ERROR               = &H8000
Public Const KSF_NO_FIFO                        = &H1000
Public Const KSF_USE_FIFO                       = &H40000
Public Const KSF_9_BIT_MODE                     = &H80000
Public Const KSF_TOGGLE_DTR                     = &H100000
Public Const KSF_TOGGLE_RTS                     = &H200000
Public Const KSF_TOGGLE_INVERSE                 = &H400000

'------ Uart commands ------
Public Const KS_CLR_DTR                         = &H4
Public Const KS_SET_DTR                         = &H5
Public Const KS_CLR_RTS                         = &H6
Public Const KS_SET_RTS                         = &H7
Public Const KS_CLR_BREAK                       = &H8
Public Const KS_SET_BREAK                       = &HA
Public Const KS_SKIP_NEXT                       = &HB
Public Const KS_CHANGE_LINE_CONTROL             = &HD
Public Const KS_SET_BAUD_RATE_MAX               = &HE
Public Const KS_ENABLE_FIFO                     = &HF
Public Const KS_DISABLE_FIFO                    = &H10
Public Const KS_ENABLE_INTERRUPTS               = &H11
Public Const KS_DISABLE_INTERRUPTS              = &H12
Public Const KS_SET_RECV_FIFO_SIZE              = &H13

'------ Line Errors ------
Public Const KS_UART_ERROR_PARITY               = &H1
Public Const KS_UART_ERROR_FRAME                = &H2
Public Const KS_UART_ERROR_OVERRUN              = &H4
Public Const KS_UART_ERROR_BREAK                = &H8
Public Const KS_UART_ERROR_FIFO                 = &H10

'------ UART events ------
Public Const KS_UART_RECV                       = &H0
Public Const KS_UART_RECV_ERROR                 = &H1
Public Const KS_UART_XMIT                       = &H2
Public Const KS_UART_XMIT_EMPTY                 = &H3
Public Const KS_UART_LSR_CHANGE                 = &H4
Public Const KS_UART_MSR_CHANGE                 = &H5
Public Const KS_UART_BREAK                      = &H6

'------ Types & Structures ------
Public Type UartUserContext
  ctxType As Long
  hUart As Long
  value As Long
  lineError As Long
End Type

Public Type UartRegisterUserContext
  ctxType As Long
  hUart As Long
  reg As Byte
End Type

Public Type KSUartProperties
  baudRate As Long
  parity As Byte
  dataBit As Byte
  stopBit As Byte
  reserved As Byte
End Type

Public Type KSUartState
  structSize As Long
  properties As KSUartProperties
  modemState As Byte
  lineState As Byte
  reserved As Integer
  xmitCount As Long
  xmitFree As Long
  recvCount As Long
  recvAvail As Long
  recvErrorCount As Long
  recvLostCount As Long
End Type

'------ Common functions ------
Declare Function KS_openUart Lib "KrtsDemo.dll" ( _
  ByRef phUart As Long, ByVal name As String, ByVal port As Long, ByRef pProperties As KSUartProperties, ByVal recvBufferLength As Long, ByVal xmitBufferLength As Long, ByVal flags As Long) As Long
Declare Function KS_openUartEx Lib "KrtsDemo.dll" ( _
  ByRef phUart As Long, ByVal hConnection As Long, ByVal port As Long, ByRef pProperties As KSUartProperties, ByVal recvBufferLength As Long, ByVal xmitBufferLength As Long, ByVal flags As Long) As Long
Declare Function KS_closeUart Lib "KrtsDemo.dll" ( _
  ByVal hUart As Long, ByVal flags As Long) As Long

'------ Communication functions ------
Declare Function KS_xmitUart Lib "KrtsDemo.dll" ( _
  ByVal hUart As Long, ByVal value As Long, ByVal flags As Long) As Long
Declare Function KS_recvUart Lib "KrtsDemo.dll" ( _
  ByVal hUart As Long, ByRef pValue As Long, ByRef pLineError As Long, ByVal flags As Long) As Long
Declare Function KS_xmitUartBlock Lib "KrtsDemo.dll" ( _
  ByVal hUart As Long, ByRef pBytes As Any, ByVal length As Long, ByRef pLength As Long, ByVal flags As Long) As Long
Declare Function KS_recvUartBlock Lib "KrtsDemo.dll" ( _
  ByVal hUart As Long, ByRef pBytes As Any, ByVal length As Long, ByRef pLength As Long, ByVal flags As Long) As Long
Declare Function KS_installUartHandler Lib "KrtsDemo.dll" ( _
  ByVal hUart As Long, ByVal eventCode As Long, ByVal hSignal As Long, ByVal flags As Long) As Long
Declare Function KS_getUartState Lib "KrtsDemo.dll" ( _
  ByVal hUart As Long, ByRef pUartState As KSUartState, ByVal flags As Long) As Long
Declare Function KS_execUartCommand Lib "KrtsDemo.dll" ( _
  ByVal hUart As Long, ByVal command As Long, ByRef pParam As Any, ByVal flags As Long) As Long

'---------------------------------------------------------------------------------------------------------------
' COMM Module
'---------------------------------------------------------------------------------------------------------------

'------ Line Errors ------
Public Const KS_COMM_ERROR_PARITY               = &H1
Public Const KS_COMM_ERROR_FRAME                = &H2
Public Const KS_COMM_ERROR_OVERRUN              = &H4
Public Const KS_COMM_ERROR_BREAK                = &H8
Public Const KS_COMM_ERROR_FIFO                 = &H10

'------ COMM events ------
Public Const KS_COMM_RECV                       = &H0
Public Const KS_COMM_RECV_ERROR                 = &H1
Public Const KS_COMM_XMIT                       = &H2
Public Const KS_COMM_XMIT_EMPTY                 = &H3
Public Const KS_COMM_LSR_CHANGE                 = &H4
Public Const KS_COMM_MSR_CHANGE                 = &H5
Public Const KS_COMM_BREAK                      = &H6

'------ Types & Structures ------
Public Type CommUserContext
  ctxType As Long
  hComm As Long
  value As Long
  lineError As Long
End Type

Public Type CommRegisterUserContext
  ctxType As Long
  hComm As Long
  reg As Byte
End Type

Public Type KSCommProperties
  baudRate As Long
  parity As Byte
  dataBit As Byte
  stopBit As Byte
  reserved As Byte
End Type

Public Type KSCommState
  structSize As Long
  properties As KSCommProperties
  modemState As Byte
  lineState As Byte
  reserved As Integer
  xmitCount As Long
  xmitFree As Long
  recvCount As Long
  recvAvail As Long
  recvErrorCount As Long
  recvLostCount As Long
End Type

'------ COMM functions ------
Declare Function KS_openComm Lib "KrtsDemo.dll" ( _
  ByRef phComm As Long, ByVal name As String, ByRef pProperties As KSCommProperties, ByVal recvBufferLength As Long, ByVal xmitBufferLength As Long, ByVal flags As Long) As Long
Declare Function KS_closeComm Lib "KrtsDemo.dll" ( _
  ByVal hComm As Long, ByVal flags As Long) As Long
Declare Function KS_xmitComm Lib "KrtsDemo.dll" ( _
  ByVal hComm As Long, ByVal value As Long, ByVal flags As Long) As Long
Declare Function KS_recvComm Lib "KrtsDemo.dll" ( _
  ByVal hComm As Long, ByRef pValue As Long, ByRef pLineError As Long, ByVal flags As Long) As Long
Declare Function KS_xmitCommBlock Lib "KrtsDemo.dll" ( _
  ByVal hComm As Long, ByRef pBytes As Any, ByVal length As Long, ByRef pLength As Long, ByVal flags As Long) As Long
Declare Function KS_recvCommBlock Lib "KrtsDemo.dll" ( _
  ByVal hComm As Long, ByRef pBytes As Any, ByVal length As Long, ByRef pLength As Long, ByVal flags As Long) As Long
Declare Function KS_installCommHandler Lib "KrtsDemo.dll" ( _
  ByVal hComm As Long, ByVal eventCode As Long, ByVal hSignal As Long, ByVal flags As Long) As Long
Declare Function KS_getCommState Lib "KrtsDemo.dll" ( _
  ByVal hComm As Long, ByRef pCommState As KSCommState, ByVal flags As Long) As Long
Declare Function KS_execCommCommand Lib "KrtsDemo.dll" ( _
  ByVal hComm As Long, ByVal command As Long, ByRef pParam As Any, ByVal flags As Long) As Long

'---------------------------------------------------------------------------------------------------------------
' RealTime Module
'---------------------------------------------------------------------------------------------------------------

'------ Error Codes ------
Public Const KSERROR_TIMER_NOT_INSTALLED        = (KSERROR_CATEGORY_TIMER+&H0)
Public Const KSERROR_TIMER_REMOVED              = (KSERROR_CATEGORY_TIMER+&H10000)
Public Const KSERROR_TIMER_DISABLED             = (KSERROR_CATEGORY_TIMER+&H20000)
Public Const KSERROR_TIMER_NOT_DISABLED         = (KSERROR_CATEGORY_TIMER+&H30000)
Public Const KSERROR_CANNOT_INSTALL_TIMER       = (KSERROR_CATEGORY_TIMER+&H40000)
Public Const KSERROR_NO_TIMER_AVAILABLE         = (KSERROR_CATEGORY_TIMER+&H50000)
Public Const KSERROR_WAIT_CANCELLED             = (KSERROR_CATEGORY_TIMER+&H60000)
Public Const KSERROR_TIMER_ALREADY_RUNNING      = (KSERROR_CATEGORY_TIMER+&H70000)
Public Const KSERROR_TIMER_ALREADY_STOPPED      = (KSERROR_CATEGORY_TIMER+&H80000)
Public Const KSERROR_CANNOT_START_TIMER         = (KSERROR_CATEGORY_TIMER+&H90000)

'------ Flags ------
Public Const KSF_DISTINCT_MODE                  = &H40000
Public Const KSF_DRIFT_CORRECTION               = &H10000
Public Const KSF_DISABLE_PROTECTION             = &H20000
Public Const KSF_USE_IEEE1588_TIME              = &H40000

'------ Timer module config ------
Public Const KSCONFIG_SOFTWARE_TIMER            = &H1

'------ Timer events ------
Public Const KS_TIMER_CONTEXT                   = &H200

'------ Context structures ------
Public Type TimerUserContext
  ctxType As Long
  hTimer As Long
  delay As Long
End Type

'------ Real-time functions ------
Declare Function KS_createTimer Lib "KrtsDemo.dll" ( _
  ByRef phTimer As Long, ByVal delay As Long, ByVal hSignal As Long, ByVal flags As Long) As Long
Declare Function KS_removeTimer Lib "KrtsDemo.dll" ( _
  ByVal hTimer As Long) As Long
Declare Function KS_cancelTimer Lib "KrtsDemo.dll" ( _
  ByVal hTimer As Long) As Long
Declare Function KS_startTimer Lib "KrtsDemo.dll" ( _
  ByVal hTimer As Long, ByVal flags As Long, ByVal delay As Long) As Long
Declare Function KS_stopTimer Lib "KrtsDemo.dll" ( _
  ByVal hTimer As Long) As Long
Declare Function KS_startTimerDelayed Lib "KrtsDemo.dll" ( _
  ByVal hTimer As Long, ByRef start As UInt64, ByVal period As Long, ByVal flags As Long) As Long
Declare Function KS_adjustTimer Lib "KrtsDemo.dll" ( _
  ByVal hTimer As Long, ByVal period As Long, ByVal flags As Long) As Long

'------ Obsolete timer functions - do not use! ------
Declare Function KS_disableTimer Lib "KrtsDemo.dll" ( _
  ByVal hTimer As Long) As Long
Declare Function KS_enableTimer Lib "KrtsDemo.dll" ( _
  ByVal hTimer As Long) As Long
Declare Function KS_getTimerState Lib "KrtsDemo.dll" ( _
  ByVal hTimer As Long, ByRef pState As HandlerState) As Long

'------ Obsolete timer resolution functions - do not use! ------
Declare Function KS_setTimerResolution Lib "KrtsDemo.dll" ( _
  ByVal resolution As Long, ByVal flags As Long) As Long
Declare Function KS_getTimerResolution Lib "KrtsDemo.dll" ( _
  ByRef pResolution As Long, ByVal flags As Long) As Long

'---------------------------------------------------------------------------------------------------------------
' LIN Module
'---------------------------------------------------------------------------------------------------------------

'------ LIN commands ------
Public Const KS_LIN_RESET                       = &H0
Public Const KS_LIN_SLEEP                       = &H1
Public Const KS_LIN_WAKEUP                      = &H2
Public Const KS_LIN_SET_BAUD_RATE               = &H4
Public Const KS_LIN_SET_VERSION                 = &H5
Public Const KS_LIN_DISABLE_QUEUE_ERROR         = &H6
Public Const KS_LIN_ENABLE_QUEUE_ERROR          = &H7

'------ LIN bit rates ------
Public Const KS_LIN_BAUD_19200                  = &H4B00
Public Const KS_LIN_BAUD_10417                  = &H28B1
Public Const KS_LIN_BAUD_9600                   = &H2580
Public Const KS_LIN_BAUD_4800                   = &H12C0
Public Const KS_LIN_BAUD_2400                   = &H960
Public Const KS_LIN_BAUD_1200                   = &H4B0

'------ LIN Errors ------
Public Const KS_LIN_ERROR_PHYSICAL              = &H1
Public Const KS_LIN_ERROR_TRANSPORT             = &H2
Public Const KS_LIN_ERROR_BUS_COLLISION         = &H3
Public Const KS_LIN_ERROR_PARITY                = &H4
Public Const KS_LIN_ERROR_CHECKSUM              = &H5
Public Const KS_LIN_ERROR_BREAK_EXPECTED        = &H6
Public Const KS_LIN_ERROR_RESPONSE_TIMEOUT      = &H7
Public Const KS_LIN_ERROR_PID_TIMEOUT           = &H9
Public Const KS_LIN_ERROR_RESPONSE_TOO_SHORT    = &H10
Public Const KS_LIN_ERROR_RECV_QUEUE_FULL       = &H11
Public Const KS_LIN_ERROR_RESPONSE_WITHOUT_HEADER 
                                                = &H12

'------ LIN events ------
Public Const KS_LIN_ERROR                       = &H0
Public Const KS_LIN_RECV_HEADER                 = &H1
Public Const KS_LIN_RECV_RESPONSE               = &H2

'------ LIN data structures ------
Public Type KSLinProperties
  structSize As Long
  linVersion As Long
  baudRate As Long
End Type

Public Type KSLinState
  structSize As Long
  properties As KSLinProperties
  xmitHdrCount As Long
  xmitRspCount As Long
  recvCount As Long
  recvAvail As Long
  recvErrorCount As Long
  recvNoRspCount As Long
  collisionCount As Long
End Type

Public Type KSLinHeader
  identifier As Long
  parity As Byte
  parityOk As Byte
End Type

Public Type KSLinResponse
  data(0 To 7) As Byte
  dataLen As Long
  checksum As Byte
  checksumOk As Byte
End Type

Public Type KSLinMsg
  header As KSLinHeader
  response As KSLinResponse
  timestamp As UInt64
End Type

'------ LIN Context structures ------
Public Type LinUserContext
  ctxType As Long
  hLin As Long
End Type

Public Type LinErrorUserContext
  ctxType As Long
  hLin As Long
  timestamp As UInt64
  errorCode As Long
End Type

Public Type LinHeaderUserContext
  ctxType As Long
  hLin As Long
  timestamp As UInt64
  header As KSLinHeader
End Type

Public Type LinResponseUserContext
  ctxType As Long
  hLin As Long
  timestamp As UInt64
  msg As KSLinMsg
End Type

'------ Common functions ------
Declare Function KS_openLin Lib "KrtsDemo.dll" ( _
  ByRef phLin As Long, ByVal pDeviceName As String, ByVal port As Long, ByRef pProperties As KSLinProperties, ByVal flags As Long) As Long
Declare Function KS_openLinEx Lib "KrtsDemo.dll" ( _
  ByRef phLin As Long, ByVal hLinDevice As Long, ByRef pProperties As KSLinProperties, ByVal flags As Long) As Long
Declare Function KS_closeLin Lib "KrtsDemo.dll" ( _
  ByVal hLin As Long, ByVal flags As Long) As Long
Declare Function KS_installLinHandler Lib "KrtsDemo.dll" ( _
  ByVal hLin As Long, ByVal eventCode As Long, ByVal hSignal As Long, ByVal flags As Long) As Long
Declare Function KS_execLinCommand Lib "KrtsDemo.dll" ( _
  ByVal hLin As Long, ByVal command As Long, ByRef pParam As Any, ByVal flags As Long) As Long
Declare Function KS_xmitLinHeader Lib "KrtsDemo.dll" ( _
  ByVal hLin As Long, ByVal identifier As Long, ByVal flags As Long) As Long
Declare Function KS_xmitLinResponse Lib "KrtsDemo.dll" ( _
  ByVal hLin As Long, ByRef pData As Any, ByVal size As Long, ByVal flags As Long) As Long
Declare Function KS_recvLinMsg Lib "KrtsDemo.dll" ( _
  ByVal hLin As Long, ByRef pLinMsg As KSLinMsg, ByVal flags As Long) As Long
Declare Function KS_getLinState Lib "KrtsDemo.dll" ( _
  ByVal hLin As Long, ByRef pLinState As KSLinState, ByVal flags As Long) As Long


