! Copyright (c) 2011-2016 by Kithara Software GmbH. All rights reserved.

!###############################################################################################################
!
! File:             KrtsDemo.f90 (v10.00d)
!
! Description:      Fortran 90 API for Kithara �RealTime Suite�
!
! REV   DATE        LOG    DESCRIPTION
! ----- ----------  ------ ------------------------------------------------------------------------------------
! u.jes 2011-02-02  CREAT: Original - (file created)
! ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:01
!
!###############################################################################################################

module KrtsDemo

!---------------------------------------------------------------------------------------------------------------
! Base Module
!---------------------------------------------------------------------------------------------------------------

!------ Common Constants ------

integer*4, parameter :: KS_INVALID_HANDLE                       = #00000000

!------ Error Categories ------

integer*4, parameter :: KSERROR_CATEGORY_OPERATING_SYSTEM       = #00000000
integer*4, parameter :: KSERROR_CATEGORY_USER                   = #0f000000
integer*4, parameter :: KSERROR_CATEGORY_BASE                   = #10000000
integer*4, parameter :: KSERROR_CATEGORY_DEAL                   = #11000000
integer*4, parameter :: KSERROR_CATEGORY_HANDLING               = #17000000
integer*4, parameter :: KSERROR_CATEGORY_IOPORT                 = #14000000
integer*4, parameter :: KSERROR_CATEGORY_MEMORY                 = #12000000
integer*4, parameter :: KSERROR_CATEGORY_KERNEL                 = #18000000
integer*4, parameter :: KSERROR_CATEGORY_KEYBOARD               = #1a000000
integer*4, parameter :: KSERROR_CATEGORY_INTERRUPT              = #13000000
integer*4, parameter :: KSERROR_CATEGORY_TIMER                  = #16000000
integer*4, parameter :: KSERROR_CATEGORY_USB                    = #19000000
integer*4, parameter :: KSERROR_CATEGORY_IDT                    = #1b000000
integer*4, parameter :: KSERROR_CATEGORY_NODE                   = #1c000000
integer*4, parameter :: KSERROR_CATEGORY_SOCKET                 = #1d000000
integer*4, parameter :: KSERROR_CATEGORY_SYSTEM                 = #1e000000
integer*4, parameter :: KSERROR_CATEGORY_ETHERCAT               = #1f000000
integer*4, parameter :: KSERROR_CATEGORY_MF                     = #20000000
integer*4, parameter :: KSERROR_CATEGORY_CAN                    = #21000000
integer*4, parameter :: KSERROR_CATEGORY_PROFIBUS               = #22000000
integer*4, parameter :: KSERROR_CATEGORY_PLC                    = #23000000
integer*4, parameter :: KSERROR_CATEGORY_CANOPEN                = #24000000
integer*4, parameter :: KSERROR_CATEGORY_FLEXRAY                = #25000000
integer*4, parameter :: KSERROR_CATEGORY_PACKET                 = #26000000
integer*4, parameter :: KSERROR_CATEGORY_CAMERA                 = #27000000
integer*4, parameter :: KSERROR_CATEGORY_TASK                   = #28000000
integer*4, parameter :: KSERROR_CATEGORY_SPECIAL                = #29000000
integer*4, parameter :: KSERROR_CATEGORY_XHCI                   = #2a000000
integer*4, parameter :: KSERROR_CATEGORY_DISK                   = #2b000000
integer*4, parameter :: KSERROR_CATEGORY_FILE                   = #2c000000
integer*4, parameter :: KSERROR_CATEGORY_NEXT                   = #2d000000
integer*4, parameter :: KSERROR_CATEGORY_EXCEPTION              = #3f000000

!------ Error Codes ------

integer*4, parameter :: KSERROR_OPERATING_SYSTEM                = (KSERROR_CATEGORY_BASE+#00000000)
integer*4, parameter :: KSERROR_UNKNOWN_ERROR_CODE              = (KSERROR_CATEGORY_BASE+#00010000)
integer*4, parameter :: KSERROR_UNKNOWN_ERROR_CATEGORY          = (KSERROR_CATEGORY_BASE+#00020000)
integer*4, parameter :: KSERROR_UNKNOWN_ERROR_LANGUAGE          = (KSERROR_CATEGORY_BASE+#00030000)
integer*4, parameter :: KSERROR_CANNOT_FIND_LIBRARY             = (KSERROR_CATEGORY_BASE+#00040000)
integer*4, parameter :: KSERROR_CANNOT_FIND_ADDRESS             = (KSERROR_CATEGORY_BASE+#00050000)
integer*4, parameter :: KSERROR_BAD_PARAM                       = (KSERROR_CATEGORY_BASE+#00060000)
integer*4, parameter :: KSERROR_BAD_VERSION                     = (KSERROR_CATEGORY_BASE+#00070000)
integer*4, parameter :: KSERROR_INTERNAL                        = (KSERROR_CATEGORY_BASE+#00080000)
integer*4, parameter :: KSERROR_UNKNOWN                         = (KSERROR_CATEGORY_BASE+#00090000)
integer*4, parameter :: KSERROR_FUNCTION_NOT_AVAILABLE          = (KSERROR_CATEGORY_BASE+#000a0000)
integer*4, parameter :: KSERROR_DRIVER_NOT_OPENED               = (KSERROR_CATEGORY_BASE+#000b0000)
integer*4, parameter :: KSERROR_NOT_ENOUGH_MEMORY               = (KSERROR_CATEGORY_BASE+#000c0000)
integer*4, parameter :: KSERROR_CANNOT_OPEN_KERNEL              = (KSERROR_CATEGORY_BASE+#000d0000)
integer*4, parameter :: KSERROR_CANNOT_CLOSE_KERNEL             = (KSERROR_CATEGORY_BASE+#000e0000)
integer*4, parameter :: KSERROR_BAD_KERNEL_VERSION              = (KSERROR_CATEGORY_BASE+#000f0000)
integer*4, parameter :: KSERROR_CANNOT_FIND_KERNEL              = (KSERROR_CATEGORY_BASE+#00100000)
integer*4, parameter :: KSERROR_INVALID_HANDLE                  = (KSERROR_CATEGORY_BASE+#00110000)
integer*4, parameter :: KSERROR_DEVICE_IO_FAILED                = (KSERROR_CATEGORY_BASE+#00120000)
integer*4, parameter :: KSERROR_REGISTRY_FAILURE                = (KSERROR_CATEGORY_BASE+#00130000)
integer*4, parameter :: KSERROR_CANNOT_START_KERNEL             = (KSERROR_CATEGORY_BASE+#00140000)
integer*4, parameter :: KSERROR_CANNOT_STOP_KERNEL              = (KSERROR_CATEGORY_BASE+#00150000)
integer*4, parameter :: KSERROR_ACCESS_DENIED                   = (KSERROR_CATEGORY_BASE+#00160000)
integer*4, parameter :: KSERROR_DEVICE_NOT_OPENED               = (KSERROR_CATEGORY_BASE+#00170000)
integer*4, parameter :: KSERROR_FUNCTION_DENIED                 = (KSERROR_CATEGORY_BASE+#00180000)
integer*4, parameter :: KSERROR_DEVICE_ALREADY_USED             = (KSERROR_CATEGORY_BASE+#00190000)
integer*4, parameter :: KSERROR_OUT_OF_MEMORY                   = (KSERROR_CATEGORY_BASE+#001a0000)
integer*4, parameter :: KSERROR_TOO_MANY_OPEN_PROCESSES         = (KSERROR_CATEGORY_BASE+#001b0000)
integer*4, parameter :: KSERROR_SIGNAL_OVERFLOW                 = (KSERROR_CATEGORY_BASE+#001c0000)
integer*4, parameter :: KSERROR_CANNOT_SIGNAL                   = (KSERROR_CATEGORY_BASE+#001d0000)
integer*4, parameter :: KSERROR_FILE_NOT_FOUND                  = (KSERROR_CATEGORY_BASE+#001e0000)
integer*4, parameter :: KSERROR_DEVICE_NOT_FOUND                = (KSERROR_CATEGORY_BASE+#001f0000)
integer*4, parameter :: KSERROR_ASSERTION_FAILED                = (KSERROR_CATEGORY_BASE+#00200000)
integer*4, parameter :: KSERROR_FUNCTION_FAILED                 = (KSERROR_CATEGORY_BASE+#00210000)
integer*4, parameter :: KSERROR_OPERATION_ABORTED               = (KSERROR_CATEGORY_BASE+#00220000)
integer*4, parameter :: KSERROR_TIMEOUT                         = (KSERROR_CATEGORY_BASE+#00230000)
integer*4, parameter :: KSERROR_REBOOT_NECESSARY                = (KSERROR_CATEGORY_BASE+#00240000)
integer*4, parameter :: KSERROR_DEVICE_DISABLED                 = (KSERROR_CATEGORY_BASE+#00250000)
integer*4, parameter :: KSERROR_ATTACH_FAILED                   = (KSERROR_CATEGORY_BASE+#00260000)
integer*4, parameter :: KSERROR_DEVICE_REMOVED                  = (KSERROR_CATEGORY_BASE+#00270000)
integer*4, parameter :: KSERROR_TYPE_MISMATCH                   = (KSERROR_CATEGORY_BASE+#00280000)
integer*4, parameter :: KSERROR_FUNCTION_NOT_IMPLEMENTED        = (KSERROR_CATEGORY_BASE+#00290000)
integer*4, parameter :: KSERROR_COMMUNICATION_BROKEN            = (KSERROR_CATEGORY_BASE+#002a0000)
integer*4, parameter :: KSERROR_DEVICE_NOT_READY                = (KSERROR_CATEGORY_BASE+#00370000)
integer*4, parameter :: KSERROR_NO_RESPONSE                     = (KSERROR_CATEGORY_BASE+#00380000)
integer*4, parameter :: KSERROR_OPERATION_PENDING               = (KSERROR_CATEGORY_BASE+#00390000)
integer*4, parameter :: KSERROR_PORT_BUSY                       = (KSERROR_CATEGORY_BASE+#003a0000)
integer*4, parameter :: KSERROR_UNKNOWN_SYSTEM                  = (KSERROR_CATEGORY_BASE+#003b0000)
integer*4, parameter :: KSERROR_BAD_CONTEXT                     = (KSERROR_CATEGORY_BASE+#003c0000)
integer*4, parameter :: KSERROR_END_OF_FILE                     = (KSERROR_CATEGORY_BASE+#003d0000)
integer*4, parameter :: KSERROR_INTERRUPT_ACTIVATION_FAILED     = (KSERROR_CATEGORY_BASE+#003e0000)
integer*4, parameter :: KSERROR_INTERRUPT_IS_SHARED             = (KSERROR_CATEGORY_BASE+#003f0000)
integer*4, parameter :: KSERROR_NO_REALTIME_ACCESS              = (KSERROR_CATEGORY_BASE+#00400000)
integer*4, parameter :: KSERROR_HARDWARE_NOT_SUPPORTED          = (KSERROR_CATEGORY_BASE+#00410000)
integer*4, parameter :: KSERROR_TIMER_ACTIVATION_FAILED         = (KSERROR_CATEGORY_BASE+#00420000)
integer*4, parameter :: KSERROR_SOCKET_FAILURE                  = (KSERROR_CATEGORY_BASE+#00430000)
integer*4, parameter :: KSERROR_LINE_ERROR                      = (KSERROR_CATEGORY_BASE+#00440000)
integer*4, parameter :: KSERROR_STACK_CORRUPTION                = (KSERROR_CATEGORY_BASE+#00450000)
integer*4, parameter :: KSERROR_INVALID_ARGUMENT                = (KSERROR_CATEGORY_BASE+#00460000)
integer*4, parameter :: KSERROR_PARSE_ERROR                     = (KSERROR_CATEGORY_BASE+#00470000)
integer*4, parameter :: KSERROR_INVALID_IDENTIFIER              = (KSERROR_CATEGORY_BASE+#00480000)
integer*4, parameter :: KSERROR_REALTIME_ALREADY_USED           = (KSERROR_CATEGORY_BASE+#00490000)
integer*4, parameter :: KSERROR_COMMUNICATION_FAILED            = (KSERROR_CATEGORY_BASE+#004a0000)
integer*4, parameter :: KSERROR_INVALID_SIZE                    = (KSERROR_CATEGORY_BASE+#004b0000)
integer*4, parameter :: KSERROR_OBJECT_NOT_FOUND                = (KSERROR_CATEGORY_BASE+#004c0000)

!------ Error codes of handling category ------

integer*4, parameter :: KSERROR_CANNOT_CREATE_EVENT             = (KSERROR_CATEGORY_HANDLING+#00000000)
integer*4, parameter :: KSERROR_CANNOT_CREATE_THREAD            = (KSERROR_CATEGORY_HANDLING+#00010000)
integer*4, parameter :: KSERROR_CANNOT_BLOCK_THREAD             = (KSERROR_CATEGORY_HANDLING+#00020000)
integer*4, parameter :: KSERROR_CANNOT_SIGNAL_EVENT             = (KSERROR_CATEGORY_HANDLING+#00030000)
integer*4, parameter :: KSERROR_CANNOT_POST_MESSAGE             = (KSERROR_CATEGORY_HANDLING+#00040000)
integer*4, parameter :: KSERROR_CANNOT_CREATE_SHARED            = (KSERROR_CATEGORY_HANDLING+#00050000)
integer*4, parameter :: KSERROR_SHARED_DIFFERENT_SIZE           = (KSERROR_CATEGORY_HANDLING+#00060000)
integer*4, parameter :: KSERROR_BAD_SHARED_MEM                  = (KSERROR_CATEGORY_HANDLING+#00070000)
integer*4, parameter :: KSERROR_WAIT_TIMEOUT                    = (KSERROR_CATEGORY_HANDLING+#00080000)
integer*4, parameter :: KSERROR_EVENT_NOT_SIGNALED              = (KSERROR_CATEGORY_HANDLING+#00090000)
integer*4, parameter :: KSERROR_CANNOT_CREATE_CALLBACK          = (KSERROR_CATEGORY_HANDLING+#000a0000)
integer*4, parameter :: KSERROR_NO_DATA_AVAILABLE               = (KSERROR_CATEGORY_HANDLING+#000b0000)
integer*4, parameter :: KSERROR_REQUEST_NESTED                  = (KSERROR_CATEGORY_HANDLING+#000c0000)

!------ Deal error codes ------

integer*4, parameter :: KSERROR_BAD_CUSTNUM                     = (KSERROR_CATEGORY_DEAL+#00030000)
integer*4, parameter :: KSERROR_KEY_BAD_FORMAT                  = (KSERROR_CATEGORY_DEAL+#00060000)
integer*4, parameter :: KSERROR_KEY_BAD_DATA                    = (KSERROR_CATEGORY_DEAL+#00080000)
integer*4, parameter :: KSERROR_KEY_PERMISSION_EXPIRED          = (KSERROR_CATEGORY_DEAL+#00090000)
integer*4, parameter :: KSERROR_BAD_LICENCE                     = (KSERROR_CATEGORY_DEAL+#000a0000)
integer*4, parameter :: KSERROR_CANNOT_LOAD_KEY                 = (KSERROR_CATEGORY_DEAL+#000c0000)
integer*4, parameter :: KSERROR_BAD_NAME_OR_FIRM                = (KSERROR_CATEGORY_DEAL+#000f0000)
integer*4, parameter :: KSERROR_NO_LICENCES_FOUND               = (KSERROR_CATEGORY_DEAL+#00100000)
integer*4, parameter :: KSERROR_LICENCE_CAPACITY_EXHAUSTED      = (KSERROR_CATEGORY_DEAL+#00110000)
integer*4, parameter :: KSERROR_FEATURE_NOT_LICENSED            = (KSERROR_CATEGORY_DEAL+#00120000)

!------ Exception Codes ------

integer*4, parameter :: KSERROR_DIVIDE_BY_ZERO                  = (KSERROR_CATEGORY_EXCEPTION+#00000000)
integer*4, parameter :: KSERROR_DEBUG_EXCEPTION                 = (KSERROR_CATEGORY_EXCEPTION+#00010000)
integer*4, parameter :: KSERROR_NONMASKABLE_INTERRUPT           = (KSERROR_CATEGORY_EXCEPTION+#00020000)
integer*4, parameter :: KSERROR_BREAKPOINT                      = (KSERROR_CATEGORY_EXCEPTION+#00030000)
integer*4, parameter :: KSERROR_OVERFLOW                        = (KSERROR_CATEGORY_EXCEPTION+#00040000)
integer*4, parameter :: KSERROR_BOUND_RANGE                     = (KSERROR_CATEGORY_EXCEPTION+#00050000)
integer*4, parameter :: KSERROR_INVALID_OPCODE                  = (KSERROR_CATEGORY_EXCEPTION+#00060000)
integer*4, parameter :: KSERROR_NO_MATH                         = (KSERROR_CATEGORY_EXCEPTION+#00070000)
integer*4, parameter :: KSERROR_DOUBLE_FAULT                    = (KSERROR_CATEGORY_EXCEPTION+#00080000)
integer*4, parameter :: KSERROR_COPROCESSOR_SEGMENT_OVERRUN     = (KSERROR_CATEGORY_EXCEPTION+#00090000)
integer*4, parameter :: KSERROR_INVALID_TSS                     = (KSERROR_CATEGORY_EXCEPTION+#000a0000)
integer*4, parameter :: KSERROR_SEGMENT_NOT_PRESENT             = (KSERROR_CATEGORY_EXCEPTION+#000b0000)
integer*4, parameter :: KSERROR_STACK_FAULT                     = (KSERROR_CATEGORY_EXCEPTION+#000c0000)
integer*4, parameter :: KSERROR_PROTECTION_FAULT                = (KSERROR_CATEGORY_EXCEPTION+#000d0000)
integer*4, parameter :: KSERROR_PAGE_FAULT                      = (KSERROR_CATEGORY_EXCEPTION+#000e0000)
integer*4, parameter :: KSERROR_FPU_FAULT                       = (KSERROR_CATEGORY_EXCEPTION+#00100000)
integer*4, parameter :: KSERROR_ALIGNMENT_CHECK                 = (KSERROR_CATEGORY_EXCEPTION+#00110000)
integer*4, parameter :: KSERROR_MACHINE_CHECK                   = (KSERROR_CATEGORY_EXCEPTION+#00120000)
integer*4, parameter :: KSERROR_SIMD_FAULT                      = (KSERROR_CATEGORY_EXCEPTION+#00130000)

!------ Flags ------

integer*4, parameter :: KSF_OPENED                              = #01000000
integer*4, parameter :: KSF_ACTIVE                              = #02000000
integer*4, parameter :: KSF_RUNNING                             = #02000000
integer*4, parameter :: KSF_FINISHED                            = #04000000
integer*4, parameter :: KSF_CANCELED                            = #08000000
integer*4, parameter :: KSF_DISABLED                            = #10000000
integer*4, parameter :: KSF_REQUESTED                           = #20000000
integer*4, parameter :: KSF_TERMINATE                           = #00000000
integer*4, parameter :: KSF_DONT_WAIT                           = #00000001
integer*4, parameter :: KSF_KERNEL_EXEC                         = #00000004
integer*4, parameter :: KSF_ASYNC_EXEC                          = #0000000c
integer*4, parameter :: KSF_DIRECT_EXEC                         = #0000000c
integer*4, parameter :: KSF_DONT_START                          = #00000010
integer*4, parameter :: KSF_ONE_SHOT                            = #00000020
integer*4, parameter :: KSF_SINGLE_SHOT                         = #00000020
integer*4, parameter :: KSF_SAVE_FPU                            = #00000040
integer*4, parameter :: KSF_USE_SSE                             = #00000001
integer*4, parameter :: KSF_USER_EXEC                           = #00000080
integer*4, parameter :: KSF_REALTIME_EXEC                       = #00000100
integer*4, parameter :: KSF_WAIT                                = #00000200
integer*4, parameter :: KSF_ACCEPT_INCOMPLETE                   = #00000200
integer*4, parameter :: KSF_REPORT_RESOURCE                     = #00000400
integer*4, parameter :: KSF_USE_TIMEOUTS                        = #00000400
integer*4, parameter :: KSF_FORCE_OVERRIDE                      = #00001000
integer*4, parameter :: KSF_OPTION_ON                           = #00001000
integer*4, parameter :: KSF_OPTION_OFF                          = #00002000
integer*4, parameter :: KSF_FORCE_OPEN                          = #00002000
integer*4, parameter :: KSF_UNLIMITED                           = #00004000
integer*4, parameter :: KSF_DONT_USE_SMP                        = #00004000
integer*4, parameter :: KSF_NO_CONTEXT                          = #00008000
integer*4, parameter :: KSF_HIGH_PRECISION                      = #00008000
integer*4, parameter :: KSF_CONTINUOUS                          = #00010000
integer*4, parameter :: KSF_ZERO_INIT                           = #00020000
integer*4, parameter :: KSF_TX_PDO                              = #00010000
integer*4, parameter :: KSF_RX_PDO                              = #00020000
integer*4, parameter :: KSF_READ                                = #00040000
integer*4, parameter :: KSF_WRITE                               = #00080000
integer*4, parameter :: KSF_USE_SMP                             = #00080000
integer*4, parameter :: KSF_ALTERNATIVE                         = #00200000
integer*4, parameter :: KSF_PDO                                 = #00040000
integer*4, parameter :: KSF_SDO                                 = #00080000
integer*4, parameter :: KSF_IDN                                 = #00100000
integer*4, parameter :: KSF_MANUAL_RESET                        = #00200000
integer*4, parameter :: KSF_RESET_STATE                         = #00800000
integer*4, parameter :: KSF_ISA_BUS                             = #00000001
integer*4, parameter :: KSF_PCI_BUS                             = #00000005
integer*4, parameter :: KSF_PRIO_NORMAL                         = #00000000
integer*4, parameter :: KSF_PRIO_LOW                            = #00000001
integer*4, parameter :: KSF_PRIO_LOWER                          = #00000002
integer*4, parameter :: KSF_PRIO_LOWEST                         = #00000003
integer*4, parameter :: KSF_MESSAGE_PIPE                        = #00001000
integer*4, parameter :: KSF_PIPE_NOTIFY_GET                     = #00002000
integer*4, parameter :: KSF_PIPE_NOTIFY_PUT                     = #00004000
integer*4, parameter :: KSF_ALL_CPUS                            = #00000000
integer*4, parameter :: KSF_READ_ACCESS                         = #00000001
integer*4, parameter :: KSF_WRITE_ACCESS                        = #00000002
integer*4, parameter :: KSF_SIZE_8_BIT                          = #00010000
integer*4, parameter :: KSF_SIZE_16_BIT                         = #00020000
integer*4, parameter :: KSF_SIZE_32_BIT                         = #00040000

!------ Obsolete flags - do not use! ------

integer*4, parameter :: KSF_ACCEPT_PENDING                      = #00000002
integer*4, parameter :: KSF_DONT_RAISE                          = #00200000

!------ Constant for returning no-error ------

integer*4, parameter :: KS_OK                                   = #00000000

!------ Language constants ------

integer*4, parameter :: KSLNG_DEFAULT                           = #00000000

!------ Context type values ------

integer*4, parameter :: USER_CONTEXT                            = #00000000
integer*4, parameter :: INTERRUPT_CONTEXT                       = #00000100
integer*4, parameter :: TIMER_CONTEXT                           = #00000200
integer*4, parameter :: KEYBOARD_CONTEXT                        = #00000300
integer*4, parameter :: USB_BASE                                = #00000506
integer*4, parameter :: PACKET_BASE                             = #00000700
integer*4, parameter :: DEVICE_BASE                             = #00000800
integer*4, parameter :: ETHERCAT_BASE                           = #00000900
integer*4, parameter :: SOCKET_BASE                             = #00000a00
integer*4, parameter :: TASK_BASE                               = #00000b00
integer*4, parameter :: MULTIFUNCTION_BASE                      = #00000c00
integer*4, parameter :: CAN_BASE                                = #00000d00
integer*4, parameter :: PROFIBUS_BASE                           = #00000e00
integer*4, parameter :: CANOPEN_BASE                            = #00000f00
integer*4, parameter :: FLEXRAY_BASE                            = #00001000
integer*4, parameter :: XHCI_BASE                               = #00001100
integer*4, parameter :: V86_BASE                                = #00001200
integer*4, parameter :: PLC_BASE                                = #00002000

!------ Config code for "Driver" ------

integer*4, parameter :: KSCONFIG_DRIVER_NAME                    = #00000001
integer*4, parameter :: KSCONFIG_FUNCTION_LOGGING               = #00000002

!------ Config code for "Base" ------

integer*4, parameter :: KSCONFIG_DISABLE_MESSAGE_LOGGING        = #00000001
integer*4, parameter :: KSCONFIG_ENABLE_MESSAGE_LOGGING         = #00000002
integer*4, parameter :: KSCONFIG_FLUSH_MESSAGE_PIPE             = #00000003

!------ Config code for "Debug" ------


!------ Commonly used commands ------

integer*4, parameter :: KS_ABORT_XMIT_CMD                       = #00000001
integer*4, parameter :: KS_ABORT_RECV_CMD                       = #00000002
integer*4, parameter :: KS_FLUSH_XMIT_BUF                       = #00000003
integer*4, parameter :: KS_FLUSH_RECV_BUF                       = #00000009
integer*4, parameter :: KS_SET_BAUD_RATE                        = #0000000c

!------ Pipe contexts ------

integer*4, parameter :: PIPE_PUT_CONTEXT                        = (USER_CONTEXT+#00000040)
integer*4, parameter :: PIPE_GET_CONTEXT                        = (USER_CONTEXT+#00000041)

!------ Quick mutex levels ------

integer*4, parameter :: KS_APP_LEVEL                            = #00000000
integer*4, parameter :: KS_DPC_LEVEL                            = #00000001
integer*4, parameter :: KS_ISR_LEVEL                            = #00000002
integer*4, parameter :: KS_RTX_LEVEL                            = #00000003
integer*4, parameter :: KS_CPU_LEVEL                            = #00000004

!------ Data types ------

integer*4, parameter :: KS_DATATYPE_UNKNOWN                     = #00000000
integer*4, parameter :: KS_DATATYPE_BOOLEAN                     = #00000001
integer*4, parameter :: KS_DATATYPE_INTEGER8                    = #00000002
integer*4, parameter :: KS_DATATYPE_UINTEGER8                   = #00000005
integer*4, parameter :: KS_DATATYPE_INTEGER16                   = #00000003
integer*4, parameter :: KS_DATATYPE_UINTEGER16                  = #00000006
integer*4, parameter :: KS_DATATYPE_INTEGER32                   = #00000004
integer*4, parameter :: KS_DATATYPE_UINTEGER32                  = #00000007
integer*4, parameter :: KS_DATATYPE_INTEGER64                   = #00000015
integer*4, parameter :: KS_DATATYPE_UNSIGNED64                  = #0000001b
integer*4, parameter :: KS_DATATYPE_REAL32                      = #00000008
integer*4, parameter :: KS_DATATYPE_REAL64                      = #00000011
integer*4, parameter :: KS_DATATYPE_TINY                        = #00000002
integer*4, parameter :: KS_DATATYPE_BYTE                        = #00000005
integer*4, parameter :: KS_DATATYPE_SHORT                       = #00000003
integer*4, parameter :: KS_DATATYPE_USHORT                      = #00000006
integer*4, parameter :: KS_DATATYPE_INT                         = #00000004
integer*4, parameter :: KS_DATATYPE_UINT                        = #00000007
integer*4, parameter :: KS_DATATYPE_LLONG                       = #00000015
integer*4, parameter :: KS_DATATYPE_ULONG                       = #0000001b
integer*4, parameter :: KS_DATATYPE_VISIBLE_STRING              = #00000009
integer*4, parameter :: KS_DATATYPE_OCTET_STRING                = #0000000a
integer*4, parameter :: KS_DATATYPE_UNICODE_STRING              = #0000000b
integer*4, parameter :: KS_DATATYPE_TIME_OF_DAY                 = #0000000c
integer*4, parameter :: KS_DATATYPE_TIME_DIFFERENCE             = #0000000d
integer*4, parameter :: KS_DATATYPE_DOMAIN                      = #0000000f
integer*4, parameter :: KS_DATATYPE_TIME_OF_DAY32               = #0000001c
integer*4, parameter :: KS_DATATYPE_DATE32                      = #0000001d
integer*4, parameter :: KS_DATATYPE_DATE_AND_TIME64             = #0000001e
integer*4, parameter :: KS_DATATYPE_TIME_DIFFERENCE64           = #0000001f
integer*4, parameter :: KS_DATATYPE_BIT1                        = #00000030
integer*4, parameter :: KS_DATATYPE_BIT2                        = #00000031
integer*4, parameter :: KS_DATATYPE_BIT3                        = #00000032
integer*4, parameter :: KS_DATATYPE_BIT4                        = #00000033
integer*4, parameter :: KS_DATATYPE_BIT5                        = #00000034
integer*4, parameter :: KS_DATATYPE_BIT6                        = #00000035
integer*4, parameter :: KS_DATATYPE_BIT7                        = #00000036
integer*4, parameter :: KS_DATATYPE_BIT8                        = #00000037
integer*4, parameter :: KS_DATATYPE_INTEGER24                   = #00000010
integer*4, parameter :: KS_DATATYPE_UNSIGNED24                  = #00000016
integer*4, parameter :: KS_DATATYPE_INTEGER40                   = #00000012
integer*4, parameter :: KS_DATATYPE_UNSIGNED40                  = #00000018
integer*4, parameter :: KS_DATATYPE_INTEGER48                   = #00000013
integer*4, parameter :: KS_DATATYPE_UNSIGNED48                  = #00000019
integer*4, parameter :: KS_DATATYPE_INTEGER56                   = #00000014
integer*4, parameter :: KS_DATATYPE_UNSIGNED56                  = #0000001a
integer*4, parameter :: KS_DATATYPE_KSHANDLE                    = #00000040

!------ DataObj type flags ------

integer*4, parameter :: KS_DATAOBJ_READABLE                     = #00010000
integer*4, parameter :: KS_DATAOBJ_WRITEABLE                    = #00020000
integer*4, parameter :: KS_DATAOBJ_ARRAY                        = #01000000
integer*4, parameter :: KS_DATAOBJ_ENUM                         = #02000000
integer*4, parameter :: KS_DATAOBJ_STRUCT                       = #04000000

!------ Common types and structures ------

type HandlerState
  integer*4 requested
  integer*4 performed
  integer*4 state
  integer*4 error
end type

type KSTimeOut
  integer*4 baseTime
  integer*4 charTime
end type

type KSDeviceInfo
  integer*4 structSize
  byte pDeviceName(0:79)
  byte pDeviceDesc(0:79)
  byte pDeviceVendor(0:79)
  byte pFriendlyName(0:79)
  byte pHardwareId(0:79)
  byte pClassName(0:79)
  byte pInfPath(0:79)
  byte pDriverDesc(0:79)
  byte pDriverVendor(0:79)
  byte pDriverDate(0:79)
  byte pDriverVersion(0:79)
  byte pServiceName(0:79)
  byte pServiceDisp(0:79)
  byte pServiceDesc(0:79)
  byte pServicePath(0:79)
end type

type KSSystemInformation
  integer*4 structSize
  integer*4 numberOfCPUs
  integer*4 numberOfSharedCPUs
  integer*4 kiloBytesOfRAM
  byte isDll64Bit
  byte isSys64Bit
  byte isStableVersion
  byte isLogVersion
  integer*4 dllVersion
  integer*4 dllBuildNumber
  integer*4 sysVersion
  integer*4 sysBuildNumber
  integer*4 systemVersion
  byte pSystemName(0:63)
  byte pServicePack(0:63)
  byte pDllPath(0:255)
  byte pSysPath(0:255)
end type

type KSProcessorInformation
  integer*4 structSize
  integer*4 index
  integer*4 group
  integer*4 number
  integer*4 currentTemperature
  integer*4 allowedTemperature
end type

type KSSharedMemInfo
  integer*4 structSize
  integer*4 hHandle
  byte pName(0:255)
  integer*4 size
  integer*4 pThisPtr
end type

!------ Context structures ------

type PipeUserContext
  integer*4 ctxType
  integer*4 hPipe
end type

!------ Common functions ------

interface

integer*4 function KS_openDriver(customerNumber)
  !dec$ attributes stdcall, alias : '_KS_openDriver@4' :: KS_openDriver
  integer*4 customerNumber
end function

integer*4 function KS_closeDriver()
  !dec$ attributes stdcall, alias : '_KS_closeDriver@0' :: KS_closeDriver
end function

integer*4 function KS_getDriverVersion(pVersion)
  !dec$ attributes stdcall, alias : '_KS_getDriverVersion@4' :: KS_getDriverVersion
  integer*4 pVersion
end function

integer*4 function KS_getErrorString(code, msg, language)
  !dec$ attributes stdcall, alias : '_KS_getErrorString@12' :: KS_getErrorString
  integer*4 code
  integer*4 msg
  integer*4 language
end function

integer*4 function KS_addErrorString(code, msg, language)
  !dec$ attributes stdcall, alias : '_KS_addErrorString@12' :: KS_addErrorString
  integer*4 code
  integer*4 msg
  integer*4 language
end function

integer*4 function KS_getDriverConfig(module, configType, pData)
  !dec$ attributes stdcall, alias : '_KS_getDriverConfig@12' :: KS_getDriverConfig
  integer*4 module
  integer*4 configType
  integer*4 pData
end function

integer*4 function KS_setDriverConfig(module, configType, pData)
  !dec$ attributes stdcall, alias : '_KS_setDriverConfig@12' :: KS_setDriverConfig
  integer*4 module
  integer*4 configType
  integer*4 pData
end function

integer*4 function KS_getSystemInformation(pSystemInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_getSystemInformation@8' :: KS_getSystemInformation
  integer*4 pSystemInfo
  integer*4 flags
end function

integer*4 function KS_getProcessorInformation(index, pProcessorInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_getProcessorInformation@12' :: KS_getProcessorInformation
  integer*4 index
  integer*4 pProcessorInfo
  integer*4 flags
end function

integer*4 function KS_closeHandle(handle, flags)
  !dec$ attributes stdcall, alias : '_KS_closeHandle@8' :: KS_closeHandle
  integer*4 handle
  integer*4 flags
end function

end interface

!------ Debugging & test ------

interface

integer*4 function KS_showMessage(ksError, msg)
  !dec$ attributes stdcall, alias : '_KS_showMessage@8' :: KS_showMessage
  integer*4 ksError
  integer*4 msg
end function

integer*4 function KS_logMessage(msgType, msgText)
  !dec$ attributes stdcall, alias : '_KS_logMessage@8' :: KS_logMessage
  integer*4 msgType
  integer*4 msgText
end function

integer*4 function KS_bufMessage(msgType, pBuffer, length, msgText)
  !dec$ attributes stdcall, alias : '_KS_bufMessage@16' :: KS_bufMessage
  integer*4 msgType
  integer*4 pBuffer
  integer*4 length
  integer*4 msgText
end function

integer*4 function KS_dbgMessage(fileName, line, msgText)
  !dec$ attributes stdcall, alias : '_KS_dbgMessage@12' :: KS_dbgMessage
  integer*4 fileName
  integer*4 line
  integer*4 msgText
end function

integer*4 function KS_beep(freq, duration)
  !dec$ attributes stdcall, alias : '_KS_beep@8' :: KS_beep
  integer*4 freq
  integer*4 duration
end function

integer*4 function KS_throwException(error, pText, pFile, line)
  !dec$ attributes stdcall, alias : '_KS_throwException@16' :: KS_throwException
  integer*4 error
  integer*4 pText
  integer*4 pFile
  integer*4 line
end function

end interface

!------ Device handling ------

interface

integer*4 function KS_enumDevices(deviceType, index, pDeviceNameBuf, flags)
  !dec$ attributes stdcall, alias : '_KS_enumDevices@16' :: KS_enumDevices
  integer*4 deviceType
  integer*4 index
  integer*4 pDeviceNameBuf
  integer*4 flags
end function

integer*4 function KS_enumDevicesEx(className, busType, hEnumerator, index, pDeviceNameBuf, flags)
  !dec$ attributes stdcall, alias : '_KS_enumDevicesEx@24' :: KS_enumDevicesEx
  integer*4 className
  integer*4 busType
  integer*4 hEnumerator
  integer*4 index
  integer*4 pDeviceNameBuf
  integer*4 flags
end function

integer*4 function KS_getDevices(className, busType, hEnumerator, pCount, pBuf, size, flags)
  !dec$ attributes stdcall, alias : '_KS_getDevices@28' :: KS_getDevices
  integer*4 className
  integer*4 busType
  integer*4 hEnumerator
  integer*4 pCount
  integer*4 pBuf
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_getDeviceInfo(deviceName, pDeviceInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_getDeviceInfo@12' :: KS_getDeviceInfo
  integer*4 deviceName
  integer*4 pDeviceInfo
  integer*4 flags
end function

integer*4 function KS_updateDriver(deviceName, fileName, flags)
  !dec$ attributes stdcall, alias : '_KS_updateDriver@12' :: KS_updateDriver
  integer*4 deviceName
  integer*4 fileName
  integer*4 flags
end function

end interface

!------ Threads & priorities ------

interface

integer*4 function KS_createThread(procName, pArgs, phThread)
  !dec$ attributes stdcall, alias : '_KS_createThread@12' :: KS_createThread
  integer*4 procName
  integer*4 pArgs
  integer*4 phThread
end function

integer*4 function KS_removeThread()
  !dec$ attributes stdcall, alias : '_KS_removeThread@0' :: KS_removeThread
end function

integer*4 function KS_setThreadPrio(prio)
  !dec$ attributes stdcall, alias : '_KS_setThreadPrio@4' :: KS_setThreadPrio
  integer*4 prio
end function

integer*4 function KS_getThreadPrio(pPrio)
  !dec$ attributes stdcall, alias : '_KS_getThreadPrio@4' :: KS_getThreadPrio
  integer*4 pPrio
end function

end interface

!------ Shared memory ------

interface

integer*4 function KS_createSharedMem(ppAppPtr, ppSysPtr, name, size, flags)
  !dec$ attributes stdcall, alias : '_KS_createSharedMem@20' :: KS_createSharedMem
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 name
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_freeSharedMem(pAppPtr)
  !dec$ attributes stdcall, alias : '_KS_freeSharedMem@4' :: KS_freeSharedMem
  integer*4 pAppPtr
end function

integer*4 function KS_getSharedMem(ppPtr, name)
  !dec$ attributes stdcall, alias : '_KS_getSharedMem@8' :: KS_getSharedMem
  integer*4 ppPtr
  integer*4 name
end function

integer*4 function KS_readMem(pBuffer, pAppPtr, size, offset)
  !dec$ attributes stdcall, alias : '_KS_readMem@16' :: KS_readMem
  integer*4 pBuffer
  integer*4 pAppPtr
  integer*4 size
  integer*4 offset
end function

integer*4 function KS_writeMem(pBuffer, pAppPtr, size, offset)
  !dec$ attributes stdcall, alias : '_KS_writeMem@16' :: KS_writeMem
  integer*4 pBuffer
  integer*4 pAppPtr
  integer*4 size
  integer*4 offset
end function

integer*4 function KS_createSharedMemEx(phHandle, name, size, flags)
  !dec$ attributes stdcall, alias : '_KS_createSharedMemEx@16' :: KS_createSharedMemEx
  integer*4 phHandle
  integer*4 name
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_freeSharedMemEx(hHandle, flags)
  !dec$ attributes stdcall, alias : '_KS_freeSharedMemEx@8' :: KS_freeSharedMemEx
  integer*4 hHandle
  integer*4 flags
end function

integer*4 function KS_getSharedMemEx(hHandle, pPtr, flags)
  !dec$ attributes stdcall, alias : '_KS_getSharedMemEx@12' :: KS_getSharedMemEx
  integer*4 hHandle
  integer*4 pPtr
  integer*4 flags
end function

end interface

!------ Events ------

interface

integer*4 function KS_createEvent(phEvent, name, flags)
  !dec$ attributes stdcall, alias : '_KS_createEvent@12' :: KS_createEvent
  integer*4 phEvent
  integer*4 name
  integer*4 flags
end function

integer*4 function KS_closeEvent(hEvent)
  !dec$ attributes stdcall, alias : '_KS_closeEvent@4' :: KS_closeEvent
  integer*4 hEvent
end function

integer*4 function KS_setEvent(hEvent)
  !dec$ attributes stdcall, alias : '_KS_setEvent@4' :: KS_setEvent
  integer*4 hEvent
end function

integer*4 function KS_resetEvent(hEvent)
  !dec$ attributes stdcall, alias : '_KS_resetEvent@4' :: KS_resetEvent
  integer*4 hEvent
end function

integer*4 function KS_pulseEvent(hEvent)
  !dec$ attributes stdcall, alias : '_KS_pulseEvent@4' :: KS_pulseEvent
  integer*4 hEvent
end function

integer*4 function KS_waitForEvent(hEvent, flags, timeout)
  !dec$ attributes stdcall, alias : '_KS_waitForEvent@12' :: KS_waitForEvent
  integer*4 hEvent
  integer*4 flags
  integer*4 timeout
end function

integer*4 function KS_getEventState(hEvent, pState)
  !dec$ attributes stdcall, alias : '_KS_getEventState@8' :: KS_getEventState
  integer*4 hEvent
  integer*4 pState
end function

integer*4 function KS_postMessage(hWnd, msg, wP, lP)
  !dec$ attributes stdcall, alias : '_KS_postMessage@16' :: KS_postMessage
  integer*4 hWnd
  integer*4 msg
  integer*4 wP
  integer*4 lP
end function

end interface

!------ CallBacks ------

interface

integer*4 function KS_createCallBack(phCallBack, routine, pArgs, flags, prio)
  !dec$ attributes stdcall, alias : '_KS_createCallBack@20' :: KS_createCallBack
  integer*4 phCallBack
  integer*4 routine
  integer*4 pArgs
  integer*4 flags
  integer*4 prio
end function

integer*4 function KS_removeCallBack(hCallBack)
  !dec$ attributes stdcall, alias : '_KS_removeCallBack@4' :: KS_removeCallBack
  integer*4 hCallBack
end function

integer*4 function KS_execCallBack(hCallBack, pContext, flags)
  !dec$ attributes stdcall, alias : '_KS_execCallBack@12' :: KS_execCallBack
  integer*4 hCallBack
  integer*4 pContext
  integer*4 flags
end function

integer*4 function KS_getCallState(hSignal, pState)
  !dec$ attributes stdcall, alias : '_KS_getCallState@8' :: KS_getCallState
  integer*4 hSignal
  integer*4 pState
end function

integer*4 function KS_signalObject(hObject, pContext, flags)
  !dec$ attributes stdcall, alias : '_KS_signalObject@12' :: KS_signalObject
  integer*4 hObject
  integer*4 pContext
  integer*4 flags
end function

end interface

!------ Pipes ------

interface

integer*4 function KS_createPipe(phPipe, name, itemSize, itemCount, hNotify, flags)
  !dec$ attributes stdcall, alias : '_KS_createPipe@24' :: KS_createPipe
  integer*4 phPipe
  integer*4 name
  integer*4 itemSize
  integer*4 itemCount
  integer*4 hNotify
  integer*4 flags
end function

integer*4 function KS_removePipe(hPipe)
  !dec$ attributes stdcall, alias : '_KS_removePipe@4' :: KS_removePipe
  integer*4 hPipe
end function

integer*4 function KS_flushPipe(hPipe, flags)
  !dec$ attributes stdcall, alias : '_KS_flushPipe@8' :: KS_flushPipe
  integer*4 hPipe
  integer*4 flags
end function

integer*4 function KS_getPipe(hPipe, pBuffer, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_getPipe@20' :: KS_getPipe
  integer*4 hPipe
  integer*4 pBuffer
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_putPipe(hPipe, pBuffer, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_putPipe@20' :: KS_putPipe
  integer*4 hPipe
  integer*4 pBuffer
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

end interface

!------ Quick mutexes ------

interface

integer*4 function KS_createQuickMutex(phMutex, level, flags)
  !dec$ attributes stdcall, alias : '_KS_createQuickMutex@12' :: KS_createQuickMutex
  integer*4 phMutex
  integer*4 level
  integer*4 flags
end function

integer*4 function KS_removeQuickMutex(hMutex)
  !dec$ attributes stdcall, alias : '_KS_removeQuickMutex@4' :: KS_removeQuickMutex
  integer*4 hMutex
end function

integer*4 function KS_requestQuickMutex(hMutex)
  !dec$ attributes stdcall, alias : '_KS_requestQuickMutex@4' :: KS_requestQuickMutex
  integer*4 hMutex
end function

integer*4 function KS_releaseQuickMutex(hMutex)
  !dec$ attributes stdcall, alias : '_KS_releaseQuickMutex@4' :: KS_releaseQuickMutex
  integer*4 hMutex
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Kernel Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_RTX_TOO_MANY_FUNCTIONS          = (KSERROR_CATEGORY_KERNEL+#00000000)
integer*4, parameter :: KSERROR_CANNOT_EXECUTE_RTX_CALL         = (KSERROR_CATEGORY_KERNEL+#00010000)
integer*4, parameter :: KSERROR_CANNOT_PREPARE_RTX              = (KSERROR_CATEGORY_KERNEL+#00020000)
integer*4, parameter :: KSERROR_RTX_BAD_SYSTEM_CALL             = (KSERROR_CATEGORY_KERNEL+#00030000)
integer*4, parameter :: KSERROR_CANNOT_LOAD_KERNEL              = (KSERROR_CATEGORY_KERNEL+#00040000)
integer*4, parameter :: KSERROR_CANNOT_RELOCATE_KERNEL          = (KSERROR_CATEGORY_KERNEL+#00050000)
integer*4, parameter :: KSERROR_KERNEL_ALREADY_LOADED           = (KSERROR_CATEGORY_KERNEL+#00060000)
integer*4, parameter :: KSERROR_KERNEL_NOT_LOADED               = (KSERROR_CATEGORY_KERNEL+#00070000)

!------ Flags ------

integer*4, parameter :: KSF_ANALYSE_ONLY                        = #00010000
integer*4, parameter :: KSF_FUNC_TABLE_GIVEN                    = #00020000
integer*4, parameter :: KSF_FORCE_RELOC                         = #00040000
integer*4, parameter :: KSF_LOAD_DEPENDENCIES                   = #00001000
integer*4, parameter :: KSF_EXEC_ENTRY_POINT                    = #00002000

!------ Kernel commands ------

integer*4, parameter :: KS_GET_KERNEL_BASE_ADDRESS              = #00000001

!------ Relocation by byte-wise instruction mapping ------

interface

integer*4 function KS_prepareKernelExec(callback, pRelocated, flags)
  !dec$ attributes stdcall, alias : '_KS_prepareKernelExec@12' :: KS_prepareKernelExec
  integer*4 callback
  integer*4 pRelocated
  integer*4 flags
end function

integer*4 function KS_endKernelExec(relocated)
  !dec$ attributes stdcall, alias : '_KS_endKernelExec@4' :: KS_endKernelExec
  integer*4 relocated
end function

integer*4 function KS_testKernelExec(relocated, pArgs)
  !dec$ attributes stdcall, alias : '_KS_testKernelExec@8' :: KS_testKernelExec
  integer*4 relocated
  integer*4 pArgs
end function

end interface

!------ Relocation by loading DLL into kernel-space ------

interface

integer*4 function KS_registerKernelAddress(name, appFunction, sysFunction, flags)
  !dec$ attributes stdcall, alias : '_KS_registerKernelAddress@16' :: KS_registerKernelAddress
  integer*4 name
  integer*4 appFunction
  integer*4 sysFunction
  integer*4 flags
end function

integer*4 function KS_loadKernel(phKernel, dllName, initProcName, pArgs, flags)
  !dec$ attributes stdcall, alias : '_KS_loadKernel@20' :: KS_loadKernel
  integer*4 phKernel
  integer*4 dllName
  integer*4 initProcName
  integer*4 pArgs
  integer*4 flags
end function

integer*4 function KS_loadKernelFromBuffer(phKernel, dllName, pBuffer, length, initProcName, pArgs, flags)
  !dec$ attributes stdcall, alias : '_KS_loadKernelFromBuffer@28' :: KS_loadKernelFromBuffer
  integer*4 phKernel
  integer*4 dllName
  integer*4 pBuffer
  integer*4 length
  integer*4 initProcName
  integer*4 pArgs
  integer*4 flags
end function

integer*4 function KS_freeKernel(hKernel)
  !dec$ attributes stdcall, alias : '_KS_freeKernel@4' :: KS_freeKernel
  integer*4 hKernel
end function

integer*4 function KS_execKernelFunction(hKernel, name, pArgs, pContext, flags)
  !dec$ attributes stdcall, alias : '_KS_execKernelFunction@20' :: KS_execKernelFunction
  integer*4 hKernel
  integer*4 name
  integer*4 pArgs
  integer*4 pContext
  integer*4 flags
end function

integer*4 function KS_getKernelFunction(hKernel, name, pRelocated, flags)
  !dec$ attributes stdcall, alias : '_KS_getKernelFunction@16' :: KS_getKernelFunction
  integer*4 hKernel
  integer*4 name
  integer*4 pRelocated
  integer*4 flags
end function

integer*4 function KS_enumKernelFunctions(hKernel, index, pName, flags)
  !dec$ attributes stdcall, alias : '_KS_enumKernelFunctions@16' :: KS_enumKernelFunctions
  integer*4 hKernel
  integer*4 index
  integer*4 pName
  integer*4 flags
end function

integer*4 function KS_execKernelCommand(hKernel, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execKernelCommand@16' :: KS_execKernelCommand
  integer*4 hKernel
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

integer*4 function KS_createKernelCallBack(phCallBack, hKernel, name, pArgs, flags, prio)
  !dec$ attributes stdcall, alias : '_KS_createKernelCallBack@24' :: KS_createKernelCallBack
  integer*4 phCallBack
  integer*4 hKernel
  integer*4 name
  integer*4 pArgs
  integer*4 flags
  integer*4 prio
end function

end interface

!------ Helper functions ------

interface

integer*4 function KS_execSyncFunction(hHandler, proc, pArgs, flags)
  !dec$ attributes stdcall, alias : '_KS_execSyncFunction@16' :: KS_execSyncFunction
  integer*4 hHandler
  integer*4 proc
  integer*4 pArgs
  integer*4 flags
end function

integer*4 function KS_execSystemCall(fileName, funcName, pData, flags)
  !dec$ attributes stdcall, alias : '_KS_execSystemCall@16' :: KS_execSystemCall
  integer*4 fileName
  integer*4 funcName
  integer*4 pData
  integer*4 flags
end function

end interface

!------ Memory functions ------

interface

integer*4 function KS_memCpy(pDst, pSrc, size, flags)
  !dec$ attributes stdcall, alias : '_KS_memCpy@16' :: KS_memCpy
  integer*4 pDst
  integer*4 pSrc
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_memMove(pDst, pSrc, size, flags)
  !dec$ attributes stdcall, alias : '_KS_memMove@16' :: KS_memMove
  integer*4 pDst
  integer*4 pSrc
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_memSet(pMem, value, size, flags)
  !dec$ attributes stdcall, alias : '_KS_memSet@16' :: KS_memSet
  integer*4 pMem
  integer*4 value
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_malloc(ppAllocated, bytes, flags)
  !dec$ attributes stdcall, alias : '_KS_malloc@12' :: KS_malloc
  integer*4 ppAllocated
  integer*4 bytes
  integer*4 flags
end function

integer*4 function KS_free(pAllocated, flags)
  !dec$ attributes stdcall, alias : '_KS_free@8' :: KS_free
  integer*4 pAllocated
  integer*4 flags
end function

end interface

!------ Interlocked functions ------

interface

integer*4 function KS_interlocked(pInterlocked, operation, pParameter1, pParameter2, flags)
  !dec$ attributes stdcall, alias : '_KS_interlocked@20' :: KS_interlocked
  integer*4 pInterlocked
  integer*4 operation
  integer*4 pParameter1
  integer*4 pParameter2
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! IoPort Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_CANNOT_ENABLE_IO_RANGE          = (KSERROR_CATEGORY_IOPORT+#00000000)
integer*4, parameter :: KSERROR_BUS_NOT_FOUND                   = (KSERROR_CATEGORY_IOPORT+#00010000)
integer*4, parameter :: KSERROR_BAD_SLOT_NUMBER                 = (KSERROR_CATEGORY_IOPORT+#00020000)

!------ Number of address entries in PCI data ------

integer*4, parameter :: KS_PCI_TYPE0_ADDRESSES                  = #00000006
integer*4, parameter :: KS_PCI_TYPE1_ADDRESSES                  = #00000002
integer*4, parameter :: KS_PCI_TYPE2_ADDRESSES                  = #00000004

!------ Bit encodings for KSPciBusData.headerType ------

integer*4, parameter :: KSF_PCI_MULTIFUNCTION                   = #00000080
integer*4, parameter :: KSF_PCI_DEVICE_TYPE                     = #00000000
integer*4, parameter :: KSF_PCI_PCI_BRIDGE_TYPE                 = #00000001
integer*4, parameter :: KSF_PCI_CARDBUS_BRIDGE_TYPE             = #00000002

!------ Bit encodings for KSPciBusData.command ------

integer*4, parameter :: KSF_PCI_ENABLE_IO_SPACE                 = #00000001
integer*4, parameter :: KSF_PCI_ENABLE_MEMORY_SPACE             = #00000002
integer*4, parameter :: KSF_PCI_ENABLE_BUS_MASTER               = #00000004
integer*4, parameter :: KSF_PCI_ENABLE_SPECIAL_CYCLES           = #00000008
integer*4, parameter :: KSF_PCI_ENABLE_WRITE_AND_INVALIDATE     = #00000010
integer*4, parameter :: KSF_PCI_ENABLE_VGA_COMPATIBLE_PALETTE   = #00000020
integer*4, parameter :: KSF_PCI_ENABLE_PARITY                   = #00000040
integer*4, parameter :: KSF_PCI_ENABLE_WAIT_CYCLE               = #00000080
integer*4, parameter :: KSF_PCI_ENABLE_SERR                     = #00000100
integer*4, parameter :: KSF_PCI_ENABLE_FAST_BACK_TO_BACK        = #00000200

!------ Bit encodings for KSPciBusData.status ------

integer*4, parameter :: KSF_PCI_STATUS_CAPABILITIES_LIST        = #00000010
integer*4, parameter :: KSF_PCI_STATUS_FAST_BACK_TO_BACK        = #00000080
integer*4, parameter :: KSF_PCI_STATUS_DATA_PARITY_DETECTED     = #00000100
integer*4, parameter :: KSF_PCI_STATUS_DEVSEL                   = #00000600
integer*4, parameter :: KSF_PCI_STATUS_SIGNALED_TARGET_ABORT    = #00000800
integer*4, parameter :: KSF_PCI_STATUS_RECEIVED_TARGET_ABORT    = #00001000
integer*4, parameter :: KSF_PCI_STATUS_RECEIVED_MASTER_ABORT    = #00002000
integer*4, parameter :: KSF_PCI_STATUS_SIGNALED_SYSTEM_ERROR    = #00004000
integer*4, parameter :: KSF_PCI_STATUS_DETECTED_PARITY_ERROR    = #00008000

!------ Bit encodings for KSPciBusData.baseAddresses ------

integer*4, parameter :: KSF_PCI_ADDRESS_IO_SPACE                = #00000001
integer*4, parameter :: KSF_PCI_ADDRESS_MEMORY_TYPE_MASK        = #00000006
integer*4, parameter :: KSF_PCI_ADDRESS_MEMORY_PREFETCHABLE     = #00000008

!------ PCI types ------

integer*4, parameter :: KSF_PCI_TYPE_32BIT                      = #00000000
integer*4, parameter :: KSF_PCI_TYPE_20BIT                      = #00000002
integer*4, parameter :: KSF_PCI_TYPE_64BIT                      = #00000004

!------ Resource types ------

integer*4, parameter :: KSRESOURCE_TYPE_NONE                    = #00000000
integer*4, parameter :: KSRESOURCE_TYPE_PORT                    = #00000001
integer*4, parameter :: KSRESOURCE_TYPE_INTERRUPT               = #00000002
integer*4, parameter :: KSRESOURCE_TYPE_MEMORY                  = #00000003
integer*4, parameter :: KSRESOURCE_TYPE_STRING                  = #000000ff

!------ KSRESOURCE_SHARE ------

integer*4, parameter :: KSRESOURCE_SHARE_UNDETERMINED           = #00000000
integer*4, parameter :: KSRESOURCE_SHARE_DEVICEEXCLUSIVE        = #00000001
integer*4, parameter :: KSRESOURCE_SHARE_DRIVEREXCLUSIVE        = #00000002
integer*4, parameter :: KSRESOURCE_SHARE_SHARED                 = #00000003

!------ KSRESOURCE_MODE / KSRESOURCE_TYPE_PORT ------

integer*4, parameter :: KSRESOURCE_MODE_MEMORY                  = #00000000
integer*4, parameter :: KSRESOURCE_MODE_IO                      = #00000001

!------ KSRESOURCE_MODE / KSRESOURCE_TYPE_INTERRUPT ------

integer*4, parameter :: KSRESOURCE_MODE_LEVELSENSITIVE          = #00000000
integer*4, parameter :: KSRESOURCE_MODE_LATCHED                 = #00000001

!------ KSRESOURCE_MODE / KSRESOURCE_TYPE_MEMORY ------

integer*4, parameter :: KSRESOURCE_MODE_READ_WRITE              = #00000000
integer*4, parameter :: KSRESOURCE_MODE_READ_ONLY               = #00000001
integer*4, parameter :: KSRESOURCE_MODE_WRITE_ONLY              = #00000002

type KSPciBusData
  integer*2 vendorID
  integer*2 deviceID
  integer*2 command
  integer*2 status
  byte revisionID
  byte progIf
  byte subClass
  byte baseClass
  byte cacheLineSize
  byte latencyTimer
  byte headerType
  byte BIST
  integer*4 pBaseAddresses(0:5)
  integer*4 CIS
  integer*2 subVendorID
  integer*2 subSystemID
  integer*4 baseROMAddress
  integer*4 capabilityList
  integer*4 reserved
  byte interruptLine
  byte interruptPin
  byte minimumGrant
  byte maximumLatency
  byte pDeviceSpecific(0:191)
end type

type KSResourceItem
  integer*4 flags
  integer*4 data0
  integer*4 data1
  integer*4 data2
end type

type KSRangeResourceItem
  integer*4 flags
  integer*4 address
  integer*4 reserved
  integer*4 length
end type

type KSInterruptResourceItem
  integer*4 flags
  integer*4 level
  integer*4 vector
  integer*4 affinity
end type

type KSResourceInfoEx
  integer*4 busType
  integer*4 busNumber
  integer*4 version
  integer*4 itemCount
  type (KSRangeResourceItem) range0
  type (KSRangeResourceItem) range1
  type (KSRangeResourceItem) range2
  type (KSRangeResourceItem) range3
  type (KSRangeResourceItem) range4
  type (KSRangeResourceItem) range5
  type (KSInterruptResourceItem) interruptItem
  type (KSResourceItem) reserved
end type

!------ I/O port access functions ------

interface

integer*4 function KS_enableIoRange(ioPortBase, count, flags)
  !dec$ attributes stdcall, alias : '_KS_enableIoRange@12' :: KS_enableIoRange
  integer*4 ioPortBase
  integer*4 count
  integer*4 flags
end function

integer*4 function KS_readIoPort(ioPort, pValue, flags)
  !dec$ attributes stdcall, alias : '_KS_readIoPort@12' :: KS_readIoPort
  integer*4 ioPort
  integer*4 pValue
  integer*4 flags
end function

integer*4 function KS_writeIoPort(ioPort, value, flags)
  !dec$ attributes stdcall, alias : '_KS_writeIoPort@12' :: KS_writeIoPort
  integer*4 ioPort
  integer*4 value
  integer*4 flags
end function

integer*4 function KS_inpb(ioPort)
  !dec$ attributes stdcall, alias : '_KS_inpb@4' :: KS_inpb
  integer*4 ioPort
end function

integer*4 function KS_inpw(ioPort)
  !dec$ attributes stdcall, alias : '_KS_inpw@4' :: KS_inpw
  integer*4 ioPort
end function

integer*4 function KS_inpd(ioPort)
  !dec$ attributes stdcall, alias : '_KS_inpd@4' :: KS_inpd
  integer*4 ioPort
end function

subroutine KS_outpb(ioPort, value)
  !dec$ attributes stdcall, alias : '_KS_outpb@8' :: KS_outpb
  integer*4 ioPort
  integer*4 value
end subroutine

subroutine KS_outpw(ioPort, value)
  !dec$ attributes stdcall, alias : '_KS_outpw@8' :: KS_outpw
  integer*4 ioPort
  integer*4 value
end subroutine

subroutine KS_outpd(ioPort, value)
  !dec$ attributes stdcall, alias : '_KS_outpd@8' :: KS_outpd
  integer*4 ioPort
  integer*4 value
end subroutine

end interface

!------ Bus data & resource info ------

interface

integer*4 function KS_getBusData(pBusData, busNumber, slotNumber, flags)
  !dec$ attributes stdcall, alias : '_KS_getBusData@16' :: KS_getBusData
  integer*4 pBusData
  integer*4 busNumber
  integer*4 slotNumber
  integer*4 flags
end function

integer*4 function KS_setBusData(pBusData, busNumber, slotNumber, flags)
  !dec$ attributes stdcall, alias : '_KS_setBusData@16' :: KS_setBusData
  integer*4 pBusData
  integer*4 busNumber
  integer*4 slotNumber
  integer*4 flags
end function

integer*4 function KS_getResourceInfoEx(name, pInfo)
  !dec$ attributes stdcall, alias : '_KS_getResourceInfoEx@8' :: KS_getResourceInfoEx
  integer*4 name
  integer*4 pInfo
end function

end interface

!------ Quiet section (obsolete) ------

interface

end interface

!---------------------------------------------------------------------------------------------------------------
! Memory Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_CANNOT_MAP_PHYSMEM              = (KSERROR_CATEGORY_MEMORY+#00000000)
integer*4, parameter :: KSERROR_CANNOT_UNMAP_PHYSMEM            = (KSERROR_CATEGORY_MEMORY+#00010000)
integer*4, parameter :: KSERROR_PHYSMEM_DIFFERENT_SIZE          = (KSERROR_CATEGORY_MEMORY+#00020000)
integer*4, parameter :: KSERROR_CANNOT_ALLOC_PHYSMEM            = (KSERROR_CATEGORY_MEMORY+#00030000)
integer*4, parameter :: KSERROR_CANNOT_LOCK_MEM                 = (KSERROR_CATEGORY_MEMORY+#00040000)
integer*4, parameter :: KSERROR_CANNOT_UNLOCK_MEM               = (KSERROR_CATEGORY_MEMORY+#00050000)

!------ Flags ------

integer*4, parameter :: KSF_MAP_INTERNAL                        = #00000000

!------ Physical memory management functions ------

interface

integer*4 function KS_mapDeviceMem(ppAppPtr, ppSysPtr, pInfo, index, flags)
  !dec$ attributes stdcall, alias : '_KS_mapDeviceMem@20' :: KS_mapDeviceMem
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 pInfo
  integer*4 index
  integer*4 flags
end function

integer*4 function KS_mapPhysMem(ppAppPtr, ppSysPtr, physAddr, size, flags)
  !dec$ attributes stdcall, alias : '_KS_mapPhysMem@20' :: KS_mapPhysMem
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 physAddr
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_unmapPhysMem(pAppPtr)
  !dec$ attributes stdcall, alias : '_KS_unmapPhysMem@4' :: KS_unmapPhysMem
  integer*4 pAppPtr
end function

integer*4 function KS_allocPhysMem(ppAppPtr, ppSysPtr, physAddr, size, flags)
  !dec$ attributes stdcall, alias : '_KS_allocPhysMem@20' :: KS_allocPhysMem
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 physAddr
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_freePhysMem(pAppPtr)
  !dec$ attributes stdcall, alias : '_KS_freePhysMem@4' :: KS_freePhysMem
  integer*4 pAppPtr
end function

integer*4 function KS_copyPhysMem(physAddr, pBuffer, size, offset, flags)
  !dec$ attributes stdcall, alias : '_KS_copyPhysMem@20' :: KS_copyPhysMem
  integer*4 physAddr
  integer*4 pBuffer
  integer*4 size
  integer*4 offset
  integer*4 flags
end function

integer*4 function KS_getPhysAddr(pPhysAddr, pDosLinAddr)
  !dec$ attributes stdcall, alias : '_KS_getPhysAddr@8' :: KS_getPhysAddr
  integer*4 pPhysAddr
  integer*4 pDosLinAddr
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Clock Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_NO_RDTSC_AVAILABLE              = (KSERROR_CATEGORY_SPECIAL+#00010000)

!------ Flags ------

integer*4, parameter :: KSF_USE_GIVEN_FREQ                      = #00010000
integer*4, parameter :: KSF_CLOCK_DELEGATE                      = #00000001
integer*4, parameter :: KSF_CLOCK_CONVERT_ONLY                  = #00000002

!------ Clock source constants (for 'clockId') ------

integer*4, parameter :: KS_CLOCK_DEFAULT                        = #00000000
integer*4, parameter :: KS_CLOCK_MEASURE_SYSTEM_TIMER           = #00000001
integer*4, parameter :: KS_CLOCK_MEASURE_CPU_COUNTER            = #00000002
integer*4, parameter :: KS_CLOCK_MEASURE_APIC_TIMER             = #00000003
integer*4, parameter :: KS_CLOCK_MEASURE_PM_TIMER               = #00000004
integer*4, parameter :: KS_CLOCK_MEASURE_PC_TIMER               = #00000005
integer*4, parameter :: KS_CLOCK_MEASURE_INTERVAL_COUNTER       = #00000006
integer*4, parameter :: KS_CLOCK_MEASURE_HPET                   = #00000007
integer*4, parameter :: KS_CLOCK_MEASURE_HIGHEST                = #00000008
integer*4, parameter :: KS_CLOCK_CONVERT_HIGHEST                = #00000009
integer*4, parameter :: KS_CLOCK_PC_TIMER                       = #0000000a
integer*4, parameter :: KS_CLOCK_MACHINE_TIME                   = #0000000b
integer*4, parameter :: KS_CLOCK_ABSOLUTE_TIME                  = #0000000c
integer*4, parameter :: KS_CLOCK_ABSOLUTE_TIME_LOCAL            = #0000000d
integer*4, parameter :: KS_CLOCK_MICROSECONDS                   = #0000000e
integer*4, parameter :: KS_CLOCK_UNIX_TIME                      = #0000000f
integer*4, parameter :: KS_CLOCK_UNIX_TIME_LOCAL                = #00000010
integer*4, parameter :: KS_CLOCK_MILLISECONDS                   = #00000011
integer*4, parameter :: KS_CLOCK_DOS_TIME                       = #00000012
integer*4, parameter :: KS_CLOCK_DOS_TIME_LOCAL                 = #00000013
integer*4, parameter :: KS_CLOCK_IEEE1588_TIME                  = #00000014
integer*4, parameter :: KS_CLOCK_IEEE1588_CONVERT               = #00000015
integer*4, parameter :: KS_CLOCK_TIME_OF_DAY                    = #00000016
integer*4, parameter :: KS_CLOCK_TIME_OF_DAY_LOCAL              = #00000017
integer*4, parameter :: KS_CLOCK_FIRST_USER                     = #00000018

!------ Obsolete clock source constant (use KS_CLOCK_MACHINE_TIME instead!) ------

integer*4, parameter :: KS_CLOCK_SYSTEM_TIME                    = #0000000b

type KSClockSource
  integer*4 structSize
  integer*4 flags
  integer*4 frequency
  integer*4 offset
  integer*4 baseClockId
end type

!------ Clock functions - Obsolete! Do not use! ------

interface

integer*4 function KS_calibrateMachineTime(pFrequency, flags)
  !dec$ attributes stdcall, alias : '_KS_calibrateMachineTime@8' :: KS_calibrateMachineTime
  integer*4 pFrequency
  integer*4 flags
end function

integer*4 function KS_getSystemTicks(pSystemTicks)
  !dec$ attributes stdcall, alias : '_KS_getSystemTicks@4' :: KS_getSystemTicks
  integer*4 pSystemTicks
end function

integer*4 function KS_getSystemTime(pSystemTime)
  !dec$ attributes stdcall, alias : '_KS_getSystemTime@4' :: KS_getSystemTime
  integer*4 pSystemTime
end function

integer*4 function KS_getMachineTime(pMachineTime)
  !dec$ attributes stdcall, alias : '_KS_getMachineTime@4' :: KS_getMachineTime
  integer*4 pMachineTime
end function

integer*4 function KS_getAbsTime(pAbsTime)
  !dec$ attributes stdcall, alias : '_KS_getAbsTime@4' :: KS_getAbsTime
  integer*4 pAbsTime
end function

end interface

!------ Clock functions ------

interface

integer*4 function KS_getClock(pClock, clockId)
  !dec$ attributes stdcall, alias : '_KS_getClock@8' :: KS_getClock
  integer*4 pClock
  integer*4 clockId
end function

integer*4 function KS_getClockSource(pClockSource, clockId)
  !dec$ attributes stdcall, alias : '_KS_getClockSource@8' :: KS_getClockSource
  integer*4 pClockSource
  integer*4 clockId
end function

integer*4 function KS_setClockSource(pClockSource, clockId)
  !dec$ attributes stdcall, alias : '_KS_setClockSource@8' :: KS_setClockSource
  integer*4 pClockSource
  integer*4 clockId
end function

integer*4 function KS_microDelay(delay)
  !dec$ attributes stdcall, alias : '_KS_microDelay@4' :: KS_microDelay
  integer*4 delay
end function

integer*4 function KS_convertClock(pValue, clockIdFrom, clockIdTo, flags)
  !dec$ attributes stdcall, alias : '_KS_convertClock@16' :: KS_convertClock
  integer*4 pValue
  integer*4 clockIdFrom
  integer*4 clockIdTo
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! System Module
!---------------------------------------------------------------------------------------------------------------

!------ System contexts ------

integer*4, parameter :: SYSTEMHANDLER_CONTEXT                   = (USER_CONTEXT+#00000030)

!------ System events ------

integer*4, parameter :: KS_SYSTEM_CRASH                         = #00000001
integer*4, parameter :: KS_SYSTEM_SHUTDOWN                      = #00000002
integer*4, parameter :: KS_SYSTEM_PROCESS_ATTACH                = #00000010
integer*4, parameter :: KS_SYSTEM_PROCESS_DETACH                = #00000020
integer*4, parameter :: KS_SYSTEM_PROCESS_ABORT                 = #00000040

type SystemHandlerUserContext
  integer*4 ctxType
  integer*4 eventType
  integer*4 handlerProcessId
  integer*4 currentProcessId
end type

!------ System handling functions ------

interface

integer*4 function KS_installSystemHandler(eventMask, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installSystemHandler@12' :: KS_installSystemHandler
  integer*4 eventMask
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_removeSystemHandler(eventMask)
  !dec$ attributes stdcall, alias : '_KS_removeSystemHandler@4' :: KS_removeSystemHandler
  integer*4 eventMask
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Device Module
!---------------------------------------------------------------------------------------------------------------

!------ Flags ------

integer*4, parameter :: KSF_SERIAL_PORT                         = #00200000

!------ Function codes ------

integer*4, parameter :: KS_DEVICE_CREATE                        = #00000000
integer*4, parameter :: KS_DEVICE_CLOSE                         = #00000001
integer*4, parameter :: KS_DEVICE_READ                          = #00000002
integer*4, parameter :: KS_DEVICE_WRITE                         = #00000003
integer*4, parameter :: KS_DEVICE_CONTROL                       = #00000004
integer*4, parameter :: KS_DEVICE_CLEANUP                       = #00000005
integer*4, parameter :: KS_DEVICE_CANCEL_REQUEST                = #00010000

!------ Return codes ------

integer*4, parameter :: KS_RETURN_SUCCESS                       = #00000000
integer*4, parameter :: KS_RETURN_UNSUCCESSFUL                  = #00000001
integer*4, parameter :: KS_RETURN_INVALID_DEVICE_REQUEST        = #00000002
integer*4, parameter :: KS_RETURN_INVALID_PARAMETER             = #00000003
integer*4, parameter :: KS_RETURN_TIMEOUT                       = #00000004
integer*4, parameter :: KS_RETURN_CANCELLED                     = #00000005
integer*4, parameter :: KS_RETURN_PENDING                       = #00000006
integer*4, parameter :: KS_RETURN_DELETE_PENDING                = #00000007
integer*4, parameter :: KS_RETURN_BUFFER_TOO_SMALL              = #00000008

type DeviceUserContext
  integer*4 ctxType
  integer*4 hRequest
  integer*4 functionCode
  integer*4 controlCode
  integer*4 pBuffer
  integer*4 size
  integer*4 returnCode
end type

type KSDeviceFunctions
  integer*4 hOnCreate
  integer*4 hOnClose
  integer*4 hOnRead
  integer*4 hOnWrite
  integer*4 hOnControl
  integer*4 hOnCleanup
end type

!------ Device functions ------

interface

integer*4 function KS_createDevice(phDevice, name, pFunctions, flags)
  !dec$ attributes stdcall, alias : '_KS_createDevice@16' :: KS_createDevice
  integer*4 phDevice
  integer*4 name
  integer*4 pFunctions
  integer*4 flags
end function

integer*4 function KS_closeDevice(hDevice)
  !dec$ attributes stdcall, alias : '_KS_closeDevice@4' :: KS_closeDevice
  integer*4 hDevice
end function

integer*4 function KS_resumeDevice(hDevice, hRequest, pBuffer, size, returnCode, flags)
  !dec$ attributes stdcall, alias : '_KS_resumeDevice@24' :: KS_resumeDevice
  integer*4 hDevice
  integer*4 hRequest
  integer*4 pBuffer
  integer*4 size
  integer*4 returnCode
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Keyboard Module
!---------------------------------------------------------------------------------------------------------------

!------ Flags ------

integer*4, parameter :: KSF_SIGNAL_MAKE                         = #00000001
integer*4, parameter :: KSF_SIGNAL_BREAK                        = #00000002
integer*4, parameter :: KSF_IGNORE_KEY                          = #00000010
integer*4, parameter :: KSF_MODIFY_KEY                          = #00000020

!------ Key mask modifier ------

integer*4, parameter :: KSF_KEY_SHIFT                           = #00000011
integer*4, parameter :: KSF_KEY_SHIFTR                          = #00000001
integer*4, parameter :: KSF_KEY_SHIFTL                          = #00000010
integer*4, parameter :: KSF_KEY_CTRL                            = #00000022
integer*4, parameter :: KSF_KEY_CTRLR                           = #00000002
integer*4, parameter :: KSF_KEY_CTRLL                           = #00000020
integer*4, parameter :: KSF_KEY_ALT                             = #00000044
integer*4, parameter :: KSF_KEY_ALTR                            = #00000004
integer*4, parameter :: KSF_KEY_ALTL                            = #00000040
integer*4, parameter :: KSF_KEY_WIN                             = #00000088
integer*4, parameter :: KSF_KEY_WINR                            = #00000008
integer*4, parameter :: KSF_KEY_WINL                            = #00000080

!------ Key mask groups ------

integer*4, parameter :: KSF_KEY_ALPHA_NUM                       = #00000100
integer*4, parameter :: KSF_KEY_NUM_PAD                         = #00000200
integer*4, parameter :: KSF_KEY_CONTROL                         = #00000400
integer*4, parameter :: KSF_KEY_FUNCTION                        = #00000800

!------ Key mask combinations ------

integer*4, parameter :: KSF_KEY_CTRL_ALT_DEL                    = #00010000
integer*4, parameter :: KSF_KEY_CTRL_ESC                        = #00020000
integer*4, parameter :: KSF_KEY_ALT_ESC                         = #00040000
integer*4, parameter :: KSF_KEY_ALT_TAB                         = #00080000
integer*4, parameter :: KSF_KEY_ALT_ENTER                       = #00100000
integer*4, parameter :: KSF_KEY_ALT_PRINT                       = #00200000
integer*4, parameter :: KSF_KEY_ALT_SPACE                       = #00400000
integer*4, parameter :: KSF_KEY_ESC                             = #00800000
integer*4, parameter :: KSF_KEY_TAB                             = #01000000
integer*4, parameter :: KSF_KEY_ENTER                           = #02000000
integer*4, parameter :: KSF_KEY_PRINT                           = #04000000
integer*4, parameter :: KSF_KEY_SPACE                           = #08000000
integer*4, parameter :: KSF_KEY_BACKSPACE                       = #10000000
integer*4, parameter :: KSF_KEY_PAUSE                           = #20000000
integer*4, parameter :: KSF_KEY_APP                             = #40000000
integer*4, parameter :: KSF_KEY_ALL                             = #7fffffff

integer*4, parameter :: KSF_KEY_MODIFIER                        = #000000ff
  ! KSF_KEY_CTRL | KSF_KEY_ALT | KSF_KEY_SHIFT | KSF_KEY_WIN

integer*4, parameter :: KSF_KEY_SYSTEM                          = #000f0088
  ! KSF_KEY_WIN | KSF_KEY_CTRL_ALT_DEL | KSF_KEY_CTRL_ESC |
  ! KSF_KEY_ALT_ESC | KSF_KEY_ALT_TAB

integer*4, parameter :: KSF_KEY_SYSTEM_DOS                      = #047f0088
  ! KSF_KEY_SYSTEM | KSF_KEY_ALT_ENTER | KSF_KEY_ALT_PRINT |
  ! KSF_KEY_ALT_SPACE | KSF_KEY_PRINT

!------ Keyboard events ------

integer*4, parameter :: KS_KEYBOARD_CONTEXT                     = #00000300

!------ Context structures ------

type KeyboardUserContext
  integer*4 ctxType
  integer*4 mask
  integer*2 modifier
  integer*2 port
  integer*2 state
  integer*2 key
end type

!------ Keyboard functions ------

interface

integer*4 function KS_createKeyHandler(mask, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_createKeyHandler@12' :: KS_createKeyHandler
  integer*4 mask
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_removeKeyHandler(mask)
  !dec$ attributes stdcall, alias : '_KS_removeKeyHandler@4' :: KS_removeKeyHandler
  integer*4 mask
end function

integer*4 function KS_simulateKeyStroke(pKeys)
  !dec$ attributes stdcall, alias : '_KS_simulateKeyStroke@4' :: KS_simulateKeyStroke
  integer*4 pKeys
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Interrupt Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_IRQ_ALREADY_USED                = (KSERROR_CATEGORY_INTERRUPT+#00000000)
integer*4, parameter :: KSERROR_CANNOT_INSTALL_HANDLER          = (KSERROR_CATEGORY_INTERRUPT+#00010000)
integer*4, parameter :: KSERROR_BAD_IRQ_INSTALLATION            = (KSERROR_CATEGORY_INTERRUPT+#00020000)
integer*4, parameter :: KSERROR_NO_HANDLER_INSTALLED            = (KSERROR_CATEGORY_INTERRUPT+#00030000)
integer*4, parameter :: KSERROR_NOBODY_IS_WAITING               = (KSERROR_CATEGORY_INTERRUPT+#00040000)
integer*4, parameter :: KSERROR_IRQ_DISABLED                    = (KSERROR_CATEGORY_INTERRUPT+#00050000)
integer*4, parameter :: KSERROR_IRQ_ENABLED                     = (KSERROR_CATEGORY_INTERRUPT+#00060000)
integer*4, parameter :: KSERROR_IRQ_IN_SERVICE                  = (KSERROR_CATEGORY_INTERRUPT+#00070000)
integer*4, parameter :: KSERROR_IRQ_HANDLER_REMOVED             = (KSERROR_CATEGORY_INTERRUPT+#00080000)

!------ Flags ------

integer*4, parameter :: KSF_DONT_PASS_NEXT                      = #00020000
integer*4, parameter :: KSF_PCI_INTERRUPT                       = #00040000

!------ Interrupt events ------

integer*4, parameter :: KS_INTERRUPT_CONTEXT                    = #00000100

type InterruptUserContext
  integer*4 ctxType
  integer*4 hInterrupt
  integer*4 irq
  integer*4 consumed
end type

!------ Interrupt handling functions ------

interface

integer*4 function KS_createDeviceInterrupt(phInterrupt, pInfo, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_createDeviceInterrupt@16' :: KS_createDeviceInterrupt
  integer*4 phInterrupt
  integer*4 pInfo
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_createDeviceInterruptEx(phInterrupt, deviceName, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_createDeviceInterruptEx@16' :: KS_createDeviceInterruptEx
  integer*4 phInterrupt
  integer*4 deviceName
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_createInterrupt(phInterrupt, irq, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_createInterrupt@16' :: KS_createInterrupt
  integer*4 phInterrupt
  integer*4 irq
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_removeInterrupt(hInterrupt)
  !dec$ attributes stdcall, alias : '_KS_removeInterrupt@4' :: KS_removeInterrupt
  integer*4 hInterrupt
end function

integer*4 function KS_disableInterrupt(hInterrupt)
  !dec$ attributes stdcall, alias : '_KS_disableInterrupt@4' :: KS_disableInterrupt
  integer*4 hInterrupt
end function

integer*4 function KS_enableInterrupt(hInterrupt)
  !dec$ attributes stdcall, alias : '_KS_enableInterrupt@4' :: KS_enableInterrupt
  integer*4 hInterrupt
end function

integer*4 function KS_simulateInterrupt(hInterrupt)
  !dec$ attributes stdcall, alias : '_KS_simulateInterrupt@4' :: KS_simulateInterrupt
  integer*4 hInterrupt
end function

integer*4 function KS_getInterruptState(hInterrupt, pState)
  !dec$ attributes stdcall, alias : '_KS_getInterruptState@8' :: KS_getInterruptState
  integer*4 hInterrupt
  integer*4 pState
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! UART Module
!---------------------------------------------------------------------------------------------------------------

!------ Flags ------

integer*4, parameter :: KSF_CHECK_LINE_ERROR                    = #00008000
integer*4, parameter :: KSF_NO_FIFO                             = #00001000
integer*4, parameter :: KSF_USE_FIFO                            = #00040000
integer*4, parameter :: KSF_9_BIT_MODE                          = #00080000
integer*4, parameter :: KSF_TOGGLE_DTR                          = #00100000
integer*4, parameter :: KSF_TOGGLE_RTS                          = #00200000
integer*4, parameter :: KSF_TOGGLE_INVERSE                      = #00400000

!------ Uart commands ------

integer*4, parameter :: KS_CLR_DTR                              = #00000004
integer*4, parameter :: KS_SET_DTR                              = #00000005
integer*4, parameter :: KS_CLR_RTS                              = #00000006
integer*4, parameter :: KS_SET_RTS                              = #00000007
integer*4, parameter :: KS_CLR_BREAK                            = #00000008
integer*4, parameter :: KS_SET_BREAK                            = #0000000a
integer*4, parameter :: KS_SKIP_NEXT                            = #0000000b
integer*4, parameter :: KS_CHANGE_LINE_CONTROL                  = #0000000d
integer*4, parameter :: KS_SET_BAUD_RATE_MAX                    = #0000000e
integer*4, parameter :: KS_ENABLE_FIFO                          = #0000000f
integer*4, parameter :: KS_DISABLE_FIFO                         = #00000010
integer*4, parameter :: KS_ENABLE_INTERRUPTS                    = #00000011
integer*4, parameter :: KS_DISABLE_INTERRUPTS                   = #00000012
integer*4, parameter :: KS_SET_RECV_FIFO_SIZE                   = #00000013

!------ Line Errors ------

integer*4, parameter :: KS_UART_ERROR_PARITY                    = #00000001
integer*4, parameter :: KS_UART_ERROR_FRAME                     = #00000002
integer*4, parameter :: KS_UART_ERROR_OVERRUN                   = #00000004
integer*4, parameter :: KS_UART_ERROR_BREAK                     = #00000008
integer*4, parameter :: KS_UART_ERROR_FIFO                      = #00000010

!------ UART events ------

integer*4, parameter :: KS_UART_RECV                            = #00000000
integer*4, parameter :: KS_UART_RECV_ERROR                      = #00000001
integer*4, parameter :: KS_UART_XMIT                            = #00000002
integer*4, parameter :: KS_UART_XMIT_EMPTY                      = #00000003
integer*4, parameter :: KS_UART_LSR_CHANGE                      = #00000004
integer*4, parameter :: KS_UART_MSR_CHANGE                      = #00000005
integer*4, parameter :: KS_UART_BREAK                           = #00000006

type UartUserContext
  integer*4 ctxType
  integer*4 hUart
  integer*4 value
  integer*4 lineError
end type

type UartRegisterUserContext
  integer*4 ctxType
  integer*4 hUart
  byte reg
end type

type KSUartProperties
  integer*4 baudRate
  byte parity
  byte dataBit
  byte stopBit
  byte reserved
end type

type KSUartState
  integer*4 structSize
  type (KSUartProperties) properties
  byte modemState
  byte lineState
  integer*2 reserved
  integer*4 xmitCount
  integer*4 xmitFree
  integer*4 recvCount
  integer*4 recvAvail
  integer*4 recvErrorCount
  integer*4 recvLostCount
end type

!------ Common functions ------

interface

integer*4 function KS_openUart(phUart, name, port, pProperties, recvBufferLength, xmitBufferLength, flags)
  !dec$ attributes stdcall, alias : '_KS_openUart@28' :: KS_openUart
  integer*4 phUart
  integer*4 name
  integer*4 port
  integer*4 pProperties
  integer*4 recvBufferLength
  integer*4 xmitBufferLength
  integer*4 flags
end function

integer*4 function KS_openUartEx(phUart, hConnection, port, pProperties, recvBufferLength, xmitBufferLength, flags)
  !dec$ attributes stdcall, alias : '_KS_openUartEx@28' :: KS_openUartEx
  integer*4 phUart
  integer*4 hConnection
  integer*4 port
  integer*4 pProperties
  integer*4 recvBufferLength
  integer*4 xmitBufferLength
  integer*4 flags
end function

integer*4 function KS_closeUart(hUart, flags)
  !dec$ attributes stdcall, alias : '_KS_closeUart@8' :: KS_closeUart
  integer*4 hUart
  integer*4 flags
end function

end interface

!------ Communication functions ------

interface

integer*4 function KS_xmitUart(hUart, value, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitUart@12' :: KS_xmitUart
  integer*4 hUart
  integer*4 value
  integer*4 flags
end function

integer*4 function KS_recvUart(hUart, pValue, pLineError, flags)
  !dec$ attributes stdcall, alias : '_KS_recvUart@16' :: KS_recvUart
  integer*4 hUart
  integer*4 pValue
  integer*4 pLineError
  integer*4 flags
end function

integer*4 function KS_xmitUartBlock(hUart, pBytes, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitUartBlock@20' :: KS_xmitUartBlock
  integer*4 hUart
  integer*4 pBytes
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_recvUartBlock(hUart, pBytes, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_recvUartBlock@20' :: KS_recvUartBlock
  integer*4 hUart
  integer*4 pBytes
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_installUartHandler(hUart, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installUartHandler@16' :: KS_installUartHandler
  integer*4 hUart
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_getUartState(hUart, pUartState, flags)
  !dec$ attributes stdcall, alias : '_KS_getUartState@12' :: KS_getUartState
  integer*4 hUart
  integer*4 pUartState
  integer*4 flags
end function

integer*4 function KS_execUartCommand(hUart, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execUartCommand@16' :: KS_execUartCommand
  integer*4 hUart
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! COMM Module
!---------------------------------------------------------------------------------------------------------------

!------ Line Errors ------

integer*4, parameter :: KS_COMM_ERROR_PARITY                    = #00000001
integer*4, parameter :: KS_COMM_ERROR_FRAME                     = #00000002
integer*4, parameter :: KS_COMM_ERROR_OVERRUN                   = #00000004
integer*4, parameter :: KS_COMM_ERROR_BREAK                     = #00000008
integer*4, parameter :: KS_COMM_ERROR_FIFO                      = #00000010

!------ COMM events ------

integer*4, parameter :: KS_COMM_RECV                            = #00000000
integer*4, parameter :: KS_COMM_RECV_ERROR                      = #00000001
integer*4, parameter :: KS_COMM_XMIT                            = #00000002
integer*4, parameter :: KS_COMM_XMIT_EMPTY                      = #00000003
integer*4, parameter :: KS_COMM_LSR_CHANGE                      = #00000004
integer*4, parameter :: KS_COMM_MSR_CHANGE                      = #00000005
integer*4, parameter :: KS_COMM_BREAK                           = #00000006

type CommUserContext
  integer*4 ctxType
  integer*4 hComm
  integer*4 value
  integer*4 lineError
end type

type CommRegisterUserContext
  integer*4 ctxType
  integer*4 hComm
  byte reg
end type

type KSCommProperties
  integer*4 baudRate
  byte parity
  byte dataBit
  byte stopBit
  byte reserved
end type

type KSCommState
  integer*4 structSize
  type (KSCommProperties) properties
  byte modemState
  byte lineState
  integer*2 reserved
  integer*4 xmitCount
  integer*4 xmitFree
  integer*4 recvCount
  integer*4 recvAvail
  integer*4 recvErrorCount
  integer*4 recvLostCount
end type

!------ COMM functions ------

interface

integer*4 function KS_openComm(phComm, name, pProperties, recvBufferLength, xmitBufferLength, flags)
  !dec$ attributes stdcall, alias : '_KS_openComm@24' :: KS_openComm
  integer*4 phComm
  integer*4 name
  integer*4 pProperties
  integer*4 recvBufferLength
  integer*4 xmitBufferLength
  integer*4 flags
end function

integer*4 function KS_closeComm(hComm, flags)
  !dec$ attributes stdcall, alias : '_KS_closeComm@8' :: KS_closeComm
  integer*4 hComm
  integer*4 flags
end function

integer*4 function KS_xmitComm(hComm, value, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitComm@12' :: KS_xmitComm
  integer*4 hComm
  integer*4 value
  integer*4 flags
end function

integer*4 function KS_recvComm(hComm, pValue, pLineError, flags)
  !dec$ attributes stdcall, alias : '_KS_recvComm@16' :: KS_recvComm
  integer*4 hComm
  integer*4 pValue
  integer*4 pLineError
  integer*4 flags
end function

integer*4 function KS_xmitCommBlock(hComm, pBytes, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitCommBlock@20' :: KS_xmitCommBlock
  integer*4 hComm
  integer*4 pBytes
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_recvCommBlock(hComm, pBytes, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_recvCommBlock@20' :: KS_recvCommBlock
  integer*4 hComm
  integer*4 pBytes
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_installCommHandler(hComm, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installCommHandler@16' :: KS_installCommHandler
  integer*4 hComm
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_getCommState(hComm, pCommState, flags)
  !dec$ attributes stdcall, alias : '_KS_getCommState@12' :: KS_getCommState
  integer*4 hComm
  integer*4 pCommState
  integer*4 flags
end function

integer*4 function KS_execCommCommand(hComm, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execCommCommand@16' :: KS_execCommCommand
  integer*4 hComm
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! USB Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_USB_REQUEST_FAILED              = (KSERROR_CATEGORY_USB+#00000000)

!------ Flags ------

integer*4, parameter :: KSF_SOCKET_MODE                         = #00080000

!------ USB endpoint commands ------

integer*4, parameter :: KS_USB_RESET_PORT                       = #00000004
integer*4, parameter :: KS_USB_SET_ISO_PACKET_SIZE              = #00000005
integer*4, parameter :: KS_USB_GET_PACKET_SIZE                  = #00000007
integer*4, parameter :: KS_USB_SET_PACKET_TIMEOUT               = #00000008

!------ USB device commands ------

integer*4, parameter :: KS_USB_GET_DEVICE_DESCRIPTOR            = #00020000
integer*4, parameter :: KS_USB_GET_CONFIG_DESCRIPTOR            = #00030000
integer*4, parameter :: KS_USB_GET_STRING_DESCRIPTOR            = #00040000
integer*4, parameter :: KS_USB_SELECT_CONFIG                    = #00050000
integer*4, parameter :: KS_USB_SELECT_INTERFACE                 = #00060000
integer*4, parameter :: KS_USB_GET_CONFIG                       = #00070000
integer*4, parameter :: KS_USB_GET_INTERFACE                    = #00080000
integer*4, parameter :: KS_USB_CLR_DEVICE_FEATURE               = #00090000
integer*4, parameter :: KS_USB_SET_DEVICE_FEATURE               = #000a0000
integer*4, parameter :: KS_USB_CLR_INTERFACE_FEATURE            = #000b0000
integer*4, parameter :: KS_USB_SET_INTERFACE_FEATURE            = #000c0000
integer*4, parameter :: KS_USB_CLR_ENDPOINT_FEATURE             = #000d0000
integer*4, parameter :: KS_USB_SET_ENDPOINT_FEATURE             = #000e0000
integer*4, parameter :: KS_USB_GET_STATUS_FROM_DEVICE           = #000f0000
integer*4, parameter :: KS_USB_GET_STATUS_FROM_INTERFACE        = #00100000
integer*4, parameter :: KS_USB_GET_STATUS_FROM_ENDPOINT         = #00110000
integer*4, parameter :: KS_USB_VENDOR_DEVICE_REQUEST            = #00120000
integer*4, parameter :: KS_USB_VENDOR_INTERFACE_REQUEST         = #00130000
integer*4, parameter :: KS_USB_VENDOR_ENDPOINT_REQUEST          = #00140000
integer*4, parameter :: KS_USB_CLASS_DEVICE_REQUEST             = #00150000
integer*4, parameter :: KS_USB_CLASS_INTERFACE_REQUEST          = #00160000
integer*4, parameter :: KS_USB_CLASS_ENDPOINT_REQUEST           = #00170000

!------ USB descriptor types ------

integer*4, parameter :: KS_USB_DEVICE                           = #00000001
integer*4, parameter :: KS_USB_CONFIGURATION                    = #00000002
integer*4, parameter :: KS_USB_STRING                           = #00000003
integer*4, parameter :: KS_USB_INTERFACE                        = #00000004
integer*4, parameter :: KS_USB_ENDPOINT                         = #00000005

!------ USB end point types ------

integer*4, parameter :: KS_USB_CONTROL                          = #00000000
integer*4, parameter :: KS_USB_ISOCHRONOUS                      = #00000001
integer*4, parameter :: KS_USB_BULK                             = #00000002
integer*4, parameter :: KS_USB_INTERRUPT                        = #00000003

!------ USB port events ------

integer*4, parameter :: KS_USB_SURPRISE_REMOVAL                 = (USB_BASE+#00000000)
integer*4, parameter :: KS_USB_RECV                             = (USB_BASE+#00000001)
integer*4, parameter :: KS_USB_RECV_ERROR                       = (USB_BASE+#00000002)
integer*4, parameter :: KS_USB_XMIT                             = (USB_BASE+#00000003)
integer*4, parameter :: KS_USB_XMIT_EMPTY                       = (USB_BASE+#00000004)
integer*4, parameter :: KS_USB_XMIT_ERROR                       = (USB_BASE+#00000005)
integer*4, parameter :: KS_USB_TIMEOUT                          = (USB_BASE+#00000006)

type UsbUserContext
  integer*4 ctxType
  integer*4 hUsb
  integer*4 error
end type

type UsbClassOrVendorRequest
  integer*4 request
  integer*4 value
  integer*4 index
  integer*4 pData
  integer*4 size
  integer*4 write
end type

type KS_USB_DEVICE_DESCRIPTOR
  byte bLength
  byte bDescriptorType
  integer*2 bcdUSB
  byte bDeviceClass
  byte bDeviceSubClass
  byte bDeviceProtocol
  byte bMaxPacketSize0
  integer*2 idVendor
  integer*2 idProduct
  integer*2 bcdDevice
  byte iManufacturer
  byte iProduct
  byte iSerialNumber
  byte bNumConfigurations
end type

type KS_USB_CONFIGURATION_DESCRIPTOR
  byte bLength
  byte bDescriptorType
  integer*2 wTotalLength
  byte bNumInterfaces
  byte bConfigurationValue
  byte iConfiguration
  byte bmAttributes
  byte MaxPower
end type

type KS_USB_INTERFACE_DESCRIPTOR
  byte bLength
  byte bDescriptorType
  byte bInterfaceNumber
  byte bAlternateSetting
  byte bNumEndpoints
  byte bInterfaceClass
  byte bInterfaceSubClass
  byte bInterfaceProtocol
  byte iInterface
end type

type KS_USB_ENDPOINT_DESCRIPTOR
  byte bLength
  byte bDescriptorType
  byte bEndpointAddress
  byte bmAttributes
  integer*2 wMaxPacketSize
  byte bInterval
end type

type KS_USB_STRING_DESCRIPTOR
  byte bLength
  byte bDescriptorType
  integer*2 bString(0:0)
end type

type KS_USB_COMMON_DESCRIPTOR
  byte bLength
  byte bDescriptorType
end type

type KS_USB_DEVICE_QUALIFIER_DESCRIPTOR
  byte bLength
  byte bDescriptorType
  integer*2 bcdUSB
  byte bDeviceClass
  byte bDeviceSubClass
  byte bDeviceProtocol
  byte bMaxPacketSize0
  byte bNumConfigurations
  byte bReserved
end type

type KSUsbState
  integer*4 structSize
  integer*4 xmitCount
  integer*4 xmitFree
  integer*4 xmitErrorCount
  integer*4 recvCount
  integer*4 recvAvail
  integer*4 recvErrorCount
  integer*4 packetSize
  integer*4 isoPacketSize
  integer*4 packetTimeout
end type

!------ Common functions ------

interface

integer*4 function KS_openUsb(phUsbDevice, deviceName, flags)
  !dec$ attributes stdcall, alias : '_KS_openUsb@12' :: KS_openUsb
  integer*4 phUsbDevice
  integer*4 deviceName
  integer*4 flags
end function

integer*4 function KS_openUsbEndPoint(hUsbDevice, phUsbEndPoint, endPointAddress, packetSize, packetCount, flags)
  !dec$ attributes stdcall, alias : '_KS_openUsbEndPoint@24' :: KS_openUsbEndPoint
  integer*4 hUsbDevice
  integer*4 phUsbEndPoint
  integer*4 endPointAddress
  integer*4 packetSize
  integer*4 packetCount
  integer*4 flags
end function

integer*4 function KS_closeUsb(hUsb, flags)
  !dec$ attributes stdcall, alias : '_KS_closeUsb@8' :: KS_closeUsb
  integer*4 hUsb
  integer*4 flags
end function

end interface

!------ Communication functions ------

interface

integer*4 function KS_xmitUsb(hUsb, pData, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitUsb@20' :: KS_xmitUsb
  integer*4 hUsb
  integer*4 pData
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_recvUsb(hUsb, pData, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_recvUsb@20' :: KS_recvUsb
  integer*4 hUsb
  integer*4 pData
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_installUsbHandler(hUsb, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installUsbHandler@16' :: KS_installUsbHandler
  integer*4 hUsb
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_getUsbState(hUsb, pUsbState, flags)
  !dec$ attributes stdcall, alias : '_KS_getUsbState@12' :: KS_getUsbState
  integer*4 hUsb
  integer*4 pUsbState
  integer*4 flags
end function

integer*4 function KS_execUsbCommand(hUsb, command, index, pData, size, flags)
  !dec$ attributes stdcall, alias : '_KS_execUsbCommand@24' :: KS_execUsbCommand
  integer*4 hUsb
  integer*4 command
  integer*4 index
  integer*4 pData
  integer*4 size
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! XHCI Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_XHCI_REQUEST_FAILED             = (KSERROR_CATEGORY_XHCI+#00000000)
integer*4, parameter :: KSERROR_XHCI_HOST_CONTROLLER_ERROR      = (KSERROR_CATEGORY_XHCI+#00010000)
integer*4, parameter :: KSERROR_XHCI_CONFIGURATION_ERROR        = (KSERROR_CATEGORY_XHCI+#00020000)
integer*4, parameter :: KSERROR_XHCI_INTERFACE_ERROR            = (KSERROR_CATEGORY_XHCI+#00030000)
integer*4, parameter :: KSERROR_XHCI_PACKET_DROPPED             = (KSERROR_CATEGORY_XHCI+#00040000)
integer*4, parameter :: KSERROR_XHCI_PACKET_ERROR               = (KSERROR_CATEGORY_XHCI+#00050000)

!------ XHCI endpoint commands ------

integer*4, parameter :: KS_XHCI_RESET_ENDPOINT                  = #00000001
integer*4, parameter :: KS_XHCI_GET_PACKET_SIZE                 = #00000002
integer*4, parameter :: KS_XHCI_CLEAR_RECEIVE_QUEUE             = #00000003

!------ XHCI device commands ------

integer*4, parameter :: KS_XHCI_GET_DEVICE_DESCRIPTOR           = #00010001
integer*4, parameter :: KS_XHCI_GET_CONFIG_DESCRIPTOR           = #00010002
integer*4, parameter :: KS_XHCI_GET_STRING_DESCRIPTOR           = #00010003
integer*4, parameter :: KS_XHCI_SET_CONFIG                      = #00010004
integer*4, parameter :: KS_XHCI_GET_CONFIG                      = #00010005
integer*4, parameter :: KS_XHCI_SET_INTERFACE                   = #00010006
integer*4, parameter :: KS_XHCI_GET_INTERFACE                   = #00010007

!------ XHCI controller commands ------

integer*4, parameter :: KS_XHCI_SET_POWER_ROOT_HUB              = #00020001

!------ XHCI descriptor types ------

integer*4, parameter :: KS_XHCI_DEVICE_DESCRIPTOR               = #00000001
integer*4, parameter :: KS_XHCI_CONFIGURATION_DESCRIPTOR        = #00000002
integer*4, parameter :: KS_XHCI_STRING_DESCRIPTOR               = #00000003
integer*4, parameter :: KS_XHCI_INTERFACE_DESCRIPTOR            = #00000004
integer*4, parameter :: KS_XHCI_ENDPOINT_DESCRIPTOR             = #00000005
integer*4, parameter :: KS_XHCI_INTERFACE_ASSOCIATION_DESCRIPTOR 
                                                                = #0000000b
integer*4, parameter :: KS_XHCI_ENDPOINT_COMPANION_DESCRIPTOR   = #00000030

!------ XHCI endpoint types ------

integer*4, parameter :: KS_XHCI_CONTROL_ENDPOINT                = #00000000
integer*4, parameter :: KS_XHCI_ISOCHRONOUS_ENDPOINT            = #00000001
integer*4, parameter :: KS_XHCI_BULK_ENDPOINT                   = #00000002
integer*4, parameter :: KS_XHCI_INTERRUPT_ENDPOINT              = #00000003

!------ XHCI events ------

integer*4, parameter :: KS_XHCI_ADD_DEVICE                      = (XHCI_BASE+#00000000)
integer*4, parameter :: KS_XHCI_REMOVE_DEVICE                   = (XHCI_BASE+#00000001)
integer*4, parameter :: KS_XHCI_RECV                            = (XHCI_BASE+#00000002)
integer*4, parameter :: KS_XHCI_RECV_ERROR                      = (XHCI_BASE+#00000003)
integer*4, parameter :: KS_XHCI_XMIT_LOW                        = (XHCI_BASE+#00000004)
integer*4, parameter :: KS_XHCI_XMIT_EMPTY                      = (XHCI_BASE+#00000005)
integer*4, parameter :: KS_XHCI_XMIT_ERROR                      = (XHCI_BASE+#00000006)

!------ Context structures ------

type KSXhciContext
  integer*4 ctxType
  integer*4 hXhci
end type

type KSXhciAddDeviceContext
  integer*4 ctxType
  integer*4 hXhciController
  byte deviceName(0:255)
end type

type KSXhciRecvContext
  integer*4 ctxType
  integer*4 hXhciEndPoint
  integer*4 pXhciPacketApp
  integer*4 pXhciPacketSys
  integer*4 packetSize
end type

type KSXhciErrorContext
  integer*4 ctxType
  integer*4 hXhci
  integer*4 error
end type

!------ XHCI common structures ------

type KSXhciControlRequest
  integer*4 requestType
  integer*4 request
  integer*4 value
  integer*4 index
end type

type KSXhciEndPointState
  integer*4 structSize
  integer*4 endPointType
  integer*4 packetSize
  integer*4 xmitCount
  integer*4 xmitFree
  integer*4 xmitErrorCount
  integer*4 recvCount
  integer*4 recvAvail
  integer*4 recvErrorCount
  integer*4 recvDroppedCount
end type

!------ XHCI Descriptors ------

type KSXhciCommonDescriptor
  byte bLength
  byte bDescriptorType
end type

type KSXhciDeviceDescriptor
  byte bLength
  byte bDescriptorType
  integer*2 bcdUSB
  byte bDeviceClass
  byte bDeviceSubClass
  byte bDeviceProtocol
  byte bMaxPacketSize0
  integer*2 idVendor
  integer*2 idProduct
  integer*2 bcdDevice
  byte iManufacturer
  byte iProduct
  byte iSerialNumber
  byte bNumConfigurations
end type

type KSXhciConfigurationDescriptor
  byte bLength
  byte bDescriptorType
  integer*2 wTotalLength
  byte bNumInterfaces
  byte bConfigurationValue
  byte iConfiguration
  byte bmAttributes
  byte MaxPower
end type

type KSXhciInterfaceDescriptor
  byte bLength
  byte bDescriptorType
  byte bInterfaceNumber
  byte bAlternateSetting
  byte bNumEndpoints
  byte bInterfaceClass
  byte bInterfaceSubClass
  byte bInterfaceProtocol
  byte iInterface
end type

type KSXhciInterfaceAssociationDescriptor
  byte bLength
  byte bDescriptorType
  byte bFirstInterface
  byte bNumInterfaces
  byte bFunctionClass
  byte bFunctionSubClass
  byte bFunctionProtocol
  byte iFunction
end type

type KSXhciEndpointDescriptor
  byte bLength
  byte bDescriptorType
  byte bEndpointAddress
  byte bmAttributes
  integer*2 wMaxPacketSize
  byte bInterval
end type

type KSXhciEndpointCompanionDescriptor
  byte bLength
  byte bDescriptorType
  byte bMaxBurst
  byte bmAttributes
  integer*2 wBytesPerInterval
end type

type KSXhciStringDescriptor
  byte bLength
  byte bDescriptorType
  integer*2 bString(0:0)
end type

!------ Common functions ------

interface

integer*4 function KS_openXhciController(phXhciController, deviceName, flags)
  !dec$ attributes stdcall, alias : '_KS_openXhciController@12' :: KS_openXhciController
  integer*4 phXhciController
  integer*4 deviceName
  integer*4 flags
end function

integer*4 function KS_openXhciDevice(phXhciDevice, deviceName, flags)
  !dec$ attributes stdcall, alias : '_KS_openXhciDevice@12' :: KS_openXhciDevice
  integer*4 phXhciDevice
  integer*4 deviceName
  integer*4 flags
end function

integer*4 function KS_openXhciEndPoint(hXhciDevice, phXhciEndPoint, endPointAddress, packetSize, packetCount, flags)
  !dec$ attributes stdcall, alias : '_KS_openXhciEndPoint@24' :: KS_openXhciEndPoint
  integer*4 hXhciDevice
  integer*4 phXhciEndPoint
  integer*4 endPointAddress
  integer*4 packetSize
  integer*4 packetCount
  integer*4 flags
end function

integer*4 function KS_closeXhci(hXhci, flags)
  !dec$ attributes stdcall, alias : '_KS_closeXhci@8' :: KS_closeXhci
  integer*4 hXhci
  integer*4 flags
end function

end interface

!------ Communication functions ------

interface

integer*4 function KS_requestXhciPacket(hXhciEndPoint, ppXhciPacket, flags)
  !dec$ attributes stdcall, alias : '_KS_requestXhciPacket@12' :: KS_requestXhciPacket
  integer*4 hXhciEndPoint
  integer*4 ppXhciPacket
  integer*4 flags
end function

integer*4 function KS_releaseXhciPacket(hXhciEndPoint, pXhciPacket, flags)
  !dec$ attributes stdcall, alias : '_KS_releaseXhciPacket@12' :: KS_releaseXhciPacket
  integer*4 hXhciEndPoint
  integer*4 pXhciPacket
  integer*4 flags
end function

integer*4 function KS_xmitXhciPacket(hXhciEndPoint, pXhciPacket, size, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitXhciPacket@16' :: KS_xmitXhciPacket
  integer*4 hXhciEndPoint
  integer*4 pXhciPacket
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_recvXhciPacket(hXhciEndPoint, ppXhciPacket, pSize, flags)
  !dec$ attributes stdcall, alias : '_KS_recvXhciPacket@16' :: KS_recvXhciPacket
  integer*4 hXhciEndPoint
  integer*4 ppXhciPacket
  integer*4 pSize
  integer*4 flags
end function

integer*4 function KS_sendXhciControlRequest(hXhciDevice, pRequestData, pData, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_sendXhciControlRequest@24' :: KS_sendXhciControlRequest
  integer*4 hXhciDevice
  integer*4 pRequestData
  integer*4 pData
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_installXhciHandler(hXhci, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installXhciHandler@16' :: KS_installXhciHandler
  integer*4 hXhci
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_getXhciEndPointState(hXhciEndPoint, pState, flags)
  !dec$ attributes stdcall, alias : '_KS_getXhciEndPointState@12' :: KS_getXhciEndPointState
  integer*4 hXhciEndPoint
  integer*4 pState
  integer*4 flags
end function

integer*4 function KS_execXhciCommand(hXhci, command, index, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execXhciCommand@20' :: KS_execXhciCommand
  integer*4 hXhci
  integer*4 command
  integer*4 index
  integer*4 pParam
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! RealTime Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_TIMER_NOT_INSTALLED             = (KSERROR_CATEGORY_TIMER+#00000000)
integer*4, parameter :: KSERROR_TIMER_REMOVED                   = (KSERROR_CATEGORY_TIMER+#00010000)
integer*4, parameter :: KSERROR_TIMER_DISABLED                  = (KSERROR_CATEGORY_TIMER+#00020000)
integer*4, parameter :: KSERROR_TIMER_NOT_DISABLED              = (KSERROR_CATEGORY_TIMER+#00030000)
integer*4, parameter :: KSERROR_CANNOT_INSTALL_TIMER            = (KSERROR_CATEGORY_TIMER+#00040000)
integer*4, parameter :: KSERROR_NO_TIMER_AVAILABLE              = (KSERROR_CATEGORY_TIMER+#00050000)
integer*4, parameter :: KSERROR_WAIT_CANCELLED                  = (KSERROR_CATEGORY_TIMER+#00060000)
integer*4, parameter :: KSERROR_TIMER_ALREADY_RUNNING           = (KSERROR_CATEGORY_TIMER+#00070000)
integer*4, parameter :: KSERROR_TIMER_ALREADY_STOPPED           = (KSERROR_CATEGORY_TIMER+#00080000)
integer*4, parameter :: KSERROR_CANNOT_START_TIMER              = (KSERROR_CATEGORY_TIMER+#00090000)

!------ Flags ------

integer*4, parameter :: KSF_DISTINCT_MODE                       = #00040000
integer*4, parameter :: KSF_DRIFT_CORRECTION                    = #00010000
integer*4, parameter :: KSF_DISABLE_PROTECTION                  = #00020000
integer*4, parameter :: KSF_USE_IEEE1588_TIME                   = #00040000

!------ Timer module config ------

integer*4, parameter :: KSCONFIG_SOFTWARE_TIMER                 = #00000001

!------ Timer events ------

integer*4, parameter :: KS_TIMER_CONTEXT                        = #00000200

!------ Context structures ------

type TimerUserContext
  integer*4 ctxType
  integer*4 hTimer
  integer*4 delay
end type

!------ Real-time functions ------

interface

integer*4 function KS_createTimer(phTimer, delay, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_createTimer@16' :: KS_createTimer
  integer*4 phTimer
  integer*4 delay
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_removeTimer(hTimer)
  !dec$ attributes stdcall, alias : '_KS_removeTimer@4' :: KS_removeTimer
  integer*4 hTimer
end function

integer*4 function KS_cancelTimer(hTimer)
  !dec$ attributes stdcall, alias : '_KS_cancelTimer@4' :: KS_cancelTimer
  integer*4 hTimer
end function

integer*4 function KS_startTimer(hTimer, flags, delay)
  !dec$ attributes stdcall, alias : '_KS_startTimer@12' :: KS_startTimer
  integer*4 hTimer
  integer*4 flags
  integer*4 delay
end function

integer*4 function KS_stopTimer(hTimer)
  !dec$ attributes stdcall, alias : '_KS_stopTimer@4' :: KS_stopTimer
  integer*4 hTimer
end function

integer*4 function KS_startTimerDelayed(hTimer, start, period, flags)
  !dec$ attributes stdcall, alias : '_KS_startTimerDelayed@16' :: KS_startTimerDelayed
  integer*4 hTimer
  integer*8 start
  integer*4 period
  integer*4 flags
end function

integer*4 function KS_adjustTimer(hTimer, period, flags)
  !dec$ attributes stdcall, alias : '_KS_adjustTimer@12' :: KS_adjustTimer
  integer*4 hTimer
  integer*4 period
  integer*4 flags
end function

end interface

!------ Obsolete timer functions - do not use! ------

interface

integer*4 function KS_disableTimer(hTimer)
  !dec$ attributes stdcall, alias : '_KS_disableTimer@4' :: KS_disableTimer
  integer*4 hTimer
end function

integer*4 function KS_enableTimer(hTimer)
  !dec$ attributes stdcall, alias : '_KS_enableTimer@4' :: KS_enableTimer
  integer*4 hTimer
end function

integer*4 function KS_getTimerState(hTimer, pState)
  !dec$ attributes stdcall, alias : '_KS_getTimerState@8' :: KS_getTimerState
  integer*4 hTimer
  integer*4 pState
end function

end interface

!------ Obsolete timer resolution functions - do not use! ------

interface

integer*4 function KS_setTimerResolution(resolution, flags)
  !dec$ attributes stdcall, alias : '_KS_setTimerResolution@8' :: KS_setTimerResolution
  integer*4 resolution
  integer*4 flags
end function

integer*4 function KS_getTimerResolution(pResolution, flags)
  !dec$ attributes stdcall, alias : '_KS_getTimerResolution@8' :: KS_getTimerResolution
  integer*4 pResolution
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Task Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_CPU_BOOT_FAILURE                = (KSERROR_CATEGORY_TASK+#00000000)

!------ Task states ------

integer*4, parameter :: KS_TASK_DORMANT                         = #00000001
integer*4, parameter :: KS_TASK_READY                           = #00000002
integer*4, parameter :: KS_TASK_RUNNING                         = #00000004
integer*4, parameter :: KS_TASK_WAITING                         = #00000008
integer*4, parameter :: KS_TASK_SUSPENDED                       = #00000010
integer*4, parameter :: KS_TASK_TERMINATED                      = #00000020

!------ Flags ------

integer*4, parameter :: KSF_NUM_PROCESSORS                      = #00000040
integer*4, parameter :: KSF_NON_INTERRUPTABLE                   = #00040000
integer*4, parameter :: KSF_CUSTOM_STACK_SIZE                   = #00004000

!------ Task contexts ------

integer*4, parameter :: TASK_CONTEXT                            = (TASK_BASE+#00000000)

!------ Common structure ------

type KSContextInformation
  integer*4 structSize
  integer*4 currentLevel
  integer*4 taskHandle
  integer*4 taskPriority
  integer*4 currentCpuNumber
  integer*4 stackSize
  integer*4 stackUsage
end type

!------ Context structure ------

type TaskUserContext
  integer*4 ctxType
  integer*4 hTask
  integer*4 priority
end type

type TaskErrorUserContext
  integer*4 ctxType
  integer*4 error
  integer*4 param1
  integer*4 param2
  integer*4 param3
  integer*4 param4
  integer*4 hTask
  byte kernel(0:63)
  byte functionName(0:63)
  integer*4 functionOffset
end type

!------ Task functions ------

interface

integer*4 function KS_createTask(phTask, hCallBack, priority, flags)
  !dec$ attributes stdcall, alias : '_KS_createTask@16' :: KS_createTask
  integer*4 phTask
  integer*4 hCallBack
  integer*4 priority
  integer*4 flags
end function

integer*4 function KS_removeTask(hTask)
  !dec$ attributes stdcall, alias : '_KS_removeTask@4' :: KS_removeTask
  integer*4 hTask
end function

integer*4 function KS_exitTask(hTask, exitCode)
  !dec$ attributes stdcall, alias : '_KS_exitTask@8' :: KS_exitTask
  integer*4 hTask
  integer*4 exitCode
end function

integer*4 function KS_killTask(hTask, exitCode)
  !dec$ attributes stdcall, alias : '_KS_killTask@8' :: KS_killTask
  integer*4 hTask
  integer*4 exitCode
end function

integer*4 function KS_terminateTask(hTask, timeout, flags)
  !dec$ attributes stdcall, alias : '_KS_terminateTask@12' :: KS_terminateTask
  integer*4 hTask
  integer*4 timeout
  integer*4 flags
end function

integer*4 function KS_suspendTask(hTask)
  !dec$ attributes stdcall, alias : '_KS_suspendTask@4' :: KS_suspendTask
  integer*4 hTask
end function

integer*4 function KS_resumeTask(hTask)
  !dec$ attributes stdcall, alias : '_KS_resumeTask@4' :: KS_resumeTask
  integer*4 hTask
end function

integer*4 function KS_triggerTask(hTask)
  !dec$ attributes stdcall, alias : '_KS_triggerTask@4' :: KS_triggerTask
  integer*4 hTask
end function

integer*4 function KS_yieldTask()
  !dec$ attributes stdcall, alias : '_KS_yieldTask@0' :: KS_yieldTask
end function

integer*4 function KS_sleepTask(delay)
  !dec$ attributes stdcall, alias : '_KS_sleepTask@4' :: KS_sleepTask
  integer*4 delay
end function

integer*4 function KS_setTaskPrio(hTask, newPrio, pOldPrio)
  !dec$ attributes stdcall, alias : '_KS_setTaskPrio@12' :: KS_setTaskPrio
  integer*4 hTask
  integer*4 newPrio
  integer*4 pOldPrio
end function

integer*4 function KS_setTaskPriority(hObject, priority, flags)
  !dec$ attributes stdcall, alias : '_KS_setTaskPriority@12' :: KS_setTaskPriority
  integer*4 hObject
  integer*4 priority
  integer*4 flags
end function

integer*4 function KS_setTaskPriorityRelative(hObject, hRelative, distance, flags)
  !dec$ attributes stdcall, alias : '_KS_setTaskPriorityRelative@16' :: KS_setTaskPriorityRelative
  integer*4 hObject
  integer*4 hRelative
  integer*4 distance
  integer*4 flags
end function

integer*4 function KS_getTaskPriority(hObject, pPriority, flags)
  !dec$ attributes stdcall, alias : '_KS_getTaskPriority@12' :: KS_getTaskPriority
  integer*4 hObject
  integer*4 pPriority
  integer*4 flags
end function

integer*4 function KS_getTaskState(hTask, pTaskState, pExitCode)
  !dec$ attributes stdcall, alias : '_KS_getTaskState@12' :: KS_getTaskState
  integer*4 hTask
  integer*4 pTaskState
  integer*4 pExitCode
end function

integer*4 function KS_setTaskStackSize(size, flags)
  !dec$ attributes stdcall, alias : '_KS_setTaskStackSize@8' :: KS_setTaskStackSize
  integer*4 size
  integer*4 flags
end function

end interface

!------ MultiCore & information functions ------

interface

integer*4 function KS_setTargetProcessor(processor, flags)
  !dec$ attributes stdcall, alias : '_KS_setTargetProcessor@8' :: KS_setTargetProcessor
  integer*4 processor
  integer*4 flags
end function

integer*4 function KS_getCurrentProcessor(pProcessor, flags)
  !dec$ attributes stdcall, alias : '_KS_getCurrentProcessor@8' :: KS_getCurrentProcessor
  integer*4 pProcessor
  integer*4 flags
end function

integer*4 function KS_getCurrentContext(pContextInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_getCurrentContext@8' :: KS_getCurrentContext
  integer*4 pContextInfo
  integer*4 flags
end function

end interface

!------ Semaphore functions ------

interface

integer*4 function KS_createSemaphore(phSemaphore, maxCount, initCount, flags)
  !dec$ attributes stdcall, alias : '_KS_createSemaphore@16' :: KS_createSemaphore
  integer*4 phSemaphore
  integer*4 maxCount
  integer*4 initCount
  integer*4 flags
end function

integer*4 function KS_removeSemaphore(hSemaphore)
  !dec$ attributes stdcall, alias : '_KS_removeSemaphore@4' :: KS_removeSemaphore
  integer*4 hSemaphore
end function

integer*4 function KS_requestSemaphore(hSemaphore, timeout)
  !dec$ attributes stdcall, alias : '_KS_requestSemaphore@8' :: KS_requestSemaphore
  integer*4 hSemaphore
  integer*4 timeout
end function

integer*4 function KS_releaseSemaphore(hSemaphore)
  !dec$ attributes stdcall, alias : '_KS_releaseSemaphore@4' :: KS_releaseSemaphore
  integer*4 hSemaphore
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Dedicated Module
!---------------------------------------------------------------------------------------------------------------

!------ Dedicated functions ------

interface

end interface

!---------------------------------------------------------------------------------------------------------------
! Packet Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_INIT_PHY_ERROR                  = (KSERROR_CATEGORY_PACKET+#00000000)
integer*4, parameter :: KSERROR_INIT_MAC_ERROR                  = (KSERROR_CATEGORY_PACKET+#00010000)
integer*4, parameter :: KSERROR_MAC_STARTUP_ERROR               = (KSERROR_CATEGORY_PACKET+#00020000)

!------ Flags ------

integer*4, parameter :: KSF_SUPPORT_JUMBO_FRAMES                = #00008000
integer*4, parameter :: KSF_DELAY_RECEIVE_INTERRUPT             = #00080000
integer*4, parameter :: KSF_NO_RECEIVE_INTERRUPT                = #00000020
integer*4, parameter :: KSF_NO_TRANSMIT_INTERRUPT               = #00000040
integer*4, parameter :: KSF_RAW_ETHERNET_PACKET                 = #00100000
integer*4, parameter :: KSF_SAFE_START                          = #00200000
integer*4, parameter :: KSF_CHECK_IP_CHECKSUM                   = #00400000
integer*4, parameter :: KSF_ACCEPT_ALL                          = #00800000
integer*4, parameter :: KSF_FORCE_MASTER                        = #00000001
integer*4, parameter :: KSF_FORCE_100_MBIT                      = #00000002
integer*4, parameter :: KSF_WINDOWS_CONNECTION                  = #00004000

!------ Packet events ------

integer*4, parameter :: KS_PACKET_RECV                          = (PACKET_BASE+#00000000)
integer*4, parameter :: KS_PACKET_LINK_CHANGE                   = (PACKET_BASE+#00000001)
integer*4, parameter :: KS_PACKET_SEND_EMPTY                    = (PACKET_BASE+#00000002)
integer*4, parameter :: KS_PACKET_SEND_LOW                      = (PACKET_BASE+#00000003)

!------ Adapter commands ------

integer*4, parameter :: KS_PACKET_ABORT_SEND                    = #00000001
integer*4, parameter :: KS_PACKET_ADD_ARP_TABLE_ENTRY           = #00000002
integer*4, parameter :: KS_PACKET_SET_IP_CONFIG                 = #00000003
integer*4, parameter :: KS_PACKET_SET_MAC_MULTICAST             = #00000004
integer*4, parameter :: KS_PACKET_SET_IP_MULTICAST              = #00000005
integer*4, parameter :: KS_PACKET_SET_MAC_CONFIG                = #00000006
integer*4, parameter :: KS_PACKET_CLEAR_RECEIVE_QUEUE           = #00000007
integer*4, parameter :: KS_PACKET_GET_IP_CONFIG                 = #00000008
integer*4, parameter :: KS_PACKET_GET_MAC_CONFIG                = #00000009
integer*4, parameter :: KS_PACKET_GET_MAXIMUM_SIZE              = #0000000a

!------ Connection flags ------

integer*4, parameter :: KS_PACKET_SPEED_10_MBIT                 = #00010000
integer*4, parameter :: KS_PACKET_SPEED_100_MBIT                = #00020000
integer*4, parameter :: KS_PACKET_SPEED_1000_MBIT               = #00040000
integer*4, parameter :: KS_PACKET_SPEED_10_GBIT                 = #00080000
integer*4, parameter :: KS_PACKET_DUPLEX_FULL                   = #00000001
integer*4, parameter :: KS_PACKET_DUPLEX_HALF                   = #00000002
integer*4, parameter :: KS_PACKET_MASTER                        = #00000004
integer*4, parameter :: KS_PACKET_SLAVE                         = #00000008

type PacketUserContext
  integer*4 ctxType
  integer*4 hAdapter
  integer*4 pPacketApp
  integer*4 pPacketSys
  integer*4 length
end type

type PacketAdapterUserContext
  integer*4 ctxType
  integer*4 hAdapter
end type

type PacketLinkChangeUserContext
  integer*4 ctxType
  integer*4 hAdapter
  integer*4 connection
end type

type KSMACHeader
  byte destination(0:5)
  byte source(0:5)
  integer*2 typeOrLength
end type

type KSIPHeader
  integer*4 header1
  integer*4 header2
  integer*4 header3
  integer*4 srcIpAddress
  integer*4 dstIpAddress
  integer*4 header6
end type

type KSUDPHeader
  integer*2 srcPort
  integer*2 dstPort
  integer*2 msgLength
  integer*2 checkSum
end type

type KSIPConfig
  integer*4 localAddress
  integer*4 subnetMask
  integer*4 gatewayAddress
end type

type KSARPTableEntry
  integer*4 ipAddress
  byte macAddress(0:5)
end type

type KSAdapterState
  integer*4 structSize
  integer*4 numPacketsSendOk
  integer*4 numPacketsSendError
  integer*4 numPacketsSendARPTimeout
  integer*4 numPacketsWaitingForARP
  integer*4 numPacketsRecvOk
  integer*4 numPacketsRecvError
  integer*4 numPacketsRecvDropped
  integer*4 connection
  byte macAddress(0:5)
  byte connected
end type

!------ Helper functions ------

interface

end interface

!------ Common functions ------

interface

integer*4 function KS_openAdapter(phAdapter, name, recvPoolLength, sendPoolLength, flags)
  !dec$ attributes stdcall, alias : '_KS_openAdapter@20' :: KS_openAdapter
  integer*4 phAdapter
  integer*4 name
  integer*4 recvPoolLength
  integer*4 sendPoolLength
  integer*4 flags
end function

integer*4 function KS_openAdapterEx(phAdapter, hConnection, recvPoolLength, sendPoolLength, flags)
  !dec$ attributes stdcall, alias : '_KS_openAdapterEx@20' :: KS_openAdapterEx
  integer*4 phAdapter
  integer*4 hConnection
  integer*4 recvPoolLength
  integer*4 sendPoolLength
  integer*4 flags
end function

integer*4 function KS_closeAdapter(hAdapter)
  !dec$ attributes stdcall, alias : '_KS_closeAdapter@4' :: KS_closeAdapter
  integer*4 hAdapter
end function

integer*4 function KS_getAdapterState(hAdapter, pAdapterState, flags)
  !dec$ attributes stdcall, alias : '_KS_getAdapterState@12' :: KS_getAdapterState
  integer*4 hAdapter
  integer*4 pAdapterState
  integer*4 flags
end function

integer*4 function KS_execAdapterCommand(hAdapter, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execAdapterCommand@16' :: KS_execAdapterCommand
  integer*4 hAdapter
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

integer*4 function KS_aggregateAdapter(hAdapter, hLink, flags)
  !dec$ attributes stdcall, alias : '_KS_aggregateAdapter@12' :: KS_aggregateAdapter
  integer*4 hAdapter
  integer*4 hLink
  integer*4 flags
end function

end interface

!------ Communication functions ------

interface

integer*4 function KS_requestPacket(hAdapter, ppPacket, flags)
  !dec$ attributes stdcall, alias : '_KS_requestPacket@12' :: KS_requestPacket
  integer*4 hAdapter
  integer*4 ppPacket
  integer*4 flags
end function

integer*4 function KS_releasePacket(hAdapter, pPacket, flags)
  !dec$ attributes stdcall, alias : '_KS_releasePacket@12' :: KS_releasePacket
  integer*4 hAdapter
  integer*4 pPacket
  integer*4 flags
end function

integer*4 function KS_sendPacket(hAdapter, pPacket, size, flags)
  !dec$ attributes stdcall, alias : '_KS_sendPacket@16' :: KS_sendPacket
  integer*4 hAdapter
  integer*4 pPacket
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_recvPacket(hAdapter, ppPacket, flags)
  !dec$ attributes stdcall, alias : '_KS_recvPacket@12' :: KS_recvPacket
  integer*4 hAdapter
  integer*4 ppPacket
  integer*4 flags
end function

integer*4 function KS_recvPacketEx(hAdapter, ppPacket, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_recvPacketEx@16' :: KS_recvPacketEx
  integer*4 hAdapter
  integer*4 ppPacket
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_installPacketHandler(hAdapter, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installPacketHandler@16' :: KS_installPacketHandler
  integer*4 hAdapter
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Socket Module
!---------------------------------------------------------------------------------------------------------------

!------ Flags ------

integer*4, parameter :: KSF_CLIENT                              = #00000020
integer*4, parameter :: KSF_SERVER                              = #00000040
integer*4, parameter :: KSF_ACCEPT_BROADCAST                    = #00800000
integer*4, parameter :: KSF_INSERT_CHECKSUM                     = #00200000
integer*4, parameter :: KSF_CHECK_CHECKSUM                      = #00400000

!------ Family types ------

integer*4, parameter :: KS_FAMILY_IPV4                          = #00000004

!------ Protocol types ------

integer*4, parameter :: KS_PROTOCOL_RAW                         = #00000000
integer*4, parameter :: KS_PROTOCOL_UDP                         = #00000001
integer*4, parameter :: KS_PROTOCOL_TCP                         = #00000002

!------ Socket events ------

integer*4, parameter :: KS_SOCKET_CONNECTED                     = (SOCKET_BASE+#00000000)
integer*4, parameter :: KS_SOCKET_DISCONNECTED                  = (SOCKET_BASE+#00000001)
integer*4, parameter :: KS_SOCKET_RECV                          = (SOCKET_BASE+#00000002)
integer*4, parameter :: KS_SOCKET_CLEAR_TO_SEND                 = (SOCKET_BASE+#00000003)
integer*4, parameter :: KS_SOCKET_SEND_EMPTY                    = (SOCKET_BASE+#00000004)
integer*4, parameter :: KS_SOCKET_SEND_LOW                      = (SOCKET_BASE+#00000005)

!------ Socket commands ------

integer*4, parameter :: KS_SOCKET_SET_MTU                       = #00000001

type KSSocketAddr
  integer*2 family
  integer*2 port
  integer*4 address(0:0)
end type

type SocketUserContext
  integer*4 ctxType
  integer*4 hSocket
end type

!------ Socket functions ------

interface

integer*4 function KS_openSocket(phSocket, hAdapter, pSocketAddr, protocol, flags)
  !dec$ attributes stdcall, alias : '_KS_openSocket@20' :: KS_openSocket
  integer*4 phSocket
  integer*4 hAdapter
  integer*4 pSocketAddr
  integer*4 protocol
  integer*4 flags
end function

integer*4 function KS_closeSocket(hSocket)
  !dec$ attributes stdcall, alias : '_KS_closeSocket@4' :: KS_closeSocket
  integer*4 hSocket
end function

integer*4 function KS_recvFromSocket(hSocket, pSourceAddr, pBuffer, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_recvFromSocket@24' :: KS_recvFromSocket
  integer*4 hSocket
  integer*4 pSourceAddr
  integer*4 pBuffer
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_sendToSocket(hSocket, pTargetAddr, pBuffer, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_sendToSocket@24' :: KS_sendToSocket
  integer*4 hSocket
  integer*4 pTargetAddr
  integer*4 pBuffer
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_recvSocket(hSocket, pBuffer, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_recvSocket@20' :: KS_recvSocket
  integer*4 hSocket
  integer*4 pBuffer
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_sendSocket(hSocket, pBuffer, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_sendSocket@20' :: KS_sendSocket
  integer*4 hSocket
  integer*4 pBuffer
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_installSocketHandler(hSocket, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installSocketHandler@16' :: KS_installSocketHandler
  integer*4 hSocket
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_connectSocket(hClient, pServerAddr, flags)
  !dec$ attributes stdcall, alias : '_KS_connectSocket@12' :: KS_connectSocket
  integer*4 hClient
  integer*4 pServerAddr
  integer*4 flags
end function

integer*4 function KS_acceptSocket(hServer, pClientAddr, flags)
  !dec$ attributes stdcall, alias : '_KS_acceptSocket@12' :: KS_acceptSocket
  integer*4 hServer
  integer*4 pClientAddr
  integer*4 flags
end function

integer*4 function KS_execSocketCommand(hSocket, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execSocketCommand@16' :: KS_execSocketCommand
  integer*4 hSocket
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! EtherCAT Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_EMERGENCY_REQUEST               = (KSERROR_CATEGORY_ETHERCAT+#00000000)
integer*4, parameter :: KSERROR_LINK_CHANGE                     = (KSERROR_CATEGORY_ETHERCAT+#00010000)
integer*4, parameter :: KSERROR_TOPOLOGY_CHANGE                 = (KSERROR_CATEGORY_ETHERCAT+#00020000)
integer*4, parameter :: KSERROR_SLAVE_ERROR                     = (KSERROR_CATEGORY_ETHERCAT+#00030000)
integer*4, parameter :: KSERROR_DATA_INCOMPLETE                 = (KSERROR_CATEGORY_ETHERCAT+#00040000)
integer*4, parameter :: KSERROR_PHYSICAL_ACCESS_FAILED          = (KSERROR_CATEGORY_ETHERCAT+#00050000)
integer*4, parameter :: KSERROR_FMMU_ERROR                      = (KSERROR_CATEGORY_ETHERCAT+#00060000)
integer*4, parameter :: KSERROR_DATA_LINK_ERROR                 = (KSERROR_CATEGORY_ETHERCAT+#00070000)
integer*4, parameter :: KSERROR_SII_ERROR                       = (KSERROR_CATEGORY_ETHERCAT+#00080000)
integer*4, parameter :: KSERROR_MAILBOX_ERROR                   = (KSERROR_CATEGORY_ETHERCAT+#00090000)
integer*4, parameter :: KSERROR_APPLICATION_LAYER_ERROR         = (KSERROR_CATEGORY_ETHERCAT+#000a0000)
integer*4, parameter :: KSERROR_SYNC_MANAGER_ERROR              = (KSERROR_CATEGORY_ETHERCAT+#000b0000)
integer*4, parameter :: KSERROR_DC_CONTROL_ERROR                = (KSERROR_CATEGORY_ETHERCAT+#000c0000)
integer*4, parameter :: KSERROR_SOE_REQUEST_ERROR               = (KSERROR_CATEGORY_ETHERCAT+#000e0000)
integer*4, parameter :: KSERROR_MISSING_XML_INFORMATION         = (KSERROR_CATEGORY_ETHERCAT+#000f0000)
integer*4, parameter :: KSERROR_SAFETY_ERROR                    = (KSERROR_CATEGORY_ETHERCAT+#00100000)
integer*4, parameter :: KSERROR_DC_CONFIGURATION_ERROR          = (KSERROR_CATEGORY_ETHERCAT+#00110000)
integer*4, parameter :: KSERROR_COE_ERROR                       = (KSERROR_CATEGORY_ETHERCAT+#00120000)

!------ Flags ------

integer*4, parameter :: KSF_FORCE_UPDATE                        = #00000008
integer*4, parameter :: KSF_SECONDARY                           = #00000020
integer*4, parameter :: KSF_SYNC_DC_GLOBAL                      = #00000040
integer*4, parameter :: KSF_SYNC_DC_LOCAL                       = #00800000
integer*4, parameter :: KSF_BIG_DATASET                         = #00004000
integer*4, parameter :: KSF_STARTUP_WITHOUT_INIT_STATE          = #00008000
integer*4, parameter :: KSF_COMPLETE_ACCESS                     = #00200000
integer*4, parameter :: KSF_IGNORE_INIT_CMDS                    = #01000000
integer*4, parameter :: KSF_STRICT_XML_CHECK                    = #00000040

!------ DataObj type flags ------

integer*4, parameter :: KS_DATAOBJ_PDO_TYPE                     = #00000001
integer*4, parameter :: KS_DATAOBJ_SDO_TYPE                     = #00000002
integer*4, parameter :: KS_DATAOBJ_IDN_TYPE                     = #00000003
integer*4, parameter :: KS_DATAOBJ_ACTIVE                       = #00040000
integer*4, parameter :: KS_DATAOBJ_MAPPABLE                     = #00080000
integer*4, parameter :: KS_DATAOBJ_SAFETY                       = #00100000

!------ State type ------

integer*4, parameter :: KS_ECAT_STATE_INIT                      = #00000001
integer*4, parameter :: KS_ECAT_STATE_PREOP                     = #00000002
integer*4, parameter :: KS_ECAT_STATE_BOOT                      = #00000003
integer*4, parameter :: KS_ECAT_STATE_SAFEOP                    = #00000004
integer*4, parameter :: KS_ECAT_STATE_OP                        = #00000008

!------ State type (legacy, do not use anymore) ------

integer*4, parameter :: KS_STATE_INIT                           = #00000001
integer*4, parameter :: KS_STATE_PREOP                          = #00000002
integer*4, parameter :: KS_STATE_BOOT                           = #00000003
integer*4, parameter :: KS_STATE_SAFEOP                         = #00000004
integer*4, parameter :: KS_STATE_OP                             = #00000008

!------ Sync manager index commons ------

integer*4, parameter :: KS_ECAT_SYNC_ALL                        = #ffffffff
integer*4, parameter :: KS_ECAT_SYNC_INPUT                      = #fffffffe
integer*4, parameter :: KS_ECAT_SYNC_OUTPUT                     = #fffffffd

!------ EtherCAT events (legacy, do not use anymore) ------

integer*4, parameter :: KS_ETHERCAT_ERROR                       = (ETHERCAT_BASE+#00000000)
integer*4, parameter :: KS_DATASET_SIGNAL                       = (ETHERCAT_BASE+#00000001)
integer*4, parameter :: KS_TOPOLOGY_CHANGE                      = (ETHERCAT_BASE+#00000002)

!------ EtherCAT events ------

integer*4, parameter :: KS_ECAT_ERROR                           = (ETHERCAT_BASE+#00000000)
integer*4, parameter :: KS_ECAT_DATASET_SIGNAL                  = (ETHERCAT_BASE+#00000001)
integer*4, parameter :: KS_ECAT_TOPOLOGY_CHANGE                 = (ETHERCAT_BASE+#00000002)
integer*4, parameter :: KS_ECAT_STATE_CHANGE                    = (ETHERCAT_BASE+#00000003)
integer*4, parameter :: KS_ECAT_OBJECT_ACCESS                   = (ETHERCAT_BASE+#00000004)
integer*4, parameter :: KS_ECAT_FILE_ACCESS                     = (ETHERCAT_BASE+#00000005)
integer*4, parameter :: KS_ECAT_PROCESS_DATA_ACCESS             = (ETHERCAT_BASE+#00000006)
integer*4, parameter :: KS_ECAT_REDUNDANCY_EFFECTIVE            = (ETHERCAT_BASE+#00000007)

!------ Topology change reasons ------

integer*4, parameter :: KS_ECAT_TOPOLOGY_MASTER_CONNECTED       = #00000000
integer*4, parameter :: KS_ECAT_TOPOLOGY_MASTER_DISCONNECTED    = #00000001
integer*4, parameter :: KS_ECAT_TOPOLOGY_SLAVE_COUNT_CHANGED    = #00000002
integer*4, parameter :: KS_ECAT_TOPOLOGY_SLAVE_ONLINE           = #00000003
integer*4, parameter :: KS_ECAT_TOPOLOGY_SLAVE_OFFLINE          = #00000004

!------ DataSet commands ------

integer*4, parameter :: KS_ECAT_GET_BUFFER_SIZE                 = #00000001

!------ Slave commands ------

integer*4, parameter :: KS_ECAT_READ_SYNC0_STATUS               = #00000001
integer*4, parameter :: KS_ECAT_READ_SYNC1_STATUS               = #00000002
integer*4, parameter :: KS_ECAT_READ_LATCH0_STATUS              = #00000003
integer*4, parameter :: KS_ECAT_READ_LATCH1_STATUS              = #00000004
integer*4, parameter :: KS_ECAT_READ_LATCH_TIMESTAMPS           = #00000005
integer*4, parameter :: KS_ECAT_ENABLE_CYCLIC_MAILBOX_CHECK     = #00000006
integer*4, parameter :: KS_ECAT_GET_PDO_OVERSAMPLING_FACTOR     = #00000007
integer*4, parameter :: KS_ECAT_SET_SYNCMANAGER_WATCHDOGS       = #00000008
integer*4, parameter :: KS_ECAT_GET_SYNCMANAGER_WATCHDOGS       = #00000009
integer*4, parameter :: KS_ECAT_HAS_XML_INFORMATION             = #0000000a

!------ Slave Device / EAP-Node commands ------

integer*4, parameter :: KS_ECAT_SIGNAL_AL_ERROR                 = #00000001

!------ EtherCAT data structures ------

type KSEcatMasterState
  integer*4 structSize
  integer*4 connected
  integer*4 slavesOnline
  integer*4 slavesCreated
  integer*4 slavesCreatedAndOnline
  integer*4 masterState
  integer*4 lastError
  integer*4 redundancyEffective
  integer*4 framesSent
  integer*4 framesLost
end type

type KSEcatSlaveState
  integer*4 structSize
  integer*4 online
  integer*4 created
  integer*4 vendor
  integer*4 product
  integer*4 revision
  integer*4 id
  integer*4 relPosition
  integer*4 absPosition
  integer*4 linkState
  integer*4 slaveState
  integer*4 statusCode
end type

type KSEcatDataVarInfo
  integer*4 objType
  integer*4 name
  integer*4 dataType
  integer*4 bitLength
  integer*4 subIndex
end type

type KSEcatDataObjInfo
  integer*4 objType
  integer*4 name
  integer*4 bitLength
  integer*4 index
  integer*4 syncIndex
  integer*4 varCount
  integer*4 vars(0:0)
end type

type KSEcatSlaveInfo
  integer*4 vendorId
  integer*4 productId
  integer*4 revision
  integer*4 serial
  integer*4 group
  integer*4 image
  integer*4 order
  integer*4 name
  integer*4 objCount
  integer*4 objs(0:0)
end type

type KSEcatDcParams
  integer*2 assignActivate
  byte latch0Control
  byte latch1Control
  integer*4 pulseLength
  integer*4 cycleTimeSync0
  integer*4 cycleTimeSync0Factor
  integer*4 shiftTimeSync0
  integer*4 shiftTimeSync0Factor
  integer*4 cycleTimeSync1
  integer*4 cycleTimeSync1Factor
  integer*4 shiftTimeSync1
  integer*4 shiftTimeSync1Factor
end type

type KSEcatDcLatchTimeStamps
  integer*8 latch0PositivEdge
  integer*8 latch0NegativEdge
  integer*8 latch1PositivEdge
  integer*8 latch1NegativEdge
  integer*8 smBufferChange
  integer*8 smPdiBufferStart
  integer*8 smPdiBufferChange
end type

type KSEcatEapNodeState
  integer*4 structSize
  integer*4 connected
  integer*4 proxiesOnline
  integer*4 proxiesCreated
  integer*4 proxiesCreatedAndOnline
  integer*4 nodeState
  integer*4 statusCode
end type

type KSEcatEapProxyState
  integer*4 structSize
  integer*4 online
  integer*4 created
  integer*4 ip
  integer*4 proxyState
  integer*4 statusCode
end type

type KSEcatEapNodeId
  integer*4 structSize
  integer*4 ip
end type

type KSEcatSlaveDeviceState
  integer*4 structSize
  integer*4 slaveState
  integer*4 statusCode
end type

!------ Context structures ------

type EcatErrorUserContext
  integer*4 ctxType
  integer*4 hMaster
  integer*4 hSlave
  integer*4 error
end type

type EcatDataSetUserContext
  integer*4 ctxType
  integer*4 hDataSet
end type

type EcatTopologyUserContext
  integer*4 ctxType
  integer*4 hMaster
  integer*4 slavesOnline
  integer*4 slavesCreated
  integer*4 slavesCreatedAndOnline
  integer*4 reason
  integer*4 hSlave
end type

type EcatRedundancyUserContext
  integer*4 ctxType
  integer*4 hMaster
  integer*4 redundancyEffective
end type

type EcatStateChangeUserContext
  integer*4 ctxType
  integer*4 hObject
  integer*4 currentState
  integer*4 requestedState
end type

type EcatObjectAccessUserContext
  integer*4 ctxType
  integer*4 hObject
  integer*4 index
  integer*4 subIndex
  integer*4 writeAccess
  integer*4 completeAccess
end type

type EcatFileAccessUserContext
  integer*4 ctxType
  integer*4 hObject
  byte fileName(0:255)
  integer*4 password
  integer*4 writeAccess
end type

type EcatProcessDataAccessUserContext
  integer*4 ctxType
  integer*4 hObject
end type

!------ Master functions ------

interface

integer*4 function KS_createEcatMaster(phMaster, hConnection, libraryPath, topologyFile, flags)
  !dec$ attributes stdcall, alias : '_KS_createEcatMaster@20' :: KS_createEcatMaster
  integer*4 phMaster
  integer*4 hConnection
  integer*4 libraryPath
  integer*4 topologyFile
  integer*4 flags
end function

integer*4 function KS_closeEcatMaster(hMaster)
  !dec$ attributes stdcall, alias : '_KS_closeEcatMaster@4' :: KS_closeEcatMaster
  integer*4 hMaster
end function

integer*4 function KS_installEcatMasterHandler(hMaster, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installEcatMasterHandler@16' :: KS_installEcatMasterHandler
  integer*4 hMaster
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_queryEcatMasterState(hMaster, pMasterState, flags)
  !dec$ attributes stdcall, alias : '_KS_queryEcatMasterState@12' :: KS_queryEcatMasterState
  integer*4 hMaster
  integer*4 pMasterState
  integer*4 flags
end function

integer*4 function KS_changeEcatMasterState(hMaster, state, flags)
  !dec$ attributes stdcall, alias : '_KS_changeEcatMasterState@12' :: KS_changeEcatMasterState
  integer*4 hMaster
  integer*4 state
  integer*4 flags
end function

integer*4 function KS_addEcatRedundancy(hMaster, hConnection, flags)
  !dec$ attributes stdcall, alias : '_KS_addEcatRedundancy@12' :: KS_addEcatRedundancy
  integer*4 hMaster
  integer*4 hConnection
  integer*4 flags
end function

end interface

!------ Slave functions ------

interface

integer*4 function KS_createEcatSlave(hMaster, phSlave, id, position, vendor, product, revision, flags)
  !dec$ attributes stdcall, alias : '_KS_createEcatSlave@32' :: KS_createEcatSlave
  integer*4 hMaster
  integer*4 phSlave
  integer*4 id
  integer*4 position
  integer*4 vendor
  integer*4 product
  integer*4 revision
  integer*4 flags
end function

integer*4 function KS_createEcatSlaveIndirect(hMaster, phSlave, pSlaveState, flags)
  !dec$ attributes stdcall, alias : '_KS_createEcatSlaveIndirect@16' :: KS_createEcatSlaveIndirect
  integer*4 hMaster
  integer*4 phSlave
  integer*4 pSlaveState
  integer*4 flags
end function

integer*4 function KS_enumEcatSlaves(hMaster, index, pSlaveState, flags)
  !dec$ attributes stdcall, alias : '_KS_enumEcatSlaves@16' :: KS_enumEcatSlaves
  integer*4 hMaster
  integer*4 index
  integer*4 pSlaveState
  integer*4 flags
end function

integer*4 function KS_deleteEcatSlave(hSlave)
  !dec$ attributes stdcall, alias : '_KS_deleteEcatSlave@4' :: KS_deleteEcatSlave
  integer*4 hSlave
end function

integer*4 function KS_writeEcatSlaveId(hSlave, id, flags)
  !dec$ attributes stdcall, alias : '_KS_writeEcatSlaveId@12' :: KS_writeEcatSlaveId
  integer*4 hSlave
  integer*4 id
  integer*4 flags
end function

integer*4 function KS_queryEcatSlaveState(hSlave, pSlaveState, flags)
  !dec$ attributes stdcall, alias : '_KS_queryEcatSlaveState@12' :: KS_queryEcatSlaveState
  integer*4 hSlave
  integer*4 pSlaveState
  integer*4 flags
end function

integer*4 function KS_changeEcatSlaveState(hSlave, state, flags)
  !dec$ attributes stdcall, alias : '_KS_changeEcatSlaveState@12' :: KS_changeEcatSlaveState
  integer*4 hSlave
  integer*4 state
  integer*4 flags
end function

integer*4 function KS_queryEcatSlaveInfo(hSlave, ppSlaveInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_queryEcatSlaveInfo@12' :: KS_queryEcatSlaveInfo
  integer*4 hSlave
  integer*4 ppSlaveInfo
  integer*4 flags
end function

integer*4 function KS_queryEcatDataObjInfo(hSlave, objIndex, ppDataObjInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_queryEcatDataObjInfo@16' :: KS_queryEcatDataObjInfo
  integer*4 hSlave
  integer*4 objIndex
  integer*4 ppDataObjInfo
  integer*4 flags
end function

integer*4 function KS_queryEcatDataVarInfo(hSlave, objIndex, varIndex, ppDataVarInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_queryEcatDataVarInfo@20' :: KS_queryEcatDataVarInfo
  integer*4 hSlave
  integer*4 objIndex
  integer*4 varIndex
  integer*4 ppDataVarInfo
  integer*4 flags
end function

integer*4 function KS_enumEcatDataObjInfo(hSlave, objEnumIndex, ppDataObjInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_enumEcatDataObjInfo@16' :: KS_enumEcatDataObjInfo
  integer*4 hSlave
  integer*4 objEnumIndex
  integer*4 ppDataObjInfo
  integer*4 flags
end function

integer*4 function KS_enumEcatDataVarInfo(hSlave, objEnumIndex, varEnumIndex, ppDataVarInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_enumEcatDataVarInfo@20' :: KS_enumEcatDataVarInfo
  integer*4 hSlave
  integer*4 objEnumIndex
  integer*4 varEnumIndex
  integer*4 ppDataVarInfo
  integer*4 flags
end function

integer*4 function KS_loadEcatInitCommands(hSlave, filename, flags)
  !dec$ attributes stdcall, alias : '_KS_loadEcatInitCommands@12' :: KS_loadEcatInitCommands
  integer*4 hSlave
  integer*4 filename
  integer*4 flags
end function

end interface

!------ DataSet related functions ------

interface

integer*4 function KS_createEcatDataSet(hObject, phSet, ppAppPtr, ppSysPtr, flags)
  !dec$ attributes stdcall, alias : '_KS_createEcatDataSet@20' :: KS_createEcatDataSet
  integer*4 hObject
  integer*4 phSet
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 flags
end function

integer*4 function KS_deleteEcatDataSet(hSet)
  !dec$ attributes stdcall, alias : '_KS_deleteEcatDataSet@4' :: KS_deleteEcatDataSet
  integer*4 hSet
end function

integer*4 function KS_assignEcatDataSet(hSet, hObject, syncIndex, placement, flags)
  !dec$ attributes stdcall, alias : '_KS_assignEcatDataSet@20' :: KS_assignEcatDataSet
  integer*4 hSet
  integer*4 hObject
  integer*4 syncIndex
  integer*4 placement
  integer*4 flags
end function

integer*4 function KS_readEcatDataSet(hSet, flags)
  !dec$ attributes stdcall, alias : '_KS_readEcatDataSet@8' :: KS_readEcatDataSet
  integer*4 hSet
  integer*4 flags
end function

integer*4 function KS_postEcatDataSet(hSet, flags)
  !dec$ attributes stdcall, alias : '_KS_postEcatDataSet@8' :: KS_postEcatDataSet
  integer*4 hSet
  integer*4 flags
end function

integer*4 function KS_installEcatDataSetHandler(hSet, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installEcatDataSetHandler@16' :: KS_installEcatDataSetHandler
  integer*4 hSet
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

end interface

!------ Common functions ------

interface

integer*4 function KS_changeEcatState(hObject, state, flags)
  !dec$ attributes stdcall, alias : '_KS_changeEcatState@12' :: KS_changeEcatState
  integer*4 hObject
  integer*4 state
  integer*4 flags
end function

integer*4 function KS_installEcatHandler(hObject, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installEcatHandler@16' :: KS_installEcatHandler
  integer*4 hObject
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

end interface

!------ Data exchange functions ------

interface

integer*4 function KS_getEcatDataObjAddress(hSet, hObject, pdoIndex, varIndex, ppAppPtr, ppSysPtr, pBitOffset, pBitLength, flags)
  !dec$ attributes stdcall, alias : '_KS_getEcatDataObjAddress@36' :: KS_getEcatDataObjAddress
  integer*4 hSet
  integer*4 hObject
  integer*4 pdoIndex
  integer*4 varIndex
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 pBitOffset
  integer*4 pBitLength
  integer*4 flags
end function

integer*4 function KS_readEcatDataObj(hObject, objIndex, varIndex, pData, pSize, flags)
  !dec$ attributes stdcall, alias : '_KS_readEcatDataObj@24' :: KS_readEcatDataObj
  integer*4 hObject
  integer*4 objIndex
  integer*4 varIndex
  integer*4 pData
  integer*4 pSize
  integer*4 flags
end function

integer*4 function KS_postEcatDataObj(hObject, objIndex, varIndex, pData, size, flags)
  !dec$ attributes stdcall, alias : '_KS_postEcatDataObj@24' :: KS_postEcatDataObj
  integer*4 hObject
  integer*4 objIndex
  integer*4 varIndex
  integer*4 pData
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_setEcatPdoAssign(hObject, syncIndex, pdoIndex, flags)
  !dec$ attributes stdcall, alias : '_KS_setEcatPdoAssign@16' :: KS_setEcatPdoAssign
  integer*4 hObject
  integer*4 syncIndex
  integer*4 pdoIndex
  integer*4 flags
end function

integer*4 function KS_setEcatPdoMapping(hObject, pdoIndex, objIndex, varIndex, bitLength, flags)
  !dec$ attributes stdcall, alias : '_KS_setEcatPdoMapping@24' :: KS_setEcatPdoMapping
  integer*4 hObject
  integer*4 pdoIndex
  integer*4 objIndex
  integer*4 varIndex
  integer*4 bitLength
  integer*4 flags
end function

integer*4 function KS_uploadEcatFile(hObject, fileName, password, pData, pSize, flags)
  !dec$ attributes stdcall, alias : '_KS_uploadEcatFile@24' :: KS_uploadEcatFile
  integer*4 hObject
  integer*4 fileName
  integer*4 password
  integer*4 pData
  integer*4 pSize
  integer*4 flags
end function

integer*4 function KS_downloadEcatFile(hObject, fileName, password, pData, size, flags)
  !dec$ attributes stdcall, alias : '_KS_downloadEcatFile@24' :: KS_downloadEcatFile
  integer*4 hObject
  integer*4 fileName
  integer*4 password
  integer*4 pData
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_readEcatSlaveMem(hSlave, address, pData, size, flags)
  !dec$ attributes stdcall, alias : '_KS_readEcatSlaveMem@20' :: KS_readEcatSlaveMem
  integer*4 hSlave
  integer*4 address
  integer*4 pData
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_writeEcatSlaveMem(hSlave, address, pData, size, flags)
  !dec$ attributes stdcall, alias : '_KS_writeEcatSlaveMem@20' :: KS_writeEcatSlaveMem
  integer*4 hSlave
  integer*4 address
  integer*4 pData
  integer*4 size
  integer*4 flags
end function

end interface

!------ DC functions ------

interface

integer*4 function KS_activateEcatDcMode(hObject, startTime, cycleTime, shiftTime, hTimer, flags)
  !dec$ attributes stdcall, alias : '_KS_activateEcatDcMode@24' :: KS_activateEcatDcMode
  integer*4 hObject
  integer*8 startTime
  integer*4 cycleTime
  integer*4 shiftTime
  integer*4 hTimer
  integer*4 flags
end function

integer*4 function KS_enumEcatDcOpModes(hSlave, index, pDcOpModeBuf, pDescriptionBuf, flags)
  !dec$ attributes stdcall, alias : '_KS_enumEcatDcOpModes@20' :: KS_enumEcatDcOpModes
  integer*4 hSlave
  integer*4 index
  integer*4 pDcOpModeBuf
  integer*4 pDescriptionBuf
  integer*4 flags
end function

integer*4 function KS_lookupEcatDcOpMode(hSlave, dcOpMode, pDcParams, flags)
  !dec$ attributes stdcall, alias : '_KS_lookupEcatDcOpMode@16' :: KS_lookupEcatDcOpMode
  integer*4 hSlave
  integer*4 dcOpMode
  integer*4 pDcParams
  integer*4 flags
end function

integer*4 function KS_configEcatDcOpMode(hSlave, dcOpMode, pDcParams, flags)
  !dec$ attributes stdcall, alias : '_KS_configEcatDcOpMode@16' :: KS_configEcatDcOpMode
  integer*4 hSlave
  integer*4 dcOpMode
  integer*4 pDcParams
  integer*4 flags
end function

end interface

!------ Auxiliary functions ------

interface

integer*4 function KS_execEcatCommand(hObject, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execEcatCommand@16' :: KS_execEcatCommand
  integer*4 hObject
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

integer*4 function KS_getEcatAlias(hObject, objIndex, varIndex, pAliasBuf, flags)
  !dec$ attributes stdcall, alias : '_KS_getEcatAlias@20' :: KS_getEcatAlias
  integer*4 hObject
  integer*4 objIndex
  integer*4 varIndex
  integer*4 pAliasBuf
  integer*4 flags
end function

integer*4 function KS_setEcatAlias(hObject, objIndex, varIndex, alias, flags)
  !dec$ attributes stdcall, alias : '_KS_setEcatAlias@20' :: KS_setEcatAlias
  integer*4 hObject
  integer*4 objIndex
  integer*4 varIndex
  integer*4 alias
  integer*4 flags
end function

end interface

!------ EtherCAT Slave Device functions ------

interface

integer*4 function KS_openEcatSlaveDevice(phSlaveDevice, name, flags)
  !dec$ attributes stdcall, alias : '_KS_openEcatSlaveDevice@12' :: KS_openEcatSlaveDevice
  integer*4 phSlaveDevice
  integer*4 name
  integer*4 flags
end function

integer*4 function KS_closeEcatSlaveDevice(hSlaveDevice, flags)
  !dec$ attributes stdcall, alias : '_KS_closeEcatSlaveDevice@8' :: KS_closeEcatSlaveDevice
  integer*4 hSlaveDevice
  integer*4 flags
end function

integer*4 function KS_queryEcatSlaveDeviceState(hSlaveDevice, pSlaveState, flags)
  !dec$ attributes stdcall, alias : '_KS_queryEcatSlaveDeviceState@12' :: KS_queryEcatSlaveDeviceState
  integer*4 hSlaveDevice
  integer*4 pSlaveState
  integer*4 flags
end function

end interface

!------ EtherCAT Automation Protocol functions ------

interface

integer*4 function KS_createEcatEapNode(phNode, hConnection, networkFile, flags)
  !dec$ attributes stdcall, alias : '_KS_createEcatEapNode@16' :: KS_createEcatEapNode
  integer*4 phNode
  integer*4 hConnection
  integer*4 networkFile
  integer*4 flags
end function

integer*4 function KS_closeEcatEapNode(hNode, flags)
  !dec$ attributes stdcall, alias : '_KS_closeEcatEapNode@8' :: KS_closeEcatEapNode
  integer*4 hNode
  integer*4 flags
end function

integer*4 function KS_queryEcatEapNodeState(hNode, pNodeState, flags)
  !dec$ attributes stdcall, alias : '_KS_queryEcatEapNodeState@12' :: KS_queryEcatEapNodeState
  integer*4 hNode
  integer*4 pNodeState
  integer*4 flags
end function

integer*4 function KS_createEcatEapProxy(hNode, phProxy, pNodeId, flags)
  !dec$ attributes stdcall, alias : '_KS_createEcatEapProxy@16' :: KS_createEcatEapProxy
  integer*4 hNode
  integer*4 phProxy
  integer*4 pNodeId
  integer*4 flags
end function

integer*4 function KS_deleteEcatEapProxy(hProxy, flags)
  !dec$ attributes stdcall, alias : '_KS_deleteEcatEapProxy@8' :: KS_deleteEcatEapProxy
  integer*4 hProxy
  integer*4 flags
end function

integer*4 function KS_queryEcatEapProxyState(hProxy, pProxyState, flags)
  !dec$ attributes stdcall, alias : '_KS_queryEcatEapProxyState@12' :: KS_queryEcatEapProxyState
  integer*4 hProxy
  integer*4 pProxyState
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! MultiFunction Module
!---------------------------------------------------------------------------------------------------------------

!------ Flags ------

integer*4, parameter :: KSF_UNIPOLAR                            = #00000001
integer*4, parameter :: KSF_BIPOLAR                             = #00000000
integer*4, parameter :: KSF_DIFFERENTIAL                        = #00000002
integer*4, parameter :: KSF_SINGLE_ENDED                        = #00000000
integer*4, parameter :: KSF_RISING_EDGE                         = #00001000
integer*4, parameter :: KSF_FALLING_EDGE                        = #00000000
integer*4, parameter :: KSF_NORMALIZE                           = #00000800
integer*4, parameter :: KSF_STOP_ANALOG_STREAM                  = #00000400

!------ Multi function events ------

integer*4, parameter :: KS_MULTIFUNCTION_INTERRUPT              = (MULTIFUNCTION_BASE+#00000000)
integer*4, parameter :: KS_MULTIFUNCTION_ANALOGWATCHDOG         = (MULTIFUNCTION_BASE+#00000001)
integer*4, parameter :: KS_MULTIFUNCTION_DIGITALWATCHDOG        = (MULTIFUNCTION_BASE+#00000002)
integer*4, parameter :: KS_MULTIFUNCTION_TIMER                  = (MULTIFUNCTION_BASE+#00000003)
integer*4, parameter :: KS_MULTIFUNCTION_ANALOG_SEQUENCE        = (MULTIFUNCTION_BASE+#00000004)
integer*4, parameter :: KS_MULTIFUNCTION_ANALOG_STREAM          = (MULTIFUNCTION_BASE+#00000006)
integer*4, parameter :: KS_MULTIFUNCTION_DIGITAL_IN             = (MULTIFUNCTION_BASE+#00000007)
integer*4, parameter :: KS_MULTIFUNCTION_BUFFER_FULL            = (MULTIFUNCTION_BASE+#00000008)

!------ Board commands ------

integer*4, parameter :: KS_BOARD_RESET                          = #00000000
integer*4, parameter :: KS_BOARD_SEQUENCE_READY                 = #00000001

!------ Context structures ------

type BoardUserContext
  integer*4 ctxType
  integer*4 hBoard
end type

type BoardStreamErrorContext
  integer*4 ctxType
  integer*4 hBoard
  integer*4 lostDataCount
end type

type KSMfBoardInfo
  integer*4 structSize
  byte pBoardName(0:63)
  byte pBoardSpec(0:255)
  integer*4 analogInp
  integer*4 analogOut
  integer*4 digitalInOut
  integer*4 digitalInp
  integer*4 digitalOut
  integer*4 digitalPortSize
  integer*4 analogValueSize
end type

!------ Basic functions ------

interface

integer*4 function KS_openBoard(phBoard, name, hKernel, flags)
  !dec$ attributes stdcall, alias : '_KS_openBoard@16' :: KS_openBoard
  integer*4 phBoard
  integer*4 name
  integer*4 hKernel
  integer*4 flags
end function

integer*4 function KS_closeBoard(hBoard, flags)
  !dec$ attributes stdcall, alias : '_KS_closeBoard@8' :: KS_closeBoard
  integer*4 hBoard
  integer*4 flags
end function

integer*4 function KS_getBoardInfo(hBoard, pBoardInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_getBoardInfo@12' :: KS_getBoardInfo
  integer*4 hBoard
  integer*4 pBoardInfo
  integer*4 flags
end function

integer*4 function KS_setBoardObject(hBoard, pAppAddr, pSysAddr)
  !dec$ attributes stdcall, alias : '_KS_setBoardObject@12' :: KS_setBoardObject
  integer*4 hBoard
  integer*4 pAppAddr
  integer*4 pSysAddr
end function

integer*4 function KS_getBoardObject(hBoard, ppAddr)
  !dec$ attributes stdcall, alias : '_KS_getBoardObject@8' :: KS_getBoardObject
  integer*4 hBoard
  integer*4 ppAddr
end function

integer*4 function KS_installBoardHandler(hBoard, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installBoardHandler@16' :: KS_installBoardHandler
  integer*4 hBoard
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_execBoardCommand(hBoard, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execBoardCommand@16' :: KS_execBoardCommand
  integer*4 hBoard
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

end interface

!------ Low-level functions ------

interface

integer*4 function KS_readBoardRegister(hBoard, rangeIndex, offset, pValue, flags)
  !dec$ attributes stdcall, alias : '_KS_readBoardRegister@20' :: KS_readBoardRegister
  integer*4 hBoard
  integer*4 rangeIndex
  integer*4 offset
  integer*4 pValue
  integer*4 flags
end function

integer*4 function KS_writeBoardRegister(hBoard, rangeIndex, offset, value, flags)
  !dec$ attributes stdcall, alias : '_KS_writeBoardRegister@20' :: KS_writeBoardRegister
  integer*4 hBoard
  integer*4 rangeIndex
  integer*4 offset
  integer*4 value
  integer*4 flags
end function

end interface

!------ Digital input functions ------

interface

integer*4 function KS_getDigitalBit(hBoard, channel, pValue, flags)
  !dec$ attributes stdcall, alias : '_KS_getDigitalBit@16' :: KS_getDigitalBit
  integer*4 hBoard
  integer*4 channel
  integer*4 pValue
  integer*4 flags
end function

integer*4 function KS_getDigitalPort(hBoard, port, pValue, flags)
  !dec$ attributes stdcall, alias : '_KS_getDigitalPort@16' :: KS_getDigitalPort
  integer*4 hBoard
  integer*4 port
  integer*4 pValue
  integer*4 flags
end function

end interface

!------ Digital output functions ------

interface

integer*4 function KS_setDigitalBit(hBoard, channel, value, flags)
  !dec$ attributes stdcall, alias : '_KS_setDigitalBit@16' :: KS_setDigitalBit
  integer*4 hBoard
  integer*4 channel
  integer*4 value
  integer*4 flags
end function

integer*4 function KS_setDigitalPort(hBoard, port, value, mask, flags)
  !dec$ attributes stdcall, alias : '_KS_setDigitalPort@20' :: KS_setDigitalPort
  integer*4 hBoard
  integer*4 port
  integer*4 value
  integer*4 mask
  integer*4 flags
end function

end interface

!------ Analog input functions ------

interface

integer*4 function KS_configAnalogChannel(hBoard, channel, gain, mode, flags)
  !dec$ attributes stdcall, alias : '_KS_configAnalogChannel@20' :: KS_configAnalogChannel
  integer*4 hBoard
  integer*4 channel
  integer*4 gain
  integer*4 mode
  integer*4 flags
end function

integer*4 function KS_getAnalogValue(hBoard, channel, pValue, flags)
  !dec$ attributes stdcall, alias : '_KS_getAnalogValue@16' :: KS_getAnalogValue
  integer*4 hBoard
  integer*4 channel
  integer*4 pValue
  integer*4 flags
end function

integer*4 function KS_initAnalogStream(hBoard, pChannelSequence, sequenceLength, sequenceCount, period, flags)
  !dec$ attributes stdcall, alias : '_KS_initAnalogStream@24' :: KS_initAnalogStream
  integer*4 hBoard
  integer*4 pChannelSequence
  integer*4 sequenceLength
  integer*4 sequenceCount
  integer*4 period
  integer*4 flags
end function

integer*4 function KS_startAnalogStream(hBoard, flags)
  !dec$ attributes stdcall, alias : '_KS_startAnalogStream@8' :: KS_startAnalogStream
  integer*4 hBoard
  integer*4 flags
end function

integer*4 function KS_getAnalogStream(hBoard, pBuffer, length, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_getAnalogStream@20' :: KS_getAnalogStream
  integer*4 hBoard
  integer*4 pBuffer
  integer*4 length
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_valueToVolt(hBoard, gain, mode, value, pVolt, flags)
  !dec$ attributes stdcall, alias : '_KS_valueToVolt@24' :: KS_valueToVolt
  integer*4 hBoard
  integer*4 gain
  integer*4 mode
  integer*4 value
  integer*4 pVolt
  integer*4 flags
end function

end interface

!------ Analog output functions ------

interface

integer*4 function KS_setAnalogValue(hBoard, channel, mode, value, flags)
  !dec$ attributes stdcall, alias : '_KS_setAnalogValue@20' :: KS_setAnalogValue
  integer*4 hBoard
  integer*4 channel
  integer*4 mode
  integer*4 value
  integer*4 flags
end function

integer*4 function KS_voltToValue(hBoard, mode, volt, pValue, flags)
  !dec$ attributes stdcall, alias : '_KS_voltToValue@20' :: KS_voltToValue
  integer*4 hBoard
  integer*4 mode
  real*4 volt
  integer*4 pValue
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! CAN Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_BAD_IDENTIFIER                  = (KSERROR_CATEGORY_CAN+#00000000)
integer*4, parameter :: KSERROR_RECV_BUFFER_FULL                = (KSERROR_CATEGORY_CAN+#00010000)
integer*4, parameter :: KSERROR_XMIT_BUFFER_FULL                = (KSERROR_CATEGORY_CAN+#00020000)
integer*4, parameter :: KSERROR_BUS_OFF                         = (KSERROR_CATEGORY_CAN+#00030000)
integer*4, parameter :: KSERROR_BUS_PASSIVE                     = (KSERROR_CATEGORY_CAN+#00040000)
integer*4, parameter :: KSERROR_ERROR_COUNTER_WARNING           = (KSERROR_CATEGORY_CAN+#00050000)
integer*4, parameter :: KSERROR_PORT_DATA_BUFFER_OVERRUN        = (KSERROR_CATEGORY_CAN+#00060000)
integer*4, parameter :: KSERROR_BUS_ERROR                       = (KSERROR_CATEGORY_CAN+#00070000)

!------ CAN mode ------

integer*4, parameter :: KS_CAN_NORMAL_MODE                      = #00000001
integer*4, parameter :: KS_CAN_RESET_MODE                       = #00000002
integer*4, parameter :: KS_CAN_LISTEN_ONLY_MODE                 = #00000004

!------ CAN state ------

integer*4, parameter :: KS_CAN_BUS_ON                           = #00000001
integer*4, parameter :: KS_CAN_BUS_HEAVY                        = #00000002
integer*4, parameter :: KS_CAN_BUS_OFF                          = #00000004
integer*4, parameter :: KS_CAN_RECV_AVAIL                       = #00000008
integer*4, parameter :: KS_CAN_XMIT_PENDING                     = #00000010
integer*4, parameter :: KS_CAN_WARN_LIMIT_REACHED               = #00000020
integer*4, parameter :: KS_CAN_DATA_BUFFER_OVERRUN              = #00000040

!------ CAN commands ------

integer*4, parameter :: KS_SIGNAL_XMIT_EMPTY                    = #00000004
integer*4, parameter :: KS_SET_LISTEN_ONLY                      = #00000005
integer*4, parameter :: KS_RESET_PORT                           = #00000006
integer*4, parameter :: KS_GET_ERROR_CODE_CAPTURE               = #00000007
integer*4, parameter :: KS_CAN_SET_XMIT_TIMEOUT                 = #00000008
integer*4, parameter :: KS_CAN_CLEAR_BUS_OFF                    = #0000000a

!------ CAN baud rates ------

integer*4, parameter :: KS_CAN_BAUD_1M                          = #000f4240
integer*4, parameter :: KS_CAN_BAUD_800K                        = #000c3500
integer*4, parameter :: KS_CAN_BAUD_666K                        = #000a2c2a
integer*4, parameter :: KS_CAN_BAUD_500K                        = #0007a120
integer*4, parameter :: KS_CAN_BAUD_250K                        = #0003d090
integer*4, parameter :: KS_CAN_BAUD_125K                        = #0001e848
integer*4, parameter :: KS_CAN_BAUD_100K                        = #000186a0
integer*4, parameter :: KS_CAN_BAUD_50K                         = #0000c350
integer*4, parameter :: KS_CAN_BAUD_20K                         = #00004e20
integer*4, parameter :: KS_CAN_BAUD_10K                         = #00002710

!------ CAN-FD baud rates ------

integer*4, parameter :: KS_CAN_FD_BAUD_12M                      = #00b71b00
integer*4, parameter :: KS_CAN_FD_BAUD_10M                      = #00989680
integer*4, parameter :: KS_CAN_FD_BAUD_8M                       = #007a1200
integer*4, parameter :: KS_CAN_FD_BAUD_6M                       = #005b8d80
integer*4, parameter :: KS_CAN_FD_BAUD_4M                       = #003d0900
integer*4, parameter :: KS_CAN_FD_BAUD_2M                       = #001e8480

!------ Common CAN and CAN-FD baud rates ------

integer*4, parameter :: KS_CAN_BAUD_CUSTOM                      = #00000000

!------ CAN message types ------

integer*4, parameter :: KS_CAN_MSGTYPE_STANDARD                 = #00000001
integer*4, parameter :: KS_CAN_MSGTYPE_EXTENDED                 = #00000002
integer*4, parameter :: KS_CAN_MSGTYPE_RTR                      = #00000004
integer*4, parameter :: KS_CAN_MSGTYPE_FD                       = #00000008
integer*4, parameter :: KS_CAN_MSGTYPE_BRS                      = #00000010

!------ Legacy CAN events ------

integer*4, parameter :: KS_CAN_RECV                             = (CAN_BASE+#00000000)
integer*4, parameter :: KS_CAN_XMIT_EMPTY                       = (CAN_BASE+#00000001)
integer*4, parameter :: KS_CAN_XMIT_RTR                         = (CAN_BASE+#00000002)
integer*4, parameter :: KS_CAN_ERROR                            = (CAN_BASE+#00000003)
integer*4, parameter :: KS_CAN_FILTER                           = (CAN_BASE+#00000004)
integer*4, parameter :: KS_CAN_TIMEOUT                          = (CAN_BASE+#00000005)

!------ CAN-FD events ------

integer*4, parameter :: KS_CAN_FD_RECV                          = (CAN_BASE+#00000006)
integer*4, parameter :: KS_CAN_FD_XMIT_EMPTY                    = (CAN_BASE+#00000007)
integer*4, parameter :: KS_CAN_FD_ERROR                         = (CAN_BASE+#00000008)

!------ CAN-FD data structures ------

type KSCanFdMsg
  integer*4 structSize
  integer*4 identifier
  integer*4 msgType
  integer*4 dataLength
  byte msgData(0:63)
  integer*8 timestamp
end type

type KSCanFdState
  integer*4 structSize
  integer*4 state
  integer*4 waitingForXmit
  integer*4 waitingForRecv
  integer*4 xmitErrCounter
  integer*4 recvErrCounter
end type

type KSCanFdConfig
  integer*4 structSize
  integer*4 clockFrequency
  integer*4 canBaudRate
  integer*4 canBrp
  integer*4 canTseg1
  integer*4 canTseg2
  integer*4 canSjw
  integer*4 fdBaudRate
  integer*4 fdBrp
  integer*4 fdTseg1
  integer*4 fdTseg2
  integer*4 fdSjw
end type

!------ CAN-FD Context structures ------

type KSCanFdContext
  integer*4 ctxType
  integer*4 hCanFd
end type

type KSCanFdErrorContext
  integer*4 ctxType
  integer*4 hCanFd
  integer*4 errorValue
  integer*8 timestamp
end type

type KSCanFdMsgContext
  integer*4 ctxType
  integer*4 hCanFd
  type (KSCanFdMsg) msg
end type

!------ CAN data structures ------

type KSCanMsg
  integer*4 identifier
  integer*2 msgType
  integer*2 dataLength
  byte msgData(0:7)
  integer*8 timestamp
end type

type KSCanState
  integer*4 structSize
  integer*4 mode
  integer*4 state
  integer*4 baudRate
  integer*4 waitingForXmit
  integer*4 waitingForRecv
  integer*4 errorWarnLimit
  integer*4 recvErrCounter
  integer*4 xmitErrCounter
  integer*4 lastError
end type

!------ CAN Context structures ------

type CanUserContext
  integer*4 ctxType
  integer*4 hCan
end type

type CanErrorUserContext
  integer*4 ctxType
  integer*4 hCan
  integer*4 errorValue
  integer*8 timestamp
end type

type CanMsgUserContext
  integer*4 ctxType
  integer*4 hCan
  type (KSCanMsg) msg
  integer*4 discarded
end type

type CanTimeoutUserContext
  integer*4 ctxType
  integer*4 hCan
  type (KSCanMsg) msg
end type

!------ Common CAN-FD functions ------

interface

integer*4 function KS_openCanFd(phCanFd, name, port, pCanFdConfig, flags)
  !dec$ attributes stdcall, alias : '_KS_openCanFd@20' :: KS_openCanFd
  integer*4 phCanFd
  integer*4 name
  integer*4 port
  integer*4 pCanFdConfig
  integer*4 flags
end function

integer*4 function KS_closeCanFd(hCanFd, flags)
  !dec$ attributes stdcall, alias : '_KS_closeCanFd@8' :: KS_closeCanFd
  integer*4 hCanFd
  integer*4 flags
end function

end interface

!------ CAN-FD messaging functions ------

interface

integer*4 function KS_xmitCanFdMsg(hCanFd, pCanFdMsg, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitCanFdMsg@12' :: KS_xmitCanFdMsg
  integer*4 hCanFd
  integer*4 pCanFdMsg
  integer*4 flags
end function

integer*4 function KS_recvCanFdMsg(hCanFd, pCanFdMsg, flags)
  !dec$ attributes stdcall, alias : '_KS_recvCanFdMsg@12' :: KS_recvCanFdMsg
  integer*4 hCanFd
  integer*4 pCanFdMsg
  integer*4 flags
end function

integer*4 function KS_installCanFdHandler(hCanFd, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installCanFdHandler@16' :: KS_installCanFdHandler
  integer*4 hCanFd
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_getCanFdState(hCanFd, pCanFdState, flags)
  !dec$ attributes stdcall, alias : '_KS_getCanFdState@12' :: KS_getCanFdState
  integer*4 hCanFd
  integer*4 pCanFdState
  integer*4 flags
end function

integer*4 function KS_execCanFdCommand(hCanFd, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execCanFdCommand@16' :: KS_execCanFdCommand
  integer*4 hCanFd
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

end interface

!------ Common CAN functions ------

interface

integer*4 function KS_openCan(phCan, name, port, baudRate, flags)
  !dec$ attributes stdcall, alias : '_KS_openCan@20' :: KS_openCan
  integer*4 phCan
  integer*4 name
  integer*4 port
  integer*4 baudRate
  integer*4 flags
end function

integer*4 function KS_openCanEx(phCan, hConnection, port, baudRate, flags)
  !dec$ attributes stdcall, alias : '_KS_openCanEx@20' :: KS_openCanEx
  integer*4 phCan
  integer*4 hConnection
  integer*4 port
  integer*4 baudRate
  integer*4 flags
end function

integer*4 function KS_closeCan(hCan)
  !dec$ attributes stdcall, alias : '_KS_closeCan@4' :: KS_closeCan
  integer*4 hCan
end function

end interface

!------ CAN messaging functions ------

interface

integer*4 function KS_xmitCanMsg(hCan, pCanMsg, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitCanMsg@12' :: KS_xmitCanMsg
  integer*4 hCan
  integer*4 pCanMsg
  integer*4 flags
end function

integer*4 function KS_recvCanMsg(hCan, pCanMsg, flags)
  !dec$ attributes stdcall, alias : '_KS_recvCanMsg@12' :: KS_recvCanMsg
  integer*4 hCan
  integer*4 pCanMsg
  integer*4 flags
end function

integer*4 function KS_installCanHandler(hCan, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installCanHandler@16' :: KS_installCanHandler
  integer*4 hCan
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_getCanState(hCan, pCanState)
  !dec$ attributes stdcall, alias : '_KS_getCanState@8' :: KS_getCanState
  integer*4 hCan
  integer*4 pCanState
end function

integer*4 function KS_execCanCommand(hCan, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execCanCommand@16' :: KS_execCanCommand
  integer*4 hCan
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! LIN Module
!---------------------------------------------------------------------------------------------------------------

!------ LIN commands ------

integer*4, parameter :: KS_LIN_RESET                            = #00000000
integer*4, parameter :: KS_LIN_SLEEP                            = #00000001
integer*4, parameter :: KS_LIN_WAKEUP                           = #00000002
integer*4, parameter :: KS_LIN_SET_BAUD_RATE                    = #00000004
integer*4, parameter :: KS_LIN_SET_VERSION                      = #00000005
integer*4, parameter :: KS_LIN_DISABLE_QUEUE_ERROR              = #00000006
integer*4, parameter :: KS_LIN_ENABLE_QUEUE_ERROR               = #00000007

!------ LIN bit rates ------

integer*4, parameter :: KS_LIN_BAUD_19200                       = #00004b00
integer*4, parameter :: KS_LIN_BAUD_10417                       = #000028b1
integer*4, parameter :: KS_LIN_BAUD_9600                        = #00002580
integer*4, parameter :: KS_LIN_BAUD_4800                        = #000012c0
integer*4, parameter :: KS_LIN_BAUD_2400                        = #00000960
integer*4, parameter :: KS_LIN_BAUD_1200                        = #000004b0

!------ LIN Errors ------

integer*4, parameter :: KS_LIN_ERROR_PHYSICAL                   = #00000001
integer*4, parameter :: KS_LIN_ERROR_TRANSPORT                  = #00000002
integer*4, parameter :: KS_LIN_ERROR_BUS_COLLISION              = #00000003
integer*4, parameter :: KS_LIN_ERROR_PARITY                     = #00000004
integer*4, parameter :: KS_LIN_ERROR_CHECKSUM                   = #00000005
integer*4, parameter :: KS_LIN_ERROR_BREAK_EXPECTED             = #00000006
integer*4, parameter :: KS_LIN_ERROR_RESPONSE_TIMEOUT           = #00000007
integer*4, parameter :: KS_LIN_ERROR_PID_TIMEOUT                = #00000009
integer*4, parameter :: KS_LIN_ERROR_RESPONSE_TOO_SHORT         = #00000010
integer*4, parameter :: KS_LIN_ERROR_RECV_QUEUE_FULL            = #00000011
integer*4, parameter :: KS_LIN_ERROR_RESPONSE_WITHOUT_HEADER    = #00000012

!------ LIN events ------

integer*4, parameter :: KS_LIN_ERROR                            = #00000000
integer*4, parameter :: KS_LIN_RECV_HEADER                      = #00000001
integer*4, parameter :: KS_LIN_RECV_RESPONSE                    = #00000002

!------ LIN data structures ------

type KSLinProperties
  integer*4 structSize
  integer*4 linVersion
  integer*4 baudRate
end type

type KSLinState
  integer*4 structSize
  type (KSLinProperties) properties
  integer*4 xmitHdrCount
  integer*4 xmitRspCount
  integer*4 recvCount
  integer*4 recvAvail
  integer*4 recvErrorCount
  integer*4 recvNoRspCount
  integer*4 collisionCount
end type

type KSLinHeader
  integer*4 identifier
  byte parity
  byte parityOk
end type

type KSLinResponse
  byte data(0:7)
  integer*4 dataLen
  byte checksum
  byte checksumOk
end type

type KSLinMsg
  type (KSLinHeader) header
  type (KSLinResponse) response
  integer*8 timestamp
end type

!------ LIN Context structures ------

type LinUserContext
  integer*4 ctxType
  integer*4 hLin
end type

type LinErrorUserContext
  integer*4 ctxType
  integer*4 hLin
  integer*8 timestamp
  integer*4 errorCode
end type

type LinHeaderUserContext
  integer*4 ctxType
  integer*4 hLin
  integer*8 timestamp
  type (KSLinHeader) header
end type

type LinResponseUserContext
  integer*4 ctxType
  integer*4 hLin
  integer*8 timestamp
  type (KSLinMsg) msg
end type

!------ Common functions ------

interface

integer*4 function KS_openLin(phLin, pDeviceName, port, pProperties, flags)
  !dec$ attributes stdcall, alias : '_KS_openLin@20' :: KS_openLin
  integer*4 phLin
  integer*4 pDeviceName
  integer*4 port
  integer*4 pProperties
  integer*4 flags
end function

integer*4 function KS_openLinEx(phLin, hLinDevice, pProperties, flags)
  !dec$ attributes stdcall, alias : '_KS_openLinEx@16' :: KS_openLinEx
  integer*4 phLin
  integer*4 hLinDevice
  integer*4 pProperties
  integer*4 flags
end function

integer*4 function KS_closeLin(hLin, flags)
  !dec$ attributes stdcall, alias : '_KS_closeLin@8' :: KS_closeLin
  integer*4 hLin
  integer*4 flags
end function

integer*4 function KS_installLinHandler(hLin, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installLinHandler@16' :: KS_installLinHandler
  integer*4 hLin
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_execLinCommand(hLin, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execLinCommand@16' :: KS_execLinCommand
  integer*4 hLin
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

integer*4 function KS_xmitLinHeader(hLin, identifier, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitLinHeader@12' :: KS_xmitLinHeader
  integer*4 hLin
  integer*4 identifier
  integer*4 flags
end function

integer*4 function KS_xmitLinResponse(hLin, pData, size, flags)
  !dec$ attributes stdcall, alias : '_KS_xmitLinResponse@16' :: KS_xmitLinResponse
  integer*4 hLin
  integer*4 pData
  integer*4 size
  integer*4 flags
end function

integer*4 function KS_recvLinMsg(hLin, pLinMsg, flags)
  !dec$ attributes stdcall, alias : '_KS_recvLinMsg@12' :: KS_recvLinMsg
  integer*4 hLin
  integer*4 pLinMsg
  integer*4 flags
end function

integer*4 function KS_getLinState(hLin, pLinState, flags)
  !dec$ attributes stdcall, alias : '_KS_getLinState@12' :: KS_getLinState
  integer*4 hLin
  integer*4 pLinState
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! CANopen Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_CANO_EMERGENCY_REQUEST          = (KSERROR_CATEGORY_CANOPEN+#00000000)
integer*4, parameter :: KSERROR_CANO_TOPOLOGY_CHANGE            = (KSERROR_CATEGORY_CANOPEN+#00020000)
integer*4, parameter :: KSERROR_CANO_SLAVE_ERROR                = (KSERROR_CATEGORY_CANOPEN+#00030000)
integer*4, parameter :: KSERROR_CANO_DATA_INCOMPLETE            = (KSERROR_CATEGORY_CANOPEN+#00040000)
integer*4, parameter :: KSERROR_CANO_RECEIVED_SYNC              = (KSERROR_CATEGORY_CANOPEN+#00050000)
integer*4, parameter :: KSERROR_CANO_PDO_NOT_VALID              = (KSERROR_CATEGORY_CANOPEN+#00060000)

!------ DataObj type flags ------

integer*4, parameter :: KS_CANO_DATAOBJ_PDO_TYPE                = #00000001
integer*4, parameter :: KS_CANO_DATAOBJ_SDO_TYPE                = #00000002
integer*4, parameter :: KS_CANO_DATAOBJ_READABLE                = #00010000
integer*4, parameter :: KS_CANO_DATAOBJ_WRITEABLE               = #00020000
integer*4, parameter :: KS_CANO_DATAOBJ_ACTIVE                  = #00040000
integer*4, parameter :: KS_CANO_DATAOBJ_MAPPABLE                = #00080000

!------ Functionality flags ------

integer*4, parameter :: KS_CANO_SIMPLE_BOOTUP_MASTER            = #00000001
integer*4, parameter :: KS_CANO_SIMPLE_BOOTUP_SLAVE             = #00000002
integer*4, parameter :: KS_CANO_DYNAMIC_CHANNELS                = #00000004
integer*4, parameter :: KS_CANO_GROUP_MESSAGING                 = #00000008
integer*4, parameter :: KS_CANO_LAYER_SETTINGS                  = #00000010

!------ State type ------

integer*4, parameter :: KS_CANO_STATE_INIT                      = #00000001
integer*4, parameter :: KS_CANO_STATE_PREOP                     = #00000002
integer*4, parameter :: KS_CANO_STATE_STOP                      = #00000003
integer*4, parameter :: KS_CANO_STATE_OP                        = #00000004

!------ CANopen commands ------

integer*4, parameter :: KS_CANO_XMIT_TIME_STAMP                 = #00000001

!------ common index ------

integer*4, parameter :: KS_CANO_INDEX_ALL                       = #ffffffff
integer*4, parameter :: KS_CANO_INDEX_INPUT                     = #fffffffe
integer*4, parameter :: KS_CANO_INDEX_OUTPUT                    = #fffffffd

!------ CANopen events ------

integer*4, parameter :: KS_CANO_ERROR                           = (CANOPEN_BASE+#00000000)
integer*4, parameter :: KS_CANO_DATASET_SIGNAL                  = (CANOPEN_BASE+#00000001)
integer*4, parameter :: KS_CANO_TOPOLOGY_CHANGE                 = (CANOPEN_BASE+#00000002)

!------ Topology change reasons ------

integer*4, parameter :: KS_CANO_TOPOLOGY_MASTER_CONNECTED       = #00000000
integer*4, parameter :: KS_CANO_TOPOLOGY_MASTER_DISCONNECTED    = #00000001
integer*4, parameter :: KS_CANO_TOPOLOGY_SLAVE_COUNT_CHANGED    = #00000002
integer*4, parameter :: KS_CANO_TOPOLOGY_SLAVE_ONLINE           = #00000003
integer*4, parameter :: KS_CANO_TOPOLOGY_SLAVE_OFFLINE          = #00000004

type CanoErrorUserContext
  integer*4 ctxType
  integer*4 hMaster
  integer*4 hSlave
  integer*4 error
end type

type CanoDataSetUserContext
  integer*4 ctxType
  integer*4 hDataSet
end type

type CanoTopologyUserContext
  integer*4 ctxType
  integer*4 hMaster
  integer*4 slavesOnline
  integer*4 slavesCreated
  integer*4 slavesCreatedAndOnline
  integer*4 reason
  integer*4 hSlave
end type

type KSCanoEmergencyObj
  integer*2 errorCode
  byte errorRegister
  byte additionalCode(0:4)
end type

type KSCanoMasterState
  integer*4 structSize
  integer*4 connected
  integer*4 slavesOnline
  integer*4 slaveCreated
  integer*4 slavesCreatedAndOnline
  integer*4 masterState
  integer*4 lastError
end type

type KSCanoSlaveState
  integer*4 structSize
  integer*4 online
  integer*4 created
  integer*4 vendor
  integer*4 product
  integer*4 revision
  integer*4 id
  integer*4 slaveState
  type (KSCanoEmergencyObj) emergencyObj
end type

type KSCanoDataVarInfo
  integer*4 objType
  integer*4 name
  integer*4 dataType
  integer*4 bitLength
  integer*4 subIndex
  integer*4 defaultValue
  integer*4 minValue
  integer*4 maxValue
end type

type KSCanoDataObjInfo
  integer*4 objType
  integer*4 name
  integer*4 bitLength
  integer*4 index
  integer*4 cobId
  integer*4 mappingIndex
  integer*4 transmissionType
  integer*4 varCount
  integer*4 vars(0:0)
end type

type KSCanoSlaveInfo
  integer*4 vendorId
  integer*4 productId
  integer*4 revision
  integer*4 name
  integer*4 orderCode
  integer*4 rxPdoCount
  integer*4 txPdoCount
  integer*4 granularity
  integer*4 functionality
  integer*4 objCount
  integer*4 objs(0:0)
end type

!------ Master functions ------

interface

integer*4 function KS_createCanoMaster(phMaster, hConnection, libraryPath, topologyFile, flags)
  !dec$ attributes stdcall, alias : '_KS_createCanoMaster@20' :: KS_createCanoMaster
  integer*4 phMaster
  integer*4 hConnection
  integer*4 libraryPath
  integer*4 topologyFile
  integer*4 flags
end function

integer*4 function KS_closeCanoMaster(hMaster)
  !dec$ attributes stdcall, alias : '_KS_closeCanoMaster@4' :: KS_closeCanoMaster
  integer*4 hMaster
end function

integer*4 function KS_installCanoMasterHandler(hMaster, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installCanoMasterHandler@16' :: KS_installCanoMasterHandler
  integer*4 hMaster
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_queryCanoMasterState(hMaster, pMasterState, flags)
  !dec$ attributes stdcall, alias : '_KS_queryCanoMasterState@12' :: KS_queryCanoMasterState
  integer*4 hMaster
  integer*4 pMasterState
  integer*4 flags
end function

integer*4 function KS_changeCanoMasterState(hMaster, state, flags)
  !dec$ attributes stdcall, alias : '_KS_changeCanoMasterState@12' :: KS_changeCanoMasterState
  integer*4 hMaster
  integer*4 state
  integer*4 flags
end function

end interface

!------ Slave functions ------

interface

integer*4 function KS_createCanoSlave(hMaster, phSlave, id, position, vendor, product, revision, flags)
  !dec$ attributes stdcall, alias : '_KS_createCanoSlave@32' :: KS_createCanoSlave
  integer*4 hMaster
  integer*4 phSlave
  integer*4 id
  integer*4 position
  integer*4 vendor
  integer*4 product
  integer*4 revision
  integer*4 flags
end function

integer*4 function KS_createCanoSlaveIndirect(hMaster, phSlave, pSlaveState, flags)
  !dec$ attributes stdcall, alias : '_KS_createCanoSlaveIndirect@16' :: KS_createCanoSlaveIndirect
  integer*4 hMaster
  integer*4 phSlave
  integer*4 pSlaveState
  integer*4 flags
end function

integer*4 function KS_enumCanoSlaves(hMaster, index, pSlaveState, flags)
  !dec$ attributes stdcall, alias : '_KS_enumCanoSlaves@16' :: KS_enumCanoSlaves
  integer*4 hMaster
  integer*4 index
  integer*4 pSlaveState
  integer*4 flags
end function

integer*4 function KS_deleteCanoSlave(hSlave)
  !dec$ attributes stdcall, alias : '_KS_deleteCanoSlave@4' :: KS_deleteCanoSlave
  integer*4 hSlave
end function

integer*4 function KS_queryCanoSlaveState(hSlave, pSlaveState, flags)
  !dec$ attributes stdcall, alias : '_KS_queryCanoSlaveState@12' :: KS_queryCanoSlaveState
  integer*4 hSlave
  integer*4 pSlaveState
  integer*4 flags
end function

integer*4 function KS_changeCanoSlaveState(hSlave, state, flags)
  !dec$ attributes stdcall, alias : '_KS_changeCanoSlaveState@12' :: KS_changeCanoSlaveState
  integer*4 hSlave
  integer*4 state
  integer*4 flags
end function

integer*4 function KS_queryCanoSlaveInfo(hSlave, ppSlaveInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_queryCanoSlaveInfo@12' :: KS_queryCanoSlaveInfo
  integer*4 hSlave
  integer*4 ppSlaveInfo
  integer*4 flags
end function

integer*4 function KS_queryCanoDataObjInfo(hSlave, objIndex, ppDataObjInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_queryCanoDataObjInfo@16' :: KS_queryCanoDataObjInfo
  integer*4 hSlave
  integer*4 objIndex
  integer*4 ppDataObjInfo
  integer*4 flags
end function

integer*4 function KS_queryCanoDataVarInfo(hSlave, objIndex, varIndex, ppDataVarInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_queryCanoDataVarInfo@20' :: KS_queryCanoDataVarInfo
  integer*4 hSlave
  integer*4 objIndex
  integer*4 varIndex
  integer*4 ppDataVarInfo
  integer*4 flags
end function

integer*4 function KS_enumCanoDataObjInfo(hSlave, objIndex, ppDataObjInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_enumCanoDataObjInfo@16' :: KS_enumCanoDataObjInfo
  integer*4 hSlave
  integer*4 objIndex
  integer*4 ppDataObjInfo
  integer*4 flags
end function

integer*4 function KS_enumCanoDataVarInfo(hSlave, objIndex, varIndex, ppDataVarInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_enumCanoDataVarInfo@20' :: KS_enumCanoDataVarInfo
  integer*4 hSlave
  integer*4 objIndex
  integer*4 varIndex
  integer*4 ppDataVarInfo
  integer*4 flags
end function

end interface

!------ DataSet related functions ------

interface

integer*4 function KS_createCanoDataSet(hMaster, phSet, ppAppPtr, ppSysPtr, flags)
  !dec$ attributes stdcall, alias : '_KS_createCanoDataSet@20' :: KS_createCanoDataSet
  integer*4 hMaster
  integer*4 phSet
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 flags
end function

integer*4 function KS_deleteCanoDataSet(hSet)
  !dec$ attributes stdcall, alias : '_KS_deleteCanoDataSet@4' :: KS_deleteCanoDataSet
  integer*4 hSet
end function

integer*4 function KS_assignCanoDataSet(hSet, hSlave, pdoIndex, placement, flags)
  !dec$ attributes stdcall, alias : '_KS_assignCanoDataSet@20' :: KS_assignCanoDataSet
  integer*4 hSet
  integer*4 hSlave
  integer*4 pdoIndex
  integer*4 placement
  integer*4 flags
end function

integer*4 function KS_readCanoDataSet(hSet, flags)
  !dec$ attributes stdcall, alias : '_KS_readCanoDataSet@8' :: KS_readCanoDataSet
  integer*4 hSet
  integer*4 flags
end function

integer*4 function KS_postCanoDataSet(hSet, flags)
  !dec$ attributes stdcall, alias : '_KS_postCanoDataSet@8' :: KS_postCanoDataSet
  integer*4 hSet
  integer*4 flags
end function

integer*4 function KS_installCanoDataSetHandler(hSet, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installCanoDataSetHandler@16' :: KS_installCanoDataSetHandler
  integer*4 hSet
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

end interface

!------ Data exchange functions ------

interface

integer*4 function KS_getCanoDataObjAddress(hSet, hSlave, pdoIndex, subIndex, ppAppPtr, ppSysPtr, pBitOffset, pBitLength, flags)
  !dec$ attributes stdcall, alias : '_KS_getCanoDataObjAddress@36' :: KS_getCanoDataObjAddress
  integer*4 hSet
  integer*4 hSlave
  integer*4 pdoIndex
  integer*4 subIndex
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 pBitOffset
  integer*4 pBitLength
  integer*4 flags
end function

integer*4 function KS_readCanoDataObj(hSlave, index, subIndex, pData, pSize, flags)
  !dec$ attributes stdcall, alias : '_KS_readCanoDataObj@24' :: KS_readCanoDataObj
  integer*4 hSlave
  integer*4 index
  integer*4 subIndex
  integer*4 pData
  integer*4 pSize
  integer*4 flags
end function

integer*4 function KS_postCanoDataObj(hSlave, index, subIndex, pData, size, flags)
  !dec$ attributes stdcall, alias : '_KS_postCanoDataObj@24' :: KS_postCanoDataObj
  integer*4 hSlave
  integer*4 index
  integer*4 subIndex
  integer*4 pData
  integer*4 size
  integer*4 flags
end function

end interface

!------ Auxiliary functions ------

interface

integer*4 function KS_changeCanoState(hObject, state, flags)
  !dec$ attributes stdcall, alias : '_KS_changeCanoState@12' :: KS_changeCanoState
  integer*4 hObject
  integer*4 state
  integer*4 flags
end function

integer*4 function KS_execCanoCommand(hObject, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execCanoCommand@16' :: KS_execCanoCommand
  integer*4 hObject
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Profibus Module
!---------------------------------------------------------------------------------------------------------------

!------ Error Codes ------

integer*4, parameter :: KSERROR_PBUS_CONNECTION_CHANGE          = (KSERROR_CATEGORY_PROFIBUS+#00010000)
integer*4, parameter :: KSERROR_PBUS_TOPOLOGY_CHANGE            = (KSERROR_CATEGORY_PROFIBUS+#00020000)
integer*4, parameter :: KSERROR_PBUS_SLAVE_ERROR                = (KSERROR_CATEGORY_PROFIBUS+#00030000)
integer*4, parameter :: KSERROR_PBUS_ALARM_BUFFER_FULL          = (KSERROR_CATEGORY_PROFIBUS+#00040000)
integer*4, parameter :: KSERROR_BAD_PARAMETERIZATION_SET        = (KSERROR_CATEGORY_PROFIBUS+#00050000)
integer*4, parameter :: KSERROR_PBUS_MASTER_ERROR               = (KSERROR_CATEGORY_PROFIBUS+#00060000)
integer*4, parameter :: KSERROR_HARDWARE_INTERFACE              = (KSERROR_CATEGORY_PROFIBUS+#00070000)
integer*4, parameter :: KSERROR_AUTO_CLEAR                      = (KSERROR_CATEGORY_PROFIBUS+#00080000)

!------ Flags ------

integer*4, parameter :: KSF_ACCEPT_OFFLINE                      = #00000001
integer*4, parameter :: KSF_SLOT                                = #00000002

!------ Master State ------

integer*4, parameter :: KS_PBUS_OFFLINE                         = #00000001
integer*4, parameter :: KS_PBUS_STOP                            = #00000002
integer*4, parameter :: KS_PBUS_CLEAR                           = #00000003
integer*4, parameter :: KS_PBUS_OPERATE                         = #00000004

!------ Profibus special indices ------

integer*4, parameter :: KS_PBUS_INDEX_ALL                       = #ffffffff
integer*4, parameter :: KS_PBUS_INDEX_INPUT                     = #fffffffe
integer*4, parameter :: KS_PBUS_INDEX_OUTPUT                    = #fffffffd

!------ Profibus commands ------

integer*4, parameter :: KS_UNFREEZE                             = #00000001
integer*4, parameter :: KS_FREEZE                               = #00000002
integer*4, parameter :: KS_UNSYNC                               = #00000004
integer*4, parameter :: KS_SYNC                                 = #00000008

!------ Profibus command params ------

integer*4, parameter :: KS_SELECT_GROUP_1                       = #00000100
integer*4, parameter :: KS_SELECT_GROUP_2                       = #00000200
integer*4, parameter :: KS_SELECT_GROUP_3                       = #00000400
integer*4, parameter :: KS_SELECT_GROUP_4                       = #00000800
integer*4, parameter :: KS_SELECT_GROUP_5                       = #00001000
integer*4, parameter :: KS_SELECT_GROUP_6                       = #00002000
integer*4, parameter :: KS_SELECT_GROUP_7                       = #00004000
integer*4, parameter :: KS_SELECT_GROUP_8                       = #00008000

!------ Profibus bit rates ------

integer*4, parameter :: KS_PBUS_BAUD_12M                        = #00b71b00
integer*4, parameter :: KS_PBUS_BAUD_6M                         = #005b8d80
integer*4, parameter :: KS_PBUS_BAUD_3M                         = #002dc6c0
integer*4, parameter :: KS_PBUS_BAUD_1500K                      = #0016e360
integer*4, parameter :: KS_PBUS_BAUD_500K                       = #0007a120
integer*4, parameter :: KS_PBUS_BAUD_187500                     = #0002dc6c
integer*4, parameter :: KS_PBUS_BAUD_93750                      = #00016e36
integer*4, parameter :: KS_PBUS_BAUD_45450                      = #0000b18a
integer*4, parameter :: KS_PBUS_BAUD_31250                      = #00007a12
integer*4, parameter :: KS_PBUS_BAUD_19200                      = #00004b00
integer*4, parameter :: KS_PBUS_BAUD_9600                       = #00002580

!------ Profibus events ------

integer*4, parameter :: KS_PBUS_ERROR                           = (PROFIBUS_BASE+#00000000)
integer*4, parameter :: KS_PBUS_DATASET_SIGNAL                  = (PROFIBUS_BASE+#00000001)
integer*4, parameter :: KS_PBUS_TOPOLOGY_CHANGE                 = (PROFIBUS_BASE+#00000002)
integer*4, parameter :: KS_PBUS_ALARM                           = (PROFIBUS_BASE+#00000003)
integer*4, parameter :: KS_PBUS_DIAGNOSTIC                      = (PROFIBUS_BASE+#00000004)

!------ Profibus alarm types ------

integer*4, parameter :: KS_PBUS_DIAGNOSTIC_ALARM                = #00000001
integer*4, parameter :: KS_PBUS_PROCESS_ALARM                   = #00000002
integer*4, parameter :: KS_PBUS_PULL_ALARM                      = #00000003
integer*4, parameter :: KS_PBUS_PLUG_ALARM                      = #00000004
integer*4, parameter :: KS_PBUS_STATUS_ALARM                    = #00000005
integer*4, parameter :: KS_PBUS_UPDATE_ALARM                    = #00000006

!------ Profibus topology change reasons ------

integer*4, parameter :: KS_PBUS_TOPOLOGY_MASTER_CONNECTED       = #00000000
integer*4, parameter :: KS_PBUS_TOPOLOGY_MASTER_DISCONNECTED    = #00000001
integer*4, parameter :: KS_PBUS_TOPOLOGY_SLAVE_COUNT_CHANGED    = #00000002
integer*4, parameter :: KS_PBUS_TOPOLOGY_SLAVE_ONLINE           = #00000003
integer*4, parameter :: KS_PBUS_TOPOLOGY_SLAVE_OFFLINE          = #00000004

type PbusErrorUserContext
  integer*4 ctxType
  integer*4 hMaster
  integer*4 hSlave
  integer*4 error
end type

type PbusDataSetUserContext
  integer*4 ctxType
  integer*4 hDataSet
end type

type PbusTopologyUserContext
  integer*4 ctxType
  integer*4 hMaster
  integer*4 hSlave
  integer*4 slavesOnline
  integer*4 slavesConfigured
  integer*4 reason
end type

type PbusAlarmUserContext
  integer*4 ctxType
  integer*4 hMaster
  integer*4 hSlave
  integer*4 slaveAddress
  integer*4 alarmType
  integer*4 slotNumber
  integer*4 specifier
  integer*4 diagnosticLength
  integer*4 diagnosticData(0:59)
end type

type PbusDiagnosticUserContext
  integer*4 ctxType
  integer*4 hMaster
  integer*4 hSlave
  integer*4 slaveAddress
end type

type KSPbusMasterState
  integer*4 structSize
  integer*4 connected
  integer*4 slavesOnline
  integer*4 highestStationAddr
  integer*4 slavesConfigured
  integer*4 masterState
  integer*4 busErrorCounter
  integer*4 timeoutCounter
  integer*4 lastError
end type

type KSPbusSlaveState
  integer*4 structSize
  integer*4 address
  integer*4 identNumber
  integer*4 configured
  integer*4 assigned
  integer*4 state
  byte stdStationStatus(0:5)
  byte extendedDiag(0:237)
end type

type KSPbusDataVarInfo
  integer*4 dataType
  integer*4 subIndex
  integer*4 byteLength
  integer*4 name
end type

type KSPbusSlotInfo
  integer*4 slotNumber
  integer*4 varCount
  integer*4 byteLength
  integer*4 name
  integer*4 vars(0:0)
end type

type KSPbusSlaveInfo
  integer*4 address
  integer*4 identNumber
  integer*4 slotCount
  integer*4 byteLength
  integer*4 name
  integer*4 slots(0:0)
end type

!------ Master functions ------

interface

integer*4 function KS_createPbusMaster(phMaster, name, libraryPath, topologyFile, flags)
  !dec$ attributes stdcall, alias : '_KS_createPbusMaster@20' :: KS_createPbusMaster
  integer*4 phMaster
  integer*4 name
  integer*4 libraryPath
  integer*4 topologyFile
  integer*4 flags
end function

integer*4 function KS_closePbusMaster(hMaster)
  !dec$ attributes stdcall, alias : '_KS_closePbusMaster@4' :: KS_closePbusMaster
  integer*4 hMaster
end function

integer*4 function KS_queryPbusMasterState(hMaster, pMasterState, flags)
  !dec$ attributes stdcall, alias : '_KS_queryPbusMasterState@12' :: KS_queryPbusMasterState
  integer*4 hMaster
  integer*4 pMasterState
  integer*4 flags
end function

integer*4 function KS_changePbusMasterState(hMaster, state, flags)
  !dec$ attributes stdcall, alias : '_KS_changePbusMasterState@12' :: KS_changePbusMasterState
  integer*4 hMaster
  integer*4 state
  integer*4 flags
end function

integer*4 function KS_enumPbusSlaves(hMaster, index, pSlaveState, flags)
  !dec$ attributes stdcall, alias : '_KS_enumPbusSlaves@16' :: KS_enumPbusSlaves
  integer*4 hMaster
  integer*4 index
  integer*4 pSlaveState
  integer*4 flags
end function

integer*4 function KS_installPbusMasterHandler(hMaster, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installPbusMasterHandler@16' :: KS_installPbusMasterHandler
  integer*4 hMaster
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_execPbusMasterCommand(hMaster, command, pParam, flags)
  !dec$ attributes stdcall, alias : '_KS_execPbusMasterCommand@16' :: KS_execPbusMasterCommand
  integer*4 hMaster
  integer*4 command
  integer*4 pParam
  integer*4 flags
end function

end interface

!------ Slave functions ------

interface

integer*4 function KS_createPbusSlave(hMaster, phSlave, address, flags)
  !dec$ attributes stdcall, alias : '_KS_createPbusSlave@16' :: KS_createPbusSlave
  integer*4 hMaster
  integer*4 phSlave
  integer*4 address
  integer*4 flags
end function

integer*4 function KS_deletePbusSlave(hSlave)
  !dec$ attributes stdcall, alias : '_KS_deletePbusSlave@4' :: KS_deletePbusSlave
  integer*4 hSlave
end function

integer*4 function KS_queryPbusSlaveState(hSlave, pSlaveState, flags)
  !dec$ attributes stdcall, alias : '_KS_queryPbusSlaveState@12' :: KS_queryPbusSlaveState
  integer*4 hSlave
  integer*4 pSlaveState
  integer*4 flags
end function

integer*4 function KS_changePbusSlaveState(hSlave, state, flags)
  !dec$ attributes stdcall, alias : '_KS_changePbusSlaveState@12' :: KS_changePbusSlaveState
  integer*4 hSlave
  integer*4 state
  integer*4 flags
end function

integer*4 function KS_queryPbusSlaveInfo(hSlave, ppSlaveInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_queryPbusSlaveInfo@12' :: KS_queryPbusSlaveInfo
  integer*4 hSlave
  integer*4 ppSlaveInfo
  integer*4 flags
end function

integer*4 function KS_queryPbusSlotInfo(hSlave, objIndex, ppSlotInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_queryPbusSlotInfo@16' :: KS_queryPbusSlotInfo
  integer*4 hSlave
  integer*4 objIndex
  integer*4 ppSlotInfo
  integer*4 flags
end function

integer*4 function KS_queryPbusDataVarInfo(hSlave, objIndex, varIndex, ppDataVarInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_queryPbusDataVarInfo@20' :: KS_queryPbusDataVarInfo
  integer*4 hSlave
  integer*4 objIndex
  integer*4 varIndex
  integer*4 ppDataVarInfo
  integer*4 flags
end function

end interface

!------ Data exchange functions ------

interface

integer*4 function KS_createPbusDataSet(hMaster, phSet, ppAppPtr, ppSysPtr, flags)
  !dec$ attributes stdcall, alias : '_KS_createPbusDataSet@20' :: KS_createPbusDataSet
  integer*4 hMaster
  integer*4 phSet
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 flags
end function

integer*4 function KS_deletePbusDataSet(hSet)
  !dec$ attributes stdcall, alias : '_KS_deletePbusDataSet@4' :: KS_deletePbusDataSet
  integer*4 hSet
end function

integer*4 function KS_assignPbusDataSet(hSet, hSlave, slot, index, placement, flags)
  !dec$ attributes stdcall, alias : '_KS_assignPbusDataSet@24' :: KS_assignPbusDataSet
  integer*4 hSet
  integer*4 hSlave
  integer*4 slot
  integer*4 index
  integer*4 placement
  integer*4 flags
end function

integer*4 function KS_readPbusDataSet(hSet, flags)
  !dec$ attributes stdcall, alias : '_KS_readPbusDataSet@8' :: KS_readPbusDataSet
  integer*4 hSet
  integer*4 flags
end function

integer*4 function KS_postPbusDataSet(hSet, flags)
  !dec$ attributes stdcall, alias : '_KS_postPbusDataSet@8' :: KS_postPbusDataSet
  integer*4 hSet
  integer*4 flags
end function

integer*4 function KS_installPbusDataSetHandler(hSet, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installPbusDataSetHandler@16' :: KS_installPbusDataSetHandler
  integer*4 hSet
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

integer*4 function KS_getPbusDataObjAddress(hSet, hSlave, slot, index, ppAppPtr, ppSysPtr, pBitOffset, pBitLength, flags)
  !dec$ attributes stdcall, alias : '_KS_getPbusDataObjAddress@36' :: KS_getPbusDataObjAddress
  integer*4 hSet
  integer*4 hSlave
  integer*4 slot
  integer*4 index
  integer*4 ppAppPtr
  integer*4 ppSysPtr
  integer*4 pBitOffset
  integer*4 pBitLength
  integer*4 flags
end function

integer*4 function KS_readPbusDataObj(hSlave, slot, index, pData, pSize, flags)
  !dec$ attributes stdcall, alias : '_KS_readPbusDataObj@24' :: KS_readPbusDataObj
  integer*4 hSlave
  integer*4 slot
  integer*4 index
  integer*4 pData
  integer*4 pSize
  integer*4 flags
end function

integer*4 function KS_postPbusDataObj(hSlave, slot, index, pData, size, flags)
  !dec$ attributes stdcall, alias : '_KS_postPbusDataObj@24' :: KS_postPbusDataObj
  integer*4 hSlave
  integer*4 slot
  integer*4 index
  integer*4 pData
  integer*4 size
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! RTL Module
!---------------------------------------------------------------------------------------------------------------

!------ Mathematical constants ------

real*8, parameter :: KS_PI                                      = 3.14159265358979323846
real*8, parameter :: KS_E                                       = 2.71828182845904523536

!------ stdlib.h replacements ------

interface

integer*4 function KSRTL_calloc(num, size)
  !dec$ attributes stdcall, alias : '_KSRTL_calloc@8' :: KSRTL_calloc
  integer*4 num
  integer*4 size
end function

subroutine KSRTL_free(ptr)
  !dec$ attributes stdcall, alias : '_KSRTL_free@4' :: KSRTL_free
  integer*4 ptr
end subroutine

integer*4 function KSRTL_malloc(size)
  !dec$ attributes stdcall, alias : '_KSRTL_malloc@4' :: KSRTL_malloc
  integer*4 size
end function

end interface

!------ math.h replacements ------

interface

real*8 function KSRTL_sin(x)
  !dec$ attributes stdcall, alias : '_KSRTL_sin@4' :: KSRTL_sin
  real*8 x
end function

real*8 function KSRTL_cos(x)
  !dec$ attributes stdcall, alias : '_KSRTL_cos@4' :: KSRTL_cos
  real*8 x
end function

real*8 function KSRTL_tan(x)
  !dec$ attributes stdcall, alias : '_KSRTL_tan@4' :: KSRTL_tan
  real*8 x
end function

real*8 function KSRTL_asin(x)
  !dec$ attributes stdcall, alias : '_KSRTL_asin@4' :: KSRTL_asin
  real*8 x
end function

real*8 function KSRTL_acos(x)
  !dec$ attributes stdcall, alias : '_KSRTL_acos@4' :: KSRTL_acos
  real*8 x
end function

real*8 function KSRTL_atan(x)
  !dec$ attributes stdcall, alias : '_KSRTL_atan@4' :: KSRTL_atan
  real*8 x
end function

real*8 function KSRTL_atan2(y, x)
  !dec$ attributes stdcall, alias : '_KSRTL_atan2@8' :: KSRTL_atan2
  real*8 y
  real*8 x
end function

real*8 function KSRTL_sinh(x)
  !dec$ attributes stdcall, alias : '_KSRTL_sinh@4' :: KSRTL_sinh
  real*8 x
end function

real*8 function KSRTL_cosh(x)
  !dec$ attributes stdcall, alias : '_KSRTL_cosh@4' :: KSRTL_cosh
  real*8 x
end function

real*8 function KSRTL_tanh(x)
  !dec$ attributes stdcall, alias : '_KSRTL_tanh@4' :: KSRTL_tanh
  real*8 x
end function

real*8 function KSRTL_exp(x)
  !dec$ attributes stdcall, alias : '_KSRTL_exp@4' :: KSRTL_exp
  real*8 x
end function

real*8 function KSRTL_frexp(x, exp)
  !dec$ attributes stdcall, alias : '_KSRTL_frexp@8' :: KSRTL_frexp
  real*8 x
  integer*4 exp
end function

real*8 function KSRTL_ldexp(x, exp)
  !dec$ attributes stdcall, alias : '_KSRTL_ldexp@8' :: KSRTL_ldexp
  real*8 x
  integer*4 exp
end function

real*8 function KSRTL_log(x)
  !dec$ attributes stdcall, alias : '_KSRTL_log@4' :: KSRTL_log
  real*8 x
end function

real*8 function KSRTL_log10(x)
  !dec$ attributes stdcall, alias : '_KSRTL_log10@4' :: KSRTL_log10
  real*8 x
end function

real*8 function KSRTL_modf(x, intpart)
  !dec$ attributes stdcall, alias : '_KSRTL_modf@8' :: KSRTL_modf
  real*8 x
  integer*4 intpart
end function

real*8 function KSRTL_pow(base, exponent)
  !dec$ attributes stdcall, alias : '_KSRTL_pow@8' :: KSRTL_pow
  real*8 base
  real*8 exponent
end function

real*8 function KSRTL_sqrt(x)
  !dec$ attributes stdcall, alias : '_KSRTL_sqrt@4' :: KSRTL_sqrt
  real*8 x
end function

real*8 function KSRTL_fabs(x)
  !dec$ attributes stdcall, alias : '_KSRTL_fabs@4' :: KSRTL_fabs
  real*8 x
end function

real*8 function KSRTL_ceil(x)
  !dec$ attributes stdcall, alias : '_KSRTL_ceil@4' :: KSRTL_ceil
  real*8 x
end function

real*8 function KSRTL_floor(x)
  !dec$ attributes stdcall, alias : '_KSRTL_floor@4' :: KSRTL_floor
  real*8 x
end function

real*8 function KSRTL_fmod(numerator, denominator)
  !dec$ attributes stdcall, alias : '_KSRTL_fmod@8' :: KSRTL_fmod
  real*8 numerator
  real*8 denominator
end function

end interface

!------ string.h replacements ------

interface

integer*4 function KSRTL_memchr(ptr, value, num)
  !dec$ attributes stdcall, alias : '_KSRTL_memchr@12' :: KSRTL_memchr
  integer*4 ptr
  integer*4 value
  integer*4 num
end function

integer*4 function KSRTL_memcmp(ptr1, ptr2, num)
  !dec$ attributes stdcall, alias : '_KSRTL_memcmp@12' :: KSRTL_memcmp
  integer*4 ptr1
  integer*4 ptr2
  integer*4 num
end function

integer*4 function KSRTL_memcpy(destination, source, num)
  !dec$ attributes stdcall, alias : '_KSRTL_memcpy@12' :: KSRTL_memcpy
  integer*4 destination
  integer*4 source
  integer*4 num
end function

integer*4 function KSRTL_memmove(destination, source, num)
  !dec$ attributes stdcall, alias : '_KSRTL_memmove@12' :: KSRTL_memmove
  integer*4 destination
  integer*4 source
  integer*4 num
end function

integer*4 function KSRTL_memset(ptr, value, num)
  !dec$ attributes stdcall, alias : '_KSRTL_memset@12' :: KSRTL_memset
  integer*4 ptr
  integer*4 value
  integer*4 num
end function

integer*4 function KSRTL_strlen(str)
  !dec$ attributes stdcall, alias : '_KSRTL_strlen@4' :: KSRTL_strlen
  integer*4 str
end function

integer*4 function KSRTL_strcmp(str1, str2)
  !dec$ attributes stdcall, alias : '_KSRTL_strcmp@8' :: KSRTL_strcmp
  integer*4 str1
  integer*4 str2
end function

integer*4 function KSRTL_strncmp(str1, str2, num)
  !dec$ attributes stdcall, alias : '_KSRTL_strncmp@12' :: KSRTL_strncmp
  integer*4 str1
  integer*4 str2
  integer*4 num
end function

integer*4 function KSRTL_strcpy(destination, source)
  !dec$ attributes stdcall, alias : '_KSRTL_strcpy@8' :: KSRTL_strcpy
  integer*4 destination
  integer*4 source
end function

integer*4 function KSRTL_strncpy(destination, source, num)
  !dec$ attributes stdcall, alias : '_KSRTL_strncpy@12' :: KSRTL_strncpy
  integer*4 destination
  integer*4 source
  integer*4 num
end function

integer*4 function KSRTL_strcat(destination, source)
  !dec$ attributes stdcall, alias : '_KSRTL_strcat@8' :: KSRTL_strcat
  integer*4 destination
  integer*4 source
end function

integer*4 function KSRTL_strncat(destination, source, num)
  !dec$ attributes stdcall, alias : '_KSRTL_strncat@12' :: KSRTL_strncat
  integer*4 destination
  integer*4 source
  integer*4 num
end function

integer*4 function KSRTL_strchr(str, character)
  !dec$ attributes stdcall, alias : '_KSRTL_strchr@8' :: KSRTL_strchr
  integer*4 str
  integer*4 character
end function

integer*4 function KSRTL_strcoll(str1, str2)
  !dec$ attributes stdcall, alias : '_KSRTL_strcoll@8' :: KSRTL_strcoll
  integer*4 str1
  integer*4 str2
end function

integer*4 function KSRTL_strrchr(str1, character)
  !dec$ attributes stdcall, alias : '_KSRTL_strrchr@8' :: KSRTL_strrchr
  integer*4 str1
  integer*4 character
end function

integer*4 function KSRTL_strstr(str1, str2)
  !dec$ attributes stdcall, alias : '_KSRTL_strstr@8' :: KSRTL_strstr
  integer*4 str1
  integer*4 str2
end function

integer*4 function KSRTL_strspn(str1, str2)
  !dec$ attributes stdcall, alias : '_KSRTL_strspn@8' :: KSRTL_strspn
  integer*4 str1
  integer*4 str2
end function

integer*4 function KSRTL_strcspn(str1, str2)
  !dec$ attributes stdcall, alias : '_KSRTL_strcspn@8' :: KSRTL_strcspn
  integer*4 str1
  integer*4 str2
end function

integer*4 function KSRTL_strpbrk(str1, str2)
  !dec$ attributes stdcall, alias : '_KSRTL_strpbrk@8' :: KSRTL_strpbrk
  integer*4 str1
  integer*4 str2
end function

integer*4 function KSRTL_strtok(str, delimiters)
  !dec$ attributes stdcall, alias : '_KSRTL_strtok@8' :: KSRTL_strtok
  integer*4 str
  integer*4 delimiters
end function

integer*4 function KSRTL_strxfrm(destination, source, num)
  !dec$ attributes stdcall, alias : '_KSRTL_strxfrm@12' :: KSRTL_strxfrm
  integer*4 destination
  integer*4 source
  integer*4 num
end function

integer*4 function KSRTL_vsprintf(buffer, format, pArgs)
  !dec$ attributes stdcall, alias : '_KSRTL_vsprintf@12' :: KSRTL_vsprintf
  integer*4 buffer
  integer*4 format
  integer*4 pArgs
end function

integer*4 function KSRTL_vsnprintf(buffer, n, format, pArgs)
  !dec$ attributes stdcall, alias : '_KSRTL_vsnprintf@16' :: KSRTL_vsnprintf
  integer*4 buffer
  integer*4 n
  integer*4 format
  integer*4 pArgs
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Camera Module
!---------------------------------------------------------------------------------------------------------------

!------ Flags ------

integer*4, parameter :: KSF_NO_DISCOVERY                        = #00000800
integer*4, parameter :: KSF_CAMERA_NO_GENICAM                   = #00001000
integer*4, parameter :: KSF_CAMERA_BROADCAST_DISCOVERY          = #00800000

type KSCameraInfo
  integer*4 structSize
  byte hardwareId(0:79)
  byte vendor(0:79)
  byte name(0:79)
  byte serialNumber(0:79)
  byte userName(0:79)
end type

type KSCameraTimeouts
  integer*4 structSize
  integer*4 commandTimeout
  integer*4 commandRetries
  integer*4 heartbeatTimeout
end type

type KSCameraForceIp
  integer*4 structSize
  byte macAddress(0:17)
  integer*4 address
  integer*4 subnetMask
  integer*4 gatewayAddress
end type

type KSCameraInfoGev
  integer*4 structSize
  byte vendor(0:31)
  byte name(0:31)
  byte serialNumber(0:15)
  byte userName(0:15)
  byte macAddress(0:17)
  integer*4 instance
  byte instanceName(0:255)
end type

type KSCameraEvent
  integer*4 structSize
  integer*4 eventId
  integer*8 timestamp
  integer*8 blockId
  integer*4 channel
  integer*4 size
  integer*4 pAppPtr
  integer*4 pSysPtr
end type

!------ Block type structures ------

type KSCameraBlock
  integer*4 blockType
  integer*4 pAppPtr
  integer*4 pSysPtr
  integer*8 id
  integer*8 timestamp
end type

type KSCameraImage
  integer*4 blockType
  integer*4 pAppPtr
  integer*4 pSysPtr
  integer*8 id
  integer*8 timestamp
  integer*4 fieldCount
  integer*4 fieldId
  integer*4 pixelFormat
  integer*4 width
  integer*4 height
  integer*4 offsetX
  integer*4 offsetY
  integer*4 linePadding
  integer*4 imagePadding
  integer*4 chunkDataPayloadLength
  integer*4 chunkLayoutId
end type

type KSCameraJpeg
  integer*4 blockType
  integer*4 pAppPtr
  integer*4 pSysPtr
  integer*8 id
  integer*8 timestamp
  integer*4 fieldCount
  integer*4 fieldId
  integer*8 jpegPayloadSize
  integer*8 tickFrequency
  integer*4 dataFormat
  integer*4 isColorSpace
  integer*4 chunkDataPayloadLength
  integer*4 chunkLayoutId
end type

type KSCameraChunk
  integer*4 blockType
  integer*4 pAppPtr
  integer*4 pSysPtr
  integer*8 id
  integer*8 timestamp
  integer*4 chunkDataPayloadLength
  integer*4 chunkLayoutId
end type

!------ Block types ------

integer*4, parameter :: KS_CAMERA_BLOCKTYPE_NONE                = #00000000
integer*4, parameter :: KS_CAMERA_BLOCKTYPE_IMAGE               = #00000001
integer*4, parameter :: KS_CAMERA_BLOCKTYPE_CHUNK               = #00000002
integer*4, parameter :: KS_CAMERA_BLOCKTYPE_JPEG                = #00000003
integer*4, parameter :: KS_CAMERA_BLOCKTYPE_JPEG2000            = #00000004

!------ Camera commands ------

integer*4, parameter :: KS_CAMERA_READREG                       = #00000001
integer*4, parameter :: KS_CAMERA_READMEM                       = #00000002
integer*4, parameter :: KS_CAMERA_WRITEREG                      = #00000003
integer*4, parameter :: KS_CAMERA_SET_TIMEOUTS                  = #00000004
integer*4, parameter :: KS_CAMERA_GET_TIMEOUTS                  = #00000005
integer*4, parameter :: KS_CAMERA_GET_XML_FILE                  = #00000006
integer*4, parameter :: KS_CAMERA_GET_XML_INFO                  = #00000007
integer*4, parameter :: KS_CAMERA_SET_DISCOVERY_INTERVAL        = #00000008
integer*4, parameter :: KS_CAMERA_RESIZE_BUFFERS                = #00000009
integer*4, parameter :: KS_CAMERA_GET_INFO                      = #0000000a
integer*4, parameter :: KS_CAMERA_SET_CHUNK_DATA                = #0000000b
integer*4, parameter :: KS_CAMERA_SEND_TO_SOURCE_PORT           = #0000000c
integer*4, parameter :: KS_CAMERA_FORCEIP                       = #0000000d
integer*4, parameter :: KS_CAMERA_WRITEMEM                      = #0000000e
integer*4, parameter :: KS_CAMERA_RECV_EVENT                    = #00000011

!------ Acquisition modes ------

integer*4, parameter :: KS_CAMERA_CONTINUOUS                    = #ffffffff
integer*4, parameter :: KS_CAMERA_PRESERVE                      = #00000000
integer*4, parameter :: KS_CAMERA_SINGLE_FRAME                  = #00000001
integer*4, parameter :: KS_CAMERA_MULTI_FRAME                   = #00000000

!------ Configuration commands ------

integer*4, parameter :: KS_CAMERA_CONFIG_GET                    = #00000001
integer*4, parameter :: KS_CAMERA_CONFIG_SET                    = #00000002
integer*4, parameter :: KS_CAMERA_CONFIG_TYPE                   = #00000003
integer*4, parameter :: KS_CAMERA_CONFIG_ENUMERATE              = #00000004
integer*4, parameter :: KS_CAMERA_CONFIG_MIN                    = #00000005
integer*4, parameter :: KS_CAMERA_CONFIG_MAX                    = #00000006
integer*4, parameter :: KS_CAMERA_CONFIG_INC                    = #00000007
integer*4, parameter :: KS_CAMERA_CONFIG_LENGTH                 = #00010000

!------ Configuration types ------

integer*4, parameter :: KS_CAMERA_CONFIG_CATEGORY               = #00000001
integer*4, parameter :: KS_CAMERA_CONFIG_BOOLEAN                = #00000002
integer*4, parameter :: KS_CAMERA_CONFIG_ENUMERATION            = #00000003
integer*4, parameter :: KS_CAMERA_CONFIG_INTEGER                = #00000004
integer*4, parameter :: KS_CAMERA_CONFIG_COMMAND                = #00000005
integer*4, parameter :: KS_CAMERA_CONFIG_FLOAT                  = #00000006
integer*4, parameter :: KS_CAMERA_CONFIG_STRING                 = #00000007
integer*4, parameter :: KS_CAMERA_CONFIG_REGISTER               = #00000008

!------ Receive and error handler types ------

integer*4, parameter :: KS_CAMERA_IMAGE_RECEIVED                = #00000001
integer*4, parameter :: KS_CAMERA_IMAGE_DROPPED                 = #00000002
integer*4, parameter :: KS_CAMERA_ERROR                         = #00000003
integer*4, parameter :: KS_CAMERA_GEV_ATTACHED                  = #00000004
integer*4, parameter :: KS_CAMERA_GEV_DETACHED                  = #00000005
integer*4, parameter :: KS_CAMERA_ATTACHED                      = #00000006
integer*4, parameter :: KS_CAMERA_DETACHED                      = #00000007
integer*4, parameter :: KS_CAMERA_EVENT                         = #00000008

!------ Error codes ------

integer*4, parameter :: KSERROR_CAMERA_COMMAND_ERROR            = (KSERROR_CATEGORY_CAMERA+#00010000)
integer*4, parameter :: KSERROR_CAMERA_COMMAND_TIMEOUT          = (KSERROR_CATEGORY_CAMERA+#00020000)
integer*4, parameter :: KSERROR_CAMERA_COMMAND_FAILED           = (KSERROR_CATEGORY_CAMERA+#00030000)
integer*4, parameter :: KSERROR_CAMERA_COMMAND_BAD_ALIGNMENT    = (KSERROR_CATEGORY_CAMERA+#00040000)
integer*4, parameter :: KSERROR_CAMERA_RECONNECTED              = (KSERROR_CATEGORY_CAMERA+#000f0000)
integer*4, parameter :: KSERROR_CAMERA_STREAM_INVALID_DATA      = (KSERROR_CATEGORY_CAMERA+#00100000)
integer*4, parameter :: KSERROR_CAMERA_STREAM_NO_BUFFER         = (KSERROR_CATEGORY_CAMERA+#00110000)
integer*4, parameter :: KSERROR_CAMERA_STREAM_NOT_TRANSMITTED   = (KSERROR_CATEGORY_CAMERA+#00120000)
integer*4, parameter :: KSERROR_CAMERA_STREAM_INCOMPLETE_TRANSMISSION 
                                                                = (KSERROR_CATEGORY_CAMERA+#00130000)
integer*4, parameter :: KSERROR_CAMERA_STREAM_BUFFER_OVERRUN    = (KSERROR_CATEGORY_CAMERA+#00140000)
integer*4, parameter :: KSERROR_CAMERA_STREAM_BUFFER_QUEUED     = (KSERROR_CATEGORY_CAMERA+#00150000)
integer*4, parameter :: KSERROR_GENICAM_ERROR                   = (KSERROR_CATEGORY_CAMERA+#00800000)
integer*4, parameter :: KSERROR_GENICAM_FEATURE_NOT_IMPLEMENTED = (KSERROR_CATEGORY_CAMERA+#00810000)
integer*4, parameter :: KSERROR_GENICAM_FEATURE_NOT_AVAILABLE   = (KSERROR_CATEGORY_CAMERA+#00820000)
integer*4, parameter :: KSERROR_GENICAM_FEATURE_ACCESS_DENIED   = (KSERROR_CATEGORY_CAMERA+#00830000)
integer*4, parameter :: KSERROR_GENICAM_FEATURE_NOT_FOUND       = (KSERROR_CATEGORY_CAMERA+#00840000)
integer*4, parameter :: KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND    = (KSERROR_CATEGORY_CAMERA+#00850000)
integer*4, parameter :: KSERROR_GENICAM_SUBNODE_NOT_REACHABLE   = (KSERROR_CATEGORY_CAMERA+#00860000)
integer*4, parameter :: KSERROR_GENICAM_PORT_NOT_REACHABLE      = (KSERROR_CATEGORY_CAMERA+#00870000)
integer*4, parameter :: KSERROR_GENICAM_FEATURE_NOT_SUPPORTED   = (KSERROR_CATEGORY_CAMERA+#00880000)
integer*4, parameter :: KSERROR_GENICAM_VALUE_TOO_LARGE         = (KSERROR_CATEGORY_CAMERA+#00890000)
integer*4, parameter :: KSERROR_GENICAM_VALUE_TOO_SMALL         = (KSERROR_CATEGORY_CAMERA+#008a0000)
integer*4, parameter :: KSERROR_GENICAM_VALUE_BAD_INCREMENT     = (KSERROR_CATEGORY_CAMERA+#008b0000)
integer*4, parameter :: KSERROR_GENICAM_BAD_REGISTER_DESCRIPTION 
                                                                = (KSERROR_CATEGORY_CAMERA+#008c0000)

!------ GigE Vision<sup>®</sup> registers ------

integer*4, parameter :: KS_CAMERA_REGISTER_STREAM_CHANNELS      = #00000904

!------ Camera module config ------

integer*4, parameter :: KSCONFIG_OVERRIDE_CAMERA_XML_FILE       = #00000001

!------ GigE Vision<sup>®</sup> pixel formats ------

integer*4, parameter :: KS_CAMERA_PIXEL_MONO_8                  = #01080001
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_8S                 = #01080002
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_10                 = #01100003
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_10_PACKED          = #010c0004
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_12                 = #01100005
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_12_PACKED          = #010c0006
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_16                 = #01100007
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GR_8              = #01080008
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_RG_8              = #01080009
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GB_8              = #0108000a
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_BG_8              = #0108000b
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GR_10             = #0110000c
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_RG_10             = #0110000d
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GB_10             = #0110000e
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_BG_10             = #0110000f
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GR_12             = #01100010
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_RG_12             = #01100011
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GB_12             = #01100012
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_BG_12             = #01100013
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_8                   = #02180014
integer*4, parameter :: KS_CAMERA_PIXEL_BGR_8                   = #02180015
integer*4, parameter :: KS_CAMERA_PIXEL_RGBA_8                  = #02200016
integer*4, parameter :: KS_CAMERA_PIXEL_BGRA_8                  = #02200017
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_10                  = #02300018
integer*4, parameter :: KS_CAMERA_PIXEL_BGR_10                  = #02300019
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_12                  = #0230001a
integer*4, parameter :: KS_CAMERA_PIXEL_BGR_12                  = #0230001b
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_10_V1_PACKED        = #0220001c
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_10_P_32             = #0220001d
integer*4, parameter :: KS_CAMERA_PIXEL_YUV_411_8_UYYVYY        = #020c001e
integer*4, parameter :: KS_CAMERA_PIXEL_YUV_422_8_UYVY          = #0210001f
integer*4, parameter :: KS_CAMERA_PIXEL_YUV_8_UYV               = #02180020
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_8_PLANAR            = #02180021
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_10_PLANAR           = #02300022
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_12_PLANAR           = #02300023
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_16_PLANAR           = #02300024
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_14                 = #01100025
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GR_10_PACKED      = #010c0026
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_RG_10_PACKED      = #010c0027
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GB_10_PACKED      = #010c0028
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_BG_10_PACKED      = #010c0029
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GR_12_PACKED      = #010c002a
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_RG_12_PACKED      = #010c002b
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GB_12_PACKED      = #010c002c
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_BG_12_PACKED      = #010c002d
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GR_16             = #0110002e
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_RG_16             = #0110002f
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_GB_16             = #01100030
integer*4, parameter :: KS_CAMERA_PIXEL_BAYER_BG_16             = #01100031
integer*4, parameter :: KS_CAMERA_PIXEL_YUV_422_8               = #020c0032
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_16                  = #02300033
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_12_V1_PACKED        = #02240034
integer*4, parameter :: KS_CAMERA_PIXEL_RGB_565_P               = #02100035
integer*4, parameter :: KS_CAMERA_PIXEL_BGR_565_P               = #02100036
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_1P                 = #01010037
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_2P                 = #01020038
integer*4, parameter :: KS_CAMERA_PIXEL_MONO_4P                 = #01040039
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_8_CBYCR           = #0218003a
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_422_8             = #0210003b
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_411_8_CBYYCRYY    = #020c003c
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_601_8_CBYCR       = #0218003d
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_601_422_8         = #0210003e
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_601_411_8_CBYYCRYY 
                                                                = #020c003f
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_709_8_CBYCR       = #02180040
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_709_422_8         = #02100041
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_709_411_8_CBYYCRYY 
                                                                = #020c0042
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_422_8_CBYCRY      = #02100043
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_601_422_8_CBYCRY  = #02100044
integer*4, parameter :: KS_CAMERA_PIXEL_YCBCR_709_422_8_CBYCRY  = #02100045

!------ Context structures ------

type KSCameraRecvImageContext
  integer*4 ctxType
  integer*4 hStream
  integer*4 hCamera
end type

type KSCameraDroppedImageContext
  integer*4 ctxType
  integer*4 hStream
  integer*4 hCamera
  integer*8 imageId
end type

type KSCameraErrorContext
  integer*4 ctxType
  integer*4 hCamera
  integer*4 error
end type

type KSCameraAttachContext
  integer*4 ctxType
  integer*4 hController
  type (KSCameraInfo) cameraInfo
end type

type KSCameraGevAttachContext
  integer*4 ctxType
  integer*4 hAdapter
  type (KSCameraInfoGev) cameraInfo
end type

type KSCameraEventContext
  integer*4 ctxType
  integer*4 hCamera
end type

!------ Basic functions ------

interface

integer*4 function KS_enumCameras(hObject, index, pCameraInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_enumCameras@16' :: KS_enumCameras
  integer*4 hObject
  integer*4 index
  integer*4 pCameraInfo
  integer*4 flags
end function

integer*4 function KS_execCameraCommand(hObject, command, address, pData, pLength, flags)
  !dec$ attributes stdcall, alias : '_KS_execCameraCommand@24' :: KS_execCameraCommand
  integer*4 hObject
  integer*4 command
  integer*4 address
  integer*4 pData
  integer*4 pLength
  integer*4 flags
end function

integer*4 function KS_installCameraHandler(hHandle, eventType, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installCameraHandler@16' :: KS_installCameraHandler
  integer*4 hHandle
  integer*4 eventType
  integer*4 hSignal
  integer*4 flags
end function

end interface

!------ Camera connection functions ------

interface

integer*4 function KS_openCamera(phCamera, hObject, pHardwareId, flags)
  !dec$ attributes stdcall, alias : '_KS_openCamera@16' :: KS_openCamera
  integer*4 phCamera
  integer*4 hObject
  integer*4 pHardwareId
  integer*4 flags
end function

integer*4 function KS_openCameraEx(phCamera, hObject, pCameraInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_openCameraEx@16' :: KS_openCameraEx
  integer*4 phCamera
  integer*4 hObject
  integer*4 pCameraInfo
  integer*4 flags
end function

integer*4 function KS_closeCamera(hCamera, flags)
  !dec$ attributes stdcall, alias : '_KS_closeCamera@8' :: KS_closeCamera
  integer*4 hCamera
  integer*4 flags
end function

integer*4 function KS_readCameraMem(hCamera, address, pData, length, flags)
  !dec$ attributes stdcall, alias : '_KS_readCameraMem@20' :: KS_readCameraMem
  integer*4 hCamera
  integer*8 address
  integer*4 pData
  integer*4 length
  integer*4 flags
end function

integer*4 function KS_writeCameraMem(hCamera, address, pData, length, flags)
  !dec$ attributes stdcall, alias : '_KS_writeCameraMem@20' :: KS_writeCameraMem
  integer*4 hCamera
  integer*8 address
  integer*4 pData
  integer*4 length
  integer*4 flags
end function

integer*4 function KS_configCamera(hCamera, configCommand, pName, index, pData, length, flags)
  !dec$ attributes stdcall, alias : '_KS_configCamera@28' :: KS_configCamera
  integer*4 hCamera
  integer*4 configCommand
  integer*4 pName
  integer*4 index
  integer*4 pData
  integer*4 length
  integer*4 flags
end function

integer*4 function KS_startCameraAcquisition(hCamera, acquisitionMode, flags)
  !dec$ attributes stdcall, alias : '_KS_startCameraAcquisition@12' :: KS_startCameraAcquisition
  integer*4 hCamera
  integer*4 acquisitionMode
  integer*4 flags
end function

integer*4 function KS_stopCameraAcquisition(hCamera, flags)
  !dec$ attributes stdcall, alias : '_KS_stopCameraAcquisition@8' :: KS_stopCameraAcquisition
  integer*4 hCamera
  integer*4 flags
end function

end interface

!------ Stream functions ------

interface

integer*4 function KS_createCameraStream(phStream, hCamera, channelNumber, numBuffers, bufferSize, flags)
  !dec$ attributes stdcall, alias : '_KS_createCameraStream@24' :: KS_createCameraStream
  integer*4 phStream
  integer*4 hCamera
  integer*4 channelNumber
  integer*4 numBuffers
  integer*4 bufferSize
  integer*4 flags
end function

integer*4 function KS_closeCameraStream(hStream, flags)
  !dec$ attributes stdcall, alias : '_KS_closeCameraStream@8' :: KS_closeCameraStream
  integer*4 hStream
  integer*4 flags
end function

integer*4 function KS_recvCameraImage(hStream, ppData, ppBlockInfo, flags)
  !dec$ attributes stdcall, alias : '_KS_recvCameraImage@16' :: KS_recvCameraImage
  integer*4 hStream
  integer*4 ppData
  integer*4 ppBlockInfo
  integer*4 flags
end function

integer*4 function KS_releaseCameraImage(hStream, pData, flags)
  !dec$ attributes stdcall, alias : '_KS_releaseCameraImage@12' :: KS_releaseCameraImage
  integer*4 hStream
  integer*4 pData
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! PLC Module
!---------------------------------------------------------------------------------------------------------------

!------ Error codes ------

integer*4, parameter :: KSERROR_PLC_SCAN_ERROR                  = (KSERROR_CATEGORY_PLC+#00000000)
integer*4, parameter :: KSERROR_PLC_TYPE_ERROR                  = (KSERROR_CATEGORY_PLC+#00010000)
integer*4, parameter :: KSERROR_PLC_BACKEND                     = (KSERROR_CATEGORY_PLC+#00020000)
integer*4, parameter :: KSERROR_PLC_LINKER_ERROR                = (KSERROR_CATEGORY_PLC+#00030000)
integer*4, parameter :: KSERROR_PLC_INVALID_CONFIG_NUMBER       = (KSERROR_CATEGORY_PLC+#00040000)
integer*4, parameter :: KSERROR_PLC_INVALID_RESOURCE_NUMBER     = (KSERROR_CATEGORY_PLC+#00060000)
integer*4, parameter :: KSERROR_PLC_INVALID_TASK_NUMBER         = (KSERROR_CATEGORY_PLC+#00070000)

!------ PLC languages ------

integer*4, parameter :: KS_PLC_BYTECODE                         = #00000000
integer*4, parameter :: KS_PLC_INSTRUCTION_LIST                 = #00000001
integer*4, parameter :: KS_PLC_STRUCTURED_TEXT                  = #00000002

!------ Flags ------

integer*4, parameter :: KSF_USE_KITHARA_PRIORITY                = #00002000
integer*4, parameter :: KSF_32BIT                               = #00000400
integer*4, parameter :: KSF_64BIT                               = #00000800
integer*4, parameter :: KSF_DEBUG                               = #10000000
integer*4, parameter :: KSF_PLC_START                           = #00004000
integer*4, parameter :: KSF_PLC_STOP                            = #00008000
integer*4, parameter :: KSF_IGNORE_CASE                         = #00000010

!------ PLC commands ------

integer*4, parameter :: KS_PLC_ABORT_COMPILE                    = #00000001

!------ Error levels ------

integer*4, parameter :: KS_ERROR_LEVEL_IGNORE                   = #00000000
integer*4, parameter :: KS_ERROR_LEVEL_WARNING                  = #00000001
integer*4, parameter :: KS_ERROR_LEVEL_ERROR                    = #00000002
integer*4, parameter :: KS_ERROR_LEVEL_FATAL                    = #00000003

!------ PLC compiler errors ------

integer*4, parameter :: KS_PLCERROR_NONE                        = #00000000
integer*4, parameter :: KS_PLCERROR_SYNTAX                      = #00000001
integer*4, parameter :: KS_PLCERROR_EXPECTED_ID                 = #00000002
integer*4, parameter :: KS_PLCERROR_EXPECTED_RESCOURCE_ID       = #00000003
integer*4, parameter :: KS_PLCERROR_EXPECTED_PROG_ID            = #00000004
integer*4, parameter :: KS_PLCERROR_EXPECTED_FB_ID              = #00000005
integer*4, parameter :: KS_PLCERROR_EXPECTED_FB_INSTANCE        = #00000006
integer*4, parameter :: KS_PLCERROR_EXPECTED_FUNCTION_ID        = #00000007
integer*4, parameter :: KS_PLCERROR_EXPECTED_SINGLE_ID          = #00000008
integer*4, parameter :: KS_PLCERROR_EXPECTED_INTEGER            = #00000009
integer*4, parameter :: KS_PLCERROR_EXPECTED_STRUCT             = #0000000a
integer*4, parameter :: KS_PLCERROR_EXPECTED_STMT               = #0000000b
integer*4, parameter :: KS_PLCERROR_EXPECTED_OPERAND            = #0000000c
integer*4, parameter :: KS_PLCERROR_EXPECTED_VAR                = #0000000d
integer*4, parameter :: KS_PLCERROR_EXPECTED_TYPE               = #0000000e
integer*4, parameter :: KS_PLCERROR_EXPECTED_STRING             = #0000000f
integer*4, parameter :: KS_PLCERROR_EXPECTED_VALUE              = #00000010
integer*4, parameter :: KS_PLCERROR_EXPECTED_NUMERAL            = #00000011
integer*4, parameter :: KS_PLCERROR_EXPECTED_BOOL               = #00000012
integer*4, parameter :: KS_PLCERROR_EXPECTED_CONST              = #00000013
integer*4, parameter :: KS_PLCERROR_EXPECTED_READ_ONLY          = #00000014
integer*4, parameter :: KS_PLCERROR_EXPECTED_VAR_INPUT          = #00000015
integer*4, parameter :: KS_PLCERROR_EXPECTED_VAR_OUTPUT         = #00000016
integer*4, parameter :: KS_PLCERROR_EXPECTED_SIMPLE_TYPE        = #00000017
integer*4, parameter :: KS_PLCERROR_EXPECTED_TIME               = #00000018
integer*4, parameter :: KS_PLCERROR_EXPECTED_GLOBAL             = #00000019
integer*4, parameter :: KS_PLCERROR_EXPECTED_LABEL              = #0000001a
integer*4, parameter :: KS_PLCERROR_EXPECTED_INCOMPLETE_DECL    = #0000001b
integer*4, parameter :: KS_PLCERROR_EXPECTED_VAR_CONFIG         = #0000001c
integer*4, parameter :: KS_PLCERROR_ID_DEFINED                  = #0000001d
integer*4, parameter :: KS_PLCERROR_ID_UNKNOWN                  = #0000001e
integer*4, parameter :: KS_PLCERROR_ID_CONST                    = #0000001f
integer*4, parameter :: KS_PLCERROR_ID_TOO_MANY                 = #00000020
integer*4, parameter :: KS_PLCERROR_UNKNOWN_LOCATION            = #00000021
integer*4, parameter :: KS_PLCERROR_UNKNOWN_SIZE                = #00000022
integer*4, parameter :: KS_PLCERROR_TYPE_MISMATCH               = #00000023
integer*4, parameter :: KS_PLCERROR_INDEX_VIOLATION             = #00000024
integer*4, parameter :: KS_PLCERROR_FORMAT_MISMATCH             = #00000025
integer*4, parameter :: KS_PLCERROR_OVERFLOW                    = #00000026
integer*4, parameter :: KS_PLCERROR_DATA_LOSS                   = #00000027
integer*4, parameter :: KS_PLCERROR_NOT_SUPPORTED               = #00000028
integer*4, parameter :: KS_PLCERROR_NEGATIVE_SIZE               = #00000029
integer*4, parameter :: KS_PLCERROR_MISSING_ARRAY_ELEM_TYPE     = #0000002a
integer*4, parameter :: KS_PLCERROR_MISSING_RETURN_VALUE        = #0000002b
integer*4, parameter :: KS_PLCERROR_BAD_VAR_USE                 = #0000002c
integer*4, parameter :: KS_PLCERROR_BAD_DIRECT_VAR_INOUT        = #0000002d
integer*4, parameter :: KS_PLCERROR_BAD_LABEL                   = #0000002e
integer*4, parameter :: KS_PLCERROR_BAD_SUBRANGE                = #0000002f
integer*4, parameter :: KS_PLCERROR_BAD_DATETIME                = #00000030
integer*4, parameter :: KS_PLCERROR_BAD_ARRAY_SIZE              = #00000031
integer*4, parameter :: KS_PLCERROR_BAD_CASE                    = #00000032
integer*4, parameter :: KS_PLCERROR_DIV_BY_ZERO                 = #00000033
integer*4, parameter :: KS_PLCERROR_AMBIGUOUS                   = #00000034
integer*4, parameter :: KS_PLCERROR_INTERNAL                    = #00000035
integer*4, parameter :: KS_PLCERROR_PARAMETER_MISMATCH          = #00000036
integer*4, parameter :: KS_PLCERROR_PARAMETER_TOO_MANY          = #00000037
integer*4, parameter :: KS_PLCERROR_PARAMETER_FEW               = #00000038
integer*4, parameter :: KS_PLCERROR_INIT_MANY                   = #00000039
integer*4, parameter :: KS_PLCERROR_BAD_ACCESS                  = #0000003a
integer*4, parameter :: KS_PLCERROR_BAD_CONFIG                  = #0000003b
integer*4, parameter :: KS_PLCERROR_READ_ONLY                   = #0000003c
integer*4, parameter :: KS_PLCERROR_WRITE_ONLY                  = #0000003d
integer*4, parameter :: KS_PLCERROR_BAD_EXIT                    = #0000003e
integer*4, parameter :: KS_PLCERROR_BAD_RECURSION               = #0000003f
integer*4, parameter :: KS_PLCERROR_BAD_PRIORITY                = #00000040
integer*4, parameter :: KS_PLCERROR_BAD_FB_OPERATOR             = #00000041
integer*4, parameter :: KS_PLCERROR_BAD_INIT                    = #00000042
integer*4, parameter :: KS_PLCERROR_BAD_CONTINUE                = #00000043
integer*4, parameter :: KS_PLCERROR_DUPLICATE_CASE              = #00000044
integer*4, parameter :: KS_PLCERROR_UNEXPECTED_TYPE             = #00000045
integer*4, parameter :: KS_PLCERROR_CASE_MISSING                = #00000046
integer*4, parameter :: KS_PLCERROR_DEAD_INSTRUCTION            = #00000047
integer*4, parameter :: KS_PLCERROR_FOLD_EXPRESSION             = #00000048
integer*4, parameter :: KS_PLCERROR_NOT_IMPLEMENTED             = #00000049
integer*4, parameter :: KS_PLCERROR_BAD_CONNECTED_PARAM_VAR     = #0000004a
integer*4, parameter :: KS_PLCERROR_PARAMETER_MIX               = #0000004b
integer*4, parameter :: KS_PLCERROR_EXPECTED_PROGRAM_DECL       = #0000004c
integer*4, parameter :: KS_PLCERROR_EXPECTED_INTERFACE          = #0000004d
integer*4, parameter :: KS_PLCERROR_EXPECTED_METHOD             = #0000004e
integer*4, parameter :: KS_PLCERROR_ABSTRACT                    = #0000004f
integer*4, parameter :: KS_PLCERROR_FINAL                       = #00000050
integer*4, parameter :: KS_PLCERROR_MISSING_ABSTRACT_METHOD     = #00000051
integer*4, parameter :: KS_PLCERROR_ABSTRACT_INCOMPLETE         = #00000052
integer*4, parameter :: KS_PLCERROR_BAD_ACCESS_MODIFIER         = #00000053
integer*4, parameter :: KS_PLCERROR_BAD_METHOD_ACCESS           = #00000054
integer*4, parameter :: KS_PLCERROR_ID_INPUT                    = #00000055
integer*4, parameter :: KS_PLCERROR_BAD_THIS                    = #00000056
integer*4, parameter :: KS_PLCERROR_MISSING_MODULE              = #00000057
integer*4, parameter :: KS_PLCERROR_EXPECTED_DNV                = #00000058
integer*4, parameter :: KS_PLCERROR_EXPECTED_DIRECT_VAR         = #00000059
integer*4, parameter :: KS_PLCERROR_BAD_VAR_DECL                = #0000005a
integer*4, parameter :: KS_PLCERROR_HAS_ABSTRACT_BODY           = #0000005b
integer*4, parameter :: KS_PLCERROR_BAD_MEMBER_ACCESS           = #0000005c
integer*4, parameter :: KS_PLCERROR_EXPECTED_FORMAL_CALL        = #0000005d
integer*4, parameter :: KS_PLCERROR_EXPECTED_INTERFACE_VAR      = #0000005e

!------ PLC runtime errors ------

integer*4, parameter :: KS_PLCERROR_DIVIDED_BY_ZERO             = #0000012c
integer*4, parameter :: KS_PLCERROR_ARRAY_INDEX_VIOLATION       = #0000012d
integer*4, parameter :: KS_PLCERROR_MEMORY_ACCESS_VIOLATION     = #0000012e
integer*4, parameter :: KS_PLCERROR_IO_VARIABLE_ACCESS          = #0000012f
integer*4, parameter :: KS_PLCERROR_BAD_VAR_EXTERNAL            = #00000130
integer*4, parameter :: KS_PLCERROR_BAD_SUB_RANGE               = #00000131

!------ PLC events ------

integer*4, parameter :: KS_PLC_COMPILE_ERROR                    = (PLC_BASE+#00000001)
integer*4, parameter :: KS_PLC_COMPILE_FINISHED                 = (PLC_BASE+#00000002)
integer*4, parameter :: KS_PLC_RUNTIME_ERROR                    = (PLC_BASE+#00000003)
integer*4, parameter :: KS_PLC_CONFIG_RESSOURCE                 = (PLC_BASE+#00000004)

!------ Context structures ------

type PlcErrorMsgContext
  integer*4 ctxType
  integer*4 hCompiler
  integer*4 line
  integer*4 column
  integer*4 errorCode
  integer*4 errorLevel
  integer*4 flags
  byte pMsg(0:199)
end type

type PlcCompileContext
  integer*4 ctxType
  integer*4 hCompiler
  integer*4 ksError
  integer*4 id
end type

type PlcRuntimeErrorContext
  integer*4 ctxType
  integer*4 hPlc
  integer*4 line
  integer*4 column
  integer*4 errorCode
  integer*4 errorLevel
  integer*4 flags
  integer*4 ksError
end type

type PlcConfigContext
  integer*4 ctxType
  integer*4 hPlc
  integer*4 configIndex
  integer*4 resourceIndex
  integer*4 flags
end type

!------ PLC compiler functions ------

interface

integer*4 function KS_createPlcCompiler(phCompiler, flags)
  !dec$ attributes stdcall, alias : '_KS_createPlcCompiler@8' :: KS_createPlcCompiler
  integer*4 phCompiler
  integer*4 flags
end function

integer*4 function KS_closePlcCompiler(hCompiler, flags)
  !dec$ attributes stdcall, alias : '_KS_closePlcCompiler@8' :: KS_closePlcCompiler
  integer*4 hCompiler
  integer*4 flags
end function

integer*4 function KS_compilePlcFile(hCompiler, sourcePath, sourceLanguage, destinationPath, flags)
  !dec$ attributes stdcall, alias : '_KS_compilePlcFile@20' :: KS_compilePlcFile
  integer*4 hCompiler
  integer*4 sourcePath
  integer*4 sourceLanguage
  integer*4 destinationPath
  integer*4 flags
end function

integer*4 function KS_compilePlcBuffer(hCompiler, pSourceBuffer, sourceLength, sourceLang, pDestBuffer, pDestLength, flags)
  !dec$ attributes stdcall, alias : '_KS_compilePlcBuffer@28' :: KS_compilePlcBuffer
  integer*4 hCompiler
  integer*4 pSourceBuffer
  integer*4 sourceLength
  integer*4 sourceLang
  integer*4 pDestBuffer
  integer*4 pDestLength
  integer*4 flags
end function

end interface

!------ PLC functions ------

interface

integer*4 function KS_createPlcFromFile(phPlc, binaryPath, flags)
  !dec$ attributes stdcall, alias : '_KS_createPlcFromFile@12' :: KS_createPlcFromFile
  integer*4 phPlc
  integer*4 binaryPath
  integer*4 flags
end function

integer*4 function KS_createPlcFromBuffer(phPlc, pBinaryBuffer, bufferLength, flags)
  !dec$ attributes stdcall, alias : '_KS_createPlcFromBuffer@16' :: KS_createPlcFromBuffer
  integer*4 phPlc
  integer*4 pBinaryBuffer
  integer*4 bufferLength
  integer*4 flags
end function

integer*4 function KS_closePlc(hPlc, flags)
  !dec$ attributes stdcall, alias : '_KS_closePlc@8' :: KS_closePlc
  integer*4 hPlc
  integer*4 flags
end function

integer*4 function KS_startPlc(hPlc, configIndex, start, flags)
  !dec$ attributes stdcall, alias : '_KS_startPlc@16' :: KS_startPlc
  integer*4 hPlc
  integer*4 configIndex
  integer*8 start
  integer*4 flags
end function

integer*4 function KS_stopPlc(hPlc, flags)
  !dec$ attributes stdcall, alias : '_KS_stopPlc@8' :: KS_stopPlc
  integer*4 hPlc
  integer*4 flags
end function

end interface

!------ Information functions ------

interface

integer*4 function KS_enumPlcConfigurations(hPlc, configIndex, pNameBuf, flags)
  !dec$ attributes stdcall, alias : '_KS_enumPlcConfigurations@16' :: KS_enumPlcConfigurations
  integer*4 hPlc
  integer*4 configIndex
  integer*4 pNameBuf
  integer*4 flags
end function

integer*4 function KS_enumPlcResources(hPlc, configIndex, resourceIndex, pNameBuf, flags)
  !dec$ attributes stdcall, alias : '_KS_enumPlcResources@20' :: KS_enumPlcResources
  integer*4 hPlc
  integer*4 configIndex
  integer*4 resourceIndex
  integer*4 pNameBuf
  integer*4 flags
end function

integer*4 function KS_enumPlcTasks(hPlc, configIndex, resourceIndex, taskIndex, pNameBuf, flags)
  !dec$ attributes stdcall, alias : '_KS_enumPlcTasks@24' :: KS_enumPlcTasks
  integer*4 hPlc
  integer*4 configIndex
  integer*4 resourceIndex
  integer*4 taskIndex
  integer*4 pNameBuf
  integer*4 flags
end function

end interface

!------ Generic functions ------

interface

integer*4 function KS_execPlcCommand(hPlc, command, pData, flags)
  !dec$ attributes stdcall, alias : '_KS_execPlcCommand@16' :: KS_execPlcCommand
  integer*4 hPlc
  integer*4 command
  integer*4 pData
  integer*4 flags
end function

integer*4 function KS_installPlcHandler(hPlc, eventCode, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installPlcHandler@16' :: KS_installPlcHandler
  integer*4 hPlc
  integer*4 eventCode
  integer*4 hSignal
  integer*4 flags
end function

end interface

!---------------------------------------------------------------------------------------------------------------
! Vision Module
!---------------------------------------------------------------------------------------------------------------

!------ Basic functions ------

interface

integer*4 function KS_loadVisionKernel(phKernel, dllName, initProcName, pArgs, flags)
  !dec$ attributes stdcall, alias : '_KS_loadVisionKernel@20' :: KS_loadVisionKernel
  integer*4 phKernel
  integer*4 dllName
  integer*4 initProcName
  integer*4 pArgs
  integer*4 flags
end function

end interface

!------ Control functions ------

interface

integer*4 function KS_installVisionHandler(eventType, hSignal, flags)
  !dec$ attributes stdcall, alias : '_KS_installVisionHandler@12' :: KS_installVisionHandler
  integer*4 eventType
  integer*4 hSignal
  integer*4 flags
end function

end interface

!------ Context structures ------

type KSVisionTaskContext
  integer*4 ctxType
  integer*4 hTask
end type

!------ Handler events ------

integer*4, parameter :: KS_VISION_TASK_CREATED                  = #00000001
integer*4, parameter :: KS_VISION_TASK_CLOSED                   = #00000002

!---------------------------------------------------------------------------------------------------------------
! SigProc Module
!---------------------------------------------------------------------------------------------------------------

!------ Filter types ------

integer*4, parameter :: KS_FIR_LOWPASS_HAMMING                  = #00000000
integer*4, parameter :: KS_FIR_LOWPASS_HANNING                  = #00000001
integer*4, parameter :: KS_FIR_LOWPASS_RECTANGULAR              = #00000002
integer*4, parameter :: KS_FIR_HIGHPASS_HAMMING                 = #00000003
integer*4, parameter :: KS_FIR_HIGHPASS_HANNING                 = #00000004
integer*4, parameter :: KS_FIR_HIGHPASS_RECTANGULAR             = #00000005
integer*4, parameter :: KS_FIR_BANDPASS_HAMMING                 = #00000006
integer*4, parameter :: KS_FIR_BANDPASS_HANNING                 = #00000007
integer*4, parameter :: KS_FIR_BANDPASS_RECTANGULAR             = #00000008
integer*4, parameter :: KS_FIR_BANDSTOP_HAMMING                 = #00000009
integer*4, parameter :: KS_FIR_BANDSTOP_HANNING                 = #0000000a
integer*4, parameter :: KS_FIR_BANDSTOP_RECTANGULAR             = #0000000b
integer*4, parameter :: KS_IIR_LOWPASS_CHEBYSHEV_I              = #00000014
integer*4, parameter :: KS_IIR_LOWPASS_BUTTERWORTH              = #00000015
integer*4, parameter :: KS_IIR_LOWPASS_CHEBYSHEV_II             = #00000016
integer*4, parameter :: KS_IIR_HIGHPASS_CHEBYSHEV_I             = #00000017
integer*4, parameter :: KS_IIR_HIGHPASS_BUTTERWORTH             = #00000018
integer*4, parameter :: KS_IIR_HIGHPASS_CHEBYSHEV_II            = #00000019
integer*4, parameter :: KS_IIR_BANDPASS_CHEBYSHEV_I             = #0000001a
integer*4, parameter :: KS_IIR_BANDPASS_BUTTERWORTH             = #0000001b
integer*4, parameter :: KS_IIR_BANDPASS_CHEBYSHEV_II            = #0000001c
integer*4, parameter :: KS_IIR_BANDSTOP_CHEBYSHEV_I             = #0000001d
integer*4, parameter :: KS_IIR_BANDSTOP_BUTTERWORTH             = #0000001e
integer*4, parameter :: KS_IIR_BANDSTOP_CHEBYSHEV_II            = #0000001f
integer*4, parameter :: KS_IIR_INDIVIDUAL                       = #00000020

!------ Standard ripples ------

real*8, parameter :: KS_CHEBYSHEV_3DB                           = 0.707106781
real*8, parameter :: KS_CHEBYSHEV_1DB                           = 0.891250938

!------ Some filter specified errorcodes ------

integer*4, parameter :: KSERROR_SP_SHANNON_THEOREM_MISMATCH     = (KSERROR_CATEGORY_SPECIAL+#00020000)

!------ Paramters structures ------

type KSSignalFilterParams
  integer*4 structSize
  integer*4 filterType
  integer*4 order
  real*8 maxFrequency
  real*8 frequency
  real*8 sampleRate
  real*8 width
  real*8 delta
end type

type KSIndividualSignalFilterParams
  integer*4 structSize
  integer*4 pXCoeffs
  integer*4 xOrder
  integer*4 pYCoeffs
  integer*4 yOrder
  real*8 maxFrequency
  real*8 sampleRate
end type

type KSPIDControllerParams
  integer*4 structSize
  real*8 proportional
  real*8 integral
  real*8 derivative
  real*8 setpoint
  real*8 sampleRate
end type

!------ Digital Filter functions ------

interface

integer*4 function KS_createSignalFilter(phFilter, pParams, flags)
  !dec$ attributes stdcall, alias : '_KS_createSignalFilter@12' :: KS_createSignalFilter
  integer*4 phFilter
  integer*4 pParams
  integer*4 flags
end function

integer*4 function KS_createIndividualSignalFilter(phFilter, pParams, flags)
  !dec$ attributes stdcall, alias : '_KS_createIndividualSignalFilter@12' :: KS_createIndividualSignalFilter
  integer*4 phFilter
  integer*4 pParams
  integer*4 flags
end function

integer*4 function KS_getFrequencyResponse(hFilter, frequency, pAmplitude, pPhase, flags)
  !dec$ attributes stdcall, alias : '_KS_getFrequencyResponse@20' :: KS_getFrequencyResponse
  integer*4 hFilter
  real*8 frequency
  integer*4 pAmplitude
  integer*4 pPhase
  integer*4 flags
end function

integer*4 function KS_scaleSignalFilter(hFilter, frequency, value, flags)
  !dec$ attributes stdcall, alias : '_KS_scaleSignalFilter@16' :: KS_scaleSignalFilter
  integer*4 hFilter
  real*8 frequency
  real*8 value
  integer*4 flags
end function

integer*4 function KS_resetSignalFilter(hFilter, flags)
  !dec$ attributes stdcall, alias : '_KS_resetSignalFilter@8' :: KS_resetSignalFilter
  integer*4 hFilter
  integer*4 flags
end function

end interface

!------ PID Controller functions ------

interface

integer*4 function KS_createPIDController(phController, pParams, flags)
  !dec$ attributes stdcall, alias : '_KS_createPIDController@12' :: KS_createPIDController
  integer*4 phController
  integer*4 pParams
  integer*4 flags
end function

integer*4 function KS_changeSetpoint(hController, setpoint, flags)
  !dec$ attributes stdcall, alias : '_KS_changeSetpoint@12' :: KS_changeSetpoint
  integer*4 hController
  real*8 setpoint
  integer*4 flags
end function

end interface

!------ General signal processing functions ------

interface

integer*4 function KS_removeSignalProcessor(hSigProc, flags)
  !dec$ attributes stdcall, alias : '_KS_removeSignalProcessor@8' :: KS_removeSignalProcessor
  integer*4 hSigProc
  integer*4 flags
end function

integer*4 function KS_processSignal(hSigProc, input, pOutput, flags)
  !dec$ attributes stdcall, alias : '_KS_processSignal@16' :: KS_processSignal
  integer*4 hSigProc
  real*8 input
  integer*4 pOutput
  integer*4 flags
end function

end interface

end module

