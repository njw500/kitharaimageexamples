// Copyright (c) 1996-2016 by Kithara Software GmbH. All rights reserved.

//##############################################################################################################
//
// File:             KrtsDemo.h (v10.00d)
//
// Description:      C/C++ API for Kithara �RealTime Suite�
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// u.jes 1996-07-05  CREAT: Original - (file created)
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:01
//
//##############################################################################################################

#ifndef __KRTSDEMO_H
#define __KRTSDEMO_H

//--------------------------------------------------------------------------------------------------------------
// KRTSDEMO
//--------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------
// Base Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Check Software Platforms ------
#if !defined(_Windows) && !defined(_WINDOWS) && !defined(_WIN32) \
 && !defined(__WIN32__) && !defined(_WINDLL) && !defined(_USRDLL)
#error Unknown operating system platform!
#endif

//------ Check Multi Threading ------
#if !defined(__MT__) && !defined(_MT) && !defined(KS_MULTITHREAD)
#error You should use multi-thread capable run-time libraries!
// in Borland C++:
//        mark 'Multi-Thread' in 'TargetExpert' dialog box
// in Microsoft Visual C++:
//      enter /MT or /MTd in 'Project Settings | C/C++ | Project Options'
// or
//      #define KS_MULTITHREAD // before including this file!
#endif

#ifndef __KDEFS_H

// If you need older type definitions, then define KS_INCL_OLD_TYPES
// in your source file before including the Kithara header!
#ifdef KS_INCL_OLD_TYPES

// Borland C++Builder already defines some basic types in the namespace 'System',
// but this namespace is unfortunately always used. To avoid ambiguity, this:
// (Please ensure to include the header file <vcl\vcl.h> at first of all !!)
#if !defined(__BORLANDC__) || __BCPLUSPLUS__ < 0x520 ||                 \
   !(defined(VCLDB_H) || defined(VCLMIN_H) || defined(VCLMAX_H) ||      \
    defined(VCL_H) || defined(__VCL0_H__) || defined(SYSDEFS_H) ||      \
    defined(SYSMAC_H))

// Don't use anymore!
typedef char                                    Char;
typedef unsigned char                           Byte;
typedef signed int                              Int;
typedef unsigned int                            UInt32;

#endif // !VCL

// Don't use anymore!
typedef unsigned char                           UChar;
typedef char                                    Int8;
typedef unsigned char                           UInt8;
typedef signed short                            Short;
typedef unsigned short                          UShort;
typedef signed short                            Int16;
typedef unsigned short                          UInt16;
typedef unsigned int                            UInt;
typedef signed int                              Int32;
typedef int                                     Signed;
typedef unsigned                                Unsigned;
typedef void                                    Void;
typedef int                                     Count;
typedef unsigned                                UInteger;
typedef unsigned                                Size;
typedef unsigned                                Flags;
typedef int                                     Bool;

#endif // KS_INCL_OLD_TYPES

//------ New Typing Scheme ------
typedef unsigned char                           byte;
typedef unsigned short                          ushort;
typedef unsigned int                            uint;
typedef unsigned __int64                        ulong;

typedef __int64                                 int64;

#ifdef __cplusplus
typedef const int64&                            int64ref;
typedef int64*                                  int64ptr;
#else
typedef const int64*                            int64ref;
typedef int64*                                  int64ptr;
#endif

//------ 64 Bit ------
typedef struct {
  uint lo;
  uint hi;
} UInt64;

//------ Synonyms ------
typedef int                                     Error;
typedef void*                                   Win32Handle;
#ifdef _WIN64
typedef int                                     KSHandle;
#else
typedef void*                                   KSHandle;
#endif

typedef KSHandle                                Handle;

//------ Function Prototypes ------
typedef void (*VoidProc)(void);
typedef Error (__stdcall* CallBackProc)(void* pArgs);
typedef Error (__stdcall* CallBackRoutine)(void* pArgs, void* pContext);

//------ Common Constants - NULL ------
#ifdef	NULL
#  undef NULL
#endif
#ifndef __cplusplus
#  define NULL ((void *)0)
#else
#  define NULL 0
#endif

#endif // __KDEFS_H

#if defined(__BORLANDC__) && __BORLANDC__ <= 0x0660
#  define KS_DO_NOT_USE_CLASS_LIBRARY
#endif

//------ Error Code Extraction ------
#define KSERROR_CATEGORY(err)                   ((err) & 0x3f000000)
#define KSERROR_VALUE(err)                      ((err) & 0x00ff0000)
#define KSERROR_CODE(err)                       ((err) & 0x3fff0000)
#define KSERROR_SYSTEM_CATEGORY(err)            ((err) & 0xc0000000)
#define KSERROR_SYSTEM_VALUE(err)               ((err) & 0x0000ffff)
#define KSERROR_SYSTEM_CODE(err)                ((err) & 0xc000ffff)

//------ Common Constants ------
#define KS_INVALID_HANDLE                               0x00000000

//------ Error Categories ------
// Operating system errors:
#define KSERROR_CATEGORY_OPERATING_SYSTEM               0x00000000

// Categories 0x01000000 ... 0x0f000000 are free for customer usage!
#define KSERROR_CATEGORY_USER                           0x0f000000
#define KSERROR_CATEGORY_BASE                           0x10000000
#define KSERROR_CATEGORY_DEAL                           0x11000000
#define KSERROR_CATEGORY_HANDLING                       0x17000000
#define KSERROR_CATEGORY_IOPORT                         0x14000000
#define KSERROR_CATEGORY_MEMORY                         0x12000000
#define KSERROR_CATEGORY_KERNEL                         0x18000000
#define KSERROR_CATEGORY_KEYBOARD                       0x1a000000
#define KSERROR_CATEGORY_INTERRUPT                      0x13000000
#define KSERROR_CATEGORY_TIMER                          0x16000000
#define KSERROR_CATEGORY_USB                            0x19000000
#define KSERROR_CATEGORY_IDT                            0x1b000000
#define KSERROR_CATEGORY_NODE                           0x1c000000
#define KSERROR_CATEGORY_SOCKET                         0x1d000000
#define KSERROR_CATEGORY_SYSTEM                         0x1e000000
#define KSERROR_CATEGORY_ETHERCAT                       0x1f000000
#define KSERROR_CATEGORY_MF                             0x20000000
#define KSERROR_CATEGORY_CAN                            0x21000000
#define KSERROR_CATEGORY_PROFIBUS                       0x22000000
#define KSERROR_CATEGORY_PLC                            0x23000000
#define KSERROR_CATEGORY_CANOPEN                        0x24000000
#define KSERROR_CATEGORY_FLEXRAY                        0x25000000
#define KSERROR_CATEGORY_PACKET                         0x26000000
#define KSERROR_CATEGORY_CAMERA                         0x27000000
#define KSERROR_CATEGORY_TASK                           0x28000000
#define KSERROR_CATEGORY_SPECIAL                        0x29000000
#define KSERROR_CATEGORY_XHCI                           0x2a000000
#define KSERROR_CATEGORY_DISK                           0x2b000000
#define KSERROR_CATEGORY_FILE                           0x2c000000
#define KSERROR_CATEGORY_NEXT                           0x2d000000
#define KSERROR_CATEGORY_EXCEPTION                      0x3f000000

//------ Error Codes ------
#define KSERROR_OPERATING_SYSTEM                        (KSERROR_CATEGORY_BASE+0x00000000)
#define KSERROR_UNKNOWN_ERROR_CODE                      (KSERROR_CATEGORY_BASE+0x00010000)
#define KSERROR_UNKNOWN_ERROR_CATEGORY                  (KSERROR_CATEGORY_BASE+0x00020000)
#define KSERROR_UNKNOWN_ERROR_LANGUAGE                  (KSERROR_CATEGORY_BASE+0x00030000)
#define KSERROR_CANNOT_FIND_LIBRARY                     (KSERROR_CATEGORY_BASE+0x00040000)
#define KSERROR_CANNOT_FIND_ADDRESS                     (KSERROR_CATEGORY_BASE+0x00050000)
#define KSERROR_BAD_PARAM                               (KSERROR_CATEGORY_BASE+0x00060000)
#define KSERROR_BAD_VERSION                             (KSERROR_CATEGORY_BASE+0x00070000)
#define KSERROR_INTERNAL                                (KSERROR_CATEGORY_BASE+0x00080000)
#define KSERROR_UNKNOWN                                 (KSERROR_CATEGORY_BASE+0x00090000)
#define KSERROR_FUNCTION_NOT_AVAILABLE                  (KSERROR_CATEGORY_BASE+0x000a0000)
#define KSERROR_DRIVER_NOT_OPENED                       (KSERROR_CATEGORY_BASE+0x000b0000)
#define KSERROR_NOT_ENOUGH_MEMORY                       (KSERROR_CATEGORY_BASE+0x000c0000)
#define KSERROR_CANNOT_OPEN_KERNEL                      (KSERROR_CATEGORY_BASE+0x000d0000)
#define KSERROR_CANNOT_CLOSE_KERNEL                     (KSERROR_CATEGORY_BASE+0x000e0000)
#define KSERROR_BAD_KERNEL_VERSION                      (KSERROR_CATEGORY_BASE+0x000f0000)
#define KSERROR_CANNOT_FIND_KERNEL                      (KSERROR_CATEGORY_BASE+0x00100000)
#define KSERROR_INVALID_HANDLE                          (KSERROR_CATEGORY_BASE+0x00110000)
#define KSERROR_DEVICE_IO_FAILED                        (KSERROR_CATEGORY_BASE+0x00120000)
#define KSERROR_REGISTRY_FAILURE                        (KSERROR_CATEGORY_BASE+0x00130000)
#define KSERROR_CANNOT_START_KERNEL                     (KSERROR_CATEGORY_BASE+0x00140000)
#define KSERROR_CANNOT_STOP_KERNEL                      (KSERROR_CATEGORY_BASE+0x00150000)
#define KSERROR_ACCESS_DENIED                           (KSERROR_CATEGORY_BASE+0x00160000)
#define KSERROR_DEVICE_NOT_OPENED                       (KSERROR_CATEGORY_BASE+0x00170000)
#define KSERROR_FUNCTION_DENIED                         (KSERROR_CATEGORY_BASE+0x00180000)
#define KSERROR_DEVICE_ALREADY_USED                     (KSERROR_CATEGORY_BASE+0x00190000)
#define KSERROR_OUT_OF_MEMORY                           (KSERROR_CATEGORY_BASE+0x001a0000)
#define KSERROR_TOO_MANY_OPEN_PROCESSES                 (KSERROR_CATEGORY_BASE+0x001b0000)
#define KSERROR_SIGNAL_OVERFLOW                         (KSERROR_CATEGORY_BASE+0x001c0000)
#define KSERROR_CANNOT_SIGNAL                           (KSERROR_CATEGORY_BASE+0x001d0000)
#define KSERROR_FILE_NOT_FOUND                          (KSERROR_CATEGORY_BASE+0x001e0000)
#define KSERROR_DEVICE_NOT_FOUND                        (KSERROR_CATEGORY_BASE+0x001f0000)
#define KSERROR_ASSERTION_FAILED                        (KSERROR_CATEGORY_BASE+0x00200000)
#define KSERROR_FUNCTION_FAILED                         (KSERROR_CATEGORY_BASE+0x00210000)
#define KSERROR_OPERATION_ABORTED                       (KSERROR_CATEGORY_BASE+0x00220000)
#define KSERROR_TIMEOUT                                 (KSERROR_CATEGORY_BASE+0x00230000)
#define KSERROR_REBOOT_NECESSARY                        (KSERROR_CATEGORY_BASE+0x00240000)
#define KSERROR_DEVICE_DISABLED                         (KSERROR_CATEGORY_BASE+0x00250000)
#define KSERROR_ATTACH_FAILED                           (KSERROR_CATEGORY_BASE+0x00260000)
#define KSERROR_DEVICE_REMOVED                          (KSERROR_CATEGORY_BASE+0x00270000)
#define KSERROR_TYPE_MISMATCH                           (KSERROR_CATEGORY_BASE+0x00280000)
#define KSERROR_FUNCTION_NOT_IMPLEMENTED                (KSERROR_CATEGORY_BASE+0x00290000)
#define KSERROR_COMMUNICATION_BROKEN                    (KSERROR_CATEGORY_BASE+0x002a0000)
#define KSERROR_DEVICE_NOT_READY                        (KSERROR_CATEGORY_BASE+0x00370000)
#define KSERROR_NO_RESPONSE                             (KSERROR_CATEGORY_BASE+0x00380000)
#define KSERROR_OPERATION_PENDING                       (KSERROR_CATEGORY_BASE+0x00390000)
#define KSERROR_PORT_BUSY                               (KSERROR_CATEGORY_BASE+0x003a0000)
#define KSERROR_UNKNOWN_SYSTEM                          (KSERROR_CATEGORY_BASE+0x003b0000)
#define KSERROR_BAD_CONTEXT                             (KSERROR_CATEGORY_BASE+0x003c0000)
#define KSERROR_END_OF_FILE                             (KSERROR_CATEGORY_BASE+0x003d0000)
#define KSERROR_INTERRUPT_ACTIVATION_FAILED             (KSERROR_CATEGORY_BASE+0x003e0000)
#define KSERROR_INTERRUPT_IS_SHARED                     (KSERROR_CATEGORY_BASE+0x003f0000)
#define KSERROR_NO_REALTIME_ACCESS                      (KSERROR_CATEGORY_BASE+0x00400000)
#define KSERROR_HARDWARE_NOT_SUPPORTED                  (KSERROR_CATEGORY_BASE+0x00410000)
#define KSERROR_TIMER_ACTIVATION_FAILED                 (KSERROR_CATEGORY_BASE+0x00420000)
#define KSERROR_SOCKET_FAILURE                          (KSERROR_CATEGORY_BASE+0x00430000)
#define KSERROR_LINE_ERROR                              (KSERROR_CATEGORY_BASE+0x00440000)
#define KSERROR_STACK_CORRUPTION                        (KSERROR_CATEGORY_BASE+0x00450000)
#define KSERROR_INVALID_ARGUMENT                        (KSERROR_CATEGORY_BASE+0x00460000)
#define KSERROR_PARSE_ERROR                             (KSERROR_CATEGORY_BASE+0x00470000)
#define KSERROR_INVALID_IDENTIFIER                      (KSERROR_CATEGORY_BASE+0x00480000)
#define KSERROR_REALTIME_ALREADY_USED                   (KSERROR_CATEGORY_BASE+0x00490000)
#define KSERROR_COMMUNICATION_FAILED                    (KSERROR_CATEGORY_BASE+0x004a0000)
#define KSERROR_INVALID_SIZE                            (KSERROR_CATEGORY_BASE+0x004b0000)
#define KSERROR_OBJECT_NOT_FOUND                        (KSERROR_CATEGORY_BASE+0x004c0000)

//------ Error codes of handling category ------
#define KSERROR_CANNOT_CREATE_EVENT                     (KSERROR_CATEGORY_HANDLING+0x00000000)
#define KSERROR_CANNOT_CREATE_THREAD                    (KSERROR_CATEGORY_HANDLING+0x00010000)
#define KSERROR_CANNOT_BLOCK_THREAD                     (KSERROR_CATEGORY_HANDLING+0x00020000)
#define KSERROR_CANNOT_SIGNAL_EVENT                     (KSERROR_CATEGORY_HANDLING+0x00030000)
#define KSERROR_CANNOT_POST_MESSAGE                     (KSERROR_CATEGORY_HANDLING+0x00040000)
#define KSERROR_CANNOT_CREATE_SHARED                    (KSERROR_CATEGORY_HANDLING+0x00050000)
#define KSERROR_SHARED_DIFFERENT_SIZE                   (KSERROR_CATEGORY_HANDLING+0x00060000)
#define KSERROR_BAD_SHARED_MEM                          (KSERROR_CATEGORY_HANDLING+0x00070000)
#define KSERROR_WAIT_TIMEOUT                            (KSERROR_CATEGORY_HANDLING+0x00080000)
#define KSERROR_EVENT_NOT_SIGNALED                      (KSERROR_CATEGORY_HANDLING+0x00090000)
#define KSERROR_CANNOT_CREATE_CALLBACK                  (KSERROR_CATEGORY_HANDLING+0x000a0000)
#define KSERROR_NO_DATA_AVAILABLE                       (KSERROR_CATEGORY_HANDLING+0x000b0000)
#define KSERROR_REQUEST_NESTED                          (KSERROR_CATEGORY_HANDLING+0x000c0000)

//------ Deal error codes ------
#define KSERROR_BAD_CUSTNUM                             (KSERROR_CATEGORY_DEAL+0x00030000)
#define KSERROR_KEY_BAD_FORMAT                          (KSERROR_CATEGORY_DEAL+0x00060000)
#define KSERROR_KEY_BAD_DATA                            (KSERROR_CATEGORY_DEAL+0x00080000)
#define KSERROR_KEY_PERMISSION_EXPIRED                  (KSERROR_CATEGORY_DEAL+0x00090000)
#define KSERROR_BAD_LICENCE                             (KSERROR_CATEGORY_DEAL+0x000a0000)
#define KSERROR_CANNOT_LOAD_KEY                         (KSERROR_CATEGORY_DEAL+0x000c0000)
#define KSERROR_BAD_NAME_OR_FIRM                        (KSERROR_CATEGORY_DEAL+0x000f0000)
#define KSERROR_NO_LICENCES_FOUND                       (KSERROR_CATEGORY_DEAL+0x00100000)
#define KSERROR_LICENCE_CAPACITY_EXHAUSTED              (KSERROR_CATEGORY_DEAL+0x00110000)
#define KSERROR_FEATURE_NOT_LICENSED                    (KSERROR_CATEGORY_DEAL+0x00120000)

//------ Exception Codes ------
#define KSERROR_DIVIDE_BY_ZERO                          (KSERROR_CATEGORY_EXCEPTION+0x00000000)
#define KSERROR_DEBUG_EXCEPTION                         (KSERROR_CATEGORY_EXCEPTION+0x00010000)
#define KSERROR_NONMASKABLE_INTERRUPT                   (KSERROR_CATEGORY_EXCEPTION+0x00020000)
#define KSERROR_BREAKPOINT                              (KSERROR_CATEGORY_EXCEPTION+0x00030000)
#define KSERROR_OVERFLOW                                (KSERROR_CATEGORY_EXCEPTION+0x00040000)
#define KSERROR_BOUND_RANGE                             (KSERROR_CATEGORY_EXCEPTION+0x00050000)
#define KSERROR_INVALID_OPCODE                          (KSERROR_CATEGORY_EXCEPTION+0x00060000)
#define KSERROR_NO_MATH                                 (KSERROR_CATEGORY_EXCEPTION+0x00070000)
#define KSERROR_DOUBLE_FAULT                            (KSERROR_CATEGORY_EXCEPTION+0x00080000)
#define KSERROR_COPROCESSOR_SEGMENT_OVERRUN             (KSERROR_CATEGORY_EXCEPTION+0x00090000)
#define KSERROR_INVALID_TSS                             (KSERROR_CATEGORY_EXCEPTION+0x000a0000)
#define KSERROR_SEGMENT_NOT_PRESENT                     (KSERROR_CATEGORY_EXCEPTION+0x000b0000)
#define KSERROR_STACK_FAULT                             (KSERROR_CATEGORY_EXCEPTION+0x000c0000)
#define KSERROR_PROTECTION_FAULT                        (KSERROR_CATEGORY_EXCEPTION+0x000d0000)
#define KSERROR_PAGE_FAULT                              (KSERROR_CATEGORY_EXCEPTION+0x000e0000)
#define KSERROR_FPU_FAULT                               (KSERROR_CATEGORY_EXCEPTION+0x00100000)
#define KSERROR_ALIGNMENT_CHECK                         (KSERROR_CATEGORY_EXCEPTION+0x00110000)
#define KSERROR_MACHINE_CHECK                           (KSERROR_CATEGORY_EXCEPTION+0x00120000)
#define KSERROR_SIMD_FAULT                              (KSERROR_CATEGORY_EXCEPTION+0x00130000)

//------ Flags ------
#define KSF_OPENED                                      0x01000000
#define KSF_ACTIVE                                      0x02000000
#define KSF_RUNNING                                     0x02000000
#define KSF_FINISHED                                    0x04000000
#define KSF_CANCELED                                    0x08000000
#define KSF_DISABLED                                    0x10000000
#define KSF_REQUESTED                                   0x20000000
#define KSF_INTERNAL                                    0x40000000
#define KSF_TERMINATE                                   0x00000000
#define KSF_DONT_WAIT                                   0x00000001
#define KSF_KERNEL_EXEC                                 0x00000004
#define KSF_ASYNC_EXEC                                  0x0000000c
#define KSF_DIRECT_EXEC                                 0x0000000c
#define KSF_DONT_START                                  0x00000010
#define KSF_ONE_SHOT                                    0x00000020
#define KSF_SINGLE_SHOT                                 0x00000020
#define KSF_SAVE_FPU                                    0x00000040
#define KSF_USE_SSE                                     0x00000001
#define KSF_USER_EXEC                                   0x00000080
#define KSF_REALTIME_EXEC                               0x00000100
#define KSF_WAIT                                        0x00000200
#define KSF_ACCEPT_INCOMPLETE                           0x00000200
#define KSF_REPORT_RESOURCE                             0x00000400
#define KSF_USE_TIMEOUTS                                0x00000400
#define KSF_FORCE_OVERRIDE                              0x00001000
#define KSF_OPTION_ON                                   0x00001000
#define KSF_OPTION_OFF                                  0x00002000
#define KSF_FORCE_OPEN                                  0x00002000
#define KSF_UNLIMITED                                   0x00004000
#define KSF_DONT_USE_SMP                                0x00004000
#define KSF_NO_CONTEXT                                  0x00008000
#define KSF_HIGH_PRECISION                              0x00008000
#define KSF_CONTINUOUS                                  0x00010000
#define KSF_ZERO_INIT                                   0x00020000
#define KSF_TX_PDO                                      0x00010000
#define KSF_RX_PDO                                      0x00020000
#define KSF_READ                                        0x00040000
#define KSF_WRITE                                       0x00080000
#define KSF_USE_SMP                                     0x00080000
#define KSF_ALTERNATIVE                                 0x00200000
#define KSF_PDO                                         0x00040000
#define KSF_SDO                                         0x00080000
#define KSF_IDN                                         0x00100000
#define KSF_MANUAL_RESET                                0x00200000
#define KSF_RESET_STATE                                 0x00800000
#define KSF_ISA_BUS                                     0x00000001
#define KSF_PCI_BUS                                     0x00000005
#define KSF_PRIO_NORMAL                                 0x00000000
#define KSF_PRIO_LOW                                    0x00000001
#define KSF_PRIO_LOWER                                  0x00000002
#define KSF_PRIO_LOWEST                                 0x00000003
#define KSF_MESSAGE_PIPE                                0x00001000
#define KSF_PIPE_NOTIFY_GET                             0x00002000
#define KSF_PIPE_NOTIFY_PUT                             0x00004000
#define KSF_ALL_CPUS                                    0x00000000
#define KSF_READ_ACCESS                                 0x00000001
#define KSF_WRITE_ACCESS                                0x00000002
#define KSF_SIZE_8_BIT                                  0x00010000
#define KSF_SIZE_16_BIT                                 0x00020000
#define KSF_SIZE_32_BIT                                 0x00040000

//------ Obsolete flags - do not use! ------
#define KSF_ACCEPT_PENDING                              0x00000002
#define KSF_DONT_RAISE                                  0x00200000

//------ Constant for returning no-error ------
#undef KS_OK
#define KS_OK                                           0x00000000

//------ Language constants ------
#define KSLNG_DEFAULT                                   0x00000000

//------ Context type values ------
#define USER_CONTEXT                                    0x00000000
#define INTERRUPT_CONTEXT                               0x00000100
#define TIMER_CONTEXT                                   0x00000200
#define KEYBOARD_CONTEXT                                0x00000300
#define USB_BASE                                        0x00000506
#define PACKET_BASE                                     0x00000700
#define DEVICE_BASE                                     0x00000800
#define ETHERCAT_BASE                                   0x00000900
#define SOCKET_BASE                                     0x00000a00
#define TASK_BASE                                       0x00000b00
#define MULTIFUNCTION_BASE                              0x00000c00
#define CAN_BASE                                        0x00000d00
#define PROFIBUS_BASE                                   0x00000e00
#define CANOPEN_BASE                                    0x00000f00
#define FLEXRAY_BASE                                    0x00001000
#define XHCI_BASE                                       0x00001100
#define V86_BASE                                        0x00001200
#define PLC_BASE                                        0x00002000

//------ Config code for "Driver" ------
#define KSCONFIG_DRIVER_NAME                            0x00000001
#define KSCONFIG_FUNCTION_LOGGING                       0x00000002

//------ Config code for "Base" ------
#define KSCONFIG_DISABLE_MESSAGE_LOGGING                0x00000001
#define KSCONFIG_ENABLE_MESSAGE_LOGGING                 0x00000002
#define KSCONFIG_FLUSH_MESSAGE_PIPE                     0x00000003

//------ Config code for "Debug" ------

//------ Commonly used commands ------
#define KS_ABORT_XMIT_CMD                               0x00000001
#define KS_ABORT_RECV_CMD                               0x00000002
#define KS_FLUSH_XMIT_BUF                               0x00000003
#define KS_FLUSH_RECV_BUF                               0x00000009
#define KS_SET_BAUD_RATE                                0x0000000c

//------ Pipe contexts ------
#define PIPE_PUT_CONTEXT                                (USER_CONTEXT+0x00000040)
#define PIPE_GET_CONTEXT                                (USER_CONTEXT+0x00000041)

//------ Quick mutex levels ------
#define KS_APP_LEVEL                                    0x00000000
#define KS_DPC_LEVEL                                    0x00000001
#define KS_ISR_LEVEL                                    0x00000002
#define KS_RTX_LEVEL                                    0x00000003
#define KS_CPU_LEVEL                                    0x00000004

//------ Data types ------
#define KS_DATATYPE_UNKNOWN                             0x00000000
#define KS_DATATYPE_BOOLEAN                             0x00000001
#define KS_DATATYPE_INTEGER8                            0x00000002
#define KS_DATATYPE_UINTEGER8                           0x00000005
#define KS_DATATYPE_INTEGER16                           0x00000003
#define KS_DATATYPE_UINTEGER16                          0x00000006
#define KS_DATATYPE_INTEGER32                           0x00000004
#define KS_DATATYPE_UINTEGER32                          0x00000007
#define KS_DATATYPE_INTEGER64                           0x00000015
#define KS_DATATYPE_UNSIGNED64                          0x0000001b
#define KS_DATATYPE_REAL32                              0x00000008
#define KS_DATATYPE_REAL64                              0x00000011
#define KS_DATATYPE_TINY                                0x00000002
#define KS_DATATYPE_BYTE                                0x00000005
#define KS_DATATYPE_SHORT                               0x00000003
#define KS_DATATYPE_USHORT                              0x00000006
#define KS_DATATYPE_INT                                 0x00000004
#define KS_DATATYPE_UINT                                0x00000007
#define KS_DATATYPE_LLONG                               0x00000015
#define KS_DATATYPE_ULONG                               0x0000001b
#define KS_DATATYPE_VISIBLE_STRING                      0x00000009
#define KS_DATATYPE_OCTET_STRING                        0x0000000a
#define KS_DATATYPE_UNICODE_STRING                      0x0000000b
#define KS_DATATYPE_TIME_OF_DAY                         0x0000000c
#define KS_DATATYPE_TIME_DIFFERENCE                     0x0000000d
#define KS_DATATYPE_DOMAIN                              0x0000000f
#define KS_DATATYPE_TIME_OF_DAY32                       0x0000001c
#define KS_DATATYPE_DATE32                              0x0000001d
#define KS_DATATYPE_DATE_AND_TIME64                     0x0000001e
#define KS_DATATYPE_TIME_DIFFERENCE64                   0x0000001f
#define KS_DATATYPE_BIT1                                0x00000030
#define KS_DATATYPE_BIT2                                0x00000031
#define KS_DATATYPE_BIT3                                0x00000032
#define KS_DATATYPE_BIT4                                0x00000033
#define KS_DATATYPE_BIT5                                0x00000034
#define KS_DATATYPE_BIT6                                0x00000035
#define KS_DATATYPE_BIT7                                0x00000036
#define KS_DATATYPE_BIT8                                0x00000037
#define KS_DATATYPE_INTEGER24                           0x00000010
#define KS_DATATYPE_UNSIGNED24                          0x00000016
#define KS_DATATYPE_INTEGER40                           0x00000012
#define KS_DATATYPE_UNSIGNED40                          0x00000018
#define KS_DATATYPE_INTEGER48                           0x00000013
#define KS_DATATYPE_UNSIGNED48                          0x00000019
#define KS_DATATYPE_INTEGER56                           0x00000014
#define KS_DATATYPE_UNSIGNED56                          0x0000001a
#define KS_DATATYPE_KSHANDLE                            0x00000040

//------ DataObj type flags ------
#define KS_DATAOBJ_READABLE                             0x00010000
#define KS_DATAOBJ_WRITEABLE                            0x00020000
#define KS_DATAOBJ_ARRAY                                0x01000000
#define KS_DATAOBJ_ENUM                                 0x02000000
#define KS_DATAOBJ_STRUCT                               0x04000000

//------ Common types and structures ------
typedef struct {
  int requested;
  int performed;
  int state;
  Error error;
} HandlerState;

typedef struct {
  int baseTime;
  int charTime;
} KSTimeOut;

typedef struct {
  int structSize;
  char pDeviceName[80];
  char pDeviceDesc[80];
  char pDeviceVendor[80];
  char pFriendlyName[80];
  char pHardwareId[80];
  char pClassName[80];
  char pInfPath[80];
  char pDriverDesc[80];
  char pDriverVendor[80];
  char pDriverDate[80];
  char pDriverVersion[80];
  char pServiceName[80];
  char pServiceDisp[80];
  char pServiceDesc[80];
  char pServicePath[80];
} KSDeviceInfo;

typedef struct {
  int structSize;
  int numberOfCPUs;
  int numberOfSharedCPUs;
  int kiloBytesOfRAM;
  byte isDll64Bit;
  byte isSys64Bit;
  byte isStableVersion;
  byte isLogVersion;
  int dllVersion;
  int dllBuildNumber;
  int sysVersion;
  int sysBuildNumber;
  int systemVersion;
  char pSystemName[64];
  char pServicePack[64];
  char pDllPath[256];
  char pSysPath[256];
} KSSystemInformation;

typedef struct {
  int structSize;
  int index;
  int group;
  int number;
  int currentTemperature;
  int allowedTemperature;
} KSProcessorInformation;

typedef struct {
  int structSize;
  KSHandle hHandle;
  char pName[256];
  int size;
  void* pThisPtr;
} KSSharedMemInfo;

//------ Context structures ------
typedef struct {
  uint type;
  // ...
} ContextBase;

typedef struct {
  int ctxType;
  KSHandle hPipe;
} PipeUserContext;

//------ Common functions ------
Error __stdcall KS_startKernel(
                void);
Error __stdcall KS_stopKernel(
                void);
Error __stdcall KS_resetKernel(
                int flags);
Error __stdcall KS_openDriver(
                const char* customerNumber);
Error __stdcall KS_closeDriver(
                void);
Error __stdcall KS_getDriverVersion(
                uint* pVersion);
Error __stdcall KS_getErrorString(
                Error code, char** msg, uint language);
Error __stdcall KS_getErrorStringEx(
                Error code, char* pMsgBuf, uint language);
Error __stdcall KS_addErrorString(
                Error code, const char* msg, uint language);
Error __stdcall KS_getDriverConfig(
                const char* module, int configType, void* pData);
Error __stdcall KS_setDriverConfig(
                const char* module, int configType, const void* pData);
Error __stdcall KS_getSystemInformation(
                KSSystemInformation* pSystemInfo, int flags);
Error __stdcall KS_getProcessorInformation(
                int index, KSProcessorInformation* pProcessorInfo, int flags);
Error __stdcall KS_closeHandle(
                KSHandle handle, int flags);

//------ Debugging & test ------
Error __stdcall KS_showMessage(
                Error ksError, const char* msg);
Error __stdcall KS_logMessage(
                int msgType, const char* msgText);
Error __stdcall KS_bufMessage(
                int msgType, const void* pBuffer, int length, const char* msgText);
Error __stdcall KS_dbgMessage(
                const char* fileName, int line, const char* msgText);
Error __stdcall KS_beep(
                int freq, int duration);
Error __stdcall KS_vdebugK(
                const char* fileName, int line, const char* format, void* pArgs);
Error __stdcall KS_vprintK(
                const char* format, void* pArgs);
Error __stdcall KS_throwException(
                int error, const char* pText, const char* pFile, int line);

//------ Device handling ------
Error __stdcall KS_enumDevices(
                const char* deviceType, int index, char* pDeviceNameBuf, int flags);
Error __stdcall KS_enumDevicesEx(
                const char* className, const char* busType, KSHandle hEnumerator, int index, char* pDeviceNameBuf, int flags);
Error __stdcall KS_getDevices(
                const char* className, const char* busType, KSHandle hEnumerator, int* pCount, void** pBuf, int size, int flags);
Error __stdcall KS_getDeviceInfo(
                const char* deviceName, KSDeviceInfo* pDeviceInfo, int flags);
Error __stdcall KS_updateDriver(
                const char* deviceName, const char* fileName, int flags);

//------ Threads & priorities ------
Error __stdcall KS_createThread(
                CallBackProc procName, void* pArgs, Win32Handle* phThread);
Error __stdcall KS_removeThread(
                void);
Error __stdcall KS_setThreadPrio(
                int prio);
Error __stdcall KS_getThreadPrio(
                uint* pPrio);

//------ Shared memory ------
Error __stdcall KS_createSharedMem(
                void** ppAppPtr, void** ppSysPtr, const char* name, int size, int flags);
Error __stdcall KS_freeSharedMem(
                void* pAppPtr);
Error __stdcall KS_getSharedMem(
                void** ppPtr, const char* name);
Error __stdcall KS_readMem(
                void* pBuffer, void* pAppPtr, int size, int offset);
Error __stdcall KS_writeMem(
                const void* pBuffer, void* pAppPtr, int size, int offset);
Error __stdcall KS_createSharedMemEx(
                KSHandle* phHandle, const char* name, int size, int flags);
Error __stdcall KS_freeSharedMemEx(
                KSHandle hHandle, int flags);
Error __stdcall KS_getSharedMemEx(
                KSHandle hHandle, void** pPtr, int flags);

//------ Events ------
Error __stdcall KS_createEvent(
                KSHandle* phEvent, const char* name, int flags);
Error __stdcall KS_closeEvent(
                KSHandle hEvent);
Error __stdcall KS_setEvent(
                KSHandle hEvent);
Error __stdcall KS_resetEvent(
                KSHandle hEvent);
Error __stdcall KS_pulseEvent(
                KSHandle hEvent);
Error __stdcall KS_waitForEvent(
                KSHandle hEvent, int flags, int timeout);
Error __stdcall KS_getEventState(
                KSHandle hEvent, uint* pState);
Error __stdcall KS_postMessage(
                Win32Handle hWnd, uint msg, uint wP, uint lP);

//------ CallBacks ------
Error __stdcall KS_createCallBack(
                KSHandle* phCallBack, CallBackRoutine routine, void* pArgs, int flags, int prio);
Error __stdcall KS_removeCallBack(
                KSHandle hCallBack);
Error __stdcall KS_execCallBack(
                KSHandle hCallBack, void* pContext, int flags);
Error __stdcall KS_getCallState(
                KSHandle hSignal, HandlerState* pState);
Error __stdcall KS_signalObject(
                KSHandle hObject, void* pContext, int flags);

//------ Pipes ------
Error __stdcall KS_createPipe(
                KSHandle* phPipe, const char* name, int itemSize, int itemCount, KSHandle hNotify, int flags);
Error __stdcall KS_removePipe(
                KSHandle hPipe);
Error __stdcall KS_flushPipe(
                KSHandle hPipe, int flags);
Error __stdcall KS_getPipe(
                KSHandle hPipe, void* pBuffer, int length, int* pLength, int flags);
Error __stdcall KS_putPipe(
                KSHandle hPipe, const void* pBuffer, int length, int* pLength, int flags);

//------ Quick mutexes ------
Error __stdcall KS_createQuickMutex(
                KSHandle* phMutex, int level, int flags);
Error __stdcall KS_removeQuickMutex(
                KSHandle hMutex);
Error __stdcall KS_requestQuickMutex(
                KSHandle hMutex);
Error __stdcall KS_releaseQuickMutex(
                KSHandle hMutex);

// ATTENTION! If you use the following functions or macros, don't forget
//            to insert the file ..\dev\Kithara.cpp into your project!

Error __cdecl KS_printK(const char* format, ...);
Error __cdecl KS_debugK(const char* file, int line, const char* format, ...);

#define KS_DEBUG0(f)                                                            \
        KS_debugK(__FILE__, __LINE__, f)
#define KS_DEBUG1(f, p1)                                                        \
        KS_debugK(__FILE__, __LINE__, f, p1)
#define KS_DEBUG2(f, p1, p2)                                                    \
        KS_debugK(__FILE__, __LINE__, f, p1, p2)
#define KS_DEBUG3(f, p1, p2, p3)                                                \
        KS_debugK(__FILE__, __LINE__, f, p1, p2, p3)
#define KS_DEBUG4(f, p1, p2, p3, p4)                                            \
        KS_debugK(__FILE__, __LINE__, f, p1, p2, p3, p4)
#define KS_DEBUG5(f, p1, p2, p3, p4, p5)                                        \
        KS_debugK(__FILE__, __LINE__, f, p1, p2, p3, p4, p5)
#define KS_DEBUG6(f, p1, p2, p3, p4, p5, p6)                                    \
        KS_debugK(__FILE__, __LINE__, f, p1, p2, p3, p4, p5, p6)
#define KS_DEBUG7(f, p1, p2, p3, p4, p5, p6, p7)                                \
        KS_debugK(__FILE__, __LINE__, f, p1, p2, p3, p4, p5, p6, p7)
#define KS_DEBUG8(f, p1, p2, p3, p4, p5, p6, p7, p8)                            \
        KS_debugK(__FILE__, __LINE__, f, p1, p2, p3, p4, p5, p6, p7, p8)

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// null
//--------------------------------------------------------------------------------------------------------------

#ifndef null

#ifdef _NATIVE_NULLPTR_SUPPORTED
#define null nullptr
#else
#define null NULL
#endif

#endif // null

//--------------------------------------------------------------------------------------------------------------
// _KsAutoPtr
//--------------------------------------------------------------------------------------------------------------

template <typename T>
struct _KsAutoPtr {
public:
  _KsAutoPtr()
    : pValue_(null) {
  }
  template <typename U>
  _KsAutoPtr(U* p)
    : pValue_(p) {
  }
  ~_KsAutoPtr() {
    delete pValue_;
  }

  T* get() const {
    return pValue_;
  }
  T* release() {
    T* pValue = pValue_;
    pValue_ = null;
    return pValue;
  }

  operator T* () const {
    return pValue_;
  }

  T* operator -> () const  {
    return pValue_;
  }

  template <typename U>
  _KsAutoPtr& operator = (U* p) {
    delete pValue_;
    pValue_ = p;
    return *this;
  }
#ifdef _NATIVE_NULLPTR_SUPPORTED
  _KsAutoPtr& operator = (const decltype(null)&) {
    delete pValue_;
    pValue_ = null;
    return *this;
  }
#endif

private:
  T* pValue_;

  template <typename U>
  _KsAutoPtr(const _KsAutoPtr<U>&);
  _KsAutoPtr(const _KsAutoPtr&);
  template <typename U>
  _KsAutoPtr& operator = (const _KsAutoPtr<U>&);
  _KsAutoPtr& operator = (const _KsAutoPtr&);
};

//--------------------------------------------------------------------------------------------------------------
// _KsVector
//--------------------------------------------------------------------------------------------------------------

template <typename T>
struct _KsVector {
public:
  _KsVector()
    : pElements_(null),
      count_(0) {
    clear();
  }
  _KsVector(const T* pElements, int count = 0)
    : pElements_(null),
      count_(0) {
    if (pElements == null) {
      clear();
      return;
    }
    if (count == 0) {
      for (const T* pStart = pElements; *pStart != T(); ++pStart)
        ++count;
    }
    pElements_ = new T[count + 1];
    copy(pElements_, pElements, count);
    pElements_[count] = T();
    count_ = count;
  }
  _KsVector(const _KsVector& other)
    : pElements_(new T[other.count_ + 1]),
      count_(other.count_) {
    copy(pElements_, other.pElements_, count_ + 1);
  }
  ~_KsVector() {
    delete [] pElements_;
  }

  _KsVector& operator = (const _KsVector& other) {
    delete [] pElements_;
    count_ = other.count_;
    pElements_ = new T[count_ + 1];
    copy(pElements_, other.pElements_, count_ + 1);
    return *this;
  }

  void add(const T& element) {
    T* pNew = new T[count_ + 2];
    copy(pNew, pElements_, count_);
    ++count_;
    delete [] pElements_;
    pElements_ = pNew;
    pElements_[count_ - 1] = element;
    pElements_[count_] = T();
  }
  void clear() {
    delete [] pElements_;
    count_ = 0;
    pElements_ = new T[1];
    pElements_[0] = T();
  };

  int length() const {
    return count_;
  }

  operator T* () {
    return pElements_;
  }
  operator const T* () const {
    return pElements_;
  }

  T& operator [] (int index) {
    return pElements_[index];
  }
  const T& operator [] (int index) const {
    return pElements_[index];
  }

  bool operator == (const _KsVector& other) const {
    if (count_ != other.count_)
      return false;
    for (int i = 0; i < count_; ++i) {
      if (pElements_[i] != other.pElements_[i])
        return false;
    }
    return true;
  }
  bool operator == (const T* pElements) const {
    int count = 0;
    for (const T* pStart = pElements; *pStart != T(); ++pStart)
      ++count;
    if (count != count_)
      return false;
    for (int i = 0; i < count_; ++i) {
      if (pElements_[i] != pElements[i])
        return false;
    }
    return true;
  }

private:
  void copy(T* pDst, const T* pSrc, int count) {
    for (int i = 0; i < count; ++i)
      pDst[i] = pSrc[i];
  }

  T* pElements_;
  int count_;
};

//--------------------------------------------------------------------------------------------------------------
// KsString
//--------------------------------------------------------------------------------------------------------------

#ifndef __KDEFS_H

typedef _KsVector<char> KsString;

#endif

//--------------------------------------------------------------------------------------------------------------
// KsException
//--------------------------------------------------------------------------------------------------------------

#ifndef __KDEFS_H

struct KsException {
public:
  KsException(Error error, const KsString& what)
    : error_(error),
      what_(what) {
  }

  Error error() const {
    return error_;
  }
  const KsString& what() const {
    return what_;
  }

private:
  Error error_;
  KsString what_;
};

#endif

//--------------------------------------------------------------------------------------------------------------
// _KsEnumeration
//--------------------------------------------------------------------------------------------------------------

template <typename T>
struct _KsEnumeration : protected _KsVector<T> {
  using _KsVector<T>::length;

  using _KsVector<T>::operator const T*;
  using _KsVector<T>::operator [];
};

//--------------------------------------------------------------------------------------------------------------
// KsDriver
//--------------------------------------------------------------------------------------------------------------

struct KsDriver {
  KsDriver() {
  }
  KsDriver(const KsString& customerNumber) {
    open(customerNumber);
  }
  ~KsDriver() {
    KS_closeDriver();
  }

  void open(const KsString& customerNumber) {
    if (Error error = KS_openDriver(customerNumber))
      throw KsException(error, "KS_openDriver");
  }

  static void _getConfig(const KsString& service, uint configType, void* pData) {
    if (Error error = KS_getDriverConfig(service, configType, pData))
      throw KsException(error, "KS_getDriverConfig");
  }
  void getConfig(const KsString& service, uint configType, void* pData) const {
    _getConfig(service, configType, pData);
  }
  void getConfig(const KsString& service, uint configType, KsString* pData) const {
    char pBuffer[256];
    _getConfig(service, configType, pBuffer);
    *pData = pBuffer;
  }

  static void _setConfig(const KsString& service, uint configType, const void* pData) {
    if (Error error = KS_setDriverConfig(service, configType, pData))
      throw KsException(error, "KS_getDriverConfig");
  }
  void setConfig(const KsString& service, uint configType, const void* pData) const {
    _setConfig(service, configType, pData);
  }

  void getSystemInformation(KSSystemInformation* pSystemInformation) const {
    if (Error error = KS_getSystemInformation(pSystemInformation, 0))
      throw KsException(error, "KS_getSystemInformation");
  }
  KSSystemInformation systemInformation() const {
    KSSystemInformation systemInformation = { sizeof(KSSystemInformation) };
    getSystemInformation(&systemInformation);
    return systemInformation;
  }

  void getProcessorInformation(int index, KSProcessorInformation* pProcessorInformation) const {
    if (Error error = KS_getProcessorInformation(index, pProcessorInformation, 0)) {
      throw KsException(error, "KS_getProcessorInformation");
    }
  }
  KSProcessorInformation processorInformation(int index) const {
    KSProcessorInformation processorInformation = { sizeof(KSProcessorInformation) };
    getProcessorInformation(index, &processorInformation);
    return processorInformation;
  }

  void getVersion(uint* pVersion) const {
    if (Error error = KS_getDriverVersion(pVersion))
      throw KsException(error, "KS_getDriverVersion");
  }
  uint version() const {
    uint version;
    getVersion(&version);
    return version;
  }

  void addErrorString(Error code, const KsString& msg, uint language = KSLNG_DEFAULT) {
    if (Error error = KS_addErrorString(code, msg, language))
      throw KsException(error, "KS_addErrorString");
  }

  void getErrorString(Error code, char** msg, uint language = KSLNG_DEFAULT) {
    if (Error error = KS_getErrorString(code, msg, language))
      throw KsException(error, "KS_getErrorString");
  }
  void getErrorString(Error code, char* pMsgBuf, uint language = KSLNG_DEFAULT) {
    if (Error error = KS_getErrorStringEx(code, pMsgBuf, language))
      throw KsException(error, "KS_getErrorStringEx");
  }
  void getErrorString(Error code, KsString* msg, uint language = KSLNG_DEFAULT) {
    char* pMsg;
    getErrorString(code, &pMsg, language);
    *msg = pMsg;
  }
  KsString getErrorString(Error code, uint language = KSLNG_DEFAULT) {
    KsString msg;
    getErrorString(code, &msg, language);
    return msg;
  }

  KsString name() const {
    char pBuffer[32];
    getConfig("", KSCONFIG_DRIVER_NAME, pBuffer);
    return pBuffer;
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsDeviceEnumeration
//--------------------------------------------------------------------------------------------------------------

struct KsDeviceEnumeration : public _KsEnumeration<KsString> {
  KsDeviceEnumeration(const KsString& type = null, int flags = 0) {
    for (int i = 0; ; ++i) {
      char pDeviceName[256];
      Error error = KS_enumDevices(type, i, pDeviceName, flags);
      if (KSERROR_CODE(error) == KSERROR_DEVICE_NOT_FOUND)
        break;
      if (error)
        throw KsException(error, "KS_enumDevices");
      add(pDeviceName);
    }
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsSharedMem
//--------------------------------------------------------------------------------------------------------------

struct KsSharedMem {
public:
  KsSharedMem(int size, int flags = 0) {
    alloc(size, null, flags);
  }
  KsSharedMem(int size, const KsString& name, int flags = 0) {
    alloc(size, name, flags);
  }
  ~KsSharedMem() {
    free();
  }

  void alloc(int size, const KsString& name, int flags) {
    if (Error error = KS_createSharedMem(&pApp_, &pSys_, name, size, flags))
      throw KsException(error, "KS_createSharedMem");
  }
  void free() {
    if (pApp_ == null)
      return;
    if (Error error = KS_freeSharedMem(pApp_))
      throw KsException(error, "KS_freeSharedMem");

    pApp_ = pSys_ = null;
  }

  void* ptr() const {
    return pApp_;
  }
  void* sys() const {
    return pSys_;
  }

private:
  void* pApp_;
  void* pSys_;

  KsSharedMem(const KsSharedMem&);
  KsSharedMem& operator = (const KsSharedMem&);
};

//--------------------------------------------------------------------------------------------------------------
// _KsSharedMem
//--------------------------------------------------------------------------------------------------------------

template <typename T, int times = 1>
struct _KsSharedMem : public KsSharedMem {
public:
  _KsSharedMem(int flags = 0)
    : KsSharedMem(sizeof(T) * times, flags) {
  }
  _KsSharedMem(const KsString& name, int flags = 0)
    : KsSharedMem(sizeof(T) * times, name, flags) {
  }

  void alloc(const KsString& name) {
    alloc(sizeof(T) * times, name)
  }

  T* ptr() const {
    return reinterpret_cast<T*>(KsSharedMem::ptr());
  }
  T* sys() const {
    return reinterpret_cast<T*>(KsSharedMem::sys());
  }

  operator T* () const {
    return ptr();
  }
  T* operator -> () const {
    return ptr();
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsHandle
//--------------------------------------------------------------------------------------------------------------

struct KsHandle {
public:
  ~KsHandle() {
    close();
  }

  void close() {
    if (hHandle_ != KS_INVALID_HANDLE) {
      KS_closeHandle(hHandle_, 0);
      hHandle_ = KS_INVALID_HANDLE;
    }
  }

  operator KSHandle () const {
    return hHandle_;
  }

protected:
  KsHandle(KSHandle handle = KS_INVALID_HANDLE)
    : hHandle_(handle) {
  }

  KSHandle hHandle_;

private:
  KsHandle(const KsHandle&);
  KsHandle& operator = (const KsHandle&);
};

//--------------------------------------------------------------------------------------------------------------
// KsSignal
//--------------------------------------------------------------------------------------------------------------

struct KsSignal : public KsHandle {
protected:
  KsSignal() {
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsEvent
//--------------------------------------------------------------------------------------------------------------

struct KsEvent : public KsSignal {
public:
  KsEvent(int flags = 0) {
    if (Error error = KS_createEvent(&hHandle_, null, flags))
      throw KsException(error, "KS_createEvent");
  }
  KsEvent(const char* name, int flags = 0) {
    if (Error error = KS_createEvent(&hHandle_, name, flags))
      throw KsException(error, "KS_createEvent");
  }

  void set() {
    if (Error error = KS_setEvent(hHandle_))
      throw KsException(error, "KS_setEvent");
  }
  void reset() {
    if (Error error = KS_resetEvent(hHandle_))
      throw KsException(error, "KS_resetEvent");
  }
  void pulse() {
    if (Error error = KS_pulseEvent(hHandle_))
      throw KsException(error, "KS_pulseEvent");
  }

  bool waitFor(int timeOut = 0) {
    switch (Error error = KS_waitForEvent(hHandle_, 0, timeOut)) {
      case KS_OK:
        return true;
      case KSERROR_WAIT_TIMEOUT:
        return false;
      default:
        throw KsException(error, "KS_waitForEvent");
    }
  }
  bool isSet() const {
    switch (Error error = KS_getEventState(hHandle_, null)) {
      case KS_OK:
        return true;
      case KSERROR_EVENT_NOT_SIGNALED:
        return false;
      default:
        throw KsException(error, "KS_getEventState");
    }
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsPipe
//--------------------------------------------------------------------------------------------------------------

struct KsPipe : public KsHandle {
public:
  KsPipe(int itemSize, int nItems, const KsString& name, KSHandle hNotify = KS_INVALID_HANDLE, int flags = 0) {
    if (Error error = KS_createPipe(&hHandle_, name, itemSize, nItems, hNotify, flags))
      throw KsException(error, "KS_createPipe");
  }

  bool get(void* pBuffer, int length, int* pLength = null, int flags = 0) {
    switch (Error error = KS_getPipe(hHandle_, pBuffer, length, pLength, flags)) {
      case KS_OK:
        return true;
      case KSERROR_NO_DATA_AVAILABLE:
        return false;
      case KSERROR_NOT_ENOUGH_MEMORY:
        return false;
      default:
        throw KsException(error, "KS_getPipe");
    }
  }

  bool put(const void* pBuffer, int length, int* pLength = null, int flags = 0) {
    switch (Error error = KS_putPipe(hHandle_, pBuffer, length, pLength, flags)) {
      case KS_OK:
        return true;
      case KSERROR_NOT_ENOUGH_MEMORY:
        return false;
      default:
        throw KsException(error, "KS_putPipe");
    }
  }

  void flush(int flags = 0) {
    if (Error error = KS_flushPipe(hHandle_, flags))
      throw KsException(error, "KS_flushPipe");
  }
};

//--------------------------------------------------------------------------------------------------------------
// _KsDataPipe
//--------------------------------------------------------------------------------------------------------------

template <typename T>
struct _KsDataPipe : public KsPipe {
public:
  _KsDataPipe(int nItems, const KsString& name = null, KSHandle hNotify = KS_INVALID_HANDLE, int flags = 0)
    : KsPipe(sizeof(T), nItems, name, hNotify, flags) {
  }

  using KsPipe::get;
  using KsPipe::put;

  T get() {
    T value = T();
    get(&value, 1);
    return value;
  }

  bool put(const T& value) {
    return put(&value, 1);
  }
};

//--------------------------------------------------------------------------------------------------------------
// _KsMessagePipe
//--------------------------------------------------------------------------------------------------------------

template <int messageSize>
struct _KsMessagePipe : public KsPipe {
public:
  _KsMessagePipe(int nItems, const KsString& name = null, KSHandle hNotify = KS_INVALID_HANDLE, int flags = 0)
    : KsPipe(messageSize, nItems, name, hNotify, flags | KSF_MESSAGE_PIPE) {
  }

  using KsPipe::get;
  using KsPipe::put;

  KsString get() {
    char buf[messageSize];
    int size = 0;
    if (!get(buf, messageSize, &size))
      return KsString();
    buf[size] = '\0';
    return buf;
  }

  bool put(const KsString& msg) {
    return put(static_cast<const char*>(msg), msg.length());
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsQuickMutex
//--------------------------------------------------------------------------------------------------------------

struct KsQuickMutex : public KsHandle  {
  KsQuickMutex(int level, int flags) {
    if (Error error = KS_createQuickMutex(&hHandle_, level, flags))
      throw KsException(error, "KS_createQuickMutex");
  }

  void request() const {
    if (Error error = KS_requestQuickMutex(hHandle_))
      throw KsException(error, "KS_requestQuickMutex");
  }

  void release() const {
    if (Error error = KS_releaseQuickMutex(hHandle_))
      throw KsException(error, "KS_releaseQuickMutex");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsQuickLock
//--------------------------------------------------------------------------------------------------------------

struct KsQuickLock {
public:
  KsQuickLock(const KsQuickMutex& mutex)
    : mutex_(mutex) {
    mutex_.request();
  }
  KsQuickLock(const KsQuickMutex* pMutex)
    : mutex_(*pMutex) {
    mutex_.request();
  }
  ~KsQuickLock() {
    mutex_.release();
  }

private:
  const KsQuickMutex& mutex_;
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_BASE_MODULE

//--------------------------------------------------------------------------------------------------------------
// Kernel Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_RTX_TOO_MANY_FUNCTIONS                  (KSERROR_CATEGORY_KERNEL+0x00000000)
#define KSERROR_CANNOT_EXECUTE_RTX_CALL                 (KSERROR_CATEGORY_KERNEL+0x00010000)
#define KSERROR_CANNOT_PREPARE_RTX                      (KSERROR_CATEGORY_KERNEL+0x00020000)
#define KSERROR_RTX_BAD_SYSTEM_CALL                     (KSERROR_CATEGORY_KERNEL+0x00030000)
#define KSERROR_CANNOT_LOAD_KERNEL                      (KSERROR_CATEGORY_KERNEL+0x00040000)
#define KSERROR_CANNOT_RELOCATE_KERNEL                  (KSERROR_CATEGORY_KERNEL+0x00050000)
#define KSERROR_KERNEL_ALREADY_LOADED                   (KSERROR_CATEGORY_KERNEL+0x00060000)
#define KSERROR_KERNEL_NOT_LOADED                       (KSERROR_CATEGORY_KERNEL+0x00070000)

//------ Flags ------
#define KSF_ANALYSE_ONLY                                0x00010000
#define KSF_FUNC_TABLE_GIVEN                            0x00020000
#define KSF_FORCE_RELOC                                 0x00040000
#define KSF_LOAD_DEPENDENCIES                           0x00001000
#define KSF_EXEC_ENTRY_POINT                            0x00002000

//------ Kernel commands ------
#define KS_GET_KERNEL_BASE_ADDRESS                      0x00000001

//------ Relocation by byte-wise instruction mapping ------
Error __stdcall KS_prepareKernelExec(
                CallBackProc callback, CallBackProc* pRelocated, int flags);
Error __stdcall KS_endKernelExec(
                CallBackProc relocated);
Error __stdcall KS_testKernelExec(
                CallBackProc relocated, void* pArgs);

//------ Relocation by loading DLL into kernel-space ------
Error __stdcall KS_registerKernelAddress(
                const char* name, CallBackProc appFunction, CallBackProc sysFunction, int flags);
Error __stdcall KS_loadKernel(
                KSHandle* phKernel, const char* dllName, const char* initProcName, void* pArgs, int flags);
Error __stdcall KS_loadKernelFromBuffer(
                KSHandle* phKernel, const char* dllName, const void* pBuffer, int length, const char* initProcName, void* pArgs, int flags);
Error __stdcall KS_freeKernel(
                KSHandle hKernel);
Error __stdcall KS_execKernelFunction(
                KSHandle hKernel, const char* name, void* pArgs, void* pContext, int flags);
Error __stdcall KS_getKernelFunction(
                KSHandle hKernel, const char* name, CallBackProc* pRelocated, int flags);
Error __stdcall KS_enumKernelFunctions(
                KSHandle hKernel, int index, char* pName, int flags);
Error __stdcall KS_execKernelCommand(
                KSHandle hKernel, int command, void* pParam, int flags);
Error __stdcall KS_createKernelCallBack(
                KSHandle* phCallBack, KSHandle hKernel, const char* name, void* pArgs, int flags, int prio);

//------ Helper functions ------
Error __stdcall KS_execSyncFunction(
                KSHandle hHandler, CallBackProc proc, void* pArgs, int flags);
Error __stdcall KS_execSystemCall(
                const char* fileName, const char* funcName, void* pData, int flags);

//------ Memory functions ------
Error __stdcall KS_memCpy(
                void* pDst, const void* pSrc, int size, int flags);
Error __stdcall KS_memMove(
                void* pDst, const void* pSrc, int size, int flags);
Error __stdcall KS_memSet(
                void* pMem, int value, int size, int flags);
Error __stdcall KS_malloc(
                void** ppAllocated, int bytes, int flags);
Error __stdcall KS_free(
                void* pAllocated, int flags);

//------ Interlocked functions ------
Error __stdcall KS_interlocked(
                void* pInterlocked, int operation, void* pParameter1, void* pParameter2, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if 0

//--------------------------------------------------------------------------------------------------------------
// operator new
//--------------------------------------------------------------------------------------------------------------

inline void* __cdecl operator new (size_t size) {
  void* pAllocated;
  if (Error error = KS_malloc(&pAllocated, static_cast<int>(size), 0))
    throw KsException(error, "KS_malloc");
  return pAllocated;
}

//--------------------------------------------------------------------------------------------------------------
// operator new []
//--------------------------------------------------------------------------------------------------------------

inline void* __cdecl operator new [] (size_t size) {
  void* pAllocated;
  if (Error error = KS_malloc(&pAllocated, static_cast<int>(size), 0))
    throw KsException(error, "KS_malloc");
  return pAllocated;
}

#endif // 0

#if 0

//--------------------------------------------------------------------------------------------------------------
// operator delete
//--------------------------------------------------------------------------------------------------------------

inline void __cdecl operator delete (void* pMemory) {
  KS_free(pMemory, 0);
}

//--------------------------------------------------------------------------------------------------------------
// operator delete []
//--------------------------------------------------------------------------------------------------------------

inline void __cdecl operator delete [] (void* pMemory) {
  KS_free(pMemory, 0);
}

#endif // 0

#ifndef __PLACEMENT_NEW_INLINE

//--------------------------------------------------------------------------------------------------------------
// operator new
//--------------------------------------------------------------------------------------------------------------

inline void* __cdecl operator new (size_t, void* pMemory) {
  return pMemory;
}

//--------------------------------------------------------------------------------------------------------------
// operator delete
//--------------------------------------------------------------------------------------------------------------

inline void __cdecl operator delete (void*, void*) {
}

#define __PLACEMENT_NEW_INLINE

#endif // __PLACEMENT_NEW_INLINE

//--------------------------------------------------------------------------------------------------------------
// KsKernel
//--------------------------------------------------------------------------------------------------------------

struct KsKernel : public KsHandle {
public:
  KsKernel() {
  }
  KsKernel(const KsString& name, int flags) {
    load(name, null, null, flags);
  }
  KsKernel(const KsString& name, const KsString& initProcName, int flags) {
    load(name, initProcName, null, flags);
  }
  KsKernel(const KsString& name, const KsString& initProcName, void* pParam, int flags) {
    load(name, initProcName, pParam, flags);
  }

  void load(const KsString& name, const KsString& initProcName, void* pParam, int flags) {
    if (Error error = KS_loadKernel(&hHandle_, name, initProcName, pParam, flags))
      throw KsException(error, "KS_loadKernel");
  }

  void load(const KsString& name, const void* pBuffer, int length, int flags = 0) {
    if (Error error = KS_loadKernelFromBuffer(&hHandle_, name, pBuffer, length, null, null, flags))
      throw KsException(error, "KS_loadKernelFromBuffer");
  }
  void load(const KsString& name, const void* pBuffer, int length, const KsString& initProcName = null, void* pParam = null, int flags = 0) {
    if (Error error = KS_loadKernelFromBuffer(&hHandle_, name, pBuffer, length, initProcName, pParam, flags))
      throw KsException(error, "KS_loadKernelFromBuffer");
  }

  void execFunction(const KsString& name, const KsSharedMem* pArgs, void* pContext, int flags = 0) {
    if (Error error = KS_execKernelFunction(hHandle_, name, _chooseSharedMem(pArgs, flags), pContext, flags))
      throw KsException(error, "KS_execKernelFunction");
  }

  bool enumFunctions(int index, KsString* pName) const {
    char pBuffer[256];
    Error error = KS_enumKernelFunctions(hHandle_, index, pBuffer, 0);
    if (error && KSERROR_CODE(error) != KSERROR_CANNOT_FIND_ADDRESS)
      throw KsException(error, "KS_enumKernelFunctions");
    *pName = pBuffer;
    return error == KS_OK;
  }

private:
  static void* _chooseSharedMem(const KsSharedMem* pArgs, int flags) {
    if (pArgs == null)
      return null;
    if (flags & (KSF_DIRECT_EXEC | KSF_KERNEL_EXEC | KSF_REALTIME_EXEC))
      return pArgs->sys();
    return pArgs->ptr();
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsKernelFunctionEnumeration
//--------------------------------------------------------------------------------------------------------------

struct KsKernelFunctionEnumeration : public _KsEnumeration<KsString> {
public:
  KsKernelFunctionEnumeration(const KsKernel& kernel) : kernel_(kernel) {
    refresh();
  }

  void refresh() {
    clear();
    KsString name;
    for (int i = 0; ; ++i) {
      if (!kernel_.enumFunctions(i, &name))
        break;
      add(name);
    }
  }

private:
  const KsKernel& kernel_;
};

//--------------------------------------------------------------------------------------------------------------
// KsCallBack
//--------------------------------------------------------------------------------------------------------------

struct KsCallBack : public KsSignal {
public:
#ifdef _NATIVE_NULLPTR_SUPPORTED
  KsCallBack(CallBackRoutine pProc, const decltype(null)&, int flags = 0, int prio = 0) {
    init(pProc, static_cast<void*>(null), flags, prio);
  }
#endif
  KsCallBack(CallBackRoutine pProc, void* pArgs = null, int flags = 0, int prio = 0) {
    init(pProc, pArgs, flags, prio);
  }
  KsCallBack(CallBackRoutine pProc, const KsSharedMem& args, int flags = 0, int prio = 0) {
    init(pProc, &args, flags, prio);
  }
  KsCallBack(CallBackRoutine pProc, const KsSharedMem* pArgs, int flags = 0, int prio = 0) {
    init(pProc, pArgs, flags, prio);
  }

#ifdef _NATIVE_NULLPTR_SUPPORTED
  KsCallBack(CallBackProc pProc, const decltype(null)&, int flags = 0, int prio = 0) {
    init(reinterpret_cast<CallBackRoutine>(pProc), static_cast<void*>(null), flags | KSF_NO_CONTEXT, prio);
  }
#endif
  KsCallBack(CallBackProc pProc, void* pArgs = null, int flags = 0, int prio = 0) {
    init(reinterpret_cast<CallBackRoutine>(pProc), pArgs, flags | KSF_NO_CONTEXT, prio);
  }
  KsCallBack(CallBackProc pProc, const KsSharedMem& args, int flags = 0, int prio = 0) {
    init(reinterpret_cast<CallBackRoutine>(pProc), &args, flags | KSF_NO_CONTEXT, prio);
  }
  KsCallBack(CallBackProc pProc, const KsSharedMem* pArgs, int flags = 0, int prio = 0) {
    init(reinterpret_cast<CallBackRoutine>(pProc), pArgs, flags | KSF_NO_CONTEXT, prio);
  }

#ifdef _NATIVE_NULLPTR_SUPPORTED
  KsCallBack(const KsKernel& kernel, const KsString& name, const decltype(null)&, int flags = 0, int prio = 0) {
    init(kernel, name, static_cast<void*>(null), flags, prio);
  }
#endif
  KsCallBack(const KsKernel& kernel, const KsString& name, void* pArgs = null, int flags = 0, int prio = 0) {
    init(kernel, name, pArgs, flags, prio);
  }
  KsCallBack(const KsKernel& kernel, const KsString& name, const KsSharedMem& args, int flags = 0, int prio = 0) {
    init(kernel, name, &args, flags, prio);
  }
  KsCallBack(const KsKernel& kernel, const KsString& name, const KsSharedMem* pArgs, int flags = 0, int prio = 0) {
    init(kernel, name, pArgs, flags, prio);
  }

  Error exec(void* pContext = null) {
    return KS_execCallBack(hHandle_, pContext, 0);
  }

private:
  void init(CallBackRoutine pProc, void* pArgs, int flags, int prio) {
    if (Error error = KS_createCallBack(&hHandle_, pProc, pArgs, flags, prio))
      throw KsException(error, "KS_createCallBack");
  }
  void init(const KsKernel& kernel, const KsString& name, void* pArgs, int flags, int prio) {
    if (Error error = KS_createKernelCallBack(&hHandle_, kernel, name, pArgs, flags, prio))
      throw KsException(error, "KS_createKernelCallBack");
  }
  void init(CallBackRoutine pProc, const KsSharedMem* pArgs, int flags, int prio) {
    init(pProc, _chooseSharedMem(pArgs, flags), flags, prio);
  }
  void init(const KsKernel& kernel, const KsString& name, const KsSharedMem* pArgs, int flags, int prio) {
    init(kernel, name, _chooseSharedMem(pArgs, flags), flags, prio);
  }

  static void* _chooseSharedMem(const KsSharedMem* pArgs, int flags) {
    if (pArgs == null)
      return null;
    if (flags & (KSF_DIRECT_EXEC | KSF_KERNEL_EXEC | KSF_REALTIME_EXEC))
      return pArgs->sys();
    return pArgs->ptr();
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_KERNEL_MODULE

//--------------------------------------------------------------------------------------------------------------
// IoPort Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_CANNOT_ENABLE_IO_RANGE                  (KSERROR_CATEGORY_IOPORT+0x00000000)
#define KSERROR_BUS_NOT_FOUND                           (KSERROR_CATEGORY_IOPORT+0x00010000)
#define KSERROR_BAD_SLOT_NUMBER                         (KSERROR_CATEGORY_IOPORT+0x00020000)

//------ Number of address entries in PCI data ------
#define KS_PCI_TYPE0_ADDRESSES                          0x00000006
#define KS_PCI_TYPE1_ADDRESSES                          0x00000002
#define KS_PCI_TYPE2_ADDRESSES                          0x00000004

//------ Bit encodings for KSPciBusData.headerType ------
#define KSF_PCI_MULTIFUNCTION                           0x00000080
#define KSF_PCI_DEVICE_TYPE                             0x00000000
#define KSF_PCI_PCI_BRIDGE_TYPE                         0x00000001
#define KSF_PCI_CARDBUS_BRIDGE_TYPE                     0x00000002

//------ Bit encodings for KSPciBusData.command ------
#define KSF_PCI_ENABLE_IO_SPACE                         0x00000001
#define KSF_PCI_ENABLE_MEMORY_SPACE                     0x00000002
#define KSF_PCI_ENABLE_BUS_MASTER                       0x00000004
#define KSF_PCI_ENABLE_SPECIAL_CYCLES                   0x00000008
#define KSF_PCI_ENABLE_WRITE_AND_INVALIDATE             0x00000010
#define KSF_PCI_ENABLE_VGA_COMPATIBLE_PALETTE           0x00000020
#define KSF_PCI_ENABLE_PARITY                           0x00000040
#define KSF_PCI_ENABLE_WAIT_CYCLE                       0x00000080
#define KSF_PCI_ENABLE_SERR                             0x00000100
#define KSF_PCI_ENABLE_FAST_BACK_TO_BACK                0x00000200

//------ Bit encodings for KSPciBusData.status ------
#define KSF_PCI_STATUS_CAPABILITIES_LIST                0x00000010
#define KSF_PCI_STATUS_FAST_BACK_TO_BACK                0x00000080
#define KSF_PCI_STATUS_DATA_PARITY_DETECTED             0x00000100
#define KSF_PCI_STATUS_DEVSEL                           0x00000600
#define KSF_PCI_STATUS_SIGNALED_TARGET_ABORT            0x00000800
#define KSF_PCI_STATUS_RECEIVED_TARGET_ABORT            0x00001000
#define KSF_PCI_STATUS_RECEIVED_MASTER_ABORT            0x00002000
#define KSF_PCI_STATUS_SIGNALED_SYSTEM_ERROR            0x00004000
#define KSF_PCI_STATUS_DETECTED_PARITY_ERROR            0x00008000

//------ Bit encodings for KSPciBusData.baseAddresses ------
#define KSF_PCI_ADDRESS_IO_SPACE                        0x00000001
#define KSF_PCI_ADDRESS_MEMORY_TYPE_MASK                0x00000006
#define KSF_PCI_ADDRESS_MEMORY_PREFETCHABLE             0x00000008

//------ PCI types ------
#define KSF_PCI_TYPE_32BIT                              0x00000000
#define KSF_PCI_TYPE_20BIT                              0x00000002
#define KSF_PCI_TYPE_64BIT                              0x00000004

//------ Resource types ------
#define KSRESOURCE_TYPE_NONE                            0x00000000
#define KSRESOURCE_TYPE_PORT                            0x00000001
#define KSRESOURCE_TYPE_INTERRUPT                       0x00000002
#define KSRESOURCE_TYPE_MEMORY                          0x00000003
#define KSRESOURCE_TYPE_STRING                          0x000000ff

//------ KSRESOURCE_SHARE ------
#define KSRESOURCE_SHARE_UNDETERMINED                   0x00000000
#define KSRESOURCE_SHARE_DEVICEEXCLUSIVE                0x00000001
#define KSRESOURCE_SHARE_DRIVEREXCLUSIVE                0x00000002
#define KSRESOURCE_SHARE_SHARED                         0x00000003

//------ KSRESOURCE_MODE / KSRESOURCE_TYPE_PORT ------
#define KSRESOURCE_MODE_MEMORY                          0x00000000
#define KSRESOURCE_MODE_IO                              0x00000001

//------ KSRESOURCE_MODE / KSRESOURCE_TYPE_INTERRUPT ------
#define KSRESOURCE_MODE_LEVELSENSITIVE                  0x00000000
#define KSRESOURCE_MODE_LATCHED                         0x00000001

//------ KSRESOURCE_MODE / KSRESOURCE_TYPE_MEMORY ------
#define KSRESOURCE_MODE_READ_WRITE                      0x00000000
#define KSRESOURCE_MODE_READ_ONLY                       0x00000001
#define KSRESOURCE_MODE_WRITE_ONLY                      0x00000002

#define KSRESOURCE_TYPE(flags)                  ((flags) & 0x000000ff)
#define KSRESOURCE_SHARE(flags)                 (((flags) & 0x0000ff00) >> 8)
#define KSRESOURCE_MODE(flags)                  (((flags) & 0x00ff0000) >> 16)

#define KS_MAKE_PCI_SLOT_NUMBER(deviceNumber,functionNumber)    \
        ((deviceNumber & 0x001f) | (functionNumber << 5))

//------ Types & Structures ------
typedef struct {
  ushort vendorID;
  ushort deviceID;
  ushort command;
  ushort status;
  byte revisionID;
  byte progIf;
  byte subClass;
  byte baseClass;
  byte cacheLineSize;
  byte latencyTimer;
  byte headerType;
  byte BIST;
  uint pBaseAddresses[6];
  uint CIS;
  ushort subVendorID;
  ushort subSystemID;
  uint baseROMAddress;
  uint capabilityList;
  uint reserved;
  byte interruptLine;
  byte interruptPin;
  byte minimumGrant;
  byte maximumLatency;
  byte pDeviceSpecific[192];
} KSPciBusData;

// DO NOT USE! - OBSOLETE!
typedef struct {
  uint flags;
  uint data0;
  uint data1;
  uint data2;
} KSResourceItem;

// DO NOT USE! - OBSOLETE! - USE 'KSResourceInfoEx' instead!
typedef struct {
  uint busType;
  uint busNumber;
  uint version;
  uint itemCount;
  KSResourceItem pItems[8];
} KSResourceInfo;

typedef struct {
  uint flags;
  uint address;
  uint reserved;
  uint length;
} KSRangeResourceItem;

typedef struct {
  uint flags;
  uint level;
  uint vector;
  uint affinity;
} KSInterruptResourceItem;

typedef struct {
  int busType;
  int busNumber;
  int version;
  int itemCount;
  KSRangeResourceItem range0;
  KSRangeResourceItem range1;
  KSRangeResourceItem range2;
  KSRangeResourceItem range3;
  KSRangeResourceItem range4;
  KSRangeResourceItem range5;
  KSInterruptResourceItem interruptItem;
  KSResourceItem reserved;
} KSResourceInfoEx;

#define KSRESOURCEINFOEX_DEFINED

//------ I/O port access functions ------
Error __stdcall KS_enableIoRange(
                uint ioPortBase, int count, int flags);
Error __stdcall KS_readIoPort(
                uint ioPort, uint* pValue, int flags);
Error __stdcall KS_writeIoPort(
                uint ioPort, uint value, int flags);
uint __stdcall KS_inpb(
                uint ioPort);
uint __stdcall KS_inpw(
                uint ioPort);
uint __stdcall KS_inpd(
                uint ioPort);
void __stdcall KS_outpb(
                uint ioPort, uint value);
void __stdcall KS_outpw(
                uint ioPort, uint value);
void __stdcall KS_outpd(
                uint ioPort, uint value);

//------ macros ------
#if defined(KS_INCL_PORT_IO_MACROS)

#define inp(port)                               KS_inpb((port))
#define inpb(port)                              KS_inpb((port))
#define inpw(port)                              KS_inpw((port))
#define inpd(port)                              KS_inpd((port))

#define outp(port,value)                        KS_outpb((port),(value))
#define outpb(port,value)                       KS_outpb((port),(value))
#define outpw(port,value)                       KS_outpw((port),(value))
#define outpd(port,value)                       KS_outpd((port),(value))

#endif // KS_INCL_PORT_IO_MACROS

//------ Bus data & resource info ------
Error __stdcall KS_getBusData(
                KSPciBusData* pBusData, int busNumber, int slotNumber, int flags);
Error __stdcall KS_setBusData(
                KSPciBusData* pBusData, int busNumber, int slotNumber, int flags);
// DO NOT USE! - OBSOLETE! - USE 'KS_getResourceInfoEx' instead!
Error __stdcall KS_getResourceInfo(
                const char* name, KSResourceInfo* pInfo);
Error __stdcall KS_getResourceInfoEx(
                const char* name, KSResourceInfoEx* pInfo);

//------ Quiet section (obsolete) ------
Error __stdcall KS_enterQuietSection(
                int flags);
Error __stdcall KS_releaseQuietSection(
                int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_IOPORT_MODULE

//--------------------------------------------------------------------------------------------------------------
// Memory Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_CANNOT_MAP_PHYSMEM                      (KSERROR_CATEGORY_MEMORY+0x00000000)
#define KSERROR_CANNOT_UNMAP_PHYSMEM                    (KSERROR_CATEGORY_MEMORY+0x00010000)
#define KSERROR_PHYSMEM_DIFFERENT_SIZE                  (KSERROR_CATEGORY_MEMORY+0x00020000)
#define KSERROR_CANNOT_ALLOC_PHYSMEM                    (KSERROR_CATEGORY_MEMORY+0x00030000)
#define KSERROR_CANNOT_LOCK_MEM                         (KSERROR_CATEGORY_MEMORY+0x00040000)
#define KSERROR_CANNOT_UNLOCK_MEM                       (KSERROR_CATEGORY_MEMORY+0x00050000)

//------ Flags ------
#define KSF_MAP_INTERNAL                                0x00000000

#ifndef KSRESOURCEINFOEX_DEFINED
#define KSResourceInfoEx void
#endif

//------ Physical memory management functions ------
Error __stdcall KS_mapDeviceMem(
                void** ppAppPtr, void** ppSysPtr, KSResourceInfoEx* pInfo, int index, int flags);
Error __stdcall KS_mapPhysMem(
                void** ppAppPtr, void** ppSysPtr, uint physAddr, int size, int flags);
Error __stdcall KS_unmapPhysMem(
                void* pAppPtr);
Error __stdcall KS_allocPhysMem(
                void** ppAppPtr, void** ppSysPtr, uint* physAddr, int size, int flags);
Error __stdcall KS_freePhysMem(
                void* pAppPtr);
Error __stdcall KS_copyPhysMem(
                uint physAddr, void* pBuffer, uint size, uint offset, int flags);
Error __stdcall KS_getPhysAddr(
                void** pPhysAddr, void* pDosLinAddr);

// ATTENTION: functions for getting shared memory: see 'Base Module'

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_MEMORY_MODULE

//--------------------------------------------------------------------------------------------------------------
// Clock Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_NO_RDTSC_AVAILABLE                      (KSERROR_CATEGORY_SPECIAL+0x00010000)

//------ Flags ------
#define KSF_USE_GIVEN_FREQ                              0x00010000
#define KSF_CLOCK_DELEGATE                              0x00000001
#define KSF_CLOCK_CONVERT_ONLY                          0x00000002

//------ Clock source constants (for 'clockId') ------
#define KS_CLOCK_DEFAULT                                0x00000000
#define KS_CLOCK_MEASURE_SYSTEM_TIMER                   0x00000001
#define KS_CLOCK_MEASURE_CPU_COUNTER                    0x00000002
#define KS_CLOCK_MEASURE_APIC_TIMER                     0x00000003
#define KS_CLOCK_MEASURE_PM_TIMER                       0x00000004
#define KS_CLOCK_MEASURE_PC_TIMER                       0x00000005
#define KS_CLOCK_MEASURE_INTERVAL_COUNTER               0x00000006
#define KS_CLOCK_MEASURE_HPET                           0x00000007
#define KS_CLOCK_MEASURE_HIGHEST                        0x00000008
#define KS_CLOCK_CONVERT_HIGHEST                        0x00000009
#define KS_CLOCK_PC_TIMER                               0x0000000a
#define KS_CLOCK_MACHINE_TIME                           0x0000000b
#define KS_CLOCK_ABSOLUTE_TIME                          0x0000000c
#define KS_CLOCK_ABSOLUTE_TIME_LOCAL                    0x0000000d
#define KS_CLOCK_MICROSECONDS                           0x0000000e
#define KS_CLOCK_UNIX_TIME                              0x0000000f
#define KS_CLOCK_UNIX_TIME_LOCAL                        0x00000010
#define KS_CLOCK_MILLISECONDS                           0x00000011
#define KS_CLOCK_DOS_TIME                               0x00000012
#define KS_CLOCK_DOS_TIME_LOCAL                         0x00000013
#define KS_CLOCK_IEEE1588_TIME                          0x00000014
#define KS_CLOCK_IEEE1588_CONVERT                       0x00000015
#define KS_CLOCK_TIME_OF_DAY                            0x00000016
#define KS_CLOCK_TIME_OF_DAY_LOCAL                      0x00000017
#define KS_CLOCK_FIRST_USER                             0x00000018

//------ Obsolete clock source constant (use KS_CLOCK_MACHINE_TIME instead!) ------
#define KS_CLOCK_SYSTEM_TIME                            0x0000000b

//------ Types & Structures ------
typedef struct {
  int structSize;
  int flags;
  UInt64 frequency;
  UInt64 offset;
  uint baseClockId;
} KSClockSource;

//------ Clock functions - Obsolete! Do not use! ------
Error __stdcall KS_calibrateMachineTime(
                uint* pFrequency, int flags);
Error __stdcall KS_getSystemTicks(
                UInt64* pSystemTicks);
Error __stdcall KS_getSystemTime(
                UInt64* pSystemTime);
Error __stdcall KS_getMachineTime(
                UInt64* pMachineTime);
Error __stdcall KS_getAbsTime(
                UInt64* pAbsTime);

//------ Clock functions ------
Error __stdcall KS_getClock(
                UInt64* pClock, int clockId);
Error __stdcall KS_getClockSource(
                KSClockSource* pClockSource, int clockId);
Error __stdcall KS_setClockSource(
                const KSClockSource* pClockSource, int clockId);
Error __stdcall KS_microDelay(
                int delay);
Error __stdcall KS_convertClock(
                UInt64* pValue, int clockIdFrom, int clockIdTo, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsClock
//--------------------------------------------------------------------------------------------------------------

struct KsClock {
public:
  KsClock(int id = KS_CLOCK_MACHINE_TIME)
    : id_(id) {
  }

  operator int64 () {
    int64 clock;
    if (Error error = KS_getClock(reinterpret_cast<UInt64*>(&clock), id_))
      throw KsException(error, "KS_getClock");
    return clock;
  }

  void getSource(KSClockSource* pSource) {
    if (Error error = KS_getClockSource(pSource, id_))
      throw KsException(error, "KS_getClockSource");
  }
  KSClockSource source() {
    KSClockSource source;
    source.structSize = sizeof(KSClockSource);
    getSource(&source);
    return source;
  }
  int64 frequency() {
    KSClockSource source;
    source.structSize = sizeof(KSClockSource);
    getSource(&source);
    return *reinterpret_cast<int64*>(&source.frequency);
  }
  int64 offset() {
    KSClockSource source;
    source.structSize = sizeof(KSClockSource);
    getSource(&source);
    return *reinterpret_cast<int64*>(&source.offset);
  }

private:
  const int id_;
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_CLOCK_MODULE

//--------------------------------------------------------------------------------------------------------------
// System Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ System contexts ------
#define SYSTEMHANDLER_CONTEXT                           (USER_CONTEXT+0x00000030)

//------ System events ------
#define KS_SYSTEM_CRASH                                 0x00000001
#define KS_SYSTEM_SHUTDOWN                              0x00000002
#define KS_SYSTEM_PROCESS_ATTACH                        0x00000010
#define KS_SYSTEM_PROCESS_DETACH                        0x00000020
#define KS_SYSTEM_PROCESS_ABORT                         0x00000040

//------ Types & Structures ------
typedef struct {
  int ctxType;
  int eventType;
  int handlerProcessId;
  int currentProcessId;
} SystemHandlerUserContext;

//------ System handling functions ------
Error __stdcall KS_installSystemHandler(
                int eventMask, KSHandle hSignal, int flags);
Error __stdcall KS_removeSystemHandler(
                int eventMask);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_SYSTEM_MODULE

//--------------------------------------------------------------------------------------------------------------
// Device Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Flags ------
#define KSF_SERIAL_PORT                                 0x00200000

//------ Function codes ------
#define KS_DEVICE_CREATE                                0x00000000
#define KS_DEVICE_CLOSE                                 0x00000001
#define KS_DEVICE_READ                                  0x00000002
#define KS_DEVICE_WRITE                                 0x00000003
#define KS_DEVICE_CONTROL                               0x00000004
#define KS_DEVICE_CLEANUP                               0x00000005
#define KS_DEVICE_CANCEL_REQUEST                        0x00010000

//------ Return codes ------
#define KS_RETURN_SUCCESS                               0x00000000
#define KS_RETURN_UNSUCCESSFUL                          0x00000001
#define KS_RETURN_INVALID_DEVICE_REQUEST                0x00000002
#define KS_RETURN_INVALID_PARAMETER                     0x00000003
#define KS_RETURN_TIMEOUT                               0x00000004
#define KS_RETURN_CANCELLED                             0x00000005
#define KS_RETURN_PENDING                               0x00000006
#define KS_RETURN_DELETE_PENDING                        0x00000007
#define KS_RETURN_BUFFER_TOO_SMALL                      0x00000008

//------ Types & Structures ------
typedef struct {
  int ctxType;
  KSHandle hRequest;
  int functionCode;
  int controlCode;
  byte* pBuffer;
  uint size;
  int returnCode;
} DeviceUserContext;

typedef struct {
  KSHandle hOnCreate;
  KSHandle hOnClose;
  KSHandle hOnRead;
  KSHandle hOnWrite;
  KSHandle hOnControl;
  KSHandle hOnCleanup;
} KSDeviceFunctions;

//------ Device functions ------
Error __stdcall KS_createDevice(
                KSHandle* phDevice, const char* name, KSDeviceFunctions* pFunctions, int flags);
Error __stdcall KS_closeDevice(
                KSHandle hDevice);
Error __stdcall KS_resumeDevice(
                KSHandle hDevice, KSHandle hRequest, byte* pBuffer, int size, int returnCode, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_DEVICE_MODULE

//--------------------------------------------------------------------------------------------------------------
// Keyboard Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Flags ------
#define KSF_SIGNAL_MAKE                                 0x00000001
#define KSF_SIGNAL_BREAK                                0x00000002
#define KSF_IGNORE_KEY                                  0x00000010
#define KSF_MODIFY_KEY                                  0x00000020

//------ Key mask modifier ------
#define KSF_KEY_SHIFT                                   0x00000011
#define KSF_KEY_SHIFTR                                  0x00000001
#define KSF_KEY_SHIFTL                                  0x00000010
#define KSF_KEY_CTRL                                    0x00000022
#define KSF_KEY_CTRLR                                   0x00000002
#define KSF_KEY_CTRLL                                   0x00000020
#define KSF_KEY_ALT                                     0x00000044
#define KSF_KEY_ALTR                                    0x00000004
#define KSF_KEY_ALTL                                    0x00000040
#define KSF_KEY_WIN                                     0x00000088
#define KSF_KEY_WINR                                    0x00000008
#define KSF_KEY_WINL                                    0x00000080

//------ Key mask groups ------
#define KSF_KEY_ALPHA_NUM                               0x00000100
#define KSF_KEY_NUM_PAD                                 0x00000200
#define KSF_KEY_CONTROL                                 0x00000400
#define KSF_KEY_FUNCTION                                0x00000800

//------ Key mask combinations ------
#define KSF_KEY_CTRL_ALT_DEL                            0x00010000
#define KSF_KEY_CTRL_ESC                                0x00020000
#define KSF_KEY_ALT_ESC                                 0x00040000
#define KSF_KEY_ALT_TAB                                 0x00080000
#define KSF_KEY_ALT_ENTER                               0x00100000
#define KSF_KEY_ALT_PRINT                               0x00200000
#define KSF_KEY_ALT_SPACE                               0x00400000
#define KSF_KEY_ESC                                     0x00800000
#define KSF_KEY_TAB                                     0x01000000
#define KSF_KEY_ENTER                                   0x02000000
#define KSF_KEY_PRINT                                   0x04000000
#define KSF_KEY_SPACE                                   0x08000000
#define KSF_KEY_BACKSPACE                               0x10000000
#define KSF_KEY_PAUSE                                   0x20000000
#define KSF_KEY_APP                                     0x40000000
#define KSF_KEY_ALL                                     0x7fffffff

#define KSF_KEY_MODIFIER                        (KSF_KEY_CTRL | KSF_KEY_ALT | \
                                                KSF_KEY_SHIFT | KSF_KEY_WIN)

#define KSF_KEY_SYSTEM                          (KSF_KEY_WIN | \
                                                KSF_KEY_CTRL_ALT_DEL | KSF_KEY_CTRL_ESC | \
                                                KSF_KEY_ALT_ESC | KSF_KEY_ALT_TAB)

#define KSF_KEY_SYSTEM_DOS                      (KSF_KEY_SYSTEM | \
                                                KSF_KEY_ALT_ENTER | KSF_KEY_ALT_PRINT | \
                                                KSF_KEY_ALT_SPACE | KSF_KEY_PRINT)

//------ Keyboard events ------
#define KS_KEYBOARD_CONTEXT                             0x00000300

//------ Context structures ------
typedef struct {
  int ctxType;
  int mask;
  ushort modifier;
  ushort port;
  ushort state;
  ushort key;
} KeyboardUserContext;

typedef KeyboardUserContext KeyBoardUserContext;
//------ Keyboard functions ------
Error __stdcall KS_createKeyHandler(
                uint mask, KSHandle hSignal, int flags);
Error __stdcall KS_removeKeyHandler(
                uint mask);
Error __stdcall KS_simulateKeyStroke(
                uint* pKeys);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_KEYBOARD_MODULE

//--------------------------------------------------------------------------------------------------------------
// Interrupt Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_IRQ_ALREADY_USED                        (KSERROR_CATEGORY_INTERRUPT+0x00000000)
#define KSERROR_CANNOT_INSTALL_HANDLER                  (KSERROR_CATEGORY_INTERRUPT+0x00010000)
#define KSERROR_BAD_IRQ_INSTALLATION                    (KSERROR_CATEGORY_INTERRUPT+0x00020000)
#define KSERROR_NO_HANDLER_INSTALLED                    (KSERROR_CATEGORY_INTERRUPT+0x00030000)
#define KSERROR_NOBODY_IS_WAITING                       (KSERROR_CATEGORY_INTERRUPT+0x00040000)
#define KSERROR_IRQ_DISABLED                            (KSERROR_CATEGORY_INTERRUPT+0x00050000)
#define KSERROR_IRQ_ENABLED                             (KSERROR_CATEGORY_INTERRUPT+0x00060000)
#define KSERROR_IRQ_IN_SERVICE                          (KSERROR_CATEGORY_INTERRUPT+0x00070000)
#define KSERROR_IRQ_HANDLER_REMOVED                     (KSERROR_CATEGORY_INTERRUPT+0x00080000)

//------ Flags ------
#define KSF_DONT_PASS_NEXT                              0x00020000
#define KSF_PCI_INTERRUPT                               0x00040000

//------ Interrupt events ------
#define KS_INTERRUPT_CONTEXT                            0x00000100

//------ Types & Structures ------
typedef struct {
  int ctxType;
  KSHandle hInterrupt;
  uint irq;
  uint consumed;
} InterruptUserContext;

#ifndef KSRESOURCEINFOEX_DEFINED
#define KSResourceInfoEx void
#endif

//------ Interrupt handling functions ------
Error __stdcall KS_createDeviceInterrupt(
                KSHandle* phInterrupt, KSResourceInfoEx* pInfo, KSHandle hSignal, int flags);
Error __stdcall KS_createDeviceInterruptEx(
                KSHandle* phInterrupt, const char* deviceName, KSHandle hSignal, int flags);
Error __stdcall KS_createInterrupt(
                KSHandle* phInterrupt, int irq, KSHandle hSignal, int flags);
Error __stdcall KS_removeInterrupt(
                KSHandle hInterrupt);
Error __stdcall KS_disableInterrupt(
                KSHandle hInterrupt);
Error __stdcall KS_enableInterrupt(
                KSHandle hInterrupt);
Error __stdcall KS_simulateInterrupt(
                KSHandle hInterrupt);
Error __stdcall KS_getInterruptState(
                KSHandle hInterrupt, HandlerState* pState);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_INTERRUPT_MODULE

//--------------------------------------------------------------------------------------------------------------
// UART Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Flags ------
#define KSF_CHECK_LINE_ERROR                            0x00008000
#define KSF_NO_FIFO                                     0x00001000
#define KSF_USE_FIFO                                    0x00040000
#define KSF_9_BIT_MODE                                  0x00080000
#define KSF_TOGGLE_DTR                                  0x00100000
#define KSF_TOGGLE_RTS                                  0x00200000
#define KSF_TOGGLE_INVERSE                              0x00400000

//------ Uart commands ------
#define KS_CLR_DTR                                      0x00000004
#define KS_SET_DTR                                      0x00000005
#define KS_CLR_RTS                                      0x00000006
#define KS_SET_RTS                                      0x00000007
#define KS_CLR_BREAK                                    0x00000008
#define KS_SET_BREAK                                    0x0000000a
#define KS_SKIP_NEXT                                    0x0000000b
#define KS_CHANGE_LINE_CONTROL                          0x0000000d
#define KS_SET_BAUD_RATE_MAX                            0x0000000e
#define KS_ENABLE_FIFO                                  0x0000000f
#define KS_DISABLE_FIFO                                 0x00000010
#define KS_ENABLE_INTERRUPTS                            0x00000011
#define KS_DISABLE_INTERRUPTS                           0x00000012
#define KS_SET_RECV_FIFO_SIZE                           0x00000013

//------ Line Errors ------
#define KS_UART_ERROR_PARITY                            0x00000001
#define KS_UART_ERROR_FRAME                             0x00000002
#define KS_UART_ERROR_OVERRUN                           0x00000004
#define KS_UART_ERROR_BREAK                             0x00000008
#define KS_UART_ERROR_FIFO                              0x00000010

//------ UART events ------
#define KS_UART_RECV                                    0x00000000
#define KS_UART_RECV_ERROR                              0x00000001
#define KS_UART_XMIT                                    0x00000002
#define KS_UART_XMIT_EMPTY                              0x00000003
#define KS_UART_LSR_CHANGE                              0x00000004
#define KS_UART_MSR_CHANGE                              0x00000005
#define KS_UART_BREAK                                   0x00000006

//------ Types & Structures ------
typedef struct {
  int ctxType;
  KSHandle hUart;
  int value;
  int lineError;
} UartUserContext;

typedef struct {
  int ctxType;
  KSHandle hUart;
  byte reg;
} UartRegisterUserContext;

typedef struct {
  int baudRate;
  byte parity;
  byte dataBit;
  byte stopBit;
  byte reserved;
} KSUartProperties;

typedef struct {
  int structSize;
  KSUartProperties properties;
  byte modemState;
  byte lineState;
  ushort reserved;
  int xmitCount;
  int xmitFree;
  int recvCount;
  int recvAvail;
  int recvErrorCount;
  int recvLostCount;
} KSUartState;

//------ Common functions ------
Error __stdcall KS_openUart(
                KSHandle* phUart, const char* name, int port, const KSUartProperties* pProperties, int recvBufferLength, int xmitBufferLength, int flags);
Error __stdcall KS_openUartEx(
                KSHandle* phUart, KSHandle hConnection, int port, KSUartProperties* pProperties, int recvBufferLength, int xmitBufferLength, int flags);
Error __stdcall KS_closeUart(
                KSHandle hUart, int flags);

//------ Communication functions ------
Error __stdcall KS_xmitUart(
                KSHandle hUart, int value, int flags);
Error __stdcall KS_recvUart(
                KSHandle hUart, int* pValue, int* pLineError, int flags);
Error __stdcall KS_xmitUartBlock(
                KSHandle hUart, const void* pBytes, int length, int* pLength, int flags);
Error __stdcall KS_recvUartBlock(
                KSHandle hUart, void* pBytes, int length, int* pLength, int flags);
Error __stdcall KS_installUartHandler(
                KSHandle hUart, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_getUartState(
                KSHandle hUart, KSUartState* pUartState, int flags);
Error __stdcall KS_execUartCommand(
                KSHandle hUart, int command, void* pParam, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_UART_MODULE

//--------------------------------------------------------------------------------------------------------------
// COMM Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Line Errors ------
#define KS_COMM_ERROR_PARITY                            0x00000001
#define KS_COMM_ERROR_FRAME                             0x00000002
#define KS_COMM_ERROR_OVERRUN                           0x00000004
#define KS_COMM_ERROR_BREAK                             0x00000008
#define KS_COMM_ERROR_FIFO                              0x00000010

//------ COMM events ------
#define KS_COMM_RECV                                    0x00000000
#define KS_COMM_RECV_ERROR                              0x00000001
#define KS_COMM_XMIT                                    0x00000002
#define KS_COMM_XMIT_EMPTY                              0x00000003
#define KS_COMM_LSR_CHANGE                              0x00000004
#define KS_COMM_MSR_CHANGE                              0x00000005
#define KS_COMM_BREAK                                   0x00000006

//------ Types & Structures ------
typedef struct {
  int ctxType;
  KSHandle hComm;
  int value;
  int lineError;
} CommUserContext;

typedef struct {
  int ctxType;
  KSHandle hComm;
  byte reg;
} CommRegisterUserContext;

typedef struct {
  int baudRate;
  byte parity;
  byte dataBit;
  byte stopBit;
  byte reserved;
} KSCommProperties;

typedef struct {
  int structSize;
  KSCommProperties properties;
  byte modemState;
  byte lineState;
  ushort reserved;
  int xmitCount;
  int xmitFree;
  int recvCount;
  int recvAvail;
  int recvErrorCount;
  int recvLostCount;
} KSCommState;

//------ COMM functions ------
Error __stdcall KS_openComm(
                KSHandle* phComm, const char* name, const KSCommProperties* pProperties, int recvBufferLength, int xmitBufferLength, int flags);
Error __stdcall KS_closeComm(
                KSHandle hComm, int flags);
Error __stdcall KS_xmitComm(
                KSHandle hComm, int value, int flags);
Error __stdcall KS_recvComm(
                KSHandle hComm, int* pValue, int* pLineError, int flags);
Error __stdcall KS_xmitCommBlock(
                KSHandle hComm, const void* pBytes, int length, int* pLength, int flags);
Error __stdcall KS_recvCommBlock(
                KSHandle hComm, void* pBytes, int length, int* pLength, int flags);
Error __stdcall KS_installCommHandler(
                KSHandle hComm, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_getCommState(
                KSHandle hComm, KSCommState* pCommState, int flags);
Error __stdcall KS_execCommCommand(
                KSHandle hComm, int command, void* pParam, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsCommPort
//--------------------------------------------------------------------------------------------------------------

struct KsCommPort : public KsHandle {
  KsCommPort() {
  }
  KsCommPort(const KsString& name, const KSCommProperties& properties, int flags = 0) {
    open(name, properties, 256, 256, flags);
  }
  KsCommPort(const KsString& name, const KSCommProperties& properties, int recvPoolLength, int sendPoolLength,
             int flags = 0) {
    open(name, properties, recvPoolLength, sendPoolLength, flags);
  }
  ~KsCommPort() {
    close();
  }

  void open(const KsString& name, const KSCommProperties& properties, int recvPoolLength, int sendPoolLength,
            int flags = 0) {
    if (Error error = KS_openComm(&hHandle_, name, &properties, recvPoolLength, sendPoolLength, flags))
      throw KsException(error, "KS_openComm");
  }
  void close() {
    if (hHandle_ != KS_INVALID_HANDLE) {
      KS_closeComm(hHandle_, 0);
      hHandle_ = KS_INVALID_HANDLE;
    }
  }

  void getState(KSCommState* pState, int flags = 0) const {
    if (Error error = KS_getCommState(hHandle_, pState, flags))
      throw KsException(error, "KS_getCommState");
  }
  KSCommState state(int flags = 0) const {
    KSCommState state;
    state.structSize = sizeof(KSCommState);
    getState(&state, flags);
    return state;
  }

  void xmit(int value, int flags = 0) {
    if (Error error = KS_xmitComm(hHandle_, value, flags))
      throw KsException(error, "KS_xmitComm");
  }

  int recv(int flags = 0) {
    int value, lineError;
    if (Error error = KS_recvComm(hHandle_, &value, &lineError, flags))
      throw KsException(error, "KS_recvComm");
    if (lineError)
      throw KsException(KSERROR_COMMUNICATION_FAILED | lineError, "KS_recvComm");
    return value;
  }
  bool recv(int* pValue, int* pLineError = null, int flags = 0) {
    switch (Error error = KS_recvComm(hHandle_, pValue, pLineError, flags)) {
      case KSERROR_NO_DATA_AVAILABLE:
        return false;
      default:
        throw KsException(error, "KS_recvComm");
    }
    return true;
  }

  int xmitBlock(const void* pBytes, int length = 0, int flags = 0) {
    int sent = 0;
    if (Error error = KS_xmitCommBlock(hHandle_, pBytes, length, &sent, flags))
      throw KsException(error, "KS_xmitCommBlock");
    return sent;
  }

  int recvBlock(void* pBytes, int length, int flags = 0) {
    int read = 0;
    switch (Error error = KS_recvCommBlock(hHandle_, pBytes, length, &read, flags)) {
      case KSERROR_NO_DATA_AVAILABLE:
        return 0;
      default:
        throw KsException(error, "KS_recvCommBlock");
    }
    return read;
  }

  void execCommand(int command, void* pParam = null, int flags = 0) {
    if (Error error = KS_execCommCommand(hHandle_, command, pParam, flags))
      throw KsException(error, "KS_execCommCommand");
  }
  void setLineControl(const KSCommProperties& properties) {
    execCommand(KS_CHANGE_LINE_CONTROL, const_cast<void*>(static_cast<const void*>(&properties)));
  }
  void setBaudRate(int baudRate) {
    execCommand(KS_SET_BAUD_RATE, &baudRate);
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_COMM_MODULE

//--------------------------------------------------------------------------------------------------------------
// USB Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_USB_REQUEST_FAILED                      (KSERROR_CATEGORY_USB+0x00000000)

//------ Flags ------
#define KSF_SOCKET_MODE                                 0x00080000

//------ USB endpoint commands ------
#define KS_USB_RESET_PORT                               0x00000004
#define KS_USB_SET_ISO_PACKET_SIZE                      0x00000005
#define KS_USB_GET_PACKET_SIZE                          0x00000007
#define KS_USB_SET_PACKET_TIMEOUT                       0x00000008

//------ USB device commands ------
#define KS_USB_GET_DEVICE_DESCRIPTOR                    0x00020000
#define KS_USB_GET_CONFIG_DESCRIPTOR                    0x00030000
#define KS_USB_GET_STRING_DESCRIPTOR                    0x00040000
#define KS_USB_SELECT_CONFIG                            0x00050000
#define KS_USB_SELECT_INTERFACE                         0x00060000
#define KS_USB_GET_CONFIG                               0x00070000
#define KS_USB_GET_INTERFACE                            0x00080000
#define KS_USB_CLR_DEVICE_FEATURE                       0x00090000
#define KS_USB_SET_DEVICE_FEATURE                       0x000a0000
#define KS_USB_CLR_INTERFACE_FEATURE                    0x000b0000
#define KS_USB_SET_INTERFACE_FEATURE                    0x000c0000
#define KS_USB_CLR_ENDPOINT_FEATURE                     0x000d0000
#define KS_USB_SET_ENDPOINT_FEATURE                     0x000e0000
#define KS_USB_GET_STATUS_FROM_DEVICE                   0x000f0000
#define KS_USB_GET_STATUS_FROM_INTERFACE                0x00100000
#define KS_USB_GET_STATUS_FROM_ENDPOINT                 0x00110000
#define KS_USB_VENDOR_DEVICE_REQUEST                    0x00120000
#define KS_USB_VENDOR_INTERFACE_REQUEST                 0x00130000
#define KS_USB_VENDOR_ENDPOINT_REQUEST                  0x00140000
#define KS_USB_CLASS_DEVICE_REQUEST                     0x00150000
#define KS_USB_CLASS_INTERFACE_REQUEST                  0x00160000
#define KS_USB_CLASS_ENDPOINT_REQUEST                   0x00170000

//------ USB descriptor types ------
#define KS_USB_DEVICE                                   0x00000001
#define KS_USB_CONFIGURATION                            0x00000002
#define KS_USB_STRING                                   0x00000003
#define KS_USB_INTERFACE                                0x00000004
#define KS_USB_ENDPOINT                                 0x00000005

//------ USB end point types ------
#define KS_USB_CONTROL                                  0x00000000
#define KS_USB_ISOCHRONOUS                              0x00000001
#define KS_USB_BULK                                     0x00000002
#define KS_USB_INTERRUPT                                0x00000003

//------ USB port events ------
#define KS_USB_SURPRISE_REMOVAL                         (USB_BASE+0x00000000)
#define KS_USB_RECV                                     (USB_BASE+0x00000001)
#define KS_USB_RECV_ERROR                               (USB_BASE+0x00000002)
#define KS_USB_XMIT                                     (USB_BASE+0x00000003)
#define KS_USB_XMIT_EMPTY                               (USB_BASE+0x00000004)
#define KS_USB_XMIT_ERROR                               (USB_BASE+0x00000005)
#define KS_USB_TIMEOUT                                  (USB_BASE+0x00000006)

//------ Types & Structures ------
typedef struct {
  int ctxType;
  KSHandle hUsb;
  int error;
} UsbUserContext;

typedef struct {
  int request;
  int value;
  int index;
  void* pData;
  int size;
  int write;
} UsbClassOrVendorRequest;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  ushort bcdUSB;
  byte bDeviceClass;
  byte bDeviceSubClass;
  byte bDeviceProtocol;
  byte bMaxPacketSize0;
  ushort idVendor;
  ushort idProduct;
  ushort bcdDevice;
  byte iManufacturer;
  byte iProduct;
  byte iSerialNumber;
  byte bNumConfigurations;
} KS_USB_DEVICE_DESCRIPTOR;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  ushort wTotalLength;
  byte bNumInterfaces;
  byte bConfigurationValue;
  byte iConfiguration;
  byte bmAttributes;
  byte MaxPower;
} KS_USB_CONFIGURATION_DESCRIPTOR;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  byte bInterfaceNumber;
  byte bAlternateSetting;
  byte bNumEndpoints;
  byte bInterfaceClass;
  byte bInterfaceSubClass;
  byte bInterfaceProtocol;
  byte iInterface;
} KS_USB_INTERFACE_DESCRIPTOR;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  byte bEndpointAddress;
  byte bmAttributes;
  ushort wMaxPacketSize;
  byte bInterval;
} KS_USB_ENDPOINT_DESCRIPTOR;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  ushort bString[1];
} KS_USB_STRING_DESCRIPTOR;

typedef struct {
  byte bLength;
  byte bDescriptorType;
} KS_USB_COMMON_DESCRIPTOR;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  ushort bcdUSB;
  byte bDeviceClass;
  byte bDeviceSubClass;
  byte bDeviceProtocol;
  byte bMaxPacketSize0;
  byte bNumConfigurations;
  byte bReserved;
} KS_USB_DEVICE_QUALIFIER_DESCRIPTOR;

typedef struct {
  int structSize;
  int xmitCount;
  int xmitFree;
  int xmitErrorCount;
  int recvCount;
  int recvAvail;
  int recvErrorCount;
  int packetSize;
  int isoPacketSize;
  int packetTimeout;
} KSUsbState;

//------ Common functions ------
Error __stdcall KS_openUsb(
                KSHandle* phUsbDevice, const char* deviceName, int flags);
Error __stdcall KS_openUsbEndPoint(
                KSHandle hUsbDevice, KSHandle* phUsbEndPoint, int endPointAddress, int packetSize, int packetCount, int flags);
Error __stdcall KS_closeUsb(
                KSHandle hUsb, int flags);

//------ Communication functions ------
Error __stdcall KS_xmitUsb(
                KSHandle hUsb, void* pData, int length, int* pLength, int flags);
Error __stdcall KS_recvUsb(
                KSHandle hUsb, void* pData, int length, int* pLength, int flags);
Error __stdcall KS_installUsbHandler(
                KSHandle hUsb, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_getUsbState(
                KSHandle hUsb, KSUsbState* pUsbState, int flags);
Error __stdcall KS_execUsbCommand(
                KSHandle hUsb, int command, int index, void* pData, int size, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_USB_MODULE

//--------------------------------------------------------------------------------------------------------------
// XHCI Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_XHCI_REQUEST_FAILED                     (KSERROR_CATEGORY_XHCI+0x00000000)
#define KSERROR_XHCI_HOST_CONTROLLER_ERROR              (KSERROR_CATEGORY_XHCI+0x00010000)
#define KSERROR_XHCI_CONFIGURATION_ERROR                (KSERROR_CATEGORY_XHCI+0x00020000)
#define KSERROR_XHCI_INTERFACE_ERROR                    (KSERROR_CATEGORY_XHCI+0x00030000)
#define KSERROR_XHCI_PACKET_DROPPED                     (KSERROR_CATEGORY_XHCI+0x00040000)
#define KSERROR_XHCI_PACKET_ERROR                       (KSERROR_CATEGORY_XHCI+0x00050000)

//------ XHCI endpoint commands ------
#define KS_XHCI_RESET_ENDPOINT                          0x00000001
#define KS_XHCI_GET_PACKET_SIZE                         0x00000002
#define KS_XHCI_CLEAR_RECEIVE_QUEUE                     0x00000003

//------ XHCI device commands ------
#define KS_XHCI_GET_DEVICE_DESCRIPTOR                   0x00010001
#define KS_XHCI_GET_CONFIG_DESCRIPTOR                   0x00010002
#define KS_XHCI_GET_STRING_DESCRIPTOR                   0x00010003
#define KS_XHCI_SET_CONFIG                              0x00010004
#define KS_XHCI_GET_CONFIG                              0x00010005
#define KS_XHCI_SET_INTERFACE                           0x00010006
#define KS_XHCI_GET_INTERFACE                           0x00010007

//------ XHCI controller commands ------
#define KS_XHCI_SET_POWER_ROOT_HUB                      0x00020001

//------ XHCI descriptor types ------
#define KS_XHCI_DEVICE_DESCRIPTOR                       0x00000001
#define KS_XHCI_CONFIGURATION_DESCRIPTOR                0x00000002
#define KS_XHCI_STRING_DESCRIPTOR                       0x00000003
#define KS_XHCI_INTERFACE_DESCRIPTOR                    0x00000004
#define KS_XHCI_ENDPOINT_DESCRIPTOR                     0x00000005
#define KS_XHCI_INTERFACE_ASSOCIATION_DESCRIPTOR        0x0000000b
#define KS_XHCI_ENDPOINT_COMPANION_DESCRIPTOR           0x00000030

//------ XHCI endpoint types ------
#define KS_XHCI_CONTROL_ENDPOINT                        0x00000000
#define KS_XHCI_ISOCHRONOUS_ENDPOINT                    0x00000001
#define KS_XHCI_BULK_ENDPOINT                           0x00000002
#define KS_XHCI_INTERRUPT_ENDPOINT                      0x00000003

//------ XHCI events ------
#define KS_XHCI_ADD_DEVICE                              (XHCI_BASE+0x00000000)
#define KS_XHCI_REMOVE_DEVICE                           (XHCI_BASE+0x00000001)
#define KS_XHCI_RECV                                    (XHCI_BASE+0x00000002)
#define KS_XHCI_RECV_ERROR                              (XHCI_BASE+0x00000003)
#define KS_XHCI_XMIT_LOW                                (XHCI_BASE+0x00000004)
#define KS_XHCI_XMIT_EMPTY                              (XHCI_BASE+0x00000005)
#define KS_XHCI_XMIT_ERROR                              (XHCI_BASE+0x00000006)

//------ Context structures ------
typedef struct {
  int ctxType;
  KSHandle hXhci;
} KSXhciContext;

typedef struct {
  int ctxType;
  KSHandle hXhciController;
  char deviceName[256];
} KSXhciAddDeviceContext;

typedef struct {
  int ctxType;
  KSHandle hXhciEndPoint;
  void* pXhciPacketApp;
  void* pXhciPacketSys;
  int packetSize;
} KSXhciRecvContext;

typedef struct {
  int ctxType;
  KSHandle hXhci;
  int error;
} KSXhciErrorContext;

//------ XHCI common structures ------
typedef struct {
  int requestType;
  int request;
  int value;
  int index;
} KSXhciControlRequest;

typedef struct {
  int structSize;
  int endPointType;
  int packetSize;
  int xmitCount;
  int xmitFree;
  int xmitErrorCount;
  int recvCount;
  int recvAvail;
  int recvErrorCount;
  int recvDroppedCount;
} KSXhciEndPointState;

//------ XHCI Descriptors ------
typedef struct {
  byte bLength;
  byte bDescriptorType;
} KSXhciCommonDescriptor;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  ushort bcdUSB;
  byte bDeviceClass;
  byte bDeviceSubClass;
  byte bDeviceProtocol;
  byte bMaxPacketSize0;
  ushort idVendor;
  ushort idProduct;
  ushort bcdDevice;
  byte iManufacturer;
  byte iProduct;
  byte iSerialNumber;
  byte bNumConfigurations;
} KSXhciDeviceDescriptor;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  ushort wTotalLength;
  byte bNumInterfaces;
  byte bConfigurationValue;
  byte iConfiguration;
  byte bmAttributes;
  byte MaxPower;
} KSXhciConfigurationDescriptor;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  byte bInterfaceNumber;
  byte bAlternateSetting;
  byte bNumEndpoints;
  byte bInterfaceClass;
  byte bInterfaceSubClass;
  byte bInterfaceProtocol;
  byte iInterface;
} KSXhciInterfaceDescriptor;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  byte bFirstInterface;
  byte bNumInterfaces;
  byte bFunctionClass;
  byte bFunctionSubClass;
  byte bFunctionProtocol;
  byte iFunction;
} KSXhciInterfaceAssociationDescriptor;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  byte bEndpointAddress;
  byte bmAttributes;
  ushort wMaxPacketSize;
  byte bInterval;
} KSXhciEndpointDescriptor;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  byte bMaxBurst;
  byte bmAttributes;
  ushort wBytesPerInterval;
} KSXhciEndpointCompanionDescriptor;

typedef struct {
  byte bLength;
  byte bDescriptorType;
  ushort bString[1];
} KSXhciStringDescriptor;

//------ Common functions ------
Error __stdcall KS_openXhciController(
                KSHandle* phXhciController, const char* deviceName, int flags);
Error __stdcall KS_openXhciDevice(
                KSHandle* phXhciDevice, const char* deviceName, int flags);
Error __stdcall KS_openXhciEndPoint(
                KSHandle hXhciDevice, KSHandle* phXhciEndPoint, int endPointAddress, int packetSize, int packetCount, int flags);
Error __stdcall KS_closeXhci(
                KSHandle hXhci, int flags);

//------ Communication functions ------
Error __stdcall KS_requestXhciPacket(
                KSHandle hXhciEndPoint, void** ppXhciPacket, int flags);
Error __stdcall KS_releaseXhciPacket(
                KSHandle hXhciEndPoint, void* pXhciPacket, int flags);
Error __stdcall KS_xmitXhciPacket(
                KSHandle hXhciEndPoint, void* pXhciPacket, int size, int flags);
Error __stdcall KS_recvXhciPacket(
                KSHandle hXhciEndPoint, void** ppXhciPacket, int* pSize, int flags);
Error __stdcall KS_sendXhciControlRequest(
                KSHandle hXhciDevice, const KSXhciControlRequest* pRequestData, void* pData, int length, int* pLength, int flags);
Error __stdcall KS_installXhciHandler(
                KSHandle hXhci, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_getXhciEndPointState(
                KSHandle hXhciEndPoint, KSXhciEndPointState* pState, int flags);
Error __stdcall KS_execXhciCommand(
                KSHandle hXhci, int command, int index, void* pParam, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsXhciController
//--------------------------------------------------------------------------------------------------------------

struct KsXhciController : public KsHandle {
  KsXhciController() {}
  KsXhciController(const KsString& name, int flags = 0) {
    open(name, flags);
  }

  void open(const KsString& name, int flags = 0) {
    if (Error error = KS_openXhciController(&hHandle_, name, flags))
      throw KsException(error, "KS_openXhci");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsXhciDevice
//--------------------------------------------------------------------------------------------------------------

struct KsXhciDevice : public KsHandle {
  KsXhciDevice() {}
  KsXhciDevice(const KsString& name, int flags = 0) {
    open(name, flags);
  }

  void open(const KsString& name, int flags = 0) {
    if (Error error = KS_openXhciDevice(&hHandle_, name, flags))
      throw KsException(error, "KS_openXhciDevice");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsXhciEndPoint
//--------------------------------------------------------------------------------------------------------------

struct KsXhciEndPoint : public KsHandle {
  KsXhciEndPoint() {}
  KsXhciEndPoint(const KsXhciDevice& device, int endPointAddress, int flags = 0) {
    open(device, endPointAddress, 0, 256, flags);
  }
  KsXhciEndPoint(const KsXhciDevice& device, int endPointAddress, int packetSize, int packetCount, int flags = 0) {
    open(device, endPointAddress, packetSize, packetCount, flags);
  }

  void open(const KsXhciDevice& device, int endPointAddress, int packetSize, int packetCount, int flags = 0) {
    if (Error error = KS_openXhciEndPoint(device, &hHandle_, endPointAddress, packetSize, packetCount, flags))
      throw KsException(error, "KS_openXhciEndPoint");
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_XHCI_MODULE

//--------------------------------------------------------------------------------------------------------------
// RealTime Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_TIMER_NOT_INSTALLED                     (KSERROR_CATEGORY_TIMER+0x00000000)
#define KSERROR_TIMER_REMOVED                           (KSERROR_CATEGORY_TIMER+0x00010000)
#define KSERROR_TIMER_DISABLED                          (KSERROR_CATEGORY_TIMER+0x00020000)
#define KSERROR_TIMER_NOT_DISABLED                      (KSERROR_CATEGORY_TIMER+0x00030000)
#define KSERROR_CANNOT_INSTALL_TIMER                    (KSERROR_CATEGORY_TIMER+0x00040000)
#define KSERROR_NO_TIMER_AVAILABLE                      (KSERROR_CATEGORY_TIMER+0x00050000)
#define KSERROR_WAIT_CANCELLED                          (KSERROR_CATEGORY_TIMER+0x00060000)
#define KSERROR_TIMER_ALREADY_RUNNING                   (KSERROR_CATEGORY_TIMER+0x00070000)
#define KSERROR_TIMER_ALREADY_STOPPED                   (KSERROR_CATEGORY_TIMER+0x00080000)
#define KSERROR_CANNOT_START_TIMER                      (KSERROR_CATEGORY_TIMER+0x00090000)

//------ Flags ------
#define KSF_DISTINCT_MODE                               0x00040000
#define KSF_DRIFT_CORRECTION                            0x00010000
#define KSF_DISABLE_PROTECTION                          0x00020000
#define KSF_USE_IEEE1588_TIME                           0x00040000

//------ Timer module config ------
#define KSCONFIG_SOFTWARE_TIMER                         0x00000001

//------ Timer events ------
#define KS_TIMER_CONTEXT                                0x00000200

//------ Context structures ------
typedef struct {
  int ctxType;
  KSHandle hTimer;
  int delay;
} TimerUserContext;

//------ Real-time functions ------
Error __stdcall KS_createTimer(
                KSHandle* phTimer, int delay, KSHandle hSignal, int flags);
Error __stdcall KS_removeTimer(
                KSHandle hTimer);
Error __stdcall KS_cancelTimer(
                KSHandle hTimer);
Error __stdcall KS_startTimer(
                KSHandle hTimer, int flags, int delay);
Error __stdcall KS_stopTimer(
                KSHandle hTimer);
Error __stdcall KS_startTimerDelayed(
                KSHandle hTimer, int64ref start, int period, int flags);
Error __stdcall KS_adjustTimer(
                KSHandle hTimer, int period, int flags);

//------ Obsolete timer functions - do not use! ------
Error __stdcall KS_disableTimer(
                KSHandle hTimer);
Error __stdcall KS_enableTimer(
                KSHandle hTimer);
Error __stdcall KS_getTimerState(
                KSHandle hTimer, HandlerState* pState);

//------ Obsolete timer resolution functions - do not use! ------
Error __stdcall KS_setTimerResolution(
                uint resolution, int flags);
Error __stdcall KS_getTimerResolution(
                uint* pResolution, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsTimer
//--------------------------------------------------------------------------------------------------------------

struct KsTimer : public KsHandle {
public:
  KsTimer(int delay, const KsSignal& signal, int flags = 0) {
    if (Error error = KS_createTimer(&hHandle_, delay, signal, flags))
      throw KsException(error, "KS_createTimer");
  }

  void start(int delay = 0, int flags = 0) {
    if (Error error = KS_startTimer(hHandle_, flags, delay))
      throw KsException(error, "KS_startTimer");
  }

  void stop() {
    if (Error error = KS_stopTimer(hHandle_))
      throw KsException(error, "KS_stopTimer");
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_REALTIME_MODULE

//--------------------------------------------------------------------------------------------------------------
// Task Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_CPU_BOOT_FAILURE                        (KSERROR_CATEGORY_TASK+0x00000000)

//------ Task states ------
#define KS_TASK_DORMANT                                 0x00000001
#define KS_TASK_READY                                   0x00000002
#define KS_TASK_RUNNING                                 0x00000004
#define KS_TASK_WAITING                                 0x00000008
#define KS_TASK_SUSPENDED                               0x00000010
#define KS_TASK_TERMINATED                              0x00000020

//------ Flags ------
#define KSF_NUM_PROCESSORS                              0x00000040
#define KSF_NON_INTERRUPTABLE                           0x00040000
#define KSF_CUSTOM_STACK_SIZE                           0x00004000

//------ Task contexts ------
#define TASK_CONTEXT                                    (TASK_BASE+0x00000000)

//------ Common structure ------
typedef struct {
  int structSize;
  int currentLevel;
  KSHandle taskHandle;
  int taskPriority;
  int currentCpuNumber;
  int stackSize;
  int stackUsage;
} KSContextInformation;

//------ Context structure ------
typedef struct {
  int ctxType;
  KSHandle hTask;
  int priority;
} TaskUserContext;

typedef struct {
  int ctxType;
  Error error;
  void* param1;
  void* param2;
  void* param3;
  void* param4;
  KSHandle hTask;
  char kernel[64];
  char functionName[64];
  int functionOffset;
} TaskErrorUserContext;

//------ Task functions ------
Error __stdcall KS_createTask(
                KSHandle* phTask, KSHandle hCallBack, int priority, int flags);
Error __stdcall KS_removeTask(
                KSHandle hTask);
Error __stdcall KS_exitTask(
                KSHandle hTask, int exitCode);
Error __stdcall KS_killTask(
                KSHandle hTask, int exitCode);
Error __stdcall KS_terminateTask(
                KSHandle hTask, int timeout, int flags);
Error __stdcall KS_suspendTask(
                KSHandle hTask);
Error __stdcall KS_resumeTask(
                KSHandle hTask);
Error __stdcall KS_triggerTask(
                KSHandle hTask);
Error __stdcall KS_yieldTask(
                void);
Error __stdcall KS_sleepTask(
                int delay);
Error __stdcall KS_setTaskPrio(
                KSHandle hTask, int newPrio, int* pOldPrio);
Error __stdcall KS_setTaskPriority(
                KSHandle hObject, int priority, int flags);
Error __stdcall KS_setTaskPriorityRelative(
                KSHandle hObject, KSHandle hRelative, int distance, int flags);
Error __stdcall KS_getTaskPriority(
                KSHandle hObject, int* pPriority, int flags);
Error __stdcall KS_getTaskState(
                KSHandle hTask, uint* pTaskState, Error* pExitCode);
Error __stdcall KS_setTaskStackSize(
                int size, int flags);

//------ MultiCore & information functions ------
Error __stdcall KS_setTargetProcessor(
                int processor, int flags);
Error __stdcall KS_getCurrentProcessor(
                int* pProcessor, int flags);
Error __stdcall KS_getCurrentContext(
                KSContextInformation* pContextInfo, int flags);

//------ Semaphore functions ------
Error __stdcall KS_createSemaphore(
                KSHandle* phSemaphore, int maxCount, int initCount, int flags);
Error __stdcall KS_removeSemaphore(
                KSHandle hSemaphore);
Error __stdcall KS_requestSemaphore(
                KSHandle hSemaphore, int timeout);
Error __stdcall KS_releaseSemaphore(
                KSHandle hSemaphore);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsTask
//--------------------------------------------------------------------------------------------------------------

struct KsTask : public KsSignal {
public:
  KsTask(const KsCallBack& callBack, int prio, int flags = 0) {
    if (Error error = KS_createTask(&hHandle_, callBack, prio, flags))
      throw KsException(error, "KS_createTask");
  }

  void trigger() {
    if (Error error = KS_triggerTask(hHandle_))
      throw KsException(error, "KS_triggerTask");
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_TASK_MODULE

//--------------------------------------------------------------------------------------------------------------
// Dedicated Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Dedicated functions ------

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_DEDICATED_MODULE

//--------------------------------------------------------------------------------------------------------------
// Packet Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

#ifdef __cplusplus

#ifndef KS_ENABLE_NAMESPACE
} // extern "C"
#endif

#ifndef __KDEFS_H
inline byte lo(ushort val)                      { return byte(val); }
inline byte hi(ushort val)                      { return byte(val >> 8); }

inline ushort lo(uint val)                      { return ushort(val); }
inline ushort hi(uint val)                      { return ushort(val >> 16); }

inline ushort mk16(byte lo, byte hi = 0)        { return ushort(lo | (ushort(hi) << 8)); }
inline uint mk32(ushort lo, ushort hi = 0)      { return uint(lo | (uint(hi) << 16)); }
#endif // __KDEFS_H

inline ushort KS_htons(ushort val)              { return mk16(hi(val), lo(val)); }
inline uint KS_htonl(uint val)                  { return mk32(KS_htons(hi(val)), KS_htons(lo(val))); }

inline ushort KS_ntohs(ushort val)              { return KS_htons(val); }
inline uint KS_ntohl(uint val)                  { return KS_htonl(val); }

inline void KS_makeIPv4(uint* pAddr, byte b3, byte b2, byte b1, byte b0)
                                                { *pAddr = (b0 << 24) | (b1 << 16) | (b2 << 8) | b3; }
#ifndef KS_ENABLE_NAMESPACE
extern "C" {
#endif

#else

#define lo16(val)                               byte(val)
#define hi16(val)                               byte((val) >> 8)

#define lo32(val)                               ushort(va)
#define hi32(val)                               ushort((val) >> 16)

#define mk16(lo, hi)                            ushort((lo) | (ushort(hi) << 8))
#define mk32(lo, hi)                            uint((lo) | (uint(hi) << 16))

#define KS_htons(val)                           mk16(hi16(val), lo16(val))
#define KS_htonl(val)                           mk32(KS_htons(hi32(val)), KS_htons(lo32(val)))

#define KS_ntohs(val)                           KS_htons(val)
#define KS_ntohl(val)                           KS_htonl(val)

#define KS_makeIPv4(pAddr, b3, b2, b1, b0)      ( *pAddr = (b0 << 24) | (b1 << 16) | (b2 << 8) | b3 )

#endif

//------ Error Codes ------
#define KSERROR_INIT_PHY_ERROR                          (KSERROR_CATEGORY_PACKET+0x00000000)
#define KSERROR_INIT_MAC_ERROR                          (KSERROR_CATEGORY_PACKET+0x00010000)
#define KSERROR_MAC_STARTUP_ERROR                       (KSERROR_CATEGORY_PACKET+0x00020000)

//------ Flags ------
#define KSF_SUPPORT_JUMBO_FRAMES                        0x00008000
#define KSF_DELAY_RECEIVE_INTERRUPT                     0x00080000
#define KSF_NO_RECEIVE_INTERRUPT                        0x00000020
#define KSF_NO_TRANSMIT_INTERRUPT                       0x00000040
#define KSF_RAW_ETHERNET_PACKET                         0x00100000
#define KSF_SAFE_START                                  0x00200000
#define KSF_CHECK_IP_CHECKSUM                           0x00400000
#define KSF_ACCEPT_ALL                                  0x00800000
#define KSF_FORCE_MASTER                                0x00000001
#define KSF_FORCE_100_MBIT                              0x00000002
#define KSF_WINDOWS_CONNECTION                          0x00004000

//------ Packet events ------
#define KS_PACKET_RECV                                  (PACKET_BASE+0x00000000)
#define KS_PACKET_LINK_CHANGE                           (PACKET_BASE+0x00000001)
#define KS_PACKET_SEND_EMPTY                            (PACKET_BASE+0x00000002)
#define KS_PACKET_SEND_LOW                              (PACKET_BASE+0x00000003)

//------ Adapter commands ------
#define KS_PACKET_ABORT_SEND                            0x00000001
#define KS_PACKET_ADD_ARP_TABLE_ENTRY                   0x00000002
#define KS_PACKET_SET_IP_CONFIG                         0x00000003
#define KS_PACKET_SET_MAC_MULTICAST                     0x00000004
#define KS_PACKET_SET_IP_MULTICAST                      0x00000005
#define KS_PACKET_SET_MAC_CONFIG                        0x00000006
#define KS_PACKET_CLEAR_RECEIVE_QUEUE                   0x00000007
#define KS_PACKET_GET_IP_CONFIG                         0x00000008
#define KS_PACKET_GET_MAC_CONFIG                        0x00000009
#define KS_PACKET_GET_MAXIMUM_SIZE                      0x0000000a

//------ Connection flags ------
#define KS_PACKET_SPEED_10_MBIT                         0x00010000
#define KS_PACKET_SPEED_100_MBIT                        0x00020000
#define KS_PACKET_SPEED_1000_MBIT                       0x00040000
#define KS_PACKET_SPEED_10_GBIT                         0x00080000
#define KS_PACKET_DUPLEX_FULL                           0x00000001
#define KS_PACKET_DUPLEX_HALF                           0x00000002
#define KS_PACKET_MASTER                                0x00000004
#define KS_PACKET_SLAVE                                 0x00000008

//------ Types & Structures ------
typedef struct {
  int ctxType;
  KSHandle hAdapter;
  void* pPacketApp;
  void* pPacketSys;
  int length;
} PacketUserContext;

typedef struct {
  int ctxType;
  KSHandle hAdapter;
} PacketAdapterUserContext;

typedef struct {
  int ctxType;
  KSHandle hAdapter;
  uint connection;
} PacketLinkChangeUserContext;

typedef struct {
  byte destination[6];
  byte source[6];
  ushort typeOrLength;
} KSMACHeader;

typedef struct {
  uint header1;
  uint header2;
  uint header3;
  uint srcIpAddress;
  uint dstIpAddress;
  uint header6;
} KSIPHeader;

typedef struct {
  ushort srcPort;
  ushort dstPort;
  ushort msgLength;
  ushort checkSum;
} KSUDPHeader;

typedef struct {
  uint localAddress;
  uint subnetMask;
  uint gatewayAddress;
} KSIPConfig;

typedef struct {
  uint ipAddress;
  byte macAddress[6];
} KSARPTableEntry;

typedef struct {
  int structSize;
  int numPacketsSendOk;
  int numPacketsSendError;
  int numPacketsSendARPTimeout;
  int numPacketsWaitingForARP;
  int numPacketsRecvOk;
  int numPacketsRecvError;
  int numPacketsRecvDropped;
  uint connection;
  byte macAddress[6];
  byte connected;
} KSAdapterState;

//------ Helper functions ------

//------ Common functions ------
Error __stdcall KS_openAdapter(
                KSHandle* phAdapter, const char* name, int recvPoolLength, int sendPoolLength, int flags);
Error __stdcall KS_openAdapterEx(
                KSHandle* phAdapter, KSHandle hConnection, int recvPoolLength, int sendPoolLength, int flags);
Error __stdcall KS_closeAdapter(
                KSHandle hAdapter);
Error __stdcall KS_getAdapterState(
                KSHandle hAdapter, KSAdapterState* pAdapterState, int flags);
Error __stdcall KS_execAdapterCommand(
                KSHandle hAdapter, int command, void* pParam, int flags);
Error __stdcall KS_aggregateAdapter(
                KSHandle hAdapter, KSHandle hLink, int flags);

//------ Communication functions ------
Error __stdcall KS_requestPacket(
                KSHandle hAdapter, void** ppPacket, int flags);
Error __stdcall KS_releasePacket(
                KSHandle hAdapter, void* pPacket, int flags);
Error __stdcall KS_sendPacket(
                KSHandle hAdapter, void* pPacket, uint size, int flags);
Error __stdcall KS_recvPacket(
                KSHandle hAdapter, void** ppPacket, int flags);
Error __stdcall KS_recvPacketEx(
                KSHandle hAdapter, void** ppPacket, int* pLength, int flags);
Error __stdcall KS_installPacketHandler(
                KSHandle hAdapter, int eventCode, KSHandle hSignal, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsNicAdapter
//--------------------------------------------------------------------------------------------------------------

struct KsNicAdapter : public KsHandle {
  KsNicAdapter() {
  }
  KsNicAdapter(const KsString& name, int flags = 0) {
    open(name, 256, 256, flags);
  }
  KsNicAdapter(const KsString& name, int recvPoolLength, int sendPoolLength, int flags = 0) {
    open(name, recvPoolLength, sendPoolLength, flags);
  }
  KsNicAdapter(const KsNicAdapter& nicAdapter, int flags = 0) {
    openEx(nicAdapter, 256, 256, flags);
  }
  KsNicAdapter(const KsNicAdapter& nicAdapter, int recvPoolLength, int sendPoolLength, int flags = 0) {
    openEx(nicAdapter, recvPoolLength, sendPoolLength, flags);
  }

  void open(const KsString& name, int recvPoolLength, int sendPoolLength, int flags = 0) {
    if (Error error = KS_openAdapter(&hHandle_, name, recvPoolLength, sendPoolLength, flags))
      throw KsException(error, "KS_openAdapter");
  }
  void openEx(const KsNicAdapter& nicAdapter, int recvPoolLength, int sendPoolLength, int flags = 0) {
    if (Error error = KS_openAdapterEx(&hHandle_, nicAdapter, recvPoolLength, sendPoolLength, flags))
      throw KsException(error, "KS_openAdapterEx");
  }

  void execCommand(int command, void* pParam, int flags = 0) const {
    if (Error error = KS_execAdapterCommand(hHandle_, command, pParam, flags))
      throw KsException(error, "KS_execAdapterCommand");
  }
  void execCommand(int command, void* pParam, int flags = 0) {
    const_cast<const KsNicAdapter*>(this)->execCommand(command, pParam, flags);
  }

  KSIPConfig ipConfig() const {
    KSIPConfig ipConfig;
    execCommand(KS_PACKET_GET_IP_CONFIG, &ipConfig);
    return ipConfig;
  }
  void setIpConfig(const KSIPConfig& ipConfig) {
    execCommand(KS_PACKET_SET_IP_CONFIG, const_cast<KSIPConfig*>(&ipConfig));
  }

  void getState(KSAdapterState* pState, int flags = 0) const {
    if (Error error = KS_getAdapterState(hHandle_, pState, flags))
      throw KsException(error, "KS_getAdapterState");
  }
  KSAdapterState state() const {
    KSAdapterState state;
    state.structSize = sizeof(KSAdapterState);
    getState(&state);
    return state;
  }

  void requestPacket(void** pPacket) {
    if (Error error = KS_requestPacket(hHandle_, pPacket, 0))
      throw KsException(error, "KS_requestPacket");
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_PACKET_MODULE

//--------------------------------------------------------------------------------------------------------------
// Socket Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Flags ------
#define KSF_CLIENT                                      0x00000020
#define KSF_SERVER                                      0x00000040
#define KSF_ACCEPT_BROADCAST                            0x00800000
#define KSF_INSERT_CHECKSUM                             0x00200000
#define KSF_CHECK_CHECKSUM                              0x00400000

//------ Family types ------
#define KS_FAMILY_IPV4                                  0x00000004

//------ Protocol types ------
#define KS_PROTOCOL_RAW                                 0x00000000
#define KS_PROTOCOL_UDP                                 0x00000001
#define KS_PROTOCOL_TCP                                 0x00000002

//------ Socket events ------
#define KS_SOCKET_CONNECTED                             (SOCKET_BASE+0x00000000)
#define KS_SOCKET_DISCONNECTED                          (SOCKET_BASE+0x00000001)
#define KS_SOCKET_RECV                                  (SOCKET_BASE+0x00000002)
#define KS_SOCKET_CLEAR_TO_SEND                         (SOCKET_BASE+0x00000003)
#define KS_SOCKET_SEND_EMPTY                            (SOCKET_BASE+0x00000004)
#define KS_SOCKET_SEND_LOW                              (SOCKET_BASE+0x00000005)

//------ Socket commands ------
#define KS_SOCKET_SET_MTU                               0x00000001

//------ Types & Structures ------
typedef struct {
  ushort family;
  ushort port;
  uint address[1];
} KSSocketAddr;

typedef struct {
  int ctxType;
  KSHandle hSocket;
} SocketUserContext;

//------ Socket functions ------
Error __stdcall KS_openSocket(
                KSHandle* phSocket, KSHandle hAdapter, const KSSocketAddr* pSocketAddr, int protocol, int flags);
Error __stdcall KS_closeSocket(
                KSHandle hSocket);
Error __stdcall KS_recvFromSocket(
                KSHandle hSocket, KSSocketAddr* pSourceAddr, void* pBuffer, int length, int* pLength, int flags);
Error __stdcall KS_sendToSocket(
                KSHandle hSocket, const KSSocketAddr* pTargetAddr, const void* pBuffer, int length, int* pLength, int flags);
Error __stdcall KS_recvSocket(
                KSHandle hSocket, void* pBuffer, int length, int* pLength, int flags);
Error __stdcall KS_sendSocket(
                KSHandle hSocket, const void* pBuffer, int length, int* pLength, int flags);
Error __stdcall KS_installSocketHandler(
                KSHandle hSocket, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_connectSocket(
                KSHandle hClient, const KSSocketAddr* pServerAddr, int flags);
Error __stdcall KS_acceptSocket(
                KSHandle hServer, KSSocketAddr* pClientAddr, int flags);
Error __stdcall KS_execSocketCommand(
                KSHandle hSocket, int command, void* pParam, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsSocket
//--------------------------------------------------------------------------------------------------------------

struct KsSocket : public KsHandle {
public:
  KsSocket(const KsNicAdapter& adapter, ushort port, uint address, int protocol, int flags = 0) {
    KSSocketAddr socketAddr = { KS_FAMILY_IPV4, port, { address } };
    if (Error error = KS_openSocket(&hHandle_, adapter, &socketAddr, protocol, flags))
      throw KsException(error, "KS_openSocket");
  }

  void installHandler(int eventCode, const KsSignal& signal) {
    if (Error error = KS_installSocketHandler(hHandle_, eventCode, signal, 0))
      throw KsException(error, "KS_installSocketHandler");
  }
  void uninstallHandler(int eventCode, int flags = 0) {
    if (Error error = KS_installSocketHandler(hHandle_, eventCode, KS_INVALID_HANDLE, flags))
      throw KsException(error, "KS_installSocketHandler");
  }

  void connect(ushort port, uint address) {
    KSSocketAddr socketAddr = { KS_FAMILY_IPV4, port, { address } };
    if (Error error = KS_connectSocket(hHandle_, &socketAddr, 0))
      throw KsException(error, "KS_connectSocket");
  }

  KSSocketAddr accept(int flags = 0) {
    KSSocketAddr socketAddr;
    if (Error error = KS_acceptSocket(hHandle_, &socketAddr, flags))
      throw KsException(error, "KS_acceptSocket");
    return socketAddr;
  }

  void send(ushort port, uint address, const void* pBuffer, int length, int* pLength = null, int flags = 0) {
    KSSocketAddr socketAddr = { KS_FAMILY_IPV4, port, { address } };
    if (Error error = KS_sendToSocket(hHandle_, &socketAddr, pBuffer, length, pLength, flags))
      throw KsException(error, "KS_sendToSocket");
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_SOCKET_MODULE

//--------------------------------------------------------------------------------------------------------------
// EtherCAT Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_EMERGENCY_REQUEST                       (KSERROR_CATEGORY_ETHERCAT+0x00000000)
#define KSERROR_LINK_CHANGE                             (KSERROR_CATEGORY_ETHERCAT+0x00010000)
#define KSERROR_TOPOLOGY_CHANGE                         (KSERROR_CATEGORY_ETHERCAT+0x00020000)
#define KSERROR_SLAVE_ERROR                             (KSERROR_CATEGORY_ETHERCAT+0x00030000)
#define KSERROR_DATA_INCOMPLETE                         (KSERROR_CATEGORY_ETHERCAT+0x00040000)
#define KSERROR_PHYSICAL_ACCESS_FAILED                  (KSERROR_CATEGORY_ETHERCAT+0x00050000)
#define KSERROR_FMMU_ERROR                              (KSERROR_CATEGORY_ETHERCAT+0x00060000)
#define KSERROR_DATA_LINK_ERROR                         (KSERROR_CATEGORY_ETHERCAT+0x00070000)
#define KSERROR_SII_ERROR                               (KSERROR_CATEGORY_ETHERCAT+0x00080000)
#define KSERROR_MAILBOX_ERROR                           (KSERROR_CATEGORY_ETHERCAT+0x00090000)
#define KSERROR_APPLICATION_LAYER_ERROR                 (KSERROR_CATEGORY_ETHERCAT+0x000a0000)
#define KSERROR_SYNC_MANAGER_ERROR                      (KSERROR_CATEGORY_ETHERCAT+0x000b0000)
#define KSERROR_DC_CONTROL_ERROR                        (KSERROR_CATEGORY_ETHERCAT+0x000c0000)
#define KSERROR_SOE_REQUEST_ERROR                       (KSERROR_CATEGORY_ETHERCAT+0x000e0000)
#define KSERROR_MISSING_XML_INFORMATION                 (KSERROR_CATEGORY_ETHERCAT+0x000f0000)
#define KSERROR_SAFETY_ERROR                            (KSERROR_CATEGORY_ETHERCAT+0x00100000)
#define KSERROR_DC_CONFIGURATION_ERROR                  (KSERROR_CATEGORY_ETHERCAT+0x00110000)
#define KSERROR_COE_ERROR                               (KSERROR_CATEGORY_ETHERCAT+0x00120000)

//------ Flags ------
#define KSF_FORCE_UPDATE                                0x00000008
#define KSF_SECONDARY                                   0x00000020
#define KSF_SYNC_DC_GLOBAL                              0x00000040
#define KSF_SYNC_DC_LOCAL                               0x00800000
#define KSF_BIG_DATASET                                 0x00004000
#define KSF_STARTUP_WITHOUT_INIT_STATE                  0x00008000
#define KSF_COMPLETE_ACCESS                             0x00200000
#define KSF_IGNORE_INIT_CMDS                            0x01000000
#define KSF_STRICT_XML_CHECK                            0x00000040

//------ DataObj type flags ------
#define KS_DATAOBJ_PDO_TYPE                             0x00000001
#define KS_DATAOBJ_SDO_TYPE                             0x00000002
#define KS_DATAOBJ_IDN_TYPE                             0x00000003
#define KS_DATAOBJ_ACTIVE                               0x00040000
#define KS_DATAOBJ_MAPPABLE                             0x00080000
#define KS_DATAOBJ_SAFETY                               0x00100000

//------ State type ------
#define KS_ECAT_STATE_INIT                              0x00000001
#define KS_ECAT_STATE_PREOP                             0x00000002
#define KS_ECAT_STATE_BOOT                              0x00000003
#define KS_ECAT_STATE_SAFEOP                            0x00000004
#define KS_ECAT_STATE_OP                                0x00000008

//------ State type (legacy, do not use anymore) ------
#define KS_STATE_INIT                                   0x00000001
#define KS_STATE_PREOP                                  0x00000002
#define KS_STATE_BOOT                                   0x00000003
#define KS_STATE_SAFEOP                                 0x00000004
#define KS_STATE_OP                                     0x00000008

//------ Sync manager index commons ------
#define KS_ECAT_SYNC_ALL                                0xffffffff
#define KS_ECAT_SYNC_INPUT                              0xfffffffe
#define KS_ECAT_SYNC_OUTPUT                             0xfffffffd

//------ EtherCAT events (legacy, do not use anymore) ------
#define KS_ETHERCAT_ERROR                               (ETHERCAT_BASE+0x00000000)
#define KS_DATASET_SIGNAL                               (ETHERCAT_BASE+0x00000001)
#define KS_TOPOLOGY_CHANGE                              (ETHERCAT_BASE+0x00000002)

//------ EtherCAT events ------
#define KS_ECAT_ERROR                                   (ETHERCAT_BASE+0x00000000)
#define KS_ECAT_DATASET_SIGNAL                          (ETHERCAT_BASE+0x00000001)
#define KS_ECAT_TOPOLOGY_CHANGE                         (ETHERCAT_BASE+0x00000002)
#define KS_ECAT_STATE_CHANGE                            (ETHERCAT_BASE+0x00000003)
#define KS_ECAT_OBJECT_ACCESS                           (ETHERCAT_BASE+0x00000004)
#define KS_ECAT_FILE_ACCESS                             (ETHERCAT_BASE+0x00000005)
#define KS_ECAT_PROCESS_DATA_ACCESS                     (ETHERCAT_BASE+0x00000006)
#define KS_ECAT_REDUNDANCY_EFFECTIVE                    (ETHERCAT_BASE+0x00000007)

//------ Topology change reasons ------
#define KS_ECAT_TOPOLOGY_MASTER_CONNECTED               0x00000000
#define KS_ECAT_TOPOLOGY_MASTER_DISCONNECTED            0x00000001
#define KS_ECAT_TOPOLOGY_SLAVE_COUNT_CHANGED            0x00000002
#define KS_ECAT_TOPOLOGY_SLAVE_ONLINE                   0x00000003
#define KS_ECAT_TOPOLOGY_SLAVE_OFFLINE                  0x00000004

//------ DataSet commands ------
#define KS_ECAT_GET_BUFFER_SIZE                         0x00000001

//------ Slave commands ------
#define KS_ECAT_READ_SYNC0_STATUS                       0x00000001
#define KS_ECAT_READ_SYNC1_STATUS                       0x00000002
#define KS_ECAT_READ_LATCH0_STATUS                      0x00000003
#define KS_ECAT_READ_LATCH1_STATUS                      0x00000004
#define KS_ECAT_READ_LATCH_TIMESTAMPS                   0x00000005
#define KS_ECAT_ENABLE_CYCLIC_MAILBOX_CHECK             0x00000006
#define KS_ECAT_GET_PDO_OVERSAMPLING_FACTOR             0x00000007
#define KS_ECAT_SET_SYNCMANAGER_WATCHDOGS               0x00000008
#define KS_ECAT_GET_SYNCMANAGER_WATCHDOGS               0x00000009
#define KS_ECAT_HAS_XML_INFORMATION                     0x0000000a

//------ Slave Device / EAP-Node commands ------
#define KS_ECAT_SIGNAL_AL_ERROR                         0x00000001

//------ EtherCAT data structures ------
typedef struct {
  int structSize;
  int connected;
  int slavesOnline;
  int slavesCreated;
  int slavesCreatedAndOnline;
  int masterState;
  int lastError;
  int redundancyEffective;
  int framesSent;
  int framesLost;
} KSEcatMasterState;

typedef struct {
  int structSize;
  int online;
  int created;
  int vendor;
  int product;
  int revision;
  int id;
  int relPosition;
  int absPosition;
  int linkState;
  int slaveState;
  int statusCode;
} KSEcatSlaveState;

typedef struct {
  int objType;
  const char* name;
  int dataType;
  int bitLength;
  int subIndex;
} KSEcatDataVarInfo;

typedef struct {
  int objType;
  const char* name;
  int bitLength;
  int index;
  int syncIndex;
  int varCount;
  KSEcatDataVarInfo* vars[1];
} KSEcatDataObjInfo;

typedef struct {
  int vendorId;
  int productId;
  int revision;
  int serial;
  const char* group;
  const char* image;
  const char* order;
  const char* name;
  int objCount;
  KSEcatDataObjInfo* objs[1];
} KSEcatSlaveInfo;

typedef struct {
  ushort assignActivate;
  byte latch0Control;
  byte latch1Control;
  int pulseLength;
  int cycleTimeSync0;
  int cycleTimeSync0Factor;
  int shiftTimeSync0;
  int shiftTimeSync0Factor;
  int cycleTimeSync1;
  int cycleTimeSync1Factor;
  int shiftTimeSync1;
  int shiftTimeSync1Factor;
} KSEcatDcParams;

typedef struct {
  int64 latch0PositivEdge;
  int64 latch0NegativEdge;
  int64 latch1PositivEdge;
  int64 latch1NegativEdge;
  int64 smBufferChange;
  int64 smPdiBufferStart;
  int64 smPdiBufferChange;
} KSEcatDcLatchTimeStamps;

typedef struct {
  int structSize;
  int connected;
  int proxiesOnline;
  int proxiesCreated;
  int proxiesCreatedAndOnline;
  int nodeState;
  int statusCode;
} KSEcatEapNodeState;

typedef struct {
  int structSize;
  int online;
  int created;
  uint ip;
  int proxyState;
  int statusCode;
} KSEcatEapProxyState;

typedef struct {
  int structSize;
  uint ip;
} KSEcatEapNodeId;

typedef struct {
  int structSize;
  int slaveState;
  int statusCode;
} KSEcatSlaveDeviceState;

//------ Context structures ------
typedef struct {
  int ctxType;
  KSHandle hMaster;
  KSHandle hSlave;
  int error;
} EcatErrorUserContext;

typedef struct {
  int ctxType;
  KSHandle hDataSet;
} EcatDataSetUserContext;

typedef struct {
  int ctxType;
  KSHandle hMaster;
  int slavesOnline;
  int slavesCreated;
  int slavesCreatedAndOnline;
  int reason;
  KSHandle hSlave;
} EcatTopologyUserContext;

typedef struct {
  int ctxType;
  KSHandle hMaster;
  int redundancyEffective;
} EcatRedundancyUserContext;

typedef struct {
  int ctxType;
  KSHandle hObject;
  int currentState;
  int requestedState;
} EcatStateChangeUserContext;

typedef struct {
  int ctxType;
  KSHandle hObject;
  int index;
  int subIndex;
  int writeAccess;
  int completeAccess;
} EcatObjectAccessUserContext;

typedef struct {
  int ctxType;
  KSHandle hObject;
  char fileName[256];
  int password;
  int writeAccess;
} EcatFileAccessUserContext;

typedef struct {
  int ctxType;
  KSHandle hObject;
} EcatProcessDataAccessUserContext;

//------ Master functions ------
Error __stdcall KS_createEcatMaster(
                KSHandle* phMaster, KSHandle hConnection, const char* libraryPath, const char* topologyFile, int flags);
Error __stdcall KS_closeEcatMaster(
                KSHandle hMaster);
Error __stdcall KS_installEcatMasterHandler(
                KSHandle hMaster, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_queryEcatMasterState(
                KSHandle hMaster, KSEcatMasterState* pMasterState, int flags);
Error __stdcall KS_changeEcatMasterState(
                KSHandle hMaster, int state, int flags);
Error __stdcall KS_addEcatRedundancy(
                KSHandle hMaster, KSHandle hConnection, int flags);

//------ Slave functions ------
Error __stdcall KS_createEcatSlave(
                KSHandle hMaster, KSHandle* phSlave, int id, int position, int vendor, int product, int revision, int flags);
Error __stdcall KS_createEcatSlaveIndirect(
                KSHandle hMaster, KSHandle* phSlave, const KSEcatSlaveState* pSlaveState, int flags);
Error __stdcall KS_enumEcatSlaves(
                KSHandle hMaster, int index, KSEcatSlaveState* pSlaveState, int flags);
Error __stdcall KS_deleteEcatSlave(
                KSHandle hSlave);
Error __stdcall KS_writeEcatSlaveId(
                KSHandle hSlave, int id, int flags);
Error __stdcall KS_queryEcatSlaveState(
                KSHandle hSlave, KSEcatSlaveState* pSlaveState, int flags);
Error __stdcall KS_changeEcatSlaveState(
                KSHandle hSlave, int state, int flags);
Error __stdcall KS_queryEcatSlaveInfo(
                KSHandle hSlave, KSEcatSlaveInfo** ppSlaveInfo, int flags);
Error __stdcall KS_queryEcatDataObjInfo(
                KSHandle hSlave, int objIndex, KSEcatDataObjInfo** ppDataObjInfo, int flags);
Error __stdcall KS_queryEcatDataVarInfo(
                KSHandle hSlave, int objIndex, int varIndex, KSEcatDataVarInfo** ppDataVarInfo, int flags);
Error __stdcall KS_enumEcatDataObjInfo(
                KSHandle hSlave, int objEnumIndex, KSEcatDataObjInfo** ppDataObjInfo, int flags);
Error __stdcall KS_enumEcatDataVarInfo(
                KSHandle hSlave, int objEnumIndex, int varEnumIndex, KSEcatDataVarInfo** ppDataVarInfo, int flags);
Error __stdcall KS_loadEcatInitCommands(
                KSHandle hSlave, const char* filename, int flags);

//------ DataSet related functions ------
Error __stdcall KS_createEcatDataSet(
                KSHandle hObject, KSHandle* phSet, void** ppAppPtr, void** ppSysPtr, int flags);
Error __stdcall KS_deleteEcatDataSet(
                KSHandle hSet);
Error __stdcall KS_assignEcatDataSet(
                KSHandle hSet, KSHandle hObject, int syncIndex, int placement, int flags);
Error __stdcall KS_readEcatDataSet(
                KSHandle hSet, int flags);
Error __stdcall KS_postEcatDataSet(
                KSHandle hSet, int flags);
Error __stdcall KS_installEcatDataSetHandler(
                KSHandle hSet, int eventCode, KSHandle hSignal, int flags);

//------ Common functions ------
Error __stdcall KS_changeEcatState(
                KSHandle hObject, int state, int flags);
Error __stdcall KS_installEcatHandler(
                KSHandle hObject, int eventCode, KSHandle hSignal, int flags);

//------ Data exchange functions ------
Error __stdcall KS_getEcatDataObjAddress(
                KSHandle hSet, KSHandle hObject, int pdoIndex, int varIndex, void** ppAppPtr, void** ppSysPtr, int* pBitOffset, int* pBitLength, int flags);
Error __stdcall KS_readEcatDataObj(
                KSHandle hObject, int objIndex, int varIndex, void* pData, int* pSize, int flags);
Error __stdcall KS_postEcatDataObj(
                KSHandle hObject, int objIndex, int varIndex, const void* pData, int size, int flags);
Error __stdcall KS_setEcatPdoAssign(
                KSHandle hObject, int syncIndex, int pdoIndex, int flags);
Error __stdcall KS_setEcatPdoMapping(
                KSHandle hObject, int pdoIndex, int objIndex, int varIndex, int bitLength, int flags);
Error __stdcall KS_uploadEcatFile(
                KSHandle hObject, const char* fileName, int password, void* pData, int* pSize, int flags);
Error __stdcall KS_downloadEcatFile(
                KSHandle hObject, const char* fileName, int password, const void* pData, int size, int flags);
Error __stdcall KS_readEcatSlaveMem(
                KSHandle hSlave, int address, void* pData, int size, int flags);
Error __stdcall KS_writeEcatSlaveMem(
                KSHandle hSlave, int address, const void* pData, int size, int flags);

//------ DC functions ------
Error __stdcall KS_activateEcatDcMode(
                KSHandle hObject, int64ref startTime, int cycleTime, int shiftTime, KSHandle hTimer, int flags);
Error __stdcall KS_enumEcatDcOpModes(
                KSHandle hSlave, int index, char* pDcOpModeBuf, char* pDescriptionBuf, int flags);
Error __stdcall KS_lookupEcatDcOpMode(
                KSHandle hSlave, const char* dcOpMode, KSEcatDcParams* pDcParams, int flags);
Error __stdcall KS_configEcatDcOpMode(
                KSHandle hSlave, const char* dcOpMode, const KSEcatDcParams* pDcParams, int flags);

//------ Auxiliary functions ------
Error __stdcall KS_execEcatCommand(
                KSHandle hObject, int command, void* pParam, int flags);
Error __stdcall KS_getEcatAlias(
                KSHandle hObject, int objIndex, int varIndex, char* pAliasBuf, int flags);
Error __stdcall KS_setEcatAlias(
                KSHandle hObject, int objIndex, int varIndex, const char* alias, int flags);

//------ EtherCAT Slave Device functions ------
Error __stdcall KS_openEcatSlaveDevice(
                KSHandle* phSlaveDevice, const char* name, int flags);
Error __stdcall KS_closeEcatSlaveDevice(
                KSHandle hSlaveDevice, int flags);
Error __stdcall KS_queryEcatSlaveDeviceState(
                KSHandle hSlaveDevice, KSEcatSlaveDeviceState* pSlaveState, int flags);

//------ EtherCAT Automation Protocol functions ------
Error __stdcall KS_createEcatEapNode(
                KSHandle* phNode, KSHandle hConnection, const char* networkFile, int flags);
Error __stdcall KS_closeEcatEapNode(
                KSHandle hNode, int flags);
Error __stdcall KS_queryEcatEapNodeState(
                KSHandle hNode, KSEcatEapNodeState* pNodeState, int flags);
Error __stdcall KS_createEcatEapProxy(
                KSHandle hNode, KSHandle* phProxy, const KSEcatEapNodeId* pNodeId, int flags);
Error __stdcall KS_deleteEcatEapProxy(
                KSHandle hProxy, int flags);
Error __stdcall KS_queryEcatEapProxyState(
                KSHandle hProxy, KSEcatEapProxyState* pProxyState, int flags);

// ATTENTION! The API of the older version 1.0 of the Kithara EtherCAT Master is not contained here! Please
// download the header file _ethercat_v1.h from www.kithara.de and include this file additionally!

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsEcatObject
//--------------------------------------------------------------------------------------------------------------

struct KsEcatObject : public KsHandle {
public:
  void changeState(int state, int flags = 0) {
    if (Error error = KS_changeEcatState(hHandle_, state, flags))
      throw KsException(error, "KS_changeEcatState");
  }

  void installHandler(int eventCode, const KsSignal& signal, int flags = 0) {
    if (Error error = KS_installEcatHandler(hHandle_, eventCode, signal, flags))
      throw KsException(error, "KS_installEcatHandler");
  }
  void uninstallHandler(int eventCode, int flags = 0) {
    if (Error error = KS_installEcatHandler(hHandle_, eventCode, KS_INVALID_HANDLE, flags))
      throw KsException(error, "KS_installEcatHandler");
  }

  void execCommand(int command, void* pParam, int flags = 0) {
    if (Error error = KS_execEcatCommand(hHandle_, command, pParam, flags))
      throw KsException(error, "KS_execEcatCommand");
  }

  void activateDcMode(int64ref startTime, int cycleTime, int shiftTime, int flags = 0) {
    activateDcMode(startTime, cycleTime, shiftTime, KS_INVALID_HANDLE, flags);
  }
  void activateDcMode(int64ref startTime, int cycleTime, int shiftTime, const KsTimer& timer, int flags = 0) {
    activateDcMode(startTime, cycleTime, shiftTime, timer, flags);
  }

  KsString getAlias(int index = -1, int subIndex = -1) {
    char pAlias[256];
    if (Error error = KS_getEcatAlias(hHandle_, index, subIndex, pAlias, 0))
      throw KsException(error, "KS_execEcatCommand");
    return pAlias;
  }

protected:
  KsEcatObject() {
  }

  void activateDcMode(int64ref startTime, int cycleTime, int shiftTime, KSHandle hTimer, int flags) {
    if (Error error = KS_activateEcatDcMode(hHandle_, startTime, cycleTime, shiftTime, hTimer, flags))
      throw KsException(error, "KS_activateEcatDcMode");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsEcatMaster
//--------------------------------------------------------------------------------------------------------------

struct KsEcatMaster : public KsEcatObject {
  KsEcatMaster() {
  }
  KsEcatMaster(const KsNicAdapter& nicAdapter, const KsString& libraryPath, const KsString& topologyFile = null, int flags = 0) {
    create(nicAdapter, libraryPath, topologyFile, flags);
  }

  void create(const KsNicAdapter& nicAdapter, const KsString& libraryPath, const KsString& topologyFile = null, int flags = 0) {
    if (Error error = KS_createEcatMaster(&hHandle_, nicAdapter, libraryPath, topologyFile, flags))
      throw KsException(error, "KS_createEcatMaster");
  }

  void queryState(KSEcatMasterState* pState) {
    if (Error error = KS_queryEcatMasterState(hHandle_, pState, 0))
      throw KsException(error, "KS_queryEcatMasterState");
  }
  KSEcatMasterState state() {
    KSEcatMasterState state = { sizeof(KSEcatMasterState) };
    queryState(&state);
    return state;
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsEcatSlaveEnumeration
//--------------------------------------------------------------------------------------------------------------

struct KsEcatSlaveEnumeration : public _KsEnumeration<KSEcatSlaveState> {
public:
  KsEcatSlaveEnumeration(const KsEcatMaster& ecatMaster) {
    KSEcatSlaveState requestedState = {
      sizeof(KSEcatSlaveState), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };
    init(ecatMaster, requestedState, 0);
  }
  KsEcatSlaveEnumeration(const KsEcatMaster& ecatMaster, int vendor, int product, int revision) {
    KSEcatSlaveState requestedState = {
      sizeof(KSEcatSlaveState), 0, 0, vendor, product, revision, 0, 0, 0, 0, 0, 0
    };
    init(ecatMaster, requestedState, VENDOR | PRODUCT | REVISION);
  }

private:
  enum RequestFlags {
    ONLINE      = 1 << 0,
    CREATED     = 1 << 1,
    VENDOR      = 1 << 2,
    PRODUCT     = 1 << 3,
    REVISION    = 1 << 4,
    ID          = 1 << 5,
    RELPOSITION = 1 << 6,
    ABSPOSITION = 1 << 7,
    LINKSTATE   = 1 << 8,
    SLAVESTATE  = 1 << 9
  };

  void init(const KsEcatMaster& ecatMaster, const KSEcatSlaveState& requestedState, int requestFlags) {
    for (int i = 0; ; ++i) {
      KSEcatSlaveState slaveState = { sizeof(KSEcatSlaveState) };
      Error error = KS_enumEcatSlaves(ecatMaster, i, &slaveState, 0);
      if (KSERROR_CODE(error) == KSERROR_DEVICE_NOT_FOUND)
        break;
      if (error)
        throw KsException(error, "KS_enumEcatSlaves");

      if ((requestFlags & ONLINE) && (slaveState.online != requestedState.online))
        continue;
      if ((requestFlags & CREATED) && (slaveState.created != requestedState.created))
        continue;
      if ((requestFlags & VENDOR) && (requestedState.vendor != 0) && (slaveState.vendor != requestedState.vendor))
        continue;
      if ((requestFlags & PRODUCT) && (requestedState.product != 0) && (slaveState.product != requestedState.product))
        continue;
      if ((requestFlags & REVISION) && (requestedState.revision != 0) && (slaveState.revision != requestedState.revision))
        continue;
      if ((requestFlags & ID) && (slaveState.id != requestedState.id))
        continue;
      if ((requestFlags & RELPOSITION) && (slaveState.relPosition != requestedState.relPosition))
        continue;
      if ((requestFlags & ABSPOSITION) && (slaveState.absPosition != requestedState.absPosition))
        continue;
      if ((requestFlags & LINKSTATE) && (slaveState.linkState != requestedState.linkState))
        continue;
      if ((requestFlags & SLAVESTATE) && (slaveState.slaveState != requestedState.slaveState))
        continue;

      add(slaveState);
    }
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsEcatSlave
//--------------------------------------------------------------------------------------------------------------

struct KsEcatSlave : public KsEcatObject {
  KsEcatSlave() {
  }
  KsEcatSlave(const KsEcatMaster& master, const KSEcatSlaveState& slaveState, int flags = 0) {
    create(master, slaveState, flags);
  }
  KsEcatSlave(const KsEcatMaster& master, int vendor, int product, int revision, int flags = 0) {
    create(master, vendor, product, revision, flags);
  }

  void create(const KsEcatMaster& master, const KSEcatSlaveState& slaveState, int flags = 0) {
    if (Error error = KS_createEcatSlaveIndirect(master, &hHandle_, &slaveState, flags))
      throw KsException(error, "KS_createEcatSlaveIndirect");
  }
  void create(const KsEcatMaster& master, int vendor, int product, int revision, int flags = 0) {
    if (Error error = KS_createEcatSlave(master, &hHandle_, 0, 0, vendor, product, revision, flags))
      throw KsException(error, "KS_createEcatSlave");
  }

  void queryState(KSEcatSlaveState* pState) {
    if (Error error = KS_queryEcatSlaveState(hHandle_, pState, 0))
      throw KsException(error, "KS_queryEcatSlaveState");
  }
  KSEcatSlaveState state() {
    KSEcatSlaveState state = { sizeof(KSEcatSlaveState) };
    queryState(&state);
    return state;
  }

  void queryInfo(const KSEcatSlaveInfo** ppInfo, int flags = 0) {
    if (Error error = KS_queryEcatSlaveInfo(hHandle_, const_cast<KSEcatSlaveInfo**>(ppInfo), flags))
      throw KsException(error, "KS_queryEcatSlaveInfo");
  }
  const KSEcatSlaveInfo* info(int flags = 0) {
    const KSEcatSlaveInfo* pInfo;
    queryInfo(&pInfo, flags);
    return pInfo;
  }

  void enumObjInfo(int enumIndex, const KSEcatDataObjInfo** ppInfo, int flags = 0) {
    if (Error error = KS_enumEcatDataObjInfo(hHandle_, enumIndex, const_cast<KSEcatDataObjInfo**>(ppInfo), flags))
      throw KsException(error, "KS_enumEcatDataObjInfo");
  }

  void writeId(int id) {
    if (Error error = KS_writeEcatSlaveId(hHandle_, id, 0))
      throw KsException(error, "KS_writeEcatSlaveId");
  }

  void setPdoMapping(int pdoIndex, int entryIndex, int entrySubIndex) {
    if (Error error = KS_setEcatPdoMapping(hHandle_, pdoIndex, entryIndex, entrySubIndex, -1, 0))
      throw KsException(error, "KS_setEcatPdoMapping");
  }

  void loadInitCommands(const KsString& file, int flags = 0) {
    if (Error error = KS_loadEcatInitCommands(hHandle_, file, flags))
      throw KsException(error, "KS_loadEcatInitCommands");
  }

  void readDataObj(int index, int subIndex, void* pData, int* pSize, int flags) {
    if (Error error = KS_readEcatDataObj(hHandle_, index, subIndex, pData, pSize, flags))
      throw KsException(error, "KS_readEcatDataObj");
  }
  void postDataObj(int index, int subIndex, const void* pData, int size, int flags) {
    if (Error error = KS_postEcatDataObj(hHandle_, index, subIndex, pData, size, flags))
      throw KsException(error, "KS_postEcatDataObj");
  }

  void readMem(int address, void* pData, int size, int flags) {
    if (Error error = KS_readEcatSlaveMem(hHandle_, address, pData, size, flags))
      throw KsException(error, "KS_readEcatSlaveMem");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsEcatDataSet
//--------------------------------------------------------------------------------------------------------------

struct KsEcatDataSet : public KsEcatObject {
  KsEcatDataSet(const KsEcatMaster& master, void** ppApp = null, void** ppSys = null) {
    if (Error error = KS_createEcatDataSet(master, &hHandle_, ppApp, ppSys, 0))
      throw KsException(error, "KS_createEcatDataSet");
  }

  int size() const {
    int size = 0;
    if (Error error = KS_execEcatCommand(*this, KS_ECAT_GET_BUFFER_SIZE, &size, 0))
      throw KsException(error, "KS_execEcatCommand");
    return size;
  }

  void assign(const KsEcatSlave& slave, int syncIndex, int placement, int flags)  const {
    if (Error error = KS_assignEcatDataSet(hHandle_, slave, syncIndex, placement, flags))
      throw KsException(error, "KS_assignEcatDataSet");
  }

  void post(int flags)  const {
    if (Error error = KS_postEcatDataSet(hHandle_, flags))
      throw KsException(error, "KS_postEcatDataSet");
  }

  void read(int flags) const {
    if (Error error = KS_readEcatDataSet(hHandle_, flags))
      throw KsException(error, "KS_readEcatDataSet");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsEcatDataSet
//--------------------------------------------------------------------------------------------------------------

struct KsEcatDataSetExt : public KsEcatDataSet {
  KsEcatDataSetExt(const KsEcatMaster& master)
  : KsEcatDataSet(master, &pApp_, &pSys_) {}

  void* ptr() const {
    return pApp_;
  }
  void* sys() const {
    return pSys_;
  }

  int getDataObjOffset(const KsEcatObject& object, int pdoIndex, int varIndex, int* pBitOffset, int* pBitLength) {
    void* pValue = 0;
    if (Error error = KS_getEcatDataObjAddress(hHandle_, object, pdoIndex, varIndex, &pValue, 0, pBitOffset, pBitLength, 0))
      throw KsException(error, "KS_getEcatDataObjAddress");
    return static_cast<int>(reinterpret_cast<byte*>(pValue)-reinterpret_cast<byte*>(pApp_));
  }

private:
  void* pApp_;
  void* pSys_;
};

//--------------------------------------------------------------------------------------------------------------
// KsEcatDcOpModeInformation
//--------------------------------------------------------------------------------------------------------------

struct KsEcatDcOpModeInformation {
  char name[256];
  char description[256];
  KSEcatDcParams parameters;
};

//--------------------------------------------------------------------------------------------------------------
// KsEcatDcOpModeEnumeration
//--------------------------------------------------------------------------------------------------------------

struct KsEcatDcOpModeEnumeration : public _KsEnumeration<KsEcatDcOpModeInformation> {
public:
  KsEcatDcOpModeEnumeration(const KsEcatSlave& ecatSlave) {
    init(ecatSlave);
  }
  KsEcatDcOpModeEnumeration(const KsEcatSlave* pEcatSlave) {
    init(*pEcatSlave);
  }

private:
  void init(const KsEcatSlave& ecatSlave) {
    for (int i = 0; ; ++i) {
      KsEcatDcOpModeInformation dcOpModeInfo;
      Error error = KS_enumEcatDcOpModes(ecatSlave, i, dcOpModeInfo.name, dcOpModeInfo.description, 0);
      if (KSERROR_CODE(error) == KSERROR_DEVICE_NOT_FOUND)
        break;
      if (error)
        throw KsException(error, "KS_enumEcatDcOpModes");

      if (Error error = KS_lookupEcatDcOpMode(ecatSlave, dcOpModeInfo.name, &dcOpModeInfo.parameters, 0))
        throw KsException(error, "KS_lookupDcOpMode");

      add(dcOpModeInfo);
    }
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_ETHERCAT_MODULE

//--------------------------------------------------------------------------------------------------------------
// MultiFunction Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Flags ------
#define KSF_UNIPOLAR                                    0x00000001
#define KSF_BIPOLAR                                     0x00000000
#define KSF_DIFFERENTIAL                                0x00000002
#define KSF_SINGLE_ENDED                                0x00000000
#define KSF_RISING_EDGE                                 0x00001000
#define KSF_FALLING_EDGE                                0x00000000
#define KSF_NORMALIZE                                   0x00000800
#define KSF_STOP_ANALOG_STREAM                          0x00000400

//------ Multi function events ------
#define KS_MULTIFUNCTION_INTERRUPT                      (MULTIFUNCTION_BASE+0x00000000)
#define KS_MULTIFUNCTION_ANALOGWATCHDOG                 (MULTIFUNCTION_BASE+0x00000001)
#define KS_MULTIFUNCTION_DIGITALWATCHDOG                (MULTIFUNCTION_BASE+0x00000002)
#define KS_MULTIFUNCTION_TIMER                          (MULTIFUNCTION_BASE+0x00000003)
#define KS_MULTIFUNCTION_ANALOG_SEQUENCE                (MULTIFUNCTION_BASE+0x00000004)
#define KS_MULTIFUNCTION_ANALOG_STREAM                  (MULTIFUNCTION_BASE+0x00000006)
#define KS_MULTIFUNCTION_DIGITAL_IN                     (MULTIFUNCTION_BASE+0x00000007)
#define KS_MULTIFUNCTION_BUFFER_FULL                    (MULTIFUNCTION_BASE+0x00000008)

//------ Board commands ------
#define KS_BOARD_RESET                                  0x00000000
#define KS_BOARD_SEQUENCE_READY                         0x00000001

//------ Context structures ------
typedef struct {
  int ctxType;
  KSHandle hBoard;
} BoardUserContext;

typedef struct {
  int ctxType;
  KSHandle hBoard;
  int lostDataCount;
} BoardStreamErrorContext;

typedef struct {
  int structSize;
  char pBoardName[64];
  char pBoardSpec[256];
  int analogInp;
  int analogOut;
  int digitalInOut;
  int digitalInp;
  int digitalOut;
  int digitalPortSize;
  int analogValueSize;
} KSMfBoardInfo;

//------ Basic functions ------
Error __stdcall KS_openBoard(
                KSHandle* phBoard, const char* name, KSHandle hKernel, int flags);
Error __stdcall KS_closeBoard(
                KSHandle hBoard, int flags);
Error __stdcall KS_getBoardInfo(
                KSHandle hBoard, KSMfBoardInfo* pBoardInfo, int flags);
Error __stdcall KS_setBoardObject(
                KSHandle hBoard, void* pAppAddr, void* pSysAddr);
Error __stdcall KS_getBoardObject(
                KSHandle hBoard, void** ppAddr);
Error __stdcall KS_installBoardHandler(
                KSHandle hBoard, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_execBoardCommand(
                KSHandle hBoard, int command, void* pParam, int flags);

//------ Low-level functions ------
Error __stdcall KS_readBoardRegister(
                KSHandle hBoard, int rangeIndex, int offset, int* pValue, int flags);
Error __stdcall KS_writeBoardRegister(
                KSHandle hBoard, int rangeIndex, int offset, int value, int flags);

//------ Digital input functions ------
Error __stdcall KS_getDigitalBit(
                KSHandle hBoard, int channel, uint* pValue, int flags);
Error __stdcall KS_getDigitalPort(
                KSHandle hBoard, int port, uint* pValue, int flags);

//------ Digital output functions ------
Error __stdcall KS_setDigitalBit(
                KSHandle hBoard, int channel, uint value, int flags);
Error __stdcall KS_setDigitalPort(
                KSHandle hBoard, int port, uint value, uint mask, int flags);

//------ Analog input functions ------
Error __stdcall KS_configAnalogChannel(
                KSHandle hBoard, int channel, int gain, int mode, int flags);
Error __stdcall KS_getAnalogValue(
                KSHandle hBoard, int channel, uint* pValue, int flags);
Error __stdcall KS_initAnalogStream(
                KSHandle hBoard, int* pChannelSequence, int sequenceLength, int sequenceCount, int period, int flags);
Error __stdcall KS_startAnalogStream(
                KSHandle hBoard, int flags);
Error __stdcall KS_getAnalogStream(
                KSHandle hBoard, void* pBuffer, int length, int* pLength, int flags);
Error __stdcall KS_valueToVolt(
                KSHandle hBoard, int gain, int mode, uint value, float* pVolt, int flags);

//------ Analog output functions ------
Error __stdcall KS_setAnalogValue(
                KSHandle hBoard, int channel, int mode, int value, int flags);
Error __stdcall KS_voltToValue(
                KSHandle hBoard, int mode, float volt, uint* pValue, int flags);

// -------------------------------------------------------------------------------------------------------------
// MultiFunction Module Driver Interface
// -------------------------------------------------------------------------------------------------------------

// You only need KSDRV functions, if you develop your own driver DLL!

//------ Basic functions (KSDRV API) ------
__declspec(dllexport)
Error __stdcall KSDRV_checkBoard(
                const char* pDeviceName, int flags);
                typedef Error (__stdcall* KSDRV_checkBoardProc)(const char*, int);
__declspec(dllexport)
Error __stdcall KSDRV_getDriverVersion(
                uint* pVersion, int flags);
                typedef Error (__stdcall* KSDRV_getDriverVersionProc)(uint*, int);
__declspec(dllexport)
Error __stdcall KSDRV_initKernel(
                const char* name, KSHandle hKernel);
                typedef Error (__stdcall* KSDRV_initKernelProc)(const char*, KSHandle);
__declspec(dllexport)
Error __stdcall KSDRV_openBoard(
                KSHandle hBoard, const char* name, int flags);
                typedef Error (__stdcall* KSDRV_openBoardProc)(KSHandle, const char*, int);
__declspec(dllexport)
Error __stdcall KSDRV_closeBoard(
                KSHandle hBoard, int flags);
                typedef Error (__stdcall* KSDRV_closeBoardProc)(KSHandle, int);
__declspec(dllexport)
Error __stdcall KSDRV_getBoardInfo(
                KSHandle hBoard, KSMfBoardInfo* pBoardInfo, int flags);
                typedef Error (__stdcall* KSDRV_getBoardInfoProc)(KSHandle, KSMfBoardInfo*, int);
__declspec(dllexport)
Error __stdcall KSDRV_installBoardHandler(
                KSHandle hBoard, int eventCode, KSHandle hCallBack, int flags);
                typedef Error (__stdcall* KSDRV_installBoardHandlerProc)(KSHandle, int, KSHandle, int);
__declspec(dllexport)
Error __stdcall KSDRV_execBoardCommand(
                KSHandle hBoard, int command, void* pParam, int flags);
                typedef Error (__stdcall* KSDRV_execBoardCommandProc)(KSHandle, int, void*, int);
__declspec(dllexport)
Error __stdcall KSDRV_resetBoard(
                KSHandle hBoard, int flags);
                typedef Error (__stdcall* KSDRV_resetBoardProc)(KSHandle, int);
__declspec(dllexport)
Error __stdcall KSDRV_onInterrupt(
                KSHandle hBoard, int flags);
                typedef Error (__stdcall* KSDRV_onInterruptProc)(KSHandle, int);

//------ Digital input functions (KSDRV API) ------
__declspec(dllexport)
Error __stdcall KSDRV_getDigitalBit(
                KSHandle hBoard, int channel, uint* pValue, int flags);
                typedef Error (__stdcall* KSDRV_getDigitalBitProc)(KSHandle, int, uint*, int);
__declspec(dllexport)
Error __stdcall KSDRV_getDigitalPort(
                KSHandle hBoard, int port, uint* pValue, int flags);
                typedef Error (__stdcall* KSDRV_getDigitalPortProc)(KSHandle, int, uint*, int);

//------ Digital output functions (KSDRV API) ------
__declspec(dllexport)
Error __stdcall KSDRV_setDigitalBit(
                KSHandle hBoard, int channel, uint value, int flags);
                typedef Error (__stdcall* KSDRV_setDigitalBitProc)(KSHandle, int, uint, int);
__declspec(dllexport)
Error __stdcall KSDRV_setDigitalPort(
                KSHandle hBoard, int port, uint value, uint mask, int flags);
                typedef Error (__stdcall* KSDRV_setDigitalPortProc)(KSHandle, int, uint, uint, int);

//------ Analog input functions (KSDRV API) ------
__declspec(dllexport)
Error __stdcall KSDRV_configAnalogChannel(
                KSHandle hBoard, int channel, int gain, int mode, int flags);
                typedef Error (__stdcall* KSDRV_configAnalogChannelProc)(KSHandle, int, int, int, int);
__declspec(dllexport)
Error __stdcall KSDRV_getAnalogValue(
                KSHandle hBoard, int channel, uint* pValue, int flags);
                typedef Error (__stdcall* KSDRV_getAnalogValueProc)(KSHandle, int, uint*, int);
__declspec(dllexport)
Error __stdcall KSDRV_initAnalogSequence(
                KSHandle hBoard, int* pChannelSequence, int sequenceLength, int flags);
                typedef Error (__stdcall* KSDRV_initAnalogSequenceProc)(KSHandle, int*, int, int);
__declspec(dllexport)
Error __stdcall KSDRV_getAnalogSequence(
                KSHandle hBoard, uint* pValue, int flags);
                typedef Error (__stdcall* KSDRV_getAnalogSequenceProc)(KSHandle, uint*, int);
__declspec(dllexport)
Error __stdcall KSDRV_initAnalogStream(
                KSHandle hBoard, int* pChannelSequence, int sequenceLength, int sequenceCount, int period, int flags);
                typedef Error (__stdcall* KSDRV_initAnalogStreamProc)(KSHandle, int*, int, int, int, int);
__declspec(dllexport)
Error __stdcall KSDRV_startAnalogStream(
                KSHandle hBoard, void* pBuf, ulong physMem, int bufSize, int flags);
                typedef Error (__stdcall* KSDRV_startAnalogStreamProc)(KSHandle, void*, ulong, int, int);
__declspec(dllexport)
Error __stdcall KSDRV_getAnalogStream(
                KSHandle hBoard, void* pBuffer, int length, int* pLength, int flags);
                typedef Error (__stdcall* KSDRV_getAnalogStreamProc)(KSHandle, void*, int, int*, int);
__declspec(dllexport)
Error __stdcall KSDRV_valueToVolt(
                KSHandle hBoard, int gain, int mode, uint value, float* pVolt, int flags);
                typedef Error (__stdcall* KSDRV_valueToVoltProc)(KSHandle, int, int, uint, float*, int);

//------ Analog output functions (KSDRV API) ------
__declspec(dllexport)
Error __stdcall KSDRV_setAnalogValue(
                KSHandle hBoard, int channel, int mode, int value, int flags);
                typedef Error (__stdcall* KSDRV_setAnalogValueProc)(KSHandle, int, int, int, int);
__declspec(dllexport)
Error __stdcall KSDRV_voltToValue(
                KSHandle hBoard, int mode, float volt, uint* pValue, int flags);
                typedef Error (__stdcall* KSDRV_voltToValueProc)(KSHandle, int, float, uint*, int);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_MULTIFUNCTION_MODULE

//--------------------------------------------------------------------------------------------------------------
// CAN Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_BAD_IDENTIFIER                          (KSERROR_CATEGORY_CAN+0x00000000)
#define KSERROR_RECV_BUFFER_FULL                        (KSERROR_CATEGORY_CAN+0x00010000)
#define KSERROR_XMIT_BUFFER_FULL                        (KSERROR_CATEGORY_CAN+0x00020000)
#define KSERROR_BUS_OFF                                 (KSERROR_CATEGORY_CAN+0x00030000)
#define KSERROR_BUS_PASSIVE                             (KSERROR_CATEGORY_CAN+0x00040000)
#define KSERROR_ERROR_COUNTER_WARNING                   (KSERROR_CATEGORY_CAN+0x00050000)
#define KSERROR_PORT_DATA_BUFFER_OVERRUN                (KSERROR_CATEGORY_CAN+0x00060000)
#define KSERROR_BUS_ERROR                               (KSERROR_CATEGORY_CAN+0x00070000)

//------ CAN mode ------
#define KS_CAN_NORMAL_MODE                              0x00000001
#define KS_CAN_RESET_MODE                               0x00000002
#define KS_CAN_LISTEN_ONLY_MODE                         0x00000004

//------ CAN state ------
#define KS_CAN_BUS_ON                                   0x00000001
#define KS_CAN_BUS_HEAVY                                0x00000002
#define KS_CAN_BUS_OFF                                  0x00000004
#define KS_CAN_RECV_AVAIL                               0x00000008
#define KS_CAN_XMIT_PENDING                             0x00000010
#define KS_CAN_WARN_LIMIT_REACHED                       0x00000020
#define KS_CAN_DATA_BUFFER_OVERRUN                      0x00000040

//------ CAN commands ------
#define KS_SIGNAL_XMIT_EMPTY                            0x00000004
#define KS_SET_LISTEN_ONLY                              0x00000005
#define KS_RESET_PORT                                   0x00000006
#define KS_GET_ERROR_CODE_CAPTURE                       0x00000007
#define KS_CAN_SET_XMIT_TIMEOUT                         0x00000008
#define KS_CAN_CLEAR_BUS_OFF                            0x0000000a

//------ CAN baud rates ------
#define KS_CAN_BAUD_1M                                  0x000f4240
#define KS_CAN_BAUD_800K                                0x000c3500
#define KS_CAN_BAUD_666K                                0x000a2c2a
#define KS_CAN_BAUD_500K                                0x0007a120
#define KS_CAN_BAUD_250K                                0x0003d090
#define KS_CAN_BAUD_125K                                0x0001e848
#define KS_CAN_BAUD_100K                                0x000186a0
#define KS_CAN_BAUD_50K                                 0x0000c350
#define KS_CAN_BAUD_20K                                 0x00004e20
#define KS_CAN_BAUD_10K                                 0x00002710

//------ CAN-FD baud rates ------
#define KS_CAN_FD_BAUD_12M                              0x00b71b00
#define KS_CAN_FD_BAUD_10M                              0x00989680
#define KS_CAN_FD_BAUD_8M                               0x007a1200
#define KS_CAN_FD_BAUD_6M                               0x005b8d80
#define KS_CAN_FD_BAUD_4M                               0x003d0900
#define KS_CAN_FD_BAUD_2M                               0x001e8480

//------ Common CAN and CAN-FD baud rates ------
#define KS_CAN_BAUD_CUSTOM                              0x00000000

//------ CAN message types ------
#define KS_CAN_MSGTYPE_STANDARD                         0x00000001
#define KS_CAN_MSGTYPE_EXTENDED                         0x00000002
#define KS_CAN_MSGTYPE_RTR                              0x00000004
#define KS_CAN_MSGTYPE_FD                               0x00000008
#define KS_CAN_MSGTYPE_BRS                              0x00000010

//------ Legacy CAN events ------
#define KS_CAN_RECV                                     (CAN_BASE+0x00000000)
#define KS_CAN_XMIT_EMPTY                               (CAN_BASE+0x00000001)
#define KS_CAN_XMIT_RTR                                 (CAN_BASE+0x00000002)
#define KS_CAN_ERROR                                    (CAN_BASE+0x00000003)
#define KS_CAN_FILTER                                   (CAN_BASE+0x00000004)
#define KS_CAN_TIMEOUT                                  (CAN_BASE+0x00000005)

//------ CAN-FD events ------
#define KS_CAN_FD_RECV                                  (CAN_BASE+0x00000006)
#define KS_CAN_FD_XMIT_EMPTY                            (CAN_BASE+0x00000007)
#define KS_CAN_FD_ERROR                                 (CAN_BASE+0x00000008)

//------ CAN-FD data structures ------
typedef struct {
  int structSize;
  int identifier;
  uint msgType;
  int dataLength;
  byte msgData[64];
  int64 timestamp;
} KSCanFdMsg;

typedef struct {
  int structSize;
  uint state;
  int waitingForXmit;
  int waitingForRecv;
  int xmitErrCounter;
  int recvErrCounter;
} KSCanFdState;

typedef struct {
  int structSize;
  int clockFrequency;
  int canBaudRate;
  int canBrp;
  int canTseg1;
  int canTseg2;
  int canSjw;
  int fdBaudRate;
  int fdBrp;
  int fdTseg1;
  int fdTseg2;
  int fdSjw;
} KSCanFdConfig;

//------ CAN-FD Context structures ------
typedef struct {
  int ctxType;
  KSHandle hCanFd;
} KSCanFdContext;

typedef struct {
  int ctxType;
  KSHandle hCanFd;
  Error errorValue;
  int64 timestamp;
} KSCanFdErrorContext;

typedef struct {
  int ctxType;
  KSHandle hCanFd;
  KSCanFdMsg msg;
} KSCanFdMsgContext;

//------ CAN data structures ------
typedef struct {
  int identifier;
  ushort msgType;
  ushort dataLength;
  byte msgData[8];
  int64 timestamp;
} KSCanMsg;

typedef struct {
  int structSize;
  int mode;
  int state;
  int baudRate;
  int waitingForXmit;
  int waitingForRecv;
  int errorWarnLimit;
  int recvErrCounter;
  int xmitErrCounter;
  int lastError;
} KSCanState;

//------ CAN Context structures ------
typedef struct {
  int ctxType;
  KSHandle hCan;
} CanUserContext;

typedef struct {
  int ctxType;
  KSHandle hCan;
  Error errorValue;
  int64 timestamp;
} CanErrorUserContext;

typedef struct {
  int ctxType;
  KSHandle hCan;
  KSCanMsg msg;
  int discarded;
} CanMsgUserContext;

typedef struct {
  int ctxType;
  KSHandle hCan;
  KSCanMsg msg;
} CanTimeoutUserContext;

//------ Common CAN-FD functions ------
Error __stdcall KS_openCanFd(
                KSHandle* phCanFd, const char* name, int port, const KSCanFdConfig* pCanFdConfig, int flags);
Error __stdcall KS_closeCanFd(
                KSHandle hCanFd, int flags);

//------ CAN-FD messaging functions ------
Error __stdcall KS_xmitCanFdMsg(
                KSHandle hCanFd, const KSCanFdMsg* pCanFdMsg, int flags);
Error __stdcall KS_recvCanFdMsg(
                KSHandle hCanFd, KSCanFdMsg* pCanFdMsg, int flags);
Error __stdcall KS_installCanFdHandler(
                KSHandle hCanFd, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_getCanFdState(
                KSHandle hCanFd, KSCanFdState* pCanFdState, int flags);
Error __stdcall KS_execCanFdCommand(
                KSHandle hCanFd, int command, void* pParam, int flags);

//------ Common CAN functions ------
Error __stdcall KS_openCan(
                KSHandle* phCan, const char* name, int port, int baudRate, int flags);
Error __stdcall KS_openCanEx(
                KSHandle* phCan, KSHandle hConnection, int port, int baudRate, int flags);
Error __stdcall KS_closeCan(
                KSHandle hCan);

//------ CAN messaging functions ------
Error __stdcall KS_xmitCanMsg(
                KSHandle hCan, const KSCanMsg* pCanMsg, int flags);
Error __stdcall KS_recvCanMsg(
                KSHandle hCan, KSCanMsg* pCanMsg, int flags);
Error __stdcall KS_installCanHandler(
                KSHandle hCan, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_getCanState(
                KSHandle hCan, KSCanState* pCanState);
Error __stdcall KS_execCanCommand(
                KSHandle hCan, int command, void* pParam, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsCanPort
//--------------------------------------------------------------------------------------------------------------

struct KsCanPort : public KsHandle {
public:
  KsCanPort() {}
  KsCanPort(const KsString& name, int port, int baudRate, int flags = 0) {
    open(name, port, baudRate, flags);
  }
  KsCanPort(const KsHandle& connection, int port, int baudRate, int flags = 0) {
    open(connection, port, baudRate, flags);
  }

  void open(const KsString& name, int port, int baudRate, int flags = 0) {
    if (Error error = KS_openCan(&hHandle_, name, port, baudRate, flags))
      throw KsException(error, "KS_openCan");
  }
  void open(const KsHandle& connection, int port, int baudRate, int flags = 0) {
    if (Error error = KS_openCanEx(&hHandle_, connection, port, baudRate, flags))
      throw KsException(error, "KS_openCan");
  }

  void installHandler(int eventCode, const KsSignal& signal) {
    if (Error error = KS_installCanHandler(hHandle_, eventCode, signal, 0))
      throw KsException(error, "KS_installCanHandler");
  }

  void xmitMsg(const KSCanMsg& msg, int flags = 0) {
    if (Error error = KS_xmitCanMsg(hHandle_, &msg, flags))
      throw KsException(error, "KS_xmitCanMsg");
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_CAN_MODULE

//--------------------------------------------------------------------------------------------------------------
// LIN Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ LIN commands ------
#define KS_LIN_RESET                                    0x00000000
#define KS_LIN_SLEEP                                    0x00000001
#define KS_LIN_WAKEUP                                   0x00000002
#define KS_LIN_SET_BAUD_RATE                            0x00000004
#define KS_LIN_SET_VERSION                              0x00000005
#define KS_LIN_DISABLE_QUEUE_ERROR                      0x00000006
#define KS_LIN_ENABLE_QUEUE_ERROR                       0x00000007

//------ LIN bit rates ------
#define KS_LIN_BAUD_19200                               0x00004b00
#define KS_LIN_BAUD_10417                               0x000028b1
#define KS_LIN_BAUD_9600                                0x00002580
#define KS_LIN_BAUD_4800                                0x000012c0
#define KS_LIN_BAUD_2400                                0x00000960
#define KS_LIN_BAUD_1200                                0x000004b0

//------ LIN Errors ------
#define KS_LIN_ERROR_PHYSICAL                           0x00000001
#define KS_LIN_ERROR_TRANSPORT                          0x00000002
#define KS_LIN_ERROR_BUS_COLLISION                      0x00000003
#define KS_LIN_ERROR_PARITY                             0x00000004
#define KS_LIN_ERROR_CHECKSUM                           0x00000005
#define KS_LIN_ERROR_BREAK_EXPECTED                     0x00000006
#define KS_LIN_ERROR_RESPONSE_TIMEOUT                   0x00000007
#define KS_LIN_ERROR_PID_TIMEOUT                        0x00000009
#define KS_LIN_ERROR_RESPONSE_TOO_SHORT                 0x00000010
#define KS_LIN_ERROR_RECV_QUEUE_FULL                    0x00000011
#define KS_LIN_ERROR_RESPONSE_WITHOUT_HEADER            0x00000012

//------ LIN events ------
#define KS_LIN_ERROR                                    0x00000000
#define KS_LIN_RECV_HEADER                              0x00000001
#define KS_LIN_RECV_RESPONSE                            0x00000002

//------ LIN data structures ------
typedef struct {
  int structSize;
  int linVersion;
  int baudRate;
} KSLinProperties;

typedef struct {
  int structSize;
  KSLinProperties properties;
  int xmitHdrCount;
  int xmitRspCount;
  int recvCount;
  int recvAvail;
  int recvErrorCount;
  int recvNoRspCount;
  int collisionCount;
} KSLinState;

typedef struct {
  int identifier;
  byte parity;
  byte parityOk;
} KSLinHeader;

typedef struct {
  byte data[8];
  int dataLen;
  byte checksum;
  byte checksumOk;
} KSLinResponse;

typedef struct {
  KSLinHeader header;
  KSLinResponse response;
  int64 timestamp;
} KSLinMsg;

//------ LIN Context structures ------
typedef struct {
  int ctxType;
  KSHandle hLin;
} LinUserContext;

typedef struct {
  int ctxType;
  KSHandle hLin;
  int64 timestamp;
  int errorCode;
} LinErrorUserContext;

typedef struct {
  int ctxType;
  KSHandle hLin;
  int64 timestamp;
  KSLinHeader header;
} LinHeaderUserContext;

typedef struct {
  int ctxType;
  KSHandle hLin;
  int64 timestamp;
  KSLinMsg msg;
} LinResponseUserContext;

//------ Common functions ------
Error __stdcall KS_openLin(
                KSHandle* phLin, const char* pDeviceName, int port, const KSLinProperties* pProperties, int flags);
Error __stdcall KS_openLinEx(
                KSHandle* phLin, KSHandle hLinDevice, const KSLinProperties* pProperties, int flags);
Error __stdcall KS_closeLin(
                KSHandle hLin, int flags);
Error __stdcall KS_installLinHandler(
                KSHandle hLin, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_execLinCommand(
                KSHandle hLin, int command, void* pParam, int flags);
Error __stdcall KS_xmitLinHeader(
                KSHandle hLin, int identifier, int flags);
Error __stdcall KS_xmitLinResponse(
                KSHandle hLin, const void* pData, int size, int flags);
Error __stdcall KS_recvLinMsg(
                KSHandle hLin, KSLinMsg* pLinMsg, int flags);
Error __stdcall KS_getLinState(
                KSHandle hLin, KSLinState* pLinState, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_LIN_MODULE

//--------------------------------------------------------------------------------------------------------------
// CANopen Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_CANO_EMERGENCY_REQUEST                  (KSERROR_CATEGORY_CANOPEN+0x00000000)
#define KSERROR_CANO_TOPOLOGY_CHANGE                    (KSERROR_CATEGORY_CANOPEN+0x00020000)
#define KSERROR_CANO_SLAVE_ERROR                        (KSERROR_CATEGORY_CANOPEN+0x00030000)
#define KSERROR_CANO_DATA_INCOMPLETE                    (KSERROR_CATEGORY_CANOPEN+0x00040000)
#define KSERROR_CANO_RECEIVED_SYNC                      (KSERROR_CATEGORY_CANOPEN+0x00050000)
#define KSERROR_CANO_PDO_NOT_VALID                      (KSERROR_CATEGORY_CANOPEN+0x00060000)

//------ DataObj type flags ------
#define KS_CANO_DATAOBJ_PDO_TYPE                        0x00000001
#define KS_CANO_DATAOBJ_SDO_TYPE                        0x00000002
#define KS_CANO_DATAOBJ_READABLE                        0x00010000
#define KS_CANO_DATAOBJ_WRITEABLE                       0x00020000
#define KS_CANO_DATAOBJ_ACTIVE                          0x00040000
#define KS_CANO_DATAOBJ_MAPPABLE                        0x00080000

//------ Functionality flags ------
#define KS_CANO_SIMPLE_BOOTUP_MASTER                    0x00000001
#define KS_CANO_SIMPLE_BOOTUP_SLAVE                     0x00000002
#define KS_CANO_DYNAMIC_CHANNELS                        0x00000004
#define KS_CANO_GROUP_MESSAGING                         0x00000008
#define KS_CANO_LAYER_SETTINGS                          0x00000010

//------ State type ------
#define KS_CANO_STATE_INIT                              0x00000001
#define KS_CANO_STATE_PREOP                             0x00000002
#define KS_CANO_STATE_STOP                              0x00000003
#define KS_CANO_STATE_OP                                0x00000004

//------ CANopen commands ------
#define KS_CANO_XMIT_TIME_STAMP                         0x00000001

//------ common index ------
#define KS_CANO_INDEX_ALL                               0xffffffff
#define KS_CANO_INDEX_INPUT                             0xfffffffe
#define KS_CANO_INDEX_OUTPUT                            0xfffffffd

//------ CANopen events ------
#define KS_CANO_ERROR                                   (CANOPEN_BASE+0x00000000)
#define KS_CANO_DATASET_SIGNAL                          (CANOPEN_BASE+0x00000001)
#define KS_CANO_TOPOLOGY_CHANGE                         (CANOPEN_BASE+0x00000002)

//------ Topology change reasons ------
#define KS_CANO_TOPOLOGY_MASTER_CONNECTED               0x00000000
#define KS_CANO_TOPOLOGY_MASTER_DISCONNECTED            0x00000001
#define KS_CANO_TOPOLOGY_SLAVE_COUNT_CHANGED            0x00000002
#define KS_CANO_TOPOLOGY_SLAVE_ONLINE                   0x00000003
#define KS_CANO_TOPOLOGY_SLAVE_OFFLINE                  0x00000004

//------ Types & Structures ------
typedef struct {
  int ctxType;
  KSHandle hMaster;
  KSHandle hSlave;
  int error;
} CanoErrorUserContext;

typedef struct {
  int ctxType;
  KSHandle hDataSet;
} CanoDataSetUserContext;

typedef struct {
  int ctxType;
  KSHandle hMaster;
  int slavesOnline;
  int slavesCreated;
  int slavesCreatedAndOnline;
  int reason;
  KSHandle hSlave;
} CanoTopologyUserContext;

typedef struct {
  ushort errorCode;
  byte errorRegister;
  byte additionalCode[5];
} KSCanoEmergencyObj;

typedef struct {
  int structSize;
  int connected;
  int slavesOnline;
  int slaveCreated;
  int slavesCreatedAndOnline;
  int masterState;
  int lastError;
} KSCanoMasterState;

typedef struct {
  int structSize;
  int online;
  int created;
  int vendor;
  int product;
  int revision;
  int id;
  int slaveState;
  KSCanoEmergencyObj emergencyObj;
} KSCanoSlaveState;

typedef struct {
  int objType;
  const char* name;
  int dataType;
  int bitLength;
  int subIndex;
  int defaultValue;
  int minValue;
  int maxValue;
} KSCanoDataVarInfo;

typedef struct {
  int objType;
  const char* name;
  int bitLength;
  int index;
  int cobId;
  int mappingIndex;
  int transmissionType;
  int varCount;
  KSCanoDataVarInfo* vars[1];
} KSCanoDataObjInfo;

typedef struct {
  int vendorId;
  int productId;
  int revision;
  const char* name;
  const char* orderCode;
  int rxPdoCount;
  int txPdoCount;
  int granularity;
  int functionality;
  int objCount;
  KSCanoDataObjInfo* objs[1];
} KSCanoSlaveInfo;

//------ Master functions ------
Error __stdcall KS_createCanoMaster(
                KSHandle* phMaster, KSHandle hConnection, const char* libraryPath, const char* topologyFile, int flags);
Error __stdcall KS_closeCanoMaster(
                KSHandle hMaster);
Error __stdcall KS_installCanoMasterHandler(
                KSHandle hMaster, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_queryCanoMasterState(
                KSHandle hMaster, KSCanoMasterState* pMasterState, int flags);
Error __stdcall KS_changeCanoMasterState(
                KSHandle hMaster, int state, int flags);

//------ Slave functions ------
Error __stdcall KS_createCanoSlave(
                KSHandle hMaster, KSHandle* phSlave, int id, int position, uint vendor, uint product, uint revision, int flags);
Error __stdcall KS_createCanoSlaveIndirect(
                KSHandle hMaster, KSHandle* phSlave, const KSCanoSlaveState* pSlaveState, int flags);
Error __stdcall KS_enumCanoSlaves(
                KSHandle hMaster, int index, KSCanoSlaveState* pSlaveState, int flags);
Error __stdcall KS_deleteCanoSlave(
                KSHandle hSlave);
Error __stdcall KS_queryCanoSlaveState(
                KSHandle hSlave, KSCanoSlaveState* pSlaveState, int flags);
Error __stdcall KS_changeCanoSlaveState(
                KSHandle hSlave, int state, int flags);
Error __stdcall KS_queryCanoSlaveInfo(
                KSHandle hSlave, KSCanoSlaveInfo** ppSlaveInfo, int flags);
Error __stdcall KS_queryCanoDataObjInfo(
                KSHandle hSlave, int objIndex, KSCanoDataObjInfo** ppDataObjInfo, int flags);
Error __stdcall KS_queryCanoDataVarInfo(
                KSHandle hSlave, int objIndex, int varIndex, KSCanoDataVarInfo** ppDataVarInfo, int flags);
Error __stdcall KS_enumCanoDataObjInfo(
                KSHandle hSlave, int objIndex, KSCanoDataObjInfo** ppDataObjInfo, int flags);
Error __stdcall KS_enumCanoDataVarInfo(
                KSHandle hSlave, int objIndex, int varIndex, KSCanoDataVarInfo** ppDataVarInfo, int flags);

//------ DataSet related functions ------
Error __stdcall KS_createCanoDataSet(
                KSHandle hMaster, KSHandle* phSet, void** ppAppPtr, void** ppSysPtr, int flags);
Error __stdcall KS_deleteCanoDataSet(
                KSHandle hSet);
Error __stdcall KS_assignCanoDataSet(
                KSHandle hSet, KSHandle hSlave, int pdoIndex, int placement, int flags);
Error __stdcall KS_readCanoDataSet(
                KSHandle hSet, int flags);
Error __stdcall KS_postCanoDataSet(
                KSHandle hSet, int flags);
Error __stdcall KS_installCanoDataSetHandler(
                KSHandle hSet, int eventCode, KSHandle hSignal, int flags);

//------ Data exchange functions ------
Error __stdcall KS_getCanoDataObjAddress(
                KSHandle hSet, KSHandle hSlave, int pdoIndex, int subIndex, void** ppAppPtr, void** ppSysPtr, int* pBitOffset, int* pBitLength, int flags);
Error __stdcall KS_readCanoDataObj(
                KSHandle hSlave, int index, int subIndex, void* pData, int* pSize, int flags);
Error __stdcall KS_postCanoDataObj(
                KSHandle hSlave, int index, int subIndex, const void* pData, int size, int flags);

//------ Auxiliary functions ------
Error __stdcall KS_changeCanoState(
                KSHandle hObject, int state, int flags);
Error __stdcall KS_execCanoCommand(
                KSHandle hObject, int command, void* pParam, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsCanoMaster
//--------------------------------------------------------------------------------------------------------------

struct KsCanoMaster : public KsHandle {
public:
  KsCanoMaster(const KsCanPort& connection, const KsString& libPath = null) {
    if (Error error = KS_createCanoMaster(&hHandle_, connection, libPath, null, 0))
      throw KsException(error, "KsCanoMaster");
  }
};

#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_CANOPEN_MODULE

//--------------------------------------------------------------------------------------------------------------
// Profibus Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error Codes ------
#define KSERROR_PBUS_CONNECTION_CHANGE                  (KSERROR_CATEGORY_PROFIBUS+0x00010000)
#define KSERROR_PBUS_TOPOLOGY_CHANGE                    (KSERROR_CATEGORY_PROFIBUS+0x00020000)
#define KSERROR_PBUS_SLAVE_ERROR                        (KSERROR_CATEGORY_PROFIBUS+0x00030000)
#define KSERROR_PBUS_ALARM_BUFFER_FULL                  (KSERROR_CATEGORY_PROFIBUS+0x00040000)
#define KSERROR_BAD_PARAMETERIZATION_SET                (KSERROR_CATEGORY_PROFIBUS+0x00050000)
#define KSERROR_PBUS_MASTER_ERROR                       (KSERROR_CATEGORY_PROFIBUS+0x00060000)
#define KSERROR_HARDWARE_INTERFACE                      (KSERROR_CATEGORY_PROFIBUS+0x00070000)
#define KSERROR_AUTO_CLEAR                              (KSERROR_CATEGORY_PROFIBUS+0x00080000)

//------ Flags ------
#define KSF_ACCEPT_OFFLINE                              0x00000001
#define KSF_SLOT                                        0x00000002

//------ Master State ------
#define KS_PBUS_OFFLINE                                 0x00000001
#define KS_PBUS_STOP                                    0x00000002
#define KS_PBUS_CLEAR                                   0x00000003
#define KS_PBUS_OPERATE                                 0x00000004

//------ Profibus special indices ------
#define KS_PBUS_INDEX_ALL                               0xffffffff
#define KS_PBUS_INDEX_INPUT                             0xfffffffe
#define KS_PBUS_INDEX_OUTPUT                            0xfffffffd

//------ Profibus commands ------
#define KS_UNFREEZE                                     0x00000001
#define KS_FREEZE                                       0x00000002
#define KS_UNSYNC                                       0x00000004
#define KS_SYNC                                         0x00000008

//------ Profibus command params ------
#define KS_SELECT_GROUP_1                               0x00000100
#define KS_SELECT_GROUP_2                               0x00000200
#define KS_SELECT_GROUP_3                               0x00000400
#define KS_SELECT_GROUP_4                               0x00000800
#define KS_SELECT_GROUP_5                               0x00001000
#define KS_SELECT_GROUP_6                               0x00002000
#define KS_SELECT_GROUP_7                               0x00004000
#define KS_SELECT_GROUP_8                               0x00008000

//------ Profibus bit rates ------
#define KS_PBUS_BAUD_12M                                0x00b71b00
#define KS_PBUS_BAUD_6M                                 0x005b8d80
#define KS_PBUS_BAUD_3M                                 0x002dc6c0
#define KS_PBUS_BAUD_1500K                              0x0016e360
#define KS_PBUS_BAUD_500K                               0x0007a120
#define KS_PBUS_BAUD_187500                             0x0002dc6c
#define KS_PBUS_BAUD_93750                              0x00016e36
#define KS_PBUS_BAUD_45450                              0x0000b18a
#define KS_PBUS_BAUD_31250                              0x00007a12
#define KS_PBUS_BAUD_19200                              0x00004b00
#define KS_PBUS_BAUD_9600                               0x00002580

//------ Profibus events ------
#define KS_PBUS_ERROR                                   (PROFIBUS_BASE+0x00000000)
#define KS_PBUS_DATASET_SIGNAL                          (PROFIBUS_BASE+0x00000001)
#define KS_PBUS_TOPOLOGY_CHANGE                         (PROFIBUS_BASE+0x00000002)
#define KS_PBUS_ALARM                                   (PROFIBUS_BASE+0x00000003)
#define KS_PBUS_DIAGNOSTIC                              (PROFIBUS_BASE+0x00000004)

//------ Profibus alarm types ------
#define KS_PBUS_DIAGNOSTIC_ALARM                        0x00000001
#define KS_PBUS_PROCESS_ALARM                           0x00000002
#define KS_PBUS_PULL_ALARM                              0x00000003
#define KS_PBUS_PLUG_ALARM                              0x00000004
#define KS_PBUS_STATUS_ALARM                            0x00000005
#define KS_PBUS_UPDATE_ALARM                            0x00000006

//------ Profibus topology change reasons ------
#define KS_PBUS_TOPOLOGY_MASTER_CONNECTED               0x00000000
#define KS_PBUS_TOPOLOGY_MASTER_DISCONNECTED            0x00000001
#define KS_PBUS_TOPOLOGY_SLAVE_COUNT_CHANGED            0x00000002
#define KS_PBUS_TOPOLOGY_SLAVE_ONLINE                   0x00000003
#define KS_PBUS_TOPOLOGY_SLAVE_OFFLINE                  0x00000004

//------ Types & Structures ------
typedef struct {
  int ctxType;
  KSHandle hMaster;
  KSHandle hSlave;
  int error;
} PbusErrorUserContext;

typedef struct {
  int ctxType;
  KSHandle hDataSet;
} PbusDataSetUserContext;

typedef struct {
  int ctxType;
  KSHandle hMaster;
  KSHandle hSlave;
  int slavesOnline;
  int slavesConfigured;
  int reason;
} PbusTopologyUserContext;

typedef struct {
  int ctxType;
  KSHandle hMaster;
  KSHandle hSlave;
  int slaveAddress;
  int alarmType;
  int slotNumber;
  int specifier;
  int diagnosticLength;
  int diagnosticData[60];
} PbusAlarmUserContext;

typedef struct {
  int ctxType;
  KSHandle hMaster;
  KSHandle hSlave;
  int slaveAddress;
} PbusDiagnosticUserContext;

typedef struct {
  int structSize;
  int connected;
  int slavesOnline;
  int highestStationAddr;
  int slavesConfigured;
  int masterState;
  int busErrorCounter;
  int timeoutCounter;
  int lastError;
} KSPbusMasterState;

typedef struct {
  int structSize;
  int address;
  int identNumber;
  int configured;
  int assigned;
  int state;
  byte stdStationStatus[6];
  byte extendedDiag[238];
} KSPbusSlaveState;

typedef struct {
  int dataType;
  int subIndex;
  int byteLength;
  const char* name;
} KSPbusDataVarInfo;

typedef struct {
  int slotNumber;
  int varCount;
  int byteLength;
  const char* name;
  KSPbusDataVarInfo* vars[1];
} KSPbusSlotInfo;

typedef struct {
  int address;
  int identNumber;
  int slotCount;
  int byteLength;
  const char* name;
  KSPbusSlotInfo* slots[1];
} KSPbusSlaveInfo;

//------ Master functions ------
Error __stdcall KS_createPbusMaster(
                KSHandle* phMaster, const char* name, const char* libraryPath, const char* topologyFile, int flags);
Error __stdcall KS_closePbusMaster(
                KSHandle hMaster);
Error __stdcall KS_queryPbusMasterState(
                KSHandle hMaster, KSPbusMasterState* pMasterState, int flags);
Error __stdcall KS_changePbusMasterState(
                KSHandle hMaster, int state, int flags);
Error __stdcall KS_enumPbusSlaves(
                KSHandle hMaster, int index, KSPbusSlaveState* pSlaveState, int flags);
Error __stdcall KS_installPbusMasterHandler(
                KSHandle hMaster, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_execPbusMasterCommand(
                KSHandle hMaster, int command, void* pParam, int flags);

//------ Slave functions ------
Error __stdcall KS_createPbusSlave(
                KSHandle hMaster, KSHandle* phSlave, int address, int flags);
Error __stdcall KS_deletePbusSlave(
                KSHandle hSlave);
Error __stdcall KS_queryPbusSlaveState(
                KSHandle hSlave, KSPbusSlaveState* pSlaveState, int flags);
Error __stdcall KS_changePbusSlaveState(
                KSHandle hSlave, int state, int flags);
Error __stdcall KS_queryPbusSlaveInfo(
                KSHandle hSlave, KSPbusSlaveInfo** ppSlaveInfo, int flags);
Error __stdcall KS_queryPbusSlotInfo(
                KSHandle hSlave, int objIndex, KSPbusSlotInfo** ppSlotInfo, int flags);
Error __stdcall KS_queryPbusDataVarInfo(
                KSHandle hSlave, int objIndex, int varIndex, KSPbusDataVarInfo** ppDataVarInfo, int flags);

//------ Data exchange functions ------
Error __stdcall KS_createPbusDataSet(
                KSHandle hMaster, KSHandle* phSet, void** ppAppPtr, void** ppSysPtr, int flags);
Error __stdcall KS_deletePbusDataSet(
                KSHandle hSet);
Error __stdcall KS_assignPbusDataSet(
                KSHandle hSet, KSHandle hSlave, int slot, int index, int placement, int flags);
Error __stdcall KS_readPbusDataSet(
                KSHandle hSet, int flags);
Error __stdcall KS_postPbusDataSet(
                KSHandle hSet, int flags);
Error __stdcall KS_installPbusDataSetHandler(
                KSHandle hSet, int eventCode, KSHandle hSignal, int flags);
Error __stdcall KS_getPbusDataObjAddress(
                KSHandle hSet, KSHandle hSlave, int slot, int index, void** ppAppPtr, void** ppSysPtr, int* pBitOffset, int* pBitLength, int flags);
Error __stdcall KS_readPbusDataObj(
                KSHandle hSlave, int slot, int index, void* pData, int* pSize, int flags);
Error __stdcall KS_postPbusDataObj(
                KSHandle hSlave, int slot, int index, void* pData, int size, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_PROFIBUS_MODULE

//--------------------------------------------------------------------------------------------------------------
// RTL Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Mathematical constants ------
#define KS_PI                                           3.14159265358979323846
#define KS_E                                            2.71828182845904523536

//------ stdlib.h replacements ------
void* __stdcall KSRTL_calloc(
                uint num, uint size);
void __stdcall KSRTL_free(
                void* ptr);
void* __stdcall KSRTL_malloc(
                uint size);

//------ math.h replacements ------
double __stdcall KSRTL_sin(
                double x);
double __stdcall KSRTL_cos(
                double x);
double __stdcall KSRTL_tan(
                double x);
double __stdcall KSRTL_asin(
                double x);
double __stdcall KSRTL_acos(
                double x);
double __stdcall KSRTL_atan(
                double x);
double __stdcall KSRTL_atan2(
                double y, double x);
double __stdcall KSRTL_sinh(
                double x);
double __stdcall KSRTL_cosh(
                double x);
double __stdcall KSRTL_tanh(
                double x);
double __stdcall KSRTL_exp(
                double x);
double __stdcall KSRTL_frexp(
                double x, int* exp);
double __stdcall KSRTL_ldexp(
                double x, int exp);
double __stdcall KSRTL_log(
                double x);
double __stdcall KSRTL_log10(
                double x);
double __stdcall KSRTL_modf(
                double x, double* intpart);
double __stdcall KSRTL_pow(
                double base, double exponent);
double __stdcall KSRTL_sqrt(
                double x);
double __stdcall KSRTL_fabs(
                double x);
double __stdcall KSRTL_ceil(
                double x);
double __stdcall KSRTL_floor(
                double x);
double __stdcall KSRTL_fmod(
                double numerator, double denominator);

//------ string.h replacements ------
const void* __stdcall KSRTL_memchr(
                const void* ptr, int value, uint num);
int __stdcall KSRTL_memcmp(
                const void* ptr1, const void* ptr2, uint num);
void* __stdcall KSRTL_memcpy(
                void* destination, const void* source, uint num);
void* __stdcall KSRTL_memmove(
                void* destination, const void* source, uint num);
void* __stdcall KSRTL_memset(
                void* ptr, int value, uint num);
uint __stdcall KSRTL_strlen(
                const char* str);
int __stdcall KSRTL_strcmp(
                const char* str1, const char* str2);
int __stdcall KSRTL_strncmp(
                const char* str1, const char* str2, uint num);
char* __stdcall KSRTL_strcpy(
                char* destination, const char* source);
char* __stdcall KSRTL_strncpy(
                char* destination, const char* source, uint num);
char* __stdcall KSRTL_strcat(
                char* destination, const char* source);
char* __stdcall KSRTL_strncat(
                char* destination, const char* source, uint num);
const char* __stdcall KSRTL_strchr(
                const char* str, int character);
int __stdcall KSRTL_strcoll(
                const char* str1, const char* str2);
const char* __stdcall KSRTL_strrchr(
                const char* str1, int character);
const char* __stdcall KSRTL_strstr(
                const char* str1, const char* str2);
uint __stdcall KSRTL_strspn(
                const char* str1, const char* str2);
uint __stdcall KSRTL_strcspn(
                const char* str1, const char* str2);
const char* __stdcall KSRTL_strpbrk(
                const char* str1, const char* str2);
char* __stdcall KSRTL_strtok(
                char* str, const char* delimiters);
uint __stdcall KSRTL_strxfrm(
                char* destination, const char* source, uint num);
int __stdcall KSRTL_vsprintf(
                char* buffer, const char* format, void* pArgs);
int __stdcall KSRTL_vsnprintf(
                char* buffer, uint n, const char* format, void* pArgs);

// ATTENTION! If you use the following functions or macros, don't forget
//            to insert the file ..\dev\Kithara.cpp into your project!

int __cdecl KSRTL_sprintf(char* pBuf, const char* pFormat, ...);
int __cdecl KSRTL_snprintf(char* pBuf, uint size, const char* pFormat, ...);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_RTL_MODULE

//--------------------------------------------------------------------------------------------------------------
// Camera Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Flags ------
#define KSF_NO_DISCOVERY                                0x00000800
#define KSF_CAMERA_NO_GENICAM                           0x00001000
#define KSF_CAMERA_BROADCAST_DISCOVERY                  0x00800000

//------ Types & Structures ------
typedef struct {
  int structSize;
  char hardwareId[80];
  char vendor[80];
  char name[80];
  char serialNumber[80];
  char userName[80];
} KSCameraInfo;

typedef struct {
  int structSize;
  int commandTimeout;
  int commandRetries;
  int heartbeatTimeout;
} KSCameraTimeouts;

typedef struct {
  int structSize;
  char macAddress[18];
  uint address;
  uint subnetMask;
  uint gatewayAddress;
} KSCameraForceIp;

typedef struct {
  int structSize;
  char vendor[32];
  char name[32];
  char serialNumber[16];
  char userName[16];
  char macAddress[18];
  int instance;
  char instanceName[256];
} KSCameraInfoGev;

typedef struct {
  int structSize;
  int eventId;
  ulong timestamp;
  ulong blockId;
  int channel;
  int size;
  void* pAppPtr;
  void* pSysPtr;
} KSCameraEvent;

//------ Block type structures ------
typedef struct {
  int blockType;
  void* pAppPtr;
  void* pSysPtr;
  ulong id;
  ulong timestamp;
} KSCameraBlock;

typedef struct {
  int blockType;
  void* pAppPtr;
  void* pSysPtr;
  ulong id;
  ulong timestamp;
  int fieldCount;
  int fieldId;
  uint pixelFormat;
  uint width;
  uint height;
  uint offsetX;
  uint offsetY;
  uint linePadding;
  uint imagePadding;
  uint chunkDataPayloadLength;
  uint chunkLayoutId;
} KSCameraImage;

typedef struct {
  int blockType;
  void* pAppPtr;
  void* pSysPtr;
  ulong id;
  ulong timestamp;
  int fieldCount;
  int fieldId;
  ulong jpegPayloadSize;
  ulong tickFrequency;
  uint dataFormat;
  int isColorSpace;
  uint chunkDataPayloadLength;
  uint chunkLayoutId;
} KSCameraJpeg;

typedef struct {
  int blockType;
  void* pAppPtr;
  void* pSysPtr;
  ulong id;
  ulong timestamp;
  uint chunkDataPayloadLength;
  uint chunkLayoutId;
} KSCameraChunk;

//------ Block types ------
#define KS_CAMERA_BLOCKTYPE_NONE                        0x00000000
#define KS_CAMERA_BLOCKTYPE_IMAGE                       0x00000001
#define KS_CAMERA_BLOCKTYPE_CHUNK                       0x00000002
#define KS_CAMERA_BLOCKTYPE_JPEG                        0x00000003
#define KS_CAMERA_BLOCKTYPE_JPEG2000                    0x00000004

//------ Camera commands ------
#define KS_CAMERA_READREG                               0x00000001
#define KS_CAMERA_READMEM                               0x00000002
#define KS_CAMERA_WRITEREG                              0x00000003
#define KS_CAMERA_SET_TIMEOUTS                          0x00000004
#define KS_CAMERA_GET_TIMEOUTS                          0x00000005
#define KS_CAMERA_GET_XML_FILE                          0x00000006
#define KS_CAMERA_GET_XML_INFO                          0x00000007
#define KS_CAMERA_SET_DISCOVERY_INTERVAL                0x00000008
#define KS_CAMERA_RESIZE_BUFFERS                        0x00000009
#define KS_CAMERA_GET_INFO                              0x0000000a
#define KS_CAMERA_SET_CHUNK_DATA                        0x0000000b
#define KS_CAMERA_SEND_TO_SOURCE_PORT                   0x0000000c
#define KS_CAMERA_FORCEIP                               0x0000000d
#define KS_CAMERA_WRITEMEM                              0x0000000e
#define KS_CAMERA_RECV_EVENT                            0x00000011

//------ Acquisition modes ------
#define KS_CAMERA_CONTINUOUS                            0xffffffff
#define KS_CAMERA_PRESERVE                              0x00000000
#define KS_CAMERA_SINGLE_FRAME                          0x00000001
#define KS_CAMERA_MULTI_FRAME                           0x00000000

//------ Configuration commands ------
#define KS_CAMERA_CONFIG_GET                            0x00000001
#define KS_CAMERA_CONFIG_SET                            0x00000002
#define KS_CAMERA_CONFIG_TYPE                           0x00000003
#define KS_CAMERA_CONFIG_ENUMERATE                      0x00000004
#define KS_CAMERA_CONFIG_MIN                            0x00000005
#define KS_CAMERA_CONFIG_MAX                            0x00000006
#define KS_CAMERA_CONFIG_INC                            0x00000007
#define KS_CAMERA_CONFIG_LENGTH                         0x00010000

//------ Configuration types ------
#define KS_CAMERA_CONFIG_CATEGORY                       0x00000001
#define KS_CAMERA_CONFIG_BOOLEAN                        0x00000002
#define KS_CAMERA_CONFIG_ENUMERATION                    0x00000003
#define KS_CAMERA_CONFIG_INTEGER                        0x00000004
#define KS_CAMERA_CONFIG_COMMAND                        0x00000005
#define KS_CAMERA_CONFIG_FLOAT                          0x00000006
#define KS_CAMERA_CONFIG_STRING                         0x00000007
#define KS_CAMERA_CONFIG_REGISTER                       0x00000008

//------ Receive and error handler types ------
#define KS_CAMERA_IMAGE_RECEIVED                        0x00000001
#define KS_CAMERA_IMAGE_DROPPED                         0x00000002
#define KS_CAMERA_ERROR                                 0x00000003
#define KS_CAMERA_GEV_ATTACHED                          0x00000004
#define KS_CAMERA_GEV_DETACHED                          0x00000005
#define KS_CAMERA_ATTACHED                              0x00000006
#define KS_CAMERA_DETACHED                              0x00000007
#define KS_CAMERA_EVENT                                 0x00000008

//------ Error codes ------
#define KSERROR_CAMERA_COMMAND_ERROR                    (KSERROR_CATEGORY_CAMERA+0x00010000)
#define KSERROR_CAMERA_COMMAND_TIMEOUT                  (KSERROR_CATEGORY_CAMERA+0x00020000)
#define KSERROR_CAMERA_COMMAND_FAILED                   (KSERROR_CATEGORY_CAMERA+0x00030000)
#define KSERROR_CAMERA_COMMAND_BAD_ALIGNMENT            (KSERROR_CATEGORY_CAMERA+0x00040000)
#define KSERROR_CAMERA_RECONNECTED                      (KSERROR_CATEGORY_CAMERA+0x000f0000)
#define KSERROR_CAMERA_STREAM_INVALID_DATA              (KSERROR_CATEGORY_CAMERA+0x00100000)
#define KSERROR_CAMERA_STREAM_NO_BUFFER                 (KSERROR_CATEGORY_CAMERA+0x00110000)
#define KSERROR_CAMERA_STREAM_NOT_TRANSMITTED           (KSERROR_CATEGORY_CAMERA+0x00120000)
#define KSERROR_CAMERA_STREAM_INCOMPLETE_TRANSMISSION   (KSERROR_CATEGORY_CAMERA+0x00130000)
#define KSERROR_CAMERA_STREAM_BUFFER_OVERRUN            (KSERROR_CATEGORY_CAMERA+0x00140000)
#define KSERROR_CAMERA_STREAM_BUFFER_QUEUED             (KSERROR_CATEGORY_CAMERA+0x00150000)
#define KSERROR_GENICAM_ERROR                           (KSERROR_CATEGORY_CAMERA+0x00800000)
#define KSERROR_GENICAM_FEATURE_NOT_IMPLEMENTED         (KSERROR_CATEGORY_CAMERA+0x00810000)
#define KSERROR_GENICAM_FEATURE_NOT_AVAILABLE           (KSERROR_CATEGORY_CAMERA+0x00820000)
#define KSERROR_GENICAM_FEATURE_ACCESS_DENIED           (KSERROR_CATEGORY_CAMERA+0x00830000)
#define KSERROR_GENICAM_FEATURE_NOT_FOUND               (KSERROR_CATEGORY_CAMERA+0x00840000)
#define KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND            (KSERROR_CATEGORY_CAMERA+0x00850000)
#define KSERROR_GENICAM_SUBNODE_NOT_REACHABLE           (KSERROR_CATEGORY_CAMERA+0x00860000)
#define KSERROR_GENICAM_PORT_NOT_REACHABLE              (KSERROR_CATEGORY_CAMERA+0x00870000)
#define KSERROR_GENICAM_FEATURE_NOT_SUPPORTED           (KSERROR_CATEGORY_CAMERA+0x00880000)
#define KSERROR_GENICAM_VALUE_TOO_LARGE                 (KSERROR_CATEGORY_CAMERA+0x00890000)
#define KSERROR_GENICAM_VALUE_TOO_SMALL                 (KSERROR_CATEGORY_CAMERA+0x008a0000)
#define KSERROR_GENICAM_VALUE_BAD_INCREMENT             (KSERROR_CATEGORY_CAMERA+0x008b0000)
#define KSERROR_GENICAM_BAD_REGISTER_DESCRIPTION        (KSERROR_CATEGORY_CAMERA+0x008c0000)

//------ GigE Vision<sup>®</sup> registers ------
#define KS_CAMERA_REGISTER_STREAM_CHANNELS              0x00000904

//------ Camera module config ------
#define KSCONFIG_OVERRIDE_CAMERA_XML_FILE               0x00000001

//------ GigE Vision<sup>®</sup> pixel formats ------
#define KS_CAMERA_PIXEL_MONO_8                          0x01080001
#define KS_CAMERA_PIXEL_MONO_8S                         0x01080002
#define KS_CAMERA_PIXEL_MONO_10                         0x01100003
#define KS_CAMERA_PIXEL_MONO_10_PACKED                  0x010c0004
#define KS_CAMERA_PIXEL_MONO_12                         0x01100005
#define KS_CAMERA_PIXEL_MONO_12_PACKED                  0x010c0006
#define KS_CAMERA_PIXEL_MONO_16                         0x01100007
#define KS_CAMERA_PIXEL_BAYER_GR_8                      0x01080008
#define KS_CAMERA_PIXEL_BAYER_RG_8                      0x01080009
#define KS_CAMERA_PIXEL_BAYER_GB_8                      0x0108000a
#define KS_CAMERA_PIXEL_BAYER_BG_8                      0x0108000b
#define KS_CAMERA_PIXEL_BAYER_GR_10                     0x0110000c
#define KS_CAMERA_PIXEL_BAYER_RG_10                     0x0110000d
#define KS_CAMERA_PIXEL_BAYER_GB_10                     0x0110000e
#define KS_CAMERA_PIXEL_BAYER_BG_10                     0x0110000f
#define KS_CAMERA_PIXEL_BAYER_GR_12                     0x01100010
#define KS_CAMERA_PIXEL_BAYER_RG_12                     0x01100011
#define KS_CAMERA_PIXEL_BAYER_GB_12                     0x01100012
#define KS_CAMERA_PIXEL_BAYER_BG_12                     0x01100013
#define KS_CAMERA_PIXEL_RGB_8                           0x02180014
#define KS_CAMERA_PIXEL_BGR_8                           0x02180015
#define KS_CAMERA_PIXEL_RGBA_8                          0x02200016
#define KS_CAMERA_PIXEL_BGRA_8                          0x02200017
#define KS_CAMERA_PIXEL_RGB_10                          0x02300018
#define KS_CAMERA_PIXEL_BGR_10                          0x02300019
#define KS_CAMERA_PIXEL_RGB_12                          0x0230001a
#define KS_CAMERA_PIXEL_BGR_12                          0x0230001b
#define KS_CAMERA_PIXEL_RGB_10_V1_PACKED                0x0220001c
#define KS_CAMERA_PIXEL_RGB_10_P_32                     0x0220001d
#define KS_CAMERA_PIXEL_YUV_411_8_UYYVYY                0x020c001e
#define KS_CAMERA_PIXEL_YUV_422_8_UYVY                  0x0210001f
#define KS_CAMERA_PIXEL_YUV_8_UYV                       0x02180020
#define KS_CAMERA_PIXEL_RGB_8_PLANAR                    0x02180021
#define KS_CAMERA_PIXEL_RGB_10_PLANAR                   0x02300022
#define KS_CAMERA_PIXEL_RGB_12_PLANAR                   0x02300023
#define KS_CAMERA_PIXEL_RGB_16_PLANAR                   0x02300024
#define KS_CAMERA_PIXEL_MONO_14                         0x01100025
#define KS_CAMERA_PIXEL_BAYER_GR_10_PACKED              0x010c0026
#define KS_CAMERA_PIXEL_BAYER_RG_10_PACKED              0x010c0027
#define KS_CAMERA_PIXEL_BAYER_GB_10_PACKED              0x010c0028
#define KS_CAMERA_PIXEL_BAYER_BG_10_PACKED              0x010c0029
#define KS_CAMERA_PIXEL_BAYER_GR_12_PACKED              0x010c002a
#define KS_CAMERA_PIXEL_BAYER_RG_12_PACKED              0x010c002b
#define KS_CAMERA_PIXEL_BAYER_GB_12_PACKED              0x010c002c
#define KS_CAMERA_PIXEL_BAYER_BG_12_PACKED              0x010c002d
#define KS_CAMERA_PIXEL_BAYER_GR_16                     0x0110002e
#define KS_CAMERA_PIXEL_BAYER_RG_16                     0x0110002f
#define KS_CAMERA_PIXEL_BAYER_GB_16                     0x01100030
#define KS_CAMERA_PIXEL_BAYER_BG_16                     0x01100031
#define KS_CAMERA_PIXEL_YUV_422_8                       0x020c0032
#define KS_CAMERA_PIXEL_RGB_16                          0x02300033
#define KS_CAMERA_PIXEL_RGB_12_V1_PACKED                0x02240034
#define KS_CAMERA_PIXEL_RGB_565_P                       0x02100035
#define KS_CAMERA_PIXEL_BGR_565_P                       0x02100036
#define KS_CAMERA_PIXEL_MONO_1P                         0x01010037
#define KS_CAMERA_PIXEL_MONO_2P                         0x01020038
#define KS_CAMERA_PIXEL_MONO_4P                         0x01040039
#define KS_CAMERA_PIXEL_YCBCR_8_CBYCR                   0x0218003a
#define KS_CAMERA_PIXEL_YCBCR_422_8                     0x0210003b
#define KS_CAMERA_PIXEL_YCBCR_411_8_CBYYCRYY            0x020c003c
#define KS_CAMERA_PIXEL_YCBCR_601_8_CBYCR               0x0218003d
#define KS_CAMERA_PIXEL_YCBCR_601_422_8                 0x0210003e
#define KS_CAMERA_PIXEL_YCBCR_601_411_8_CBYYCRYY        0x020c003f
#define KS_CAMERA_PIXEL_YCBCR_709_8_CBYCR               0x02180040
#define KS_CAMERA_PIXEL_YCBCR_709_422_8                 0x02100041
#define KS_CAMERA_PIXEL_YCBCR_709_411_8_CBYYCRYY        0x020c0042
#define KS_CAMERA_PIXEL_YCBCR_422_8_CBYCRY              0x02100043
#define KS_CAMERA_PIXEL_YCBCR_601_422_8_CBYCRY          0x02100044
#define KS_CAMERA_PIXEL_YCBCR_709_422_8_CBYCRY          0x02100045

//------ Context structures ------
typedef struct {
  int ctxType;
  KSHandle hStream;
  KSHandle hCamera;
} KSCameraRecvImageContext;

typedef struct {
  int ctxType;
  KSHandle hStream;
  KSHandle hCamera;
  ulong imageId;
} KSCameraDroppedImageContext;

typedef struct {
  int ctxType;
  KSHandle hCamera;
  Error error;
} KSCameraErrorContext;

typedef struct {
  int ctxType;
  KSHandle hController;
  KSCameraInfo cameraInfo;
} KSCameraAttachContext;

typedef struct {
  int ctxType;
  KSHandle hAdapter;
  KSCameraInfoGev cameraInfo;
} KSCameraGevAttachContext;

typedef struct {
  int ctxType;
  KSHandle hCamera;
} KSCameraEventContext;

//------ Basic functions ------
Error __stdcall KS_enumCameras(
                KSHandle hObject, int index, KSCameraInfo* pCameraInfo, int flags);
Error __stdcall KS_execCameraCommand(
                KSHandle hObject, int command, uint address, void* pData, int* pLength, int flags);
Error __stdcall KS_installCameraHandler(
                KSHandle hHandle, int eventType, KSHandle hSignal, int flags);

//------ Camera connection functions ------
Error __stdcall KS_openCamera(
                KSHandle* phCamera, KSHandle hObject, const char* pHardwareId, int flags);
Error __stdcall KS_openCameraEx(
                KSHandle* phCamera, KSHandle hObject, const KSCameraInfo* pCameraInfo, int flags);
Error __stdcall KS_closeCamera(
                KSHandle hCamera, int flags);
Error __stdcall KS_readCameraMem(
                KSHandle hCamera, int64ref address, void* pData, int length, int flags);
Error __stdcall KS_writeCameraMem(
                KSHandle hCamera, int64ref address, const void* pData, int length, int flags);
Error __stdcall KS_configCamera(
                KSHandle hCamera, int configCommand, const char* pName, int index, void* pData, int length, int flags);
Error __stdcall KS_startCameraAcquisition(
                KSHandle hCamera, int acquisitionMode, int flags);
Error __stdcall KS_stopCameraAcquisition(
                KSHandle hCamera, int flags);

//------ Stream functions ------
Error __stdcall KS_createCameraStream(
                KSHandle* phStream, KSHandle hCamera, int channelNumber, int numBuffers, int bufferSize, int flags);
Error __stdcall KS_closeCameraStream(
                KSHandle hStream, int flags);
Error __stdcall KS_recvCameraImage(
                KSHandle hStream, void** ppData, KSCameraBlock** ppBlockInfo, int flags);
Error __stdcall KS_releaseCameraImage(
                KSHandle hStream, void* pData, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

//--------------------------------------------------------------------------------------------------------------
// KsCameraEnumeration
//--------------------------------------------------------------------------------------------------------------

struct KsCameraEnumeration : public _KsEnumeration<KSCameraInfo> {
public:
  KsCameraEnumeration(KsHandle& controller) : hHandle_(controller) {
    refresh();
  }

  void refresh() {
    clear();
    KSCameraInfo cameraInfo;
    cameraInfo.structSize = sizeof(KSCameraInfo);
    for (int i = 0; ; ++i) {
      Error error = KS_enumCameras(hHandle_, i, &cameraInfo, 0);
      if (KSERROR_CODE(error) == KSERROR_DEVICE_NOT_FOUND)
        break;
      if (error)
        throw KsException(error, "KS_enumCameras");
      add(cameraInfo);
    }
  }

private:
  KSHandle hHandle_;
};

//--------------------------------------------------------------------------------------------------------------
// KsCameraInterface
//--------------------------------------------------------------------------------------------------------------

struct KsCameraInterface {
public:
  KsCameraInterface(KsHandle& controller) : hHandle_(controller) {}

  void forceIp(char macAddress[18], uint ipAddress, uint subnetMask, uint gateway = 0) {
    KSCameraForceIp ksForceIp;
    ksForceIp.structSize = sizeof(KSCameraForceIp);
    ksForceIp.address = ipAddress;
    ksForceIp.gatewayAddress = gateway;
    ksForceIp.subnetMask = subnetMask;
    for (int i = 0; i < sizeof(ksForceIp.macAddress); ++i)
      ksForceIp.macAddress[i] = macAddress[i];

    if (Error error = KS_execCameraCommand(hHandle_, KS_CAMERA_FORCEIP, 0, &ksForceIp, null, 0))
      throw KsException(error, "KS_execCameracommand(KS_CAMERA_FORCEIP)");
  }

  void setDiscoveryInterval(int interval) {
    if (Error error = KS_execCameraCommand(hHandle_, KS_CAMERA_SET_DISCOVERY_INTERVAL, 0, &interval, null, 0))
      throw KsException(error, "KS_execCameracommand(KS_CAMERA_SET_DISCOVERY_INTERVAL)");
  }

  void installAttachHandler(KsSignal& signal) {
    if (Error error = KS_installCameraHandler(hHandle_, KS_CAMERA_ATTACHED, signal, 0))
      throw KsException(error, "KS_installCameraHandler(KS_CAMERA_ATTACHED)");
  }

  void installDetachHandler(KsSignal& signal) {
    if (Error error = KS_installCameraHandler(hHandle_, KS_CAMERA_DETACHED, signal, 0))
      throw KsException(error, "KS_installCameraHandler(KS_CAMERA_DETACHED)");
  }

private:
  KSHandle hHandle_;
};

//--------------------------------------------------------------------------------------------------------------
// KsCamera
//--------------------------------------------------------------------------------------------------------------

class KsCamera : public KsHandle {
public:
  KsCamera() {}
  KsCamera(KsNicAdapter& controller, const KsString& cameraIdentifier, int flags = 0) {
    open(controller, cameraIdentifier, flags);
  }
  KsCamera(KsXhciController& controller, const KsString& cameraIdentifier, int flags = 0) {
    open(controller, cameraIdentifier, flags);
  }
  KsCamera(KsXhciController& controller, const KSCameraInfo& cameraInfo, int flags = 0) {
    open(controller, cameraInfo, flags);
  }
  KsCamera(KsNicAdapter& controller, const KSCameraInfo& cameraInfo, int flags = 0) {
    open(controller, cameraInfo, flags);
  }

  void open(KsNicAdapter& controller, const KsString& cameraIdentifier, int flags = 0) {
    open(static_cast<KsHandle&>(controller), cameraIdentifier, flags);
  }
  void open(KsXhciController& controller, const KsString& cameraIdentifier, int flags = 0) {
    open(static_cast<KsHandle&>(controller), cameraIdentifier, flags);
  }
  void open(KsXhciController& controller, const KSCameraInfo& cameraInfo, int flags = 0) {
    open(static_cast<KsHandle&>(controller), cameraInfo, flags);
  }
  void open(KsNicAdapter& controller, const KSCameraInfo& cameraInfo, int flags = 0) {
    open(static_cast<KsHandle&>(controller), cameraInfo, flags);
  }

  void readMemory(ulong address, void* pBuffer, int length) const {
    if (Error error = KS_readCameraMem(hHandle_, address, pBuffer, length, 0))
      throw KsException(error, "KS_readCamera()");
  }
  void writeMemory(ulong address, const void* pBuffer, int length) {
    if (Error error = KS_writeCameraMem(hHandle_, address, pBuffer, length, 0))
      throw KsException(error, "KS_writeCamera()");
  }

  int readRegister(int address) const {
    int value = 0;
    if (Error error = KS_execCameraCommand(hHandle_, KS_CAMERA_READREG, address, &value, 0, 0))
      throw KsException(error, "KS_execCameraCommand(KS_CAMERA_READREG)");
    return value;
  }
  void writeRegister(int address, int value) {
    if (Error error = KS_execCameraCommand(hHandle_, KS_CAMERA_WRITEREG, address, &value, 0, 0))
      throw KsException(error, "KS_execCameraCommand(KS_CAMERA_WRITEREG)");
  }

  KsString getXmlFileName() const {
    _KsAutoPtr<char> pBuffer = new char[256];
    int length = 0;
    if (Error error = KS_execCameraCommand(hHandle_, KS_CAMERA_GET_XML_INFO, 0, pBuffer, &length, 0))
      throw KsException(error, "KS_execCameraCommand(KS_CAMERA_GET_XML_INFO)");
    return KsString(pBuffer);
  }
  int getXmlFileSize() const {
    _KsAutoPtr<char> pBuffer = new char[256];
    int length = 0;
    if (Error error = KS_execCameraCommand(hHandle_, KS_CAMERA_GET_XML_INFO, 0, pBuffer, &length, 0))
      throw KsException(error, "KS_execCameraCommand(KS_CAMERA_GET_XML_INFO)");
    return length;
  }

  void getXmlFile(void* pBuffer, int length) const {
    if (Error error = KS_execCameraCommand(hHandle_, KS_CAMERA_GET_XML_FILE, 0, pBuffer, &length, 0))
      throw KsException(error, "KS_execCameraCommand(KS_CAMERA_GET_XML_FILE)");
  }
  KSCameraInfo info() const {
    KSCameraInfo info = { sizeof(KSCameraInfo) };
    if (Error error = KS_execCameraCommand(hHandle_, KS_CAMERA_GET_INFO, 0, &info, 0, 0))
      throw KsException(error, "KS_execCameraCommand(KS_CAMERA_GET_INFO)");
    return info;
  }

  void startAcquisition(int acquisitionMode, int flags = 0) {
    if (Error error = KS_startCameraAcquisition(hHandle_, acquisitionMode, flags))
      throw KsException(error, "KS_startCameraAcquisition");
  }
  void stopAcquisition(int flags = 0) {
    if (Error error = KS_stopCameraAcquisition(hHandle_, flags))
      throw KsException(error, "KS_stopCameraAcquisition");
  }

private:
  void open(KsHandle& controller, const KsString& cameraIdentifier, int flags = 0) {
    if (Error error = KS_openCamera(&hHandle_, controller, cameraIdentifier, flags))
      throw KsException(error, "KS_openCamera");
  }
  void open(KsHandle& controller, const KSCameraInfo& cameraInfo, int flags = 0) {
    if (Error error = KS_openCameraEx(&hHandle_, controller, &cameraInfo, flags))
      throw KsException(error, "KS_openCameraEx");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsCameraStream
//--------------------------------------------------------------------------------------------------------------

class KsCameraStream : public KsHandle {
public:
  KsCameraStream(const KsCamera& camera, int bufferCount) {
    if (Error error = KS_createCameraStream(&hHandle_, camera, 0, bufferCount, 0, 0))
      throw KsException(error, "KS_createCameraStream");
  }

  KsCameraStream(const KsCamera& camera, int channelNumber, int bufferCount, int bufferSize = 0) {
    if (Error error = KS_createCameraStream(&hHandle_, camera, channelNumber, bufferCount, bufferSize, 0))
      throw KsException(error, "KS_createCameraStream");
  }

  ~KsCameraStream() {
    KS_closeCameraStream(hHandle_, 0);
    hHandle_ = KS_INVALID_HANDLE;
  }

  void installReceiveHandler(KsSignal& signal) {
    if (Error error = KS_installCameraHandler(hHandle_, KS_CAMERA_IMAGE_RECEIVED, signal, 0))
      throw KsException(error, "KS_installCameraHandler(KS_CAMERA_IMAGE_RECEIVED)");
  }

  void installDropHandler(KsSignal& signal) {
    if (Error error = KS_installCameraHandler(hHandle_, KS_CAMERA_IMAGE_DROPPED, signal, 0))
      throw KsException(error, "KS_installCameraHandler(KS_CAMERA_IMAGE_DROPPED)");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsGenicamFeature
//--------------------------------------------------------------------------------------------------------------

struct KsGenicamFeature {
public:
  KsGenicamFeature(KSHandle hCamera, const KsString& name) : hCamera_(hCamera), name_(name) {}

  bool isImplemented() const {
    // TODO
    return true;
  }

  bool isAvailable() const {
    // TODO
    return true;
  }

  bool isReadable() const {
    // TODO
    return true;
  }

  bool isWriteable() const {
    // TODO
    return true;
  }

  const KsString& name() const {
    return name_;
  }

protected:
  Error getString(KsString* pString, int command, int index = 0) const {
    int length;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_LENGTH | command, name_, index, &length, 0, 0))
      return error;
    _KsAutoPtr<char> pBuffer = new char[length];
    if (Error error = KS_configCamera(hCamera_, command, name_, index, pBuffer, length, 0))
      return error;
    *pString = KsString(pBuffer, length);
    return KS_OK;
  }

  KSHandle hCamera_;
  KsString name_;
};

//--------------------------------------------------------------------------------------------------------------
// KsGenicamInteger
//--------------------------------------------------------------------------------------------------------------

struct KsGenicamInteger : public KsGenicamFeature {
  KsGenicamInteger(KSHandle hCamera, const KsString& name) : KsGenicamFeature(hCamera, name) {
    int type;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_TYPE, name_, 0, &type, 0, 0))
      throw KsException(error, "KsGenicamInteger");
    if (type != KS_CAMERA_CONFIG_INTEGER)
      throw KsException(KSERROR_TYPE_MISMATCH, "KsGenicamInteger");
  }

  int64 value() const {
    int64 value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_GET, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamInteger");
    return value;
  }

  void setValue(int64 value) const {
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_SET, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamInteger");
  }

  int64 min() const {
    int64 value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_MIN, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamInteger");
    return value;
  }

  int64 max() const {
    int64 value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_MAX, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamInteger");
    return value;
  }

  int64 inc() const {
    int64 value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_INC, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamInteger");
    return value;
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsGenicamFloat
//--------------------------------------------------------------------------------------------------------------

struct KsGenicamFloat : public KsGenicamFeature {
  KsGenicamFloat(KSHandle hCamera, const KsString& name) : KsGenicamFeature(hCamera, name) {
    int type;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_TYPE, name_, 0, &type, 0, 0))
      throw KsException(error, "KsGenicamFloat");
    if (type != KS_CAMERA_CONFIG_FLOAT)
      throw KsException(KSERROR_TYPE_MISMATCH, "KsGenicamFloat");
  }

  double value() const {
    double value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_GET, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamFloat");
    return value;
  }

  void setValue(double value) const {
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_SET, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamFloat");
  }

  double min() const {
    double value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_MIN, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamFloat");
    return value;
  }

  double max() const {
    double value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_MAX, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamFloat");
    return value;
  }

  double inc() const {
    double value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_INC, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamFloat");
    return value;
  }

  bool hasInc() const {
    return !(inc() == 0.0);
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsGenicamBoolean
//--------------------------------------------------------------------------------------------------------------

struct KsGenicamBoolean : public KsGenicamFeature {
  KsGenicamBoolean(KSHandle hCamera, const KsString& name) : KsGenicamFeature(hCamera, name) {
    int type;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_TYPE, name_, 0, &type, 0, 0))
      throw KsException(error, "KsGenicamBoolean");
    if (type != KS_CAMERA_CONFIG_BOOLEAN)
      throw KsException(KSERROR_TYPE_MISMATCH, "KsGenicamBoolean");
  }

  bool value() const {
    int value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_GET, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamBoolean");
    return value != 0;
  }

  void setValue(bool value) const {
    int value2 = value ? 1 : 0;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_SET, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamBoolean");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsGenicamString
//--------------------------------------------------------------------------------------------------------------

struct KsGenicamString : public KsGenicamFeature {
  KsGenicamString(KSHandle hCamera, const KsString& name) : KsGenicamFeature(hCamera, name) {
    int type;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_TYPE, name_, 0, &type, 0, 0))
      throw KsException(error, "KsGenicamString");
    if (type != KS_CAMERA_CONFIG_STRING)
      throw KsException(KSERROR_TYPE_MISMATCH, "KsGenicamString");
  }

  KsString value() const {
    KsString result;
    if (Error error = getString(&result, KS_CAMERA_CONFIG_GET))
      throw KsException(error, "KsGenicamString");
    return result;
  }

  void setValue(const KsString& value) const {
    char* pBuffer = const_cast<char*>(static_cast<const char*>(value));
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_SET, name_, 0, pBuffer, value.length(), 0))
      throw KsException(error, "KsGenicamString");
  }
};

//--------------------------------------------------------------------------------------------------------------
// KsGenicamCategory
//--------------------------------------------------------------------------------------------------------------

struct KsGenicamCategory : public KsGenicamFeature, public _KsEnumeration<KsString> {
  KsGenicamCategory(KSHandle hCamera, const KsString& name) : KsGenicamFeature(hCamera, name) {
    int type;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_TYPE, name_, 0, &type, 0, 0))
      throw KsException(error, "KsGenicamCategory");
    if (type != KS_CAMERA_CONFIG_CATEGORY)
      throw KsException(KSERROR_TYPE_MISMATCH, "KsGenicamCategory");

    for (int i = 0;; ++i) {
      KsString result;
      Error error = getString(&result, KS_CAMERA_CONFIG_ENUMERATE, i);
      if (KSERROR_CODE(error) == KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND)
        break;
      if (error)
        throw KsException(error, "KsGenicamCategory");
      add(result);
    }
  }

  // enumeration available through operator [] and length
};

//--------------------------------------------------------------------------------------------------------------
// KsGenicamEnumeration
//--------------------------------------------------------------------------------------------------------------

struct KsGenicamEnumeration : public KsGenicamFeature, public _KsEnumeration<KsString> {
  KsGenicamEnumeration(KSHandle hCamera, const KsString& name) : KsGenicamFeature(hCamera, name) {
    int type;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_TYPE, name_, 0, &type, 0, 0))
      throw KsException(error, "KsGenicamEnumeration");
    if (type != KS_CAMERA_CONFIG_ENUMERATION)
      throw KsException(KSERROR_TYPE_MISMATCH, "KsGenicamEnumeration");

    for (int i = 0;; ++i) {
      KsString result;
      Error error = getString(&result, KS_CAMERA_CONFIG_ENUMERATE, i);
      if (KSERROR_CODE(error) == KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND)
        break;
      if (KSERROR_CODE(error) == KSERROR_GENICAM_FEATURE_NOT_IMPLEMENTED)
        continue;
      if (error != KS_OK && KSERROR_CODE(error) != KSERROR_GENICAM_FEATURE_NOT_AVAILABLE)
        throw KsException(error, "KsGenicamEnumeration");
      add(result);
    }
  }

  KsString value() const {
    KsString result;
    if (Error error = getString(&result, KS_CAMERA_CONFIG_GET))
      throw KsException(error, "KsGenicamEnumeration");
    return result;
  }

  void setValue(const KsString& value) const {
    char* pBuffer = const_cast<char*>(static_cast<const char*>(value));
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_SET, name_, 0, pBuffer, value.length(), 0))
      throw KsException(error, "KsGenicamEnumeration");
  }

  // enumeration available through operator [] and length
};

//--------------------------------------------------------------------------------------------------------------
// KsGenicamCommand
//--------------------------------------------------------------------------------------------------------------

struct KsGenicamCommand : public KsGenicamFeature {
  KsGenicamCommand(KSHandle hCamera, const KsString& name) : KsGenicamFeature(hCamera, name) {
    int type;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_TYPE, name_, 0, &type, 0, 0))
      throw KsException(error, "KsGenicamCommand");
    if (type != KS_CAMERA_CONFIG_COMMAND)
      throw KsException(KSERROR_TYPE_MISMATCH, "KsGenicamCommand");
  }

  void execute() {
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_SET, name_, 0, 0, 0, 0))
      throw KsException(error, "KsGenicamCommand");
  }

  bool isDone() {
    int value;
    if (Error error = KS_configCamera(hCamera_, KS_CAMERA_CONFIG_GET, name_, 0, &value, 0, 0))
      throw KsException(error, "KsGenicamCommand");
    return value != 0;
  }
};
#endif // defined(__cplusplus) && defined(KS_ENABLE_CLASS_LIBRARY) && !defined(KS_DO_NOT_USE_CLASS_LIBRARY)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_CAMERA_MODULE

//--------------------------------------------------------------------------------------------------------------
// PLC Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Error codes ------
#define KSERROR_PLC_SCAN_ERROR                          (KSERROR_CATEGORY_PLC+0x00000000)
#define KSERROR_PLC_TYPE_ERROR                          (KSERROR_CATEGORY_PLC+0x00010000)
#define KSERROR_PLC_BACKEND                             (KSERROR_CATEGORY_PLC+0x00020000)
#define KSERROR_PLC_LINKER_ERROR                        (KSERROR_CATEGORY_PLC+0x00030000)
#define KSERROR_PLC_INVALID_CONFIG_NUMBER               (KSERROR_CATEGORY_PLC+0x00040000)
#define KSERROR_PLC_INVALID_RESOURCE_NUMBER             (KSERROR_CATEGORY_PLC+0x00060000)
#define KSERROR_PLC_INVALID_TASK_NUMBER                 (KSERROR_CATEGORY_PLC+0x00070000)

//------ PLC languages ------
#define KS_PLC_BYTECODE                                 0x00000000
#define KS_PLC_INSTRUCTION_LIST                         0x00000001
#define KS_PLC_STRUCTURED_TEXT                          0x00000002

//------ Flags ------
#define KSF_USE_KITHARA_PRIORITY                        0x00002000
#define KSF_32BIT                                       0x00000400
#define KSF_64BIT                                       0x00000800
#define KSF_DEBUG                                       0x10000000
#define KSF_PLC_START                                   0x00004000
#define KSF_PLC_STOP                                    0x00008000
#define KSF_IGNORE_CASE                                 0x00000010

//------ PLC commands ------
#define KS_PLC_ABORT_COMPILE                            0x00000001

//------ Error levels ------
#define KS_ERROR_LEVEL_IGNORE                           0x00000000
#define KS_ERROR_LEVEL_WARNING                          0x00000001
#define KS_ERROR_LEVEL_ERROR                            0x00000002
#define KS_ERROR_LEVEL_FATAL                            0x00000003

//------ PLC compiler errors ------
#define KS_PLCERROR_NONE                                0x00000000
#define KS_PLCERROR_SYNTAX                              0x00000001
#define KS_PLCERROR_EXPECTED_ID                         0x00000002
#define KS_PLCERROR_EXPECTED_RESCOURCE_ID               0x00000003
#define KS_PLCERROR_EXPECTED_PROG_ID                    0x00000004
#define KS_PLCERROR_EXPECTED_FB_ID                      0x00000005
#define KS_PLCERROR_EXPECTED_FB_INSTANCE                0x00000006
#define KS_PLCERROR_EXPECTED_FUNCTION_ID                0x00000007
#define KS_PLCERROR_EXPECTED_SINGLE_ID                  0x00000008
#define KS_PLCERROR_EXPECTED_INTEGER                    0x00000009
#define KS_PLCERROR_EXPECTED_STRUCT                     0x0000000a
#define KS_PLCERROR_EXPECTED_STMT                       0x0000000b
#define KS_PLCERROR_EXPECTED_OPERAND                    0x0000000c
#define KS_PLCERROR_EXPECTED_VAR                        0x0000000d
#define KS_PLCERROR_EXPECTED_TYPE                       0x0000000e
#define KS_PLCERROR_EXPECTED_STRING                     0x0000000f
#define KS_PLCERROR_EXPECTED_VALUE                      0x00000010
#define KS_PLCERROR_EXPECTED_NUMERAL                    0x00000011
#define KS_PLCERROR_EXPECTED_BOOL                       0x00000012
#define KS_PLCERROR_EXPECTED_CONST                      0x00000013
#define KS_PLCERROR_EXPECTED_READ_ONLY                  0x00000014
#define KS_PLCERROR_EXPECTED_VAR_INPUT                  0x00000015
#define KS_PLCERROR_EXPECTED_VAR_OUTPUT                 0x00000016
#define KS_PLCERROR_EXPECTED_SIMPLE_TYPE                0x00000017
#define KS_PLCERROR_EXPECTED_TIME                       0x00000018
#define KS_PLCERROR_EXPECTED_GLOBAL                     0x00000019
#define KS_PLCERROR_EXPECTED_LABEL                      0x0000001a
#define KS_PLCERROR_EXPECTED_INCOMPLETE_DECL            0x0000001b
#define KS_PLCERROR_EXPECTED_VAR_CONFIG                 0x0000001c
#define KS_PLCERROR_ID_DEFINED                          0x0000001d
#define KS_PLCERROR_ID_UNKNOWN                          0x0000001e
#define KS_PLCERROR_ID_CONST                            0x0000001f
#define KS_PLCERROR_ID_TOO_MANY                         0x00000020
#define KS_PLCERROR_UNKNOWN_LOCATION                    0x00000021
#define KS_PLCERROR_UNKNOWN_SIZE                        0x00000022
#define KS_PLCERROR_TYPE_MISMATCH                       0x00000023
#define KS_PLCERROR_INDEX_VIOLATION                     0x00000024
#define KS_PLCERROR_FORMAT_MISMATCH                     0x00000025
#define KS_PLCERROR_OVERFLOW                            0x00000026
#define KS_PLCERROR_DATA_LOSS                           0x00000027
#define KS_PLCERROR_NOT_SUPPORTED                       0x00000028
#define KS_PLCERROR_NEGATIVE_SIZE                       0x00000029
#define KS_PLCERROR_MISSING_ARRAY_ELEM_TYPE             0x0000002a
#define KS_PLCERROR_MISSING_RETURN_VALUE                0x0000002b
#define KS_PLCERROR_BAD_VAR_USE                         0x0000002c
#define KS_PLCERROR_BAD_DIRECT_VAR_INOUT                0x0000002d
#define KS_PLCERROR_BAD_LABEL                           0x0000002e
#define KS_PLCERROR_BAD_SUBRANGE                        0x0000002f
#define KS_PLCERROR_BAD_DATETIME                        0x00000030
#define KS_PLCERROR_BAD_ARRAY_SIZE                      0x00000031
#define KS_PLCERROR_BAD_CASE                            0x00000032
#define KS_PLCERROR_DIV_BY_ZERO                         0x00000033
#define KS_PLCERROR_AMBIGUOUS                           0x00000034
#define KS_PLCERROR_INTERNAL                            0x00000035
#define KS_PLCERROR_PARAMETER_MISMATCH                  0x00000036
#define KS_PLCERROR_PARAMETER_TOO_MANY                  0x00000037
#define KS_PLCERROR_PARAMETER_FEW                       0x00000038
#define KS_PLCERROR_INIT_MANY                           0x00000039
#define KS_PLCERROR_BAD_ACCESS                          0x0000003a
#define KS_PLCERROR_BAD_CONFIG                          0x0000003b
#define KS_PLCERROR_READ_ONLY                           0x0000003c
#define KS_PLCERROR_WRITE_ONLY                          0x0000003d
#define KS_PLCERROR_BAD_EXIT                            0x0000003e
#define KS_PLCERROR_BAD_RECURSION                       0x0000003f
#define KS_PLCERROR_BAD_PRIORITY                        0x00000040
#define KS_PLCERROR_BAD_FB_OPERATOR                     0x00000041
#define KS_PLCERROR_BAD_INIT                            0x00000042
#define KS_PLCERROR_BAD_CONTINUE                        0x00000043
#define KS_PLCERROR_DUPLICATE_CASE                      0x00000044
#define KS_PLCERROR_UNEXPECTED_TYPE                     0x00000045
#define KS_PLCERROR_CASE_MISSING                        0x00000046
#define KS_PLCERROR_DEAD_INSTRUCTION                    0x00000047
#define KS_PLCERROR_FOLD_EXPRESSION                     0x00000048
#define KS_PLCERROR_NOT_IMPLEMENTED                     0x00000049
#define KS_PLCERROR_BAD_CONNECTED_PARAM_VAR             0x0000004a
#define KS_PLCERROR_PARAMETER_MIX                       0x0000004b
#define KS_PLCERROR_EXPECTED_PROGRAM_DECL               0x0000004c
#define KS_PLCERROR_EXPECTED_INTERFACE                  0x0000004d
#define KS_PLCERROR_EXPECTED_METHOD                     0x0000004e
#define KS_PLCERROR_ABSTRACT                            0x0000004f
#define KS_PLCERROR_FINAL                               0x00000050
#define KS_PLCERROR_MISSING_ABSTRACT_METHOD             0x00000051
#define KS_PLCERROR_ABSTRACT_INCOMPLETE                 0x00000052
#define KS_PLCERROR_BAD_ACCESS_MODIFIER                 0x00000053
#define KS_PLCERROR_BAD_METHOD_ACCESS                   0x00000054
#define KS_PLCERROR_ID_INPUT                            0x00000055
#define KS_PLCERROR_BAD_THIS                            0x00000056
#define KS_PLCERROR_MISSING_MODULE                      0x00000057
#define KS_PLCERROR_EXPECTED_DNV                        0x00000058
#define KS_PLCERROR_EXPECTED_DIRECT_VAR                 0x00000059
#define KS_PLCERROR_BAD_VAR_DECL                        0x0000005a
#define KS_PLCERROR_HAS_ABSTRACT_BODY                   0x0000005b
#define KS_PLCERROR_BAD_MEMBER_ACCESS                   0x0000005c
#define KS_PLCERROR_EXPECTED_FORMAL_CALL                0x0000005d
#define KS_PLCERROR_EXPECTED_INTERFACE_VAR              0x0000005e

//------ PLC runtime errors ------
#define KS_PLCERROR_DIVIDED_BY_ZERO                     0x0000012c
#define KS_PLCERROR_ARRAY_INDEX_VIOLATION               0x0000012d
#define KS_PLCERROR_MEMORY_ACCESS_VIOLATION             0x0000012e
#define KS_PLCERROR_IO_VARIABLE_ACCESS                  0x0000012f
#define KS_PLCERROR_BAD_VAR_EXTERNAL                    0x00000130
#define KS_PLCERROR_BAD_SUB_RANGE                       0x00000131

//------ PLC events ------
#define KS_PLC_COMPILE_ERROR                            (PLC_BASE+0x00000001)
#define KS_PLC_COMPILE_FINISHED                         (PLC_BASE+0x00000002)
#define KS_PLC_RUNTIME_ERROR                            (PLC_BASE+0x00000003)
#define KS_PLC_CONFIG_RESSOURCE                         (PLC_BASE+0x00000004)

//------ Context structures ------
typedef struct {
  int ctxType;
  KSHandle hCompiler;
  int line;
  int column;
  int errorCode;
  int errorLevel;
  int flags;
  char pMsg[200];
} PlcErrorMsgContext;

typedef struct {
  int ctxType;
  KSHandle hCompiler;
  int ksError;
  int id;
} PlcCompileContext;

typedef struct {
  int ctxType;
  KSHandle hPlc;
  int line;
  int column;
  int errorCode;
  int errorLevel;
  int flags;
  int ksError;
} PlcRuntimeErrorContext;

typedef struct {
  int ctxType;
  KSHandle hPlc;
  int configIndex;
  int resourceIndex;
  int flags;
} PlcConfigContext;

//------ PLC compiler functions ------
Error __stdcall KS_createPlcCompiler(
                KSHandle* phCompiler, int flags);
Error __stdcall KS_closePlcCompiler(
                KSHandle hCompiler, int flags);
Error __stdcall KS_compilePlcFile(
                KSHandle hCompiler, const char* sourcePath, int sourceLanguage, const char* destinationPath, int flags);
Error __stdcall KS_compilePlcBuffer(
                KSHandle hCompiler, const void* pSourceBuffer, int sourceLength, int sourceLang, void* pDestBuffer, int* pDestLength, int flags);

//------ PLC functions ------
Error __stdcall KS_createPlcFromFile(
                KSHandle* phPlc, const char* binaryPath, int flags);
Error __stdcall KS_createPlcFromBuffer(
                KSHandle* phPlc, const void* pBinaryBuffer, int bufferLength, int flags);
Error __stdcall KS_closePlc(
                KSHandle hPlc, int flags);
Error __stdcall KS_startPlc(
                KSHandle hPlc, int configIndex, int64ref start, int flags);
Error __stdcall KS_stopPlc(
                KSHandle hPlc, int flags);

//------ Information functions ------
Error __stdcall KS_enumPlcConfigurations(
                KSHandle hPlc, int configIndex, char* pNameBuf, int flags);
Error __stdcall KS_enumPlcResources(
                KSHandle hPlc, int configIndex, int resourceIndex, char* pNameBuf, int flags);
Error __stdcall KS_enumPlcTasks(
                KSHandle hPlc, int configIndex, int resourceIndex, int taskIndex, char* pNameBuf, int flags);

//------ Generic functions ------
Error __stdcall KS_execPlcCommand(
                KSHandle hPlc, int command, void* pData, int flags);
Error __stdcall KS_installPlcHandler(
                KSHandle hPlc, int eventCode, KSHandle hSignal, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_PLC_MODULE

//--------------------------------------------------------------------------------------------------------------
// Vision Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Basic functions ------
Error __stdcall KS_loadVisionKernel(
                KSHandle* phKernel, const char* dllName, const char* initProcName, void* pArgs, int flags);

//------ Control functions ------
Error __stdcall KS_installVisionHandler(
                int eventType, KSHandle hSignal, int flags);

//------ Context structures ------
typedef struct {
  int ctxType;
  KSHandle hTask;
} KSVisionTaskContext;

//------ Handler events ------
#define KS_VISION_TASK_CREATED                          0x00000001
#define KS_VISION_TASK_CLOSED                           0x00000002

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_VISION_MODULE

//--------------------------------------------------------------------------------------------------------------
// SigProc Module
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 1)

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
namespace KrtsDemo {
#endif

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
extern "C" {
#endif

//------ Filter types ------
#define KS_FIR_LOWPASS_HAMMING                          0x00000000
#define KS_FIR_LOWPASS_HANNING                          0x00000001
#define KS_FIR_LOWPASS_RECTANGULAR                      0x00000002
#define KS_FIR_HIGHPASS_HAMMING                         0x00000003
#define KS_FIR_HIGHPASS_HANNING                         0x00000004
#define KS_FIR_HIGHPASS_RECTANGULAR                     0x00000005
#define KS_FIR_BANDPASS_HAMMING                         0x00000006
#define KS_FIR_BANDPASS_HANNING                         0x00000007
#define KS_FIR_BANDPASS_RECTANGULAR                     0x00000008
#define KS_FIR_BANDSTOP_HAMMING                         0x00000009
#define KS_FIR_BANDSTOP_HANNING                         0x0000000a
#define KS_FIR_BANDSTOP_RECTANGULAR                     0x0000000b
#define KS_IIR_LOWPASS_CHEBYSHEV_I                      0x00000014
#define KS_IIR_LOWPASS_BUTTERWORTH                      0x00000015
#define KS_IIR_LOWPASS_CHEBYSHEV_II                     0x00000016
#define KS_IIR_HIGHPASS_CHEBYSHEV_I                     0x00000017
#define KS_IIR_HIGHPASS_BUTTERWORTH                     0x00000018
#define KS_IIR_HIGHPASS_CHEBYSHEV_II                    0x00000019
#define KS_IIR_BANDPASS_CHEBYSHEV_I                     0x0000001a
#define KS_IIR_BANDPASS_BUTTERWORTH                     0x0000001b
#define KS_IIR_BANDPASS_CHEBYSHEV_II                    0x0000001c
#define KS_IIR_BANDSTOP_CHEBYSHEV_I                     0x0000001d
#define KS_IIR_BANDSTOP_BUTTERWORTH                     0x0000001e
#define KS_IIR_BANDSTOP_CHEBYSHEV_II                    0x0000001f
#define KS_IIR_INDIVIDUAL                               0x00000020

//------ Standard ripples ------
#define KS_CHEBYSHEV_3DB                                0.707106781
#define KS_CHEBYSHEV_1DB                                0.891250938

//------ Some filter specified errorcodes ------
#define KSERROR_SP_SHANNON_THEOREM_MISMATCH             (KSERROR_CATEGORY_SPECIAL+0x00020000)

//------ Paramters structures ------
typedef struct {
  int structSize;
  int filterType;
  int order;
  double maxFrequency;
  double frequency;
  double sampleRate;
  double width;
  double delta;
} KSSignalFilterParams;

typedef struct {
  int structSize;
  double* pXCoeffs;
  int xOrder;
  double* pYCoeffs;
  int yOrder;
  double maxFrequency;
  double sampleRate;
} KSIndividualSignalFilterParams;

typedef struct {
  int structSize;
  double proportional;
  double integral;
  double derivative;
  double setpoint;
  double sampleRate;
} KSPIDControllerParams;

//------ Digital Filter functions ------
Error __stdcall KS_createSignalFilter(
                KSHandle* phFilter, const KSSignalFilterParams* pParams, int flags);
Error __stdcall KS_createIndividualSignalFilter(
                KSHandle* phFilter, KSIndividualSignalFilterParams* pParams, int flags);
Error __stdcall KS_getFrequencyResponse(
                KSHandle hFilter, double frequency, double* pAmplitude, double* pPhase, int flags);
Error __stdcall KS_scaleSignalFilter(
                KSHandle hFilter, double frequency, double value, int flags);
Error __stdcall KS_resetSignalFilter(
                KSHandle hFilter, int flags);

//------ PID Controller functions ------
Error __stdcall KS_createPIDController(
                KSHandle* phController, KSPIDControllerParams* pParams, int flags);
Error __stdcall KS_changeSetpoint(
                KSHandle hController, double setpoint, int flags);

//------ General signal processing functions ------
Error __stdcall KS_removeSignalProcessor(
                KSHandle hSigProc, int flags);
Error __stdcall KS_processSignal(
                KSHandle hSigProc, double input, double* pOutput, int flags);

#if defined(__cplusplus) && !defined(KS_ENABLE_NAMESPACE)
} // end of 'extern "C"'
#endif

#if defined(__cplusplus) && defined(KS_ENABLE_NAMESPACE)
} // end of 'namespace KrtsDemo'
#endif

#pragma pack(pop)

#define KS_INCL_SIGPROC_MODULE

#endif // __KRTSDEMO_H
