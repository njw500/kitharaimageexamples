// Copyright (c) 1996-2016 by Kithara Software GmbH. All rights reserved.

//##############################################################################################################
//
// File:             KrtsDemo.pas (v10.00d)
//
// Description:      Delphi API for Kithara �RealTime Suite�
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// u.jes 1996-12-16  CREAT: Original - (file created)
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:01
//
//##############################################################################################################

//--------------------------------------------------------------------------------------------------------------
// KrtsDemo
//--------------------------------------------------------------------------------------------------------------

unit KrtsDemo;

//--------------------------------------------------------------------------------------------------------------
// interface
//--------------------------------------------------------------------------------------------------------------

interface

//--------------------------------------------------------------------------------------------------------------
// Base Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Categories ------
  // Operating system errors:
  KSERROR_CATEGORY_OPERATING_SYSTEM             = $00000000;

  // Categories 0x01000000 ... 0x0f000000 are free for customer usage!
  KSERROR_CATEGORY_USER                         = $0f000000;
  KSERROR_CATEGORY_BASE                         = $10000000;
  KSERROR_CATEGORY_DEAL                         = $11000000;
  KSERROR_CATEGORY_HANDLING                     = $17000000;
  KSERROR_CATEGORY_IOPORT                       = $14000000;
  KSERROR_CATEGORY_MEMORY                       = $12000000;
  KSERROR_CATEGORY_KERNEL                       = $18000000;
  KSERROR_CATEGORY_KEYBOARD                     = $1a000000;
  KSERROR_CATEGORY_INTERRUPT                    = $13000000;
  KSERROR_CATEGORY_TIMER                        = $16000000;
  KSERROR_CATEGORY_USB                          = $19000000;
  KSERROR_CATEGORY_IDT                          = $1b000000;
  KSERROR_CATEGORY_NODE                         = $1c000000;
  KSERROR_CATEGORY_SOCKET                       = $1d000000;
  KSERROR_CATEGORY_SYSTEM                       = $1e000000;
  KSERROR_CATEGORY_ETHERCAT                     = $1f000000;
  KSERROR_CATEGORY_MF                           = $20000000;
  KSERROR_CATEGORY_CAN                          = $21000000;
  KSERROR_CATEGORY_PROFIBUS                     = $22000000;
  KSERROR_CATEGORY_PLC                          = $23000000;
  KSERROR_CATEGORY_CANOPEN                      = $24000000;
  KSERROR_CATEGORY_FLEXRAY                      = $25000000;
  KSERROR_CATEGORY_PACKET                       = $26000000;
  KSERROR_CATEGORY_CAMERA                       = $27000000;
  KSERROR_CATEGORY_TASK                         = $28000000;
  KSERROR_CATEGORY_SPECIAL                      = $29000000;
  KSERROR_CATEGORY_XHCI                         = $2a000000;
  KSERROR_CATEGORY_DISK                         = $2b000000;
  KSERROR_CATEGORY_FILE                         = $2c000000;
  KSERROR_CATEGORY_NEXT                         = $2d000000;
  KSERROR_CATEGORY_EXCEPTION                    = $3f000000;

  //------ Error Codes ------
  KSERROR_OPERATING_SYSTEM                      = (KSERROR_CATEGORY_BASE+$00000000);
  KSERROR_UNKNOWN_ERROR_CODE                    = (KSERROR_CATEGORY_BASE+$00010000);
  KSERROR_UNKNOWN_ERROR_CATEGORY                = (KSERROR_CATEGORY_BASE+$00020000);
  KSERROR_UNKNOWN_ERROR_LANGUAGE                = (KSERROR_CATEGORY_BASE+$00030000);
  KSERROR_CANNOT_FIND_LIBRARY                   = (KSERROR_CATEGORY_BASE+$00040000);
  KSERROR_CANNOT_FIND_ADDRESS                   = (KSERROR_CATEGORY_BASE+$00050000);
  KSERROR_BAD_PARAM                             = (KSERROR_CATEGORY_BASE+$00060000);
  KSERROR_BAD_VERSION                           = (KSERROR_CATEGORY_BASE+$00070000);
  KSERROR_INTERNAL                              = (KSERROR_CATEGORY_BASE+$00080000);
  KSERROR_UNKNOWN                               = (KSERROR_CATEGORY_BASE+$00090000);
  KSERROR_FUNCTION_NOT_AVAILABLE                = (KSERROR_CATEGORY_BASE+$000a0000);
  KSERROR_DRIVER_NOT_OPENED                     = (KSERROR_CATEGORY_BASE+$000b0000);
  KSERROR_NOT_ENOUGH_MEMORY                     = (KSERROR_CATEGORY_BASE+$000c0000);
  KSERROR_CANNOT_OPEN_KERNEL                    = (KSERROR_CATEGORY_BASE+$000d0000);
  KSERROR_CANNOT_CLOSE_KERNEL                   = (KSERROR_CATEGORY_BASE+$000e0000);
  KSERROR_BAD_KERNEL_VERSION                    = (KSERROR_CATEGORY_BASE+$000f0000);
  KSERROR_CANNOT_FIND_KERNEL                    = (KSERROR_CATEGORY_BASE+$00100000);
  KSERROR_INVALID_HANDLE                        = (KSERROR_CATEGORY_BASE+$00110000);
  KSERROR_DEVICE_IO_FAILED                      = (KSERROR_CATEGORY_BASE+$00120000);
  KSERROR_REGISTRY_FAILURE                      = (KSERROR_CATEGORY_BASE+$00130000);
  KSERROR_CANNOT_START_KERNEL                   = (KSERROR_CATEGORY_BASE+$00140000);
  KSERROR_CANNOT_STOP_KERNEL                    = (KSERROR_CATEGORY_BASE+$00150000);
  KSERROR_ACCESS_DENIED                         = (KSERROR_CATEGORY_BASE+$00160000);
  KSERROR_DEVICE_NOT_OPENED                     = (KSERROR_CATEGORY_BASE+$00170000);
  KSERROR_FUNCTION_DENIED                       = (KSERROR_CATEGORY_BASE+$00180000);
  KSERROR_DEVICE_ALREADY_USED                   = (KSERROR_CATEGORY_BASE+$00190000);
  KSERROR_OUT_OF_MEMORY                         = (KSERROR_CATEGORY_BASE+$001a0000);
  KSERROR_TOO_MANY_OPEN_PROCESSES               = (KSERROR_CATEGORY_BASE+$001b0000);
  KSERROR_SIGNAL_OVERFLOW                       = (KSERROR_CATEGORY_BASE+$001c0000);
  KSERROR_CANNOT_SIGNAL                         = (KSERROR_CATEGORY_BASE+$001d0000);
  KSERROR_FILE_NOT_FOUND                        = (KSERROR_CATEGORY_BASE+$001e0000);
  KSERROR_DEVICE_NOT_FOUND                      = (KSERROR_CATEGORY_BASE+$001f0000);
  KSERROR_ASSERTION_FAILED                      = (KSERROR_CATEGORY_BASE+$00200000);
  KSERROR_FUNCTION_FAILED                       = (KSERROR_CATEGORY_BASE+$00210000);
  KSERROR_OPERATION_ABORTED                     = (KSERROR_CATEGORY_BASE+$00220000);
  KSERROR_TIMEOUT                               = (KSERROR_CATEGORY_BASE+$00230000);
  KSERROR_REBOOT_NECESSARY                      = (KSERROR_CATEGORY_BASE+$00240000);
  KSERROR_DEVICE_DISABLED                       = (KSERROR_CATEGORY_BASE+$00250000);
  KSERROR_ATTACH_FAILED                         = (KSERROR_CATEGORY_BASE+$00260000);
  KSERROR_DEVICE_REMOVED                        = (KSERROR_CATEGORY_BASE+$00270000);
  KSERROR_TYPE_MISMATCH                         = (KSERROR_CATEGORY_BASE+$00280000);
  KSERROR_FUNCTION_NOT_IMPLEMENTED              = (KSERROR_CATEGORY_BASE+$00290000);
  KSERROR_COMMUNICATION_BROKEN                  = (KSERROR_CATEGORY_BASE+$002a0000);
  KSERROR_DEVICE_NOT_READY                      = (KSERROR_CATEGORY_BASE+$00370000);
  KSERROR_NO_RESPONSE                           = (KSERROR_CATEGORY_BASE+$00380000);
  KSERROR_OPERATION_PENDING                     = (KSERROR_CATEGORY_BASE+$00390000);
  KSERROR_PORT_BUSY                             = (KSERROR_CATEGORY_BASE+$003a0000);
  KSERROR_UNKNOWN_SYSTEM                        = (KSERROR_CATEGORY_BASE+$003b0000);
  KSERROR_BAD_CONTEXT                           = (KSERROR_CATEGORY_BASE+$003c0000);
  KSERROR_END_OF_FILE                           = (KSERROR_CATEGORY_BASE+$003d0000);
  KSERROR_INTERRUPT_ACTIVATION_FAILED           = (KSERROR_CATEGORY_BASE+$003e0000);
  KSERROR_INTERRUPT_IS_SHARED                   = (KSERROR_CATEGORY_BASE+$003f0000);
  KSERROR_NO_REALTIME_ACCESS                    = (KSERROR_CATEGORY_BASE+$00400000);
  KSERROR_HARDWARE_NOT_SUPPORTED                = (KSERROR_CATEGORY_BASE+$00410000);
  KSERROR_TIMER_ACTIVATION_FAILED               = (KSERROR_CATEGORY_BASE+$00420000);
  KSERROR_SOCKET_FAILURE                        = (KSERROR_CATEGORY_BASE+$00430000);
  KSERROR_LINE_ERROR                            = (KSERROR_CATEGORY_BASE+$00440000);
  KSERROR_STACK_CORRUPTION                      = (KSERROR_CATEGORY_BASE+$00450000);
  KSERROR_INVALID_ARGUMENT                      = (KSERROR_CATEGORY_BASE+$00460000);
  KSERROR_PARSE_ERROR                           = (KSERROR_CATEGORY_BASE+$00470000);
  KSERROR_INVALID_IDENTIFIER                    = (KSERROR_CATEGORY_BASE+$00480000);
  KSERROR_REALTIME_ALREADY_USED                 = (KSERROR_CATEGORY_BASE+$00490000);
  KSERROR_COMMUNICATION_FAILED                  = (KSERROR_CATEGORY_BASE+$004a0000);
  KSERROR_INVALID_SIZE                          = (KSERROR_CATEGORY_BASE+$004b0000);
  KSERROR_OBJECT_NOT_FOUND                      = (KSERROR_CATEGORY_BASE+$004c0000);

  //------ Error codes of handling category ------
  KSERROR_CANNOT_CREATE_EVENT                   = (KSERROR_CATEGORY_HANDLING+$00000000);
  KSERROR_CANNOT_CREATE_THREAD                  = (KSERROR_CATEGORY_HANDLING+$00010000);
  KSERROR_CANNOT_BLOCK_THREAD                   = (KSERROR_CATEGORY_HANDLING+$00020000);
  KSERROR_CANNOT_SIGNAL_EVENT                   = (KSERROR_CATEGORY_HANDLING+$00030000);
  KSERROR_CANNOT_POST_MESSAGE                   = (KSERROR_CATEGORY_HANDLING+$00040000);
  KSERROR_CANNOT_CREATE_SHARED                  = (KSERROR_CATEGORY_HANDLING+$00050000);
  KSERROR_SHARED_DIFFERENT_SIZE                 = (KSERROR_CATEGORY_HANDLING+$00060000);
  KSERROR_BAD_SHARED_MEM                        = (KSERROR_CATEGORY_HANDLING+$00070000);
  KSERROR_WAIT_TIMEOUT                          = (KSERROR_CATEGORY_HANDLING+$00080000);
  KSERROR_EVENT_NOT_SIGNALED                    = (KSERROR_CATEGORY_HANDLING+$00090000);
  KSERROR_CANNOT_CREATE_CALLBACK                = (KSERROR_CATEGORY_HANDLING+$000a0000);
  KSERROR_NO_DATA_AVAILABLE                     = (KSERROR_CATEGORY_HANDLING+$000b0000);
  KSERROR_REQUEST_NESTED                        = (KSERROR_CATEGORY_HANDLING+$000c0000);

  //------ Deal error codes ------
  KSERROR_BAD_CUSTNUM                           = (KSERROR_CATEGORY_DEAL+$00030000);
  KSERROR_KEY_BAD_FORMAT                        = (KSERROR_CATEGORY_DEAL+$00060000);
  KSERROR_KEY_BAD_DATA                          = (KSERROR_CATEGORY_DEAL+$00080000);
  KSERROR_KEY_PERMISSION_EXPIRED                = (KSERROR_CATEGORY_DEAL+$00090000);
  KSERROR_BAD_LICENCE                           = (KSERROR_CATEGORY_DEAL+$000a0000);
  KSERROR_CANNOT_LOAD_KEY                       = (KSERROR_CATEGORY_DEAL+$000c0000);
  KSERROR_BAD_NAME_OR_FIRM                      = (KSERROR_CATEGORY_DEAL+$000f0000);
  KSERROR_NO_LICENCES_FOUND                     = (KSERROR_CATEGORY_DEAL+$00100000);
  KSERROR_LICENCE_CAPACITY_EXHAUSTED            = (KSERROR_CATEGORY_DEAL+$00110000);
  KSERROR_FEATURE_NOT_LICENSED                  = (KSERROR_CATEGORY_DEAL+$00120000);

  //------ Exception Codes ------
  KSERROR_DIVIDE_BY_ZERO                        = (KSERROR_CATEGORY_EXCEPTION+$00000000);
  KSERROR_DEBUG_EXCEPTION                       = (KSERROR_CATEGORY_EXCEPTION+$00010000);
  KSERROR_NONMASKABLE_INTERRUPT                 = (KSERROR_CATEGORY_EXCEPTION+$00020000);
  KSERROR_BREAKPOINT                            = (KSERROR_CATEGORY_EXCEPTION+$00030000);
  KSERROR_OVERFLOW                              = (KSERROR_CATEGORY_EXCEPTION+$00040000);
  KSERROR_BOUND_RANGE                           = (KSERROR_CATEGORY_EXCEPTION+$00050000);
  KSERROR_INVALID_OPCODE                        = (KSERROR_CATEGORY_EXCEPTION+$00060000);
  KSERROR_NO_MATH                               = (KSERROR_CATEGORY_EXCEPTION+$00070000);
  KSERROR_DOUBLE_FAULT                          = (KSERROR_CATEGORY_EXCEPTION+$00080000);
  KSERROR_COPROCESSOR_SEGMENT_OVERRUN           = (KSERROR_CATEGORY_EXCEPTION+$00090000);
  KSERROR_INVALID_TSS                           = (KSERROR_CATEGORY_EXCEPTION+$000a0000);
  KSERROR_SEGMENT_NOT_PRESENT                   = (KSERROR_CATEGORY_EXCEPTION+$000b0000);
  KSERROR_STACK_FAULT                           = (KSERROR_CATEGORY_EXCEPTION+$000c0000);
  KSERROR_PROTECTION_FAULT                      = (KSERROR_CATEGORY_EXCEPTION+$000d0000);
  KSERROR_PAGE_FAULT                            = (KSERROR_CATEGORY_EXCEPTION+$000e0000);
  KSERROR_FPU_FAULT                             = (KSERROR_CATEGORY_EXCEPTION+$00100000);
  KSERROR_ALIGNMENT_CHECK                       = (KSERROR_CATEGORY_EXCEPTION+$00110000);
  KSERROR_MACHINE_CHECK                         = (KSERROR_CATEGORY_EXCEPTION+$00120000);
  KSERROR_SIMD_FAULT                            = (KSERROR_CATEGORY_EXCEPTION+$00130000);

  //------ Flags ------
  KSF_OPENED                                    = $01000000;
  KSF_ACTIVE                                    = $02000000;
  KSF_RUNNING                                   = $02000000;
  KSF_FINISHED                                  = $04000000;
  KSF_CANCELED                                  = $08000000;
  KSF_DISABLED                                  = $10000000;
  KSF_REQUESTED                                 = $20000000;
  KSF_TERMINATE                                 = $00000000;
  KSF_DONT_WAIT                                 = $00000001;
  KSF_KERNEL_EXEC                               = $00000004;
  KSF_ASYNC_EXEC                                = $0000000c;
  KSF_DIRECT_EXEC                               = $0000000c;
  KSF_DONT_START                                = $00000010;
  KSF_ONE_SHOT                                  = $00000020;
  KSF_SINGLE_SHOT                               = $00000020;
  KSF_SAVE_FPU                                  = $00000040;
  KSF_USE_SSE                                   = $00000001;
  KSF_USER_EXEC                                 = $00000080;
  KSF_REALTIME_EXEC                             = $00000100;
  KSF_WAIT                                      = $00000200;
  KSF_ACCEPT_INCOMPLETE                         = $00000200;
  KSF_REPORT_RESOURCE                           = $00000400;
  KSF_USE_TIMEOUTS                              = $00000400;
  KSF_FORCE_OVERRIDE                            = $00001000;
  KSF_OPTION_ON                                 = $00001000;
  KSF_OPTION_OFF                                = $00002000;
  KSF_FORCE_OPEN                                = $00002000;
  KSF_UNLIMITED                                 = $00004000;
  KSF_DONT_USE_SMP                              = $00004000;
  KSF_NO_CONTEXT                                = $00008000;
  KSF_HIGH_PRECISION                            = $00008000;
  KSF_CONTINUOUS                                = $00010000;
  KSF_ZERO_INIT                                 = $00020000;
  KSF_TX_PDO                                    = $00010000;
  KSF_RX_PDO                                    = $00020000;
  KSF_READ                                      = $00040000;
  KSF_WRITE                                     = $00080000;
  KSF_USE_SMP                                   = $00080000;
  KSF_ALTERNATIVE                               = $00200000;
  KSF_PDO                                       = $00040000;
  KSF_SDO                                       = $00080000;
  KSF_IDN                                       = $00100000;
  KSF_MANUAL_RESET                              = $00200000;
  KSF_RESET_STATE                               = $00800000;
  KSF_ISA_BUS                                   = $00000001;
  KSF_PCI_BUS                                   = $00000005;
  KSF_PRIO_NORMAL                               = $00000000;
  KSF_PRIO_LOW                                  = $00000001;
  KSF_PRIO_LOWER                                = $00000002;
  KSF_PRIO_LOWEST                               = $00000003;
  KSF_MESSAGE_PIPE                              = $00001000;
  KSF_PIPE_NOTIFY_GET                           = $00002000;
  KSF_PIPE_NOTIFY_PUT                           = $00004000;
  KSF_ALL_CPUS                                  = $00000000;
  KSF_READ_ACCESS                               = $00000001;
  KSF_WRITE_ACCESS                              = $00000002;
  KSF_SIZE_8_BIT                                = $00010000;
  KSF_SIZE_16_BIT                               = $00020000;
  KSF_SIZE_32_BIT                               = $00040000;

  //------ Obsolete flags - do not use! ------
  KSF_ACCEPT_PENDING                            = $00000002;
  KSF_DONT_RAISE                                = $00200000;

  //------ Constant for returning no-error ------
  KS_OK                                         = $00000000;

  //------ Language constants ------
  KSLNG_DEFAULT                                 = $00000000;

  //------ Context type values ------
  USER_CONTEXT                                  = $00000000;
  INTERRUPT_CONTEXT                             = $00000100;
  TIMER_CONTEXT                                 = $00000200;
  KEYBOARD_CONTEXT                              = $00000300;
  USB_BASE                                      = $00000506;
  PACKET_BASE                                   = $00000700;
  DEVICE_BASE                                   = $00000800;
  ETHERCAT_BASE                                 = $00000900;
  SOCKET_BASE                                   = $00000a00;
  TASK_BASE                                     = $00000b00;
  MULTIFUNCTION_BASE                            = $00000c00;
  CAN_BASE                                      = $00000d00;
  PROFIBUS_BASE                                 = $00000e00;
  CANOPEN_BASE                                  = $00000f00;
  FLEXRAY_BASE                                  = $00001000;
  XHCI_BASE                                     = $00001100;
  V86_BASE                                      = $00001200;
  PLC_BASE                                      = $00002000;

  //------ Config code for "Driver" ------
  KSCONFIG_DRIVER_NAME                          = $00000001;
  KSCONFIG_FUNCTION_LOGGING                     = $00000002;

  //------ Config code for "Base" ------
  KSCONFIG_DISABLE_MESSAGE_LOGGING              = $00000001;
  KSCONFIG_ENABLE_MESSAGE_LOGGING               = $00000002;
  KSCONFIG_FLUSH_MESSAGE_PIPE                   = $00000003;

  //------ Config code for "Debug" ------

  //------ Commonly used commands ------
  KS_ABORT_XMIT_CMD                             = $00000001;
  KS_ABORT_RECV_CMD                             = $00000002;
  KS_FLUSH_XMIT_BUF                             = $00000003;
  KS_FLUSH_RECV_BUF                             = $00000009;
  KS_SET_BAUD_RATE                              = $0000000c;

  //------ Pipe contexts ------
  PIPE_PUT_CONTEXT                              = (USER_CONTEXT+$00000040);
  PIPE_GET_CONTEXT                              = (USER_CONTEXT+$00000041);

  //------ Quick mutex levels ------
  KS_APP_LEVEL                                  = $00000000;
  KS_DPC_LEVEL                                  = $00000001;
  KS_ISR_LEVEL                                  = $00000002;
  KS_RTX_LEVEL                                  = $00000003;
  KS_CPU_LEVEL                                  = $00000004;

  //------ Data types ------
  KS_DATATYPE_UNKNOWN                           = $00000000;
  KS_DATATYPE_BOOLEAN                           = $00000001;
  KS_DATATYPE_INTEGER8                          = $00000002;
  KS_DATATYPE_UINTEGER8                         = $00000005;
  KS_DATATYPE_INTEGER16                         = $00000003;
  KS_DATATYPE_UINTEGER16                        = $00000006;
  KS_DATATYPE_INTEGER32                         = $00000004;
  KS_DATATYPE_UINTEGER32                        = $00000007;
  KS_DATATYPE_INTEGER64                         = $00000015;
  KS_DATATYPE_UNSIGNED64                        = $0000001b;
  KS_DATATYPE_REAL32                            = $00000008;
  KS_DATATYPE_REAL64                            = $00000011;
  KS_DATATYPE_TINY                              = $00000002;
  KS_DATATYPE_BYTE                              = $00000005;
  KS_DATATYPE_SHORT                             = $00000003;
  KS_DATATYPE_USHORT                            = $00000006;
  KS_DATATYPE_INT                               = $00000004;
  KS_DATATYPE_UINT                              = $00000007;
  KS_DATATYPE_LLONG                             = $00000015;
  KS_DATATYPE_ULONG                             = $0000001b;
  KS_DATATYPE_VISIBLE_STRING                    = $00000009;
  KS_DATATYPE_OCTET_STRING                      = $0000000a;
  KS_DATATYPE_UNICODE_STRING                    = $0000000b;
  KS_DATATYPE_TIME_OF_DAY                       = $0000000c;
  KS_DATATYPE_TIME_DIFFERENCE                   = $0000000d;
  KS_DATATYPE_DOMAIN                            = $0000000f;
  KS_DATATYPE_TIME_OF_DAY32                     = $0000001c;
  KS_DATATYPE_DATE32                            = $0000001d;
  KS_DATATYPE_DATE_AND_TIME64                   = $0000001e;
  KS_DATATYPE_TIME_DIFFERENCE64                 = $0000001f;
  KS_DATATYPE_BIT1                              = $00000030;
  KS_DATATYPE_BIT2                              = $00000031;
  KS_DATATYPE_BIT3                              = $00000032;
  KS_DATATYPE_BIT4                              = $00000033;
  KS_DATATYPE_BIT5                              = $00000034;
  KS_DATATYPE_BIT6                              = $00000035;
  KS_DATATYPE_BIT7                              = $00000036;
  KS_DATATYPE_BIT8                              = $00000037;
  KS_DATATYPE_INTEGER24                         = $00000010;
  KS_DATATYPE_UNSIGNED24                        = $00000016;
  KS_DATATYPE_INTEGER40                         = $00000012;
  KS_DATATYPE_UNSIGNED40                        = $00000018;
  KS_DATATYPE_INTEGER48                         = $00000013;
  KS_DATATYPE_UNSIGNED48                        = $00000019;
  KS_DATATYPE_INTEGER56                         = $00000014;
  KS_DATATYPE_UNSIGNED56                        = $0000001a;
  KS_DATATYPE_KSHANDLE                          = $00000040;

  //------ DataObj type flags ------
  KS_DATAOBJ_READABLE                           = $00010000;
  KS_DATAOBJ_WRITEABLE                          = $00020000;
  KS_DATAOBJ_ARRAY                              = $01000000;
  KS_DATAOBJ_ENUM                               = $02000000;
  KS_DATAOBJ_STRUCT                             = $04000000;

  //------ Common Constants ------
  {$IFNDEF WIN64}
  KS_INVALID_HANDLE                             = NIL;
  {$ELSE}
  KS_INVALID_HANDLE                             = $00000000;
  {$ENDIF}

type
  //------ Common types and structures ------
  Int8         = ShortInt;
  Int16        = SmallInt;
  Int32        = LongInt;
  Int          = Int32;
  UInt8        = Byte;
  UInt16       = Word;
  UInt32       = LongWord;
  UInt         = UInt32;

  Error        = Int32;

  {$IFNDEF WIN64}
  KSHandle     = Pointer;
  {$ELSE}
  KSHandle     = Int32;
  {$ENDIF}
  Win32Handle  = Pointer;

  PByte        = ^Byte;
  PInt         = ^Int;
  PUInt        = ^UInt;
  PInt64       = ^Int64;
  PSingle      = ^Single;
  PDouble      = ^Double;
  PKSHandle    = ^KSHandle;
  PWin32Handle = ^Win32Handle;
  PPointer     = ^Pointer;

  {$IFNDEF KS_NO_KITHARA_LEGACY}
  Handle       = KSHandle;
  PHandle      = ^Handle;
  {$ENDIF}

  UInt64 = packed record
    lo: uint;
    hi: uint;
  end;
  PUInt64 = ^UInt64;

  HandlerState = packed record
    requested: int;
    performed: int;
    state: int;
    error: Error;
  end;
  PHandlerState = ^HandlerState;

  KSTimeOut = packed record
    baseTime: int;
    charTime: int;
  end;
  PKSTimeOut = ^KSTimeOut;

  KSDeviceInfo = packed record
    structSize: int;
    pDeviceName: array [0..79] of AnsiChar;
    pDeviceDesc: array [0..79] of AnsiChar;
    pDeviceVendor: array [0..79] of AnsiChar;
    pFriendlyName: array [0..79] of AnsiChar;
    pHardwareId: array [0..79] of AnsiChar;
    pClassName: array [0..79] of AnsiChar;
    pInfPath: array [0..79] of AnsiChar;
    pDriverDesc: array [0..79] of AnsiChar;
    pDriverVendor: array [0..79] of AnsiChar;
    pDriverDate: array [0..79] of AnsiChar;
    pDriverVersion: array [0..79] of AnsiChar;
    pServiceName: array [0..79] of AnsiChar;
    pServiceDisp: array [0..79] of AnsiChar;
    pServiceDesc: array [0..79] of AnsiChar;
    pServicePath: array [0..79] of AnsiChar;
  end;
  PKSDeviceInfo = ^KSDeviceInfo;

  KSSystemInformation = packed record
    structSize: int;
    numberOfCPUs: int;
    numberOfSharedCPUs: int;
    kiloBytesOfRAM: int;
    isDll64Bit: byte;
    isSys64Bit: byte;
    isStableVersion: byte;
    isLogVersion: byte;
    dllVersion: int;
    dllBuildNumber: int;
    sysVersion: int;
    sysBuildNumber: int;
    systemVersion: int;
    pSystemName: array [0..63] of AnsiChar;
    pServicePack: array [0..63] of AnsiChar;
    pDllPath: array [0..255] of AnsiChar;
    pSysPath: array [0..255] of AnsiChar;
  end;
  PKSSystemInformation = ^KSSystemInformation;

  KSProcessorInformation = packed record
    structSize: int;
    index: int;
    group: int;
    number: int;
    currentTemperature: int;
    allowedTemperature: int;
  end;
  PKSProcessorInformation = ^KSProcessorInformation;

  KSSharedMemInfo = packed record
    structSize: int;
    hHandle: KSHandle;
    pName: array [0..255] of AnsiChar;
    size: int;
    pThisPtr: Pointer;
  end;
  PKSSharedMemInfo = ^KSSharedMemInfo;

  //------ Context structures ------
  PipeUserContext = packed record
    ctxType: int;
    hPipe: KSHandle;
  end;
  PPipeUserContext = ^PipeUserContext;

//------ Helper functions ------
function KSERROR_CODE(error: Error):
  Error;

//------ Common functions ------
function KS_startKernel:
  Error; stdcall; external 'KrtsDemo.dll';
function KS_stopKernel:
  Error; stdcall; external 'KrtsDemo.dll';
function KS_resetKernel(flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_openDriver(customerNumber: PAnsiChar):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeDriver:
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getDriverVersion(pVersion: PUInt):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getErrorString(code: Error; var msg: PAnsiChar; language: uint):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_addErrorString(code: Error; msg: PAnsiChar; language: uint):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getDriverConfig(module: PAnsiChar; configType: int; pData: Pointer):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setDriverConfig(module: PAnsiChar; configType: int; pData: Pointer):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getSystemInformation(pSystemInfo: PKSSystemInformation; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getProcessorInformation(index: int; pProcessorInfo: PKSProcessorInformation; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeHandle(handle: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Debugging & test ------
function KS_showMessage(ksError: Error; msg: PAnsiChar):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_logMessage(msgType: int; msgText: PAnsiChar):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_bufMessage(msgType: int; pBuffer: Pointer; length: int; msgText: PAnsiChar):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_dbgMessage(fileName: PAnsiChar; line: int; msgText: PAnsiChar):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_beep(freq: int; duration: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_throwException(error: int; pText: PAnsiChar; pFile: PAnsiChar; line: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Device handling ------
function KS_enumDevices(deviceType: PAnsiChar; index: int; pDeviceNameBuf: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumDevicesEx(className: PAnsiChar; busType: PAnsiChar; hEnumerator: KSHandle; index: int; pDeviceNameBuf: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getDevices(className: PAnsiChar; busType: PAnsiChar; hEnumerator: KSHandle; pCount: PInt; pBuf: PPointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getDeviceInfo(deviceName: PAnsiChar; pDeviceInfo: PKSDeviceInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_updateDriver(deviceName: PAnsiChar; fileName: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Threads & priorities ------
function KS_createThread(procName: Pointer; pArgs: Pointer; phThread: PWin32Handle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removeThread:
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setThreadPrio(prio: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getThreadPrio(pPrio: PUInt):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Shared memory ------
function KS_createSharedMem(ppAppPtr: PPointer; ppSysPtr: PPointer; name: PAnsiChar; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_freeSharedMem(pAppPtr: Pointer):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getSharedMem(ppPtr: PPointer; name: PAnsiChar):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readMem(pBuffer: Pointer; pAppPtr: Pointer; size: int; offset: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_writeMem(pBuffer: Pointer; pAppPtr: Pointer; size: int; offset: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_createSharedMemEx(phHandle: PKSHandle; name: PAnsiChar; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_freeSharedMemEx(hHandle: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getSharedMemEx(hHandle: KSHandle; pPtr: PPointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Events ------
function KS_createEvent(phEvent: PKSHandle; name: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeEvent(hEvent: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setEvent(hEvent: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_resetEvent(hEvent: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_pulseEvent(hEvent: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_waitForEvent(hEvent: KSHandle; flags: int; timeout: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getEventState(hEvent: KSHandle; pState: PUInt):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_postMessage(hWnd: Win32Handle; msg: uint; wP: uint; lP: uint):
  Error; stdcall; external 'KrtsDemo.dll';

//------ CallBacks ------
function KS_createCallBack(phCallBack: PKSHandle; routine: Pointer; pArgs: Pointer; flags: int; prio: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removeCallBack(hCallBack: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execCallBack(hCallBack: KSHandle; pContext: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getCallState(hSignal: KSHandle; pState: PHandlerState):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_signalObject(hObject: KSHandle; pContext: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Pipes ------
function KS_createPipe(phPipe: PKSHandle; name: PAnsiChar; itemSize: int; itemCount: int; hNotify: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removePipe(hPipe: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_flushPipe(hPipe: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getPipe(hPipe: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_putPipe(hPipe: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Quick mutexes ------
function KS_createQuickMutex(phMutex: PKSHandle; level: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removeQuickMutex(hMutex: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_requestQuickMutex(hMutex: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_releaseQuickMutex(hMutex: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_BASE_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Kernel Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_RTX_TOO_MANY_FUNCTIONS                = (KSERROR_CATEGORY_KERNEL+$00000000);
  KSERROR_CANNOT_EXECUTE_RTX_CALL               = (KSERROR_CATEGORY_KERNEL+$00010000);
  KSERROR_CANNOT_PREPARE_RTX                    = (KSERROR_CATEGORY_KERNEL+$00020000);
  KSERROR_RTX_BAD_SYSTEM_CALL                   = (KSERROR_CATEGORY_KERNEL+$00030000);
  KSERROR_CANNOT_LOAD_KERNEL                    = (KSERROR_CATEGORY_KERNEL+$00040000);
  KSERROR_CANNOT_RELOCATE_KERNEL                = (KSERROR_CATEGORY_KERNEL+$00050000);
  KSERROR_KERNEL_ALREADY_LOADED                 = (KSERROR_CATEGORY_KERNEL+$00060000);
  KSERROR_KERNEL_NOT_LOADED                     = (KSERROR_CATEGORY_KERNEL+$00070000);

  //------ Flags ------
  KSF_ANALYSE_ONLY                              = $00010000;
  KSF_FUNC_TABLE_GIVEN                          = $00020000;
  KSF_FORCE_RELOC                               = $00040000;
  KSF_LOAD_DEPENDENCIES                         = $00001000;
  KSF_EXEC_ENTRY_POINT                          = $00002000;

  //------ Kernel commands ------
  KS_GET_KERNEL_BASE_ADDRESS                    = $00000001;

//------ Relocation by byte-wise instruction mapping ------
function KS_prepareKernelExec(callback: Pointer; pRelocated: PPointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_endKernelExec(relocated: Pointer):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_testKernelExec(relocated: Pointer; pArgs: Pointer):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Relocation by loading DLL into kernel-space ------
function KS_registerKernelAddress(name: PAnsiChar; appFunction: Pointer; sysFunction: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_loadKernel(phKernel: PKSHandle; dllName: PAnsiChar; initProcName: PAnsiChar; pArgs: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_loadKernelFromBuffer(phKernel: PKSHandle; dllName: PAnsiChar; pBuffer: Pointer; length: int; initProcName: PAnsiChar; pArgs: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_freeKernel(hKernel: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execKernelFunction(hKernel: KSHandle; name: PAnsiChar; pArgs: Pointer; pContext: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getKernelFunction(hKernel: KSHandle; name: PAnsiChar; pRelocated: PPointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumKernelFunctions(hKernel: KSHandle; index: int; pName: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execKernelCommand(hKernel: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_createKernelCallBack(phCallBack: PKSHandle; hKernel: KSHandle; name: PAnsiChar; pArgs: Pointer; flags: int; prio: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Helper functions ------
function KS_execSyncFunction(hHandler: KSHandle; proc: Pointer; pArgs: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execSystemCall(fileName: PAnsiChar; funcName: PAnsiChar; pData: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Memory functions ------
function KS_memCpy(pDst: Pointer; pSrc: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_memMove(pDst: Pointer; pSrc: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_memSet(pMem: Pointer; value: int; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_malloc(ppAllocated: PPointer; bytes: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_free(pAllocated: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Interlocked functions ------
function KS_interlocked(pInterlocked: Pointer; operation: int; pParameter1: Pointer; pParameter2: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_KERNEL_MODULE}

//--------------------------------------------------------------------------------------------------------------
// IoPort Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_CANNOT_ENABLE_IO_RANGE                = (KSERROR_CATEGORY_IOPORT+$00000000);
  KSERROR_BUS_NOT_FOUND                         = (KSERROR_CATEGORY_IOPORT+$00010000);
  KSERROR_BAD_SLOT_NUMBER                       = (KSERROR_CATEGORY_IOPORT+$00020000);

  //------ Number of address entries in PCI data ------
  KS_PCI_TYPE0_ADDRESSES                        = $00000006;
  KS_PCI_TYPE1_ADDRESSES                        = $00000002;
  KS_PCI_TYPE2_ADDRESSES                        = $00000004;

  //------ Bit encodings for KSPciBusData.headerType ------
  KSF_PCI_MULTIFUNCTION                         = $00000080;
  KSF_PCI_DEVICE_TYPE                           = $00000000;
  KSF_PCI_PCI_BRIDGE_TYPE                       = $00000001;
  KSF_PCI_CARDBUS_BRIDGE_TYPE                   = $00000002;

  //------ Bit encodings for KSPciBusData.command ------
  KSF_PCI_ENABLE_IO_SPACE                       = $00000001;
  KSF_PCI_ENABLE_MEMORY_SPACE                   = $00000002;
  KSF_PCI_ENABLE_BUS_MASTER                     = $00000004;
  KSF_PCI_ENABLE_SPECIAL_CYCLES                 = $00000008;
  KSF_PCI_ENABLE_WRITE_AND_INVALIDATE           = $00000010;
  KSF_PCI_ENABLE_VGA_COMPATIBLE_PALETTE         = $00000020;
  KSF_PCI_ENABLE_PARITY                         = $00000040;
  KSF_PCI_ENABLE_WAIT_CYCLE                     = $00000080;
  KSF_PCI_ENABLE_SERR                           = $00000100;
  KSF_PCI_ENABLE_FAST_BACK_TO_BACK              = $00000200;

  //------ Bit encodings for KSPciBusData.status ------
  KSF_PCI_STATUS_CAPABILITIES_LIST              = $00000010;
  KSF_PCI_STATUS_FAST_BACK_TO_BACK              = $00000080;
  KSF_PCI_STATUS_DATA_PARITY_DETECTED           = $00000100;
  KSF_PCI_STATUS_DEVSEL                         = $00000600;
  KSF_PCI_STATUS_SIGNALED_TARGET_ABORT          = $00000800;
  KSF_PCI_STATUS_RECEIVED_TARGET_ABORT          = $00001000;
  KSF_PCI_STATUS_RECEIVED_MASTER_ABORT          = $00002000;
  KSF_PCI_STATUS_SIGNALED_SYSTEM_ERROR          = $00004000;
  KSF_PCI_STATUS_DETECTED_PARITY_ERROR          = $00008000;

  //------ Bit encodings for KSPciBusData.baseAddresses ------
  KSF_PCI_ADDRESS_IO_SPACE                      = $00000001;
  KSF_PCI_ADDRESS_MEMORY_TYPE_MASK              = $00000006;
  KSF_PCI_ADDRESS_MEMORY_PREFETCHABLE           = $00000008;

  //------ PCI types ------
  KSF_PCI_TYPE_32BIT                            = $00000000;
  KSF_PCI_TYPE_20BIT                            = $00000002;
  KSF_PCI_TYPE_64BIT                            = $00000004;

  //------ Resource types ------
  KSRESOURCE_TYPE_NONE                          = $00000000;
  KSRESOURCE_TYPE_PORT                          = $00000001;
  KSRESOURCE_TYPE_INTERRUPT                     = $00000002;
  KSRESOURCE_TYPE_MEMORY                        = $00000003;
  KSRESOURCE_TYPE_STRING                        = $000000ff;

  //------ KSRESOURCE_SHARE ------
  KSRESOURCE_SHARE_UNDETERMINED                 = $00000000;
  KSRESOURCE_SHARE_DEVICEEXCLUSIVE              = $00000001;
  KSRESOURCE_SHARE_DRIVEREXCLUSIVE              = $00000002;
  KSRESOURCE_SHARE_SHARED                       = $00000003;

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_PORT ------
  KSRESOURCE_MODE_MEMORY                        = $00000000;
  KSRESOURCE_MODE_IO                            = $00000001;

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_INTERRUPT ------
  KSRESOURCE_MODE_LEVELSENSITIVE                = $00000000;
  KSRESOURCE_MODE_LATCHED                       = $00000001;

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_MEMORY ------
  KSRESOURCE_MODE_READ_WRITE                    = $00000000;
  KSRESOURCE_MODE_READ_ONLY                     = $00000001;
  KSRESOURCE_MODE_WRITE_ONLY                    = $00000002;

type
  //------ Types & Structures ------
  KSPciBusData = packed record
    vendorID: Word;
    deviceID: Word;
    command: Word;
    status: Word;
    revisionID: byte;
    progIf: byte;
    subClass: byte;
    baseClass: byte;
    cacheLineSize: byte;
    latencyTimer: byte;
    headerType: byte;
    BIST: byte;
    pBaseAddresses: array [0..5] of uint;
    CIS: uint;
    subVendorID: Word;
    subSystemID: Word;
    baseROMAddress: uint;
    capabilityList: uint;
    reserved: uint;
    interruptLine: byte;
    interruptPin: byte;
    minimumGrant: byte;
    maximumLatency: byte;
    pDeviceSpecific: array [0..191] of byte;
  end;
  PKSPciBusData = ^KSPciBusData;

  KSResourceItem = packed record
    flags: uint;
    data0: uint;
    data1: uint;
    data2: uint;
  end;
  PKSResourceItem = ^KSResourceItem;

  KSResourceInfo = packed record
    busType: uint;
    busNumber: uint;
    version: uint;
    itemCount: uint;
    pItems: array [0..7] of KSResourceItem;
  end;
  PKSResourceInfo = ^KSResourceInfo;

  KSRangeResourceItem = packed record
    flags: uint;
    address: uint;
    reserved: uint;
    length: uint;
  end;
  PKSRangeResourceItem = ^KSRangeResourceItem;

  KSInterruptResourceItem = packed record
    flags: uint;
    level: uint;
    vector: uint;
    affinity: uint;
  end;
  PKSInterruptResourceItem = ^KSInterruptResourceItem;

  KSResourceInfoEx = packed record
    busType: int;
    busNumber: int;
    version: int;
    itemCount: int;
    range0: KSRangeResourceItem;
    range1: KSRangeResourceItem;
    range2: KSRangeResourceItem;
    range3: KSRangeResourceItem;
    range4: KSRangeResourceItem;
    range5: KSRangeResourceItem;
    interruptItem: KSInterruptResourceItem;
    reserved: KSResourceItem;
  end;
  PKSResourceInfoEx = ^KSResourceInfoEx;

//------ I/O port access functions ------
function KS_enableIoRange(ioPortBase: uint; count: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readIoPort(ioPort: uint; pValue: PUInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_writeIoPort(ioPort: uint; value: uint; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_inpb(ioPort: uint):
  uint; stdcall; external 'KrtsDemo.dll';
function KS_inpw(ioPort: uint):
  uint; stdcall; external 'KrtsDemo.dll';
function KS_inpd(ioPort: uint):
  uint; stdcall; external 'KrtsDemo.dll';
procedure KS_outpb(ioPort: uint; value: uint)
  stdcall; external 'KrtsDemo.dll';
procedure KS_outpw(ioPort: uint; value: uint)
  stdcall; external 'KrtsDemo.dll';
procedure KS_outpd(ioPort: uint; value: uint)
  stdcall; external 'KrtsDemo.dll';

//------ Bus data & resource info ------
function KS_getBusData(pBusData: PKSPciBusData; busNumber: int; slotNumber: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setBusData(pBusData: PKSPciBusData; busNumber: int; slotNumber: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
// DO NOT USE! - OBSOLETE! - USE 'KS_getResourceInfoEx' instead!
function KS_getResourceInfo(name: PAnsiChar; pInfo: PKSResourceInfo):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getResourceInfoEx(name: PAnsiChar; pInfo: PKSResourceInfoEx):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Quiet section (obsolete) ------
function KS_enterQuietSection(flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_releaseQuietSection(flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_IOPORT_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Memory Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_CANNOT_MAP_PHYSMEM                    = (KSERROR_CATEGORY_MEMORY+$00000000);
  KSERROR_CANNOT_UNMAP_PHYSMEM                  = (KSERROR_CATEGORY_MEMORY+$00010000);
  KSERROR_PHYSMEM_DIFFERENT_SIZE                = (KSERROR_CATEGORY_MEMORY+$00020000);
  KSERROR_CANNOT_ALLOC_PHYSMEM                  = (KSERROR_CATEGORY_MEMORY+$00030000);
  KSERROR_CANNOT_LOCK_MEM                       = (KSERROR_CATEGORY_MEMORY+$00040000);
  KSERROR_CANNOT_UNLOCK_MEM                     = (KSERROR_CATEGORY_MEMORY+$00050000);

  //------ Flags ------
  KSF_MAP_INTERNAL                              = $00000000;

//------ Physical memory management functions ------
function KS_mapDeviceMem(ppAppPtr: PPointer; ppSysPtr: PPointer; pInfo: PKSResourceInfoEx; index: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_mapPhysMem(ppAppPtr: PPointer; ppSysPtr: PPointer; physAddr: uint; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_unmapPhysMem(pAppPtr: Pointer):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_allocPhysMem(ppAppPtr: PPointer; ppSysPtr: PPointer; physAddr: PUInt; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_freePhysMem(pAppPtr: Pointer):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_copyPhysMem(physAddr: uint; pBuffer: Pointer; size: uint; offset: uint; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getPhysAddr(pPhysAddr: PPointer; pDosLinAddr: Pointer):
  Error; stdcall; external 'KrtsDemo.dll';

// ATTENTION: functions for getting shared memory: see 'Base Module'

{$DEFINE KS_INCL_MEMORY_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Clock Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_NO_RDTSC_AVAILABLE                    = (KSERROR_CATEGORY_SPECIAL+$00010000);

  //------ Flags ------
  KSF_USE_GIVEN_FREQ                            = $00010000;
  KSF_CLOCK_DELEGATE                            = $00000001;
  KSF_CLOCK_CONVERT_ONLY                        = $00000002;

  //------ Clock source constants (for 'clockId') ------
  KS_CLOCK_DEFAULT                              = $00000000;
  KS_CLOCK_MEASURE_SYSTEM_TIMER                 = $00000001;
  KS_CLOCK_MEASURE_CPU_COUNTER                  = $00000002;
  KS_CLOCK_MEASURE_APIC_TIMER                   = $00000003;
  KS_CLOCK_MEASURE_PM_TIMER                     = $00000004;
  KS_CLOCK_MEASURE_PC_TIMER                     = $00000005;
  KS_CLOCK_MEASURE_INTERVAL_COUNTER             = $00000006;
  KS_CLOCK_MEASURE_HPET                         = $00000007;
  KS_CLOCK_MEASURE_HIGHEST                      = $00000008;
  KS_CLOCK_CONVERT_HIGHEST                      = $00000009;
  KS_CLOCK_PC_TIMER                             = $0000000a;
  KS_CLOCK_MACHINE_TIME                         = $0000000b;
  KS_CLOCK_ABSOLUTE_TIME                        = $0000000c;
  KS_CLOCK_ABSOLUTE_TIME_LOCAL                  = $0000000d;
  KS_CLOCK_MICROSECONDS                         = $0000000e;
  KS_CLOCK_UNIX_TIME                            = $0000000f;
  KS_CLOCK_UNIX_TIME_LOCAL                      = $00000010;
  KS_CLOCK_MILLISECONDS                         = $00000011;
  KS_CLOCK_DOS_TIME                             = $00000012;
  KS_CLOCK_DOS_TIME_LOCAL                       = $00000013;
  KS_CLOCK_IEEE1588_TIME                        = $00000014;
  KS_CLOCK_IEEE1588_CONVERT                     = $00000015;
  KS_CLOCK_TIME_OF_DAY                          = $00000016;
  KS_CLOCK_TIME_OF_DAY_LOCAL                    = $00000017;
  KS_CLOCK_FIRST_USER                           = $00000018;

  //------ Obsolete clock source constant (use KS_CLOCK_MACHINE_TIME instead!) ------
  KS_CLOCK_SYSTEM_TIME                          = $0000000b;

type
  //------ Types & Structures ------
  KSClockSource = packed record
    structSize: int;
    flags: int;
    frequency: UInt64;
    offset: UInt64;
    baseClockId: uint;
  end;
  PKSClockSource = ^KSClockSource;

//------ Clock functions - Obsolete! Do not use! ------
function KS_calibrateMachineTime(pFrequency: PUInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getSystemTicks(pSystemTicks: PUInt64):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getSystemTime(pSystemTime: PUInt64):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getMachineTime(pMachineTime: PUInt64):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getAbsTime(pAbsTime: PUInt64):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Clock functions ------
function KS_getClock(pClock: PUInt64; clockId: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getClockSource(pClockSource: PKSClockSource; clockId: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setClockSource(pClockSource: PKSClockSource; clockId: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_microDelay(delay: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_convertClock(pValue: PUInt64; clockIdFrom: int; clockIdTo: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_CLOCK_MODULE}

//--------------------------------------------------------------------------------------------------------------
// System Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ System contexts ------
  SYSTEMHANDLER_CONTEXT                         = (USER_CONTEXT+$00000030);

  //------ System events ------
  KS_SYSTEM_CRASH                               = $00000001;
  KS_SYSTEM_SHUTDOWN                            = $00000002;
  KS_SYSTEM_PROCESS_ATTACH                      = $00000010;
  KS_SYSTEM_PROCESS_DETACH                      = $00000020;
  KS_SYSTEM_PROCESS_ABORT                       = $00000040;

type
  //------ Types & Structures ------
  SystemHandlerUserContext = packed record
    ctxType: int;
    eventType: int;
    handlerProcessId: int;
    currentProcessId: int;
  end;
  PSystemHandlerUserContext = ^SystemHandlerUserContext;

//------ System handling functions ------
function KS_installSystemHandler(eventMask: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removeSystemHandler(eventMask: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_SYSTEM_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Device Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_SERIAL_PORT                               = $00200000;

  //------ Function codes ------
  KS_DEVICE_CREATE                              = $00000000;
  KS_DEVICE_CLOSE                               = $00000001;
  KS_DEVICE_READ                                = $00000002;
  KS_DEVICE_WRITE                               = $00000003;
  KS_DEVICE_CONTROL                             = $00000004;
  KS_DEVICE_CLEANUP                             = $00000005;
  KS_DEVICE_CANCEL_REQUEST                      = $00010000;

  //------ Return codes ------
  KS_RETURN_SUCCESS                             = $00000000;
  KS_RETURN_UNSUCCESSFUL                        = $00000001;
  KS_RETURN_INVALID_DEVICE_REQUEST              = $00000002;
  KS_RETURN_INVALID_PARAMETER                   = $00000003;
  KS_RETURN_TIMEOUT                             = $00000004;
  KS_RETURN_CANCELLED                           = $00000005;
  KS_RETURN_PENDING                             = $00000006;
  KS_RETURN_DELETE_PENDING                      = $00000007;
  KS_RETURN_BUFFER_TOO_SMALL                    = $00000008;

type
  //------ Types & Structures ------
  DeviceUserContext = packed record
    ctxType: int;
    hRequest: KSHandle;
    functionCode: int;
    controlCode: int;
    pBuffer: PByte;
    size: uint;
    returnCode: int;
  end;
  PDeviceUserContext = ^DeviceUserContext;

  KSDeviceFunctions = packed record
    hOnCreate: KSHandle;
    hOnClose: KSHandle;
    hOnRead: KSHandle;
    hOnWrite: KSHandle;
    hOnControl: KSHandle;
    hOnCleanup: KSHandle;
  end;
  PKSDeviceFunctions = ^KSDeviceFunctions;

//------ Device functions ------
function KS_createDevice(phDevice: PKSHandle; name: PAnsiChar; pFunctions: PKSDeviceFunctions; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeDevice(hDevice: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_resumeDevice(hDevice: KSHandle; hRequest: KSHandle; pBuffer: PByte; size: int; returnCode: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_DEVICE_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Keyboard Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_SIGNAL_MAKE                               = $00000001;
  KSF_SIGNAL_BREAK                              = $00000002;
  KSF_IGNORE_KEY                                = $00000010;
  KSF_MODIFY_KEY                                = $00000020;

  //------ Key mask modifier ------
  KSF_KEY_SHIFT                                 = $00000011;
  KSF_KEY_SHIFTR                                = $00000001;
  KSF_KEY_SHIFTL                                = $00000010;
  KSF_KEY_CTRL                                  = $00000022;
  KSF_KEY_CTRLR                                 = $00000002;
  KSF_KEY_CTRLL                                 = $00000020;
  KSF_KEY_ALT                                   = $00000044;
  KSF_KEY_ALTR                                  = $00000004;
  KSF_KEY_ALTL                                  = $00000040;
  KSF_KEY_WIN                                   = $00000088;
  KSF_KEY_WINR                                  = $00000008;
  KSF_KEY_WINL                                  = $00000080;

  //------ Key mask groups ------
  KSF_KEY_ALPHA_NUM                             = $00000100;
  KSF_KEY_NUM_PAD                               = $00000200;
  KSF_KEY_CONTROL                               = $00000400;
  KSF_KEY_FUNCTION                              = $00000800;

  //------ Key mask combinations ------
  KSF_KEY_CTRL_ALT_DEL                          = $00010000;
  KSF_KEY_CTRL_ESC                              = $00020000;
  KSF_KEY_ALT_ESC                               = $00040000;
  KSF_KEY_ALT_TAB                               = $00080000;
  KSF_KEY_ALT_ENTER                             = $00100000;
  KSF_KEY_ALT_PRINT                             = $00200000;
  KSF_KEY_ALT_SPACE                             = $00400000;
  KSF_KEY_ESC                                   = $00800000;
  KSF_KEY_TAB                                   = $01000000;
  KSF_KEY_ENTER                                 = $02000000;
  KSF_KEY_PRINT                                 = $04000000;
  KSF_KEY_SPACE                                 = $08000000;
  KSF_KEY_BACKSPACE                             = $10000000;
  KSF_KEY_PAUSE                                 = $20000000;
  KSF_KEY_APP                                   = $40000000;
  KSF_KEY_ALL                                   = $7fffffff;

  KSF_KEY_MODIFIER                              = KSF_KEY_CTRL or KSF_KEY_ALT or
                                                  KSF_KEY_SHIFT or KSF_KEY_WIN;

  KSF_KEY_SYSTEM                                = KSF_KEY_WIN or
                                                  KSF_KEY_CTRL_ALT_DEL or KSF_KEY_CTRL_ESC or
                                                  KSF_KEY_ALT_ESC or KSF_KEY_ALT_TAB;

  KSF_KEY_SYSTEM_DOS                            = KSF_KEY_SYSTEM or
                                                  KSF_KEY_ALT_ENTER or KSF_KEY_ALT_PRINT or
                                                  KSF_KEY_ALT_SPACE or KSF_KEY_PRINT;

  KSF_KEY_ALL_SINGLE                            = KSF_KEY_ALL and $ff80ffff;

  //------ Keyboard events ------
  KS_KEYBOARD_CONTEXT                           = $00000300;

type
  //------ Context structures ------
  KeyboardUserContext = packed record
    ctxType: int;
    mask: int;
    modifier: Word;
    port: Word;
    state: Word;
    key: Word;
  end;
  PKeyboardUserContext = ^KeyboardUserContext;

//------ Keyboard functions ------
function KS_createKeyHandler(mask: uint; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removeKeyHandler(mask: uint):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_simulateKeyStroke(pKeys: PUInt):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_KEYBOARD_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Interrupt Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_IRQ_ALREADY_USED                      = (KSERROR_CATEGORY_INTERRUPT+$00000000);
  KSERROR_CANNOT_INSTALL_HANDLER                = (KSERROR_CATEGORY_INTERRUPT+$00010000);
  KSERROR_BAD_IRQ_INSTALLATION                  = (KSERROR_CATEGORY_INTERRUPT+$00020000);
  KSERROR_NO_HANDLER_INSTALLED                  = (KSERROR_CATEGORY_INTERRUPT+$00030000);
  KSERROR_NOBODY_IS_WAITING                     = (KSERROR_CATEGORY_INTERRUPT+$00040000);
  KSERROR_IRQ_DISABLED                          = (KSERROR_CATEGORY_INTERRUPT+$00050000);
  KSERROR_IRQ_ENABLED                           = (KSERROR_CATEGORY_INTERRUPT+$00060000);
  KSERROR_IRQ_IN_SERVICE                        = (KSERROR_CATEGORY_INTERRUPT+$00070000);
  KSERROR_IRQ_HANDLER_REMOVED                   = (KSERROR_CATEGORY_INTERRUPT+$00080000);

  //------ Flags ------
  KSF_DONT_PASS_NEXT                            = $00020000;
  KSF_PCI_INTERRUPT                             = $00040000;

  //------ Interrupt events ------
  KS_INTERRUPT_CONTEXT                          = $00000100;

type
  //------ Types & Structures ------
  InterruptUserContext = packed record
    ctxType: int;
    hInterrupt: KSHandle;
    irq: uint;
    consumed: uint;
  end;
  PInterruptUserContext = ^InterruptUserContext;

//------ Interrupt handling functions ------
function KS_createDeviceInterrupt(phInterrupt: PKSHandle; pInfo: PKSResourceInfoEx; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_createDeviceInterruptEx(phInterrupt: PKSHandle; deviceName: PAnsiChar; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_createInterrupt(phInterrupt: PKSHandle; irq: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removeInterrupt(hInterrupt: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_disableInterrupt(hInterrupt: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enableInterrupt(hInterrupt: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_simulateInterrupt(hInterrupt: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getInterruptState(hInterrupt: KSHandle; pState: PHandlerState):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_INTERRUPT_MODULE}

//--------------------------------------------------------------------------------------------------------------
// UART Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_CHECK_LINE_ERROR                          = $00008000;
  KSF_NO_FIFO                                   = $00001000;
  KSF_USE_FIFO                                  = $00040000;
  KSF_9_BIT_MODE                                = $00080000;
  KSF_TOGGLE_DTR                                = $00100000;
  KSF_TOGGLE_RTS                                = $00200000;
  KSF_TOGGLE_INVERSE                            = $00400000;

  //------ Uart commands ------
  KS_CLR_DTR                                    = $00000004;
  KS_SET_DTR                                    = $00000005;
  KS_CLR_RTS                                    = $00000006;
  KS_SET_RTS                                    = $00000007;
  KS_CLR_BREAK                                  = $00000008;
  KS_SET_BREAK                                  = $0000000a;
  KS_SKIP_NEXT                                  = $0000000b;
  KS_CHANGE_LINE_CONTROL                        = $0000000d;
  KS_SET_BAUD_RATE_MAX                          = $0000000e;
  KS_ENABLE_FIFO                                = $0000000f;
  KS_DISABLE_FIFO                               = $00000010;
  KS_ENABLE_INTERRUPTS                          = $00000011;
  KS_DISABLE_INTERRUPTS                         = $00000012;
  KS_SET_RECV_FIFO_SIZE                         = $00000013;

  //------ Line Errors ------
  KS_UART_ERROR_PARITY                          = $00000001;
  KS_UART_ERROR_FRAME                           = $00000002;
  KS_UART_ERROR_OVERRUN                         = $00000004;
  KS_UART_ERROR_BREAK                           = $00000008;
  KS_UART_ERROR_FIFO                            = $00000010;

  //------ UART events ------
  KS_UART_RECV                                  = $00000000;
  KS_UART_RECV_ERROR                            = $00000001;
  KS_UART_XMIT                                  = $00000002;
  KS_UART_XMIT_EMPTY                            = $00000003;
  KS_UART_LSR_CHANGE                            = $00000004;
  KS_UART_MSR_CHANGE                            = $00000005;
  KS_UART_BREAK                                 = $00000006;

type
  //------ Types & Structures ------
  UartUserContext = packed record
    ctxType: int;
    hUart: KSHandle;
    value: int;
    lineError: int;
  end;
  PUartUserContext = ^UartUserContext;

  UartRegisterUserContext = packed record
    ctxType: int;
    hUart: KSHandle;
    reg: byte;
  end;
  PUartRegisterUserContext = ^UartRegisterUserContext;

  KSUartProperties = packed record
    baudRate: int;
    parity: byte;
    dataBit: byte;
    stopBit: byte;
    reserved: byte;
  end;
  PKSUartProperties = ^KSUartProperties;

  KSUartState = packed record
    structSize: int;
    properties: KSUartProperties;
    modemState: byte;
    lineState: byte;
    reserved: Word;
    xmitCount: int;
    xmitFree: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    recvLostCount: int;
  end;
  PKSUartState = ^KSUartState;

//------ Common functions ------
function KS_openUart(phUart: PKSHandle; name: PAnsiChar; port: int; pProperties: PKSUartProperties; recvBufferLength: int; xmitBufferLength: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_openUartEx(phUart: PKSHandle; hConnection: KSHandle; port: int; pProperties: PKSUartProperties; recvBufferLength: int; xmitBufferLength: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeUart(hUart: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Communication functions ------
function KS_xmitUart(hUart: KSHandle; value: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvUart(hUart: KSHandle; pValue: PInt; pLineError: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_xmitUartBlock(hUart: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvUartBlock(hUart: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installUartHandler(hUart: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getUartState(hUart: KSHandle; pUartState: PKSUartState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execUartCommand(hUart: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_UART_MODULE}

//--------------------------------------------------------------------------------------------------------------
// COMM Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Line Errors ------
  KS_COMM_ERROR_PARITY                          = $00000001;
  KS_COMM_ERROR_FRAME                           = $00000002;
  KS_COMM_ERROR_OVERRUN                         = $00000004;
  KS_COMM_ERROR_BREAK                           = $00000008;
  KS_COMM_ERROR_FIFO                            = $00000010;

  //------ COMM events ------
  KS_COMM_RECV                                  = $00000000;
  KS_COMM_RECV_ERROR                            = $00000001;
  KS_COMM_XMIT                                  = $00000002;
  KS_COMM_XMIT_EMPTY                            = $00000003;
  KS_COMM_LSR_CHANGE                            = $00000004;
  KS_COMM_MSR_CHANGE                            = $00000005;
  KS_COMM_BREAK                                 = $00000006;

type
  //------ Types & Structures ------
  CommUserContext = packed record
    ctxType: int;
    hComm: KSHandle;
    value: int;
    lineError: int;
  end;
  PCommUserContext = ^CommUserContext;

  CommRegisterUserContext = packed record
    ctxType: int;
    hComm: KSHandle;
    reg: byte;
  end;
  PCommRegisterUserContext = ^CommRegisterUserContext;

  KSCommProperties = packed record
    baudRate: int;
    parity: byte;
    dataBit: byte;
    stopBit: byte;
    reserved: byte;
  end;
  PKSCommProperties = ^KSCommProperties;

  KSCommState = packed record
    structSize: int;
    properties: KSCommProperties;
    modemState: byte;
    lineState: byte;
    reserved: Word;
    xmitCount: int;
    xmitFree: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    recvLostCount: int;
  end;
  PKSCommState = ^KSCommState;

//------ COMM functions ------
function KS_openComm(phComm: PKSHandle; name: PAnsiChar; pProperties: PKSCommProperties; recvBufferLength: int; xmitBufferLength: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeComm(hComm: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_xmitComm(hComm: KSHandle; value: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvComm(hComm: KSHandle; pValue: PInt; pLineError: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_xmitCommBlock(hComm: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvCommBlock(hComm: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installCommHandler(hComm: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getCommState(hComm: KSHandle; pCommState: PKSCommState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execCommCommand(hComm: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_COMM_MODULE}

//--------------------------------------------------------------------------------------------------------------
// USB Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_USB_REQUEST_FAILED                    = (KSERROR_CATEGORY_USB+$00000000);

  //------ Flags ------
  KSF_SOCKET_MODE                               = $00080000;

  //------ USB endpoint commands ------
  KS_USB_RESET_PORT                             = $00000004;
  KS_USB_SET_ISO_PACKET_SIZE                    = $00000005;
  KS_USB_GET_PACKET_SIZE                        = $00000007;
  KS_USB_SET_PACKET_TIMEOUT                     = $00000008;

  //------ USB device commands ------
  KS_USB_GET_DEVICE_DESCRIPTOR                  = $00020000;
  KS_USB_GET_CONFIG_DESCRIPTOR                  = $00030000;
  KS_USB_GET_STRING_DESCRIPTOR                  = $00040000;
  KS_USB_SELECT_CONFIG                          = $00050000;
  KS_USB_SELECT_INTERFACE                       = $00060000;
  KS_USB_GET_CONFIG                             = $00070000;
  KS_USB_GET_INTERFACE                          = $00080000;
  KS_USB_CLR_DEVICE_FEATURE                     = $00090000;
  KS_USB_SET_DEVICE_FEATURE                     = $000a0000;
  KS_USB_CLR_INTERFACE_FEATURE                  = $000b0000;
  KS_USB_SET_INTERFACE_FEATURE                  = $000c0000;
  KS_USB_CLR_ENDPOINT_FEATURE                   = $000d0000;
  KS_USB_SET_ENDPOINT_FEATURE                   = $000e0000;
  KS_USB_GET_STATUS_FROM_DEVICE                 = $000f0000;
  KS_USB_GET_STATUS_FROM_INTERFACE              = $00100000;
  KS_USB_GET_STATUS_FROM_ENDPOINT               = $00110000;
  KS_USB_VENDOR_DEVICE_REQUEST                  = $00120000;
  KS_USB_VENDOR_INTERFACE_REQUEST               = $00130000;
  KS_USB_VENDOR_ENDPOINT_REQUEST                = $00140000;
  KS_USB_CLASS_DEVICE_REQUEST                   = $00150000;
  KS_USB_CLASS_INTERFACE_REQUEST                = $00160000;
  KS_USB_CLASS_ENDPOINT_REQUEST                 = $00170000;

  //------ USB descriptor types ------
  KS_USB_DEVICE                                 = $00000001;
  KS_USB_CONFIGURATION                          = $00000002;
  KS_USB_STRING                                 = $00000003;
  KS_USB_INTERFACE                              = $00000004;
  KS_USB_ENDPOINT                               = $00000005;

  //------ USB end point types ------
  KS_USB_CONTROL                                = $00000000;
  KS_USB_ISOCHRONOUS                            = $00000001;
  KS_USB_BULK                                   = $00000002;
  KS_USB_INTERRUPT                              = $00000003;

  //------ USB port events ------
  KS_USB_SURPRISE_REMOVAL                       = (USB_BASE+$00000000);
  KS_USB_RECV                                   = (USB_BASE+$00000001);
  KS_USB_RECV_ERROR                             = (USB_BASE+$00000002);
  KS_USB_XMIT                                   = (USB_BASE+$00000003);
  KS_USB_XMIT_EMPTY                             = (USB_BASE+$00000004);
  KS_USB_XMIT_ERROR                             = (USB_BASE+$00000005);
  KS_USB_TIMEOUT                                = (USB_BASE+$00000006);

type
  //------ Types & Structures ------
  UsbUserContext = packed record
    ctxType: int;
    hUsb: KSHandle;
    error: int;
  end;
  PUsbUserContext = ^UsbUserContext;

  UsbClassOrVendorRequest = packed record
    request: int;
    value: int;
    index: int;
    pData: Pointer;
    size: int;
    write: int;
  end;
  PUsbClassOrVendorRequest = ^UsbClassOrVendorRequest;

  KS_USB_DEVICE_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bcdUSB: Word;
    bDeviceClass: byte;
    bDeviceSubClass: byte;
    bDeviceProtocol: byte;
    bMaxPacketSize0: byte;
    idVendor: Word;
    idProduct: Word;
    bcdDevice: Word;
    iManufacturer: byte;
    iProduct: byte;
    iSerialNumber: byte;
    bNumConfigurations: byte;
  end;
  PKS_USB_DEVICE_DESCRIPTOR = ^KS_USB_DEVICE_DESCRIPTOR;

  KS_USB_CONFIGURATION_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    wTotalLength: Word;
    bNumInterfaces: byte;
    bConfigurationValue: byte;
    iConfiguration: byte;
    bmAttributes: byte;
    MaxPower: byte;
  end;
  PKS_USB_CONFIGURATION_DESCRIPTOR = ^KS_USB_CONFIGURATION_DESCRIPTOR;

  KS_USB_INTERFACE_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bInterfaceNumber: byte;
    bAlternateSetting: byte;
    bNumEndpoints: byte;
    bInterfaceClass: byte;
    bInterfaceSubClass: byte;
    bInterfaceProtocol: byte;
    iInterface: byte;
  end;
  PKS_USB_INTERFACE_DESCRIPTOR = ^KS_USB_INTERFACE_DESCRIPTOR;

  KS_USB_ENDPOINT_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bEndpointAddress: byte;
    bmAttributes: byte;
    wMaxPacketSize: Word;
    bInterval: byte;
  end;
  PKS_USB_ENDPOINT_DESCRIPTOR = ^KS_USB_ENDPOINT_DESCRIPTOR;

  KS_USB_STRING_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bString: array [0..0] of Word;
  end;
  PKS_USB_STRING_DESCRIPTOR = ^KS_USB_STRING_DESCRIPTOR;

  KS_USB_COMMON_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
  end;
  PKS_USB_COMMON_DESCRIPTOR = ^KS_USB_COMMON_DESCRIPTOR;

  KS_USB_DEVICE_QUALIFIER_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bcdUSB: Word;
    bDeviceClass: byte;
    bDeviceSubClass: byte;
    bDeviceProtocol: byte;
    bMaxPacketSize0: byte;
    bNumConfigurations: byte;
    bReserved: byte;
  end;
  PKS_USB_DEVICE_QUALIFIER_DESCRIPTOR = ^KS_USB_DEVICE_QUALIFIER_DESCRIPTOR;

  KSUsbState = packed record
    structSize: int;
    xmitCount: int;
    xmitFree: int;
    xmitErrorCount: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    packetSize: int;
    isoPacketSize: int;
    packetTimeout: int;
  end;
  PKSUsbState = ^KSUsbState;

//------ Common functions ------
function KS_openUsb(phUsbDevice: PKSHandle; deviceName: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_openUsbEndPoint(hUsbDevice: KSHandle; phUsbEndPoint: PKSHandle; endPointAddress: int; packetSize: int; packetCount: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeUsb(hUsb: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Communication functions ------
function KS_xmitUsb(hUsb: KSHandle; pData: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvUsb(hUsb: KSHandle; pData: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installUsbHandler(hUsb: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getUsbState(hUsb: KSHandle; pUsbState: PKSUsbState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execUsbCommand(hUsb: KSHandle; command: int; index: int; pData: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_USB_MODULE}

//--------------------------------------------------------------------------------------------------------------
// XHCI Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_XHCI_REQUEST_FAILED                   = (KSERROR_CATEGORY_XHCI+$00000000);
  KSERROR_XHCI_HOST_CONTROLLER_ERROR            = (KSERROR_CATEGORY_XHCI+$00010000);
  KSERROR_XHCI_CONFIGURATION_ERROR              = (KSERROR_CATEGORY_XHCI+$00020000);
  KSERROR_XHCI_INTERFACE_ERROR                  = (KSERROR_CATEGORY_XHCI+$00030000);
  KSERROR_XHCI_PACKET_DROPPED                   = (KSERROR_CATEGORY_XHCI+$00040000);
  KSERROR_XHCI_PACKET_ERROR                     = (KSERROR_CATEGORY_XHCI+$00050000);

  //------ XHCI endpoint commands ------
  KS_XHCI_RESET_ENDPOINT                        = $00000001;
  KS_XHCI_GET_PACKET_SIZE                       = $00000002;
  KS_XHCI_CLEAR_RECEIVE_QUEUE                   = $00000003;

  //------ XHCI device commands ------
  KS_XHCI_GET_DEVICE_DESCRIPTOR                 = $00010001;
  KS_XHCI_GET_CONFIG_DESCRIPTOR                 = $00010002;
  KS_XHCI_GET_STRING_DESCRIPTOR                 = $00010003;
  KS_XHCI_SET_CONFIG                            = $00010004;
  KS_XHCI_GET_CONFIG                            = $00010005;
  KS_XHCI_SET_INTERFACE                         = $00010006;
  KS_XHCI_GET_INTERFACE                         = $00010007;

  //------ XHCI controller commands ------
  KS_XHCI_SET_POWER_ROOT_HUB                    = $00020001;

  //------ XHCI descriptor types ------
  KS_XHCI_DEVICE_DESCRIPTOR                     = $00000001;
  KS_XHCI_CONFIGURATION_DESCRIPTOR              = $00000002;
  KS_XHCI_STRING_DESCRIPTOR                     = $00000003;
  KS_XHCI_INTERFACE_DESCRIPTOR                  = $00000004;
  KS_XHCI_ENDPOINT_DESCRIPTOR                   = $00000005;
  KS_XHCI_INTERFACE_ASSOCIATION_DESCRIPTOR      = $0000000b;
  KS_XHCI_ENDPOINT_COMPANION_DESCRIPTOR         = $00000030;

  //------ XHCI endpoint types ------
  KS_XHCI_CONTROL_ENDPOINT                      = $00000000;
  KS_XHCI_ISOCHRONOUS_ENDPOINT                  = $00000001;
  KS_XHCI_BULK_ENDPOINT                         = $00000002;
  KS_XHCI_INTERRUPT_ENDPOINT                    = $00000003;

  //------ XHCI events ------
  KS_XHCI_ADD_DEVICE                            = (XHCI_BASE+$00000000);
  KS_XHCI_REMOVE_DEVICE                         = (XHCI_BASE+$00000001);
  KS_XHCI_RECV                                  = (XHCI_BASE+$00000002);
  KS_XHCI_RECV_ERROR                            = (XHCI_BASE+$00000003);
  KS_XHCI_XMIT_LOW                              = (XHCI_BASE+$00000004);
  KS_XHCI_XMIT_EMPTY                            = (XHCI_BASE+$00000005);
  KS_XHCI_XMIT_ERROR                            = (XHCI_BASE+$00000006);

type
  //------ Context structures ------
  KSXhciContext = packed record
    ctxType: int;
    hXhci: KSHandle;
  end;
  PKSXhciContext = ^KSXhciContext;

  KSXhciAddDeviceContext = packed record
    ctxType: int;
    hXhciController: KSHandle;
    deviceName: array [0..255] of AnsiChar;
  end;
  PKSXhciAddDeviceContext = ^KSXhciAddDeviceContext;

  KSXhciRecvContext = packed record
    ctxType: int;
    hXhciEndPoint: KSHandle;
    pXhciPacketApp: Pointer;
    pXhciPacketSys: Pointer;
    packetSize: int;
  end;
  PKSXhciRecvContext = ^KSXhciRecvContext;

  KSXhciErrorContext = packed record
    ctxType: int;
    hXhci: KSHandle;
    error: int;
  end;
  PKSXhciErrorContext = ^KSXhciErrorContext;

  //------ XHCI common structures ------
  KSXhciControlRequest = packed record
    requestType: int;
    request: int;
    value: int;
    index: int;
  end;
  PKSXhciControlRequest = ^KSXhciControlRequest;

  KSXhciEndPointState = packed record
    structSize: int;
    endPointType: int;
    packetSize: int;
    xmitCount: int;
    xmitFree: int;
    xmitErrorCount: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    recvDroppedCount: int;
  end;
  PKSXhciEndPointState = ^KSXhciEndPointState;

  //------ XHCI Descriptors ------
  KSXhciCommonDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
  end;
  PKSXhciCommonDescriptor = ^KSXhciCommonDescriptor;

  KSXhciDeviceDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bcdUSB: Word;
    bDeviceClass: byte;
    bDeviceSubClass: byte;
    bDeviceProtocol: byte;
    bMaxPacketSize0: byte;
    idVendor: Word;
    idProduct: Word;
    bcdDevice: Word;
    iManufacturer: byte;
    iProduct: byte;
    iSerialNumber: byte;
    bNumConfigurations: byte;
  end;
  PKSXhciDeviceDescriptor = ^KSXhciDeviceDescriptor;

  KSXhciConfigurationDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    wTotalLength: Word;
    bNumInterfaces: byte;
    bConfigurationValue: byte;
    iConfiguration: byte;
    bmAttributes: byte;
    MaxPower: byte;
  end;
  PKSXhciConfigurationDescriptor = ^KSXhciConfigurationDescriptor;

  KSXhciInterfaceDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bInterfaceNumber: byte;
    bAlternateSetting: byte;
    bNumEndpoints: byte;
    bInterfaceClass: byte;
    bInterfaceSubClass: byte;
    bInterfaceProtocol: byte;
    iInterface: byte;
  end;
  PKSXhciInterfaceDescriptor = ^KSXhciInterfaceDescriptor;

  KSXhciInterfaceAssociationDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bFirstInterface: byte;
    bNumInterfaces: byte;
    bFunctionClass: byte;
    bFunctionSubClass: byte;
    bFunctionProtocol: byte;
    iFunction: byte;
  end;
  PKSXhciInterfaceAssociationDescriptor = ^KSXhciInterfaceAssociationDescriptor;

  KSXhciEndpointDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bEndpointAddress: byte;
    bmAttributes: byte;
    wMaxPacketSize: Word;
    bInterval: byte;
  end;
  PKSXhciEndpointDescriptor = ^KSXhciEndpointDescriptor;

  KSXhciEndpointCompanionDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bMaxBurst: byte;
    bmAttributes: byte;
    wBytesPerInterval: Word;
  end;
  PKSXhciEndpointCompanionDescriptor = ^KSXhciEndpointCompanionDescriptor;

  KSXhciStringDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bString: array [0..0] of Word;
  end;
  PKSXhciStringDescriptor = ^KSXhciStringDescriptor;

//------ Common functions ------
function KS_openXhciController(phXhciController: PKSHandle; deviceName: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_openXhciDevice(phXhciDevice: PKSHandle; deviceName: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_openXhciEndPoint(hXhciDevice: KSHandle; phXhciEndPoint: PKSHandle; endPointAddress: int; packetSize: int; packetCount: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeXhci(hXhci: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Communication functions ------
function KS_requestXhciPacket(hXhciEndPoint: KSHandle; ppXhciPacket: PPointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_releaseXhciPacket(hXhciEndPoint: KSHandle; pXhciPacket: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_xmitXhciPacket(hXhciEndPoint: KSHandle; pXhciPacket: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvXhciPacket(hXhciEndPoint: KSHandle; ppXhciPacket: PPointer; pSize: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_sendXhciControlRequest(hXhciDevice: KSHandle; pRequestData: PKSXhciControlRequest; pData: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installXhciHandler(hXhci: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getXhciEndPointState(hXhciEndPoint: KSHandle; pState: PKSXhciEndPointState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execXhciCommand(hXhci: KSHandle; command: int; index: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_XHCI_MODULE}

//--------------------------------------------------------------------------------------------------------------
// RealTime Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_TIMER_NOT_INSTALLED                   = (KSERROR_CATEGORY_TIMER+$00000000);
  KSERROR_TIMER_REMOVED                         = (KSERROR_CATEGORY_TIMER+$00010000);
  KSERROR_TIMER_DISABLED                        = (KSERROR_CATEGORY_TIMER+$00020000);
  KSERROR_TIMER_NOT_DISABLED                    = (KSERROR_CATEGORY_TIMER+$00030000);
  KSERROR_CANNOT_INSTALL_TIMER                  = (KSERROR_CATEGORY_TIMER+$00040000);
  KSERROR_NO_TIMER_AVAILABLE                    = (KSERROR_CATEGORY_TIMER+$00050000);
  KSERROR_WAIT_CANCELLED                        = (KSERROR_CATEGORY_TIMER+$00060000);
  KSERROR_TIMER_ALREADY_RUNNING                 = (KSERROR_CATEGORY_TIMER+$00070000);
  KSERROR_TIMER_ALREADY_STOPPED                 = (KSERROR_CATEGORY_TIMER+$00080000);
  KSERROR_CANNOT_START_TIMER                    = (KSERROR_CATEGORY_TIMER+$00090000);

  //------ Flags ------
  KSF_DISTINCT_MODE                             = $00040000;
  KSF_DRIFT_CORRECTION                          = $00010000;
  KSF_DISABLE_PROTECTION                        = $00020000;
  KSF_USE_IEEE1588_TIME                         = $00040000;

  //------ Timer module config ------
  KSCONFIG_SOFTWARE_TIMER                       = $00000001;

  //------ Timer events ------
  KS_TIMER_CONTEXT                              = $00000200;

type
  //------ Context structures ------
  TimerUserContext = packed record
    ctxType: int;
    hTimer: KSHandle;
    delay: int;
  end;
  PTimerUserContext = ^TimerUserContext;

//------ Real-time functions ------
function KS_createTimer(phTimer: PKSHandle; delay: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removeTimer(hTimer: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_cancelTimer(hTimer: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_startTimer(hTimer: KSHandle; flags: int; delay: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_stopTimer(hTimer: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_startTimerDelayed(hTimer: KSHandle; var start: Int64; period: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_adjustTimer(hTimer: KSHandle; period: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Obsolete timer functions - do not use! ------
function KS_disableTimer(hTimer: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enableTimer(hTimer: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getTimerState(hTimer: KSHandle; pState: PHandlerState):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Obsolete timer resolution functions - do not use! ------
function KS_setTimerResolution(resolution: uint; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getTimerResolution(pResolution: PUInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_REALTIME_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Task Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_CPU_BOOT_FAILURE                      = (KSERROR_CATEGORY_TASK+$00000000);

  //------ Task states ------
  KS_TASK_DORMANT                               = $00000001;
  KS_TASK_READY                                 = $00000002;
  KS_TASK_RUNNING                               = $00000004;
  KS_TASK_WAITING                               = $00000008;
  KS_TASK_SUSPENDED                             = $00000010;
  KS_TASK_TERMINATED                            = $00000020;

  //------ Flags ------
  KSF_NUM_PROCESSORS                            = $00000040;
  KSF_NON_INTERRUPTABLE                         = $00040000;
  KSF_CUSTOM_STACK_SIZE                         = $00004000;

  //------ Task contexts ------
  TASK_CONTEXT                                  = (TASK_BASE+$00000000);

type
  //------ Common structure ------
  KSContextInformation = packed record
    structSize: int;
    currentLevel: int;
    taskHandle: KSHandle;
    taskPriority: int;
    currentCpuNumber: int;
    stackSize: int;
    stackUsage: int;
  end;
  PKSContextInformation = ^KSContextInformation;

  //------ Context structure ------
  TaskUserContext = packed record
    ctxType: int;
    hTask: KSHandle;
    priority: int;
  end;
  PTaskUserContext = ^TaskUserContext;

  TaskErrorUserContext = packed record
    ctxType: int;
    error: Error;
    param1: Pointer;
    param2: Pointer;
    param3: Pointer;
    param4: Pointer;
    hTask: KSHandle;
    kernel: array [0..63] of AnsiChar;
    functionName: array [0..63] of AnsiChar;
    functionOffset: int;
  end;
  PTaskErrorUserContext = ^TaskErrorUserContext;

//------ Task functions ------
function KS_createTask(phTask: PKSHandle; hCallBack: KSHandle; priority: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removeTask(hTask: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_exitTask(hTask: KSHandle; exitCode: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_killTask(hTask: KSHandle; exitCode: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_terminateTask(hTask: KSHandle; timeout: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_suspendTask(hTask: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_resumeTask(hTask: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_triggerTask(hTask: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_yieldTask:
  Error; stdcall; external 'KrtsDemo.dll';
function KS_sleepTask(delay: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setTaskPrio(hTask: KSHandle; newPrio: int; pOldPrio: PInt):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setTaskPriority(hObject: KSHandle; priority: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setTaskPriorityRelative(hObject: KSHandle; hRelative: KSHandle; distance: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getTaskPriority(hObject: KSHandle; pPriority: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getTaskState(hTask: KSHandle; pTaskState: PUInt; pExitCode: PInt):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setTaskStackSize(size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ MultiCore & information functions ------
function KS_setTargetProcessor(processor: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getCurrentProcessor(pProcessor: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getCurrentContext(pContextInfo: PKSContextInformation; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Semaphore functions ------
function KS_createSemaphore(phSemaphore: PKSHandle; maxCount: int; initCount: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_removeSemaphore(hSemaphore: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_requestSemaphore(hSemaphore: KSHandle; timeout: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_releaseSemaphore(hSemaphore: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_TASK_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Dedicated Module
//--------------------------------------------------------------------------------------------------------------

//------ Dedicated functions ------

{$DEFINE KS_INCL_DEDICATED_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Packet Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_INIT_PHY_ERROR                        = (KSERROR_CATEGORY_PACKET+$00000000);
  KSERROR_INIT_MAC_ERROR                        = (KSERROR_CATEGORY_PACKET+$00010000);
  KSERROR_MAC_STARTUP_ERROR                     = (KSERROR_CATEGORY_PACKET+$00020000);

  //------ Flags ------
  KSF_SUPPORT_JUMBO_FRAMES                      = $00008000;
  KSF_DELAY_RECEIVE_INTERRUPT                   = $00080000;
  KSF_NO_RECEIVE_INTERRUPT                      = $00000020;
  KSF_NO_TRANSMIT_INTERRUPT                     = $00000040;
  KSF_RAW_ETHERNET_PACKET                       = $00100000;
  KSF_SAFE_START                                = $00200000;
  KSF_CHECK_IP_CHECKSUM                         = $00400000;
  KSF_ACCEPT_ALL                                = $00800000;
  KSF_FORCE_MASTER                              = $00000001;
  KSF_FORCE_100_MBIT                            = $00000002;
  KSF_WINDOWS_CONNECTION                        = $00004000;

  //------ Packet events ------
  KS_PACKET_RECV                                = (PACKET_BASE+$00000000);
  KS_PACKET_LINK_CHANGE                         = (PACKET_BASE+$00000001);
  KS_PACKET_SEND_EMPTY                          = (PACKET_BASE+$00000002);
  KS_PACKET_SEND_LOW                            = (PACKET_BASE+$00000003);

  //------ Adapter commands ------
  KS_PACKET_ABORT_SEND                          = $00000001;
  KS_PACKET_ADD_ARP_TABLE_ENTRY                 = $00000002;
  KS_PACKET_SET_IP_CONFIG                       = $00000003;
  KS_PACKET_SET_MAC_MULTICAST                   = $00000004;
  KS_PACKET_SET_IP_MULTICAST                    = $00000005;
  KS_PACKET_SET_MAC_CONFIG                      = $00000006;
  KS_PACKET_CLEAR_RECEIVE_QUEUE                 = $00000007;
  KS_PACKET_GET_IP_CONFIG                       = $00000008;
  KS_PACKET_GET_MAC_CONFIG                      = $00000009;
  KS_PACKET_GET_MAXIMUM_SIZE                    = $0000000a;

  //------ Connection flags ------
  KS_PACKET_SPEED_10_MBIT                       = $00010000;
  KS_PACKET_SPEED_100_MBIT                      = $00020000;
  KS_PACKET_SPEED_1000_MBIT                     = $00040000;
  KS_PACKET_SPEED_10_GBIT                       = $00080000;
  KS_PACKET_DUPLEX_FULL                         = $00000001;
  KS_PACKET_DUPLEX_HALF                         = $00000002;
  KS_PACKET_MASTER                              = $00000004;
  KS_PACKET_SLAVE                               = $00000008;

type
  //------ Types & Structures ------
  PacketUserContext = packed record
    ctxType: int;
    hAdapter: KSHandle;
    pPacketApp: Pointer;
    pPacketSys: Pointer;
    length: int;
  end;
  PPacketUserContext = ^PacketUserContext;

  PacketAdapterUserContext = packed record
    ctxType: int;
    hAdapter: KSHandle;
  end;
  PPacketAdapterUserContext = ^PacketAdapterUserContext;

  PacketLinkChangeUserContext = packed record
    ctxType: int;
    hAdapter: KSHandle;
    connection: uint;
  end;
  PPacketLinkChangeUserContext = ^PacketLinkChangeUserContext;

  KSMACHeader = packed record
    destination: array [0..5] of byte;
    source: array [0..5] of byte;
    typeOrLength: Word;
  end;
  PKSMACHeader = ^KSMACHeader;

  KSIPHeader = packed record
    header1: uint;
    header2: uint;
    header3: uint;
    srcIpAddress: uint;
    dstIpAddress: uint;
    header6: uint;
  end;
  PKSIPHeader = ^KSIPHeader;

  KSUDPHeader = packed record
    srcPort: Word;
    dstPort: Word;
    msgLength: Word;
    checkSum: Word;
  end;
  PKSUDPHeader = ^KSUDPHeader;

  KSIPConfig = packed record
    localAddress: uint;
    subnetMask: uint;
    gatewayAddress: uint;
  end;
  PKSIPConfig = ^KSIPConfig;

  KSARPTableEntry = packed record
    ipAddress: uint;
    macAddress: array [0..5] of byte;
  end;
  PKSARPTableEntry = ^KSARPTableEntry;

  KSAdapterState = packed record
    structSize: int;
    numPacketsSendOk: int;
    numPacketsSendError: int;
    numPacketsSendARPTimeout: int;
    numPacketsWaitingForARP: int;
    numPacketsRecvOk: int;
    numPacketsRecvError: int;
    numPacketsRecvDropped: int;
    connection: uint;
    macAddress: array [0..5] of byte;
    connected: byte;
  end;
  PKSAdapterState = ^KSAdapterState;

//------ Helper functions ------
function mk16(lo, hi : byte):
  word;
function mk32(lo, hi : word):
  uint;
function KS_htons(val : word):
  word;
function KS_htonl(val : uint):
  uint;
function KS_ntohs(val : word):
  word;
function KS_ntohl(val : uint):
  uint;
procedure KS_makeIPv4(pAddr : PUint; b3, b2, b1, b0 : byte);

//------ Common functions ------
function KS_openAdapter(phAdapter: PKSHandle; name: PAnsiChar; recvPoolLength: int; sendPoolLength: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_openAdapterEx(phAdapter: PKSHandle; hConnection: KSHandle; recvPoolLength: int; sendPoolLength: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeAdapter(hAdapter: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getAdapterState(hAdapter: KSHandle; pAdapterState: PKSAdapterState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execAdapterCommand(hAdapter: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_aggregateAdapter(hAdapter: KSHandle; hLink: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Communication functions ------
function KS_requestPacket(hAdapter: KSHandle; ppPacket: PPointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_releasePacket(hAdapter: KSHandle; pPacket: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_sendPacket(hAdapter: KSHandle; pPacket: Pointer; size: uint; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvPacket(hAdapter: KSHandle; ppPacket: PPointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvPacketEx(hAdapter: KSHandle; ppPacket: PPointer; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installPacketHandler(hAdapter: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_PACKET_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Socket Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_CLIENT                                    = $00000020;
  KSF_SERVER                                    = $00000040;
  KSF_ACCEPT_BROADCAST                          = $00800000;
  KSF_INSERT_CHECKSUM                           = $00200000;
  KSF_CHECK_CHECKSUM                            = $00400000;

  //------ Family types ------
  KS_FAMILY_IPV4                                = $00000004;

  //------ Protocol types ------
  KS_PROTOCOL_RAW                               = $00000000;
  KS_PROTOCOL_UDP                               = $00000001;
  KS_PROTOCOL_TCP                               = $00000002;

  //------ Socket events ------
  KS_SOCKET_CONNECTED                           = (SOCKET_BASE+$00000000);
  KS_SOCKET_DISCONNECTED                        = (SOCKET_BASE+$00000001);
  KS_SOCKET_RECV                                = (SOCKET_BASE+$00000002);
  KS_SOCKET_CLEAR_TO_SEND                       = (SOCKET_BASE+$00000003);
  KS_SOCKET_SEND_EMPTY                          = (SOCKET_BASE+$00000004);
  KS_SOCKET_SEND_LOW                            = (SOCKET_BASE+$00000005);

  //------ Socket commands ------
  KS_SOCKET_SET_MTU                             = $00000001;

type
  //------ Types & Structures ------
  KSSocketAddr = packed record
    family: Word;
    port: Word;
    address: array [0..0] of uint;
  end;
  PKSSocketAddr = ^KSSocketAddr;

  SocketUserContext = packed record
    ctxType: int;
    hSocket: KSHandle;
  end;
  PSocketUserContext = ^SocketUserContext;

//------ Socket functions ------
function KS_openSocket(phSocket: PKSHandle; hAdapter: KSHandle; pSocketAddr: PKSSocketAddr; protocol: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeSocket(hSocket: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvFromSocket(hSocket: KSHandle; pSourceAddr: PKSSocketAddr; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_sendToSocket(hSocket: KSHandle; pTargetAddr: PKSSocketAddr; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvSocket(hSocket: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_sendSocket(hSocket: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installSocketHandler(hSocket: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_connectSocket(hClient: KSHandle; pServerAddr: PKSSocketAddr; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_acceptSocket(hServer: KSHandle; pClientAddr: PKSSocketAddr; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execSocketCommand(hSocket: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_SOCKET_MODULE}

//--------------------------------------------------------------------------------------------------------------
// EtherCAT Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_EMERGENCY_REQUEST                     = (KSERROR_CATEGORY_ETHERCAT+$00000000);
  KSERROR_LINK_CHANGE                           = (KSERROR_CATEGORY_ETHERCAT+$00010000);
  KSERROR_TOPOLOGY_CHANGE                       = (KSERROR_CATEGORY_ETHERCAT+$00020000);
  KSERROR_SLAVE_ERROR                           = (KSERROR_CATEGORY_ETHERCAT+$00030000);
  KSERROR_DATA_INCOMPLETE                       = (KSERROR_CATEGORY_ETHERCAT+$00040000);
  KSERROR_PHYSICAL_ACCESS_FAILED                = (KSERROR_CATEGORY_ETHERCAT+$00050000);
  KSERROR_FMMU_ERROR                            = (KSERROR_CATEGORY_ETHERCAT+$00060000);
  KSERROR_DATA_LINK_ERROR                       = (KSERROR_CATEGORY_ETHERCAT+$00070000);
  KSERROR_SII_ERROR                             = (KSERROR_CATEGORY_ETHERCAT+$00080000);
  KSERROR_MAILBOX_ERROR                         = (KSERROR_CATEGORY_ETHERCAT+$00090000);
  KSERROR_APPLICATION_LAYER_ERROR               = (KSERROR_CATEGORY_ETHERCAT+$000a0000);
  KSERROR_SYNC_MANAGER_ERROR                    = (KSERROR_CATEGORY_ETHERCAT+$000b0000);
  KSERROR_DC_CONTROL_ERROR                      = (KSERROR_CATEGORY_ETHERCAT+$000c0000);
  KSERROR_SOE_REQUEST_ERROR                     = (KSERROR_CATEGORY_ETHERCAT+$000e0000);
  KSERROR_MISSING_XML_INFORMATION               = (KSERROR_CATEGORY_ETHERCAT+$000f0000);
  KSERROR_SAFETY_ERROR                          = (KSERROR_CATEGORY_ETHERCAT+$00100000);
  KSERROR_DC_CONFIGURATION_ERROR                = (KSERROR_CATEGORY_ETHERCAT+$00110000);
  KSERROR_COE_ERROR                             = (KSERROR_CATEGORY_ETHERCAT+$00120000);

  //------ Flags ------
  KSF_FORCE_UPDATE                              = $00000008;
  KSF_SECONDARY                                 = $00000020;
  KSF_SYNC_DC_GLOBAL                            = $00000040;
  KSF_SYNC_DC_LOCAL                             = $00800000;
  KSF_BIG_DATASET                               = $00004000;
  KSF_STARTUP_WITHOUT_INIT_STATE                = $00008000;
  KSF_COMPLETE_ACCESS                           = $00200000;
  KSF_IGNORE_INIT_CMDS                          = $01000000;
  KSF_STRICT_XML_CHECK                          = $00000040;

  //------ DataObj type flags ------
  KS_DATAOBJ_PDO_TYPE                           = $00000001;
  KS_DATAOBJ_SDO_TYPE                           = $00000002;
  KS_DATAOBJ_IDN_TYPE                           = $00000003;
  KS_DATAOBJ_ACTIVE                             = $00040000;
  KS_DATAOBJ_MAPPABLE                           = $00080000;
  KS_DATAOBJ_SAFETY                             = $00100000;

  //------ State type ------
  KS_ECAT_STATE_INIT                            = $00000001;
  KS_ECAT_STATE_PREOP                           = $00000002;
  KS_ECAT_STATE_BOOT                            = $00000003;
  KS_ECAT_STATE_SAFEOP                          = $00000004;
  KS_ECAT_STATE_OP                              = $00000008;

  //------ State type (legacy, do not use anymore) ------
  KS_STATE_INIT                                 = $00000001;
  KS_STATE_PREOP                                = $00000002;
  KS_STATE_BOOT                                 = $00000003;
  KS_STATE_SAFEOP                               = $00000004;
  KS_STATE_OP                                   = $00000008;

  //------ Sync manager index commons ------
  KS_ECAT_SYNC_ALL                              = Integer($ffffffff);
  KS_ECAT_SYNC_INPUT                            = Integer($fffffffe);
  KS_ECAT_SYNC_OUTPUT                           = Integer($fffffffd);

  //------ EtherCAT events (legacy, do not use anymore) ------
  KS_ETHERCAT_ERROR                             = (ETHERCAT_BASE+$00000000);
  KS_DATASET_SIGNAL                             = (ETHERCAT_BASE+$00000001);
  KS_TOPOLOGY_CHANGE                            = (ETHERCAT_BASE+$00000002);

  //------ EtherCAT events ------
  KS_ECAT_ERROR                                 = (ETHERCAT_BASE+$00000000);
  KS_ECAT_DATASET_SIGNAL                        = (ETHERCAT_BASE+$00000001);
  KS_ECAT_TOPOLOGY_CHANGE                       = (ETHERCAT_BASE+$00000002);
  KS_ECAT_STATE_CHANGE                          = (ETHERCAT_BASE+$00000003);
  KS_ECAT_OBJECT_ACCESS                         = (ETHERCAT_BASE+$00000004);
  KS_ECAT_FILE_ACCESS                           = (ETHERCAT_BASE+$00000005);
  KS_ECAT_PROCESS_DATA_ACCESS                   = (ETHERCAT_BASE+$00000006);
  KS_ECAT_REDUNDANCY_EFFECTIVE                  = (ETHERCAT_BASE+$00000007);

  //------ Topology change reasons ------
  KS_ECAT_TOPOLOGY_MASTER_CONNECTED             = $00000000;
  KS_ECAT_TOPOLOGY_MASTER_DISCONNECTED          = $00000001;
  KS_ECAT_TOPOLOGY_SLAVE_COUNT_CHANGED          = $00000002;
  KS_ECAT_TOPOLOGY_SLAVE_ONLINE                 = $00000003;
  KS_ECAT_TOPOLOGY_SLAVE_OFFLINE                = $00000004;

  //------ DataSet commands ------
  KS_ECAT_GET_BUFFER_SIZE                       = $00000001;

  //------ Slave commands ------
  KS_ECAT_READ_SYNC0_STATUS                     = $00000001;
  KS_ECAT_READ_SYNC1_STATUS                     = $00000002;
  KS_ECAT_READ_LATCH0_STATUS                    = $00000003;
  KS_ECAT_READ_LATCH1_STATUS                    = $00000004;
  KS_ECAT_READ_LATCH_TIMESTAMPS                 = $00000005;
  KS_ECAT_ENABLE_CYCLIC_MAILBOX_CHECK           = $00000006;
  KS_ECAT_GET_PDO_OVERSAMPLING_FACTOR           = $00000007;
  KS_ECAT_SET_SYNCMANAGER_WATCHDOGS             = $00000008;
  KS_ECAT_GET_SYNCMANAGER_WATCHDOGS             = $00000009;
  KS_ECAT_HAS_XML_INFORMATION                   = $0000000a;

  //------ Slave Device / EAP-Node commands ------
  KS_ECAT_SIGNAL_AL_ERROR                       = $00000001;

type
  //------ EtherCAT data structures ------
  KSEcatMasterState = packed record
    structSize: int;
    connected: int;
    slavesOnline: int;
    slavesCreated: int;
    slavesCreatedAndOnline: int;
    masterState: int;
    lastError: int;
    redundancyEffective: int;
    framesSent: int;
    framesLost: int;
  end;
  PKSEcatMasterState = ^KSEcatMasterState;

  KSEcatSlaveState = packed record
    structSize: int;
    online: int;
    created: int;
    vendor: int;
    product: int;
    revision: int;
    id: int;
    relPosition: int;
    absPosition: int;
    linkState: int;
    slaveState: int;
    statusCode: int;
  end;
  PKSEcatSlaveState = ^KSEcatSlaveState;

  KSEcatDataVarInfo = packed record
    objType: int;
    name: PAnsiChar;
    dataType: int;
    bitLength: int;
    subIndex: int;
  end;
  PKSEcatDataVarInfo = ^KSEcatDataVarInfo;

  PPKSEcatDataVarInfo = ^PKSEcatDataVarInfo;

  KSEcatDataObjInfo = packed record
    objType: int;
    name: PAnsiChar;
    bitLength: int;
    index: int;
    syncIndex: int;
    varCount: int;
    vars: array [0..0] of PKSEcatDataVarInfo;
  end;
  PKSEcatDataObjInfo = ^KSEcatDataObjInfo;

  PPKSEcatDataObjInfo = ^PKSEcatDataObjInfo;

  KSEcatSlaveInfo = packed record
    vendorId: int;
    productId: int;
    revision: int;
    serial: int;
    group: PAnsiChar;
    image: PAnsiChar;
    order: PAnsiChar;
    name: PAnsiChar;
    objCount: int;
    objs: array [0..0] of PKSEcatDataObjInfo;
  end;
  PKSEcatSlaveInfo = ^KSEcatSlaveInfo;

  PPKSEcatSlaveInfo = ^PKSEcatSlaveInfo;

  KSEcatDcParams = packed record
    assignActivate: Word;
    latch0Control: byte;
    latch1Control: byte;
    pulseLength: int;
    cycleTimeSync0: int;
    cycleTimeSync0Factor: int;
    shiftTimeSync0: int;
    shiftTimeSync0Factor: int;
    cycleTimeSync1: int;
    cycleTimeSync1Factor: int;
    shiftTimeSync1: int;
    shiftTimeSync1Factor: int;
  end;
  PKSEcatDcParams = ^KSEcatDcParams;

  KSEcatDcLatchTimeStamps = packed record
    latch0PositivEdge: Int64;
    latch0NegativEdge: Int64;
    latch1PositivEdge: Int64;
    latch1NegativEdge: Int64;
    smBufferChange: Int64;
    smPdiBufferStart: Int64;
    smPdiBufferChange: Int64;
  end;
  PKSEcatDcLatchTimeStamps = ^KSEcatDcLatchTimeStamps;

  KSEcatEapNodeState = packed record
    structSize: int;
    connected: int;
    proxiesOnline: int;
    proxiesCreated: int;
    proxiesCreatedAndOnline: int;
    nodeState: int;
    statusCode: int;
  end;
  PKSEcatEapNodeState = ^KSEcatEapNodeState;

  KSEcatEapProxyState = packed record
    structSize: int;
    online: int;
    created: int;
    ip: uint;
    proxyState: int;
    statusCode: int;
  end;
  PKSEcatEapProxyState = ^KSEcatEapProxyState;

  KSEcatEapNodeId = packed record
    structSize: int;
    ip: uint;
  end;
  PKSEcatEapNodeId = ^KSEcatEapNodeId;

  KSEcatSlaveDeviceState = packed record
    structSize: int;
    slaveState: int;
    statusCode: int;
  end;
  PKSEcatSlaveDeviceState = ^KSEcatSlaveDeviceState;

  //------ Context structures ------
  EcatErrorUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    error: int;
  end;
  PEcatErrorUserContext = ^EcatErrorUserContext;

  EcatDataSetUserContext = packed record
    ctxType: int;
    hDataSet: KSHandle;
  end;
  PEcatDataSetUserContext = ^EcatDataSetUserContext;

  EcatTopologyUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    slavesOnline: int;
    slavesCreated: int;
    slavesCreatedAndOnline: int;
    reason: int;
    hSlave: KSHandle;
  end;
  PEcatTopologyUserContext = ^EcatTopologyUserContext;

  EcatRedundancyUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    redundancyEffective: int;
  end;
  PEcatRedundancyUserContext = ^EcatRedundancyUserContext;

  EcatStateChangeUserContext = packed record
    ctxType: int;
    hObject: KSHandle;
    currentState: int;
    requestedState: int;
  end;
  PEcatStateChangeUserContext = ^EcatStateChangeUserContext;

  EcatObjectAccessUserContext = packed record
    ctxType: int;
    hObject: KSHandle;
    index: int;
    subIndex: int;
    writeAccess: int;
    completeAccess: int;
  end;
  PEcatObjectAccessUserContext = ^EcatObjectAccessUserContext;

  EcatFileAccessUserContext = packed record
    ctxType: int;
    hObject: KSHandle;
    fileName: array [0..255] of AnsiChar;
    password: int;
    writeAccess: int;
  end;
  PEcatFileAccessUserContext = ^EcatFileAccessUserContext;

  EcatProcessDataAccessUserContext = packed record
    ctxType: int;
    hObject: KSHandle;
  end;
  PEcatProcessDataAccessUserContext = ^EcatProcessDataAccessUserContext;

//------ Master functions ------
function KS_createEcatMaster(phMaster: PKSHandle; hConnection: KSHandle; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeEcatMaster(hMaster: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installEcatMasterHandler(hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryEcatMasterState(hMaster: KSHandle; pMasterState: PKSEcatMasterState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_changeEcatMasterState(hMaster: KSHandle; state: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_addEcatRedundancy(hMaster: KSHandle; hConnection: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Slave functions ------
function KS_createEcatSlave(hMaster: KSHandle; phSlave: PKSHandle; id: int; position: int; vendor: int; product: int; revision: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_createEcatSlaveIndirect(hMaster: KSHandle; phSlave: PKSHandle; pSlaveState: PKSEcatSlaveState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumEcatSlaves(hMaster: KSHandle; index: int; pSlaveState: PKSEcatSlaveState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_deleteEcatSlave(hSlave: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_writeEcatSlaveId(hSlave: KSHandle; id: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryEcatSlaveState(hSlave: KSHandle; pSlaveState: PKSEcatSlaveState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_changeEcatSlaveState(hSlave: KSHandle; state: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryEcatSlaveInfo(hSlave: KSHandle; ppSlaveInfo: PPKSEcatSlaveInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryEcatDataObjInfo(hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSEcatDataObjInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryEcatDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSEcatDataVarInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumEcatDataObjInfo(hSlave: KSHandle; objEnumIndex: int; ppDataObjInfo: PPKSEcatDataObjInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumEcatDataVarInfo(hSlave: KSHandle; objEnumIndex: int; varEnumIndex: int; ppDataVarInfo: PPKSEcatDataVarInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_loadEcatInitCommands(hSlave: KSHandle; filename: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ DataSet related functions ------
function KS_createEcatDataSet(hObject: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_deleteEcatDataSet(hSet: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_assignEcatDataSet(hSet: KSHandle; hObject: KSHandle; syncIndex: int; placement: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readEcatDataSet(hSet: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_postEcatDataSet(hSet: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installEcatDataSetHandler(hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Common functions ------
function KS_changeEcatState(hObject: KSHandle; state: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installEcatHandler(hObject: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Data exchange functions ------
function KS_getEcatDataObjAddress(hSet: KSHandle; hObject: KSHandle; pdoIndex: int; varIndex: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readEcatDataObj(hObject: KSHandle; objIndex: int; varIndex: int; pData: Pointer; pSize: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_postEcatDataObj(hObject: KSHandle; objIndex: int; varIndex: int; pData: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setEcatPdoAssign(hObject: KSHandle; syncIndex: int; pdoIndex: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setEcatPdoMapping(hObject: KSHandle; pdoIndex: int; objIndex: int; varIndex: int; bitLength: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_uploadEcatFile(hObject: KSHandle; fileName: PAnsiChar; password: int; pData: Pointer; pSize: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_downloadEcatFile(hObject: KSHandle; fileName: PAnsiChar; password: int; pData: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readEcatSlaveMem(hSlave: KSHandle; address: int; pData: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_writeEcatSlaveMem(hSlave: KSHandle; address: int; pData: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ DC functions ------
function KS_activateEcatDcMode(hObject: KSHandle; var startTime: Int64; cycleTime: int; shiftTime: int; hTimer: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumEcatDcOpModes(hSlave: KSHandle; index: int; pDcOpModeBuf: PAnsiChar; pDescriptionBuf: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_lookupEcatDcOpMode(hSlave: KSHandle; dcOpMode: PAnsiChar; pDcParams: PKSEcatDcParams; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_configEcatDcOpMode(hSlave: KSHandle; dcOpMode: PAnsiChar; pDcParams: PKSEcatDcParams; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Auxiliary functions ------
function KS_execEcatCommand(hObject: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getEcatAlias(hObject: KSHandle; objIndex: int; varIndex: int; pAliasBuf: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setEcatAlias(hObject: KSHandle; objIndex: int; varIndex: int; alias: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ EtherCAT Slave Device functions ------
function KS_openEcatSlaveDevice(phSlaveDevice: PKSHandle; name: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeEcatSlaveDevice(hSlaveDevice: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryEcatSlaveDeviceState(hSlaveDevice: KSHandle; pSlaveState: PKSEcatSlaveDeviceState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ EtherCAT Automation Protocol functions ------
function KS_createEcatEapNode(phNode: PKSHandle; hConnection: KSHandle; networkFile: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeEcatEapNode(hNode: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryEcatEapNodeState(hNode: KSHandle; pNodeState: PKSEcatEapNodeState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_createEcatEapProxy(hNode: KSHandle; phProxy: PKSHandle; pNodeId: PKSEcatEapNodeId; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_deleteEcatEapProxy(hProxy: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryEcatEapProxyState(hProxy: KSHandle; pProxyState: PKSEcatEapProxyState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

// ATTENTION! The API of the older version 1.0 of the Kithara EtherCAT Master is not contained here! Please
// download the header file _ethercat_v1.pas from www.kithara.de and include this file additionally!

{$DEFINE KS_INCL_ETHERCAT_MODULE}

//--------------------------------------------------------------------------------------------------------------
// MultiFunction Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_UNIPOLAR                                  = $00000001;
  KSF_BIPOLAR                                   = $00000000;
  KSF_DIFFERENTIAL                              = $00000002;
  KSF_SINGLE_ENDED                              = $00000000;
  KSF_RISING_EDGE                               = $00001000;
  KSF_FALLING_EDGE                              = $00000000;
  KSF_NORMALIZE                                 = $00000800;
  KSF_STOP_ANALOG_STREAM                        = $00000400;

  //------ Multi function events ------
  KS_MULTIFUNCTION_INTERRUPT                    = (MULTIFUNCTION_BASE+$00000000);
  KS_MULTIFUNCTION_ANALOGWATCHDOG               = (MULTIFUNCTION_BASE+$00000001);
  KS_MULTIFUNCTION_DIGITALWATCHDOG              = (MULTIFUNCTION_BASE+$00000002);
  KS_MULTIFUNCTION_TIMER                        = (MULTIFUNCTION_BASE+$00000003);
  KS_MULTIFUNCTION_ANALOG_SEQUENCE              = (MULTIFUNCTION_BASE+$00000004);
  KS_MULTIFUNCTION_ANALOG_STREAM                = (MULTIFUNCTION_BASE+$00000006);
  KS_MULTIFUNCTION_DIGITAL_IN                   = (MULTIFUNCTION_BASE+$00000007);
  KS_MULTIFUNCTION_BUFFER_FULL                  = (MULTIFUNCTION_BASE+$00000008);

  //------ Board commands ------
  KS_BOARD_RESET                                = $00000000;
  KS_BOARD_SEQUENCE_READY                       = $00000001;

type
  //------ Context structures ------
  BoardUserContext = packed record
    ctxType: int;
    hBoard: KSHandle;
  end;
  PBoardUserContext = ^BoardUserContext;

  BoardStreamErrorContext = packed record
    ctxType: int;
    hBoard: KSHandle;
    lostDataCount: int;
  end;
  PBoardStreamErrorContext = ^BoardStreamErrorContext;

  KSMfBoardInfo = packed record
    structSize: int;
    pBoardName: array [0..63] of AnsiChar;
    pBoardSpec: array [0..255] of AnsiChar;
    analogInp: int;
    analogOut: int;
    digitalInOut: int;
    digitalInp: int;
    digitalOut: int;
    digitalPortSize: int;
    analogValueSize: int;
  end;
  PKSMfBoardInfo = ^KSMfBoardInfo;

//------ Basic functions ------
function KS_openBoard(phBoard: PKSHandle; name: PAnsiChar; hKernel: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeBoard(hBoard: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getBoardInfo(hBoard: KSHandle; pBoardInfo: PKSMfBoardInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setBoardObject(hBoard: KSHandle; pAppAddr: Pointer; pSysAddr: Pointer):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getBoardObject(hBoard: KSHandle; ppAddr: PPointer):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installBoardHandler(hBoard: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execBoardCommand(hBoard: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Low-level functions ------
function KS_readBoardRegister(hBoard: KSHandle; rangeIndex: int; offset: int; pValue: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_writeBoardRegister(hBoard: KSHandle; rangeIndex: int; offset: int; value: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Digital input functions ------
function KS_getDigitalBit(hBoard: KSHandle; channel: int; pValue: PUInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getDigitalPort(hBoard: KSHandle; port: int; pValue: PUInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Digital output functions ------
function KS_setDigitalBit(hBoard: KSHandle; channel: int; value: uint; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_setDigitalPort(hBoard: KSHandle; port: int; value: uint; mask: uint; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Analog input functions ------
function KS_configAnalogChannel(hBoard: KSHandle; channel: int; gain: int; mode: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getAnalogValue(hBoard: KSHandle; channel: int; pValue: PUInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_initAnalogStream(hBoard: KSHandle; pChannelSequence: PInt; sequenceLength: int; sequenceCount: int; period: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_startAnalogStream(hBoard: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getAnalogStream(hBoard: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_valueToVolt(hBoard: KSHandle; gain: int; mode: int; value: uint; pVolt: PSingle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Analog output functions ------
function KS_setAnalogValue(hBoard: KSHandle; channel: int; mode: int; value: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_voltToValue(hBoard: KSHandle; mode: int; volt: Single; pValue: PUInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

// -------------------------------------------------------------------------------------------------------------
// MultiFunction Module Driver Interface
// -------------------------------------------------------------------------------------------------------------

// You only need KSDRV functions, if you develop your own driver DLL!

{$DEFINE KS_INCL_MULTIFUNCTION_MODULE}

//--------------------------------------------------------------------------------------------------------------
// CAN Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_BAD_IDENTIFIER                        = (KSERROR_CATEGORY_CAN+$00000000);
  KSERROR_RECV_BUFFER_FULL                      = (KSERROR_CATEGORY_CAN+$00010000);
  KSERROR_XMIT_BUFFER_FULL                      = (KSERROR_CATEGORY_CAN+$00020000);
  KSERROR_BUS_OFF                               = (KSERROR_CATEGORY_CAN+$00030000);
  KSERROR_BUS_PASSIVE                           = (KSERROR_CATEGORY_CAN+$00040000);
  KSERROR_ERROR_COUNTER_WARNING                 = (KSERROR_CATEGORY_CAN+$00050000);
  KSERROR_PORT_DATA_BUFFER_OVERRUN              = (KSERROR_CATEGORY_CAN+$00060000);
  KSERROR_BUS_ERROR                             = (KSERROR_CATEGORY_CAN+$00070000);

  //------ CAN mode ------
  KS_CAN_NORMAL_MODE                            = $00000001;
  KS_CAN_RESET_MODE                             = $00000002;
  KS_CAN_LISTEN_ONLY_MODE                       = $00000004;

  //------ CAN state ------
  KS_CAN_BUS_ON                                 = $00000001;
  KS_CAN_BUS_HEAVY                              = $00000002;
  KS_CAN_BUS_OFF                                = $00000004;
  KS_CAN_RECV_AVAIL                             = $00000008;
  KS_CAN_XMIT_PENDING                           = $00000010;
  KS_CAN_WARN_LIMIT_REACHED                     = $00000020;
  KS_CAN_DATA_BUFFER_OVERRUN                    = $00000040;

  //------ CAN commands ------
  KS_SIGNAL_XMIT_EMPTY                          = $00000004;
  KS_SET_LISTEN_ONLY                            = $00000005;
  KS_RESET_PORT                                 = $00000006;
  KS_GET_ERROR_CODE_CAPTURE                     = $00000007;
  KS_CAN_SET_XMIT_TIMEOUT                       = $00000008;
  KS_CAN_CLEAR_BUS_OFF                          = $0000000a;

  //------ CAN baud rates ------
  KS_CAN_BAUD_1M                                = $000f4240;
  KS_CAN_BAUD_800K                              = $000c3500;
  KS_CAN_BAUD_666K                              = $000a2c2a;
  KS_CAN_BAUD_500K                              = $0007a120;
  KS_CAN_BAUD_250K                              = $0003d090;
  KS_CAN_BAUD_125K                              = $0001e848;
  KS_CAN_BAUD_100K                              = $000186a0;
  KS_CAN_BAUD_50K                               = $0000c350;
  KS_CAN_BAUD_20K                               = $00004e20;
  KS_CAN_BAUD_10K                               = $00002710;

  //------ CAN-FD baud rates ------
  KS_CAN_FD_BAUD_12M                            = $00b71b00;
  KS_CAN_FD_BAUD_10M                            = $00989680;
  KS_CAN_FD_BAUD_8M                             = $007a1200;
  KS_CAN_FD_BAUD_6M                             = $005b8d80;
  KS_CAN_FD_BAUD_4M                             = $003d0900;
  KS_CAN_FD_BAUD_2M                             = $001e8480;

  //------ Common CAN and CAN-FD baud rates ------
  KS_CAN_BAUD_CUSTOM                            = $00000000;

  //------ CAN message types ------
  KS_CAN_MSGTYPE_STANDARD                       = $00000001;
  KS_CAN_MSGTYPE_EXTENDED                       = $00000002;
  KS_CAN_MSGTYPE_RTR                            = $00000004;
  KS_CAN_MSGTYPE_FD                             = $00000008;
  KS_CAN_MSGTYPE_BRS                            = $00000010;

  //------ Legacy CAN events ------
  KS_CAN_RECV                                   = (CAN_BASE+$00000000);
  KS_CAN_XMIT_EMPTY                             = (CAN_BASE+$00000001);
  KS_CAN_XMIT_RTR                               = (CAN_BASE+$00000002);
  KS_CAN_ERROR                                  = (CAN_BASE+$00000003);
  KS_CAN_FILTER                                 = (CAN_BASE+$00000004);
  KS_CAN_TIMEOUT                                = (CAN_BASE+$00000005);

  //------ CAN-FD events ------
  KS_CAN_FD_RECV                                = (CAN_BASE+$00000006);
  KS_CAN_FD_XMIT_EMPTY                          = (CAN_BASE+$00000007);
  KS_CAN_FD_ERROR                               = (CAN_BASE+$00000008);

type
  //------ CAN-FD data structures ------
  KSCanFdMsg = packed record
    structSize: int;
    identifier: int;
    msgType: uint;
    dataLength: int;
    msgData: array [0..63] of byte;
    timestamp: Int64;
  end;
  PKSCanFdMsg = ^KSCanFdMsg;

  KSCanFdState = packed record
    structSize: int;
    state: uint;
    waitingForXmit: int;
    waitingForRecv: int;
    xmitErrCounter: int;
    recvErrCounter: int;
  end;
  PKSCanFdState = ^KSCanFdState;

  KSCanFdConfig = packed record
    structSize: int;
    clockFrequency: int;
    canBaudRate: int;
    canBrp: int;
    canTseg1: int;
    canTseg2: int;
    canSjw: int;
    fdBaudRate: int;
    fdBrp: int;
    fdTseg1: int;
    fdTseg2: int;
    fdSjw: int;
  end;
  PKSCanFdConfig = ^KSCanFdConfig;

  //------ CAN-FD Context structures ------
  KSCanFdContext = packed record
    ctxType: int;
    hCanFd: KSHandle;
  end;
  PKSCanFdContext = ^KSCanFdContext;

  KSCanFdErrorContext = packed record
    ctxType: int;
    hCanFd: KSHandle;
    errorValue: Error;
    timestamp: Int64;
  end;
  PKSCanFdErrorContext = ^KSCanFdErrorContext;

  KSCanFdMsgContext = packed record
    ctxType: int;
    hCanFd: KSHandle;
    msg: KSCanFdMsg;
  end;
  PKSCanFdMsgContext = ^KSCanFdMsgContext;

  //------ CAN data structures ------
  KSCanMsg = packed record
    identifier: int;
    msgType: Word;
    dataLength: Word;
    msgData: array [0..7] of byte;
    timestamp: Int64;
  end;
  PKSCanMsg = ^KSCanMsg;

  KSCanState = packed record
    structSize: int;
    mode: int;
    state: int;
    baudRate: int;
    waitingForXmit: int;
    waitingForRecv: int;
    errorWarnLimit: int;
    recvErrCounter: int;
    xmitErrCounter: int;
    lastError: int;
  end;
  PKSCanState = ^KSCanState;

  //------ CAN Context structures ------
  CanUserContext = packed record
    ctxType: int;
    hCan: KSHandle;
  end;
  PCanUserContext = ^CanUserContext;

  CanErrorUserContext = packed record
    ctxType: int;
    hCan: KSHandle;
    errorValue: Error;
    timestamp: Int64;
  end;
  PCanErrorUserContext = ^CanErrorUserContext;

  CanMsgUserContext = packed record
    ctxType: int;
    hCan: KSHandle;
    msg: KSCanMsg;
    discarded: int;
  end;
  PCanMsgUserContext = ^CanMsgUserContext;

  CanTimeoutUserContext = packed record
    ctxType: int;
    hCan: KSHandle;
    msg: KSCanMsg;
  end;
  PCanTimeoutUserContext = ^CanTimeoutUserContext;

//------ Common CAN-FD functions ------
function KS_openCanFd(phCanFd: PKSHandle; name: PAnsiChar; port: int; pCanFdConfig: PKSCanFdConfig; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeCanFd(hCanFd: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ CAN-FD messaging functions ------
function KS_xmitCanFdMsg(hCanFd: KSHandle; pCanFdMsg: PKSCanFdMsg; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvCanFdMsg(hCanFd: KSHandle; pCanFdMsg: PKSCanFdMsg; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installCanFdHandler(hCanFd: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getCanFdState(hCanFd: KSHandle; pCanFdState: PKSCanFdState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execCanFdCommand(hCanFd: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Common CAN functions ------
function KS_openCan(phCan: PKSHandle; name: PAnsiChar; port: int; baudRate: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_openCanEx(phCan: PKSHandle; hConnection: KSHandle; port: int; baudRate: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeCan(hCan: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';

//------ CAN messaging functions ------
function KS_xmitCanMsg(hCan: KSHandle; pCanMsg: PKSCanMsg; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvCanMsg(hCan: KSHandle; pCanMsg: PKSCanMsg; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installCanHandler(hCan: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getCanState(hCan: KSHandle; pCanState: PKSCanState):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execCanCommand(hCan: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_CAN_MODULE}

//--------------------------------------------------------------------------------------------------------------
// LIN Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ LIN commands ------
  KS_LIN_RESET                                  = $00000000;
  KS_LIN_SLEEP                                  = $00000001;
  KS_LIN_WAKEUP                                 = $00000002;
  KS_LIN_SET_BAUD_RATE                          = $00000004;
  KS_LIN_SET_VERSION                            = $00000005;
  KS_LIN_DISABLE_QUEUE_ERROR                    = $00000006;
  KS_LIN_ENABLE_QUEUE_ERROR                     = $00000007;

  //------ LIN bit rates ------
  KS_LIN_BAUD_19200                             = $00004b00;
  KS_LIN_BAUD_10417                             = $000028b1;
  KS_LIN_BAUD_9600                              = $00002580;
  KS_LIN_BAUD_4800                              = $000012c0;
  KS_LIN_BAUD_2400                              = $00000960;
  KS_LIN_BAUD_1200                              = $000004b0;

  //------ LIN Errors ------
  KS_LIN_ERROR_PHYSICAL                         = $00000001;
  KS_LIN_ERROR_TRANSPORT                        = $00000002;
  KS_LIN_ERROR_BUS_COLLISION                    = $00000003;
  KS_LIN_ERROR_PARITY                           = $00000004;
  KS_LIN_ERROR_CHECKSUM                         = $00000005;
  KS_LIN_ERROR_BREAK_EXPECTED                   = $00000006;
  KS_LIN_ERROR_RESPONSE_TIMEOUT                 = $00000007;
  KS_LIN_ERROR_PID_TIMEOUT                      = $00000009;
  KS_LIN_ERROR_RESPONSE_TOO_SHORT               = $00000010;
  KS_LIN_ERROR_RECV_QUEUE_FULL                  = $00000011;
  KS_LIN_ERROR_RESPONSE_WITHOUT_HEADER          = $00000012;

  //------ LIN events ------
  KS_LIN_ERROR                                  = $00000000;
  KS_LIN_RECV_HEADER                            = $00000001;
  KS_LIN_RECV_RESPONSE                          = $00000002;

type
  //------ LIN data structures ------
  KSLinProperties = packed record
    structSize: int;
    linVersion: int;
    baudRate: int;
  end;
  PKSLinProperties = ^KSLinProperties;

  KSLinState = packed record
    structSize: int;
    properties: KSLinProperties;
    xmitHdrCount: int;
    xmitRspCount: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    recvNoRspCount: int;
    collisionCount: int;
  end;
  PKSLinState = ^KSLinState;

  KSLinHeader = packed record
    identifier: int;
    parity: byte;
    parityOk: byte;
  end;
  PKSLinHeader = ^KSLinHeader;

  KSLinResponse = packed record
    data: array [0..7] of byte;
    dataLen: int;
    checksum: byte;
    checksumOk: byte;
  end;
  PKSLinResponse = ^KSLinResponse;

  KSLinMsg = packed record
    header: KSLinHeader;
    response: KSLinResponse;
    timestamp: Int64;
  end;
  PKSLinMsg = ^KSLinMsg;

  //------ LIN Context structures ------
  LinUserContext = packed record
    ctxType: int;
    hLin: KSHandle;
  end;
  PLinUserContext = ^LinUserContext;

  LinErrorUserContext = packed record
    ctxType: int;
    hLin: KSHandle;
    timestamp: Int64;
    errorCode: int;
  end;
  PLinErrorUserContext = ^LinErrorUserContext;

  LinHeaderUserContext = packed record
    ctxType: int;
    hLin: KSHandle;
    timestamp: Int64;
    header: KSLinHeader;
  end;
  PLinHeaderUserContext = ^LinHeaderUserContext;

  LinResponseUserContext = packed record
    ctxType: int;
    hLin: KSHandle;
    timestamp: Int64;
    msg: KSLinMsg;
  end;
  PLinResponseUserContext = ^LinResponseUserContext;

//------ Common functions ------
function KS_openLin(phLin: PKSHandle; pDeviceName: PAnsiChar; port: int; pProperties: PKSLinProperties; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_openLinEx(phLin: PKSHandle; hLinDevice: KSHandle; pProperties: PKSLinProperties; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeLin(hLin: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installLinHandler(hLin: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execLinCommand(hLin: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_xmitLinHeader(hLin: KSHandle; identifier: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_xmitLinResponse(hLin: KSHandle; pData: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvLinMsg(hLin: KSHandle; pLinMsg: PKSLinMsg; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getLinState(hLin: KSHandle; pLinState: PKSLinState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_LIN_MODULE}

//--------------------------------------------------------------------------------------------------------------
// CANopen Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_CANO_EMERGENCY_REQUEST                = (KSERROR_CATEGORY_CANOPEN+$00000000);
  KSERROR_CANO_TOPOLOGY_CHANGE                  = (KSERROR_CATEGORY_CANOPEN+$00020000);
  KSERROR_CANO_SLAVE_ERROR                      = (KSERROR_CATEGORY_CANOPEN+$00030000);
  KSERROR_CANO_DATA_INCOMPLETE                  = (KSERROR_CATEGORY_CANOPEN+$00040000);
  KSERROR_CANO_RECEIVED_SYNC                    = (KSERROR_CATEGORY_CANOPEN+$00050000);
  KSERROR_CANO_PDO_NOT_VALID                    = (KSERROR_CATEGORY_CANOPEN+$00060000);

  //------ DataObj type flags ------
  KS_CANO_DATAOBJ_PDO_TYPE                      = $00000001;
  KS_CANO_DATAOBJ_SDO_TYPE                      = $00000002;
  KS_CANO_DATAOBJ_READABLE                      = $00010000;
  KS_CANO_DATAOBJ_WRITEABLE                     = $00020000;
  KS_CANO_DATAOBJ_ACTIVE                        = $00040000;
  KS_CANO_DATAOBJ_MAPPABLE                      = $00080000;

  //------ Functionality flags ------
  KS_CANO_SIMPLE_BOOTUP_MASTER                  = $00000001;
  KS_CANO_SIMPLE_BOOTUP_SLAVE                   = $00000002;
  KS_CANO_DYNAMIC_CHANNELS                      = $00000004;
  KS_CANO_GROUP_MESSAGING                       = $00000008;
  KS_CANO_LAYER_SETTINGS                        = $00000010;

  //------ State type ------
  KS_CANO_STATE_INIT                            = $00000001;
  KS_CANO_STATE_PREOP                           = $00000002;
  KS_CANO_STATE_STOP                            = $00000003;
  KS_CANO_STATE_OP                              = $00000004;

  //------ CANopen commands ------
  KS_CANO_XMIT_TIME_STAMP                       = $00000001;

  //------ common index ------
  KS_CANO_INDEX_ALL                             = Integer($ffffffff);
  KS_CANO_INDEX_INPUT                           = Integer($fffffffe);
  KS_CANO_INDEX_OUTPUT                          = Integer($fffffffd);

  //------ CANopen events ------
  KS_CANO_ERROR                                 = (CANOPEN_BASE+$00000000);
  KS_CANO_DATASET_SIGNAL                        = (CANOPEN_BASE+$00000001);
  KS_CANO_TOPOLOGY_CHANGE                       = (CANOPEN_BASE+$00000002);

  //------ Topology change reasons ------
  KS_CANO_TOPOLOGY_MASTER_CONNECTED             = $00000000;
  KS_CANO_TOPOLOGY_MASTER_DISCONNECTED          = $00000001;
  KS_CANO_TOPOLOGY_SLAVE_COUNT_CHANGED          = $00000002;
  KS_CANO_TOPOLOGY_SLAVE_ONLINE                 = $00000003;
  KS_CANO_TOPOLOGY_SLAVE_OFFLINE                = $00000004;

type
  //------ Types & Structures ------
  CanoErrorUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    error: int;
  end;
  PCanoErrorUserContext = ^CanoErrorUserContext;

  CanoDataSetUserContext = packed record
    ctxType: int;
    hDataSet: KSHandle;
  end;
  PCanoDataSetUserContext = ^CanoDataSetUserContext;

  CanoTopologyUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    slavesOnline: int;
    slavesCreated: int;
    slavesCreatedAndOnline: int;
    reason: int;
    hSlave: KSHandle;
  end;
  PCanoTopologyUserContext = ^CanoTopologyUserContext;

  KSCanoEmergencyObj = packed record
    errorCode: Word;
    errorRegister: byte;
    additionalCode: array [0..4] of byte;
  end;
  PKSCanoEmergencyObj = ^KSCanoEmergencyObj;

  KSCanoMasterState = packed record
    structSize: int;
    connected: int;
    slavesOnline: int;
    slaveCreated: int;
    slavesCreatedAndOnline: int;
    masterState: int;
    lastError: int;
  end;
  PKSCanoMasterState = ^KSCanoMasterState;

  KSCanoSlaveState = packed record
    structSize: int;
    online: int;
    created: int;
    vendor: int;
    product: int;
    revision: int;
    id: int;
    slaveState: int;
    emergencyObj: KSCanoEmergencyObj;
  end;
  PKSCanoSlaveState = ^KSCanoSlaveState;

  KSCanoDataVarInfo = packed record
    objType: int;
    name: PAnsiChar;
    dataType: int;
    bitLength: int;
    subIndex: int;
    defaultValue: int;
    minValue: int;
    maxValue: int;
  end;
  PKSCanoDataVarInfo = ^KSCanoDataVarInfo;

  PPKSCanoDataVarInfo = ^PKSCanoDataVarInfo;

  KSCanoDataObjInfo = packed record
    objType: int;
    name: PAnsiChar;
    bitLength: int;
    index: int;
    cobId: int;
    mappingIndex: int;
    transmissionType: int;
    varCount: int;
    vars: array [0..0] of PKSCanoDataVarInfo;
  end;
  PKSCanoDataObjInfo = ^KSCanoDataObjInfo;

  PPKSCanoDataObjInfo = ^PKSCanoDataObjInfo;

  KSCanoSlaveInfo = packed record
    vendorId: int;
    productId: int;
    revision: int;
    name: PAnsiChar;
    orderCode: PAnsiChar;
    rxPdoCount: int;
    txPdoCount: int;
    granularity: int;
    functionality: int;
    objCount: int;
    objs: array [0..0] of PKSCanoDataObjInfo;
  end;
  PKSCanoSlaveInfo = ^KSCanoSlaveInfo;

  PPKSCanoSlaveInfo = ^PKSCanoSlaveInfo;

//------ Master functions ------
function KS_createCanoMaster(phMaster: PKSHandle; hConnection: KSHandle; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeCanoMaster(hMaster: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installCanoMasterHandler(hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryCanoMasterState(hMaster: KSHandle; pMasterState: PKSCanoMasterState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_changeCanoMasterState(hMaster: KSHandle; state: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Slave functions ------
function KS_createCanoSlave(hMaster: KSHandle; phSlave: PKSHandle; id: int; position: int; vendor: uint; product: uint; revision: uint; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_createCanoSlaveIndirect(hMaster: KSHandle; phSlave: PKSHandle; pSlaveState: PKSCanoSlaveState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumCanoSlaves(hMaster: KSHandle; index: int; pSlaveState: PKSCanoSlaveState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_deleteCanoSlave(hSlave: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryCanoSlaveState(hSlave: KSHandle; pSlaveState: PKSCanoSlaveState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_changeCanoSlaveState(hSlave: KSHandle; state: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryCanoSlaveInfo(hSlave: KSHandle; ppSlaveInfo: PPKSCanoSlaveInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryCanoDataObjInfo(hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSCanoDataObjInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryCanoDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSCanoDataVarInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumCanoDataObjInfo(hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSCanoDataObjInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumCanoDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSCanoDataVarInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ DataSet related functions ------
function KS_createCanoDataSet(hMaster: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_deleteCanoDataSet(hSet: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_assignCanoDataSet(hSet: KSHandle; hSlave: KSHandle; pdoIndex: int; placement: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readCanoDataSet(hSet: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_postCanoDataSet(hSet: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installCanoDataSetHandler(hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Data exchange functions ------
function KS_getCanoDataObjAddress(hSet: KSHandle; hSlave: KSHandle; pdoIndex: int; subIndex: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readCanoDataObj(hSlave: KSHandle; index: int; subIndex: int; pData: Pointer; pSize: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_postCanoDataObj(hSlave: KSHandle; index: int; subIndex: int; pData: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Auxiliary functions ------
function KS_changeCanoState(hObject: KSHandle; state: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execCanoCommand(hObject: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_CANOPEN_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Profibus Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_PBUS_CONNECTION_CHANGE                = (KSERROR_CATEGORY_PROFIBUS+$00010000);
  KSERROR_PBUS_TOPOLOGY_CHANGE                  = (KSERROR_CATEGORY_PROFIBUS+$00020000);
  KSERROR_PBUS_SLAVE_ERROR                      = (KSERROR_CATEGORY_PROFIBUS+$00030000);
  KSERROR_PBUS_ALARM_BUFFER_FULL                = (KSERROR_CATEGORY_PROFIBUS+$00040000);
  KSERROR_BAD_PARAMETERIZATION_SET              = (KSERROR_CATEGORY_PROFIBUS+$00050000);
  KSERROR_PBUS_MASTER_ERROR                     = (KSERROR_CATEGORY_PROFIBUS+$00060000);
  KSERROR_HARDWARE_INTERFACE                    = (KSERROR_CATEGORY_PROFIBUS+$00070000);
  KSERROR_AUTO_CLEAR                            = (KSERROR_CATEGORY_PROFIBUS+$00080000);

  //------ Flags ------
  KSF_ACCEPT_OFFLINE                            = $00000001;
  KSF_SLOT                                      = $00000002;

  //------ Master State ------
  KS_PBUS_OFFLINE                               = $00000001;
  KS_PBUS_STOP                                  = $00000002;
  KS_PBUS_CLEAR                                 = $00000003;
  KS_PBUS_OPERATE                               = $00000004;

  //------ Profibus special indices ------
  KS_PBUS_INDEX_ALL                             = Integer($ffffffff);
  KS_PBUS_INDEX_INPUT                           = Integer($fffffffe);
  KS_PBUS_INDEX_OUTPUT                          = Integer($fffffffd);

  //------ Profibus commands ------
  KS_UNFREEZE                                   = $00000001;
  KS_FREEZE                                     = $00000002;
  KS_UNSYNC                                     = $00000004;
  KS_SYNC                                       = $00000008;

  //------ Profibus command params ------
  KS_SELECT_GROUP_1                             = $00000100;
  KS_SELECT_GROUP_2                             = $00000200;
  KS_SELECT_GROUP_3                             = $00000400;
  KS_SELECT_GROUP_4                             = $00000800;
  KS_SELECT_GROUP_5                             = $00001000;
  KS_SELECT_GROUP_6                             = $00002000;
  KS_SELECT_GROUP_7                             = $00004000;
  KS_SELECT_GROUP_8                             = $00008000;

  //------ Profibus bit rates ------
  KS_PBUS_BAUD_12M                              = $00b71b00;
  KS_PBUS_BAUD_6M                               = $005b8d80;
  KS_PBUS_BAUD_3M                               = $002dc6c0;
  KS_PBUS_BAUD_1500K                            = $0016e360;
  KS_PBUS_BAUD_500K                             = $0007a120;
  KS_PBUS_BAUD_187500                           = $0002dc6c;
  KS_PBUS_BAUD_93750                            = $00016e36;
  KS_PBUS_BAUD_45450                            = $0000b18a;
  KS_PBUS_BAUD_31250                            = $00007a12;
  KS_PBUS_BAUD_19200                            = $00004b00;
  KS_PBUS_BAUD_9600                             = $00002580;

  //------ Profibus events ------
  KS_PBUS_ERROR                                 = (PROFIBUS_BASE+$00000000);
  KS_PBUS_DATASET_SIGNAL                        = (PROFIBUS_BASE+$00000001);
  KS_PBUS_TOPOLOGY_CHANGE                       = (PROFIBUS_BASE+$00000002);
  KS_PBUS_ALARM                                 = (PROFIBUS_BASE+$00000003);
  KS_PBUS_DIAGNOSTIC                            = (PROFIBUS_BASE+$00000004);

  //------ Profibus alarm types ------
  KS_PBUS_DIAGNOSTIC_ALARM                      = $00000001;
  KS_PBUS_PROCESS_ALARM                         = $00000002;
  KS_PBUS_PULL_ALARM                            = $00000003;
  KS_PBUS_PLUG_ALARM                            = $00000004;
  KS_PBUS_STATUS_ALARM                          = $00000005;
  KS_PBUS_UPDATE_ALARM                          = $00000006;

  //------ Profibus topology change reasons ------
  KS_PBUS_TOPOLOGY_MASTER_CONNECTED             = $00000000;
  KS_PBUS_TOPOLOGY_MASTER_DISCONNECTED          = $00000001;
  KS_PBUS_TOPOLOGY_SLAVE_COUNT_CHANGED          = $00000002;
  KS_PBUS_TOPOLOGY_SLAVE_ONLINE                 = $00000003;
  KS_PBUS_TOPOLOGY_SLAVE_OFFLINE                = $00000004;

type
  //------ Types & Structures ------
  PbusErrorUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    error: int;
  end;
  PPbusErrorUserContext = ^PbusErrorUserContext;

  PbusDataSetUserContext = packed record
    ctxType: int;
    hDataSet: KSHandle;
  end;
  PPbusDataSetUserContext = ^PbusDataSetUserContext;

  PbusTopologyUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    slavesOnline: int;
    slavesConfigured: int;
    reason: int;
  end;
  PPbusTopologyUserContext = ^PbusTopologyUserContext;

  PbusAlarmUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    slaveAddress: int;
    alarmType: int;
    slotNumber: int;
    specifier: int;
    diagnosticLength: int;
    diagnosticData: array [0..59] of int;
  end;
  PPbusAlarmUserContext = ^PbusAlarmUserContext;

  PbusDiagnosticUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    slaveAddress: int;
  end;
  PPbusDiagnosticUserContext = ^PbusDiagnosticUserContext;

  KSPbusMasterState = packed record
    structSize: int;
    connected: int;
    slavesOnline: int;
    highestStationAddr: int;
    slavesConfigured: int;
    masterState: int;
    busErrorCounter: int;
    timeoutCounter: int;
    lastError: int;
  end;
  PKSPbusMasterState = ^KSPbusMasterState;

  KSPbusSlaveState = packed record
    structSize: int;
    address: int;
    identNumber: int;
    configured: int;
    assigned: int;
    state: int;
    stdStationStatus: array [0..5] of byte;
    extendedDiag: array [0..237] of byte;
  end;
  PKSPbusSlaveState = ^KSPbusSlaveState;

  KSPbusDataVarInfo = packed record
    dataType: int;
    subIndex: int;
    byteLength: int;
    name: PAnsiChar;
  end;
  PKSPbusDataVarInfo = ^KSPbusDataVarInfo;

  PPKSPbusDataVarInfo = ^PKSPbusDataVarInfo;

  KSPbusSlotInfo = packed record
    slotNumber: int;
    varCount: int;
    byteLength: int;
    name: PAnsiChar;
    vars: array [0..0] of PKSPbusDataVarInfo;
  end;
  PKSPbusSlotInfo = ^KSPbusSlotInfo;

  PPKSPbusSlotInfo = ^PKSPbusSlotInfo;

  KSPbusSlaveInfo = packed record
    address: int;
    identNumber: int;
    slotCount: int;
    byteLength: int;
    name: PAnsiChar;
    slots: array [0..0] of PKSPbusSlotInfo;
  end;
  PKSPbusSlaveInfo = ^KSPbusSlaveInfo;

  PPKSPbusSlaveInfo = ^PKSPbusSlaveInfo;

//------ Master functions ------
function KS_createPbusMaster(phMaster: PKSHandle; name: PAnsiChar; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closePbusMaster(hMaster: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryPbusMasterState(hMaster: KSHandle; pMasterState: PKSPbusMasterState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_changePbusMasterState(hMaster: KSHandle; state: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumPbusSlaves(hMaster: KSHandle; index: int; pSlaveState: PKSPbusSlaveState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installPbusMasterHandler(hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execPbusMasterCommand(hMaster: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Slave functions ------
function KS_createPbusSlave(hMaster: KSHandle; phSlave: PKSHandle; address: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_deletePbusSlave(hSlave: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryPbusSlaveState(hSlave: KSHandle; pSlaveState: PKSPbusSlaveState; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_changePbusSlaveState(hSlave: KSHandle; state: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryPbusSlaveInfo(hSlave: KSHandle; ppSlaveInfo: PPKSPbusSlaveInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryPbusSlotInfo(hSlave: KSHandle; objIndex: int; ppSlotInfo: PPKSPbusSlotInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_queryPbusDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSPbusDataVarInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Data exchange functions ------
function KS_createPbusDataSet(hMaster: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_deletePbusDataSet(hSet: KSHandle):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_assignPbusDataSet(hSet: KSHandle; hSlave: KSHandle; slot: int; index: int; placement: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readPbusDataSet(hSet: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_postPbusDataSet(hSet: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installPbusDataSetHandler(hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getPbusDataObjAddress(hSet: KSHandle; hSlave: KSHandle; slot: int; index: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readPbusDataObj(hSlave: KSHandle; slot: int; index: int; pData: Pointer; pSize: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_postPbusDataObj(hSlave: KSHandle; slot: int; index: int; pData: Pointer; size: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_PROFIBUS_MODULE}

//--------------------------------------------------------------------------------------------------------------
// RTL Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Mathematical constants ------
  KS_PI                                         = 3.14159265358979323846;
  KS_E                                          = 2.71828182845904523536;

//------ stdlib.h replacements ------
function KSRTL_calloc(num: uint; size: uint):
  Pointer; stdcall; external 'KrtsDemo.dll';
procedure KSRTL_free(ptr: Pointer)
  stdcall; external 'KrtsDemo.dll';
function KSRTL_malloc(size: uint):
  Pointer; stdcall; external 'KrtsDemo.dll';

//------ math.h replacements ------
function KSRTL_sin(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_cos(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_tan(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_asin(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_acos(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_atan(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_atan2(y: Double; x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_sinh(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_cosh(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_tanh(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_exp(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_frexp(x: Double; exp: PInt):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_ldexp(x: Double; exp: int):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_log(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_log10(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_modf(x: Double; intpart: PDouble):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_pow(base: Double; exponent: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_sqrt(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_fabs(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_ceil(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_floor(x: Double):
  Double; stdcall; external 'KrtsDemo.dll';
function KSRTL_fmod(numerator: Double; denominator: Double):
  Double; stdcall; external 'KrtsDemo.dll';

//------ string.h replacements ------
function KSRTL_memchr(ptr: Pointer; value: int; num: uint):
  Pointer; stdcall; external 'KrtsDemo.dll';
function KSRTL_memcmp(ptr1: Pointer; ptr2: Pointer; num: uint):
  int; stdcall; external 'KrtsDemo.dll';
function KSRTL_memcpy(destination: Pointer; source: Pointer; num: uint):
  Pointer; stdcall; external 'KrtsDemo.dll';
function KSRTL_memmove(destination: Pointer; source: Pointer; num: uint):
  Pointer; stdcall; external 'KrtsDemo.dll';
function KSRTL_memset(ptr: Pointer; value: int; num: uint):
  Pointer; stdcall; external 'KrtsDemo.dll';
function KSRTL_strlen(str: PAnsiChar):
  uint; stdcall; external 'KrtsDemo.dll';
function KSRTL_strcmp(str1: PAnsiChar; str2: PAnsiChar):
  int; stdcall; external 'KrtsDemo.dll';
function KSRTL_strncmp(str1: PAnsiChar; str2: PAnsiChar; num: uint):
  int; stdcall; external 'KrtsDemo.dll';
function KSRTL_strcpy(destination: PAnsiChar; source: PAnsiChar):
  PAnsiChar; stdcall; external 'KrtsDemo.dll';
function KSRTL_strncpy(destination: PAnsiChar; source: PAnsiChar; num: uint):
  PAnsiChar; stdcall; external 'KrtsDemo.dll';
function KSRTL_strcat(destination: PAnsiChar; source: PAnsiChar):
  PAnsiChar; stdcall; external 'KrtsDemo.dll';
function KSRTL_strncat(destination: PAnsiChar; source: PAnsiChar; num: uint):
  PAnsiChar; stdcall; external 'KrtsDemo.dll';
function KSRTL_strchr(str: PAnsiChar; character: int):
  PAnsiChar; stdcall; external 'KrtsDemo.dll';
function KSRTL_strcoll(str1: PAnsiChar; str2: PAnsiChar):
  int; stdcall; external 'KrtsDemo.dll';
function KSRTL_strrchr(str1: PAnsiChar; character: int):
  PAnsiChar; stdcall; external 'KrtsDemo.dll';
function KSRTL_strstr(str1: PAnsiChar; str2: PAnsiChar):
  PAnsiChar; stdcall; external 'KrtsDemo.dll';
function KSRTL_strspn(str1: PAnsiChar; str2: PAnsiChar):
  uint; stdcall; external 'KrtsDemo.dll';
function KSRTL_strcspn(str1: PAnsiChar; str2: PAnsiChar):
  uint; stdcall; external 'KrtsDemo.dll';
function KSRTL_strpbrk(str1: PAnsiChar; str2: PAnsiChar):
  PAnsiChar; stdcall; external 'KrtsDemo.dll';
function KSRTL_strtok(str: PAnsiChar; delimiters: PAnsiChar):
  PAnsiChar; stdcall; external 'KrtsDemo.dll';
function KSRTL_strxfrm(destination: PAnsiChar; source: PAnsiChar; num: uint):
  uint; stdcall; external 'KrtsDemo.dll';
function KSRTL_vsprintf(buffer: PAnsiChar; format: PAnsiChar; pArgs: Pointer):
  int; stdcall; external 'KrtsDemo.dll';
function KSRTL_vsnprintf(buffer: PAnsiChar; n: uint; format: PAnsiChar; pArgs: Pointer):
  int; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_RTL_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Camera Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_NO_DISCOVERY                              = $00000800;
  KSF_CAMERA_NO_GENICAM                         = $00001000;
  KSF_CAMERA_BROADCAST_DISCOVERY                = $00800000;

type
  //------ Types & Structures ------
  KSCameraInfo = packed record
    structSize: int;
    hardwareId: array [0..79] of AnsiChar;
    vendor: array [0..79] of AnsiChar;
    name: array [0..79] of AnsiChar;
    serialNumber: array [0..79] of AnsiChar;
    userName: array [0..79] of AnsiChar;
  end;
  PKSCameraInfo = ^KSCameraInfo;

  KSCameraTimeouts = packed record
    structSize: int;
    commandTimeout: int;
    commandRetries: int;
    heartbeatTimeout: int;
  end;
  PKSCameraTimeouts = ^KSCameraTimeouts;

  KSCameraForceIp = packed record
    structSize: int;
    macAddress: array [0..17] of AnsiChar;
    address: uint;
    subnetMask: uint;
    gatewayAddress: uint;
  end;
  PKSCameraForceIp = ^KSCameraForceIp;

  KSCameraInfoGev = packed record
    structSize: int;
    vendor: array [0..31] of AnsiChar;
    name: array [0..31] of AnsiChar;
    serialNumber: array [0..15] of AnsiChar;
    userName: array [0..15] of AnsiChar;
    macAddress: array [0..17] of AnsiChar;
    instance: int;
    instanceName: array [0..255] of AnsiChar;
  end;
  PKSCameraInfoGev = ^KSCameraInfoGev;

  KSCameraEvent = packed record
    structSize: int;
    eventId: int;
    timestamp: UInt64;
    blockId: UInt64;
    channel: int;
    size: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
  end;
  PKSCameraEvent = ^KSCameraEvent;

  //------ Block type structures ------
  KSCameraBlock = packed record
    blockType: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
    id: UInt64;
    timestamp: UInt64;
  end;
  PKSCameraBlock = ^KSCameraBlock;

  PPKSCameraBlock = ^PKSCameraBlock;

  KSCameraImage = packed record
    blockType: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
    id: UInt64;
    timestamp: UInt64;
    fieldCount: int;
    fieldId: int;
    pixelFormat: uint;
    width: uint;
    height: uint;
    offsetX: uint;
    offsetY: uint;
    linePadding: uint;
    imagePadding: uint;
    chunkDataPayloadLength: uint;
    chunkLayoutId: uint;
  end;
  PKSCameraImage = ^KSCameraImage;

  KSCameraJpeg = packed record
    blockType: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
    id: UInt64;
    timestamp: UInt64;
    fieldCount: int;
    fieldId: int;
    jpegPayloadSize: UInt64;
    tickFrequency: UInt64;
    dataFormat: uint;
    isColorSpace: int;
    chunkDataPayloadLength: uint;
    chunkLayoutId: uint;
  end;
  PKSCameraJpeg = ^KSCameraJpeg;

  KSCameraChunk = packed record
    blockType: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
    id: UInt64;
    timestamp: UInt64;
    chunkDataPayloadLength: uint;
    chunkLayoutId: uint;
  end;
  PKSCameraChunk = ^KSCameraChunk;

const
  //------ Block types ------
  KS_CAMERA_BLOCKTYPE_NONE                      = $00000000;
  KS_CAMERA_BLOCKTYPE_IMAGE                     = $00000001;
  KS_CAMERA_BLOCKTYPE_CHUNK                     = $00000002;
  KS_CAMERA_BLOCKTYPE_JPEG                      = $00000003;
  KS_CAMERA_BLOCKTYPE_JPEG2000                  = $00000004;

  //------ Camera commands ------
  KS_CAMERA_READREG                             = $00000001;
  KS_CAMERA_READMEM                             = $00000002;
  KS_CAMERA_WRITEREG                            = $00000003;
  KS_CAMERA_SET_TIMEOUTS                        = $00000004;
  KS_CAMERA_GET_TIMEOUTS                        = $00000005;
  KS_CAMERA_GET_XML_FILE                        = $00000006;
  KS_CAMERA_GET_XML_INFO                        = $00000007;
  KS_CAMERA_SET_DISCOVERY_INTERVAL              = $00000008;
  KS_CAMERA_RESIZE_BUFFERS                      = $00000009;
  KS_CAMERA_GET_INFO                            = $0000000a;
  KS_CAMERA_SET_CHUNK_DATA                      = $0000000b;
  KS_CAMERA_SEND_TO_SOURCE_PORT                 = $0000000c;
  KS_CAMERA_FORCEIP                             = $0000000d;
  KS_CAMERA_WRITEMEM                            = $0000000e;
  KS_CAMERA_RECV_EVENT                          = $00000011;

  //------ Acquisition modes ------
  KS_CAMERA_CONTINUOUS                          = Integer($ffffffff);
  KS_CAMERA_PRESERVE                            = $00000000;
  KS_CAMERA_SINGLE_FRAME                        = $00000001;
  KS_CAMERA_MULTI_FRAME                         = $00000000;

  //------ Configuration commands ------
  KS_CAMERA_CONFIG_GET                          = $00000001;
  KS_CAMERA_CONFIG_SET                          = $00000002;
  KS_CAMERA_CONFIG_TYPE                         = $00000003;
  KS_CAMERA_CONFIG_ENUMERATE                    = $00000004;
  KS_CAMERA_CONFIG_MIN                          = $00000005;
  KS_CAMERA_CONFIG_MAX                          = $00000006;
  KS_CAMERA_CONFIG_INC                          = $00000007;
  KS_CAMERA_CONFIG_LENGTH                       = $00010000;

  //------ Configuration types ------
  KS_CAMERA_CONFIG_CATEGORY                     = $00000001;
  KS_CAMERA_CONFIG_BOOLEAN                      = $00000002;
  KS_CAMERA_CONFIG_ENUMERATION                  = $00000003;
  KS_CAMERA_CONFIG_INTEGER                      = $00000004;
  KS_CAMERA_CONFIG_COMMAND                      = $00000005;
  KS_CAMERA_CONFIG_FLOAT                        = $00000006;
  KS_CAMERA_CONFIG_STRING                       = $00000007;
  KS_CAMERA_CONFIG_REGISTER                     = $00000008;

  //------ Receive and error handler types ------
  KS_CAMERA_IMAGE_RECEIVED                      = $00000001;
  KS_CAMERA_IMAGE_DROPPED                       = $00000002;
  KS_CAMERA_ERROR                               = $00000003;
  KS_CAMERA_GEV_ATTACHED                        = $00000004;
  KS_CAMERA_GEV_DETACHED                        = $00000005;
  KS_CAMERA_ATTACHED                            = $00000006;
  KS_CAMERA_DETACHED                            = $00000007;
  KS_CAMERA_EVENT                               = $00000008;

  //------ Error codes ------
  KSERROR_CAMERA_COMMAND_ERROR                  = (KSERROR_CATEGORY_CAMERA+$00010000);
  KSERROR_CAMERA_COMMAND_TIMEOUT                = (KSERROR_CATEGORY_CAMERA+$00020000);
  KSERROR_CAMERA_COMMAND_FAILED                 = (KSERROR_CATEGORY_CAMERA+$00030000);
  KSERROR_CAMERA_COMMAND_BAD_ALIGNMENT          = (KSERROR_CATEGORY_CAMERA+$00040000);
  KSERROR_CAMERA_RECONNECTED                    = (KSERROR_CATEGORY_CAMERA+$000f0000);
  KSERROR_CAMERA_STREAM_INVALID_DATA            = (KSERROR_CATEGORY_CAMERA+$00100000);
  KSERROR_CAMERA_STREAM_NO_BUFFER               = (KSERROR_CATEGORY_CAMERA+$00110000);
  KSERROR_CAMERA_STREAM_NOT_TRANSMITTED         = (KSERROR_CATEGORY_CAMERA+$00120000);
  KSERROR_CAMERA_STREAM_INCOMPLETE_TRANSMISSION = (KSERROR_CATEGORY_CAMERA+$00130000);
  KSERROR_CAMERA_STREAM_BUFFER_OVERRUN          = (KSERROR_CATEGORY_CAMERA+$00140000);
  KSERROR_CAMERA_STREAM_BUFFER_QUEUED           = (KSERROR_CATEGORY_CAMERA+$00150000);
  KSERROR_GENICAM_ERROR                         = (KSERROR_CATEGORY_CAMERA+$00800000);
  KSERROR_GENICAM_FEATURE_NOT_IMPLEMENTED       = (KSERROR_CATEGORY_CAMERA+$00810000);
  KSERROR_GENICAM_FEATURE_NOT_AVAILABLE         = (KSERROR_CATEGORY_CAMERA+$00820000);
  KSERROR_GENICAM_FEATURE_ACCESS_DENIED         = (KSERROR_CATEGORY_CAMERA+$00830000);
  KSERROR_GENICAM_FEATURE_NOT_FOUND             = (KSERROR_CATEGORY_CAMERA+$00840000);
  KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND          = (KSERROR_CATEGORY_CAMERA+$00850000);
  KSERROR_GENICAM_SUBNODE_NOT_REACHABLE         = (KSERROR_CATEGORY_CAMERA+$00860000);
  KSERROR_GENICAM_PORT_NOT_REACHABLE            = (KSERROR_CATEGORY_CAMERA+$00870000);
  KSERROR_GENICAM_FEATURE_NOT_SUPPORTED         = (KSERROR_CATEGORY_CAMERA+$00880000);
  KSERROR_GENICAM_VALUE_TOO_LARGE               = (KSERROR_CATEGORY_CAMERA+$00890000);
  KSERROR_GENICAM_VALUE_TOO_SMALL               = (KSERROR_CATEGORY_CAMERA+$008a0000);
  KSERROR_GENICAM_VALUE_BAD_INCREMENT           = (KSERROR_CATEGORY_CAMERA+$008b0000);
  KSERROR_GENICAM_BAD_REGISTER_DESCRIPTION      = (KSERROR_CATEGORY_CAMERA+$008c0000);

  //------ GigE Vision<sup>®</sup> registers ------
  KS_CAMERA_REGISTER_STREAM_CHANNELS            = $00000904;

  //------ Camera module config ------
  KSCONFIG_OVERRIDE_CAMERA_XML_FILE             = $00000001;

  //------ GigE Vision<sup>®</sup> pixel formats ------
  KS_CAMERA_PIXEL_MONO_8                        = $01080001;
  KS_CAMERA_PIXEL_MONO_8S                       = $01080002;
  KS_CAMERA_PIXEL_MONO_10                       = $01100003;
  KS_CAMERA_PIXEL_MONO_10_PACKED                = $010c0004;
  KS_CAMERA_PIXEL_MONO_12                       = $01100005;
  KS_CAMERA_PIXEL_MONO_12_PACKED                = $010c0006;
  KS_CAMERA_PIXEL_MONO_16                       = $01100007;
  KS_CAMERA_PIXEL_BAYER_GR_8                    = $01080008;
  KS_CAMERA_PIXEL_BAYER_RG_8                    = $01080009;
  KS_CAMERA_PIXEL_BAYER_GB_8                    = $0108000a;
  KS_CAMERA_PIXEL_BAYER_BG_8                    = $0108000b;
  KS_CAMERA_PIXEL_BAYER_GR_10                   = $0110000c;
  KS_CAMERA_PIXEL_BAYER_RG_10                   = $0110000d;
  KS_CAMERA_PIXEL_BAYER_GB_10                   = $0110000e;
  KS_CAMERA_PIXEL_BAYER_BG_10                   = $0110000f;
  KS_CAMERA_PIXEL_BAYER_GR_12                   = $01100010;
  KS_CAMERA_PIXEL_BAYER_RG_12                   = $01100011;
  KS_CAMERA_PIXEL_BAYER_GB_12                   = $01100012;
  KS_CAMERA_PIXEL_BAYER_BG_12                   = $01100013;
  KS_CAMERA_PIXEL_RGB_8                         = $02180014;
  KS_CAMERA_PIXEL_BGR_8                         = $02180015;
  KS_CAMERA_PIXEL_RGBA_8                        = $02200016;
  KS_CAMERA_PIXEL_BGRA_8                        = $02200017;
  KS_CAMERA_PIXEL_RGB_10                        = $02300018;
  KS_CAMERA_PIXEL_BGR_10                        = $02300019;
  KS_CAMERA_PIXEL_RGB_12                        = $0230001a;
  KS_CAMERA_PIXEL_BGR_12                        = $0230001b;
  KS_CAMERA_PIXEL_RGB_10_V1_PACKED              = $0220001c;
  KS_CAMERA_PIXEL_RGB_10_P_32                   = $0220001d;
  KS_CAMERA_PIXEL_YUV_411_8_UYYVYY              = $020c001e;
  KS_CAMERA_PIXEL_YUV_422_8_UYVY                = $0210001f;
  KS_CAMERA_PIXEL_YUV_8_UYV                     = $02180020;
  KS_CAMERA_PIXEL_RGB_8_PLANAR                  = $02180021;
  KS_CAMERA_PIXEL_RGB_10_PLANAR                 = $02300022;
  KS_CAMERA_PIXEL_RGB_12_PLANAR                 = $02300023;
  KS_CAMERA_PIXEL_RGB_16_PLANAR                 = $02300024;
  KS_CAMERA_PIXEL_MONO_14                       = $01100025;
  KS_CAMERA_PIXEL_BAYER_GR_10_PACKED            = $010c0026;
  KS_CAMERA_PIXEL_BAYER_RG_10_PACKED            = $010c0027;
  KS_CAMERA_PIXEL_BAYER_GB_10_PACKED            = $010c0028;
  KS_CAMERA_PIXEL_BAYER_BG_10_PACKED            = $010c0029;
  KS_CAMERA_PIXEL_BAYER_GR_12_PACKED            = $010c002a;
  KS_CAMERA_PIXEL_BAYER_RG_12_PACKED            = $010c002b;
  KS_CAMERA_PIXEL_BAYER_GB_12_PACKED            = $010c002c;
  KS_CAMERA_PIXEL_BAYER_BG_12_PACKED            = $010c002d;
  KS_CAMERA_PIXEL_BAYER_GR_16                   = $0110002e;
  KS_CAMERA_PIXEL_BAYER_RG_16                   = $0110002f;
  KS_CAMERA_PIXEL_BAYER_GB_16                   = $01100030;
  KS_CAMERA_PIXEL_BAYER_BG_16                   = $01100031;
  KS_CAMERA_PIXEL_YUV_422_8                     = $020c0032;
  KS_CAMERA_PIXEL_RGB_16                        = $02300033;
  KS_CAMERA_PIXEL_RGB_12_V1_PACKED              = $02240034;
  KS_CAMERA_PIXEL_RGB_565_P                     = $02100035;
  KS_CAMERA_PIXEL_BGR_565_P                     = $02100036;
  KS_CAMERA_PIXEL_MONO_1P                       = $01010037;
  KS_CAMERA_PIXEL_MONO_2P                       = $01020038;
  KS_CAMERA_PIXEL_MONO_4P                       = $01040039;
  KS_CAMERA_PIXEL_YCBCR_8_CBYCR                 = $0218003a;
  KS_CAMERA_PIXEL_YCBCR_422_8                   = $0210003b;
  KS_CAMERA_PIXEL_YCBCR_411_8_CBYYCRYY          = $020c003c;
  KS_CAMERA_PIXEL_YCBCR_601_8_CBYCR             = $0218003d;
  KS_CAMERA_PIXEL_YCBCR_601_422_8               = $0210003e;
  KS_CAMERA_PIXEL_YCBCR_601_411_8_CBYYCRYY      = $020c003f;
  KS_CAMERA_PIXEL_YCBCR_709_8_CBYCR             = $02180040;
  KS_CAMERA_PIXEL_YCBCR_709_422_8               = $02100041;
  KS_CAMERA_PIXEL_YCBCR_709_411_8_CBYYCRYY      = $020c0042;
  KS_CAMERA_PIXEL_YCBCR_422_8_CBYCRY            = $02100043;
  KS_CAMERA_PIXEL_YCBCR_601_422_8_CBYCRY        = $02100044;
  KS_CAMERA_PIXEL_YCBCR_709_422_8_CBYCRY        = $02100045;

type
  //------ Context structures ------
  KSCameraRecvImageContext = packed record
    ctxType: int;
    hStream: KSHandle;
    hCamera: KSHandle;
  end;
  PKSCameraRecvImageContext = ^KSCameraRecvImageContext;

  KSCameraDroppedImageContext = packed record
    ctxType: int;
    hStream: KSHandle;
    hCamera: KSHandle;
    imageId: UInt64;
  end;
  PKSCameraDroppedImageContext = ^KSCameraDroppedImageContext;

  KSCameraErrorContext = packed record
    ctxType: int;
    hCamera: KSHandle;
    error: Error;
  end;
  PKSCameraErrorContext = ^KSCameraErrorContext;

  KSCameraAttachContext = packed record
    ctxType: int;
    hController: KSHandle;
    cameraInfo: KSCameraInfo;
  end;
  PKSCameraAttachContext = ^KSCameraAttachContext;

  KSCameraGevAttachContext = packed record
    ctxType: int;
    hAdapter: KSHandle;
    cameraInfo: KSCameraInfoGev;
  end;
  PKSCameraGevAttachContext = ^KSCameraGevAttachContext;

  KSCameraEventContext = packed record
    ctxType: int;
    hCamera: KSHandle;
  end;
  PKSCameraEventContext = ^KSCameraEventContext;

//------ Basic functions ------
function KS_enumCameras(hObject: KSHandle; index: int; pCameraInfo: PKSCameraInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_execCameraCommand(hObject: KSHandle; command: int; address: uint; pData: Pointer; pLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installCameraHandler(hHandle: KSHandle; eventType: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Camera connection functions ------
function KS_openCamera(phCamera: PKSHandle; hObject: KSHandle; pHardwareId: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_openCameraEx(phCamera: PKSHandle; hObject: KSHandle; pCameraInfo: PKSCameraInfo; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeCamera(hCamera: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_readCameraMem(hCamera: KSHandle; var address: Int64; pData: Pointer; length: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_writeCameraMem(hCamera: KSHandle; var address: Int64; pData: Pointer; length: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_configCamera(hCamera: KSHandle; configCommand: int; pName: PAnsiChar; index: int; pData: Pointer; length: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_startCameraAcquisition(hCamera: KSHandle; acquisitionMode: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_stopCameraAcquisition(hCamera: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Stream functions ------
function KS_createCameraStream(phStream: PKSHandle; hCamera: KSHandle; channelNumber: int; numBuffers: int; bufferSize: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closeCameraStream(hStream: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_recvCameraImage(hStream: KSHandle; ppData: PPointer; ppBlockInfo: PPKSCameraBlock; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_releaseCameraImage(hStream: KSHandle; pData: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_CAMERA_MODULE}

//--------------------------------------------------------------------------------------------------------------
// PLC Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error codes ------
  KSERROR_PLC_SCAN_ERROR                        = (KSERROR_CATEGORY_PLC+$00000000);
  KSERROR_PLC_TYPE_ERROR                        = (KSERROR_CATEGORY_PLC+$00010000);
  KSERROR_PLC_BACKEND                           = (KSERROR_CATEGORY_PLC+$00020000);
  KSERROR_PLC_LINKER_ERROR                      = (KSERROR_CATEGORY_PLC+$00030000);
  KSERROR_PLC_INVALID_CONFIG_NUMBER             = (KSERROR_CATEGORY_PLC+$00040000);
  KSERROR_PLC_INVALID_RESOURCE_NUMBER           = (KSERROR_CATEGORY_PLC+$00060000);
  KSERROR_PLC_INVALID_TASK_NUMBER               = (KSERROR_CATEGORY_PLC+$00070000);

  //------ PLC languages ------
  KS_PLC_BYTECODE                               = $00000000;
  KS_PLC_INSTRUCTION_LIST                       = $00000001;
  KS_PLC_STRUCTURED_TEXT                        = $00000002;

  //------ Flags ------
  KSF_USE_KITHARA_PRIORITY                      = $00002000;
  KSF_32BIT                                     = $00000400;
  KSF_64BIT                                     = $00000800;
  KSF_DEBUG                                     = $10000000;
  KSF_PLC_START                                 = $00004000;
  KSF_PLC_STOP                                  = $00008000;
  KSF_IGNORE_CASE                               = $00000010;

  //------ PLC commands ------
  KS_PLC_ABORT_COMPILE                          = $00000001;

  //------ Error levels ------
  KS_ERROR_LEVEL_IGNORE                         = $00000000;
  KS_ERROR_LEVEL_WARNING                        = $00000001;
  KS_ERROR_LEVEL_ERROR                          = $00000002;
  KS_ERROR_LEVEL_FATAL                          = $00000003;

  //------ PLC compiler errors ------
  KS_PLCERROR_NONE                              = $00000000;
  KS_PLCERROR_SYNTAX                            = $00000001;
  KS_PLCERROR_EXPECTED_ID                       = $00000002;
  KS_PLCERROR_EXPECTED_RESCOURCE_ID             = $00000003;
  KS_PLCERROR_EXPECTED_PROG_ID                  = $00000004;
  KS_PLCERROR_EXPECTED_FB_ID                    = $00000005;
  KS_PLCERROR_EXPECTED_FB_INSTANCE              = $00000006;
  KS_PLCERROR_EXPECTED_FUNCTION_ID              = $00000007;
  KS_PLCERROR_EXPECTED_SINGLE_ID                = $00000008;
  KS_PLCERROR_EXPECTED_INTEGER                  = $00000009;
  KS_PLCERROR_EXPECTED_STRUCT                   = $0000000a;
  KS_PLCERROR_EXPECTED_STMT                     = $0000000b;
  KS_PLCERROR_EXPECTED_OPERAND                  = $0000000c;
  KS_PLCERROR_EXPECTED_VAR                      = $0000000d;
  KS_PLCERROR_EXPECTED_TYPE                     = $0000000e;
  KS_PLCERROR_EXPECTED_STRING                   = $0000000f;
  KS_PLCERROR_EXPECTED_VALUE                    = $00000010;
  KS_PLCERROR_EXPECTED_NUMERAL                  = $00000011;
  KS_PLCERROR_EXPECTED_BOOL                     = $00000012;
  KS_PLCERROR_EXPECTED_CONST                    = $00000013;
  KS_PLCERROR_EXPECTED_READ_ONLY                = $00000014;
  KS_PLCERROR_EXPECTED_VAR_INPUT                = $00000015;
  KS_PLCERROR_EXPECTED_VAR_OUTPUT               = $00000016;
  KS_PLCERROR_EXPECTED_SIMPLE_TYPE              = $00000017;
  KS_PLCERROR_EXPECTED_TIME                     = $00000018;
  KS_PLCERROR_EXPECTED_GLOBAL                   = $00000019;
  KS_PLCERROR_EXPECTED_LABEL                    = $0000001a;
  KS_PLCERROR_EXPECTED_INCOMPLETE_DECL          = $0000001b;
  KS_PLCERROR_EXPECTED_VAR_CONFIG               = $0000001c;
  KS_PLCERROR_ID_DEFINED                        = $0000001d;
  KS_PLCERROR_ID_UNKNOWN                        = $0000001e;
  KS_PLCERROR_ID_CONST                          = $0000001f;
  KS_PLCERROR_ID_TOO_MANY                       = $00000020;
  KS_PLCERROR_UNKNOWN_LOCATION                  = $00000021;
  KS_PLCERROR_UNKNOWN_SIZE                      = $00000022;
  KS_PLCERROR_TYPE_MISMATCH                     = $00000023;
  KS_PLCERROR_INDEX_VIOLATION                   = $00000024;
  KS_PLCERROR_FORMAT_MISMATCH                   = $00000025;
  KS_PLCERROR_OVERFLOW                          = $00000026;
  KS_PLCERROR_DATA_LOSS                         = $00000027;
  KS_PLCERROR_NOT_SUPPORTED                     = $00000028;
  KS_PLCERROR_NEGATIVE_SIZE                     = $00000029;
  KS_PLCERROR_MISSING_ARRAY_ELEM_TYPE           = $0000002a;
  KS_PLCERROR_MISSING_RETURN_VALUE              = $0000002b;
  KS_PLCERROR_BAD_VAR_USE                       = $0000002c;
  KS_PLCERROR_BAD_DIRECT_VAR_INOUT              = $0000002d;
  KS_PLCERROR_BAD_LABEL                         = $0000002e;
  KS_PLCERROR_BAD_SUBRANGE                      = $0000002f;
  KS_PLCERROR_BAD_DATETIME                      = $00000030;
  KS_PLCERROR_BAD_ARRAY_SIZE                    = $00000031;
  KS_PLCERROR_BAD_CASE                          = $00000032;
  KS_PLCERROR_DIV_BY_ZERO                       = $00000033;
  KS_PLCERROR_AMBIGUOUS                         = $00000034;
  KS_PLCERROR_INTERNAL                          = $00000035;
  KS_PLCERROR_PARAMETER_MISMATCH                = $00000036;
  KS_PLCERROR_PARAMETER_TOO_MANY                = $00000037;
  KS_PLCERROR_PARAMETER_FEW                     = $00000038;
  KS_PLCERROR_INIT_MANY                         = $00000039;
  KS_PLCERROR_BAD_ACCESS                        = $0000003a;
  KS_PLCERROR_BAD_CONFIG                        = $0000003b;
  KS_PLCERROR_READ_ONLY                         = $0000003c;
  KS_PLCERROR_WRITE_ONLY                        = $0000003d;
  KS_PLCERROR_BAD_EXIT                          = $0000003e;
  KS_PLCERROR_BAD_RECURSION                     = $0000003f;
  KS_PLCERROR_BAD_PRIORITY                      = $00000040;
  KS_PLCERROR_BAD_FB_OPERATOR                   = $00000041;
  KS_PLCERROR_BAD_INIT                          = $00000042;
  KS_PLCERROR_BAD_CONTINUE                      = $00000043;
  KS_PLCERROR_DUPLICATE_CASE                    = $00000044;
  KS_PLCERROR_UNEXPECTED_TYPE                   = $00000045;
  KS_PLCERROR_CASE_MISSING                      = $00000046;
  KS_PLCERROR_DEAD_INSTRUCTION                  = $00000047;
  KS_PLCERROR_FOLD_EXPRESSION                   = $00000048;
  KS_PLCERROR_NOT_IMPLEMENTED                   = $00000049;
  KS_PLCERROR_BAD_CONNECTED_PARAM_VAR           = $0000004a;
  KS_PLCERROR_PARAMETER_MIX                     = $0000004b;
  KS_PLCERROR_EXPECTED_PROGRAM_DECL             = $0000004c;
  KS_PLCERROR_EXPECTED_INTERFACE                = $0000004d;
  KS_PLCERROR_EXPECTED_METHOD                   = $0000004e;
  KS_PLCERROR_ABSTRACT                          = $0000004f;
  KS_PLCERROR_FINAL                             = $00000050;
  KS_PLCERROR_MISSING_ABSTRACT_METHOD           = $00000051;
  KS_PLCERROR_ABSTRACT_INCOMPLETE               = $00000052;
  KS_PLCERROR_BAD_ACCESS_MODIFIER               = $00000053;
  KS_PLCERROR_BAD_METHOD_ACCESS                 = $00000054;
  KS_PLCERROR_ID_INPUT                          = $00000055;
  KS_PLCERROR_BAD_THIS                          = $00000056;
  KS_PLCERROR_MISSING_MODULE                    = $00000057;
  KS_PLCERROR_EXPECTED_DNV                      = $00000058;
  KS_PLCERROR_EXPECTED_DIRECT_VAR               = $00000059;
  KS_PLCERROR_BAD_VAR_DECL                      = $0000005a;
  KS_PLCERROR_HAS_ABSTRACT_BODY                 = $0000005b;
  KS_PLCERROR_BAD_MEMBER_ACCESS                 = $0000005c;
  KS_PLCERROR_EXPECTED_FORMAL_CALL              = $0000005d;
  KS_PLCERROR_EXPECTED_INTERFACE_VAR            = $0000005e;

  //------ PLC runtime errors ------
  KS_PLCERROR_DIVIDED_BY_ZERO                   = $0000012c;
  KS_PLCERROR_ARRAY_INDEX_VIOLATION             = $0000012d;
  KS_PLCERROR_MEMORY_ACCESS_VIOLATION           = $0000012e;
  KS_PLCERROR_IO_VARIABLE_ACCESS                = $0000012f;
  KS_PLCERROR_BAD_VAR_EXTERNAL                  = $00000130;
  KS_PLCERROR_BAD_SUB_RANGE                     = $00000131;

  //------ PLC events ------
  KS_PLC_COMPILE_ERROR                          = (PLC_BASE+$00000001);
  KS_PLC_COMPILE_FINISHED                       = (PLC_BASE+$00000002);
  KS_PLC_RUNTIME_ERROR                          = (PLC_BASE+$00000003);
  KS_PLC_CONFIG_RESSOURCE                       = (PLC_BASE+$00000004);

type
  //------ Context structures ------
  PlcErrorMsgContext = packed record
    ctxType: int;
    hCompiler: KSHandle;
    line: int;
    column: int;
    errorCode: int;
    errorLevel: int;
    flags: int;
    pMsg: array [0..199] of AnsiChar;
  end;
  PPlcErrorMsgContext = ^PlcErrorMsgContext;

  PlcCompileContext = packed record
    ctxType: int;
    hCompiler: KSHandle;
    ksError: int;
    id: int;
  end;
  PPlcCompileContext = ^PlcCompileContext;

  PlcRuntimeErrorContext = packed record
    ctxType: int;
    hPlc: KSHandle;
    line: int;
    column: int;
    errorCode: int;
    errorLevel: int;
    flags: int;
    ksError: int;
  end;
  PPlcRuntimeErrorContext = ^PlcRuntimeErrorContext;

  PlcConfigContext = packed record
    ctxType: int;
    hPlc: KSHandle;
    configIndex: int;
    resourceIndex: int;
    flags: int;
  end;
  PPlcConfigContext = ^PlcConfigContext;

//------ PLC compiler functions ------
function KS_createPlcCompiler(phCompiler: PKSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closePlcCompiler(hCompiler: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_compilePlcFile(hCompiler: KSHandle; sourcePath: PAnsiChar; sourceLanguage: int; destinationPath: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_compilePlcBuffer(hCompiler: KSHandle; pSourceBuffer: Pointer; sourceLength: int; sourceLang: int; pDestBuffer: Pointer; pDestLength: PInt; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ PLC functions ------
function KS_createPlcFromFile(phPlc: PKSHandle; binaryPath: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_createPlcFromBuffer(phPlc: PKSHandle; pBinaryBuffer: Pointer; bufferLength: int; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_closePlc(hPlc: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_startPlc(hPlc: KSHandle; configIndex: int; var start: Int64; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_stopPlc(hPlc: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Information functions ------
function KS_enumPlcConfigurations(hPlc: KSHandle; configIndex: int; pNameBuf: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumPlcResources(hPlc: KSHandle; configIndex: int; resourceIndex: int; pNameBuf: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_enumPlcTasks(hPlc: KSHandle; configIndex: int; resourceIndex: int; taskIndex: int; pNameBuf: PAnsiChar; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Generic functions ------
function KS_execPlcCommand(hPlc: KSHandle; command: int; pData: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_installPlcHandler(hPlc: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_PLC_MODULE}

//--------------------------------------------------------------------------------------------------------------
// Vision Module
//--------------------------------------------------------------------------------------------------------------

//------ Basic functions ------
function KS_loadVisionKernel(phKernel: PKSHandle; dllName: PAnsiChar; initProcName: PAnsiChar; pArgs: Pointer; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ Control functions ------
function KS_installVisionHandler(eventType: int; hSignal: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

type
  //------ Context structures ------
  KSVisionTaskContext = packed record
    ctxType: int;
    hTask: KSHandle;
  end;
  PKSVisionTaskContext = ^KSVisionTaskContext;

const
  //------ Handler events ------
  KS_VISION_TASK_CREATED                        = $00000001;
  KS_VISION_TASK_CLOSED                         = $00000002;

  {$DEFINE KS_INCL_VISION_MODULE}

//--------------------------------------------------------------------------------------------------------------
// SigProc Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Filter types ------
  KS_FIR_LOWPASS_HAMMING                        = $00000000;
  KS_FIR_LOWPASS_HANNING                        = $00000001;
  KS_FIR_LOWPASS_RECTANGULAR                    = $00000002;
  KS_FIR_HIGHPASS_HAMMING                       = $00000003;
  KS_FIR_HIGHPASS_HANNING                       = $00000004;
  KS_FIR_HIGHPASS_RECTANGULAR                   = $00000005;
  KS_FIR_BANDPASS_HAMMING                       = $00000006;
  KS_FIR_BANDPASS_HANNING                       = $00000007;
  KS_FIR_BANDPASS_RECTANGULAR                   = $00000008;
  KS_FIR_BANDSTOP_HAMMING                       = $00000009;
  KS_FIR_BANDSTOP_HANNING                       = $0000000a;
  KS_FIR_BANDSTOP_RECTANGULAR                   = $0000000b;
  KS_IIR_LOWPASS_CHEBYSHEV_I                    = $00000014;
  KS_IIR_LOWPASS_BUTTERWORTH                    = $00000015;
  KS_IIR_LOWPASS_CHEBYSHEV_II                   = $00000016;
  KS_IIR_HIGHPASS_CHEBYSHEV_I                   = $00000017;
  KS_IIR_HIGHPASS_BUTTERWORTH                   = $00000018;
  KS_IIR_HIGHPASS_CHEBYSHEV_II                  = $00000019;
  KS_IIR_BANDPASS_CHEBYSHEV_I                   = $0000001a;
  KS_IIR_BANDPASS_BUTTERWORTH                   = $0000001b;
  KS_IIR_BANDPASS_CHEBYSHEV_II                  = $0000001c;
  KS_IIR_BANDSTOP_CHEBYSHEV_I                   = $0000001d;
  KS_IIR_BANDSTOP_BUTTERWORTH                   = $0000001e;
  KS_IIR_BANDSTOP_CHEBYSHEV_II                  = $0000001f;
  KS_IIR_INDIVIDUAL                             = $00000020;

  //------ Standard ripples ------
  KS_CHEBYSHEV_3DB                              = 0.707106781;
  KS_CHEBYSHEV_1DB                              = 0.891250938;

  //------ Some filter specified errorcodes ------
  KSERROR_SP_SHANNON_THEOREM_MISMATCH           = (KSERROR_CATEGORY_SPECIAL+$00020000);

type
  //------ Paramters structures ------
  KSSignalFilterParams = packed record
    structSize: int;
    filterType: int;
    order: int;
    maxFrequency: Double;
    frequency: Double;
    sampleRate: Double;
    width: Double;
    delta: Double;
  end;
  PKSSignalFilterParams = ^KSSignalFilterParams;

  KSIndividualSignalFilterParams = packed record
    structSize: int;
    pXCoeffs: PDouble;
    xOrder: int;
    pYCoeffs: PDouble;
    yOrder: int;
    maxFrequency: Double;
    sampleRate: Double;
  end;
  PKSIndividualSignalFilterParams = ^KSIndividualSignalFilterParams;

  KSPIDControllerParams = packed record
    structSize: int;
    proportional: Double;
    integral: Double;
    derivative: Double;
    setpoint: Double;
    sampleRate: Double;
  end;
  PKSPIDControllerParams = ^KSPIDControllerParams;

//------ Digital Filter functions ------
function KS_createSignalFilter(phFilter: PKSHandle; pParams: PKSSignalFilterParams; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_createIndividualSignalFilter(phFilter: PKSHandle; pParams: PKSIndividualSignalFilterParams; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_getFrequencyResponse(hFilter: KSHandle; frequency: Double; pAmplitude: PDouble; pPhase: PDouble; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_scaleSignalFilter(hFilter: KSHandle; frequency: Double; value: Double; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_resetSignalFilter(hFilter: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ PID Controller functions ------
function KS_createPIDController(phController: PKSHandle; pParams: PKSPIDControllerParams; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_changeSetpoint(hController: KSHandle; setpoint: Double; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

//------ General signal processing functions ------
function KS_removeSignalProcessor(hSigProc: KSHandle; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';
function KS_processSignal(hSigProc: KSHandle; input: Double; pOutput: PDouble; flags: int):
  Error; stdcall; external 'KrtsDemo.dll';

{$DEFINE KS_INCL_SIGPROC_MODULE}

//--------------------------------------------------------------------------------------------------------------
// implementation
//--------------------------------------------------------------------------------------------------------------

implementation

//------ Helper functions ------
function KSERROR_CODE(error: Error):  Error;
begin
  KSERROR_CODE := error and $3fff0000;
end;

function mk16(lo, hi : byte):
  word;
begin
  mk16 := word(lo or (word(hi) shl 8));
end;

function mk32(lo, hi : word):
  uint;
begin
  mk32 := uint(lo or (uint(hi) shl 16));
end;

function KS_htons(val : word):
  word;
begin
  KS_htons := mk16(byte(val shr 8), byte(val));
end;

function KS_htonl(val : uint):
  uint;
begin
  KS_htonl := mk32(KS_htons(word(val shr 16)), KS_htons(word(val)));
end;

function KS_ntohs(val : word):
  word;
begin
  KS_ntohs := KS_htons(val);
end;

function KS_ntohl(val : uint):
  uint;
begin
  KS_ntohl := KS_htonl(val);
end;

procedure KS_makeIPv4(pAddr : PUint; b3, b2, b1, b0 : byte);
begin
  pAddr^ := (b0 shl 24) or (b1 shl 16) or (b2 shl 8) or b3;
end;

end.

