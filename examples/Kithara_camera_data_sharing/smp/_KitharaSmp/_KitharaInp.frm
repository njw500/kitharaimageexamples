VERSION 5.00
Begin VB.Form inputForm 
   BorderStyle     =   3  'Fester Dialog
   ClientHeight    =   2310
   ClientLeft      =   2835
   ClientTop       =   3480
   ClientWidth     =   3750
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1364.825
   ScaleMode       =   0  'Benutzerdefiniert
   ScaleWidth      =   3521.047
   StartUpPosition =   2  'Bildschirmmitte
   Begin VB.TextBox Text1 
      Height          =   345
      Left            =   210
      TabIndex        =   0
      Top             =   1215
      Width           =   3405
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   390
      Left            =   495
      TabIndex        =   1
      Top             =   1800
      Width           =   1140
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   390
      Left            =   2100
      TabIndex        =   2
      Top             =   1800
      Width           =   1140
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   975
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   3375
   End
End
Attribute VB_Name = "inputForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
  Me.Text1.text = ""
  Me.Hide
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub

