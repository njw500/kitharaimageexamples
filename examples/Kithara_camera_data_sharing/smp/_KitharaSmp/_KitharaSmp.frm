VERSION 5.00
Begin VB.Form mainForm 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fest Einfach
   Caption         =   "Kithara Example Program"
   ClientHeight    =   4650
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7875
   MaxButton       =   0   'False
   ScaleHeight     =   4650
   ScaleWidth      =   7875
   StartUpPosition =   2  'Bildschirmmitte
   Begin VB.TextBox Text1 
      Height          =   4335
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Beides
      TabIndex        =   0
      Top             =   120
      Width           =   7575
   End
End
Attribute VB_Name = "mainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Dim inputDlg As New inputForm

Private Sub Form_Load()
  'Me.Left = GetSetting(App.Title, "Settings", "MainLeft", 1000)
  'Me.Top = GetSetting(App.Title, "Settings", "MainTop", 1000)
  'Me.Width = GetSetting(App.Title, "Settings", "MainWidth", 6500)
  'Me.Height = GetSetting(App.Title, "Settings", "MainHeight", 6500)
  'Command1.caption = "Start sample"
  
  ' There is no multi-threading yet!
  
  'If Not outputErr(KS_openDriver(customerNumber), "KS_openDriver") Then
  '  outputErr KS_createEvent(hGlobalEvent, "UserEvent", 0), "KS_createEvent"
  '  outputErr KS_createThread(AddressOf runRoutine, 0, 0), "KS_createThread"
  'End If
  ' We must call the Show at least once, before we use it in our threads.
  'inputDlg.Show
  'inputDlg.Hide
  Me.Show
  
  ' we initialize a constant for conversion of timeouts
  ms = 10000
  runSample
End Sub

Private Sub Form_Paint()
  Text1.Top = 0
  Text1.Left = 0
  Text1.Width = Me.Width - 100
  Text1.Height = Me.Height - 500
End Sub

Private Sub Form_Unload(Cancel As Integer)
  ' Dont close this event here, it could produce an error.
  'outputErr KS_closeEvent(hGlobalEvent), "KS_closeEvent"
  'outputErr KS_closeDriver(), "KS_closeDriver"
  
  'If Me.WindowState <> vbMinimized Then
  '  SaveSetting App.Title, "Settings", "MainLeft", Me.Left
  '  SaveSetting App.Title, "Settings", "MainTop", Me.Top
  '  SaveSetting App.Title, "Settings", "MainWidth", Me.Width
  '  SaveSetting App.Title, "Settings", "MainHeight", Me.Height
  'End If
  
  End
End Sub

Private Sub Command1_Click()
  'outputErr KS_setEvent(hGlobalEvent), "KS_setEvent"
  'bGlobalStart = True
End Sub

'------ inputTxt ------
Public Function inputTxt(ByVal caption As String, Optional ByVal default As String = "") As String
  ' Dont use InputBox, it makes problems in our threads.
  inputTxt = InputBox(caption, "Kithara", default)
  
  'inputDlg.caption = "Input"
  'inputDlg.Label1 = caption
  'inputDlg.Text1.text = default
  'Dont use Show with the vbModal Parameter, it raise an exception.
  'inputDlg.Show
  'Do While inputDlg.Visible = True
  '  Sleep 100
  'Loop
  'If inputDlg.Text1.text = "" Then
  '  inputTxt = default
  'Else
  '  inputTxt = inputDlg.Text1.text
  'End If
End Function

