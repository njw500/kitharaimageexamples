﻿/* Copyright (c) 2003-2012 by Kithara Software. All rights reserved. */

//##############################################################################################################
//
// Module:           _KitharaSmp.h
//
// Descript.:        Definition of helper functions for all of the samples
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// u.jes 2003-01-08  CREAT: Original - (file created)
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:02
//
//##############################################################################################################

   /*=====================================================================*\
   |                    *** DISCLAIMER OF WARRANTY ***                     |
   |                                                                       |
   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
   \*=====================================================================*/

//##############################################################################################################
//
// Purpose:
// The functions in this file are some helper functions for all of our sample
// programs. They are used for input and output, so they are compatible to the
// other programming languages and environments supported.
//
//##############################################################################################################

#ifndef ___KitharaSmp_H
#define ___KitharaSmp_H


//--------------------------------------------------------------------------------------------------------------
// _KitharaSmp
//--------------------------------------------------------------------------------------------------------------

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <conio.h>
#include <string.h>

#include "..\..\dev\KrtsDemo.h"

#if defined(_MSC_VER) && _MSC_VER <= 1200
#define for if (0) {} else for
#endif

#ifdef _MSC_VER
inline int myGetch() { return _getch(); }
inline void myPutch(int ch) { _putch(ch); }
inline int myStricmp(const char *s1, const char *s2) { return _stricmp(s1, s2); }
inline int myKbhit() { return _kbhit(); }
#else
inline int myGetch() { return getch(); }
inline void myPutch(int ch) { putch(ch); }
inline int myStricmp(const char *s1, const char *s2) { return stricmp(s1, s2); }
inline int myKbhit() { return kbhit(); }
#endif

const int ms    = 10000;
const int s     = ms * 1000;

void outputTxt(
        const char* pText, bool line = true);
void outputBool(
        int boolean, const char* pPreTxt = "", const char* pPostTxt = "", bool line = true);
void outputDec(
        int dec, const char* pPreTxt = "", const char* pPostTxt = "", bool line = true);
void outputDec16(
        int64 dec, const char* pPreText = "", const char* pPostText = "", bool line = true);
void outputHex(
        int hex, const char* pPreTxt = "", const char* pPostTxt = "", bool line = true);
void outputHex02(
        int hex, const char* pPreTxt = "", const char* pPostTxt = "", bool line = true);
void outputHex04(
        int hex, const char* pPreTxt = "", const char* pPostTxt = "", bool line = true);
void outputHex08(
        int hex, const char* pPreTxt = "", const char* pPostTxt = "", bool line = true);
void outputErr(
        Error error, const char* pFuncName, const char* pComment = "");
char* inputTxt(
        const char* pPreText = "", const char* pDefault = "");
int inputDec(
        const char* pPreText = "", const char* pDefault = "");
int inputDec(
        const char* pPreText, int defaultVal);
int inputHex(
        const char* pPreText = "", const char* pDefault = "");
int inputHex(
        const char* pPreText, int defaultVal);
void waitTime(
        uint time);
void runSample();


//--------------------------------------------------------------------------------------------------------------
// TinyFile - short file class for example programming.
//--------------------------------------------------------------------------------------------------------------

class TinyFile {
public:
  enum { WRITEABLE = 0x0004, OVERWRITE = 0x0100 };
  enum { BEGIN = 0 /* FILE_BEGIN */, CURRENT = 1 /* FILE_CURRENT */, END = 2 /* FILE_END */ };

  TinyFile(const char* pName);
  virtual ~TinyFile()                           { close(); }

  bool isOpened() const                         { return hFile_ != 0; }

  bool open(int flags = 0);
  void close();
  int64 seek(int64 pos, int dir = BEGIN);
  bool read(void* pData, int length, int* pLength = NULL);
  bool write(void* pData, int length = 0, int* pLength = NULL);
  Win32Handle handle() const                    { return hFile_; }

protected:
  Win32Handle hFile_;
  char pFileName_[256];

private:
  // Prevent copying and assignment.
  TinyFile(const TinyFile&);
  TinyFile& operator = (const TinyFile&);
};


//--------------------------------------------------------------------------------------------------------------
// TinySerialPort - short COM port class for example programming.
//--------------------------------------------------------------------------------------------------------------

class TinySerialPort : public TinyFile {
public:
  TinySerialPort(const char* pPortName, uint baudRate = 0);

  bool setBaudRate(uint baudRate);

private:
  // Prevent copying and assignment.
  TinySerialPort(const TinySerialPort& orig);
  TinySerialPort& operator = (const TinySerialPort& orig);
};

#endif // ___KitharaSmp_H
