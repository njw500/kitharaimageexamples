﻿/* Copyright (c) 2015 by Kithara Software. All rights reserved. */
//test
//##############################################################################################################
//
// File:             VisionOpenCVWithCamera.cpp
//
// Used modules:     Vision Module, Camera Module, Kernel Module, Task Module
//
// Descript.:        Heavility modified Kithara sample application, demonstating use of the OpenCV library in 
//                   real-time tasks, with images supplied via an attached USB3 Camera.
//
//					 Note: Camera should be attached to a dedicated USB3 controller.
//
//                   This is the main program code.
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// c.was 2015-02-04  CREAT: Original - (file created)
//       2016-12-16  MOD: Modified to pass image results from opencv routines running in the kernel to the 
//                        process running in uer mode.
//
//##############################################################################################################

/*=====================================================================*\
|                    *** DISCLAIMER OF WARRANTY ***                     |
|                                                                       |
|       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
|       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
|       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
|       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
\*=====================================================================*/

//##############################################################################################################
//
// Purpose:
//
// This example demonstrates the execution of a OpenCV 3.0 application on real-time level in
// conjunction with acquisition of images from a GigE Vision camera, which are processed with OpenCV.
//
// It will load a DLL on kernel-level, which contains the OpenCV code and code for handling image reception.
//
// IMPORTANT NOTICE:
// This sample requires the OpenCV 3.0 libraries built as static libs. These libraries must have been built
// using the statically linked runtime library (CRT). The prebuild version is usable for this sample,
// but we recommend to build the OpenCV libraries with your compiler.
// Please see our OpenCV guidelines at http://www.kithara.de/en/docs/krts:tutorial:vision_opencv for further
// information.
//
// IMPORTANT NOTICE:
// For this sample to find the OpenCV 3.0 libraries and include files it assumes the environment variable
// OPENCV_DIR to exist. OPENCV_DIR must contain the installation directory of OpenCV 3.0.
// The installation directory is the one that contains the directory "include" (containing all OpenCV API
// include files) and the binary directories "x64" and/or "x86" (containing the staticlibs).
// Without this environment variable this sample won't build!
//
// Examples:
//
// When using the prebuild libraries, which have been copied to C:\opencv use:
// OPENCV_DIR = C:\opencv\build
//
// When having a custom build made in the folder C:\opencv\build, with additional copying of install files to
// the default folder (build target INSTALL) use:
// OPENCV_DIR = C:\opencv\build\install
//
//##############################################################################################################

#ifdef __BORLANDC__
#pragma inline
#pragma warn -rvl                                       // Function should return a value
#endif

#include "VisionOpenCVWithCamera.h"

#include <Windows.h>
#include <iostream>
#include <sstream>

using std::vector;

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
// Don't forget to enter your zero-leading, 6-digit customer number!
// This is required to open the driver.
//
// If you use a Demo Version, "DEMO" is also allowed instead.
// If you use a Beta Version, "BETA" is also allowed instead.
//
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const char* pCustomerNumber = "DEMO";

void deserialize(cv::Mat* pObject, void* pMemory);
void deserialize(vector<vector<cv::Point>>* pObject, void* pMemory);

//--------------------------------------------------------------------------------------------------------------
// Define the prototypes for some handling functions.
//--------------------------------------------------------------------------------------------------------------

bool openCvInit(SharedData* pSharedData, int cpu);
bool openCvExit(SharedData* pSharedData);

bool cameraInit(SharedData* pSharedData, int cpu);
bool cameraExit(SharedData* pSharedData);

bool cameraConfig(SharedData* pSharedData);
bool cameraRestoreConfig(SharedData* pSharedData);

//--------------------------------------------------------------------------------------------------------------
// Please pay special attention to include the OpenCV headers and other non-Kithara headers with a default
// structure packing of 8 in effect, since our samples are configured to a packing of 1. Otherwise there could
// be possible binary incompabilities with unpredictable results.
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push, 8)

#include <opencv2\opencv.hpp>
#include <vector>

#pragma pack(pop)

using std::vector;



//--------------------------------------------------------------------------------------------------------------
// This is the main program
//--------------------------------------------------------------------------------------------------------------

void runSample() {
	outputTxt("***** Kithara example program 'VisionOpenCVWithCamera' *****");

	Error ksError;

	//------------------------------------------------------------------------------------------------------------
	// Opening the driver is always the first step! After that, we can use other functions. If the opening fails,
	// no other function can be called.
	//
	// This function takes your customer number with Kithara (or "DEMO" or "BETA" if applicable) as a parameter.
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_openDriver(
		pCustomerNumber);                         // Customer number
	if (ksError) {
		outputErr(ksError, "KS_openDriver", "Unable to open the driver!");
		return;
	}


	//------------------------------------------------------------------------------------------------------------
	// Create a shared memory region which holds all data that is shared between user mode and kernel mode.
	//------------------------------------------------------------------------------------------------------------

	SharedData* pAppPtr;
	SharedData* pSysPtr;
	ksError = KS_createSharedMem((void**)&pAppPtr, (void**)&pSysPtr, 0, sizeof(SharedData), 0);
	if (ksError) {
		outputErr(ksError, "KS_createSharedMem", "Unable to create shared memory");
		KS_closeDriver();
		return;
	}

	pAppPtr->pAppPtr = pAppPtr;
	pAppPtr->pSysPtr = pSysPtr;
	pAppPtr->abort = false;
	pAppPtr->ready = false;



	//------------------------------------------------------------------------------------------------------------
	// Create a mutex to manage memory copying between kernel and user space
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_createQuickMutex(
		&pAppPtr->hMemoryLock,                    // Dataset handle
		KS_RTX_LEVEL,                             // State
		KSF_KERNEL_EXEC);                         // Flags
	if (ksError) {
		outputErr(ksError, "KS_createQuickMutex", "Failed to create mutex");
		KS_closeDriver();
		return;
	}

	//------------------------------------------------------------------------------------------------------------
	// Load the kernel DLL. This DLL has the OpenCV libraries linked and contains all kernel code.
	//
	// In contrast to KS_loadKernel, KS_loadVisionKernel will automatically load all
	// depending libraries and execute their entry points on real-time level in the proper environment.
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_loadVisionKernel(
		&pAppPtr->hKernel,                        // Address of handle returned
		"kernel.dll",                             // Name of the dll to be loaded
		NULL,                                     // Name of init function to execute after relocated dll
		NULL,                                     // Arguments for that function
		KSF_KERNEL_EXEC);                         // Flags, load to kernel level
	if (ksError) {
		outputErr(ksError, "KS_loadVisionKernel", "Unable to relocate dlls or problems while executing entrypoints");
		KS_closeDriver();
		return;
	}


	//------------------------------------------------------------------------------------------------------------
	// Create an event that is used for signalling the OpenCV task from withing the camera image reception handler
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_createEvent(
		&pAppPtr->hEvent,                               // Address of handle returned
		NULL,                                           // Name of the handle (not required)
		KSF_KERNEL_EXEC);                               // Flags, here waitable for kernel-tasks
	if (ksError) {
		outputErr(ksError, "KS_createEvent", "Unable to create event");
		KS_closeDriver();
		return;
	}



	//------------------------------------------------------------------------------------------------------------
	// We query the number of CPUs with KS_getSystemInformation, so we can dispatch image reception and the
	// computer vision task onto different CPUs
	//------------------------------------------------------------------------------------------------------------

	int visionCpu = 0;
	int cameraCpu = 0;

	KSSystemInformation systemInfo;
	systemInfo.structSize = sizeof(KSSystemInformation);
	ksError = KS_getSystemInformation(
		&systemInfo,                              // Struct to fill
		0);                                       // Flags, here none

	if (ksError) {
		outputTxt("Warning, unable to query system information.");
	}
	else {
		visionCpu = systemInfo.numberOfCPUs - 1;
		cameraCpu = visionCpu - 1;
		if (cameraCpu < 0)
			cameraCpu = 0;
	}


	//------------------------------------------------------------------------------------------------------------
	// Initialize all resources required for the OpenCV real-time task.
	//------------------------------------------------------------------------------------------------------------

	if (!openCvInit(pAppPtr, visionCpu)) {
		outputTxt("OpenCV set up failed");
		KS_closeDriver();
		return;
	}


	//------------------------------------------------------------------------------------------------------------
	// Open a camera connection, configure the camera and create a stream
	//------------------------------------------------------------------------------------------------------------

	if (!cameraInit(pAppPtr, cameraCpu)) {
		outputTxt("Camera set up failed");
		KS_closeDriver();
		return;
	}


	//------------------------------------------------------------------------------------------------------------
	// Now start the image acquisition
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_startCameraAcquisition(
		pAppPtr->hCamera,                         // Handle to camera
		KS_CAMERA_CONTINUOUS,                     // Acquisition mode, here 'Continuous'
		0);                                       // Flags, none
	if (ksError) {
		outputErr(ksError, "KS_startCameraAcquisition", "Unable to start acquisition.");
		cameraExit(pAppPtr);
		KS_closeDriver();
		return;
	}


	//------------------------------------------------------------------------------------------------------------
	// Wait for user to abort acquisition
	//------------------------------------------------------------------------------------------------------------

	outputTxt("Acquisition started. Press any key to abort.");

	cv::Mat image;
	byte image3[64000];
	cv::namedWindow("VisionOpenCVDisplay", cv::WINDOW_AUTOSIZE);

	int old_time = 0;

	// Set the time between updates to the users screen

	int frame_time_ms = 10;

	while (true) {

		//----------------------------------------------------------------------------------------------------------
		// Check if user aborted
		//----------------------------------------------------------------------------------------------------------

		if (myKbhit()) {
			myGetch();
			break;
		}

		//----------------------------------------------------------------------------------------------------------
		// We check the status of the OpenCV task
		//----------------------------------------------------------------------------------------------------------

		uint taskState;
		Error exitCode;
		ksError = KS_getTaskState(
			pAppPtr->hOpenCvTask,
			&taskState,
			&exitCode
			);
		if (ksError != KS_OK || taskState == KS_TASK_TERMINATED) {
			outputTxt("Error: OpenCV task is not running.");
			break;
		}

		// Set a delay so we aren't reading results from the kernel continuously

		waitTime(frame_time_ms * ms);

		if (pAppPtr->width2 > 0){

			KS_memCpy(&image3, pAppPtr->image2, 64000 * sizeof(byte), 0);

			cv::Mat image4 = cv::Mat(pAppPtr->height2, pAppPtr->width2, CV_8UC1, image3);

			// Displaying the most recent camera frame using an OpenCV image window
			cv::imshow("VisionOpenCVDisplay", image4);
			cv::waitKey(1);

		}

	}


	//------------------------------------------------------------------------------------------------------------
	// Stop acquisition
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_stopCameraAcquisition(
		pAppPtr->hCamera,                         // Handle to camera
		0);                                       // Flags, none
	if (ksError) {
		outputErr(ksError, "KS_stopCameraAcquisition", "Unable to stop acquisition.");
	}


	//------------------------------------------------------------------------------------------------------------
	// Free all resources used by the camera
	//------------------------------------------------------------------------------------------------------------

	if (!cameraExit(pAppPtr))
		outputTxt("Error: Camera clean up failed.");


	//------------------------------------------------------------------------------------------------------------
	// Free all resources used by OpenCV
	//------------------------------------------------------------------------------------------------------------

	if (!openCvExit(pAppPtr))
		outputTxt("Error: OpenCV clean up failed.");


	//------------------------------------------------------------------------------------------------------------
	// Close the event
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_closeEvent(pAppPtr->hEvent);
	if (ksError)
		outputErr(ksError, "KS_freeKernel", "Cannot close the event.");


	//------------------------------------------------------------------------------------------------------------
	// Free the kernel library
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_freeKernel(pAppPtr->hKernel);
	if (ksError)
		outputErr(ksError, "KS_freeKernel", "Cannot free the kernel.");


	//------------------------------------------------------------------------------------------------------------
	// Free the shared memory
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_freeSharedMem(pAppPtr);
	if (ksError)
		outputErr(ksError, "KS_freeSharedMem", "Cannot free shared memory.");


	//------------------------------------------------------------------------------------------------------------
	// Close the driver, to release all allocated resources
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_closeDriver();
	if (ksError) {
		outputErr(ksError, "KS_closeDriver", "Cannot close driver.");
		return;
	}

	outputTxt("End of program 'VisionOpenCVWithCamera'. Press <enter> to close.");
}

//--------------------------------------------------------------------------------------------------------------
// openCvInit
//
// This function sets everything up for a OpenCV task to work. It creates a callback and a task, that is
// triggered immediately.
//--------------------------------------------------------------------------------------------------------------

bool openCvInit(SharedData* pSharedData, int cpu) {
	Error ksError;

	//------------------------------------------------------------------------------------------------------------
	// We want all OpenCV stuff to be handled by one CPU. So we will set the target CPU. All tasks created from
	// now will be put on this CPU. If the CPU is not available, the function will fail, and the target CPU is
	// not changed. We issue a warning in that case.
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_setTargetProcessor(
		cpu,                                      // CPU to use
		0);                                       // Flags, here none

	if (ksError != KS_OK) {
		outputErr(ksError, "KS_setTargetProcessor", "Unable to set CPU for vision tasks");
		return false;
	}


	//------------------------------------------------------------------------------------------------------------
	// Create a callback to the function in the kernel DLL we want to execute.
	//
	// For callbacks calling any OpenCV function from kernel-level the KSF_SAVE_FPU flag must be set and
	// the callbacks must only be run as tasks.
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_createKernelCallBack(
		&pSharedData->hOpenCvCallback,            // Address of callback handle
		pSharedData->hKernel,                     // Handle to loaded dll
		"_openCvAction",                          // Name of function to be used
		(void*)pSharedData->pSysPtr,             // Argument for callback,
		KSF_DIRECT_EXEC |                         // Flags, execute on kernel level
		KSF_NO_CONTEXT |                          // Callback function has no context argument
		KSF_SAVE_FPU,                             // Save fpu state on context switch, required if
		// you have calls to OpenCV functions in this callback
		0);                                       // Priority, not needed here

	if (ksError != KS_OK) {
		outputErr(ksError, "KS_createKernelCallBack", "Unable to create kernel callback");
		return false;
	}


	//------------------------------------------------------------------------------------------------------------
	// Before creating the task which uses the callback, set the stack size for task to a larger value then the
	// default for kernel-mode task (which is only 3KB). This is necessary, because the OpenCV code assumes to
	// be running a real Windows thread, which comes with a stack of 1 MB by default.
	// Don't forget to use the flags KSF_CUSTOM_STACK_SIZE when creating the task!
	//------------------------------------------------------------------------------------------------------------

	//ksError = KS_setTaskStackSize(
	//	0x100000,                                 // Size in bytes of the stack to use for task
	//	0);                                       // Flags, here none
	//if (ksError != KS_OK) {
	//	outputErr(ksError, "KS_setTaskStackSize", "Unable to set task stack size");
	//	return false;
	//}

	ksError = KS_setTaskStackSize(
		0xFFF,                                 // Size in bytes of the stack to use for task
		0);                                       // Flags, here none
	if (ksError != KS_OK) {
		outputErr(ksError, "KS_setTaskStackSize", "Unable to set task stack size");
		return false;
	}


	//------------------------------------------------------------------------------------------------------------
	// Now we create the task for the OpenCV operation. All OpenCV operations must be performed from a task.
	// To let the task have the new stack size we just set, the flags KSF_CUSTOM_STACK_SIZE must be used.
	// The task will start immediately.
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_createTask(
		&pSharedData->hOpenCvTask,                // Address of task handle
		pSharedData->hOpenCvCallback,             // Callback object to be executed
		250,                                      // Priority of the task
		KSF_CUSTOM_STACK_SIZE                     // Flags, here enable the customized stack size
		);

	if (ksError != KS_OK) {
		outputErr(ksError, "KS_createTask", "Unable to create task");
		return false;
	}


	//------------------------------------------------------------------------------------------------------------
	// We're done
	//------------------------------------------------------------------------------------------------------------

	return true;
}


//--------------------------------------------------------------------------------------------------------------
// openCvExit
//
// trys to stop the OpenCV task and releases all resources used for OpenCV
//--------------------------------------------------------------------------------------------------------------

bool openCvExit(SharedData* pSharedData) {

	Error ksError;

	//------------------------------------------------------------------------------------------------------------
	// Signal an abort and set the event for the task using KS_setEvent. Wait some time for the task to finish.
	//------------------------------------------------------------------------------------------------------------

	pSharedData->abort = true;
	ksError = KS_setEvent(pSharedData->hEvent);
	if (ksError != KS_OK)
		outputErr(ksError, "KS_setEvent", "Unable to set event");

	waitTime(500 * ms);


	//------------------------------------------------------------------------------------------------------------
	// Remove the task
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_removeTask(pSharedData->hOpenCvTask);
	if (ksError != KS_OK)
		outputErr(ksError, "KS_removeTask", "Unable to remove task!");


	//------------------------------------------------------------------------------------------------------------
	// Remove the callback
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_removeCallBack(pSharedData->hOpenCvCallback);
	if (ksError != KS_OK)
		outputErr(ksError, "KS_removeCallBack", "Unable to remove callback!");


	//------------------------------------------------------------------------------------------------------------
	// We're done
	//------------------------------------------------------------------------------------------------------------

	return true;
}


//--------------------------------------------------------------------------------------------------------------
// ipv4ToString
//
// generates a readable representation of an IPv4 address
//--------------------------------------------------------------------------------------------------------------

char* ipv4ToString(char* pDest, uint addr) {
	addr = KS_ntohl(addr);
	sprintf(pDest, "%d.%d.%d.%d", addr >> 24 & 0xFF, addr >> 16 & 0xFF, addr >> 8 & 0xFF, addr >> 0 & 0xFF);
	return pDest;
}


//--------------------------------------------------------------------------------------------------------------
// cameraInit
//
// This function sets up everything for the camera to work. It will open a network adapter, a camera, configure
// it, and create a stream and install a handler for it to receive images.
//--------------------------------------------------------------------------------------------------------------
KSHandle hAdapter;

bool cameraInit(SharedData* pSharedData, int cpu) {
	Error ksError;

	//------------------------------------------------------------------------------------------------------------
	// First, the names of all network adapters are queried and displayed
	//------------------------------------------------------------------------------------------------------------

	char pDeviceName[256];

	outputTxt(" ");
	outputTxt("Following network adapters found:");

	int i;
	for (i = 0;; ++i) {

		//----------------------------------------------------------------------------------------------------------
		// The following function can be used to query the names of all network adapters assigned to the Kithara
		// Driver. The number 'i' runs beginning from zero.
		//----------------------------------------------------------------------------------------------------------

		ksError = KS_enumDevices(
			"NET",                                  // 'NET' searches for network devices
			i,                                      // Count, starting with zero
			pDeviceName,                            // Buffer for device name
			0);                                     // Flags
		if (ksError != KS_OK) {
			if (KSERROR_CODE(ksError) != KSERROR_DEVICE_NOT_FOUND)
				outputErr(ksError, "KS_enumDevices",
				"Unable to query network device name!");

			if (KSERROR_CODE(ksError) == KSERROR_DEVICE_NOT_FOUND && !i) {
				outputTxt("No devices found");
				outputTxt(" ");
				outputTxt("Did you switch the network card driver to Kithara?");
				outputTxt("Please refer to the manual, chapter 16 \"Packet Module\"");
				outputTxt("for further instructions!");
				return false;
			}
			break;
		}

		outputDec(i, "", ": ", false);
		outputTxt(pDeviceName);
	}
	outputTxt(" ");


	//------------------------------------------------------------------------------------------------------------
	// Please enter the index of the adapter, which should be opened
	//------------------------------------------------------------------------------------------------------------

	int deviceIndex = i > 1 ? inputDec("Device number: ", "") : 0;

	ksError = KS_enumDevices(
		"NET",                                    // 'NET' searches for network devices
		deviceIndex,                              // Count, starting with zero
		pDeviceName,                              // Buffer for device name
		0);                                       // Flags
	if (ksError != KS_OK) {
		outputErr(ksError, "KS_enumDevices", "Unable to query network device name!");
		return false;
	}

	outputTxt("Selected device: ", false);
	outputTxt(pDeviceName);


	//------------------------------------------------------------------------------------------------------------
	// We want all camera stuff to be handled by one CPU. So before opening the Ethernet adapter,
	// we will set the target CPU. All interrupts used by devices and tasks created from now will be put on this
	// CPU. If the CPU is not available, the function will fail, and the target CPU is not changed. We issue a
	// warning in that case.
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_setTargetProcessor(
		cpu,                                      // CPU to use
		0);                                       // Flags, here none

	if (ksError != KS_OK) {
		outputErr(ksError, "KS_setTargetProcessor", "Unable to set CPU for camera");
		return false;
	}


	//------------------------------------------------------------------------------------------------------------
	// Now the Ethernet Adapter will be opened.
	// The name is a part of the device name of the adapter.
	// "DEV_1229" would work, if there is just one Intel8255x in the system.
	//------------------------------------------------------------------------------------------------------------

	int adapters = 0;

	if (deviceIndex >= adapters) {
		ksError = KS_enumDevicesEx(
			"XHCI",                               // 'XHCI' searches for XHCI controllers
			"PCI",                                // Bus to search on, here PCI
			KS_INVALID_HANDLE,                    // Not needed
			deviceIndex - adapters,               // Count, starting with zero
			pDeviceName,                          // Buffer for device name
			0);                                   // No flags

		if (ksError != KS_OK) {
			outputErr(ksError, "KS_enumDevicesEx", "Unable to query XHCI controller name!");
			KS_closeDriver();
			return false;
		}

		outputTxt("Selected device: ", false);
		outputTxt(pDeviceName);

		//--------------------------------------------------------------------------------------------------------
		// Now the XHCI controller will be opened
		// The name is a part of the device name of the controller
		//--------------------------------------------------------------------------------------------------------

		ksError = KS_openXhciController(
			&hAdapter,                         // Controller handle
			pDeviceName,                          // Device name of the XHCI controller
			0);                                   // Flags, none required here

		if (ksError != KS_OK) {
			outputErr(ksError, "KS_openXhciController", "Failed to open XHCI controller");
			KS_closeDriver();
			return false;
		}

		//------------------------------------------------------------------------------------------------------------
		// Now we enumerate all cameras with KS_enumCameras.
		// KS_enumCameras must be called with an enumeration index of zero first, to trigger a GigE Vision discovery
		// broadcast the cameras will answer.
		// Note, that a camera will not answer a discovery packet, before it has completed its IP configuration.
		// So it may take some time until a newly attached camera will show up.
		// Another way of dealing with this is by installing handlers for the KS_CAMERA_ATTACHED / DETACHED event
		// (having KSCameraAttachContext).
		// For the purpose of this sample, the use of KS_enumCameras is sufficient.
		// On success, KS_enumCameras returns information of the camera in the KSCameraInfo structure.
		//------------------------------------------------------------------------------------------------------------

		KSCameraInfo ksCameraInfo;
		ksCameraInfo.structSize = sizeof(KSCameraInfo);

		outputTxt("Searching cameras. Press any key to abort!");
		outputTxt("The following cameras have been found:");

		for (i = 0;;) {

			ksError = KS_enumCameras(
				hAdapter,                               // NIC adapter on which to enumerate
				i,                                      // Index of enumeration, starting with zero
				&ksCameraInfo,                          // Pointer to a KSCameraInfo structure
				0);                                     // No flags

			if (ksError == KS_OK) {
				outputDec(i, "", ": ", false);
				outputTxt(ksCameraInfo.vendor, false);
				outputTxt(" ", false);
				outputTxt(ksCameraInfo.name, false);
				outputTxt(" ", false);
				outputTxt(ksCameraInfo.hardwareId);
				++i;
			}


			//----------------------------------------------------------------------------------------------------------
			// Aborted and no camera found
			//----------------------------------------------------------------------------------------------------------

			if (myKbhit() && (i == 0)) {
				outputErr(KSERROR_DEVICE_NOT_FOUND, "KS_enumCameras", "No camera found");
				return false;
			}


			//----------------------------------------------------------------------------------------------------------
			// Aborted or no more cameras enumerated after we found at least one
			//----------------------------------------------------------------------------------------------------------

			if (myKbhit() || ((ksError == KSERROR_DEVICE_NOT_FOUND) && i)) {
				outputTxt("");
				break;
			}


			//----------------------------------------------------------------------------------------------------------
			// Wait some time for the camera(s) to answer, before calling KS_enumCameras again
			//----------------------------------------------------------------------------------------------------------

			if (i == 0)
				waitTime(300 * ms);
		}


		//------------------------------------------------------------------------------------------------------------
		// Let the user select one of the cameras if there are multiple
		//------------------------------------------------------------------------------------------------------------

		int cameraIndex = i > 1 ? inputDec("Selected number: ", "") : 0;

		ksError = KS_enumCameras(
			hAdapter,                               // NIC adapter on which to enumerate
			cameraIndex,                            // Index of camera
			&ksCameraInfo,                          // Pointer to a KSCameraInfo structure
			KSF_NO_DISCOVERY);                      // Flags, don't send a discovery broadcast

		if (ksError != KS_OK) {
			outputErr(ksError, "KS_enumCameras", "Unable to select camera");
			return false;
		}


		//------------------------------------------------------------------------------------------------------------
		// Now we can open a connection to the camera with KS_openCameraEx.
		// The camera is identified by the KSCameraInfo structure provided, namely by its members macAddress and
		// instance. On success, you have control access to the camera. The connection is automatically reestablished,
		// as long as the camera is present with the same IP on the network and KS_closeCamera had not been called.
		//
		// Alternatively, the KS_openCamera function could be used here, which uses only a string of the MAC-address
		// instead of the entire structure.
		//------------------------------------------------------------------------------------------------------------

		outputTxt("Opening camera");
		KSHandle hCamera;

		// MN2: Changing opening camera to use KC_openCamera instead of openCameraEx
		// It isn't clear what the difference between the two functions is when reading the
		// Kithara documentation (http://kithara.com/en/docs/krts:api:ks_opencameraex)
		//

		//ksError = KS_openCameraEx(
		//	&hCamera,                                 // Handle to the camera
		//	hAdapter,                                 // NIC adapter
		//	&ksCameraInfo,                            // Pointer to KSCameraInfo structure
		//	0);

		ksError = KS_openCamera(
			&hCamera,                                 // Handle to the camera
			hAdapter,                                 // NIC adapter
			ksCameraInfo.hardwareId,                            // Pointer to KSCameraInfo structure
			0);

		if (ksError != KS_OK) {
			outputErr(ksError, "KS_openCamera", "Unable to open camera");
			return false;
		}
		pSharedData->hCamera = hCamera;


		//------------------------------------------------------------------------------------------------------------
		// Now we should configure the camera to our needs. Configuration should be done before creating any streams.
		//------------------------------------------------------------------------------------------------------------

		if (!cameraConfig(pSharedData)) {
			outputTxt("Error: Camera configuration failed.");
			cameraRestoreConfig(pSharedData);
			return false;
		}


		//------------------------------------------------------------------------------------------------------------
		// To get notified of images received we have to use a callback
		//------------------------------------------------------------------------------------------------------------

		KSHandle hStreamCallBack;
		ksError = KS_createKernelCallBack(
			&hStreamCallBack,                         // Address of callback handle
			pSharedData->hKernel,                     // Handle to loaded dll
			"_cameraRecv",                            // Name of function to be used
			(void*)pSharedData->pSysPtr,             // Argument for callback, here the system pointer to the shared memory
			KSF_DIRECT_EXEC |                         // Flags, execute on kernel level
			KSF_NO_CONTEXT,                           // Callback function has no context argument
			0);                                       // Priority, not needed here

		if (ksError != KS_OK) {
			outputErr(ksError, "KS_createCallBack", "Unable to create callback function");
			cameraRestoreConfig(pSharedData);
			return false;
		}
		pSharedData->hStreamCallback = hStreamCallBack;


		//------------------------------------------------------------------------------------------------------------
		// Create a stream with KS_createCameraStream to receive images.
		// GigE Vision cameras can support up to 512 stream channels, enumerated starting with zero.
		// Please note, that to process streaming images at least 2 buffers should be allocated so that the second
		// buffer can be filled, while the other is still being processed.
		//------------------------------------------------------------------------------------------------------------

		outputTxt("Creating stream");
		KSHandle hStream;
		ksError = KS_createCameraStream(
			&hStream,                                 // Handle to the stream
			hCamera,                                  // Handle to the camera on which the stream is created
			0,                                        // Stream channel number
			3,                                        // Number of buffers, here two (at least double buffering
			0,                                        // Buffer size, with 0 the buffer size is chosen
			// automatically via the GeniCam 'PayloadSize' feature
			0);                                       // Flags, here none

		if (ksError != KS_OK) {
			outputErr(ksError, "KS_createCameraStream", "Unable to create camera stream");
			cameraRestoreConfig(pSharedData);
			return false;
		}
		pSharedData->hStream = hStream;

		//------------------------------------------------------------------------------------------------------------
		// We need to install the handler for recveiving images.
		//
		// With KS_installCameraHandler you can also install handlers for the KS_CAMERA_IMAGE_DROPPED event
		// (having KSCameraDroppedImageContext) or KS_CAMERA_ERROR event (having KSCameraErrorContext)
		//------------------------------------------------------------------------------------------------------------

		ksError = KS_installCameraHandler(
			hStream,                                  // Handle to the camera stream
			KS_CAMERA_IMAGE_RECEIVED,                 // Type of handler, here for receiving
			hStreamCallBack,                          // Handle to the callback object
			0);                                       // Flags, none

		if (ksError != KS_OK) {
			outputErr(ksError, "KS_installCameraHandler", "Unable to install camera handler");
			cameraRestoreConfig(pSharedData);
			return false;
		}

		//
		char featureName[256];
		char featureName2[256];

		for (int i = 0;; ++i) {
			ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_ENUMERATE, "Root", i, &featureName, 256, 0);
			if (ksError == KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND)
				break;


			for (int j = 0;; ++j) {
				ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_ENUMERATE, featureName, j, &featureName2, 256, 0);
				if (ksError == KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND)
					break;
				char szBuff[1024];
				sprintf(szBuff, "%s %s\n\n", featureName, featureName2);
				OutputDebugString(szBuff);
			}



			// ... print the feature name or do something else  f
		}
		//------------------------------------------------------------------------------------------------------------
		// We're done
		//------------------------------------------------------------------------------------------------------------

		return true;
	}
}

//--------------------------------------------------------------------------------------------------------------
// cameraExit
//
// Frees all resources used by the camera.
//--------------------------------------------------------------------------------------------------------------

bool cameraExit(SharedData* pSharedData) {
	Error ksError;

	//------------------------------------------------------------------------------------------------------------
	// Uninstall the image receive handler by setting an invalid callback handle.
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_installCameraHandler(pSharedData->hStream, KS_CAMERA_IMAGE_RECEIVED, KS_INVALID_HANDLE, 0);


	//------------------------------------------------------------------------------------------------------------
	// Close the camera stream
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_closeCameraStream(pSharedData->hStream, 0);
	if (ksError)
		outputErr(ksError, "KS_closeCameraStream", "Cannot close the stream.");


	//------------------------------------------------------------------------------------------------------------
	// Remove the callback
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_removeCallBack(pSharedData->hStreamCallback);
	if (ksError)
		outputErr(ksError, "KS_removeCallBack", "Cannot remove the callback.");


	//------------------------------------------------------------------------------------------------------------
	// Restore the camera configuration to the prior settings
	//------------------------------------------------------------------------------------------------------------

	if (!cameraRestoreConfig(pSharedData))
		outputTxt("Warning: Restoring of camera configuration failed");


	//------------------------------------------------------------------------------------------------------------
	// Close the camera connection
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_closeCamera(pSharedData->hCamera, 0);
	if (ksError)
		outputErr(ksError, "KS_closeCamera", "Cannot close camera connection.");


	//------------------------------------------------------------------------------------------------------------
	// Close the adapter
	//------------------------------------------------------------------------------------------------------------

	//ksError = KS_closeAdapter(pSharedData->hAdapter);
	//if (ksError)
	//  outputErr(ksError, "KS_closeAdapter", "Cannot close adapter.");


	//------------------------------------------------------------------------------------------------------------
	// We're done
	//------------------------------------------------------------------------------------------------------------

	return true;
}

//--------------------------------------------------------------------------------------------------------------
// cameraConfig
//
// configures the camera via the GenICam features to our needs.
// The access point to the GenICam features is KS_configCamera function, which can enumerate, read and write
// all GenICam features the camera provides in its XML description file.
// Each feature set by this function is read in advance, so the configuration of the camera can be restored.
//
// Remark: In some cases we don't handle the return values of the KS function, since they are in a don't care
// situation. Note, that it is always a good idea to check error codes.
//--------------------------------------------------------------------------------------------------------------

bool cameraConfig(SharedData* pSharedData) {

	Error ksError;
	CameraConfiguration* pConfig = &pSharedData->config;
	KSHandle hCamera = pSharedData->hCamera;

	//------------------------------------------------------------------------------------------------------------
	// First, init the configuration data for restoration
	//------------------------------------------------------------------------------------------------------------
	pConfig->height = -1;
	pConfig->width = -1;
	pConfig->pPixelFormat[0] = '\0';
	pConfig->pTriggerSelector[0] = '\0';
	pConfig->offsetX = -1;
	pConfig->offsetY = -1;
	for (int i = 0; i < CameraConfiguration::triggerCount; ++i) {
		pConfig->ppTriggerMode[i][0] = '\0';
	}
	pConfig->frameRate = -1.0;
	pConfig->frameRateAbs = -1.0;
	pConfig->frameRateEnable = -1;

	int mn2_disable = 0;



	//------------------------------------------------------------------------------------------------------------
	// First we set the 'PixelFormat' of the to use 'Mono8', because our OpenCV part is only written to handle
	// 8-bit gray-scale images. Before the feature is set, it is read for later restoration.
	//------------------------------------------------------------------------------------------------------------

	ksError = KS_configCamera(
		hCamera,                                  // Handle to opended Camera
		KS_CAMERA_CONFIG_GET,                     // Config command (here getting)
		"PixelFormat",                            // Name of GenICam feature to access
		0,                                        // Enumeration index (not needed here)
		&pConfig->pPixelFormat,                   // Pointer to data buffer for input / output
		sizeof(pConfig->pPixelFormat),            // Size of the buffer (needed only on reads)
		0);                                       // Flags, none

	if (ksError) outputErr(ksError, "KS_configCamera", "Unable to get value of 'PixelFormat'");

	ksError = KS_configCamera(
		hCamera,                                  // Handle to opened camera
		KS_CAMERA_CONFIG_SET,                     // Configuration command (here setting)
		"PixelFormat",                            // Name of the GeniCam feature to access
		0,                                        // Enumration index, (not needed here)
		"Mono8",                                  // Pointer to data buffer, here string 'Mono8'
		0,                                        // Length of a buffer (not needed here)
		0);                                       // Flags, none

	if (ksError != KS_OK) {
		outputErr(ksError, "KS_configCamera", "Unable to set 'PixelFormat' feature to 'Mono8'");
		return false;
	}


	//------------------------------------------------------------------------------------------------------------
	// Now we want to limit the image size. We try to crop the image if its larger than 640 x 480 pixels
	// Since only the 'Width' and 'Height' features are mandatory for GigE Vision cameras and even these are not
	// necessarely writable, this may not work.
	//
	// First limit the width.
	//------------------------------------------------------------------------------------------------------------

	int64 width;
	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "Width", 0, &width, 0, 0);
	if (ksError != KS_OK) {
		outputErr(ksError, "Unable to read 'Width' feature!");
		return false;
	}

	pConfig->width = width;

	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "WidthMax", 0, &width, 0, 0);
	if (ksError != KS_OK)
		outputTxt("Unable to read 'WidthMax', fall back to 'Width'.");

	//--------------------------------------------------------
	// Now limit the width to 640 pixels and try to center it
	//--------------------------------------------------------
	if (width > 640) {
		int64 offset = (width - 640) / 2;

		width = 640;
		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "Width", 0, &width, 0, 0);
		if (ksError != KS_OK)
			outputTxt("Warning! Unable to set 'Width' feature.");

		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "OffsetX", 0, &pConfig->offsetX, 0, 0);
		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "OffsetX", 0, &offset, 0, 0);
		if (ksError != KS_OK)
			outputTxt("Unable to set 'OffsetX' feature.");
	}



	//------------------------------------------------------------------------------------------------------------
	// Now the same for the height of the image.
	//------------------------------------------------------------------------------------------------------------

	int64 height;

	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "Height", 0, &height, 0, 0);
	if (ksError != KS_OK) {
		outputErr(ksError, "Unable to read 'Height' feature!");
		return false;
	}

	pConfig->height = height;

	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "HeightMax", 0, &height, 0, 0);
	if (ksError != KS_OK)
		outputTxt("Unable to read 'HeightMax', fall back to 'Height'.");

	//--------------------------------------------------------
	// Now limit the height to 480 pixels and try to center it
	//--------------------------------------------------------
	if (height > 480) {
		int64 offset = (height - 480) / 2;

		height = 480;
		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "Height", 0, &height, 0, 0);
		if (ksError != KS_OK)
			outputTxt("Warning! Unable to set 'Height' feature.");

		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "OffsetY", 0, &pConfig->offsetY, 0, 0);
		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "OffsetY", 0, &offset, 0, 0);
		if (ksError != KS_OK)
			outputTxt("Unable to set 'OffsetY' feature.");
	}


	//------------------------------------------------------------------------------------------------------------
	// Here we disable all triggers of the camera, since we want the camera to run in a free run mode.
	// If this doesn't reflect the abilities of your camera, you must adjust the code and consider triggering the
	// camera via a timer or other means.
	// Only the first 16 triggers are disabled!
	// Since not all triggers are always available, we do not react on errors here!
	//------------------------------------------------------------------------------------------------------------

	KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "TriggerSelector", 0, &pConfig->pTriggerSelector,
		sizeof(pConfig->pTriggerSelector), 0);

	for (int i = 0; i < CameraConfiguration::triggerCount; ++i) {
		char pTrigger[64];
		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_ENUMERATE, "TriggerSelector", i, pTrigger,
			sizeof(pTrigger), 0);
		if (ksError == KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND)
			break;

		if (ksError != KS_OK)
			continue;

		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "TriggerSelector", 0, pTrigger, 0, 0);
		if (ksError != KS_OK)
			break;

		KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "TriggerMode", 0, pConfig->ppTriggerMode[i],
			sizeof(pConfig->ppTriggerMode[i]), 0);

		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "TriggerMode", 0, "Off", 0, 0);
	}

	//------------------------------------------------------------------------------------------------------------
	// MN2 - Trying to set the ROI
	//------------------------------------------------------------------------------------------------------------

	int64 sensorWidth = 100;
	int64 sensorHeight = 100;
	double exposureTime = 100.0;
	bool exposureAuto = false;

	ksError = KS_configCamera(hCamera,
		KS_CAMERA_CONFIG_SET,
		"Width",
		0,
		&sensorWidth,
		0,
		0);

	if (ksError != KS_OK){
		outputTxt("ROI Width NOT SET!");
	}
	ksError = KS_configCamera(hCamera,
		KS_CAMERA_CONFIG_SET,
		"Height",
		0,
		&sensorHeight,
		0,
		0);

	if (ksError != KS_OK){
		outputTxt("ROI Height NOT SET!");
	}

	//setting ExposureTime

	ksError = KS_configCamera(hCamera,
		KS_CAMERA_CONFIG_SET,
		"ExposureAuto",
		0,
		"Off",
		0,
		0);

	if (ksError != KS_OK){
		outputTxt("Auto Exposure not turned off");
	}

	double floatValue;
	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "ExposureTime", 0, &floatValue, 0, 0);
	printf("%f\n", floatValue);

	int type = 0;
	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_TYPE, "ExposureTime", 0, &type, 0, 0);
	printf("type = %i\n", type);
	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_TYPE, "Width", 0, &type, 0, 0);
	printf("type = %i\n", type);

	ksError = KS_configCamera(hCamera,
		KS_CAMERA_CONFIG_SET,
		"ExposureTime",
		0,
		&exposureTime,
		0,
		0);

	if (ksError != KS_OK){
		outputTxt("Exposure Time not set");
	}

	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "ExposureTime", 0, &floatValue, 0, 0);
	printf("%f\n", floatValue);


	//------------------------------------------------------------------------------------------------------------
	// This is an adjustment for the 'Basler ace640'-camera. It has a non-standard feature
	// 'AcquisitionFrameRateEnable' which must be set true to enable the frame rate ability we want to use.
	//------------------------------------------------------------------------------------------------------------

	int enable = 1;
	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "AcquisitionFrameRateEnable", 0,
		&pConfig->frameRateEnable, 0, 0);
	if (ksError == KS_OK) {
		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "AcquisitionFrameRateEnable", 0, &enable, 0, 0);
		if (ksError != KS_OK)
			outputTxt("Warning! Unable to set 'AcquisitionFrameRateEnable'.");
	}


	//------------------------------------------------------------------------------------------------------------
	// Limit the framerate to 25 frames per second.
	// The current standard feature for the frame rate is 'AcquisitionFrameRate', but there's is also a now
	// deprecated standard feature 'AcquisitionFrameRateAbs' which is checked if the former is not available.
	//------------------------------------------------------------------------------------------------------------

	double framerate = 1000.0;

	char sOutput[100];

	sprintf(sOutput, "Setting the frame rate to %f fps. Otherwise change the example", framerate);
	outputTxt(sOutput);

	ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "AcquisitionFrameRate", 0, &pConfig->frameRate, 0, 0);
	if (ksError != KS_OK) {
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_GET, "AcquisitionFrameRateAbs", 0, &pConfig->frameRateAbs, 0, 0);

		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "AcquisitionFrameRateAbs", 0, &framerate, 0, 0);
		if (ksError != KS_OK)
			outputTxt("Warning! Unable to set 'AcquisitionFrameRate(Abs)'");

	}
	else {

		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "AcquisitionFrameRate", 0, &framerate, 0, 0);
		if (ksError != KS_OK) {
			outputTxt("Warning! Unable to set 'AcquisitionFrameRate'.");
		}

	}


	//------------------------------------------------------------------------------------------------------------
	// We're done
	//------------------------------------------------------------------------------------------------------------

	return true;
}


//--------------------------------------------------------------------------------------------------------------
// cameraRestoreConfig
//
// tries to restore all the features that have been modified.
//--------------------------------------------------------------------------------------------------------------

bool cameraRestoreConfig(SharedData* pSharedData) {
	CameraConfiguration* pConfig = &pSharedData->config;
	KSHandle hCamera = pSharedData->hCamera;
	Error ksError;

	if (pConfig->frameRateAbs != -1.0)
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "AcquisitionFrameRateAbs", 0, &pConfig->frameRateAbs, 0, 0);

	if (pConfig->frameRate != -1.0)
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "AcquisitionFrameRate", 0, &pConfig->frameRate, 0, 0);

	if (pConfig->frameRateEnable != -1)
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "AcquisitionFrameRateEnable", 0,
		&pConfig->frameRateEnable, 0, 0);

	for (int i = 0; i < 16; ++i) {
		char pTrigger[64];
		ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_ENUMERATE, "TriggerSelector", i, pTrigger,
			sizeof(pTrigger), 0);
		if (ksError == KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND)
			break;

		if (ksError != KS_OK)
			continue;

		if (pConfig->ppTriggerMode[i][0] != '\0') {
			ksError = KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "TriggerSelector", 0, pTrigger, 0, 0);
			if (ksError == KS_OK)
				KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "TriggerMode", 0, pConfig->ppTriggerMode[i], 0, 0);
		}
	}

	if (pConfig->pTriggerSelector[0] != '\0')
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "TriggerSelector", 0, pConfig->pTriggerSelector, 0, 0);

	if (pConfig->offsetY != -1)
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "OffsetY", 0, &pConfig->offsetY, 0, 0);

	if (pConfig->height != -1)
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "Height", 0, &pConfig->height, 0, 0);

	if (pConfig->offsetX != -1)
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "OffsetX", 0, &pConfig->offsetX, 0, 0);

	if (pConfig->width != -1)
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "Width", 0, &pConfig->width, 0, 0);

	if (pConfig->pPixelFormat[0] != '\0')
		KS_configCamera(hCamera, KS_CAMERA_CONFIG_SET, "PixelFormat", 0, pConfig->pPixelFormat, 0, 0);

	return true;
}


