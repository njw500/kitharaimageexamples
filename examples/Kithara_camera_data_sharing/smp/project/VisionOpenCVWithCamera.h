﻿/* Copyright (c) 2015 by Kithara Software. All rights reserved. */

//##############################################################################################################
//
// Module:           VisionOpenCVWithCamera.h
//
// Descript.:        Sample application for using OpenCV library in real-time tasks with images from a camera.
//                   This is the header file.
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// c.was 2015-02-04  CREAT: Original - (file created)
//
//##############################################################################################################

   /*=====================================================================*\
   |                    *** DISCLAIMER OF WARRANTY ***                     |
   |                                                                       |
   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
   \*=====================================================================*/

//##############################################################################################################
//
// Purpose:
//
// See VisionOpenCVWithCamera.cpp!
//
//##############################################################################################################


#include "..\_KitharaSmp\_KitharaSmp.h"

#pragma pack(push)
#pragma pack(8)

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/highgui.hpp"
#include  <cstdint>

#pragma pack(pop)


typedef struct {
  enum { triggerCount = 16 };
  int64 width;
  int64 height;
  int64 offsetX;
  int64 offsetY;
  char pPixelFormat[16];
  char pTriggerSelector[64];
  char ppTriggerMode[triggerCount][4];
  double frameRate;
  double frameRateAbs;
  int frameRateEnable;
} CameraConfiguration;

struct SharedData {
  SharedData* pAppPtr;
  SharedData* pSysPtr;

  KSHandle hKernel;
  KSHandle hEvent;

  KSHandle hMemoryLock;
  
  KSHandle hAdapter;
  KSHandle hCamera;
  KSHandle hStream;
  KSHandle hStreamCallback;
  KSHandle hOpenCvCallback;
  KSHandle hOpenCvTask;

  KSHandle hTaskEvent;                                  // Event the OpenCV task waits on

  bool dontcopy;
  bool dontread;

  bool ready;
  bool abort;
  int number;

  uint8_t a;
  uint8_t b;
  uint8_t c;
  uint8_t d;
  uint8_t e;

  int width2;
  int height2;
  int step2;

  byte image2[64000];

  Error error;
  CameraConfiguration config;

  // storing image

  enum DisplayMode { INVALID = 0, SERIALIZE_MEMORY, RING_0_DISPLAY, MAX_VALUE };
  enum Action      { CONTINUE, TERMINATE, DATA };
  enum ItemType    { BUFFER_TOO_SMALL, IMAGE, CONTOURS };

  DisplayMode displayMode;                              // Choosen display method
  Action      action;                                   // Command to the OpenCV task or user mode thread

  union {                                               // Union interpretation depends on displayMode field
	  struct {                                            // Interpretation for SERIALIZE_MEMORY
		  void* pAppPtr;                                    // Shared memory user space pointer
		  void* pSysPtr;                                    // Shared memory kernel space pointer
		  int size;                                         // Size of the shared memory

		  ItemType itemType;                                // Type of the item copied ot shared memory
		  int itemSize;                                     // Size of the item copied to shared memory
	  } memory;
  };

};


//--------------------------------------------------------------------------------------------------------------
// Memory layout for the serialization of an image (a 2-D cv::Mat)
//--------------------------------------------------------------------------------------------------------------

struct SerializedImage {
	int type;                                             // "Pixel type"
	int step;                                             // Byte count between two rows
	int rows;                                             // Number of rows
	int colums;                                           // Number of colums
	byte pData[1];                                        // Image data, row by row
};
