Copyright (c) 2015 by Kithara Software GmbH. All rights reserved.

##############################################################################

Product:           Kithara Realtime Suite

Example:           VisionOpenCVWithCamera

Test log:	   2015-02-27  CPP version

##############################################################################

  /*=====================================================================*\
  |                    *** DISCLAIMER OF WARRANTY ***                     |
  |                                                                       |
  |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
  |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
  |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
  |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |                         
  \*=====================================================================*/

Purpose:

This example demonstrates the execution of a OpenCV 3.0 application on 
real-time level in conjunction with acquisition of images from a GigE Vision 
camera, which are processed with OpenCV.

It will load a DLL on kernel-level, which contains the OpenCV code and code
for handling image reception.

IMPORTANT NOTICE:
This sample requires the OpenCV 3.0 libraries built as static libs. These li-
braries must have been built using the statically linked runtime library (CRT).
The prebuild version is usable for this sample, but we recommend to build the 
OpenCV libraries with your compiler. Please see our OpenCV guidelines at 
http://www.kithara.de/en/docs/krts:tutorial:vision_opencv for further
information.

IMPORTANT NOTICE:
For this sample to find the OpenCV 3.0 libraries and include files it assumes 
the environment variable OPENCV_DIR to exist. OPENCV_DIR must contain the 
installation directory of OpenCV 3.0. The installation directory is the one 
that contains the directory "include" (containing all OpenCV API include
files) and the binary directories "x64" and/or "x86" (containing the 
staticlibs). Without this environment variable this sample won't build!

Examples:

When using the prebuild libraries, which have been copied to C:\opencv use:
OPENCV_DIR = C:\opencv\build

When having a custom build made in the folder C:\opencv\build, with additional 
copying of install files to the default folder (build target INSTALL) use:
OPENCV_DIR = C:\opencv\build\install
