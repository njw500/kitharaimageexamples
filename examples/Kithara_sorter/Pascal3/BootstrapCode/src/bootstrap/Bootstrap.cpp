#include "stdafx.h"

#include "..\..\..\Utilities\Utilities\UDPSend.h"


#include <conio.h>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#ifdef USE_PLOTTER
#include <opencv2/plot.hpp>
#endif

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;

#include "BootstrapSysParams.h"
#include "DebugFlags.h"  
#include "..\tracking\Tracking2D.h"

#include "..\..\..\Utilities\Utilities\BaslerCamera.h"
#include "..\..\..\Utilities\Utilities\DisplayIm.h"
#include "..\..\..\Utilities\Utilities\Histogram.h"


int main(int argc, char* argv[])
{
	// The exit code of the sample application.
	int exitCode = 0;
	int UseCamera = 1;	// Cant actually be turned off at the moment
	int FrameCount = 0;

	bool CamOK = false;// Status flag for camera
	bool CamOK2 = false;// 2nd camera

	bool UseFile = false; // Enable/disable toggle for file processing
	bool FileLoaded = false; // Status flag to know of file has been read in
	bool SetUpMode = true;

	cUDP UDP;
	UDP.Init(27127,false);// Pick a different port for Bootstrap

#ifdef EMULATEFROMFILE
	UseFile = true;
	SetUpMode = false;// Dont launch into the GUI display mode
#endif

	Tracker2D tracker2D;
	BootstrapSysParams Params;
	
	Params.cParams.CameraSerial = "";

	cv::Mat avg;	// Average

	try
	{
		LARGE_INTEGER Frequency, start, end;

		char string[80];

		int NumFrames = Params.NumberOfImagesToGrab;  // use to set the number of frames from file, or in a burst

		QueryPerformanceFrequency(&Frequency);
		cout << "QueryPerformanceFrequency -> " << _i64toa(Frequency.QuadPart, &string[0], 10) << endl;


		BaslerCamera  camera;// Primary Bootstrap Camera
		BaslerCamera  Sortercamera;

#ifdef ENABLECAMERA
		Params.cParams.AutoStart = true;
		Params.cParams.CameraSerial = "21744985";// Debug
		CamOK = camera.Init(Params.cParams.CameraSerial, Params.cParams); // Get first camera available

		// DEBUG
		if (!CamOK)
		{
			cout << "Cannot connect to camera " << endl;
		}
		else
		{
			camera.SetFrameRate(-1);// Max
			camera.SetGain(8);
			double FrameRate = camera.GetRealFrameRate();

			// Try the 2nd camera
			Params.cParams.CameraSerial = "21814112";// Debug
			CamOK2 = Sortercamera.Init(Params.cParams.CameraSerial, Params.cParams); // Get first camera available
			Sortercamera.SetFrameRate(-1);// Max

			if (!CamOK2)
			{
				cout << "Cannot connect to 2nd camera " << endl;
			}
		}



#else
		CamOK = false;
#endif

		// Declare an integer variable to count the number of grabbed images

		int grabbedImages = 0;

		vector<float> transitTimeList;
		vector<double> transitTimeListD;
		vector<double > timeList;

#ifdef EMULATEFROMFILE

		VideoCapture vidCap;
		int NumFramesInFile;
		Mat Frames[Params.NumberOfImagesToGrab];

		// Create an OpenCV image.
		Mat openCvImage;
		char filename[200];
		// Open stored video file
		sprintf(filename, "%s", Params.VideoFileName.c_str());

		// Open the input avi file
		vidCap.open(filename);
		NumFramesInFile = vidCap.get(CAP_PROP_FRAME_COUNT);

		// Allocate and read in the frames
		// pre-load to avoid this read giving misleading load times

		NumFrames = Params.NumberOfImagesToGrab;

		if (NumFramesInFile < NumFrames)
		{
			cout << "Too few frames" << endl;
			NumFrames = NumFramesInFile;
		}

		cout << "Reading frames" << endl;

		for (int i = 0; i < NumFrames; ++i)
		{
			vidCap.read(Frames[i]);  // Fake a frame from the stored file
			if (Frames[i].empty())
			{
				cerr << "ERROR! blank frame grabbed\n";
				break;
			}
		}

		cout << "Frames read into memory" << endl;
		FileLoaded = true;

#endif // Read in from file

		// Other openVCImages
		Mat filtered_frame, mask, frame;

#ifdef recordVideo

		// Create an OpenCV video creator.
		VideoWriter cvVideoCreator;

		// Define the video file name.
		std::string videoFileName = "c:\\temp\\openCvVideo.avi";
		// Define the video frame size.
		cv::Size CamFrameSize = Size(Params.BootstrapFrame_ROI_Width, Params.BootstrapFrame_ROI_Height + 100);
		// Set the codec type and the frame rate. 
		cvVideoCreator.open(videoFileName, CV_FOURCC('D', 'I', 'V', 'X'), 20, CamFrameSize, true);
		//cvVideoCreator.open(videoFileName, CV_FOURCC('M','P','4','2'), 20, frameSize, true); 
#endif

		QueryPerformanceCounter(&start);

		// Make a rotation matrix to align the current generate video
		cv::Point2f pc(camera.GetCurWidth() / 2., camera.GetCurHeight() / 2.);

		cv::Mat r = cv::getRotationMatrix2D(pc, Params.FrameRotate, 1.0);// Build Rotation matrix for specified rotation

		/// Create a Trackbar for user to enter threshold
		//createTrackbar("Min Threshold:", window_name, &lowThreshold, max_lowThreshold, CannyThreshold);

		// Start main grab and process loop
		bool StopLoop = false;

		// Just to be sure
#ifndef ENABLECAMERA
		CamOK = false;
#endif
		bool GotValidFrame = false;

		if (!UseFile &&  !CamOK)
		{
			cerr << "NO CAMERA OR DATA FILE" << endl << "giving up " << endl;
			cerr << endl << "Press Enter to exit." << endl;
			while (cin.get() != '\n');
			return(-1);
		}



		std::vector<cv::Mat> cFrames(Params.NumGrab);
		std::vector<float> DeltaT;

		Rect OutRect;

		int BurstFrameIndex;
		bool BurstComplete = false;
		bool NewBurst = true;
		SetUpMode = false;

		std::vector<cv::Rect> roiList;


		// ED.:  THIS NEEDS FIXING TO LIMIT TO NOT BE LARGER THAN THE FRAME SIZE
		// Which could be cam frame, stored frame(avi) or cropped-stored frame
		roiList.push_back(Params.BS_roiVelRegion1);
		roiList.push_back(Params.BS_roiVelRegion2);

		Mat avg;


		// Main capture, process (display) loop
		while (!StopLoop)
		{
			// Wait for an image and then retrieve it. 
			GotValidFrame = false;// Set to false for safety

			// Launch into display mode first
			if (SetUpMode && CamOK)
			{
				camera.DisplayLoop(OutRect);// Loop displaying frame and allowing the ROI to be adjusted
				SetUpMode = false;
			}

			// Bootstrap works in a burst and process mode
			if ((CamOK) && (NewBurst))
			{
				camera.Burst(cFrames, DeltaT, Params.NumGrab);
				NumFrames = DeltaT.size();

				BurstFrameIndex = 0;
				if (NumFrames > 10)
				{
					BurstComplete = true;
					NewBurst = false;
					tracker2D.initialised = false;
				}
				else
					cout << "Failed to grab" << endl;	

			}
			else
			{
				// Otherwise the camera is just disconnected and we are in video mode
			}

			Mat getframe;
#ifdef EMULATEFROMFILE
			// SWap in the sorted video frame

			if (UseFile)
			{
				// Swap from stored video frames
				getframe = Frames[FrameCount];
				getframe.copyTo(frame);
				if ((frame.rows > 10) && (frame.cols > 10))
					GotValidFrame = true;
				else
					GotValidFrame = false;
			}
#else
			// Not emulating but actually using video data
			getframe = cFrames[FrameCount];
			getframe.copyTo(frame);
			displayIm("Copied frame", frame);
			GotValidFrame = true;
#endif
			// Process loop
			if (GotValidFrame)
			{
				// We have a frame worth processing


				//Extract square box from frame
				if (Params.DoFrameRotate) // Avoid this affine rotate if possible
					cv::warpAffine(frame, frame, r, frame.size());

				cv::Mat cframe;
				Mat ffcrop;
				Mat frameDisp;
				Mat phaseFrame;
				cv::Mat background;

#ifdef EMULATEFROMFILE

				if (!Params.VideoIsPreCropped)
					cframe = frame(Rect(Params.BootstrapFrame_ROI_Left, Params.BootstrapFrame_ROI_Top,
					Params.BootstrapFrame_ROI_Width, Params.BootstrapFrame_ROI_Height));
				else  // DONT crop as the video is already the active ROI
					cframe = frame;
#else
				cframe = frame;// copy
#endif
				if (Params.DisplayMode[WCropped])
					displayIm("CROPPED FRAME", cframe);

				// Initialize Average
				if (FrameCount == 0)
				{
					avg = Mat(cframe.rows, cframe.cols, CV_32F);
					cframe.convertTo(avg, CV_32F);
				}
				else
				{
					cframe.convertTo(ffcrop, CV_32F);// Promote current crop to float
					accumulateWeighted(ffcrop, avg, Params.AverageDecay);
					
				}
				
				convertScaleAbs(avg, background);

				cv::Mat Subtracted;
				// Calculate this again
				subtract(background, cframe, Subtracted);

				// TEST. Flip subtracted into image
				// TEST TEST TEST
				cframe = Subtracted;


				//Mat trackerFrame;
				// Keep tracker frame colour
				//trackerFrame = cframe.clone();
#ifdef EMULATEFROMFILE
				cvtColor(cframe, cframe, COLOR_BGR2GRAY);
#endif
				cframe.convertTo(cframe, CV_32F);// Promote current crop to float
				frameDisp = cframe.clone();
				phaseFrame = cframe.clone();
				displayIm("trackerFrame2", phaseFrame);

				cHistogram timeHist;

				// Create the regions of interest.

				// Initialise the tracker if required
				if (tracker2D.initialised == false)
				{
					// Initialise the tracker
					std::vector<double>velList;
					tracker2D.init(roiList, phaseFrame);
				}
				// If already initialised, update the tracker and (optionally) plot
				else
				{
					tracker2D.update(phaseFrame);

					if (Params.DisplayMode[WTrackerFilter])
					{
						
						Mat trackReg = frameDisp.clone();
						//tracker2D.updatePlotTrackRegion(&trackReg, Params.Bs_StartRow, Params.Bs_EndRow, 20);
						tracker2D.updatePlot(&frameDisp, Params.Bs_StartRow, Params.Bs_EndRow);
						//displayIm("Tracked Region", trackReg);
						waitKey(1);
						
					}

					if (Params.DisplayMode[WHist])
					{

						if (tracker2D.medianVelocityList.size() > 0)
						{
							transitTimeList.push_back(tracker2D.calculateFramesBetweenPoints(Params.Bs_StartRow, Params.Bs_EndRow));

							//transitTimeListD.push_back(tracker2D.calculateFramesBetweenPoints(Params.Bs_StartRow, Params.Bs_EndRow)); NJW WAS THIS DEBUG 

							timeList.push_back((float)FrameCount);

							float rmx, rmn, rstd;

							timeHist.GetHistogram(transitTimeList, transitTimeList.size(), rmx, rmn, rstd, cv::String("Transit Time Histogram"));
							stringstream str1;
							str1 << rstd;
							timeHist.Annotate(str1.str(), "", "");
							timeHist.Show();

#ifdef USE_PLOTTER
							Ptr<plot::Plot2d> newPlot, newPlot2;
							newPlot = plot::createPlot2d(timeList, transitTimeListD);
							newPlot->setMaxY(150);
							newPlot->setMinY(0);

							Mat plot_result;
							newPlot->render(plot_result);

							imshow("Transit Time vs Time", plot_result);
#endif		
						}
					}

				}

				if (Params.DisplayMode[WTrackerFilter])
					displayIm("Tracked Frame", frameDisp);
				waitKey(1);


				// Saving output to files for inspection

				/*	std::ostringstream s1;
					// Create image name files with ascending grabbed image numbers.
					s1 << "imageNew_" << FrameCount << ".jpg";
					std::string imageName(s1.str());
					// Save an OpenCV image.
					imwrite(imageName, frameDisp);
					*/


				// Set recordVideo to '1' to record AVI video file.
#ifdef recordVideo

				cvVideoCreator.write(frameDisp);
				grabbedImages++;
				//cv::accumulate(openCvImage, avgImg);

#endif
			}

			FrameCount = FrameCount + 1;
			cout << "FC: " << FrameCount << endl;
			if (FrameCount > NumFrames - 2)
			{
				FrameCount = 2;// avoid the special conditions
				cout << "Rewind" << endl;
				NewBurst = true;
				tracker2D.reset();
				// NJW FIX AreaCount = 0;
				//getDropletSize.WriteVectors();
				//getDropletSize.ResetVectors();

				//StopLoop = true;
			}

#ifdef PYLON_WIN_BUILD
			// Display the grabbed image in pylon.
			///Pylon::DisplayImage(1, ptrGrabResult);
#endif

			if (_kbhit())
			{
				cout << "ok" << endl;
				int key = _getch();

				switch (key)
				{
				case 'q':
					StopLoop = true;
					break;

				case ';':
					tracker2D.ShiftRoi(-1, 1, -1);
					break;

				case ',':
					tracker2D.ShiftRoi(-1, 1, -1);
					break;

				case '.':

					tracker2D.ShiftRoi(-1, 1, 1);
					break;
					/*
				if (key == '<')
				{
				//P//arams.CannyThres1 = Params.CannyThres1 - 1;
				//cout << endl << "Changed Edge1:  " << fixed << Params.CannyThres1 << endl << endl;
				}
				if (key == '>')
				{
				//Params.CannyThres1 = Params.CannyThres1 + 1;
				//cout << endl << "Changed Edge1:  " << fixed << Params.CannyThres1 << endl;
				}*/
				case '[':
				{
					float gain;
					uint16_t Exp, Rate;
					camera.GetGainExpRate(gain, Exp, Rate);

					cout << "Gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;
					gain = gain + 0.5f;
					camera.SetGain(gain);
				}
				break;

				case ']':
				{
					float gain;
					uint16_t Exp, Rate;
					camera.GetGainExpRate(gain, Exp, Rate);
					cout << "Gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;

					gain = gain - 0.5f;
					if (gain < 0.0)
						gain = 0.0;
					camera.SetGain(gain);
				}
				break;




				case 'a':
				{
					bool flag = true;
					if (Params.DisplayMode[WGrab])
						flag = false;
					Params.DisplayMode[WGrab] = flag; // Display this window   WOutput
					Params.DisplayMode[WHough] = flag;
					Params.DisplayMode[WBack] = flag;
					Params.DisplayMode[WCropped] = flag;
					Params.DisplayMode[WEdges] = flag;
					Params.DisplayMode[WHist] = true;
					Params.DisplayMode[WOutput] = flag;
					Params.DisplayMode[WDisplayEd] = flag;
				}
				break;

				case 'j':
				{

					camera.RoiVert(1, -1);
					Rect Cur = camera.GetROI();
					cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
				}
				break;
				case 'k':
				{
					camera.RoiVert(1, +1);
					Rect Cur = camera.GetROI();
					cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
				}
				break;
				case 'o':
				{
					Params.Bs_EndRow = Params.Bs_EndRow - 8;
					Params.BS_roiVelRegion1 = cv::Rect(Params.Bs_EndRow + 470 + 170, 1, 492, 20);
					Params.BS_roiVelRegion2 = cv::Rect(Params.Bs_EndRow, 1, 470, 20);
					std::vector<cv::Rect> roiList;
					roiList.push_back(Params.BS_roiVelRegion1);
					roiList.push_back(Params.BS_roiVelRegion2);
					//roiList.push_back(Params.BS_roiVelRegion3);
					//roiList.push_back(Params.BS_roiVelRegion4);
					// Initialise the tracker if required
					// Initialise the tracker
					tracker2D.updateRoi(roiList);
				}
				break;


				case 'p':
				{
					Params.Bs_EndRow = Params.Bs_EndRow + 8;
					Params.BS_roiVelRegion1 = cv::Rect(Params.Bs_EndRow + 470 + 170, 1, 492, 20);
					Params.BS_roiVelRegion2 = cv::Rect(Params.Bs_EndRow, 1, 470, 20);
					std::vector<cv::Rect> roiList;
					roiList.push_back(Params.BS_roiVelRegion1);
					roiList.push_back(Params.BS_roiVelRegion2);
					//roiList.push_back(Params.BS_roiVelRegion3);
					//roiList.push_back(Params.BS_roiVelRegion4);
					// Initialise the tracker if required
					// Initialise the tracker
					tracker2D.updateRoi(roiList);
				}
				break;

				case 'v':
				{
					if (UseFile)
						UseFile = false;
					else
						UseFile = true;
					FrameCount = 0;
					//					getDropletSize.ResetVectors();
				}
				break;
				}
			}// kbhit
		}
		QueryPerformanceCounter(&end);

		{
			// Display the elapsed time
			double freq, diff;
			diff = (double)(end.QuadPart - start.QuadPart);
			freq = (double)Frequency.QuadPart;
			diff = diff / freq;
			cout << "end - start -> " << _i64toa(end.QuadPart - start.QuadPart, &string[0], 10) << endl;

			printf("%d frames in %g sec, rate %g\n", grabbedImages, diff, ((double)Params.NumberOfImagesToGrab) / diff);

			cout << "Num frames" << _i64toa_s(grabbedImages, &string[0], 30, 10) << endl;
		}

		// Release the video file on leaving.
#ifdef recordVideo

		cvVideoCreator.release();
#endif
	}
	catch (GenICam::GenericException &e)
	{
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
		exitCode = 1;
		cerr << endl << "Press Enter to exit." << endl;
		while (cin.get() != '\n');
	}

	// Comment the following two lines to disable waiting on exit.
	cerr << endl << "Press Enter to exit." << endl;
	while (cin.get() != '\n');

	return exitCode;
}
