// Detect Drop
// Still straight C
// Droplet detection class

#include "stdafx.h"
#include "io.h"
#include <algorithm>
// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include <iostream>

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;


bool DetectDrop(cv::Mat Frame, int roi_y1, int roi_y2, double min_pk_height, double min_pk_dist, int *DropLocation)
{
	/*  Detect if a drop is present on the input frame by measuring the peaks
	along the X axis of the frame.
	Returns True if drop detected and the X coordinate positions of the drop edges as(X1, x2). :PY */

	//Find droplets by looking for peaks across line in image.
	int opencv_peaks(cv::Mat peak_array, cv::Mat &PeakLoc, cv::Mat &PeakHeight, double minimum_peak_height, double minimum_peak_distance);

	// Apply ROI
	int width = Frame.cols;
	int height = Frame.rows;

	width = width - 10;

	//Define the region of interest of the channel by cropping the frame.
	cv::Rect ROI(1, roi_y1, width, roi_y2 - roi_y1);
	cv::Mat peak_array = Frame(ROI);
#ifdef FDBG
	imwrite("PeakArray.jpg", peak_array);
#endif
	//imshow("Sub ROI", peak_array);
	//waitKey(1);

	// integrate this to a single line profile

	reduce(peak_array, peak_array, 0, REDUCE_AVG, -1);

	// Gaussian Blur, which needs the data to be float32
	
	peak_array.convertTo(peak_array, CV_32F);

#ifdef FDBG
	WriteVector(peak_array, "PreBlur.csv");
#endif

	const int GaussianBlurSize = 11;
	GaussianBlur(peak_array, peak_array, Size(GaussianBlurSize, GaussianBlurSize), 0, 0);

#ifdef FDB
	WriteVector(peak_array, "PostBlur.csv");
#endif
	cv::Mat Peak_loc;
	cv::Mat Peak_height;

	// Set default, NULL locations for the Droplet
	DropLocation[0] = 0;
	DropLocation[1] = 0;

	int NumPeaks = opencv_peaks(peak_array, Peak_loc, Peak_height, min_pk_height, min_pk_dist);

	int i = 0;
	bool DropDetected = false;

	if (NumPeaks > 1)
	{
		// use the iffy code to check peak intensities (this works on the the demo video file)
		while (!DropDetected & (i < NumPeaks - 1))
		{
			if (Peak_height.at<float>(i) >Peak_height.at<float>(i + 1))
			{
				DropDetected = true;
				DropLocation[0] = Peak_loc.at<unsigned short int>(i);
				DropLocation[1] = Peak_loc.at<unsigned short int>(i + 1);
			}
			++i;
		}
	}
	if (DropDetected && (DropLocation[1] < DropLocation[0]))
	{
		cout << "Backwards" << endl;
	}
	return(DropDetected);
}





int opencv_peaks(cv::Mat peak_array, cv::Mat &PeakLoc, cv::Mat &PeakHeight, double minimum_peak_height, double minimum_peak_distance)
{
	// Detect peaks on a 1D array using OpenCV calls to calculate derivatives.
	// Return the index of the peaks on the input array.
	// calculate 1st and 2nd deriviatives
	// zeroth derivative, grad0
	// Laplacian grad2
#ifdef FDBG
	WriteVector(peak_array, "PeakArray.csv");
#endif
	// 2nd derivative, grad2
	cv::Mat grad0, grad2;

	// threshold zeroth derivative above minimum peak height
	double thresh;
	thresh = threshold(peak_array, grad0, minimum_peak_height, 1, THRESH_BINARY);
#ifdef FDBG
	WriteVector(grad0, "Thres1.csv");
#endif
	// 2nd derivative 
	Laplacian(peak_array, grad2, CV_32F, 1);
	cv::InputArray NullArray = cv::noArray();
#ifdef FDBG
	WriteVector(grad2, "Laplacian.csv");
#endif
	double second_derivative_threshold = -0.14;

	// grad2 is the threshold of the computed 2nd derivative for the values BELOW the threshold
	thresh = threshold(grad2, grad2, second_derivative_threshold, 1, THRESH_BINARY_INV);
#ifdef FDBG
	WriteVector(grad2, "Thres2.csv");
#endif
	Mat res;

	//# Find the locations where all the conditions specified are met.
	int Type0 = grad0.type();
	int Type1 = grad2.type();
	grad2.convertTo(grad2, Type0);
	bitwise_and(grad0, grad2, res, NullArray);

	//Get the index locations of the peaks.
	//peak_locations = np.where(peaks == True)
	//# Convert to a 1D vector.
	//peak_locations = peak_locations[0]
#ifdef DBG
	WriteVector(res, "BitWiseAnd.csv");
#endif
	Mat list(1, res.cols, CV_16U);
	int count = 0;
	// Find where the data is non-Zero
	// Skip 15 points at the start and end
	for (int i = 15; i < res.cols - 15; ++i)
	{
		if (res.at<float>(i) > 0)
		{
			list.at<unsigned short int>(count) = i;
			count = count + 1;
		}
	}

	// Reduce size of list to just the filled values
	cv::Mat RedList = list(Rect(0, 0, count, 1));
#ifdef FDBG
	WriteVector(RedList, "RPointList.csv");
#endif
	const int BufLen = 100;
	//int CurrentIndex[BufLen];
	//int CurrentValue[BufLen];

	int TruePeakLocations[BufLen];
	float TruePeakHeight[BufLen];
	int TrueCount = 0;
	int mx_pt;
	float mx_val = -999.0;
	float val;
	int pt, last_pt;

	//memset(&CurrentValue, 0, sizeof(int) * BufLen);// Zero the array

	int CurC = 0;
	bool FoundSeq = false;
	for (int loc = 1; loc < count; ++loc)
	{
		pt = RedList.at<unsigned short int>(loc);
		last_pt = RedList.at<unsigned short int>(loc - 1);
		if (pt - 1 == last_pt)
		{
			// Consecutive points
			//CurrentIndex[CurC] = pt;
			val = peak_array.at<float>(pt);
			//CurrentValue[CurC] = val;
			if (val > mx_val)
			{
				mx_val = val;
				mx_pt = pt;
				FoundSeq = true;
			}
			//++CurC;
		}
		else
		{
			if (FoundSeq)
			{
				// We are about to skip to the new block, so process this one
				TruePeakLocations[TrueCount] = mx_pt;
				TruePeakHeight[TrueCount] = mx_val;
				++TrueCount;
			}
			mx_val = -999.0;
			mx_pt = 0;
			FoundSeq = false;


		}

		if (FoundSeq && (loc == count - 1))
		{
			//Process end block, or actually just dont bother
			TruePeakLocations[TrueCount] = mx_pt;
			TruePeakHeight[TrueCount] = mx_val;
			++TrueCount;

			mx_val = -999.0;
			mx_pt = 0;
			FoundSeq = false;
		}

	}


	Mat PL(1, TrueCount, CV_16U);
	Mat PH(1, TrueCount, CV_32F);
	for (int i = 0; i < TrueCount; ++i)
	{
		PL.at<unsigned short int>(i) = TruePeakLocations[i];
		PH.at<float>(i) = TruePeakHeight[i];
	}

	PeakLoc = PL;
	PeakHeight = PH;
	return(PL.cols);
}



