#ifndef DROPDETECTION_H
#define DROPDETECTION_H


#include "DropDetection.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>
#include "io.h"
using namespace std;



void Writevector(cv::Mat res, 
	char * filename);

int opencv_peaks(cv::Mat peak_array, 
	cv::Mat &PeakLoc, 
	cv::Mat &PeakHeight, 
	double minimum_peak_height, 
	double minimum_peak_distance);

bool DetectDrop(cv::Mat Frame,
	int roi_y1, 
	int roi_y2, 
	double min_pk_height,
	double min_pk_dist, 
	int *DropLocation,
	int counter);

bool DetectDrop_Ed(cv::Mat Frame,
	int roi_y1,
	int roi_y2,
	int topbottomborder_px,
	int thresholdLevel,
	double min_pk_dist,
	int *DropLocationX,
	int *numDrops);

void CreateDropRoi(cv::Mat frame, 
	cv::Mat &DropROI, 
	int * DropLocation, 
	const int Padding,
	int &roiX,
	int &roiY);

float CreateDropMask(cv::Mat DropROI, 
	cv::Mat &DM, 
	int *DropCentre, 
	double thresh);

bool DetectCells(cv::Mat DropROI, 
	cv::Mat DropMask,
	int *CellLocation, 
	double Threshold, 
	int dbg);


bool DetectCells_Ed(cv::Mat DropROI,
	cv::Mat DropMask,
	int *CellLocation,
	int openSize,
	int erodeSize,
	int threshold,
	int dbg);


bool FindDropWidths(cv::Mat frame,
	vector<float> &widths2,
	int *numDrops,
	cv::Mat *debugOutput,
	vector<float> &xList,
	vector<float> &yList,
	int roi_y1,
	int roi_y2,
	int top,
	int thresholdLevel1,
	double min_pk_distance,
	int thresholdLevel2,
	int structureEl1,
	int structureEl2,
	int padding_px);

#endif