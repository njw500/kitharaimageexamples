#include "stdafx.h"
#include "HoughDroplets.h"
#include "DebugFlags.h"
#include "SysParams.h"
#include "WriteVector.h"
#include "get_droplet_contour.h"

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;


void HoughDroplets(Mat data, vector<float> &output, const SysParams Params)
{
	vector<Vec3f> circles;
	
	double minDist = 30;
	double minRadius = 20;
	double maxRadius = 45;

	HoughCircles(data, circles, CV_HOUGH_GRADIENT, 2, minDist, 100, 100, minRadius, maxRadius);

	Mat img;
	data.convertTo(img, CV_8UC3);

	for (size_t i = 0; i < circles.size(); i++)
	{
		Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
		int radius = cvRound(circles[i][2]);
		// draw the circle center
	
		circle(img, center, 3, Scalar(255, 255, 0), -1, 8, 0);
		// draw the circle outline
		//circle(img, center, radius, Scalar(255, 0, 255), 3, 8, 0);
	}

	//namedWindow("circles", 1);
	//imshow("circles", img);

	// For each circle pull out the box (as in sort)

	//vector<RotatedRect> minRect(circles.size());
	vector<Vec2i> VertEdges(circles.size());

	vector<float> Widths;

	Widths.reserve(circles.size());
	
	int WidthCount=0;
	Vec2i CurEdge;

	//data.convertTo(img, CV_8UC3);

	for (int i = 0; i < circles.size(); ++i)
	{
		
		Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
	    float radius = cvRound(circles[i][2]);
		
		FindCircleEdges(data, center, radius, CurEdge);// VertEdges);
		VertEdges[i][0] = CurEdge[0];
		VertEdges[i][1] = CurEdge[1];
		if (CurEdge[0] > 0)
		{
			Widths.push_back((float)(CurEdge[1] - CurEdge[0]));
			//Widths[WidthCount++] = ;// Float for compatility 
			line(img, Point(CurEdge[0], 0), Point(CurEdge[0],img.rows), Scalar(255, 0, 255));
			line(img, Point(CurEdge[1], 0), Point(CurEdge[1], img.rows), Scalar(255, 0, 255));
		}
		// Draw edges into display image
		//reduce(im, vec, 1, REDUCE_AVG, -1);
		//WriteVector(vec, "Reduce1.csv");
	}
	if (Params.DisplayMode[WHough])
		displayIm("Lines", img);
	//imshow("Lines", img);
	//waitKey(2);
	
	output = Widths;
}


void FindCircleEdges(Mat data, const Point centre, const float radius, Vec2i &VertEdges)
{
	int Horiz = radius*1.4;
	int Vert = radius *0.7;
	
	int Top = centre.y - Vert;

	if (Top < 0)
		Top = 0;

	int Hi = Vert;
	if (Hi + Top + Vert >= data.rows)
		Hi = (data.rows - Top - Vert) - 2;

	Rect R;
	if ((centre.x + Horiz >= data.cols - 2) || (centre.x - Horiz < 5))
		R = Rect(centre.x, centre.y, 3, 3);// Junk
	else
		R = Rect(centre.x - Horiz, Top, 2 * Horiz, Vert + Hi);


	Mat im = data(R);
	//imshow("Subx", im);

	Mat vec;
	//imwrite("Sub.jpg", im);

	reduce(im, vec, 0, REDUCE_MAX, -1);
	//WriteVector(vec, "Reduce0.csv");



	double thresh = 127.0;
	Mat grad0;
	thresh = threshold(vec, grad0, thresh, 1, THRESH_BINARY);
	//WriteVector(grad0, "Grad0.csv");
	grad0.convertTo(grad0, CV_16U);

	Mat gf;
	grad0.convertTo(gf, CV_32F);

	Mat mp;
	Mat kernel;
	kernel = kernel.ones(Size(5, 1), gf.type());
	dilate(gf, mp, kernel);
	erode(mp, gf, kernel);

	gf.convertTo(grad0, CV_16U);
	//WriteVector(grad0, "Grad0x.csv");
	

	// Look through Grad to find first and last transition
	// But start at the centre and work out to avoid overlapping droplet issues

	int VecCentre = grad0.cols / 2;
	int Start = -1, End = -1;
	bool FoundEdges = false;

	// Go up first
	int y = VecCentre;
	while ((y < grad0.cols) && (grad0.at<unsigned short int>(y) < 1))
		++y;
	// Maybe at edge
	if (y< grad0.cols) // (grad0.at<unsigned short int>(y) > 0)
	{
		// Continue search
		while ((y < grad0.cols) && (grad0.at<unsigned short int>(y) > 0))
			++y;

		if (y < grad0.cols) // grad0.at<unsigned short int>(y) == 0)
			End = y - 1;
	}

	if (End > 0)
	{
		y = VecCentre;
		while ((y > 0) && (grad0.at<unsigned short int>(y) < 1))
			--y;
		if (grad0.at<unsigned short int>(y) > 0)
		{
			while ((y > 0) && (grad0.at<unsigned short int>(y) > 0))
				--y;

			if (grad0.at<unsigned short int>(y) == 0)
			{
				Start = y + 1;
				FoundEdges = true;
			}
		}
	}

	if (FoundEdges)
	{
		/*
		im.convertTo(im, CV_8UC3);
		line(im, Point(Start, 0), Point(Start, im.rows), Scalar(255, 0, 255));
		line(im, Point(End, 0), Point(End, im.rows), Scalar(255, 0, 255));
	
		Mat out;
		Size osize;
		resize(im, out, osize,3, 3, INTER_LINEAR);

		imshow("Sub", out);
		waitKey(2);*/

		VertEdges[0] = R.x + Start;
		VertEdges[1] = R.x + End;
		//waitKey(2);
	}
	else
	{
		VertEdges[0] = -1;
		VertEdges[1] = -1;
	}



}