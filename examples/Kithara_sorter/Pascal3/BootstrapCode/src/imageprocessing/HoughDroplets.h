#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>


#include "SysParams.h"

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;



void HoughDroplets(Mat data, vector<float> &output , const SysParams Params);

void FindCircleEdges(Mat data, Point center, float radius, Vec2i &VertEdges);