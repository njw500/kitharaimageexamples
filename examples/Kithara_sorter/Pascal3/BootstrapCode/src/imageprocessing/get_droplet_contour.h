#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>
#include "SysParams.h"

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;

//void get_droplet_contour(Mat background, Mat frame, float threshold1, float threshold2,
//					float detection_radius, Mat kernel);

void get_droplet_contour(Mat background, Mat frame, Mat &OutputImage, vector<vector<Point> > &output, float threshold1, float threshold2,
	float detection_radius, Mat kernel, SysParams Params);

//void complexhull(Mat greyframe, const float threshold1, const float threshold2);
void complexhull(Mat greyframe, vector<vector<Point> > &Hull, const float threshold1, const float threshold2, SysParams Params);
void complexhull_threshold(Mat greyframe, vector<vector<Point> > &Hull);
void contours_within_radius(vector<vector<Point> >contours, vector<vector<Point> > &output, const float detection_radius, Mat frame);

void displayIm(String str, Mat im);
void displayIm(String str, Mat im, int flag);
void ScaleImage(Mat in, Mat &out);
void ScaleImageAbs(Mat in, Mat &out);

float GetHistogram(const vector<float>& data, int count, float &mn, float &mx, float &std,String name);
//void drawHist(const vector<float>& data, Mat3b& dst, int binSize = 3, int height = 0);