#include "stdafx.h"
#include "BootstrapSysParams.h"
#include "DebugFlags.h"

// For Debug Macros

// Namespace for using cout
using namespace std;


BootstrapSysParams::BootstrapSysParams()
{

	FrameROIr.x = 300;// Prefferred format
	FrameROIr.y = 664;
	FrameROIr.width = 1800;// 2600;// 1800;
	FrameROIr.height = 25;

	NumGrab = 500;

	cParams.SetRect(FrameROIr);

	cParams.BlockMode = true;
	cParams.AutoStart = false;
	
	
	InteractiveMode = true; // Start in single frame display and cursor mode
	//CameraSerial = "21799755";// Local office
	//CameraSerial = "21982883";// Generator

	// 21997816;  // SORTER
	// 22025914;  // DISP
	// 21982883;  // BOOT

	DisplayMode[WGrab] = true; // Display this window   WOutput
	DisplayMode[WHough] = true;
	DisplayMode[WBack] = false;
	DisplayMode[WCropped] = true;
	DisplayMode[WEdges] = false;
	DisplayMode[WHist] = true;
	DisplayMode[WOutput] = true;
	DisplayMode[WDisplayEd] = true;
	DisplayMode[WTrackerFilter] = true;

	//VideoFileName = "C:/Temp/161129_bootstrap_newDrops_A7000_B1000_V6Close_600FPS.avi";
	VideoFileName = "c:/Temp/161129_bootstrap_newDrops_A7000_B1000_600FPS.avi";

	VideoFileName = "V:/Projects/Pascal/Bootstrap/Blocked/gain8-6dB.avi";

#ifdef ENABLECAMERA

	// ED: THIS NEEDS FIXING AND CLEANING.

	// Probably need to express the regions as proper parameter values, i.e., x and y coords...
	Bs_EndRow = 231; // The end row (on the left of the image) representing the final point that the journey time should be calculated
	BS_roiVelRegion1 = cv::Rect(Bs_EndRow + 470 + 170, 1, 492, 20);
	BS_roiVelRegion2 = cv::Rect(Bs_EndRow, 1, 470, 20);
	Bs_StartRow = 2000; // The start row (on the right of the image) from which point the journey time should be calculated

#endif

#ifdef EMULATEFROMFILE
	// This block wins if both #define flags are active here

	VideoIsPreCropped = true;
	// If the above flag is true then the ROI below is ignored
	BootstrapFrame_ROI_Left = 527;
	BootstrapFrame_ROI_Top = 20;// 58;  .. WAS 58 NJW 12/12/16
	BootstrapFrame_ROI_Width = 2064;
	BootstrapFrame_ROI_Height = 22;

	/* Probably need to express the regions as proper parameter values, i.e., x and y coords...*/

	int Width1 = 650; //2000 - 1701
	int Width2 = 550; // 1700 - 1453
	// 2nd val was 58
	BS_roiVelRegion1 = cv::Rect(1350, 0, Width1, 20);// A region over which the velocity will be separately computed
	BS_roiVelRegion2 = cv::Rect(650, 0, Width2, 20);// A region over which the velocity will be separately computed
	
	Bs_StartRow = 1000; // The start row (on the right of the image) from which point the journey time should be calculated
	Bs_EndRow = 700; // The end row (on the left of the image) representing the final point that the journey time should be calculated
#endif


}

// Do nothing in Destructor
BootstrapSysParams::~BootstrapSysParams()
{
}