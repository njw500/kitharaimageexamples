
#pragma once

#ifndef BOOTSTRAPSYSPARAMS_C
#define BOOTSTRAPSYSPARAMS_C

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "..\..\..\Utilities\Utilities\CamParams.h"


// Namespace for using cout.
using namespace std;

#define WGrab  0
#define WHough 1
#define WBack 2
#define WCropped 3
#define WEdges 4
#define WHist 5
#define WOutput 6
#define WDisplayEd 7
#define WTrackerFilter 8

class BootstrapSysParams
{
public:
	BootstrapSysParams();
	~BootstrapSysParams();

	// Define all the parameters that are tuned and tuneable in the algorithm
	static const uint NumberOfImagesToGrab = 600;
	int NumGrab;

	int roi1;

	CamParams cParams;

	const bool DoFrameRotate = false;
	bool VideoIsPreCropped = false;
	const float FrameRotate = 0.0;

	int Frame_ROI_Left, Frame_ROI_Top, Frame_ROI_Width, Frame_ROI_Height;
	cv::Rect FrameROIr;

	int BootstrapFrame_ROI_Left, BootstrapFrame_ROI_Top, BootstrapFrame_ROI_Width, BootstrapFrame_ROI_Height;
	//cv::Rect BS_roiVelRegion4, BS_roiVelRegion3;
	cv::Rect	BS_roiVelRegion2, BS_roiVelRegion1;


	const float Zone_Detection_Radius = 0.4f;	
	const float AverageDecay = 0.1f;

	const double Drop_minDist = 30;
	const double Drop_minRadius = 20;
	const double Drop_maxRadius = 45;
	const int	   MinDropsPerFrame = 4;

	bool InteractiveMode;
	bool DisplayMode[8];
	cv::String VideoFileName;

	// Define a set of parameters for the droplet detection using morphological operations
	int roi_y1 = 25;  //roi_y1 : The start column that should be selected
	int roi_y2 = 30;  // roi_y2 : The end column that should be selected

	int top = 5;
	int thresholdLevel1 = 100;

	double min_pk_distance = 2 * Drop_minRadius; // 40 min_pk_dist : The distance which corresponds to the minimum diameter of a drop.
	int thresholdLevel2 = 90;
	int structureEl1 = 1;
	int structureEl2 = 15;
	int padding_px = 5;



	int Bs_StartRow, Bs_EndRow;

	int BS_roi_y1 = 8;
	int BS_roi_y2 = 9;
	
private:
	int ok;

};

#endif