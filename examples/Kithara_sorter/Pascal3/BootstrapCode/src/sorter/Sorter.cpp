/*
New Sorter code.

After Ed Bean's debug

*/

#include "stdafx.h"
#include "DropDetection.hpp"
#include <ctime>
#include <iostream>
#include <fstream>

#include "DebugFlags.h"


// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>

#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif


// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;

#include "BaslerCamera.h"
#include "SysParams.h"

// Number of images to be grabbed.
static const uint32_t c_countOfImagesToGrab = 200;


int main(int argc, char* argv[])
{
   

	int startFrom = 0; // Use this to start processing from a particular location
	int FrameCount =0;
	
	bool CamOK = false;

	SysParams Params;


	// Parameters defining the square box from the top (?)
	const int Left = 250, Top = 10, Width = 280, Height = 250;
	const int MinimumPeakHeight = 50; // Some parameter to do with drop detection... 
	const int MinimumPeakDistance = 100; // Some parameter to do with drop detection...
	int Padding = 15; // Padding of the drop region
	int roi_y1 = 100; // The ROI for the drop detection starting height
	int roi_y2 = 120; // The ROI for the drop detection end height
	int roiX, roiY; // Additional offset from the drop ROI

	// Declare + Initialise
	bool Detected = false; // Flag to indicate whether a cell is detected
	int DropLocation[2]; // The location of the drop (if detected)
	int DropCentre[2]; // The centre of the drop with respect to the 
	int DropRadius;
	double CellThreshold = 600;
	int CellLocation[2];
	int CellLocation2[2];
	bool CellDetected2;
	bool CellDetected;

	// Initialise the required openCV images
	cv::Mat croppedImage;
	cv::Mat croppedImageBackup;
	cv::Mat DropROI;
	cv::Mat DropROI2;
	cv::Mat DropMask;
	cv::Mat disp;
	cv::Mat invertedImage;
	
	Scalar Colour;




    // Automagically call PylonInitialize and PylonTerminate to ensure the pylon runtime system
    // is initialized during the lifetime of this object.
    Pylon::PylonAutoInitTerm autoInitTerm;

	cv::Mat avgImg; 

    try
    {
		LARGE_INTEGER Frequency, start, end;

		double freq,diff;
		char string[80];
		char filename[100];

		QueryPerformanceFrequency(&Frequency);
		cout << "QueryPerformanceFrequency -> " << _i64toa(Frequency.QuadPart, &string[0], 10) << endl;

   
		// Create an instant camera object with the camera device found first.

		BaslerCamera  camera;

		CamOK = camera.Init(false, "", CAM_FREE_MODE); // Get first camera available

		if (CamOK)
			camera.SetGain(0.0);
		

			//avgImg.create(width, height, CV_32FC3);

			// The parameter MaxNumBuffer can be used to control the count of buffers
			// allocated for grabbing. The default value of this parameter is 10.
			//camera.MaxNumBuffer = 500;
			//camera.ExposureTime.SetValue(200.0);

			// Create a pylon ImageFormatConverter object.
			CImageFormatConverter formatConverter;
			// Specify the output pixel format.
			formatConverter.OutputPixelFormat = PixelType_BGR8packed;
	
		// Create a PylonImage that will be used to create OpenCV images later.
		CPylonImage pylonImage;
		// Declare an integer variable to count the number of grabbed images
		// and create image file names with ascending number.
		int grabbedImages= 20;

		// Create an OpenCV video creator.
		VideoWriter cvVideoCreator;

		// Open stored video file
		sprintf(filename, "%s", Params.VideoFileName.c_str());

#ifdef DEBUGOUTPUT
		ofstream myfile;
		myfile.open("results4.csv");
#endif
		printf("File Loaded");
		VideoCapture vidCap;

		vidCap.open(filename);
		printf("Vod Cap Open");
		int NumFramesInFile = vidCap.get(CAP_PROP_FRAME_COUNT);

		// Create an OpenCV image.
		Mat openCvImage;


		// Allocate and read in the frames
		// pre-load to avoid this read giving misleading load times

		Mat Frames[Params.c_countOfImagesToGrab];
		int NumFrames = Params.c_countOfImagesToGrab;

		if (NumFramesInFile < NumFrames)
		{
			cout << "Too few frames" << endl;
			NumFrames = NumFramesInFile;
		}

		cout << "Reading frames" << endl;

		for (int i = 0; i < NumFrames; ++i)
		{
			vidCap.read(Frames[i]);  // Fake a frame from the stored file
			if (Frames[i].empty())
			{
				cerr << "ERROR! blank frame grabbed\n";
				break;
			}
		}
		cout << "Frames read into memory" << endl;


		// Other openVCImages
		Mat filtered_frame, mask,frame;

#ifdef recordVideo
		// Define the video file name.
		std::string videoFileName = "c:\\Temp\\openCvVideo.avi";

		// Define the video frame size.
		cv::Size frameSize = Size((int)camera.GetWidth(), (int)camera.GetHeight());

		// Set the codec type and the frame rate. You have 3 codec options here.
		// The frame rate should match or be lower than the camera acquisition frame rate.
		cvVideoCreator.open(videoFileName, CV_FOURCC('D','I','V','X'), 20, frameSize, true);
		//cvVideoCreator.open(videoFileName, CV_FOURCC('M','P','4','2'), 20, frameSize, true); 
		//cvVideoCreator.open(videoFileName, CV_FOURCC('M','J','P','G'), 20, frameSize, true);
#endif

		QueryPerformanceCounter(&start);

	
        // Start the grabbing of c_countOfImagesToGrab images.
        // The camera device is parameterized with a default configuration which
        // sets up free-running continuous acquisition.
		//camera.StartGrabbing( c_countOfImagesToGrab, GrabStrategy_LatestImageOnly);

        // This smart pointer will receive the grab result data.
        CGrabResultPtr ptrGrabResult;

        // Camera.StopGrabbing() is called automatically by the RetrieveResult() method
        // when c_countOfImagesToGrab images have been retrieved.

		// Start main grab and process loop
		bool StopLoop = false;

		while (!StopLoop)
        {
			// Wait for an image and then retrieve it. A timeout of 5000 ms is used.
			//camera.RetrieveResult( 5000, ptrGrabResult, TimeoutHandling_ThrowException);
			if (CamOK)
				camera.GrabNextImg(); // NOT USED OR CONVERTED YET

			cout << fixed << FrameCount << endl;
			Mat getframe;
			getframe = Frames[FrameCount];
			getframe.copyTo(frame);

			//vidCap.read(frame);  // Fake a frame from the stored file
		
            // Image grabbed successfully?
			if (camera.GrabSuccesful())
			{
				// Access the image data.
				//cout << "SizeX: " << ptrGrabResult->GetWidth() << endl;
				//cout << "SizeY: " << ptrGrabResult->GetHeight() << endl;

				// Convert the grabbed buffer to a pylon image.

				//formatConverter.Convert(pylonImage, ptrGrabResult);

				// Create an OpenCV image from a pylon image.
				//openCvImage = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, (uint8_t *)pylonImage.GetBuffer());
		
#ifdef DEBUGOUTPUT
				// Comment DEBUGOUTPUT to avoid saving images
				// Create the current image name for saving.
				if (saveImages)
				{
					std::ostringstream s;
					// Create image name files with ascending grabbed image numbers.
					s << "image_" << grabbedImages << ".jpg";
					std::string imageName(s.str());
					// Save an OpenCV image.
					imwrite(imageName, openCvImage);
					grabbedImages++;
				}
				
#endif
				// Process loop
				if (FrameCount>=startFrom)
				{ 
				
					// Extract square box from the top
					croppedImage = frame(Rect(Left, Top, Width, Height));
					// Write the cropped image to file
					croppedImageBackup = croppedImage.clone();

					cvtColor(croppedImage, croppedImage, COLOR_BGR2GRAY);
#ifdef DEBUGOUTPUT
					imwrite("Cropped.bmp", croppedImage);			
				
					imwrite("CroppedBGR.bmp", croppedImage);
#endif
					invertedImage = croppedImage.clone();
					threshold(croppedImage, invertedImage, 5, 255, THRESH_BINARY_INV);
#ifdef DEBUGOUTPUT
					imwrite("Inverted.bmp", invertedImage);
#endif
					// Detect the drop
					Detected = DetectDrop_Ed(croppedImage, roi_y1, roi_y2, (int) 30, (int)5,MinimumPeakDistance, DropLocation, FrameCount);
					//namedWindow("Inverted", WINDOW_AUTOSIZE);

#ifdef DEBUGPLOTS
					imshow("Inverted", invertedImage);
					cout << "Drop Detected: " << Detected << endl;
					namedWindow("croppedImage", WINDOW_AUTOSIZE);
					imshow("croppedImage", croppedImage);
#endif
					CellDetected = false;
					CellDetected2 = false;
					if (Detected)
					{

						// Create an image containing only the detected drop and mask outside of the drop radius
						CreateDropRoi(croppedImage, DropROI, DropLocation, Padding,roiX,roiY);

						DropRadius = CreateDropMask(DropROI, DropMask, DropCentre, 60.0);
#ifdef DEBUGPLOTS
						namedWindow("Masked Drop", WINDOW_AUTOSIZE);
						imshow("Masked Drop", DropMask);
#endif
						//double elapsed_secs;
						
						//clock_t begin = clock();
						//CellDetected = DetectCells(DropROI, DropMask, CellLocation2, CellThreshold, 1);
						//clock_t end = clock();
						//elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
						//cout << "Elapsed Time 1: " << elapsed_secs << endl;
						

						DropROI2 = DropROI.clone();
						//clock_t begin2 = clock();
						CellDetected2 = DetectCells_Ed(DropROI2, DropMask, CellLocation2,(int)8, (int)16, (int)20, 1);
						//clock_t end2 = clock();
						//elapsed_secs = double(end2 - begin2) / CLOCKS_PER_SEC;
						

						//cout << "Frame: " << fixed << FrameCount << " Cell?: " << fixed << CellDetected << endl;

#ifdef DEBUGPLOTS
						//cout << "Elapsed Time 2: " << elapsed_secs << endl;
						namedWindow("CROPPED FRAME", WINDOW_AUTOSIZE);
						imshow("CROPPED FRAME", croppedImageBackup);
#endif

#ifdef DEBUGOUTPUT
						if (CellDetected)
						{
							Colour = Scalar(0, 255, 0);
						}
						else
						{
							Colour = Scalar(255, 0, 0);
						}

						cvtColor(DropROI, disp, COLOR_GRAY2BGR);
						
						circle(disp, Point(DropCentre[0], DropCentre[1]), (int)DropRadius, Colour);
						int CellRadius = (int)(DropRadius / 9);
						/*if (CellDetected)
						{
							circle(disp, Point(CellLocation[0], CellLocation[1]), CellRadius, Colour);
						}*/
						if (CellDetected2)
						{
							Scalar Colour2 = Scalar(0, 0, 255);
							circle(disp, Point(CellLocation2[0], CellLocation2[1]), CellRadius, Colour2);
						}

						std::string result;
						result = "Diag";

						result = "Diag" + std::to_string(FrameCount);
						

						//namedWindow(result , WINDOW_AUTOSIZE);
						
						putText(disp, std::to_string(FrameCount), cvPoint(30,30), 
							FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,255,0), 1, CV_AA);
						
						//imshow(result, disp);
						result = "Result_" + std::to_string(FrameCount) + ".bmp";
						imwrite(result, disp);
#endif
						// Set recordVideo to '1' to record AVI video file.
#ifdef recordVideo
						
							cvVideoCreator.write(openCvImage);
							grabbedImages++;
							//cv::accumulate(openCvImage, avgImg);
					
#endif					

						/*
						if(Detected)
						{
						// Create an OpenCV display window.
						namedWindow("OpenCV Display Window", CV_WINDOW_NORMAL); // other options: CV_AUTOSIZE, CV_FREERATIO

						// Display the current image in the OpenCV display window.
						imshow("OpenCV Display Window", croppedImage);// openCvImage);
						// Define a timeout for customer's input in ms.
						// '0' means indefinite, i.e. the next image will be displayed after closing the window.
						// '1' means live stream

						}
						waitKey(1);
						*/

						// Some tidying up...
						
						disp.release();
						DropMask.release();
						DropROI.release();
						croppedImage.release();
						openCvImage.release();

						// Other openVCImages
						filtered_frame.release();
						mask.release();
						
					
						
#ifdef PYLON_WIN_BUILD
						// Display the grabbed image in pylon.
						//Pylon::DisplayImage(1, ptrGrabResult);
#endif
					}
				}
				// Produce a nice image with the cell location outlined
				if (Detected)
				{
					Colour = Scalar(255, 0, 0);
					circle(frame, Point(DropCentre[0] + Left + roiX, DropCentre[1] + Top + roiY), DropRadius, Colour);
				}
				else
				{
					Colour = Scalar(0, 255, 0);
				}
				
				
#ifdef DEBUGOUTPUT
				myfile << FrameCount << "," << CellDetected2 << "," << CellDetected << "," << Detected << "," << CellLocation2[0] << "," << CellLocation2[1] << "," << DropLocation[0] << "," << DropLocation[1] << endl;
#endif				

#ifdef DEBUGPLOTS
				namedWindow("Full Frame", WINDOW_AUTOSIZE);
				moveWindow("Full Frame", 0, 800);
				imshow("Full Frame", frame);

				
				frame.release();
				waitKey(1);
#endif
            }
            else
            {
                cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
            }


			FrameCount = FrameCount + 1;
			FrameCount = FrameCount + 1;
			if (FrameCount > NumFrames - 2)
			{
				FrameCount = 2;// avoid the special conditions
				cout << "Rewind" << endl;
				//AreaCount = 0;
				StopLoop = true;
			}

        }

		QueryPerformanceCounter(&end);
#ifdef DEBUGOUTPUT
		myfile.close();
#endif

		diff = (double)(end.QuadPart - start.QuadPart);
		freq = (double)Frequency.QuadPart;
		diff = diff / freq;
		cout << "end - start -> " << _i64toa(end.QuadPart - start.QuadPart, &string[0], 10) << endl;


		printf("%d frames in %g sec, rate %g\n", c_countOfImagesToGrab, diff, ((double)c_countOfImagesToGrab) / diff);

		cout << "Num frames" << _i64toa_s(grabbedImages,&string[0],30,10) << endl;

		// Release the video file on leaving.
#ifdef recordVideo
			cvVideoCreator.release();
#endif
    }
    catch (GenICam::GenericException &e)
    {
        // Error handling.
        cerr << "An exception occurred." << endl
        << e.GetDescription() << endl;

		cerr << endl << "Press Enter to exit." << endl;
		while (cin.get() != '\n');
    }

    // Comment the following two lines to disable waiting on exit.
    cerr << endl << "Press Enter to exit." << endl;
    while( cin.get() != '\n');

    return 0;
}
