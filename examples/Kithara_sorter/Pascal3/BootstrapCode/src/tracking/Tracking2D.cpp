// Detect Drop
// Still straight C
// Droplet detection class

#include "stdafx.h"
#include "io.h"
#include <algorithm>
// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "Tracking2D.h"
// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;

// Do nothing in Constructor
Tracker2D::Tracker2D()
{
	initialised = false;
	//velocityList = std::vector<double>();
}


// Do nothing in 2DTracker()
Tracker2D::~Tracker2D()
{
	
}

// Initialise the tracker by defining a set of regions of interest for which the velocity
// should be independently assessed. Note that the parameter roiListIn must read from RIGHT to LEFT 
// newFrame should be the same size as consequent frames. 
void Tracker2D::init(std::vector<cv::Rect> roiListIn, cv::Mat &newFrame)
{
	roiList = roiListIn;

	for (std::vector<cv::Rect>::iterator roi = roiList.begin();
		roi != roiList.end();
		roi++)
	{
		// Create a  hanning window
		Mat hann;
		Mat tmp = newFrame(*roi);
		createHanningWindow(hann, tmp.size(), CV_32F);
		hanningWindows.push_back(hann);
		averagedVelocityList.push_back(deque<double>());
	}
	 
	oldFrame = newFrame.clone(); // Keep a local copy of the oldFrame. This will be manually copied each time.  
	initialised = true;	

}

void Tracker2D::updateRoi(std::vector<cv::Rect> roiListIn)
{
	roiList = roiListIn;
	hanningWindows.clear();
	averagedVelocityList.clear();
	for (std::vector<cv::Rect>::iterator roi = roiList.begin();
		roi != roiList.end();
		roi++)
	{
		// Create a  hanning window
		Mat hann;
		createHanningWindow(hann, (oldFrame)(*roi).size(), CV_32F);
		hanningWindows.push_back(hann);
		averagedVelocityList.push_back(deque<double>());
	}

	//oldFrame = newFrame->clone(); // Keep a local copy of the oldFrame. This will be manually copied each time.  
	//initialised = true;

}

void Tracker2D::reset()
{
	averagedVelocityList.clear();
}

void Tracker2D::ShiftRoi(int Which, int Dir, int sz)
{
	// Clear current 
	//roiList = roiListIn;
	hanningWindows.clear();

	averagedVelocityList.clear();

	if (Which < 0)
	{
		// ALL
		for (int i = 0; i < roiList.size(); ++i)
		{
			if (Dir == 1)
			{
				roiList[i].y = roiList[i].y + sz;
				if (roiList[i].y < 0)
					roiList[i].y = 0;
			}
			else
			{
				roiList[i].x = roiList[i].x + sz;
				if (roiList[i].x < 0)
					roiList[i].x = 0;
			}
		}
	}
	else
	{
		if (Dir == 1)
		{
			roiList[Which].y = roiList[Which].y + sz;
			if (roiList[Which].y < 0)
				roiList[Which].y = 0;
		}
		else
		{
			roiList[Which].x = roiList[Which].x + sz;
			if (roiList[Which].x < 0)
				roiList[Which].x = 0;
		}
	}

	for (std::vector<cv::Rect>::iterator roi = roiList.begin();
		roi != roiList.end();
		roi++)
	{
		// Create a  hanning window
		Mat hann;
		createHanningWindow(hann, (oldFrame)(*roi).size(), CV_32F);
		hanningWindows.push_back(hann);
		averagedVelocityList.push_back(deque<double>());
	}


}


// Update the class with a new frame (note, this copies the data to an old 
// frame stored within the class). It calculates the new velocity using the 
// find2DShift function, and then by taking a median across a given size
// history of velocities. (to minimise potential outliers from the phase method)

void Tracker2D::update(const cv::Mat newFrame)
{
	// clear velocity list (every frame)
	// will be used to estimate the velocity (by taking the median)

	std::vector<double> velocityList; // The velocities for each ROI
	velocityList.clear();// Keep this local to avoid confusion 

	medianVelocityList.clear();// This is ONLY ever two ROI elements long
	SDVelocityList.clear();
	// Determine the 2D shift between the new frame and the last frame
	// This calculates the TWO (or more) velocitices by cross correlation of the windowed frames
	find2DShift(oldFrame, newFrame, roiList, velocityList);

	// Make a copy of the old frame
	oldFrame = newFrame.clone();

	double Av, AvM, SD, Md;

	ValidVelocities = true;// Set on the hope that all will be well, but....

	// Calculate the median velocity for each ROI
	for (int i = 0; i < averagedVelocityList.size(); i++)
	{
		if (averagedVelocityList.at(i).size() < windowSize)
		{
			averagedVelocityList.at(i).push_front(velocityList.at(i));
		}
		else
		{
			averagedVelocityList.at(i).pop_back();
			averagedVelocityList.at(i).push_front(velocityList.at(i));
		}

		if (averagedVelocityList.at(i).size() > 10)// More than 10, so calc stats
		{
			GetVectorStats(averagedVelocityList.at(i), Av, AvM, SD, Md);
			medianVelocityList.push_back(AvM);// Use the Percentile Mean 
			SDVelocityList.push_back(SD);// Invalid value
		}
		else
		{
			medianVelocityList.push_back(-999);// Invalid value
			SDVelocityList.push_back(-999);// Invalid value
			ValidVelocities = false;// ALL calculations must be valid
		}
		
	}

	cout << "Cur Vels: " << medianVelocityList.at(0) << " :  " << medianVelocityList.at(1) << endl;
	cout << "SD Vels: " << SDVelocityList.at(0) << " :  " << SDVelocityList.at(1) << endl;
}


void Tracker2D::GetVectorStats(deque<double> data, double &Av, double &AvM, double &SD, double &Md)
{

	// Calculate max,min,sum and sum^2
	double mn = data[0];

	if (mn < 0)
		mn = 0;// <0 is not allowed

	double mx = mn;

	double sum = 0;
	double sum2 = 0;
	double val;
	double count = data.size();
	int NZcount = 0;
	for (int i = 0; i < data.size(); ++i)
	{
		val = data[i];
		
		if (val > 0.0)
		{
			++NZcount;

			if (val > mx)
				mx = val;

			if (val < mn)
				mn = val;

			sum = sum + val;
			sum2 = sum2 + (val*val);
		}
	}

	Av = sum / (float)NZcount;
	SD = (sum2 / (float)(NZcount)) - (Av*Av);
	SD = sqrt(SD);

	// Now find the percentile limits and the median

	std::deque<double>temp;
	//double median, median2;

	temp = data;
	std::sort(temp.begin(), temp.end());
	int n = temp.size() / 2;
	Md = temp.at(n);
	

	// Sort into 20 bins
	double range = mx - mn;
	
	const int Bins = 50;
	int Hist[Bins+1];
	for (int i = 0; i < Bins+1; ++i)
		Hist[i] = 0;

	double Scaled;
	NZcount = 0;
	for (int i = 0; i < data.size(); ++i)
	{
		if (data[i] > -1e-10)// Positivity constraint
		{
			Scaled = (int)(((data[i] - mn) / range) * Bins);
			if (Scaled < 0)
				Scaled = 0;// Should not happen

			++Hist[(int)Scaled];
			++NZcount;
		}
	}

	int Lo = (int)((double)NZcount *0.05);
	int Hi = (int)((double)NZcount *0.95);
	double PcL = -1;
	double PcH=-1;
	
	int hSum = 0;
	for (int i = 0; i < Bins+1; ++i)
	{
		hSum = hSum + Hist[i];
		if ((hSum >= Lo) && (PcL < 0))
			PcL = i;
		if ((hSum >= Hi) && (PcH < 0))
		{
			PcH = i;
			break;
		}
	}
	double step = range / Bins;

	PcL = (PcL*step) + mn;
	PcH = ((PcH+1)*step) + mn;

	// Now repeat Mean and SD calculation
	// This is the 5% to 95% percentile limited distribution

	sum = 0;
	sum2 = 0;
	NZcount = 0;
	for (int i = 0; i < data.size(); ++i)
	{
		val = data[i];

		if ((val >=PcL) && (val<=PcH))
		{
			++NZcount;

			if (val > mx)
				mx = val;

			if (val < mn)
				mn = val;

			sum = sum + val;
			sum2 = sum2 + (val*val);
		}
	}

	AvM = sum / (float)NZcount;

	if (!isfinite(AvM))
	{
		cout << "NOT FINITE !" << endl;
	}
	double SDL = (sum2 / (float)(NZcount)) - (AvM*AvM);
	SDL = sqrt(SDL);
	SD = SDL;// Override

}


// Function to calculate the x velocity through the phaseCorrelation method

void Tracker2D::find2DShift(const cv::Mat oldFrame, const cv::Mat newFrame, 
	std::vector<cv::Rect> roiList, std::vector<double> &vels)

{
	Point2d shift;
	int i = 0;
	
	// Loop over each region of interest
	for (std::vector<cv::Rect>::iterator roi = roiList.begin();
		roi != roiList.end();
		roi++)
	{	
		// Calculate the phase shift in the given region of interest
		shift = phaseCorrelate((newFrame)(*roi), (oldFrame)(*roi), hanningWindows.at(i));

		// Add the x shift as the velocity
		vels.push_back(shift.x);
		i++;
	}

}

// Calculate the number of frames that a drop will take to move from
// xStart to xEnd. This makes use of the current velocity in each region
// and integrates up accordingly. Note that the efficiency of this
// function could be considerably improved by calculating for the fixed
// velocity points. 
double Tracker2D::calculateFramesBetweenPoints(int xStart, int xEnd)
{
	
	double time_frames;

	time_frames = 0;
	for (int i = xStart; i > xEnd; i--)
	{
		double vel;
		vel = calculateCurrentVelocity(i);
		if (vel > 0)
		{
			time_frames = time_frames + (double)1 / vel;
		}
	}
	
	return time_frames;
}

// Calculate the velocity at a given position of x based on the assessed
// velocity history. 
double Tracker2D::calculateCurrentVelocity(int x)
{
	
	cv::Rect thisRoi = roiList.at(0);
	double lastMedian = 0;

	// Only calculate if enough measurement points have been added
	// NJW: BUT THERE ARE ALWAYS ONLY TWO POINTS HERE. 

	if (medianVelocityList.size() > 0)
	{
		if (x >= (thisRoi.x + thisRoi.width))
		{
			return medianVelocityList.at(0);
		}
		thisRoi = roiList.at(roiList.size() - 1);
		if (x <= (thisRoi.x))
		{
			return medianVelocityList.at(medianVelocityList.size() - 1);
		}

		double lastx = 0;
		
		bool lastxFlag = false;

		for (int i = 0; i < medianVelocityList.size(); i++)
		{
			thisRoi = roiList.at(i);
			
			if (lastxFlag == true && x > thisRoi.x + thisRoi.width)
			{
				// linearly interpolate velocities at any gaps
				double m = (medianVelocityList.at(i) - lastMedian) / (thisRoi.x  + thisRoi.width - lastx);
				double c = lastMedian - m * lastx;
				// for now: do not interpolate, and take the slowest velocity!
				return lastMedian;
				return m * x + c;
			}

			if (x <= (thisRoi.x + thisRoi.width) && (x > thisRoi.x))
			{
					return medianVelocityList.at(i);
			}
			if (x <= thisRoi.x)
			{
				lastx = thisRoi.x;
				lastxFlag = true;
				lastMedian = medianVelocityList.at(i);
			}
			else
			{
				lastxFlag = false;
			}
			

		}
	}
	return lastMedian;
	
}



void Tracker2D::updatePlotTrackRegion(cv::Mat *frameDisp, int xStart, int xEnd, int regionWidth)
{
	int offset = 100; // Border to put at the top of the image
	if (medianVelocityList.size() >= 0)
	{
		double velocity = calculateCurrentVelocity(circlePoint1);
		circlePoint1 = circlePoint1 - velocity;
		if (circlePoint1 - regionWidth <= 0)
		{
			circlePoint1 = roiList.at(0).x + roiList.at(0).width - regionWidth;
		}
		Rect roi = Rect(circlePoint1 - regionWidth,0,regionWidth * 2,frameDisp->rows);
		*frameDisp = (*frameDisp)(roi);
	}
}

// Plots a visualisation of the tracker output. 
void Tracker2D::updatePlot(cv::Mat *frameDisp, int xStart, int xEnd)
{
	int offset = 100; // Border to put at the top of the image
	if ((medianVelocityList.size() >= 0) && (ValidVelocities))
	{
		
		cvtColor(*frameDisp, *frameDisp, CV_GRAY2RGB);
		copyMakeBorder(*frameDisp, *frameDisp, offset, 0, 0, 0, 0, Scalar(0, 0, 0));

		if (circleFlag)
		{
			double velocity = calculateCurrentVelocity(circlePoint);// Pick a velocity depending on where the circle drawing point is located

		

			int xStart = 2000;
			int xEnd = 100;
			double time_frames = calculateFramesBetweenPoints(xStart, xEnd);

			cv::line(*frameDisp, Point(xStart, 0), Point(xStart,
				frameDisp->rows), Scalar(0, 0, 255), 1, 8);

			cv::line(*frameDisp, Point(xEnd, 0), Point(xEnd,
				frameDisp->rows), Scalar(0, 0, 255), 1, 8);

			stringstream velocityText;
			velocityText << "V: " << velocity;
			putText(*frameDisp, velocityText.str(), Point(30, 30), cv::FONT_HERSHEY_PLAIN, 2, Scalar(255, 255, 255), 1, 1);

			stringstream timeText;
			timeText << "Time: " << time_frames << " frames.";
			putText(*frameDisp, timeText.str(), Point(300, 30), cv::FONT_HERSHEY_PLAIN, 2, Scalar(255, 255, 255), 1, 1);

			// Display both the current velocitiesmedianVelocityList.at
			stringstream vel2,vel3;
			vel2.precision(5);
			vel2 << medianVelocityList.at(0);
			vel3.precision(5);
			vel3 << medianVelocityList.at(1);

			putText(*frameDisp, vel2.str(), Point(30, 75), cv::FONT_HERSHEY_PLAIN, 2, Scalar(255, 255, 255), 1, 1);
			putText(*frameDisp, vel3.str(), Point(30, 120), cv::FONT_HERSHEY_PLAIN, 2, Scalar(255, 255, 255), 1, 1);


			circlePoint = circlePoint - velocity;
			Scalar colour1;
			colour1 = Scalar(255, 255, 0);
			circle(*frameDisp, Point(circlePoint, 50), 20, colour1, -1, 8);
			cv::line(*frameDisp, Point(circlePoint, 0), Point(circlePoint,
				frameDisp->rows), colour1, 1, 8);

			for (std::vector<cv::Rect>::iterator roi = roiList.begin();
				roi != roiList.end();
				roi++)
			{
				roi->y = roi->y + offset;
				rectangle(*frameDisp, *roi, Scalar(0, 255, 255), 1, 8);
				roi->y = roi->y - offset;
			}
			//roiRightPart2.y = roiRightPart2.y + 100;
			//roiRightPart1.y = roiRightPart1.y + 100;
			//roiLeftPart.y = roiLeftPart.y + 100;

			//rectangle(frameDisp, roiRightPart2, Scalar(0, 255, 255), 1, 8);
			//rectangle(frameDisp, roiRightPart1, Scalar(0, 255, 255), 1, 8);
			//rectangle(frameDisp, roiLeftPart, Scalar(0, 255, 255), 1, 8);

		}
		else
		{
			circlePoint = roiList.at(0).x + roiList.at(0).width;
			circleFlag = true;
		}
		if (circlePoint < 0)
		{
			circleFlag = false;
		}
	}
}