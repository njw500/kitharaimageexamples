#pragma once


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <deque>
#include <iostream>

//#include "io.h"
using namespace std;

// This class carries out 2D tracking (i.e., motion along an x direction) over
// specific regions of interest by looking at two frames. It uses a 
// simple method to find the phase correlation between frames. It supports
// an arbitary number of regions of interest in which the velocity can be estimated,
// but not that the regions should be kept large enough to ensure succesful 
// estimation of the phase. 
class Tracker2D
{
private:
	// Function to find the 2D shift.

	void find2DShift(const cv::Mat oldFrame, const cv::Mat newFrame,
		std::vector<cv::Rect> roiList, std::vector<double> &vels); 

	cv::Mat oldFrame;	//  Pointer to the last frame
	cv::Mat *newFrame;   // Pointer to the latest frame

	std::vector<cv::Mat> hanningWindows; // List of precalculated hanning windows
	std::vector<cv::Rect> roiList; // List of regions of interest to separately 

	bool storeList;

	std::vector<deque<double>> averagedVelocityList; // List of points of which to take the median (for each ROI)

	double circlePoint; // Used for plotting (presents the x point of the plotted circle)
	double circlePoint1 = -1; // used for plotting a zoomed in region.
	// calculate the velocity over a region
	

public:
	Tracker2D();
	~Tracker2D();
	
	// Initialise the tracker with the ROI list as well as the first frame
	void init(std::vector<cv::Rect> roiList, cv::Mat &newFrame);

	// Update the tracker with the first frame
	void update(const cv::Mat newFrame);

	// Plot the latest results to a Mat image
	void updatePlot(cv::Mat *frameDisp, int xStart, int xEnd);

	void updatePlotTrackRegion(cv::Mat *frameDisp, int xStart, int xEnd, int regionWidth);

	// Calculate the time (number of frames) between two points
	double calculateFramesBetweenPoints(int xStart, int xEnd);

	// Calculate the velocity at a given location in the image
	double Tracker2D::calculateCurrentVelocity(int x);

	// Shift the ROI (support interactivity). Note: should probably reuse updateROI
	void ShiftRoi(int Which,int Dir, int sz);

	// Reset for a new burst (i.e., clear all the averages). 
	void reset();

	// Update the roi with a new set of ROI 
	void updateRoi(std::vector<cv::Rect> roiList);
	
	// will be used to estimate the velocity (by taking the median)
	//std::vector<double> velocityList; // The velocities for each ROI

	std::vector<double>medianVelocityList; // The set of velocities
	std::vector<double>SDVelocityList; // The set of velocities


	bool ValidVelocities = false;
	
	bool initialised;		// Flag to confirm that initialisation has taken place
	int listSize;		

	int windowSize = 200;	// Parameter that sets how many frames (this was 4!)
	
	bool circleFlag = false;

	void GetVectorStats(deque<double> VelocityList, double &Av, double &AvM, double &SD, double &Md);

};

