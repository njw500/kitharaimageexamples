#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//#include <stdafx.h>

int main()
{
	char buffer[1024];
	char *record, *line;
	int i = 0, j = 0;
	int mat[256][256];
	FILE *fstream = fopen("InputImageTest.csv", "r");
	if (fstream == NULL)
	{
		printf("\n file opening failed ");
		return -1;
	}
	while ((line = fgets(buffer, sizeof(buffer), fstream)) != NULL)
	{
		record = strtok(line, ";");
		while (record != NULL)
		{
			printf("record : %s", record);    //here you can put the record into the array as per your requirement.
			mat[i][j++] = atoi(record);
			record = strtok(NULL, ";");
		}
		++i;
	}
	return 0;
}