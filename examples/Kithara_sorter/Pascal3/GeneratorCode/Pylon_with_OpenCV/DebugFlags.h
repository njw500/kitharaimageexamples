#define DBG 1
#define FDBG 1
#define FERR 1

// Define if images are to be saved.
#define saveImages 0   // '0'- no; '1'- yes.
#undef saveImages

// Define if video is to be recorded.
#define recordVideo 0    // '0'- no; '1'- yes.
#undef recordVideo

