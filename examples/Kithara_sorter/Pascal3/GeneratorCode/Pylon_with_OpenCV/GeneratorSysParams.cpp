#include "stdafx.h"
#include "GeneratorSysParams.h"

// For Debug Macros
//#include "VideoOutput.h"

// Namespace for using cout
using namespace std;

#define USEPOC 1
//#undef USEPOC


GeneratorSysParams::GeneratorSysParams()
{

	// Define Camera parameters
	// ------------------------
	//cParams.CameraSerial = "21799755";// Local office
	cParams.CameraSerial = "22025921";// Generator
	//CameraSerial = "";
	// 21997816; // SORTER
	// 22025914; // DISP
	// 21982883  // BOOT

	cParams.FrameROIr.x = 1;
	cParams.FrameROIr.y = 341;
	cParams.FrameROIr.width = 1376;
	cParams.FrameROIr.height = 91;
	cParams.exp = 82;
	cParams.gain = 12;
	DoFrameRotate=false;
	FrameRotate = 0.0;

	// Define Video parameters (if using video)
	// -----------------------------------------
	// VideoFileName = "C:/Projects/Pascal/TestVideos/161111_generation_flow1500_P145.avi";
	VideoFileName = "C:/Temp/161205_generator_P14_flow1500_100FPS_3000frames.avi";
	VideoIsPreCropped = true;


	// Image Processing Parameters
	// ---------------------------
	// Parameters for the first method (Nick's method)
	Zone_Detection_Radius = 0.4f;
	AverageDecay = 0.1f;
	Drop_minDist = 30;
	Drop_minRadius = 20;
	Drop_maxRadius = 45;
	MinDropsPerFrame = 4;
	// Set the parameters for FindWidths (Ed's method) function.
	// Note that the most significant are threshold level
	roi_y1 = 18;
#ifdef EMULATEFROMFILE
	roi_y1 = 41;  
#endif
	roi_y2 = 64; 
#ifdef EMULATEFROMFILE
	roi_y2 = 65;
#endif
	regionPadding = 3;
	thresholdLevel1 = 158;
#ifdef EMULATEFROMFILE
	thresholdLevel1 = 96;
#endif
	min_pk_distance = 2 * Drop_minRadius; 
	thresholdLevel2 = 158;
#ifdef EMULATEFROMFILE
	thresholdLevel2 = 96;
#endif
	structureEl1 = 1;	
	structureEl2 = 15;   
	padding_px = 5;
#ifdef EMULATEFROMFILE
	checkDropRowTop = 96;
#endif
#ifdef EMULATEFROMFILE
	checkDropRowTop = 28;  
#endif
#ifdef EMULATEFROMFILE
	checkDropRowBottom = 74; 
#endif
	widthHistory = 300;

	// Display (Debug) Parameters
	// --------------------------
	DisplayMode[WGrab] = true; // Display this window   WOutput
	DisplayMode[WHough] = true;
	DisplayMode[WBack] = false;
	DisplayMode[WCropped] = true;
	DisplayMode[WEdges] = false;
	DisplayMode[WHist] = true;
	DisplayMode[WOutput] = true;
	DisplayMode[WDisplayEd] = true;
	DisplayMode[WDisplaySub] = true;
	DisplayMode[WDisplaySubN] = true;

	// Carry out some range checking
	checkBounds();
}

// Do nothing in Destructor
GeneratorSysParams::~GeneratorSysParams()
{
}

// A helper function to check (and correct) the bounds of key function parameters
bool GeneratorSysParams::checkBounds()
{
	bool retVal = true;
	retVal = retVal && checkVarBounds(&roi_y1, 2, cParams.FrameROIr.height);
	retVal = retVal && checkVarBounds(&roi_y2, 2, cParams.FrameROIr.height - 2);
	retVal = retVal && checkVarBounds(&roi_y1, 2, roi_y2);
	retVal = retVal && checkVarBounds(&regionPadding, 1, 50);
	retVal = retVal && checkVarBounds(&thresholdLevel1, 1, 255);
	retVal = retVal && checkVarBounds(&thresholdLevel2, 1, 255);
	retVal = retVal && checkVarBounds(&padding_px, 1, 50);
	retVal = retVal && checkVarBounds(&checkDropRowTop, 1, cParams.FrameROIr.height);
	retVal = retVal && checkVarBounds(&checkDropRowBottom, 1, cParams.FrameROIr.height);
	retVal = retVal && checkVarBounds(&structureEl1, 2, cParams.FrameROIr.height / 2);
	retVal = retVal && checkVarBounds(&structureEl2, 2, cParams.FrameROIr.height / 2);
	retVal = retVal && checkVarBounds(&structureEl1, 2, cParams.FrameROIr.width / 2);
	retVal = retVal && checkVarBounds(&structureEl2, 2, cParams.FrameROIr.width / 2);
	retVal = retVal && checkVarBounds(&(cParams.gain),(int) 0, (int)12);
	retVal = retVal && checkVarBounds(&(widthHistory), (int)10, (int)2000); 
	// Need to add some bounds checking for exposure and gain. 
	return retVal;
	
}

bool GeneratorSysParams::checkVarBounds(int *var, int mn, int mx)
{
	bool retVal = true;
	if (*var < mn)
	{
		*var = mn;
		retVal = false;
		
	}
	if (*var > mx)
	{
		*var = mx;
		retVal = false;
	}
	return retVal;
}

bool GeneratorSysParams::checkVarBounds(double *var, double mn, double mx)
{
	bool retVal = true;
	if (*var < mn)
	{
		*var = mn;
		retVal = false;

	}
	if (*var > mx)
	{
		*var = mx;
		retVal = false;
	}
	return retVal;
}