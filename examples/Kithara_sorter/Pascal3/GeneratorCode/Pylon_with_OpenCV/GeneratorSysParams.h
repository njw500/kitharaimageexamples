#pragma once

#ifndef GENERATORSYSPARAMS_C
#define GENERATORSYSPARAMS_C


#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "..\..\Utilities\Utilities\CamParams.h"

// Namespace for using cout.
using namespace std;

#define WGrab  0
#define WHough 1
#define WBack 2
#define WCropped 3
#define WEdges 4
#define WHist 5
#define WOutput 6
#define WDisplayEd 7
#define WDisplaySub 8
#define WDisplaySubN 9



/* A set of system parameters to run the generator function */
class GeneratorSysParams
{
public:
	GeneratorSysParams();
	~GeneratorSysParams();
	bool checkBounds();
	bool GeneratorSysParams::checkVarBounds(int *var, int mn, int mx);
	bool GeneratorSysParams::checkVarBounds(double *var, double min, double max);

	// Video parameters (if using file)
	static const uint c_countOfImagesToGrab = 600;

	// Camera parameters
	CamParams cParams;
	// Whether or not to rotate the frame
	bool DoFrameRotate;
	// If the frame is rotated, then the amount in degrees to rotate the frame by
	float FrameRotate;
	// The ROI that defines a cropped frame (after rotation). 
	cv::Rect FrameROIr;

	// An array of flags as to which display mode is enabled for easy tuning
	bool	DisplayMode[16];
	
	// Video parameters
	cv::String VideoFileName;
	bool VideoIsPreCropped = false;

	// Parameters relating to Nick's method for detection
	float  Zone_Detection_Radius;
	float  AverageDecay;
	double Drop_minDist;
	double Drop_minRadius;
	double Drop_maxRadius;
	int	   MinDropsPerFrame;

	// Define a set of parameters for the droplet detection using morphological operations (FindWidths)
	int roi_y1;				//roi_y1 : The start column that should be selected
	int roi_y2;				// roi_y2 : The end column that should be selected
	int regionPadding;		// padding either side of the peaks to check for whether its a drop or a gap
	int thresholdLevel1;	// The threshold level for DetectDrop_Ed
	double min_pk_distance; // min_pk_dist : The distance which corresponds to the minimum diameter of a drop.
	int thresholdLevel2;	// The threshold level for FindWidths (before the erosion and close operation). 
	int structureEl1;		// The erosion structral element size
	int structureEl2;		// The close structural element size
	int padding_px;			// padding from the coarse droplet results
	int checkDropRowTop;	// The top row which should be used to check the row
	int checkDropRowBottom; // The top row which should be used to check the row
	int widthHistory;		// The number of points over which to take the statistics for the widths.
private:
	int ok;
	

};

#endif