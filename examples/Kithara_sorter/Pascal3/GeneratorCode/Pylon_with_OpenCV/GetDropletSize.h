//#pragma once

#ifndef GETDROPLETSIZE_H
#define GETDROPLETSIZE_H

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "GeneratorSysParams.h"

#include "..\..\Utilities\Utilities\Histogram.h"


// Namespace for using cout.
using namespace std;

class GetDropletSize
{

private:
	int local;

	const int InitialAllocSize = 450;  //c_countOfImagesToGrab
	const int RingBufferLength = 400;

	int RingBufferPtr;
	vector<float>	 Area;
	vector<float>    Width;
	vector<float>    Width2;

	float OutputMean[3];
	float OutputStd[3];

	cv::Mat mAvg;	// Average

	cHistogram Hist1, Hist2, Hist3;

public:
	GetDropletSize();
	~GetDropletSize();

	void ResetVectors();
	void WriteVectors();

	void GetDropletSize::InitializeAverage(cv::Mat cframe, cv::Mat &avg);

	bool Execute(cv::Mat &avg, cv::Mat cframe, const GeneratorSysParams Params);

	bool PlotResults(GeneratorSysParams &Params);

	float GetOutputValue(){ return(OutputMean[1]); };

};

#endif