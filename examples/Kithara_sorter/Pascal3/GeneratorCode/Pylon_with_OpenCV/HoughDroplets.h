#ifndef HOUGHDROPLETS_H
#define HOUGHDROPLETS_H

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "GeneratorSysParams.h"

// Namespace for using OpenCV objects.
//using namespace cv;

// Namespace for using cout.
using namespace std;

int HoughDroplets(cv::Mat data, vector<float> &output, const GeneratorSysParams Params);

void FindCircleEdges(cv::Mat data, cv::Point center, float radius, cv::Vec2i &VertEdges);

#endif