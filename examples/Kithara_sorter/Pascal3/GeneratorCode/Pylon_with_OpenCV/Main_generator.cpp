#include "stdafx.h"
// Pylon_with_OpenCV.cpp


#include "..\..\Utilities\Utilities\UDPSend.h"
#define _WINSOCKAPI_

//#include "io.h"
#include <conio.h>


// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;

#include "..\..\Utilities\Utilities\BaslerCamera.h"

#include "GeneratorSysParams.h"
#include "DebugFlags.h"  
#include "GetDropletSize.h"

#include "..\..\Utilities\Utilities\DisplayIm.h"


int main(int argc, char* argv[])
{
    // The exit code of the sample application.
    int exitCode = 0;
	int UseCamera = 1;	// Cant actually be turned off at the moment
	int FrameCount = 0;
	
	bool CamOK = false;// Status flag for camera

	bool UseFile = false; // Enable/disable toggle for file processing
	bool FileLoaded = false; // Status flag to know of file has been read in

#ifdef EMULATEFROMFILE
	UseFile = true;
#endif

//UseFile = false;

	GeneratorSysParams Params;
	GetDropletSize getDropletSize;
	cv::Mat avg;	// Average
	
	cUDP UDP;
	UDP.Init();
	
	//cUDP UDP2;
	//UDP2.Init(27130,true);

	try
	{
		LARGE_INTEGER Frequency, start, end;

		double freq, diff;
		char string[80];
		char filename[100];

		int NumFrames = Params.c_countOfImagesToGrab;// Use for trip point even when live

		QueryPerformanceFrequency(&Frequency);
		cout << "QueryPerformanceFrequency -> " << _i64toa(Frequency.QuadPart, &string[0], 10) << endl;

		// Create an instant camera object with the camera device found first.

		BaslerCamera  camera;
#ifdef ENABLECAMERA
		CamOK=camera.Init(Params.cParams.CameraSerial, Params.cParams);
		if (CamOK)
		{
			camera.SetExposure(Params.cParams.exp);
			camera.SetGain(Params.cParams.gain);
		}
#else
		CamOK = false;
#endif

		

		// Declare an integer variable to count the number of grabbed images
		// and create image file names with ascending number.
		int grabbedImages = 0;

#ifdef EMULATEFROMFILE
		// This is used IN COMBINATION to the code flag
		VideoCapture vidCap;
		int NumFramesInFile;
		Mat Frames[Params.c_countOfImagesToGrab];
	
		// Create an OpenCV image.
		Mat openCvImage;

			// Open stored video file
			sprintf(filename, "%s", Params.VideoFileName.c_str());

			// Open the input avi file
			vidCap.open(filename);
			NumFramesInFile = vidCap.get(CAP_PROP_FRAME_COUNT);
	
			// Allocate and read in the frames
			// pre-load to avoid this read giving misleading load times

			NumFrames = Params.c_countOfImagesToGrab;

			if (NumFramesInFile < NumFrames)
			{
				cout << "Too few frames" << endl;
				NumFrames = NumFramesInFile;
			}

			cout << "Reading frames" << endl;

			for (int i = 0; i < NumFrames; ++i)
			{
				vidCap.read(Frames[i]);  // Fake a frame from the stored file
				if (Frames[i].empty())
				{
					cerr << "ERROR! blank frame grabbed\n";
					break;
				}
			}

			cout << "Frames read into memory" << endl;
			FileLoaded = true;
	
#endif // Read in from file

		// Other openVCImages
		Mat filtered_frame, mask,frame;

#ifdef recordVideo

		// Create an OpenCV video creator.
		VideoWriter cvVideoCreator;

		// Write out video
		// Define the video file name.
		std::string videoFileName = "v:\\Projects\\Pascal\\openCvVideo.avi";

		// Define the video frame size.
		cv::Size CamFrameSize = Size((int)camera.GetWidth(), (int)camera.GetHeight());

		// Set the codec type and the frame rate. 
		// The frame rate should match or be lower than the camera acquisition frame rate.
		cvVideoCreator.open(videoFileName, CV_FOURCC('D','I','V','X'), 20, CamFrameSize, true);
		//cvVideoCreator.open(videoFileName, CV_FOURCC('M','P','4','2'), 20, frameSize, true); 
#endif

		QueryPerformanceCounter(&start);

		// Make a rotation matrix to align the current generate video
		cv::Point2f pc(camera.GetCurWidth() / 2., camera.GetCurHeight()/2.);
		
		cv::Mat r = cv::getRotationMatrix2D(pc, Params.FrameRotate, 1.0);// Build Rotation matrix for specified rotation
       
		/// Create a Trackbar for user to enter threshold
		//createTrackbar("Min Threshold:", window_name, &lowThreshold, max_lowThreshold, CannyThreshold);

		// Start main grab and process loop
		bool StopLoop = false;

	// Just to be sure
#ifndef ENABLECAMERA
		CamOK = false;
#endif
		bool GotValidFrame = false;

		if ((!CamOK) && (!UseFile))
		{
			cout << "Camera and file input disabled. Giving up" << endl;
			return(false);
		}


		while ( !StopLoop)
        {
            // Wait for an image and then retrieve it. 
			GotValidFrame = false;// Set to false for safety

			if (CamOK)
				camera.GrabNextImg(); // NOT USED OR CONVERTED YET

			cout << fixed << FrameCount << endl;
	
            // Image grabbed successfully?
			if (CamOK && camera.GrabSuccesful())
			{
				Mat camframe;
				int Chf = frame.channels();

				bool ok = camera.GetMatImage(camframe);
				
				frame = camframe.clone();// Make a safe COPY out of volatile camera memory

				if (ok)
				{
					if ((frame.rows < 10) || (frame.cols < 10))
					{
						cout << "Frame is too small from camera ?! " << endl;
						ok = false;
						GotValidFrame = false;
					}
					else
					{
						GotValidFrame = true;
						if (Params.DisplayMode[WGrab])
						{
							Mat out;
							Size osize;
							try
							{
								resize(frame, out, osize, 0.5, 0.5, INTER_LINEAR);
								displayIm("Cam", out);
							}
							catch (InvalidArgumentException e)
							{
								// Get an exception sometimes, not sure why
								cout << "Frame exception: " << e.what() << endl;
							}
						}
					}
				}
			}
			else
			{
				if (CamOK)
					cout << "Failed to grab" << endl;
				// Otherwise the camera is just disconnected and we are in video mode
			}

#ifdef EMULATEFROMFILE
			// SWap in the sorted video frame
			Mat getframe;
			if (UseFile)
			{
				// Swap from stored video frames
				getframe = Frames[FrameCount];
				getframe.copyTo(frame);
				if ((frame.rows > 10) && (frame.cols > 10))
					GotValidFrame = true;
				else
					GotValidFrame = false;
			}
#endif

#ifdef saveImages		
				// Set saveImages to '1' to save images.

					// Create the current image name for saving.
					std::ostringstream s;
					// Create image name files with ascending grabbed image numbers.
					s << "image_" << grabbedImages << ".jpg";
					std::string imageName(s.str());
					// Save an OpenCV image.
					imwrite(imageName, openCvImage);
					grabbedImages++;
#endif
				// Process loop
					if (GotValidFrame)
					{
						// We have a frame worth processing


						//Extract square box from frame
						if (Params.DoFrameRotate) // Avoid this affine rotate if possible
							cv::warpAffine(frame, frame, r, frame.size());

						cv::Mat cframe;

						if (UseFile)
						{
							if (Params.VideoIsPreCropped)
							{
								cframe = frame;// Current data is pre-cropped
								Params.FrameROIr.width = frame.cols;
								Params.FrameROIr.height = frame.rows;
								Params.FrameROIr.x = 1;
								Params.FrameROIr.y = 1;
							}
							else
							{
								cframe = frame(Params.FrameROIr);
							}
							
						}
						else
							cframe = frame;// copy

						if (Params.DisplayMode[WCropped])
							displayIm("CROPPED FRAME", cframe);

						// Cropped images is now GRAYSCALE
						if (FrameCount == 0)
						{
							getDropletSize.InitializeAverage(cframe, avg);
						}
						else
						{
							// Call all 3 methods of size measurment
							bool ok=getDropletSize.Execute(avg, cframe, Params);

							if (ok)
							{
								// Which value to send ? Use (3) for now

								char Cstr[200];

								sprintf(Cstr, "%0.2f\n", getDropletSize.GetOutputValue());
								UDP.SendString(Cstr);
								

								if (FrameCount > 10)
								{
									getDropletSize.PlotResults(Params);
								}
							}
							else
							{
								UDP.SendString("00.00\n");
							}

						}

						// Set recordVideo to '1' to record AVI video file.
#ifdef recordVideo

						cvVideoCreator.write(openCvImage);
						grabbedImages++;
						//cv::accumulate(openCvImage, avgImg);

#endif
					}

				FrameCount = FrameCount + 1;
				if (FrameCount > NumFrames-2)
				{
					FrameCount = 2;// avoid the special conditions
					cout << "Rewind" << endl;
					// NJW FIX AreaCount = 0;
					getDropletSize.WriteVectors();
					getDropletSize.ResetVectors();

					//StopLoop = true;
				}	

#ifdef PYLON_WIN_BUILD
                // Display the grabbed image in pylon.
              ///Pylon::DisplayImage(1, ptrGrabResult);
#endif
                  
			if (_kbhit())
			{
				cout << "ok" << endl; 
				int key = _getch();
				if (key != 224)
				{
					if (key == 'Q')
					{
						StopLoop = true;
					}
		
					if (key == '[')
					{
						float gain;
						uint16_t Exp, Rate;
						camera.GetGainExpRate(gain, Exp, Rate);
						cout << "Current gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;
						Params.cParams.gain = Params.cParams.gain + (float)1;
						camera.SetGain(Params.cParams.gain);
					}

					if (key == ']')
					{
						float gain;
						uint16_t Exp, Rate;
						camera.GetGainExpRate(gain, Exp, Rate);
						cout << "Current Gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;
						Params.cParams.gain = Params.cParams.gain - (float)0.5;

						if (Params.cParams.gain  < 0.0)
							Params.cParams.gain = 0.0;
						camera.SetGain(Params.cParams.gain);
					}
					if (key == 'g')
					{
						// Adjust Ed's threshold (together)
						Params.thresholdLevel1 = Params.thresholdLevel1 - 1;
						Params.thresholdLevel2 = Params.thresholdLevel1;
						cout << endl << "Changed thresholdLevel1 & 2: " << fixed << Params.thresholdLevel1 << endl;

					}
					if (key == 't')
					{
						// Adjust Ed's threshold
						Params.thresholdLevel1 = Params.thresholdLevel1 + 1;
						Params.thresholdLevel2 = Params.thresholdLevel1;
						cout << endl << "Changed thresholdLevel1 & 2: " << fixed << Params.thresholdLevel1 << endl;

					}

					if (key == 'a')
					{
						// Adjust roi_y1
						Params.roi_y1 = Params.roi_y1 - 1;
						cout << endl << "Changed roi_y1: " << fixed << Params.roi_y1 << endl;

					}
					if (key == 'q')
					{
						// Adjust Ed's threshold
						Params.roi_y1 = Params.roi_y1 + 1;
						cout << endl << "Changed roi_y1 " << fixed << Params.roi_y1 << endl;
					}


					if (key == 's')
					{
						// Adjust roi_y1
						Params.roi_y2 = Params.roi_y2 - 1;
						cout << endl << "Changed roi_y1: " << fixed << Params.roi_y2 << endl;

					}
					if (key == 'w')
					{
						// Adjust Ed's threshold
						Params.roi_y2 = Params.roi_y2 + 1;

						cout << endl << "Changed roi_y2 " << fixed << Params.roi_y2 << endl;
					}

					if (key == 'f')
					{
						// Adjust roi_y1
						Params.regionPadding = Params.regionPadding - 1;
						cout << endl << "Changed regionPadding: " << fixed << Params.regionPadding << endl;

					}
					if (key == 'r')
					{
						Params.regionPadding = Params.regionPadding + 1;
						cout << endl << "Changed regionPadding " << fixed << Params.regionPadding << endl;
					}

					if (key == 'd')
					{
						Params.Drop_minRadius = Params.Drop_minRadius + 1;
						cout << endl << "Changed Drop_minRadius: " << fixed << Params.Drop_minRadius << endl;

					}
					if (key == 'c')
					{
						Params.Drop_minRadius = Params.Drop_minRadius - 1;
						cout << endl << "Changed Drop_minRadius " << fixed << Params.Drop_minRadius << endl;
					}
					if (key == 'l')
					{
						Params.structureEl1 = Params.structureEl1 - 1;
						cout << endl << "Changed structureEl1: " << fixed << Params.structureEl1 << endl;
					}
					if (key == 'o')
					{
						Params.structureEl1 = Params.structureEl1 + 1;
						cout << endl << "Changed structureEl1 " << fixed << Params.structureEl1 << endl;
					}

					if (key == 'k')
					{
						Params.structureEl2 = Params.structureEl2 - 1;
						cout << endl << "Changed Drop_minRadius: " << fixed << Params.structureEl2 << endl;
					}
					if (key == 'i')
					{
						Params.structureEl2 = Params.structureEl2 + 1;
						cout << endl << "Changed structureEl1 " << fixed << Params.structureEl2 << endl;
					}
					if (key == '.')
					{
						Params.padding_px = Params.padding_px - 1;
						cout << endl << "Changed padding_px: " << fixed << Params.padding_px << endl;
					}
					if (key == 'l')
					{
						Params.padding_px = Params.padding_px + 1;
						cout << endl << "Changed padding_px " << fixed << Params.padding_px << endl;
					}
					if (key == 'j')
					{
						Params.checkDropRowTop = Params.checkDropRowTop - 1;
						cout << endl << "Changed checkDropRowTop: " << fixed << Params.checkDropRowTop << endl;
					}
					if (key == 'm')
					{
						Params.checkDropRowTop = Params.checkDropRowTop + 1;
						cout << endl << "Changed checkDropRowTop " << fixed << Params.checkDropRowTop << endl;
					}
					if (key == 'n')
					{
						Params.checkDropRowBottom = Params.checkDropRowBottom - 1;
						cout << endl << "Changed checkDropRowBottom: " << fixed << Params.checkDropRowBottom << endl;
					}
					if (key == 'h')
					{
						Params.checkDropRowBottom = Params.checkDropRowBottom + 1;
						cout << endl << "Changed checkDropRowBottom " << fixed << Params.checkDropRowBottom << endl;
					}

					if (key == '7')
					{
						Params.checkDropRowBottom = Params.checkDropRowBottom - 1;
						cout << endl << "Changed checkDropRowBottom: " << fixed << Params.checkDropRowBottom << endl;
					}
					if (key == '8')
					{
						Params.checkDropRowBottom = Params.checkDropRowBottom + 1;
						cout << endl << "Changed checkDropRowBottom " << fixed << Params.checkDropRowBottom << endl;
					}

					/*
					if (key == 'e')
					{
					if (Params.DisplayMode[WDisplayEd])
					Params.DisplayMode[WDisplayEd] = false;
					else
					Params.DisplayMode[WDisplayEd] = true;
					}

					if (key == 'g')// Grab window
					{
					bool flag = true;
					if (Params.DisplayMode[WGrab])
					flag = false;
					Params.DisplayMode[WGrab] = flag; // Display this window   WOutput
					}

					if (key == 'a')
					{
					bool flag=true;
					if (Params.DisplayMode[WGrab])
					flag = false;
					Params.DisplayMode[WGrab] = flag; // Display this window   WOutput
					Params.DisplayMode[WHough] = flag;
					Params.DisplayMode[WBack] = flag;
					Params.DisplayMode[WCropped] = flag;
					Params.DisplayMode[WEdges] = flag;
					Params.DisplayMode[WHist] = true;
					Params.DisplayMode[WOutput] = flag;
					Params.DisplayMode[WDisplayEd] = flag;
					}

					*/ // Commented for now, will sort this out later. 
				}
				else
				{
					int key2 = _getch();
						
					if (key2 == 72) // Up
					{
#ifdef ENABLECAMERA
						camera.RoiVert(1,-1);
						Rect Cur = camera.GetROI();
						cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
						// Update params accordingly. 
						Params.FrameROIr.width = Cur.width;
						Params.FrameROIr.height = Cur.height;
#endif
						
#ifdef EMULATEFROMFILE
						
						Params.FrameROIr.x = Params.FrameROIr.x - 1;
						cout << "ROI Up: X " << Params.FrameROIr.x << endl; 
						// Update params accordingly. 
#endif

					}
					if (key2 == 80) // Down
					{
#ifdef ENABLECAMERA

						camera.RoiVert(1, +1);

						Rect Cur = camera.GetROI();
						cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
						// Update params accordingly. 
						Params.FrameROIr.width = Cur.width;
						Params.FrameROIr.height = Cur.height;
						
#endif
#ifdef EMULATEFROMFILE
						
						Params.FrameROIr.x = Params.FrameROIr.x + 1;
						bool check = Params.checkVarBounds(&(Params.FrameROIr.x),1,frame.cols);
						cout << "ROI: Down " << Params.FrameROIr.x << endl;
#endif


					}
				/*	if (key2 == 'v')
					{
						if (UseFile)
							UseFile = false;
						else
							UseFile = true;
						FrameCount = 0;
						getDropletSize.ResetVectors();
					}
					if (key2 == '?')
					{
						double val = camera.GetRealFrameRate();
						cout << " Frame rate: " << fixed << val << endl << endl;
					}
					if (key2 == 'p')
					{
						Rect Cur = camera.GetROI();
						cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
						cerr << endl << "Press Enter to continue." << endl;
						while (cin.get() != '\n');*/
						
				}
				bool changeOk = Params.checkBounds();
				if (!changeOk)
				{
					cout << "Value was out of bounds." << endl;
				}
			}
			
        }

		QueryPerformanceCounter(&end);

		diff = (double)(end.QuadPart - start.QuadPart);
		freq = (double)Frequency.QuadPart;
		diff = diff / freq;
		cout << "end - start -> " << _i64toa(end.QuadPart - start.QuadPart, &string[0], 10) << endl;

		printf("%d frames in %g sec, rate %g\n", grabbedImages, diff,((double)Params.c_countOfImagesToGrab)/diff);

		cout << "Num frames" << _i64toa_s(grabbedImages,&string[0],30,10) << endl;

		// Release the video file on leaving.
#ifdef recordVideo
		
			cvVideoCreator.release();
#endif
    }
    catch (GenICam::GenericException &e)
    {
        // Error handling.
        cerr << "An exception occurred." << endl
        << e.GetDescription() << endl;
        exitCode = 1;
		cerr << endl << "Press Enter to exit." << endl;
		while (cin.get() != '\n');
    }

    // Comment the following two lines to disable waiting on exit.
    cerr << endl << "Press Enter to exit." << endl;
    while( cin.get() != '\n');

    return exitCode;
}
