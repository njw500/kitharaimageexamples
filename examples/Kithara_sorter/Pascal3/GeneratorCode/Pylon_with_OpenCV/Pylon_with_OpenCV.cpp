// Pylon_with_OpenCV.cpp

/*
    

    The CInstantCamera class uses a pool of buffers to retrieve image data
    from the camera device. Once a buffer is filled and ready,
    the buffer can be retrieved from the camera object for processing. The buffer
    and additional image data are collected in a grab result. The grab result is
    held by a smart pointer after retrieval. The buffer is automatically reused
    when explicitly released or when the smart pointer object is destroyed.
*/



#include "stdafx.h"
//#include "io.h"
#include <conio.h>

#include <algorithm>
#include <vector>
#include <numeric>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;

#include "BaslerCamera.h"
#include "get_droplet_contour.h"
#include "SysParams.h"
#include "DebugFlags.h"  // Control for diagnostics and output files
#include "HoughDroplets.h"
#include "WriteVector.h"
#include "DropDetection.hpp"



int main(int argc, char* argv[])
{
    // The exit code of the sample application.
    int exitCode = 0;
	int UseCamera = 1;	// Cant actually be turned off at the moment
	int FrameCount = 0;
	
	bool CamOK = false;

	SysParams Params;

	cv::Mat avgImg; 	

	try
	{
		LARGE_INTEGER Frequency, start, end;

		double freq, diff;
		char string[80];
		char filename[100];

		QueryPerformanceFrequency(&Frequency);
		cout << "QueryPerformanceFrequency -> " << _i64toa(Frequency.QuadPart, &string[0], 10) << endl;

		// Create an instant camera object with the camera device found first.

		BaslerCamera  camera;

		CamOK=camera.Init(false, "", CAM_FREE_MODE); // Get first camera available

		if (CamOK)
			camera.SetGain(0.0);

		/*
		// 
	
		camera.ExposureTime.SetValue(82.0);
		camera.Gain.SetValue(12.0);
		camera.CounterReset();
		*/

		//avgImg.create(camera.GetWidth(), camera.GetHeight(), CV_32FC3);


		// Create a pylon ImageFormatConverter object.
		//CImageFormatConverter formatConverter;

		// Specify the output pixel format.
		//formatConverter.OutputPixelFormat = PixelType_BGR8packed;

		// Create a PylonImage that will be used to create OpenCV images later.
		CPylonImage pylonImage;

		// Declare an integer variable to count the number of grabbed images
		// and create image file names with ascending number.
		int grabbedImages = 0;

		// Create an OpenCV video creator.
		VideoWriter cvVideoCreator;

		// Open stored video file
		sprintf(filename, "%s",Params.VideoFileName.c_str());
		
		VideoCapture vidCap;

		// Open the avi file
		vidCap.open(filename);
		int NumFramesInFile = vidCap.get(CAP_PROP_FRAME_COUNT);

		// Create an OpenCV image.
		Mat openCvImage;

		// Allocate and read in the frames
		// pre-load to avoid this read giving misleading load times

		Mat Frames[Params.c_countOfImagesToGrab];
		int NumFrames = Params.c_countOfImagesToGrab;

		if (NumFramesInFile < NumFrames)
		{
			cout << "Too few frames" << endl;
			NumFrames = NumFramesInFile;
		}

		cout << "Reading frames" << endl;

		for (int i = 0; i < NumFrames; ++i)
		{
			vidCap.read(Frames[i]);  // Fake a frame from the stored file
			if (Frames[i].empty())
			{
				cerr << "ERROR! blank frame grabbed\n";
				break;
			}
		}

		cout << "Frames read into memory" << endl;

		// Other openVCImages
		Mat filtered_frame, mask,frame;

#ifdef recordVideo
		// Write out video

		// Define the video file name.
		std::string videoFileName = "v:\\Projects\\Pascal\\openCvVideo.avi";

		// Define the video frame size.
		cv::Size CamFrameSize = Size((int)camera.GetWidth(), (int)camera.GetHeight());

		// Set the codec type and the frame rate. 
		// The frame rate should match or be lower than the camera acquisition frame rate.
		cvVideoCreator.open(videoFileName, CV_FOURCC('D','I','V','X'), 20, CamFrameSize, true);
		//cvVideoCreator.open(videoFileName, CV_FOURCC('M','P','4','2'), 20, frameSize, true); 
#endif

		QueryPerformanceCounter(&start);

        // This smart pointer will receive the grab result data.
        CGrabResultPtr ptrGrabResult;

		// Make a rotation matrix to align the current generate video
		cv::Point2f pc(camera.GetWidth() / 2., camera.GetHeight()/2.);
		// Rotation matrix for -1.3 degrees
		cv::Mat r = cv::getRotationMatrix2D(pc, Params.FrameRotate, 1.0);
       
		cv::Mat avg;

		float Edge_thres1 = Params.CannyThres1;
		float Edge_thres2 = Params.CannyThres2;
		
		vector<float>	 Area(NumFrames * 30, 0);
		vector<float>    Width;

		vector<float>	 Area2(NumFrames * 30, 0);
		vector<float>    widths2;

		Width.reserve(NumFrames * 30);
		widths2.reserve(NumFrames * 30);

		int AreaCount = 0;
		int AreaCount2 = 0;
		/// Create a Trackbar for user to enter threshold
		//createTrackbar("Min Threshold:", window_name, &lowThreshold, max_lowThreshold, CannyThreshold);

		// Start main grab and process loop
		bool StopLoop = false;
	
		while ( !StopLoop)
        {
            // Wait for an image and then retrieve it. 
			if (CamOK)
				camera.GrabNextImg(); // NOT USED OR CONVERTED YET

			cout << fixed << FrameCount << endl;
		
			int Chf = frame.channels();

            // Image grabbed successfully?
			if (CamOK && camera.GrabSuccesful())
			{

				Mat camframe;
				bool ok = camera.GetMatImage(camframe);
				
				frame = camframe.clone();// Make a safe COPY out of volatile camera memory
				if (ok)
				{
					int r, c, ty;
					r = frame.rows;
					c = frame.cols;

				
					if (Params.DisplayMode[WGrab])
					{
						Mat out;
						Size osize;
						displayIm("Rawx", frame);
						try
						{
							resize(frame, out, osize, 0.5, 0.5, INTER_LINEAR);

							imshow("Cam", out);
						}
						catch (InvalidArgumentException e)
						{
							// Get an exception sometimes, not sure why
							cout << "Frame exception: " << e.what() << endl;

						}
					}
				}
			}
			else
			{
				if (CamOK)
					cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
				// Otherwise the camera is just disconnected and we are in video mode
			}

			Mat getframe;
			getframe = Frames[FrameCount];
			getframe.copyTo(frame);

			
				// Set saveImages to '1' to save images.
				if (saveImages)
				{
					// Create the current image name for saving.
					std::ostringstream s;
					// Create image name files with ascending grabbed image numbers.
					s << "image_" << grabbedImages << ".jpg";
					std::string imageName(s.str());
					// Save an OpenCV image.
					imwrite(imageName, openCvImage);
					grabbedImages++;
				}

				// Process loop

				//Extract square box from frame

				cv::warpAffine(frame, frame, r, frame.size());
				//imshow("Rotated", frame);

				cv::Mat cframe = frame(Rect(Params.Frame_ROI_Left, Params.Frame_ROI_Top,
									Params.Frame_ROI_Width, Params.Frame_ROI_Height));

				int Fr = cframe.rows;
				int Fc = cframe.cols;

#ifdef FDBG
				//imwrite("Cropped.bmp", cframe);
				waitKey(1);
#endif

				if (Params.DisplayMode[WCropped])
					imshow("CROPPED FRAME", cframe);

				int ch0 = cframe.channels();

				//cvtColor(croppedImage, croppedImage, COLOR_BGR2GRAY);

				// Cropped images is now GRAYSCALE

				//int ch1 = croppedImage.channels();

				if (FrameCount == 0)
				{
					avg = Mat(Fr, Fc, CV_32F);
					//avg.setTo(0.0);
					cvtColor(cframe, frame, COLOR_BGR2GRAY);
					frame.convertTo(avg, CV_32F);
				}
				else
				{
					// Make a rolling average

					//int Type0 = cframe.type();
					//int Type1 = avg.type();

					Mat ffcrop;

					// Convert BGR frame to GRAY frame
					cvtColor(cframe, frame, COLOR_BGR2GRAY);

					frame.convertTo(ffcrop, CV_32F);// Promote current crop to float

					accumulateWeighted(ffcrop, avg, Params.AverageDecay); // Add float crop into the average


					double minVal, maxVal;
					minMaxLoc(avg, &minVal, &maxVal);
					printf("Max %f Min %f\n", maxVal, minVal);

					cv::Mat background;

					convertScaleAbs(avg, background);// Take ABS value and scale to 8 bit

					//int Ty = background.type();
					//int Ch = background.channels();

					//cvtColor(background, background, COLOR_BGR2GRAY); // NOT NEEDED, ALREADY CORRECT TYPE
					//imwrite("ABSBackground.jpg", background);

				

					if (Params.DisplayMode[WBack])
						displayIm("Background", background);

					// Call the finder
				
					Mat kernel;
					kernel = kernel.ones(Size(5, 5), CV_8U);

					Mat OutputImage;
					vector<vector<Point> >Final;
	
					get_droplet_contour(background, frame, OutputImage, Final, 
						Params.CannyThres1, Params.CannyThres2,
						Params.Zone_Detection_Radius, kernel,Params);

					namedWindow("Frame1", WINDOW_AUTOSIZE);
					imshow("Frame1", frame);
	
					// Test droplet finding code using morphological operations
					int numDrops;
					Mat frameDisp;
					FindDropWidths(frame, widths2, &numDrops,
						&frameDisp,	Params.roi_y1, Params.roi_y2,
						Params.top, Params.thresholdLevel1,	Params.min_pk_distance,
						Params.thresholdLevel2,	Params.structureEl1, Params.structureEl2,
						Params.padding_px);
						
					// Get historgram and stats
					float MeanWidth2, WidthStd2;
					float mn2, mx2;
					MeanWidth2 = GetHistogram(widths2, widths2.size(), mn2, mx2, WidthStd2, "Width2");
					
					// Add text
					char Cstr[200];
					String str;
					sprintf(Cstr, "Width: %0.2f  std: %0.2f", MeanWidth2, WidthStd2);
					str = Cstr;
					putText(frameDisp, str, Point(50, 50), FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0));
					namedWindow("Frame2", WINDOW_AUTOSIZE);
					imshow("Frame2", frameDisp);


					//displayIm("FinalDiag", OutputImage);
					//cframe.convertTo(cframe, CV_8UC3);
					drawContours(cframe, Final,-1,(0,255,0),3);

//					char Cstr[200];
				//	String str;
					if (FrameCount > 10)
					{
						for (int j = 0; j < Final.size(); ++j)
						{
							float curarea = contourArea(Final[j]);
							//vArea.at(AreaCount) = curarea;
							Area[AreaCount++] = curarea;
						}

						float MeanArea, mn, mx, AreaStd;
						if (AreaCount > 30)
						{
							//Writevector(Area, AreaCount, "Area.csv");
							MeanArea = GetHistogram(Area, AreaCount, mn, mx, AreaStd,"Area");						
						}

						

						// Try 2nd approach

						cv::Mat fgmask;
						// Calculate this again
						subtract(background, frame, fgmask);
						Mat tmp;
						ScaleImage(fgmask, tmp);

						vector<float> CurWidths;
						CurWidths.reserve(20);

						int NumValid;

						HoughDroplets(tmp, CurWidths,Params);

						Width.insert(Width.end(), CurWidths.begin(), CurWidths.end());

						float MeanWidth, WidthStd;
						MeanWidth = GetHistogram(Width, Width.size(), mn, mx, WidthStd, "Width");


		

						sprintf(Cstr, "Area: %0.0f  std:  %0.2f  2nd W: %0.2f  std: %0.2f", MeanArea, AreaStd, MeanWidth,WidthStd);
						str = Cstr;

						putText(cframe, str, Point(50, 50), FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0));

						//displayIm("Output", cframe);
						imshow("Output", cframe);
						waitKey(1);
					}

				}
				bool Detected = false;
				bool CellDetected = false;

				int DropLocation[2];

			





				// Set recordVideo to '1' to record AVI video file.
#ifdef recordVideo

				cvVideoCreator.write(openCvImage);
				grabbedImages++;
				//cv::accumulate(openCvImage, avgImg);

#endif
				FrameCount = FrameCount + 1;
				if (FrameCount > NumFrames-2)
				{
					FrameCount = 2;// avoid the special conditions
					cout << "Rewind" << endl;
					AreaCount = 0;

					StopLoop = true;
				}	
				/*
				if(Detected)
				{
				// Create an OpenCV display window.
				namedWindow("OpenCV Display Window", CV_WINDOW_NORMAL); // other options: CV_AUTOSIZE, CV_FREERATIO

				// Display the current image in the OpenCV display window.
				imshow("OpenCV Display Window", croppedImage);// openCvImage);
				// Define a timeout for customer's input in ms.
				// '0' means indefinite, i.e. the next image will be displayed after closing the window. 
				// '1' means live stream
				
				}
				waitKey(1);
				*/
#ifdef PYLON_WIN_BUILD
                // Display the grabbed image in pylon.
              ///Pylon::DisplayImage(1, ptrGrabResult);
#endif
            
          

			if (_kbhit())
			{
				cout << "ok" << endl; 
				int key = _getch();
				if (key == 'q')
				{
					StopLoop = true;
				}
				if (key == ',')
				{
					Edge_thres2 = Edge_thres2 - 1;
					cout << endl << "Changed Edge2:  " << fixed << Edge_thres2 << endl << endl;
				}
				if (key == '.')
				{
					Edge_thres2 = Edge_thres2 + 1;
					cout << endl << "Changed Edge2:  " << fixed << Edge_thres2 << endl;
				}
				if (key == '<')
				{
					Edge_thres1 = Edge_thres1 - 1;
					cout << endl << "Changed Edge1:  " << fixed << Edge_thres1 << endl << endl;
				}
				if (key == '>')
				{
					Edge_thres1 = Edge_thres1 + 1;
					cout << endl << "Changed Edge1:  " << fixed << Edge_thres1 << endl;
				}
			}
        }

		QueryPerformanceCounter(&end);

		diff = (double)(end.QuadPart - start.QuadPart);
		freq = (double)Frequency.QuadPart;
		diff = diff / freq;
		cout << "end - start -> " << _i64toa(end.QuadPart - start.QuadPart, &string[0], 10) << endl;

		printf("%d frames in %g sec, rate %g\n", grabbedImages, diff,((double)Params.c_countOfImagesToGrab)/diff);

		cout << "Num frames" << _i64toa_s(grabbedImages,&string[0],30,10) << endl;

		// Release the video file on leaving.
#ifdef recordVideo
		
			cvVideoCreator.release();
#endif
    }
    catch (GenICam::GenericException &e)
    {
        // Error handling.
        cerr << "An exception occurred." << endl
        << e.GetDescription() << endl;
        exitCode = 1;
		cerr << endl << "Press Enter to exit." << endl;
		while (cin.get() != '\n');
    }

    // Comment the following two lines to disable waiting on exit.
    cerr << endl << "Press Enter to exit." << endl;
    while( cin.get() != '\n');

    return exitCode;
}
