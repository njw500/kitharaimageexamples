#include "stdafx.h"


#include "get_droplet_contour.h"

#include "DebugFlags.h"


#include "..\..\Utilities\Utilities\DisplayIm.h"

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;

// This was originally Duncan's Python code

int get_droplet_contour(Mat background, Mat frame, Mat &OutputImage, vector<vector<Point> > &output, float threshold1, float threshold2,
	float detection_radius, Mat kernel, GeneratorSysParams Params)
{
	//Detect bubbles against a background frame using complexhull() and complexhull_threshold().
	//Return the foreground mask and the countours'''
	int NumDropletsFound = 0;

	// Find forground mask by subtraction
	cv::Mat fgmask;

	subtract(background, frame, fgmask);

	int Ty = fgmask.type();

	if(Params.DisplayMode[WDisplaySub])
		displayIm("Subtract", fgmask,1);

	// Get rid of some dark noise
	GaussianBlur(fgmask, fgmask, Size(3, 3), 3.);

	//displayIm("Blur", fgmask,1);

	// Use Canny detection for first round of contour finding)
	vector<vector<Point> > contours;// Which are actually hulls
	bool OK=complexhull(fgmask, contours, threshold1, threshold2,Params);

	if ((!OK) && (contours.size() < (Params.MinDropsPerFrame * 3))) // Expect quite a large number of contours at this point
	{
		return(0);
	}

	// Does the length of contours tell us anything useful [75]  0

#ifdef EdgeOnly
	// Use to run code in Edge only mode for parameter setting
	return;
#endif

	// Output the drawn contours to the mask
	drawContours(fgmask, contours, -1, 255, -1);

	//displayIm("DrawContours1 ", fgmask, 1);
	

	// join contours up by repeating convex hull operation on the mask and writing thr contours into the mask
	vector<vector<Point> > contours2;// Which are actually hulls

	for(int i = 1; i <= 2; ++i)
	{
		complexhull_threshold(fgmask,contours2);
		// Add these contours into the mask 
		drawContours(fgmask, contours2, -1, 255, -1);
	}

	// Does the length of contours tell us anything useful here  [35]  0

	if (contours.size() < (Params.MinDropsPerFrame )) // Not too sure what the limit multiplier is here
	{
		return(0);
	}

	//displayIm("DrawContours2", fgmask,1);

	// Remove small features

	morphologyEx(fgmask, fgmask, MORPH_OPEN, kernel);
	morphologyEx(fgmask, fgmask, MORPH_CLOSE, kernel);

	//displayIm("Morph", fgmask, 1);
	// Find contours again
	complexhull_threshold(fgmask, contours);

	if (contours.size() < (Params.MinDropsPerFrame)) // Not too sure what the limit multiplier is here
	{
		return(0);
	}

	// Filter the contours to be in area of interest and correct parameters
	//vector<vector<Point> > output;

	contours_within_radius(contours, output,detection_radius, frame);
	
	OutputImage = fgmask;
	//contours = contours_within_radius(contours, detection_radius, frame)
		//return fgmask, contour
		
	return(contours.size());
}


//Create convex hulls using Canny edge detection
bool complexhull(Mat greyframe, vector<vector<Point> > &Hull, const float threshold1, const float threshold2, GeneratorSysParams Params)
{

	Mat edges;
	Canny(greyframe, edges, threshold1, threshold2);

	if(edges.rows < 1)
	{
		cout << "No edges " << endl;
		return(false);
	}

	vector<vector<Point> > contours;
	vector<Vec4i> heirarchy;

	if (Params.DisplayMode[WEdges])
		displayIm("Edges", edges);


	// Find contours
	findContours(edges, contours, heirarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE, Point(0, 0));

	// Diag draw
	/*
	Mat drawIm;
	RNG rng(12345);
	edges.convertTo(drawIm, CV_8UC3);
	for (int i = 0; i< contours.size(); i++)
	{
		Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		drawContours(drawIm, contours, i, color, 2, 8, heirarchy, 0, Point());
	}
	imshow("ContourDiag1", drawIm);
	*/

	int num = (int)contours.size();

	if (num < 1)
	{
		return(false);
	}


	vector<Point> cnt;
	vector<vector<Point> > hull(contours.size());

	for (int i = 0; i < num; ++i)
	{
		cnt = contours[i];
		convexHull(cnt, hull[i],false);
	}
	Hull = hull;

	// Output Vector is in hull
	// Don't bother returning the hierarchy
	return(true);// contours
}


void complexhull_threshold(Mat greyframe, vector<vector<Point> > &Hull)
{
	//Create convex hulls with a simple threshold.
	// Requires a solid black and white frame as input.

	Mat edges;  // Actually this is a binary and not tru edges
	double ret = threshold(greyframe, edges, 127, 255, THRESH_BINARY);

	//displayIm("Thresholded", edges,1);


	vector<vector<Point> > contours;
	vector<Vec4i> heirarchy;
	
	findContours(edges, contours, heirarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);


	/*
	Mat drawIm;
	RNG rng(12345);
	edges.convertTo(drawIm, CV_8UC3);
	for (int i = 0; i< contours.size(); i++)
	{
	Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
	drawContours(drawIm, contours, i, color, 2, 8, heirarchy, 0, Point());
	}
	imshow("complexhull_thresholdDiag", drawIm);
	*/


	int num = (int)contours.size();
	vector<Point> cnt;
	vector<vector<Point> > hull(contours.size());

	for (int i = 0; i < num; ++i)
	{
		cnt = contours[i];
		convexHull(cnt, hull[i], false);	
	}
	Hull = hull;
	return;
}


void contours_within_radius(vector<vector<Point> >contours, vector<vector<Point> > &output, const float detection_radius, Mat frame)
{
	//  Use moments to remove contours outside of centre radius'''
	//	center_contours = []
	//  Iterate through list of contours.
	int num = contours.size();

	vector<Point> cnt;
	Moments M;
	int ValidPointCount = 0;
	vector<vector<Point> >centres(num);

	for (int i = 0; i < num; ++i)
	{
		//for cnt in contours :
		cnt = contours[i];

		M = moments(cnt);

		// check that a center moment exists
		if (M.m00 != 0.0)
		{

			// Centroid position cx and cy
			double cx = (M.m10 / M.m00);
			double cy = (M.m01 / M.m00);

			// Delete contours not near center of frame
			double x0 = frame.cols / 2;
			double y0 = frame.rows / 2;

			// See if the center falls within a circle.
			double loc = pow(y0 - cy, 2.0) + pow(x0 - cx, 2.0);

			float mx_frame = (float)frame.rows;
			if (frame.cols > frame.rows)
			{
				mx_frame = (float)frame.cols;
			}
			double limit_rad = pow(detection_radius * mx_frame, 2.0);
			// This could probably be tighter but re-visit later
			if (loc < limit_rad)
			{
				
				centres[ValidPointCount] = cnt;
				++ValidPointCount;
				//center_contours.append(cnt);
				//contours = center_contours;
			}
		}
	}
		vector<vector<Point> >Out(ValidPointCount);
		for (int i = 0; i < ValidPointCount; ++i)
		{
			Out[i] = centres[i];
		}
		output = Out;
		
	
}