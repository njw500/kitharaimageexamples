#include "stdafx.h"
#include "CellDetector.h"
#include "DropDetection.h"
#include "..\Utilities\Utilities\DisplayIm.h"

using namespace cv;

void CellDetector::updateParams(int roiY1_in, int roiY2_in, int topBottomBorder_px_in, int thresholdLevelDrop_in,
	int minPkDist_in, int cellPadding_in, int openSize_in, int erodeSize_in, int thresholdLevelCell_in, int checkDropRowTop_in, 
	int checkDropRowBottom_in)
{
	roiY1 = roiY1_in;
	roiY2 = roiY2_in;
	topBottomBorder_px = topBottomBorder_px_in;
	thresholdLevelDrop = thresholdLevelDrop_in;
	thresholdLevelCell = thresholdLevelCell_in;
	cellPadding = cellPadding_in;
	minPkDist = minPkDist_in;
	openSize = openSize_in;
	erodeSize = erodeSize_in;

	checkDropRowTop = checkDropRowTop_in;
	checkDropRowBottom = checkDropRowBottom_in;
}

CellDetector::CellDetector(int roiY1_in, int roiY2_in, int topBottomBorder_px_in, int thresholdLevelDrop_in,
	int minPkDist_in, int cellPadding_in, int openSize_in, int erodeSize_in, int thresholdLevelCell_in, 
	int checkDropRowTop_in,	int checkDropRowBottom_in)
/* Initialise the cell detector and set its parameters accordingly */
{
	roiY1 = roiY1_in;
	roiY2 = roiY2_in;
	topBottomBorder_px = topBottomBorder_px_in;
	thresholdLevelDrop = thresholdLevelDrop_in;
	thresholdLevelCell = thresholdLevelCell_in;
	cellPadding = cellPadding_in;
	minPkDist = minPkDist_in;
	openSize = openSize_in;
	erodeSize = erodeSize_in;
	checkDropRowTop = checkDropRowTop_in;
	checkDropRowBottom = checkDropRowBottom_in;


	counter = 0;
	ValidAverage = false;

	//mog3 = cv::createBackgroundSubtractorMOG2(numberBackgroundSubtractions, 30, false);
}


CellDetector::~CellDetector()
{
}

void CellDetector::Init()
{
	ValidAverage = false;
	counter = 0;
}

// Caluclate the background frame
void CellDetector::BuildBackground()
{
	//mog3->apply(*frame, background);
	// Note, hard coded decay average for now!
	accumulateWeighted(*frame, avg, 0.1f);

}

bool CellDetector::updateFrame(cv::Mat *newFrame)
{
	
	
	dropDetected = false;
	// Quick check to make sure the parameters were sensible
	if (newFrame->rows - topBottomBorder_px < roiY2)
	{
		cout << "Frame rows less the top border parameter is greater than the roiY2 parameter. Check image size and parameter files" << endl;
		return false;
	}
	// Copy internal pointer
	frame = newFrame;

	Mat subtracted;
	frame->copyTo(subtracted);

	frame->convertTo(*frame, CV_32F);

	if (ValidAverage)
	{
		BuildBackground();
		//imshow("background", avg);
	
		Mat background;
		convertScaleAbs(avg, background);
	
		subtract(background, subtracted, subtracted);
		
		subtracted.convertTo(subtracted, CV_8U);

		//subtracted.copyTo(subtracted;
		bitwise_not(subtracted, subtracted);

		//imshow("frame", subtracted);
		frame->convertTo(*frame, CV_8U);

		// Call drop detection function
		/*dropDetected = DetectDrop_Ed(subtracted, roiY1, roiY2, topBottomBorder_px,
			thresholdLevelDrop, minPkDist, dropLocation, counter);*/
		int numDrops;

		Mat temp;
		//imshow("Sub", subtracted);

		threshold(subtracted, subtracted,240, 255, THRESH_BINARY_INV);
		//imshow("Threshold Sub", subtracted);
		
		frame->copyTo(temp,subtracted);
		//imshow("Temp", temp);
		
		threshold(temp, temp, 1, 255, THRESH_BINARY_INV);
		//imshow("Temp2", temp);
		
		dropDetected = DetectDrop_Ed2(temp, roiY1, roiY2,
			topBottomBorder_px, thresholdLevelDrop, checkDropRowTop,
			checkDropRowBottom, minPkDist, dropLocation2, &numDrops);

		// If a drop was detected, search for a cell
		if (dropDetected)
		{
			dropLocation[0] = dropLocation2[0];
			dropLocation[1] = dropLocation2[1];
			// Create an image containing only the detected drop and mask outside of the drop radius
			CreateDropRoi(temp, dropRoi, dropLocation, cellPadding, roiX, roiY);
			//imshow("dropROI", dropRoi);
			dropContour.clear();
			bool isOK = CreateDropMask(dropRoi, dropMask, dropCentre, 
				dropRadius, thresholdLevelDrop,dropContour);

			if (!isOK)
			{
				// Invalidate search
				cellDetected = false;
			}
			else
			{
				//CreateDropRoi(*frame, dropRoi, dropLocation, cellPadding, roiX, roiY);
				cellDetected = DetectCells_Ed(dropRoi, dropMask, cellLocation,
					openSize, erodeSize, thresholdLevelCell, 1);
				// displayIm("CROPPED FRAME", croppedImageBackup);
			}
		}
	}
	else // Not ValidAverage yet
	{
		if (counter == 0)
		{
			// Initialize average
			avg = Mat(frame->rows, frame->cols, CV_32F);
			frame->copyTo(avg);
			avg.convertTo(avg, CV_32F);
		}
		else
		{
			// Continue building average
			BuildBackground();

			// Have we got enought to be active
			if (counter > numberBackgroundSubtractions)
				ValidAverage = true;
			
		}
	}

	counter = counter + 1;

	return true;

}

void CellDetector::plotResult(cv::String plotName, cv::Mat frame, int left, int top)
{
	if (dropDetected)
	{
		Scalar colour;
		cv::Mat disp;

		if (cellDetected)
		{
			colour = Scalar(0, 255, 0);
		}
		else
		{
			colour = Scalar(255, 0, 0);
		}

		cvtColor(dropRoi, disp, COLOR_GRAY2BGR);

		//circle(disp, Point(dropCentre[0], dropCentre[1]), (int)dropRadius, colour);
		vector <vector<Point>> contourList;
		contourList.push_back(dropContour);
		drawContours(disp, contourList, 0, colour);
		int cellRadius = (int)(dropRadius / 9);
		/*if (CellDetected)
		{
		circle(disp, Point(CellLocation[0], CellLocation[1]), CellRadius, Colour);
		}*/
		if (cellDetected)
		{
			Scalar colour2 = Scalar(0, 0, 255);
			circle(disp, Point(cellLocation[0], cellLocation[1]), cellRadius, colour2);
		}



		displayIm(plotName, disp);

		cvtColor(frame, frame, COLOR_GRAY2BGR);

		/* Old code */
		if (cellDetected)
		{
			Scalar colour2 = Scalar(0, 255, 0);
			circle(frame, Point(cellLocation[0] + roiX, cellLocation[1] + roiY), cellRadius, colour2);
			colour = Scalar(0, 255, 0);

		}
		else
		{
			colour = Scalar(0, 0, 255);
		}
		//circle(frame, Point(dropCentre[0] + left + roiX, dropCentre[1] + top + roiY), dropRadius, colour);
		
		drawContours(frame, contourList, 0, colour, 1, 8, noArray(), 2147483647, Point( roiX, roiY));
		//drawContours(frame, contourList, 0, colour);

		line(frame, Point(dropLocation[0], 1), Point(dropLocation[0], frame.cols),
			Scalar(255, 255, 255), 1, 8);
		line(frame, Point(dropLocation[1], 1), Point(dropLocation[1], frame.cols),
			Scalar(255, 255, 255), 1, 8);
		//line(disp, Point(dropLocation[0], 1), Point(dropLocation[0] + left + roiX, disp->rows + top + roiY), Scalar(255, 255, 255), 1, 1);
	}
	// Also plot the roi 
	line(frame, Point(1, roiY1), Point(frame.cols, roiY1), Scalar(255, 255, 255), 1, 8);
	line(frame, Point(1, roiY2), Point(frame.cols, roiY2), Scalar(255, 255, 255), 1, 8);

	// Also plot the roi 
	line(frame, Point(1, checkDropRowTop), Point(frame.cols, checkDropRowTop), Scalar(50, 255, 255), 1, 8);
	line(frame, Point(1, checkDropRowBottom), Point(frame.cols, checkDropRowBottom), Scalar(50, 255, 255), 1, 8);

	bool Override = true;// ALLWAYS display this window (for 2nd level diag)(
	displayIm("Output Frame", frame, Override);

	/*
	stringstream ss;
	ss.str("");
	ss << "FrameResult" << counter << ".jpg";
	imwrite((string)ss.str(), frame);
	*/
	
}