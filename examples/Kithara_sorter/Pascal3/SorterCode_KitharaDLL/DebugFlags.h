#define DBG 1
#define FDBG 1
#define FERR 1

#undef DBG
#undef FDBG 
#undef SERR

// Define if images are to be saved.
// '0'- no; '1'- yes.
#define saveImages 0

// Define if video is to be recorded.
// '0'- no; '1'- yes.
#define recordVideo 0
#undef recordVideo

#define CAM_FREE_MODE 1  // From inherited HALO code
#define SWIFT_MODE 0

/// NOTE, the following are handled as preprocessor definitions within the build configuration. 
//#define ENABLECAMERA		// Use the camera
//#define EMULATEFROMFILE	// Use file rather than camera

//#define DEBUGOUTPUT		// Plot and Save for Debugging. Diagnosic windows for general display

//#define SCREENOUTPUT		// Visualise ONLY the end result of the algorithms

#undef DEBUGOUTPUT
#undef SCREENOUTPUT