﻿/* Copyright (c) 2015 by Kithara Software. All rights reserved. */

//##############################################################################################################
//
// Module:           Kernel.cpp
//
// Descript.:        Sample application for using OpenCV library in real-time tasks with images from a camera.
//                   This is the DLL code.
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// c.was 2015-02-04  CREAT: Original - (file created)
//
//##############################################################################################################

   /*=====================================================================*\
   |                    *** DISCLAIMER OF WARRANTY ***                     |
   |                                                                       |
   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
   \*=====================================================================*/

//##############################################################################################################
//
// Purpose:
//
// This is a image acquistion and processing application for execution on kernel mode.
//
// The application uses the OpenCV 3.0 C++ interface. It handles the image reception and converts received
// images to OpenCV images using the appropriate methods from OpenCV.
//
// The image is then used for a continuous background estimation. The detected foreground is displayed
// semi-transparently over the background estimate in a OpenCV window.
// Please note, that although this sample makes use of OpenCV window handling for demonstration purposes, it is
// not recommended to use windows from kernel tasks! Additionally, OpenCV windows are designed for debugging
// and development purposes.
//
// Have a look into VisionOpenCV.cpp to see how to execute and build this sample application.
//
// IMPORTANT NOTICE: Always include the OpenCV headers using the same structure packing as the OpenCV build.
// IMPORTANT NOTICE: Disable parallelization by calling cv::setNumThreads(0) first in your real-time task.
//
//##############################################################################################################

#include "Sorter.h"

#include "..\_KitharaSmp\_KitharaSmp.h"

#ifdef __BORLANDC__
#pragma inline
#pragma warn -rvl
#endif

//--------------------------------------------------------------------------------------------------------------
// Including OpenCV headers
//
// IMPORTANT NOTICE: All Kithara samples are configured to use a 1 byte structure packing.
// However, the OpenCV libraries are in general build using the default packing of 8 bytes. Thus, we must
// restore this packing when including the OpenCV headers.
//--------------------------------------------------------------------------------------------------------------

#pragma pack(push)
#pragma pack(8)

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/highgui.hpp"



#pragma pack(pop)


#include "VisionOpenCVWithCamera.h"

using std::vector;

//--------------------------------------------------------------------------------------------------------------
// Forward declaration of sendSerialized. This functions sends an OpenCV object through shared memory to
// a ring-3 thread. This is a template to accomodate for different OpenCV objects.
// The defintition is at the end of this document.
//
// OpenCV does not provide a general way for serializing objects like some other libraries do. Thus, you must
// write your own serialization routines for the OpenCV objects. This sample serializes a 2D cv::Mat object and
// a vector<vector<Point>> object, which represent OpenCV contours.
//--------------------------------------------------------------------------------------------------------------

template <typename T> void sendSerialized(const T& object, SharedData* pSharedData);

//--------------------------------------------------------------------------------------------------------------
// Forward declarations of detectEyes. This is the core OpenCV function here, it finds the contours of the
// Mandrills eyes by evaluating some selected features. The ranges for these features are defined right here.
// They have been determined empirically.
//--------------------------------------------------------------------------------------------------------------

void detectEyes(const cv::Mat& image, vector<vector<cv::Point>>* pResult);

const double MIN_AREA = 800.0;
const double MAX_AREA = 1100.0;

const double MIN_ANISOMETRY = 1.5;
const double MAX_ANISOMETRY = 1.9;

const double MIN_AREA_MATCH = 1.0;
const double MAX_AREA_MATCH = 1.1;

//--------------------------------------------------------------------------------------------------------------
// _cameraRecv
//
// Callback function for image reception, called every time an image is received. It runs in the context of
// a real-time interrupt. The callback delegates the actual handling to the OpenCV task.
//--------------------------------------------------------------------------------------------------------------

extern "C" Error __declspec(dllexport) __stdcall _cameraRecv(void* pArgs) {

  Error ksError;
  SharedData* pData = reinterpret_cast<SharedData*>(pArgs);

  if (pData->ready) {

    //----------------------------------------------------------------------------------------------------------
    // Signal the OpenCV processing task, that an image was received
    //----------------------------------------------------------------------------------------------------------

    pData->ready = false;

    ksError = KS_setEvent(
                pData->hEvent);                         // Handle to event to be set

  } else {

    //----------------------------------------------------------------------------------------------------------
    // Fetch pending images from the queue, so we always have the newest image for OpenCV.
    //----------------------------------------------------------------------------------------------------------

    KSCameraBlock* pCameraBlock;
    void* pDummy;
    while (KS_recvCameraImage(pData->hStream, &pDummy, &pCameraBlock, 0) == KS_OK)
      KS_releaseCameraImage(pData->hStream, pDummy, 0);
  }

  return KS_OK;
}
//byte *
 void matToBytes(cv::Mat image)
{
	int size = image.total() * image.elemSize();
	//byte * bytes = new byte[size];  // you will have to delete[] that later
	//std::memcpy(bytes, image.data, size * sizeof(byte));
	printf("%s", size);
	byte * bytes = new byte[size];
}

//--------------------------------------------------------------------------------------------------------------
// _openCvAction
//
// This is the function which runs as a real-time task. Only real-time tasks should call OpenCV functions.
//--------------------------------------------------------------------------------------------------------------

extern "C" Error __declspec(dllexport) __stdcall _openCvAction(void* pArgs) {
  Error ksError;
  SharedData* pData = reinterpret_cast<SharedData*>(pArgs);
  int i = 0;
  //------------------------------------------------------------------------------------------------------------
  // IMPORTANT NOTICE:
  // Currently no parallelization framework is supported by Kithara on kernel-mode. Please disable
  // parallelization for OpenCV explicitly by using cv::setNumThreads(0) !
  //------------------------------------------------------------------------------------------------------------

  cv::setNumThreads(0);


  //------------------------------------------------------------------------------------------------------------
  // Initialize the background estimation, and create a window for displaying the result.
  //
  // Please note, that OpenCV windows are only for debugging purposes, as well that using windows and the
  // OpenCV HighGui module is not recommended for kernel-mode execution.
  // However, it is used in this sample since it is the quickest way to show the results.
  //------------------------------------------------------------------------------------------------------------

  //cv::Ptr<cv::BackgroundSubtractor> pBgEstimator = cv::createBackgroundSubtractorMOG2(256, 16.0, false);
  //cv::namedWindow("Normal Image");


  //------------------------------------------------------------------------------------------------------------
  // Signal that we are ready for processing image input
  //------------------------------------------------------------------------------------------------------------
  pData->dontcopy = false;
  pData->ready = true;


  //------------------------------------------------------------------------------------------------------------
  // Processing loop, this loop is only left when an abort has been signalled.
  //------------------------------------------------------------------------------------------------------------
  
  while (true) {
	  /*

	  //----------------------------------------------------------------------------------------------------------
	  // Wait for notification of image reception or abortion
	  //----------------------------------------------------------------------------------------------------------

	  ksError = KS_waitForEvent(
		  pData->hEvent,                          // Handle to event
		  0,                                      // Flags, none
		  0);                                     // Timeout, none is wait for ever

	  if (pData->abort)
		  break;


	  //----------------------------------------------------------------------------------------------------------
	  // Get the buffer to a received image
	  //----------------------------------------------------------------------------------------------------------

	  KSCameraBlock* pCameraBlock;
	  void* pImage;
	  ksError = KS_recvCameraImage(
		  pData->hStream,                         // Handle to stream from which images should be fetched
		  &pImage,                                // Pointer to buffer-pointer to the image data
		  &pCameraBlock,                          // Pointer to KSCameraBlock-pointer,
		  0);                                     // Flags, none
	  if (ksError != KS_OK) {
		  pData->ready = true;
		  continue;
	  }

	  //----------------------------------------------------------------------------------------------------------
	  // If the received block-type is not an image, we skip. In any case, if KS_recvCameraImage was successful
	  // the received buffer must be released with KS_releaseCameraImage
	  //----------------------------------------------------------------------------------------------------------

	  if (pCameraBlock->blockType != KS_CAMERA_BLOCKTYPE_IMAGE) {
		  KS_releaseCameraImage(pData->hStream, pImage, 0);
		  break;
	  }


	  //----------------------------------------------------------------------------------------------------------
	  // Check if the image received has the right pixel format before we construct a OpenCV cv::Mat with it
	  //----------------------------------------------------------------------------------------------------------

	  KSCameraImage* pImageBlock = reinterpret_cast<KSCameraImage*>(pCameraBlock);

	  if (pImageBlock->pixelFormat != KS_CAMERA_PIXEL_MONO_8) {
		  KS_releaseCameraImage(pData->hStream, pImage, 0);
		  break;
	  }


	  //----------------------------------------------------------------------------------------------------------
	  // Now we convert the GigE Vision image to a OpenCV image. How this is done depends on the actual format
	  // of the pixel data. OpenCV can handle several different layouts. See our OpenCV guidelines at
	  // http://www.kithara.de/en/docs/krts:tutorial:vision_opencv for some examples.
	  //----------------------------------------------------------------------------------------------------------

	  cv::Mat image(
		  pImageBlock->height,                            // rows of the image
		  pImageBlock->width,                             // columns of the image
		  CV_8UC1,                                        // color format, 'Mono8' correspondents to CV_8UC1
		  pImage,                                         // pointer to image data
		  pImageBlock->width + pImageBlock->linePadding); // size in bytes of a single line


	  //----------------------------------------------------------------------------------------------------------
	  // Using the constructor of cv::Mat for user specific data does not copy the image data. However, we want
	  // to release the buffer as soon as possible to the input queue of the acquisition engine of the camera,
	  // hence we make a deep copy here using the cv::Mat::clone method. Assigning the result to the original
	  // image, will release the last reference to the buffer data.
	  //
	  // Note that it is not necessarily required to copy the data, you could also defer the release of the buffer
	  // to after the image has been processed.
	  //----------------------------------------------------------------------------------------------------------

	  image = image.clone();

	  //----------------------------------------------------------------------------------------------------------
	  // Now that we have copied the image data, we can release the buffer to the camera.
	  //----------------------------------------------------------------------------------------------------------

	  ksError = KS_releaseCameraImage(
		  pData->hStream,                         // Handle to stream
		  pImage,                                 // Pointer to pixel data
		  0);                                     // Flags, none


	  i++;
  
	  // Writing the frame number as text on the image frame

	  char numstr[21]; // enough to hold all numbers up to 64-bits
	  sprintf(numstr, "Frame = %i", i);
	  cv::putText(image, numstr, cvPoint(10, 15),
		  1, 0.8, cvScalar(255, 255, 255), 1, CV_AA);

	  pData->number = i;

	  int size = image.total() * image.elemSize();
	  byte * bytes = new byte[size];
	  
	  // Storing image information to shared memory

	  pData->height2 = image.rows;
	  pData->width2 = image.cols;
	  pData->step2 = static_cast<int>(image.step);
	  
	  if (pData->dontcopy == false){
		  pData->dontread = true;
		  
		  // Copy image to shared memory

		  KS_memCpy(pData->image2, image.data, size * sizeof(byte), 0);
		  pData->dontread = false;

		}

	  //----------------------------------------------------------------------------------------------------------
	  // Signal that we are ready for processing further image input
	  //----------------------------------------------------------------------------------------------------------

	pData->ready = true;
	*/
  }

  //------------------------------------------------------------------------------------------------------------
  // We're done, clean up is performed by implicit destructor calls
  //------------------------------------------------------------------------------------------------------------

  return KS_OK;
}


//--------------------------------------------------------------------------------------------------------------
// _cellSorter
//
// Callback function for image reception, called every time an image is received. It runs in the context of
// a real-time interrupt. The callback delegates the actual handling to the OpenCV task.
//--------------------------------------------------------------------------------------------------------------

extern "C" Error __declspec(dllexport) __stdcall _cellSorter(void* pArgs) {

	Error ksError;
	SharedData* pData = reinterpret_cast<SharedData*>(pArgs);

	//if (pData->ready) {
		printf("--------------------------------------------------\n\n");
		printf("Running sorter test WITHOUT image display\n");
		run(false);

		printf("--------------------------------------------------\n\n");
		printf("Re-running sorter test WITH image display\n");
		run(true);
		printf("--------------------------------------------------\n\n");
		printf("TEST COMPLETE\n\n");
		
		
	//}

	return KS_OK;
}
//--------------------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------------------

template <typename T> void serialize(void* pMemory, const T& object);
template <typename T> int serializedSize(const T& object);
template <typename T> SharedData::ItemType serializedType();

//--------------------------------------------------------------------------------------------------------------
// Serialization routines for cv::Mat. A 2-D cv::Mat is actually a tiny header describing the binary data
// belonging to the cv::Mat. Copying the essential data from the header and the actual binary data is sufficient
// to reconstruct the object from memory.
//--------------------------------------------------------------------------------------------------------------

template <>
void serialize<cv::Mat>(void* pMemory, const cv::Mat& image) {
	SerializedImage* pImage = reinterpret_cast<SerializedImage*>(pMemory);

	pImage->type = image.type();
	pImage->step = static_cast<int>(image.step);
	pImage->rows = image.rows;
	pImage->colums = image.cols;

	//------------------------------------------------------------------------------------------------------------
	// Copy the actual image data to memory. Please use KS_memCpy on ring-0.
	//------------------------------------------------------------------------------------------------------------

	KS_memCpy(
		pImage->pData,                                      // Pointer to destination buffer
		image.data,                                         // Pointer to source buffer
		pImage->step * pImage->rows,                        // Size in bytes of data to copy
		0);                                                 // Flags, here none

}

template <>
int serializedSize<cv::Mat>(const cv::Mat& image) {
	return static_cast<int>(sizeof(SerializedImage) - 1 + image.rows * image.step);
}

template <>
SharedData::ItemType serializedType<cv::Mat>() {
	return SharedData::IMAGE;
}


//--------------------------------------------------------------------------------------------------------------
// Serialization routines for contours.
// An OpenCV contour is a STL vector of Point. Contours are a STL vector of contour.
// The serialization strategie is to unfold a vector be prepending its item count followed by the data.
//--------------------------------------------------------------------------------------------------------------

template <>
void serialize<vector<vector<cv::Point>>>(void* pMemory, const vector<vector<cv::Point>>& contours) {
	int* pInts = reinterpret_cast<int*>(pMemory);
	int i = 0;
	pInts[i++] = static_cast<int>(contours.size());
	for (int j = 0; j < contours.size(); ++j) {
		pInts[i++] = static_cast<int>(contours[j].size());
		for (int k = 0; k < contours[j].size(); ++k) {
			pInts[i++] = contours[j][k].x;
			pInts[i++] = contours[j][k].y;
		}
	}
}

template <>
int serializedSize<vector<vector<cv::Point>>>(const vector<vector<cv::Point>>& contours) {
	int size = sizeof(int);
	for (int i = 0; i < contours.size(); ++i) {
		size += sizeof(int);
		size += static_cast<int>(contours[i].size()) * 2 * sizeof(int);
	}
	return size;
}

template <>
SharedData::ItemType serializedType<vector<vector<cv::Point>>>() {
	return SharedData::CONTOURS;
}

//--------------------------------------------------------------------------------------------------------------
// sendSerialized sends serialized objects through shared memory.
// Note that error checking is omitted here for simplicity.
//--------------------------------------------------------------------------------------------------------------

template <typename T>
void sendSerialized(const T& object, SharedData* pSharedData) {

	//------------------------------------------------------------------------------------------------------------
	// Check the size of the object to serialize. If its sufficient, serialize the object into the shared memory.
	// The type of the object must be communicated, so the correct object can be reconstructed on the other side.
	//------------------------------------------------------------------------------------------------------------

	int size = serializedSize(object);

	if (size <= pSharedData->memory.size) {
		serialize(pSharedData->memory.pSysPtr, object);
		pSharedData->memory.itemType = serializedType<T>();
		pSharedData->memory.itemSize = size;
	}
	else {
		pSharedData->memory.itemType = SharedData::ItemType::BUFFER_TOO_SMALL;
		pSharedData->memory.itemSize = size;
	}

	//------------------------------------------------------------------------------------------------------------
	// Signal the user mode thread that data has arrived, and wait for the user mode thread to signal us to
	// continue.
	//------------------------------------------------------------------------------------------------------------

	pSharedData->action = SharedData::DATA;
	KS_setEvent(pSharedData->hEvent);

	KS_waitForEvent(pSharedData->hTaskEvent, 0, 0);
}
//--------------------------------------------------------------------------------------------------------------
// Windows dll main definition
//--------------------------------------------------------------------------------------------------------------

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

BOOL WINAPI DllMain(HINSTANCE hInstDll, DWORD reason, LPVOID pReserved) {
  return TRUE;
}
