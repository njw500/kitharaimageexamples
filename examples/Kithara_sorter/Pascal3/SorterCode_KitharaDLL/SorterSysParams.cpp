#include "stdafx.h"
#include "SorterSysParams.h"

// For Debug Macros
//#include "VideoOutput.h"

using namespace cv;

// Namespace for using cout
using namespace std;

#define USEPOC 1
//#undef USEPOC
// Do nothing in Constructor
SorterSysParams::SorterSysParams()
{

	//CamROI.x = 0;
	//CamROI.y = 0;
	//CamROI.height = 100;
	//CamROI.width  = 400;

	// Set ROI for extraction of useful data from grabbed (or video frame)

	CannyThres1 = 22.0;// 22.0;// was 70 in python
	CannyThres2 = 15.0;// 15.0;// was 40
	CannyThres1 = 70.0;// 22.0;// was 70 in python
	CannyThres2 = 40.0;// 15.0;// was 40
	// This works for POC video 

	/*  Generator numbers !
	VideoFileName="V:/Projects/Pascal/Generator C Code/GeneratorVid.avi";
	*/
	cParams.FrameROIr.x		 = 1;
	cParams.FrameROIr.y		 = 772;
	cParams.FrameROIr.width  = 500;
	cParams.FrameROIr.height = 146;

	cParams.CameraSerial = "";

	//cParams.CameraSerial = "21997816"; //21997816;
	cParams.gain = 8.9;
	cParams.exp = 59;
	

	VideoFileName = "c:/Temp/Basler acA1300-200um CS (21997816)_20161213_121445700.avi"; // ED PC
	VideoFileName = "v:/projects/Pascal/Sorter/Basler acA1300-200um CS (21997816)_20161213_121445700.avi"; // NJW PC
	VideoFileName = "c:/Pascal/test.avi";

	VideoIsPreCropped = true;

	//CameraSerial = "21799755";// Local office
	//CameraSerial = "21799755"; //  ED's PC   21997816;


	//"22025921";// Generator
	// 21997816; // SORTER
	// 22025914; // DISP
	// 21982883  // BOOT

	// SORTER



	AverageDecay = 0.1f;
	Zone_Detection_Radius= 0.4f;


	DisplayMode[WGrab] = false;
	DisplayMode[WHough] = false;
	DisplayMode[WBack] = false;
	DisplayMode[WCropped] = false;
	DisplayMode[WEdges] = false;
	DisplayMode[WHist] = false;
	DisplayMode[WOutput] = false;
	DisplayMode[WCellDetection] = true;// True for final image display



	Padding = 3; // Padding of the drop region
	sorterRoiY1 = 77; // The start column that should be selected, 
	sorterRoiY2 = 81; //  The end column that should be selected	
	//topbottomborder_px : The number of pixels to ignore at the top and bottom of the image
	sorterTopBottomBorder_px = 5;
	// Grayscale threshold level for the halo surrounding drops
	sorterThresholdLevelDrop =100;
	// min_pk_dist: The distance which corresponds to the minimum diameter of a drop.//
	sorterMinPkDist = 100;
	// Cell padding parameters
	sorterCellPadding = 10;
	// The open size for cell detection
	sorterOpenSize = 8;
	// The erode size for cell detection
	sorterErodeSize = 20;
	// The threshold level for the cell
	sorterThresholdLevelCell = 30;
	// The top region for checking whether this is a gap or a drop
	sorterCheckDropRowTop = 30;
	// The bottom region for checking whether this is a gap or a drop
	sorterCheckDropRowBottom = 105;

}


// Do nothing in Destructor
SorterSysParams::~SorterSysParams()
{
}

// A helper function to check (and correct) the bounds of key function parameters
bool SorterSysParams::checkBounds()
{
	bool retVal = true;
	retVal = retVal && checkVarBounds(&sorterRoiY1, 2, cParams.FrameROIr.height);
	retVal = retVal && checkVarBounds(&sorterRoiY2, 2, cParams.FrameROIr.height - 2);
	retVal = retVal && checkVarBounds(&sorterRoiY1, 2, sorterRoiY2);
	retVal = retVal && checkVarBounds(&sorterCellPadding, 1, 50);
	retVal = retVal && checkVarBounds(&sorterThresholdLevelCell, 1, 255);
	retVal = retVal && checkVarBounds(&sorterThresholdLevelDrop, 1, 255);
	retVal = retVal && checkVarBounds(&sorterCellPadding, 1, 50);
	retVal = retVal && checkVarBounds(&sorterOpenSize, 2, cParams.FrameROIr.height / 2);
	retVal = retVal && checkVarBounds(&sorterErodeSize, 2, cParams.FrameROIr.height / 2);
	retVal = retVal && checkVarBounds(&(cParams.gain), (int)0, (int)12);

	// Need to add some bounds checking for exposure and gain. 
	return retVal;

}

bool SorterSysParams::checkVarBounds(int *var, int mn, int mx)
{
	bool retVal = true;
	if (*var < mn)
	{
		*var = mn;
		retVal = false;

	}
	if (*var > mx)
	{
		*var = mx;
		retVal = false;
	}
	return retVal;
}

bool SorterSysParams::checkVarBounds(double *var, double mn, double mx)
{
	bool retVal = true;
	if (*var < mn)
	{
		*var = mn;
		retVal = false;

	}
	if (*var > mx)
	{
		*var = mx;
		retVal = false;
	}
	return retVal;
}

