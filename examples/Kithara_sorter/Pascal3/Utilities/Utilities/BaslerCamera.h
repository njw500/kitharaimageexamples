#ifndef BASLERCAMERA_H
#define BASLERCAMERA_H

// Standard C++ includes
#include <iostream>
#include <iomanip>

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>
#include <pylon/PylonGUI.h>
#include <pylon/usb/BaslerUsbInstantCamera.h>
#include <pylon/usb/_BaslerUsbCameraParams.h>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>

// Other objects
#include "CamParams.h"

// Namespace for using cout
using namespace std;

class BaslerCamera
{

private:

	bool CameraOK=false;

	bool CallbacksEnabled = false;

	bool BlockMode = false;	// Acquire chunks into memory

	bool VerboseStart = true; // Display info on screen

	// Pointer to Pylon Camera
	Pylon::CBaslerUsbInstantCamera* mPylonCameraPtr;

	// Converter Object to convert from Raw to an Image
	// IS THIS USED Pylon::CImageFormatConverter* mRawToImageConverterPtr;

	// Last Grabbed Image Pointer
	Pylon::CGrabResultPtr mLastGrabbedResultPtr;

	// Conversion stuff <OLD>
	// Create a pylon ImageFormatConverter object.
	//CImageFormatConverter *formatConverter;

	// NJW new conversion stuff
	Pylon::CImageFormatConverter mFormatConverter;

	// Create a PylonImage that will be used to create OpenCV images later.
	Pylon::CPylonImage mPylonImage;

	// Flag for Last Grab
	bool mLastGrabSuccessful;
	bool mLastConversionSuccessful;

	// Flag to indicate whether or not the first frame in a sequence has been received
	bool mIsFirstFrame;
	int mFirstFrameCounterOffset;

	// Total Number of Frames we have seen counter 
	long mTotalNumberFrames;

	// Helper Functions
	
	void SetSettings();
	void GetSettings();

	//Store of current camera params
	string CamSerial;
	string CamModel;
	string PixelFormat;

	int64_t CamWidth;
	int64_t CamHeight;
	int64_t Xoffset;
	int64_t Yoffset;

	uint16_t Camera_Default_Frame_Rate=12;		// Fastest possible exposure		  
	uint16_t Camera_Default_Exposure_Time=30;	// 30Hz slow rate unless pushed    

	uint16_t Current_Frame_Rate;
	uint16_t Current_Exposure_Time;

	double RealFrameRate;
	double CamGain;
	double CamExposureTime;

	bool IsColour;
	bool FastMode;
	bool EventSelectorAllowed;
	bool ChunkAllowed;

	bool LimitBandwidth;
	int BandWidthLimit;
	int CurBandWidth;

	void Run();// Start grabbing mode
	void Stop();

	bool SetROILocation(cv::Point2i Location);

	int64_t xInc, yInc;
	int64_t WidthInc;
	int64_t HeightInc;

public:
	BaslerCamera();
	~BaslerCamera();


	bool AutoStart = true;	// Start on Init

	// Init Function
	bool Init(const string camerSerialNumber, CamParams Params);

	// Get the Next Image
	void GrabNextImg();

	// Convert the Image
	//void ConvImg(void* DstPtr);
	
	bool BaslerCamera::GetMatImage(cv::Mat& frame);

	// Get the Pointer to the Latest Image and whether or not the pointer is valid
	void* GetLastestImg();
	bool IsLastestImgValid();

	// Get the image number in the sequence which this image is
	int GetCurrImageSequenceNumer(bool CheckingLeftCameraFlag);
	double BaslerCamera::GetRealFrameRate();
	// Switch the converted Image Out Buffers around
	void PingPongConvOutBuffer();

	// Helper Functions
	void PrintSettings();

	void SetGain(const float value);
	void SetExposure(const float value);
	void SetFrameRate(const int FrameRate);
	void GetGainExpRate(float &Gain, uint16_t & ExpTime, uint16_t &FrameRate);
	cv::Rect BaslerCamera::SetROI(cv::Rect &NewROI);

	bool GetROI(cv::Rect &Roi);
	cv::Rect GetROI();

	//bool RoiUp(const int val);
	//bool RoiDown(const int val);
	bool RoiVert(const int val,const int Sign);
	bool RoiHoriz(const int val, const int Sign);


	uint16_t GetCurWidth() { return((uint16_t)CamWidth); };
	uint16_t GetCurHeight(){ return((uint16_t)CamHeight); };

	//bool Burst(const int NumFrames);
	bool Burst(std::vector<cv::Mat> &frames, std::vector<float> &DeltaT,const int NumFrames);

	bool DisplayLoop(cv::Rect &CurROIm);

	const bool GrabSuccesful(){ return(mLastGrabSuccessful); };
};


// Macros to convert camera settings to human readable config
#define LOOK_UP_SHUTTER_MODE_SEL(X)               ( X ? "Rolling":"Global" )
#define LOOK_UP_EXPOSURE_AUTO_SEL(X)              ( AutoAdjust[X] )
#define LOOK_UP_EXPOSURE_MODE_SEL(X)              ( X ? "Not Timed":"Timed" )
#define LOOK_UP_SENSOR_READOUT_MODE_SEL(X)        ( X ? "Fast":"Normal" )
#define LOOK_UP_TRIGGER_TYPE_SEL(X)               ( X ? "Frame Start":"Frame Burst Start" )
#define LOOK_UP_TRIGGER_SOURCE_SEL(X)             ( TriggerModes[X] )
#define LOOK_UP_TRIGGER_ACT_SEL(X)                ( X ? "Falling Edge":"Rising Edge" )
#define LOOK_UP_GAIN_AUTO_SEL(X)                  ( AutoAdjust[X] )
#define LOOK_UP_WHITE_BALANCE_SEL(X)              ( AutoAdjust[X] )
#define LOOK_UP_LIGHT_SOURCE_SEL(X)               ( LightSourcePresets[X] )
#define LOOK_UP_PIXEL_FORMAT(X)                   ( PixelFormats[X] )
#define LOOK_UP_CHUNK_DATA_SEL(X)                 ( ChunkData[X] )
#define LOOK_UP_GPIO_MODE(X)                      ( X ? "Output":"Input" ) 
#define LOOK_UP_LINE_SOURCE(X)                    ( LineSource[X] )
#define LOOK_UP_LINE_INVERTED(X)                  ( X ? "ActiveHigh":"ActiveLow" )
#define BOOL_TO_STRING(X)                         ( X ? "True":"False" )

// Const String arrays for printing
static const string TriggerModes[] = { "Software", "Line1", "Line3", "Line4" };
static const string LightSourcePresets[] = { "Off", "5000K", "6500K", "2800K" };
static const string AutoAdjust[] = { "Off", "Once", "Continuous" };
static const string PixelFormats[] = { "PixelFormat_Mono8 ", "PixelFormat_Mono10", "PixelFormat_Mono10p", "PixelFormat_Mono12", "PixelFormat_Mono12p", "PixelFormat_BayerGR8", "PixelFormat_BayerRG8  ", "PixelFormat_BayerGB8  ", "PixelFormat_BayerBG8" };
static const string ChunkData[] = { "ChunkSelector_Image", "ChunkSelector_Gain", "ChunkSelector_ExposureTime", "ChunkSelector_Timestamp", "ChunkSelector_LineStatusAll", "ChunkSelector_CounterValue", "ChunkSelector_SequencerSetActive", "ChunkSelector_PayloadCRC16" };
static const string LineSource[] = { "LineSource_Off", "LineSource_ExposureActive", "LineSource_FrameTriggerWait", "LineSource_FrameBurstTriggerWait", "LineSource_Timer1Active", "LineSource_UserOutput0", "LineSource_UserOutput1", "LineSource_UserOutput1", "LineSource_UserOutput1", "LineSource_FlashWindow" };




// Macros to LookUp GPIO Lines
#define GPIO_LINE1                                (0)
#define GPIO_LINE2                                (1)
#define GPIO_LINE3                                (2)
#define GPIO_LINE4                                (3)

// Key Values
#define CAMERA_TRIGGER_DELAY                      (0)  // In uSeconds
#define CAMERA_TARGET_BRIGHTNESS				  (0.5f)  // Float between 0.0 and 1.0 which adjusts our target brightness
#define CAMERA_FIXED_X_Y_OFFSET                   (0)  // Do not offset sensor pixels
#define CAMERA_BURST_NUM_FRAMES                   (4)  // Acquire 4 images in each burst

// Grab Strategy and Parameters
#define CAMERA_GRAB_STRATEGY        (GrabStrategy_LatestImageOnly)
#define CAMERA_GRAB_TIMEOUT_MSEC                  (3)  // 3ms timeout

// Converter 
#define CAMERA_CON_x_VERSION_OUTPUT      (PixelType_BGRA8packed) // Use this as it is the only format which has an alpha channel (which a GPU wants)





#endif