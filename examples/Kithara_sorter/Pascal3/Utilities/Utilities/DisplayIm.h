#ifndef DISPLAYIM_H
#define DISPLAYIM_H

// Standard C++ includes
#include <iostream>
#include <iomanip>

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>
#include <pylon/PylonGUI.h>
#include <pylon/usb/BaslerUsbInstantCamera.h>
#include <pylon/usb/_BaslerUsbCameraParams.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

// Boost for multi-threading support and mutexs
//#include <boost/thread.hpp>

// Include files to use OpenCV API.
//#include <opencv2/core/core.hpp>

// Other objects
//#include "SysParams.h"

// Namespace for using cout
using namespace std;


void displayIm(cv::String str, cv::Mat im);
void displayIm(cv::String str, cv::Mat im, int flag);
void displayIm(cv::String str, cv::Mat im, int flag, bool overide);
void displayIm(cv::String str, cv::Mat im, bool overide);

void ScaleImage(cv::Mat in, cv::Mat &out);
void ScaleImageAbs(cv::Mat in, cv::Mat &out);

#endif
