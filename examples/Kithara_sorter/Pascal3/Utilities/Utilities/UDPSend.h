#pragma once

#ifndef UDPSEND_H
#define UDPSEND_H


#include <winsock2.h>
//#include <ws2tcpip.h>

//#include <stdio.h>
#include <stdlib.h>

//#include <string.h>
#include <sys/types.h>

//#include <stdio.h>
//#include <stdlib.h>   // Needed for _wtoi

//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <iostream>


class cUDP
{
public:
	cUDP();
	~cUDP();
	bool OK = false;
	bool WSA_Active = false;
	
	void Init();
	void Init(int port, bool mode);
	void SendString(char *str);
	void SendByte(char byte);
	void Close();

private:
	SOCKET sock = INVALID_SOCKET;
	int channel;
	int Sock;
	struct sockaddr_in portList;
	WSADATA wsaData;// = { 0 };
	const int Default_Port = 27125;
	int CurrentPort;
	bool SendLF = false;
};





#endif