#include "stdafx.h"
#include "WriteVector.h"


void WriteVector(const vector<float>& data, int sz, char * filename)
{
	FILE *file;
	file = fopen(filename, "wt");
	int i;
	for (i = 0; i <sz; ++i)
	{
		fprintf(file, "%d,%f\n", i, data[i]);
	}
	fclose(file);
}

void WriteVector(const vector<int>& data, int sz, char * filename)
{
	FILE *file;
	file = fopen(filename, "wt");
	int i;
	for (i = 0; i <sz; ++i)
	{
		fprintf(file, "%d,%f\n", i, (float)data[i]); 
	}
	fclose(file);
}

void WriteVector(const vector<__int64>& data, int sz, char * filename)
{
	FILE *file;
	file = fopen(filename, "wt");
	int i;
	for (i = 0; i <sz; ++i)
	{
		fprintf(file, "%d,%llu\n", i, data[i]);
	}
	fclose(file);
}



void WriteVector(cv::Mat res, char * filename)
{
	FILE *file;
	cv::Mat out;

	res.convertTo(out, CV_32F);

	double minVal, maxVal;
	minMaxLoc(out, &minVal, &maxVal);
	cout << "max/min " << std::fixed << maxVal << " " << std::fixed << minVal << endl;

	float *my_data = (float *)out.data;
	file = fopen(filename, "wt");

	int i;
	int r = res.rows;
	int c = res.cols;
	int num;

	if (r > c)
		num = r;
	else
		num = c;

	for (i = 0; i < num; ++i)
	{
		fprintf(file, "%d,%f\n", i, my_data[i]);
	}
	fclose(file);

}

void WriteVector(float *out, int sz, char * filename)
{
	//#ifdef FDBG
	FILE *file;

	float *my_data = out;
	file = fopen(filename, "wt");
	int i;
	for (i = 0; i <sz; ++i)
	{
		fprintf(file, "%d,%f\n", i, my_data[i]);
	}
	fclose(file);
	//#endif
}

/*
void Writevector(cv::Mat res, char * filename)
{
FILE *file;
cv::Mat out;
res.convertTo(out, CV_32F);

double minVal, maxVal;
minMaxLoc(out, &minVal, &maxVal);
cout << "max/min " << std::fixed << maxVal << " " << std::fixed << minVal << endl;

float *my_data = (float *)out.data;
file = fopen(filename, "wt");
int i;
for (i = 0; i < res.cols; ++i)
{
fprintf(file, "%d,%f\n", i, my_data[i]);
}
fclose(file);
}*/


#ifdef FDBG
void Writevector(cv::Mat res, char * filename)
{

	FILE *file;
	cv::Mat out;

	res.convertTo(out, CV_32F);

	double minVal, maxVal;
	minMaxLoc(out, &minVal, &maxVal);
	cout << "max/min " << std::fixed << maxVal << " " << std::fixed << minVal << endl;

	float *my_data = (float *)out.data;
	file = fopen(filename, "wt");
	int i;
	for (i = 0; i < res.cols; ++i)
	{
		fprintf(file, "%d,%f\n", i, my_data[i]);
	}
	fclose(file);

}
#endif