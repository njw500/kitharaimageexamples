#include "stdafx.h"

// The use of Winsock is for the Instrument controller feedback
// As the code moves to Kithara this requirment will be removed

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "stdio.h"
#include "UDPSend.h"

// link with Ws2_32.lib
#pragma comment(lib,"Ws2_32.lib")

// Namespace for using cout
using namespace std;



cUDP::cUDP()
{
	wsaData= { 0 };
	OK = false;
	WSA_Active = false;
}


cUDP::~cUDP()
{
	//try
	//{
		if (OK)
		{
			closesocket(sock);
		}

		if (WSA_Active)
		{

			WSACleanup();
		}
	/*}
	catch 
	{
		cout << "Exception in Socket shutdown" << e
		ndl;
	}*/
}

void cUDP::SendString(char *ptr)
{
	int len = (int)strlen(ptr);

	if (OK && (len>0) && (len<200))
	{
		int num = sendto(sock, ptr, len, 0, (struct sockaddr *)&portList, sizeof(portList));

		if (num != 1)
		{
			strerror(num);
			//cout << "Failed to send byte packet" << endl;

		}
	}

}

void cUDP::SendByte(char byte)
{
	if (OK)
	{
		int num = sendto(sock, &byte, 1, 0, (struct sockaddr *)&portList, sizeof(portList));

		if (num != 1)
		{
			strerror(num);
			//cout << "Failed to send byte packet" << endl;

		}
	}

}

void cUDP::Init()
{
	Init(Default_Port, false);

}

void cUDP::Init(const int port, bool LFmode)
{
	//int z;
	//struct sockaddr_in portList;
	//int len_inet;
	//int s;
	//char dgram[512];
	//time_t td;
	//struct tm tm;

	// Declare and initialize variables
	OK = false;

	SendLF = LFmode; // Try hack for Tony's code
	CurrentPort = port;

	int iResult = 0;
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) 
	{
		wprintf(L"WSAStartup failed: %d\n", iResult);
		
		WSA_Active = false;
		return;
	}
	WSA_Active = true;

	this->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); // last was 0

	if (this->sock == INVALID_SOCKET)
	{
		strerror(errno);
		OK = false;
		return;
	}

	// Zero the address structure
	memset((void *)&portList, 0, sizeof(sockaddr_in));
	portList.sin_family = AF_INET;
	portList.sin_addr.s_addr = htonl(INADDR_ANY);
	portList.sin_addr.s_addr=inet_addr("127.0.0.1");
	
	portList.sin_port = htons(CurrentPort);

	/*
	if (bind(sock, (struct sockaddr *)&portList, sizeof(portList)) < 0)
	{
		perror("Bind failed");
		OK = false;
		return;
	}
	else
		OK = true;
	*/
	OK = true;


}



