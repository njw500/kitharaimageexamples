#include "stdafx.h"
#include "displayIm.h"

//#include "DebugFlags.h"

using namespace cv;

// Namespace for using cout.
using namespace std;


void ScaleImage(Mat in, Mat &out)
{
	if (in.type() != CV_32F)
		in.convertTo(in, CV_32F);

	double minVal, maxVal;
	minMaxLoc(in, &minVal, &maxVal);
	
	double alpha = 253.0 / (maxVal - minVal);
	double beta = -255.0 * minVal / (maxVal - minVal); 
	//displayIm("Pre-scale", in);
	in.convertTo(out, CV_32F, alpha, beta);
	minMaxLoc(out, &minVal, &maxVal);
	//displayIm("Pout-scale", out);
	out.convertTo(out, CV_8U);
	//displayIm("Pout-scale2", out);

}

void ScaleImageAbs(Mat in, Mat &out)
{
	if (in.type() != CV_32F)
		in.convertTo(in, CV_32F);

	double minVal, maxVal;
	minMaxLoc(in, &minVal, &maxVal);
	double alpha = 253.0 / (maxVal - minVal);
	double beta = -255.0 * minVal / (maxVal - minVal);
	convertScaleAbs(in,out, alpha, beta);
	minMaxLoc(out, &minVal, &maxVal);

}

void displayIm(String str, Mat im)
{
	//void displayIm(String str, Mat im, int flag,);
	displayIm(str, im, false);// for now , false);
}

void displayIm(String str, Mat im, int flag)
{
	// Global flag for disabling	
	#ifdef DEBUGOUTPUT
		displayIm(str, im, flag,false);
	#endif
}

// Can override the #define flag
void displayIm(String str, Mat im, bool flag)
{
	displayIm(str, im, 0, flag);

}

// Generic. all options
void displayIm(String str, Mat im,int flag, bool over)
{
	bool QuickExit = true;

#ifdef DEBUGOUTPUT
	QuickExit = false;
#endif

	if (QuickExit & (!over))
		return;

	if ((im.rows < 2) || (im.cols < 2))
	{
		cout << "Image is to small too display <2 pixels" << endl;
		return;
	}
	Mat fim;
	im.convertTo(fim, CV_32F);
	double minVal, maxVal;
	minMaxLoc(fim, &minVal, &maxVal);
	//cout << "max/min " << std::fixed << maxVal << " " << std::fixed << minVal << endl;
	if ((maxVal - minVal) < 0.001)
	{
		cout << "Image data range is too small to display <0.001 " << endl;
		return;
	}
	Mat drawIm;
	if (flag == 1)
	{
		float beta = -1.0f * minVal;
		float alpha = 253.0f / (maxVal - minVal);
		im.convertTo(drawIm, CV_8UC3,alpha,beta);
	}
	else
	{
		im.convertTo(drawIm, CV_8UC3);
	}
	
	imshow(str, drawIm);
	waitKey(1);
	waitKey(1);



}