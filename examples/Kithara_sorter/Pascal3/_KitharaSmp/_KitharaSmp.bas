Attribute VB_Name = "KitharaSmp"
' Copyright (c) 1999-2003 by Kithara Software. All rights reserved.

'###############################################################################
'
' File:             _KitharaSmp.bas
'
' Description:      Visual Basic helper functions for �Tool Suite� samples
'
' REV   DATE        LOG    DESCRIPTION
' ----- ----------  ------ ----------------------------------------------------
' j.ant 1999-10-31  CREAT: Original - (file created)
' ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:02
'
'###############################################################################

'   /*=====================================================================*\
'   |                    *** DISCLAIMER OF WARRANTY ***                     |
'   |                                                                       |
'   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
'   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
'   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
'   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
'   \*=====================================================================*/

'##############################################################################
'
' Purpose:
' The functions in this file are some helper functions for all of our sample
' programs. They are used for input and output, so they are compatible to the
' other programming languages and environments supported.
'
'##############################################################################


'-------------------------------------------------------------------------------
' _KitharaSmp
'-------------------------------------------------------------------------------

Option Explicit

Public ms As Long

Public hGlobalEvent As Long
Public bGlobalStart As Boolean

'------ sleep ------
Declare Function Sleep Lib "kernel32.dll" (ByVal time As Long) As Long

'------ outputTxt ------
Sub outputTxt(ByVal text As String, Optional ByVal newLine As Boolean = True)
  mainForm.Text1.text = mainForm.Text1.text + text
  If newLine Then
    mainForm.Text1.text = mainForm.Text1.text + vbNewLine
  End If
End Sub

'------ outputDec ------
Function outputDec(ByVal dec As Long, Optional ByVal preLine As String = "", Optional ByVal postLine As String = "", Optional ByVal newLine As Boolean = True)
  outputTxt preLine + Str(dec) + postLine, newLine
End Function

'------ outputHex ------
Function outputHex(ByVal hexa As Long, Optional ByVal preLine As String = "", Optional ByVal postLine As String = "", Optional ByVal newLine As Boolean = True)
  outputTxt preLine + Hex(hexa) + postLine, newLine
End Function

'------ outputErr ------
Function outputErr(ByVal error As Long, Optional ByVal caption As String = "", Optional ByVal comment As String = "Failed") As Boolean
  Dim buffer As String * 80
  Dim message As String
  If error Then
    KS_getErrorStringEx error, buffer, KSLNG_DEFAULT
    message = Left(buffer, InStr(buffer, vbNullChar) - 1)
    outputTxt caption + ": " + comment + " ERROR=&H" + Hex(error) + " ('" + message + "')"
  End If
  outputErr = error
End Function

'------ inputTxt ------
Function inputTxt(ByVal caption As String, Optional ByVal default As String = "") As String
  inputTxt = mainForm.inputTxt(caption, default)
End Function

'------ inputDec ------
Function inputDec(ByVal caption As String, Optional ByVal default As String = "") As Long
  inputDec = Val(inputTxt(caption, default))
End Function

'------ inputHex ------
Function inputHex(ByVal caption As String, Optional ByVal default As String = "") As Long
  Dim value As String
  value = UCase(inputTxt(caption, default))
  If Not value Like "&H*" Then
    value = "&H" + value
  End If
  inputHex = Val(value)
End Function

'------ waitTime ------
Sub waitTime(ByVal time As Long)
  Sleep (time / 10000)
End Sub

'------ runRoutine ------
' There is no multi-threading yet! So this function is not used now
Function runRoutine(Data As Long) As Long
  Do While Not outputErr(KS_waitForEvent(hGlobalEvent, 0, 0), "KS_waitForEvent")
    runSample
  Loop
  runRoutine = 0
End Function

