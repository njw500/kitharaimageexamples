﻿/* Copyright (c) 2002-2012 by Kithara Software. All rights reserved. */

//##############################################################################################################
//
// Module:           _KitharaSmp.cpp
//
// Descript.:        utility file for Kithara Sample Code
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// a.lun 2002-12-18  CREAT: Original - (file created)
// u.jes 2003-06-26  CHNGD: Some optimization
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:02
//
//##############################################################################################################

   /*=====================================================================*\
   |                    *** DISCLAIMER OF WARRANTY ***                     |
   |                                                                       |
   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
   \*=====================================================================*/

//##############################################################################################################
//
// Purpose:
// The functions in this file are some helper functions for all of our sample programs.
// They are used for input and output, so they are compatible to the other programming languages and
// environments supported.
//
//##############################################################################################################


//--------------------------------------------------------------------------------------------------------------
// _KitharaSmp
//--------------------------------------------------------------------------------------------------------------

#define _CRT_SECURE_NO_WARNINGS

#define NO_STRICT
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <conio.h>
#include <stdarg.h>

#include "C:\projects\Pascal3\_KitharaSmp\_KitharaSmp.h"

//------ outputTxt ------
void outputTxt(const char* pText, bool line) {
  if (pText && *pText)
    printf("%s%s", pText, line ? "\n" : "");
  else
    printf(line ? "\n" : "");
}

//------ outputBool ------
void outputBool(int boolean, const char* pPreText, const char* pPostText, bool line) {
  printf("%s%s%s%s", pPreText ? pPreText : "", boolean ? "true" : "false" , pPostText ? pPostText : "",
    line ? "\n" : "");
}


//------ outputDec ------
void outputDec(int dec, const char* pPreText, const char* pPostText, bool line) {
  printf("%s%d%s%s", pPreText ? pPreText : "", dec, pPostText ? pPostText : "", line ? "\n" : "");
}

//------ outputDec16 ------
void outputDec16(int64 dec, const char* pPreText, const char* pPostText, bool line) {
#ifdef _MSC_VER
  printf("%s%I64d%s%s", pPreText ? pPreText : "", dec, pPostText ? pPostText : "", line ? "\n" : "");
#else
  printf("%s%Ld%s%s", pPreText ? pPreText : "", dec, pPostText ? pPostText : "", line ? "\n" : "");
#endif
}

//------ outputHex ------
void outputHex(int hex, const char* pPreText, const char* pPostText, bool line) {
  printf("%s%04X%s%s", pPreText ? pPreText : "", hex, pPostText ? pPostText : "", line ? "\n" : "");
}

//------ outputHex02 ------
void outputHex02(int hex, const char* pPreText, const char* pPostText, bool line) {
  printf("%s%02X%s%s", pPreText ? pPreText : "", hex, pPostText ? pPostText : "", line ? "\n" : "");
}

//------ outputHex04 ------
void outputHex04(int hex, const char* pPreText, const char* pPostText, bool line) {
  printf("%s%04X%s%s", pPreText ? pPreText : "", hex, pPostText ? pPostText : "", line ? "\n" : "");
}

//------ outputHex08 ------
void outputHex08(int hex, const char* pPreText, const char* pPostText, bool line) {
  printf("%s%08X%s%s", pPreText ? pPreText : "", hex, pPostText ? pPostText : "", line ? "\n" : "");
}

//------ outputHex16 ------
void outputHex16(int64 hex, const char* pPreText, const char* pPostText, bool line) {
#ifdef _MSC_VER
  printf("%s%016I64X%s%s", pPreText ? pPreText : "", hex, pPostText ? pPostText : "", line ? "\n" : "");
#else
  printf("%s%L016X%s%s", pPreText ? pPreText : "", hex, pPostText ? pPostText : "", line ? "\n" : "");
#endif
}

//------ outputErr ------
void outputErr(Error error, const char* pFuncName, const char* pComment) {
  char* pError;
  KS_getErrorString(error, &pError, KSLNG_DEFAULT);
  if (error)
    printf("ERROR (%08X = \'%s\') -\n  %s: %s\n", error, pError, pFuncName, pComment);
}

//------ inputTxt ------
char* inputTxt(const char* pPreText, const char* pDefault) {
  printf("%s", pPreText);
  if (pDefault && *pDefault)
    printf("[%s] ", pDefault);
  static char _pBuffer[4096];
  for (int i = 0;;) {
    int ch = myGetch();
    if (isprint(ch))
      myPutch(ch);
    if (ch == '\b') {
      if (i > 0) {
        --i;
        printf("\b \b");
      }
      continue;
    }
    if (ch == '\r' || i == 255) {
      printf("\n");
      if (i == 0)
        strcpy(_pBuffer, pDefault);
      else
        _pBuffer[i] = '\0';
      break;
    }
    if (isprint(ch))
      _pBuffer[i++] = ch;
  }
  return _pBuffer;
}

//------ inputDec ------
int inputDec(const char* pPreText, const char* pDefault) {
  int dec = 0;
  sscanf(inputTxt(pPreText, pDefault), "%d", &dec);
  return dec;
}

//------ inputDec ------
int inputDec(const char* pPreText, int defaultVal) {
  char pBuf[32];
  sprintf(pBuf, "%d", defaultVal);
  return inputDec(pPreText, pBuf);
}

//------ inputHex ------
int inputHex(const char* pPreText, const char* pDefault) {
  int hex = 0;
  sscanf(inputTxt(pPreText, pDefault), "%x", &hex);
  return hex;
}

//------ inputHex ------
int inputHex(const char* pPreText, int defaultVal) {
  char pBuf[32];
  sprintf(pBuf, "%x", defaultVal);
  return inputHex(pPreText, pBuf);
}

//------ waitTime ------
void waitTime(uint time) {
  int delay = time / 10000;
  Sleep(delay);
}

#ifndef _USRDLL

//------ main ------
void runSample();

#ifdef _MSC_VER
int __cdecl main() {
#else
int main() {
#endif
  runSample();
  myGetch();
  return 0;
}

#endif // _USRDLL


//--------------------------------------------------------------------------------------------------------------
// The following function allows calling KS_vprintK with a variable parameter list. It cannot be delivered
// in the Kithara library, due to the necessary "C" calling convention, which is not portable between
// different compilers.
//--------------------------------------------------------------------------------------------------------------

Error __cdecl KS_printK(const char* format, ...) {
  va_list args;
  va_start(args, format);
  Error error = KS_vprintK(format, args);
  va_end(args);
  return error;
}


//--------------------------------------------------------------------------------------------------------------
// The following function allows calling KS_vdebugK with a variable parameter list. It cannot be delivered
// in the Kithara library, due to the necessary "C" calling convention, which is not portable between
// different compilers.
//--------------------------------------------------------------------------------------------------------------

Error __cdecl KS_debugK(const char* file, int line, const char* format, ...) {
  va_list args;
  va_start(args, format);
  Error error = KS_vdebugK(file, line, format, args);
  va_end(args);
  return error;
}


//--------------------------------------------------------------------------------------------------------------
// TinyFile - short file class for example programming.
//--------------------------------------------------------------------------------------------------------------

//------ TinyFile ------
TinyFile::TinyFile(const char* pName)
  : hFile_(NULL) {
  strncpy(pFileName_, pName, 256);
}

//------ open ------
bool TinyFile::open(int flags) {
  if (flags & OVERWRITE)
    flags |= WRITEABLE;
  int openFlags = (flags & OVERWRITE) ? CREATE_ALWAYS : (flags & WRITEABLE) ?
                    OPEN_ALWAYS : OPEN_EXISTING;
  int rdwrFlags = (flags & WRITEABLE) ? GENERIC_READ | GENERIC_WRITE : GENERIC_READ;
  hFile_ = CreateFile(pFileName_, rdwrFlags, 0, NULL, openFlags, 0, NULL);

  return (hFile_ == INVALID_HANDLE_VALUE) ? hFile_ = 0, false : true;
}

//------ close ------
void TinyFile::close() {
  if (hFile_) {
    CloseHandle(hFile_);
    hFile_ = NULL;
  }
}

//------ seek ------
int64 TinyFile::seek(int64 pos, int dir) {
  LARGE_INTEGER distance;
  distance.QuadPart = pos;

  LARGE_INTEGER newPosition;

  BOOL ok = SetFilePointerEx(hFile_, distance, &newPosition, dir);
  if (ok != TRUE)
    return -1;

  return newPosition.QuadPart;
}

//------ read ------
bool TinyFile::read(void* pData, int length, int* pLength) {
  if (pLength)
    *pLength = 0;
  if (length < 1)
    return false;
  if (!hFile_ && !open())
    return false;

  DWORD read;
  BOOL ok = ReadFile(hFile_, pData, length, &read, NULL);
  if (pLength)
    *pLength = read;

  if (ok != TRUE)
    return false;

  return true;
}

//------ write ------
bool TinyFile::write(void* pData, int length, int* pLength) {
  if (pLength)
    *pLength = 0;
  if (!hFile_ && !open(WRITEABLE | OVERWRITE))
    return false;
  if (length == 0)
    length = (int)strlen((const char*)pData);

  DWORD written;
  BOOL ok = WriteFile(hFile_, pData, length, &written, NULL) != FALSE;
  if (pLength)
    *pLength = written;
  return ok == TRUE;
}


//--------------------------------------------------------------------------------------------------------------
// TinySerialPort - short COM port class for example programming.
//--------------------------------------------------------------------------------------------------------------

//------ TinySerialPort ------
TinySerialPort::TinySerialPort(const char* pPortName, uint baudRate)
  : TinyFile(pPortName) {
  if (baudRate)
    setBaudRate(baudRate);
}

//------ setBaudRate ------
bool TinySerialPort::setBaudRate(uint baudRate) {
  if (!baudRate)
    return false;
  if (!hFile_ && !open(WRITEABLE | OVERWRITE))
    return false;

  DCB dcb;
  dcb.DCBlength = sizeof(DCB);

  if (!GetCommState(hFile_, &dcb))
    return false;

  dcb.BaudRate = baudRate;
  dcb.ByteSize = 8;                                     // Data size: 8 bit.
  dcb.Parity   = 0;                                     // No parity.
  dcb.StopBits = 0;                                     // 1 stop bit.

  if (!SetCommState(hFile_, &dcb))
    return false;

  return true;
}
