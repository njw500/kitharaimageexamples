// Copyright (c) 2008 by Kithara Software. All rights reserved.

//##############################################################################
//
// File:             _KitharaSmp.cs
//
// Description:      C# helper class for the Kithara �Tool Suite� samples
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ----------------------------------------------------
// a.lun 2003-03-10  CREAT: Original - (file created)
// u.jes 2003-06-26  CHNGD: Some optimization
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:02
//
//##############################################################################

   /*=====================================================================*\
   |                    *** DISCLAIMER OF WARRANTY ***                     |
   |                                                                       |
   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
   \*=====================================================================*/

//##############################################################################
//
// Purpose:
// This class is a helper class for all of our sample programs. It gathers some
// commonly used input and output functions, which are compatible to the other
// programming languages and environments supported.
//
// We are extending the class Kithara here so we can use all functions and
// constants right as we would do in other languages. This allows you to call
// Kithara functions without prepending 'Kithara.' everywhere. In your own
// programs you have two choices: either inheriting from the class Kithara
// or writing 'Kithara.' before every usage of a function or constant.
//
// See file Kithara.cs in sub-directory dev\ for details.
//
//##############################################################################


//------------------------------------------------------------------------------
// _KitharaSmp
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;


public class _KitharaSmp : Kithara {
  //------ millisecond definition ------
  public const int ms = 10000;
  //------ error mask defintion ------
  public const int error_mask = 0x3fff0000;

  // Poor little young C# does not accept default arguments! :-{
  // At least it already knows something of overloaded functions.  ;-}
  public static void outputTxt(string txt, ConsoleColor color) {
    outputTxt(txt, true, color);
  }
  public static void outputTxt(string txt, bool line, ConsoleColor color) {
    ConsoleColor c = Console.ForegroundColor;
    Console.ForegroundColor = color;
    outputTxt(txt, line);
    Console.ForegroundColor = c;
  }
  public static void outputTxt(char[] chars) {
    outputTxt(chars, true);
  }
  public static void outputTxt(byte[] chars) {
    outputTxt(chars, true);
  }
  public static unsafe void outputTxt(sbyte* chars) {
    outputTxt(new String(chars));
  }
  public static unsafe void outputTxt(char* chars) {
    outputTxt((sbyte*)chars);
  }
  public static void outputTxt(string txt) {
    outputTxt(txt, true);
  }
  public static void outputBool(int boolean, string preTxt, string postTxt)
  {
    outputBool(boolean, preTxt, postTxt, true);
  }
  public static void outputBool(int boolean, string preTxt)
  {
    outputBool(boolean, preTxt, "", true);
  }
  public static void outputBool(int boolean)
  {
    outputBool(boolean, "", "", true);
  }
  public static void outputDec(int dec, string preTxt, string postTxt) {
    outputDec(dec, preTxt, postTxt, true);
  }
  public static void outputDec(int dec, string preTxt) {
    outputDec(dec, preTxt, "", true);
  }
  public static void outputDec(int dec) {
    outputDec(dec, "", "", true);
  }
  public static void outputHex(int hex, string preTxt, string postTxt) {
    outputHex(hex, preTxt, postTxt, true);
  }
  public static void outputHex(int hex, string preTxt) {
    outputHex(hex, preTxt, "", true);
  }
  public static void outputHex(int hex)
  {
    outputHex(hex, "", "", true);
  }
  public static unsafe void outputErr(int error, string funcName) {
    outputErr(error, funcName, "");
  }
  public static string inputTxt(string preTxt) {
    return inputTxt(preTxt, "");
  }
  public static int inputDec(string preTxt) {
    return inputDec(preTxt, "0");
  }
  public static int inputHex(string preTxt) {
    return inputHex(preTxt, "0");
  }

  public static string charsToString(char[] chars) {
    StringBuilder sb = new StringBuilder(chars.Length);
    //to filter '\0' chars
    int i = 0;
    while (chars[i] != '\0') {
      sb.Append(chars[i]);
      ++i;
    }
    return sb.ToString();
  }
  public static string bytesToString(byte[] bytes) {
    return Encoding.ASCII.GetString(bytes);
  }
  //------ outputBytes ------
  public static void outputTxt(byte[] bytes, bool line) {
    outputTxt(bytesToString(bytes), line);
  }
  //------ outputChars ------
  public static void outputTxt(char[] chars, bool line) {
    outputTxt(charsToString(chars),line);
  }
  //------ outputTxt ------
  public static void outputTxt(string txt, bool line) {
    if (line)
      Console.WriteLine(txt);
    else
      Console.Write(txt);
  }

  //------ outputBool ------
  public static void outputBool(int boolean, string preTxt, string postTxt, bool line) {
    string txt = preTxt + (boolean == 0 ? "false" : "true") + postTxt;
    outputTxt(txt, line);
  }

  //------ outputDec ------
  public static void outputDec(int dec, string preTxt, string postTxt, bool line) {
    string txt = preTxt + dec + postTxt;
    outputTxt(txt, line);
  }

  //------ outputHex ------
  public static void outputHex(int hex, string preTxt, string postTxt, bool line) {
    string pTxt = string.Format("{0}0x{1:x4}{2}", preTxt, hex, postTxt);
    outputTxt(pTxt, line);
  }

  //------ outputHex08 ------
  public static void outputHex08(int hex, string preTxt, string postTxt, bool line) {
    string pTxt = string.Format("{0}0x{1:x8}{2}", preTxt, hex, postTxt);
    outputTxt(pTxt, line);
  }
  //------ outputHex08 ------
  public static void outputHex08(uint hex, string preTxt, string postTxt, bool line) {
    string pTxt = string.Format("{0}0x{1:x8}{2}", preTxt, hex, postTxt);
    outputTxt(pTxt, line);
  }

  //------ outputErr ------
  public static unsafe void outputErr(int error, string funcName, string comment) {
    byte* buf;
    KS_getErrorString(error, &buf, KSLNG_DEFAULT);
    string msg = new string((sbyte*)buf);

    StringBuilder sb = new StringBuilder();
    sb.Append("ERROR (");
    sb.AppendFormat("{0:X}", error);
    outputTxt(sb + "=" + msg + ") -\n" + funcName + ": " + comment, true);
  }

  //------ outputStruct ------
  public static void outputStruct(object structure, Type structType) {
    System.Text.StringBuilder sb = new System.Text.StringBuilder(structType.ToString() + " \n\r");
    System.Reflection.FieldInfo[] fields = structType.GetFields();

    foreach (System.Reflection.FieldInfo f in fields) {
      sb.Append(f.Name + " = ");

      if (f.FieldType.IsArray && f.FieldType.Name == typeof(char[]).Name)

        Array.ForEach<char>((char[])f.GetValue(structure), delegate(char a) { if (a != '\0') sb.Append(a); });

      else if (f.FieldType.IsArray && f.FieldType.Name == typeof(byte[]).Name)

        Array.ForEach<byte>((byte[])f.GetValue(structure), delegate(byte a) { if (a != 0) sb.Append((char)a); });

      else
        sb.Append(f.GetValue(structure));
      sb.AppendLine(" ");
    }

    outputTxt(sb.ToString());
  }

  public static unsafe void outputStructPointer(void* pStruct, Type structType) {
    outputStruct(
      System.Runtime.InteropServices.Marshal.PtrToStructure(new IntPtr(pStruct), structType),
      structType);
  }

  //------ inputTxt ------
  public static string inputTxt(string preTxt, string defaultTxt) {
    outputTxt(preTxt, false);
    if (defaultTxt != "")
      outputTxt("[" + defaultTxt + "] ", false);
    string line = Console.ReadLine();
    if (line == "")
      line = defaultTxt;
    return line;
  }

  //------ inputDec ------
  public static int inputDec(string preTxt, string defaultDec) {
    try {
      return System.Convert.ToInt32(inputTxt(preTxt, defaultDec));
    }
    catch (System.Exception) {
      return -1;
    }
  }

  //------ inputDec ------
  public static int inputDec(string preTxt, int defaultDec) {
    return inputDec(preTxt, string.Format("{0}", defaultDec));
  }

  //------ inputHex ------
  public static int inputHex(string preTxt, string defaultHex) {
    string txt = inputTxt(preTxt, defaultHex);
    return int.Parse(txt, NumberStyles.HexNumber, new NumberFormatInfo());
  }

  //------ inputHex ------
  public static int inputHex(string preTxt, int defaultHex) {
    return inputHex(preTxt, string.Format("{0:X}", defaultHex));
  }

  //------ myKbhit ------
  public static bool myKbhit() {
    return System.Console.KeyAvailable;
  }

  //------ myGetch ------
  public static char myGetch() {
    return System.Console.ReadKey(true).KeyChar;
  }

  //------ waitTime ------
  public static void waitTime(int time) {
    try {
      Thread.Sleep(time / 10000);
    }
    catch (System.ArgumentOutOfRangeException) {
    }
  }
  //------ switchColor ------
  public static void switchColor(ConsoleColor color) {
    _stdColor_ = Console.ForegroundColor;
    Console.ForegroundColor = color;
  }
  //------ switchStdColor ------
  public static void switchStdColor() {
    Console.ForegroundColor = _stdColor_;
  }
  //------ kserrCode ------
  public static int kserrCode(int error) {
    return error & error_mask;
  }

  private static ConsoleColor _stdColor_;

  public class TinyFile {
    private const int OPENED = 0x0001;
    public const int WRITEABLE = 0x0004;
    public const int OVERWRITE = 0x0100;

    public TinyFile(string name) {
      name_ = name;
    }
    ~TinyFile() {
      close();
    }

    public bool isOpened() {
      return (flags_ & OPENED) != 0;
    }

    public bool open() {
      return open(0);
    }
    public bool open(int flags) {
      if ((flags_ & OPENED) != 0)
        return true;
      if ((flags & OVERWRITE) != 0)
        flags |= WRITEABLE;
      flags_ = flags;
      FileMode mode;
      if ((flags_ & WRITEABLE) != 0)
        if ((flags_ & OVERWRITE) != 0)
          mode = FileMode.Create;
        else
          mode = FileMode.Append;
      else
        mode = FileMode.Open;

      FileAccess access;
      if ((flags_ & WRITEABLE) != 0)
        access = FileAccess.ReadWrite;
      else
        access = FileAccess.Read;

      file_ = new FileStream(name_, mode, access);
      flags_ |= OPENED;
      return true;
    }
    public void close() {
      if ((flags_ & OPENED) != 0) {
        file_.Close();
        flags_ &= ~OPENED;
      }
    }
    public unsafe bool read(string str) {
      return read(str, str.Length);
    }
    public unsafe bool read(string str, int length) {
      return read(str, length, null);
    }
    public unsafe bool read(string str, int length, int* pLength) {
      if ((flags_ & OPENED) == 0 && !open())
        return false;
      byte[] byteArr = new byte[1024];
      byteArr = System.Text.Encoding.ASCII.GetBytes(str);
      int bytesRead = file_.Read(byteArr, 0, length);
      if (pLength != null)
        *pLength = bytesRead;
      return bytesRead != 0;
    }
    public unsafe bool write(string str) {
      return write(str, str.Length);
    }
    public unsafe bool write(string str, int length) {
      return write(str, length, null);
    }
    public unsafe bool write(string str, int length, int* pLength) {
      if ((flags_ & OPENED) == 0 && !open(WRITEABLE | OVERWRITE))
        return false;
      byte[] byteArr = new byte[1024];
      byteArr = System.Text.Encoding.ASCII.GetBytes(str);
      file_.Write(byteArr, 0, length);
      if (pLength != null)
        *pLength = length;
      return true;
    }
    private int flags_;
    private string name_;
    FileStream file_;
  }
}
