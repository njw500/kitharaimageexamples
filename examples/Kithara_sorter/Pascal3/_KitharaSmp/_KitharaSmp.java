/* Copyright (c) 2003 by Kithara Software. All rights reserved. */

//##############################################################################
//
// Module:           _KitharaSmp.java
//
// Descript.:        JAVA source for Kithara Sample utility code
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ----------------------------------------------------
// a.lun 2003-01-06  CREAT: Original - (file created)
// u.jes 2003-06-26  CHNGD: Some optimization
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:03
//
//##############################################################################

   /*=====================================================================*\
   |                    *** DISCLAIMER OF WARRANTY ***                     |
   |                                                                       |
   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
   \*=====================================================================*/

//##############################################################################
//
// Purpose:
// This class is a helper class for all of our sample programs. It gathers some
// commonly used input and output functions, which are compatible to the other
// programming languages and environments supported.
//
// We are extending the class Kithara here so we can use all functions and
// constants right as we would do in other languages. This allows us to call
// Kithara functions without prepending 'Kithara.' everywhere. In your own
// programs you have two choices: either extending the class Kithara
// (inheriting from it) or writing 'Kithara.' before every usage of a function
// or constant.
//
// See file Kithara.java in sub-directory dev\ for details.
//
//##############################################################################


//------------------------------------------------------------------------------
// _KitharaSmp
//------------------------------------------------------------------------------

public class _KitharaSmp extends Kithara
{
  public static final int ms = 10000;

  // Java does not accept default arguments, but overloaded functions instead.

  public static void outputTxt( String txt )
    { outputTxt( txt, true ); }
  public static void outputDec( int dec, String preTxt, String postTxt )
    { outputDec( dec, preTxt, postTxt, true ); }
  public static void outputDec( int dec, String preTxt )
    { outputDec( dec, preTxt, "", true ); }
  public static void outputDec( int dec )
    { outputDec( dec, "", "", true ); }
  public static void outputHex( int hex, String preTxt, String postTxt )
    { outputHex( hex, preTxt, postTxt, true ); }
  public static void outputHex( int hex, String preTxt )
    { outputHex( hex, preTxt, "", true ); }
  public static void outputHex( int hex )
    { outputHex( hex, "", "", true ); }
  public static void outputErr( int error, String funcName )
    { outputErr( error, funcName, "" ); }
  public static String inputTxt( String preTxt )
    { return inputTxt( preTxt, "" ); }
  public static int inputDec( String preTxt )
    { return inputDec( preTxt, "0" ); }
  public static int inputDec( String preTxt, int defaultDec )
    { return inputDec( preTxt, new Integer( defaultDec ).toString() ); }
  public static int inputHex( String preTxt )
    { return inputHex( preTxt, "0" ); }
  public static int inputHex( String preTxt, int defaultHex )
    { return inputHex( preTxt, Integer.toHexString( defaultHex ).toUpperCase() ); }

  //------ outputTxt ------
  public static void outputTxt( String txt, boolean line )
  { if( line == true )
      System.out.println( txt );
    else
      System.out.print( txt );
  }

  //------ outputDec ------
  public static void outputDec( int dec, String preLine, String postLine, boolean line )
  { outputTxt( preLine + new Integer( dec ).toString() + postLine, line );
  }

  //------ outputHex ------
  public static void outputHex( int hex, String preLine, String postLine, boolean line )
  { outputTxt( preLine + Integer.toHexString( hex ).toUpperCase() + postLine, line );
  }

  //------ outputErr ------
  public static void outputErr( int error, String function, String comment )
  { if( error != KS_OK )
    { StrPtr pPtr = new StrPtr();
      KS_getErrorString( error, pPtr, KSLNG_DEFAULT );
      System.out.println( "ERROR (" + Integer.toHexString( error ) + " = " +
                          pPtr.value() + ") -\n  " + function +": " + comment + "\n" );
    }
  }

  //------ inputTxt ------
  public static String inputTxt( String preText, String defaultTxt )
  { try
    { outputTxt( preText, false );
      if( defaultTxt != "" )
        outputTxt( "[" + defaultTxt + "] ", false );
      byte buffer[] = new byte[ 256 ];
      int bytes = System.in.read( buffer );
      if( bytes == 2 && buffer[0] == 0x0d && buffer[1] == 0x0a )
        return defaultTxt;
      else
        return new String( buffer, 0, bytes );
    }
    catch( java.io.IOException e )
    { return null;
    }
  }

  //------ inputDec ------
  public static int inputDec( String preText, String defaultDec )
  { try
    { return Integer.parseInt( inputTxt( preText, defaultDec ).trim() );
    }
    catch( Exception e )
    { return -1;
    }
  }

  //------ inputHex ------
  public static int inputHex( String preText, String defaultHex )
  { try
    { return Integer.parseInt( inputTxt( preText, defaultHex ).trim(), 16 );
    }
    catch( Exception e )
    { return -1;
    }
  }

  //------ waitTime ------
  public static void waitTime( long time )
  { try
    { Thread.sleep( time / ms );
    }
    catch( Exception e )
    {
    }
  }
}
