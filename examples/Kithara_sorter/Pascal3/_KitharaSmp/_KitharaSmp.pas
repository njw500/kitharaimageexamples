// Copyright (c) 1999-2012 by Kithara Software. All rights reserved.

//##############################################################################################################
//
// Module:           _KitharaSmp.pas
//
// Descript.:        PASCAL unit for Kithara Delphi Samples
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// j.ant 1999-10-31  CREAT: Original - (file created)
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:03
//
//##############################################################################################################

   {*=====================================================================*\
   |                    *** DISCLAIMER OF WARRANTY ***                     |
   |                                                                       |
   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
   \*=====================================================================*}

//##############################################################################################################
//
// Purpose:
// The functions in this file are some helper functions for all of our sample programs.
// They are used for input and output, so they are compatible to the other programming languages and
// environments supported.
//
//##############################################################################################################


//--------------------------------------------------------------------------------------------------------------
// _KitharaSmp
//--------------------------------------------------------------------------------------------------------------

unit _KitharaSmp;


//--------------------------------------------------------------------------------------------------------------
// interface
//--------------------------------------------------------------------------------------------------------------

interface

uses
  SysUtils,
  Windows;

const
  ms = 10000;
  s = ms * 1000;
  WRITEABLE = $0004;
  OVERWRITE = $0100;

  //------ Constant for returning no-error ------
  KS_OK                                         = $00000000;

  //------ Language constants ------
  KSLNG_DEFAULT                                 = $00000000;

type
  //------ Common types and structures ------
  Error    = Cardinal;
  Int8     = ShortInt;
  Int16    = Smallint;
  Int32    = Longint;
  Int      = Int32;
  UInt8    = Byte;
  UInt16   = Word;
  UInt32   = Longword;
  UInt     = UInt32;
  Handle   = Pointer;

  PUInt    = ^UInt;
  PHandle  = ^Handle;
  PPointer = ^Pointer;
  PInt64   = ^Int64;

//--------------------------------------------------------------------------------------------------------------
// TinyFile - short file class for example programming.
//--------------------------------------------------------------------------------------------------------------

type
  TinyFile = class
  public
    constructor create(pName: PAnsiChar);

    function isOpened: boolean;
    function open(flags: uint = 0): boolean;
    procedure close;
    function read(pData: Pointer; length: uint; pLength: PUint = nil): boolean;
    function write(pData: array of AnsiChar; length: uint = 0; pLength: PUInt = nil): boolean;
    function handle: uint;

  protected
    hFile_: uint;
    pFileName_: array[0..255] of char;
  end;


//------------------------------------------------------------------------------------------------------
// TinySerialPort - short COM port class for example programming.
//------------------------------------------------------------------------------------------------------

type
  TinySerialPort = class(TinyFile)
  public
    constructor create(pPortName: PAnsiChar; baudRate: uint = 0);

    function setBaudRate(baudRate: uint): boolean;
  end;


//--------------------------------------------------------------------------------------------------------------
// function prototypes
//--------------------------------------------------------------------------------------------------------------

function Sleep(millis: int):
  int; stdcall; external 'KERNEL32.DLL';

procedure outputTxt(txt: string; line: Boolean = true); overload;
procedure outputTxt(txt: PAnsiChar; line: Boolean = true); overload;
procedure outputBool(bool: int; preText: string = ''; postText: string = ''; line: Boolean = true);
procedure outputDec(dec: int; preText: string = ''; postText: string = ''; line: Boolean = true);
procedure outputDec16(dec: Int64; preText: string = ''; postText: string = ''; line: Boolean = true);
procedure outputHex(hex: int; preText: string = ''; postText: string = ''; line: Boolean = true);
procedure outputHex02(hex: int; preText: string = ''; postText: string = ''; line: Boolean = true);
procedure outputHex04(hex: int; preText: string = ''; postText: string = ''; line: Boolean = true);
procedure outputHex08(hex: int; preText: string = ''; postText: string = ''; line: Boolean = true);
procedure outputErr(error: Error; pFuncName: string; pComment: string = '');
procedure waitTime(time: uint);

function inputTxt(preText: string = ''; defaultTxt: string = ''): string;
function inputDec(preText: string = ''; defaultTxt: string = ''): int; overload;
function inputDec(preText: string; defaultDec: int): int; overload;
function inputHex(preText: string = ''; defaultTxt: string = ''): int; overload;
function inputHex(preText: string; defaultHex: int): int; overload;

function myKbhit(): Integer;
function myGetch(): Integer;

// utility function only in Delphi
function hexToInt(hex: string) : int;

//--------------------------------------------------------------------------------------------------------------
// implementation
//--------------------------------------------------------------------------------------------------------------

implementation

function KS_getErrorString(code: Error; var msg: PAnsiChar; language: uint):
  Error; stdcall; external 'KrtsDemo.dll';

//------ myGetch ------
function myGetch() : Integer;
var
  ev: TInputrecord;
  events_read: DWORD;
  value: Integer;

begin
  ReadConsoleInput(GetStdhandle(STD_INPUT_HANDLE), ev, 1, events_read);
  value := 0;

  if ev.Eventtype = key_Event then
  begin
    with ev.Event.KeyEvent do
      value := Integer(AsciiChar);
  end;

  Result := value;
end;

//------ myKbhit ------
function myKbhit() : Integer;
var
  i, numEvents: Cardinal;
  events: array of TInputRecord;
  stdin: THandle;
  value: Integer;

begin
  value := 0;
  stdin := GetStdhandle(STD_INPUT_HANDLE);
  GetNumberOfConsoleInputEvents(stdin, numEvents);
  if numEvents > 0 then
  begin
    SetLength(events, numEvents);
    PeekConsoleInput(stdin, events[0], numEvents, numEvents);
    
    for i:= 0 to numEvents - 1 do
        if (events[i].EventType = key_event)
          and (events[i].Event.KeyEvent.bKeyDown) then
        begin
          value := 1;
          Break;
        end;
  end;
  Result := value;
end;

//------ outputTxt ------
procedure outputTxt(txt: string; line: Boolean); overload;
begin
  if line = true then
    Writeln(txt)
  else
    Write(txt);
end;

//------ outputTxt ------
procedure outputTxt(txt: PAnsiChar; line: Boolean); overload;
begin
  outputTxt(string(txt), line);
end;

//------ outputBool ------
procedure outputBool(bool: int; preText: string; postText: string; line: Boolean);
var boolStr: string;
begin
  if bool = 0 then
    boolStr := 'false'
  else
    boolStr := 'true';

  if line = true then
    Writeln(preText + boolStr + postText)
  else
    Write(preText + boolStr + postText);
end;

//------ outputDec ------
procedure outputDec(dec: int; preText: string; postText: string; line: Boolean);
begin
  if line = true then
    Writeln(preText + IntToStr(dec) + postText)
  else
    Write(preText + IntToStr(dec) + postText);
end;

//------ outputDec16 ------
procedure outputDec16(dec: Int64; preText: string = ''; postText: string = ''; line: Boolean = true);
begin
  if line = true then
    Writeln(preText + IntToStr(dec) + postText)
  else
    Write(preText + IntToStr(dec) + postText);
end;

//------ outputHex ------
procedure outputHex(hex: int; preText: string; postText: string; line: Boolean);
begin
  if line = true then
    Writeln(preText + IntToHex(hex, 4) + postText)
  else
    Write(preText + IntToHex(hex, 4) + postText);
end;

//------ outputHex02 ------
procedure outputHex02(hex: int; preText: string; postText: string; line: Boolean);
begin
  if line = true then
    Writeln(preText + IntToHex(hex, 2) + postText)
  else
    Write(preText + IntToHex(hex, 2) + postText);
end;

//------ outputHex04 ------
procedure outputHex04(hex: int; preText: string; postText: string; line: Boolean);
begin
  if line = true then
    Writeln(preText + IntToHex(hex, 4) + postText)
  else
    Write(preText + IntToHex(hex, 4) + postText);
end;

//------ outputHex08 ------
procedure outputHex08(hex: int; preText: string; postText: string; line: Boolean);
begin
  if line = true then
    Writeln(preText + IntToHex(hex, 8) + postText)
  else
    Write(preText + IntToHex(hex, 8) + postText);
end;

//------ outputErr ------
procedure outputErr(error: Error; pFuncName: string; pComment: string);
var pError: PAnsiChar;
begin
  KS_getErrorString(error, pError, KSLNG_DEFAULT);
  if error <> KS_OK then
  begin
    Writeln('ERROR (' + IntToHex(error, 8) + ' = ' + string(pError) + ') -');
    Writeln('  ' + pFuncName + ': ' + pComment);
  end;
end;

//------ inputTxt ------
function inputTxt(preText, defaultTxt: string): string;
var line: string;
begin
 outputTxt(preText, false);
 if defaultTxt <> '' then
   outputTxt('[' + defaultTxt + '] ', false);
 Readln(line);
 if line = '' then
   line := defaultTxt;
 Result := line
end;

//------ inputDec ------
function inputDec(preText, defaultTxt: string): int; overload;
var dec: int;
  line: string;
begin
  line := inputTxt(preText, defaultTxt);
  dec := StrToInt(line);
  Result := dec;
end;

//------ inputDec ------
function inputDec(preText: string; defaultDec: int): int; overload;
begin
  Result := inputDec(preText, IntToStr(defaultDec));
end;

//------ inputHex ------
function inputHex(preText, defaultTxt: string): int; overload;
var line: string;
begin
  line := AnsiUpperCase(inputTxt(preText, defaultTxt));
  Result := int(hexToInt(line));
end;

//------ inputHex ------
function inputHex(preText: string; defaultHex: int): int; overload;
begin
  Result := inputHex(preText, IntToHex(defaultHex, 1));
end;

//------ waitTime ------
procedure waitTime(time: uint);
begin
  Sleep(time div 10000);
end;

//------ hexToInt ------
function hexToInt(hex: string): int;
var count: int;
    pos: int;
    tempVal: int;
    exponent: int;
begin
  Result := 0;
  pos := 1;
  tempVal := 0;
  exponent := Length(hex) - 1;
  for count := 0 to Length(hex) - 1 do
  begin
    if (Ord(hex[pos]) - 55 > 9) and (Ord(hex[pos]) - 55 < 16) then
      tempVal := Ord(hex[pos]) - 55
    else if (Ord(hex[pos]) - 48 < 10) then
      tempVal := Ord(hex[pos]) - 48
    else
    begin
      Result := -1;
      break;
    end;
    Result := Result + tempVal * 1 shl (exponent * 4);
    pos := pos + 1;
    exponent := exponent - 1;
  end;
end;


//--------------------------------------------------------------------------------------------------------------
// TinyFile - short file class for example programming.
//--------------------------------------------------------------------------------------------------------------

//------ create ------
constructor TinyFile.create(pName: PAnsiChar);
begin
  hFile_ := 0;
  StrCopy(@pFileName_, pName);
end;

//------ open ------
function TinyFile.open(flags: uint): boolean;
var
  openFlags, rdwrFlags: uint;
  hFile: uint;
begin
  if (flags and OVERWRITE) <> 0 then
    flags := flags or WRITEABLE;
  if (flags and OVERWRITE) <> 0 then
    openFlags := CREATE_ALWAYS
  else
    if (flags and WRITEABLE) <> 0 then
      openFlags := OPEN_ALWAYS
    else
      openFlags := OPEN_EXISTING;

  if (flags and WRITEABLE) <> 0 then
    rdwrFlags := GENERIC_READ or GENERIC_WRITE
  else
    rdwrFlags := GENERIC_READ;

  hFile := CreateFile(pFileName_, rdwrFlags, 0, nil, openFlags, 0, 0);

  if hFile = INVALID_HANDLE_VALUE then
  begin
    outputHex(GetLastError());
    result := false
  end
  else
  begin
    hFile_ := hFile;
    result := true;
  end;
end;

//------ close ------
procedure TinyFile.close;
begin
  if hFile_ <> 0 then
  begin
    CloseHandle(hFile_);
    hFile_ := 0;
  end;
end;

//------ read ------
function TinyFile.read(pData: Pointer; length: uint; pLength: PUInt): boolean;
var
  dwRead: DWORD;
  ok: boolean;
begin
  if pLength <> nil then
    pLength^ := 0;
  if length < 1 then
  begin
    result := false;
    exit;
  end;
  if (hFile_ = 0) and (open(0) = false) then
  begin
    result := false;
    exit;
  end;
  ok := ReadFile(hFile_, pData^, length, dwRead, nil);
  if dwRead = 0 then
    ok := false;
  if pLength <> nil then
    pLength^ := dwRead;
  result := ok;
end;

//------ write ------
function TinyFile.write(pData: array of AnsiChar; length: uint; pLength: PUInt): boolean;
var
  dwWritten: DWORD;
  ok: boolean;
begin
  if pLength <> nil then
    pLength^ := 0;
  if (hFile_ = 0) and (open(WRITEABLE or OVERWRITE) = false) then
  begin
    result := false;
    exit;
  end;
  if length = 0 then
    length := strlen(pData);

  ok := WriteFile(hFile_, pData, length, dwWritten, nil);
  if pLength <> nil then
    pLength^ := dwWritten;
  result := ok;
end;

function TinyFile.isOpened: boolean;
begin
  result := hFile_ <> 0;
end;

function TinyFile.handle: uint;
begin
  result := hFile_;
end;


//--------------------------------------------------------------------------------------------------------------
// TinySerialPort - short COM port class for example programming.
//--------------------------------------------------------------------------------------------------------------

//------ TinySerialPort ------
constructor TinySerialPort.create(pPortName: PAnsiChar; baudRate: uint);
begin
  inherited create(pPortName);
  if baudRate <> 0 then
    setBaudRate(baudRate);
end;

//------ setBaudRate ------
function TinySerialPort.setBaudRate(baudRate: uint): boolean;
var
  DCB: TDCB;
begin
  if baudRate = 0 then
  begin
    result := false;
    exit;
  end;
  if (hFile_ = 0) and (open(WRITEABLE or OVERWRITE) = false) then
  begin
    result := false;
    exit;
  end;
  DCB.DCBlength := sizeof(DCB);

  if not GetCommState(hFile_, DCB) then
  begin
    result := false;
    exit;
  end;

  DCB.BaudRate := baudRate;
  DCB.ByteSize := 8;                                    // Data size: 8 bit.
  DCB.Parity   := 0;                                    // No parity.
  DCB.StopBits := 0;                                    // 1 stop bit.

  if not SetCommState(hFile_, DCB) then
  begin
    result := false;
    exit;
  end;

  result := true;
end;

end.


