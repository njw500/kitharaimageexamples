// Copyright (c) 1996-2013 by Kithara Software GmbH. All rights reserved.

//##############################################################################################################
//
// Module:           Kithara.cpp
//
// Description:      Description of the file ...
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// u.jes 2007-08-27  CREAT: Original - (created 27-August-2007 at 14:27:38)
// u.jes 2007-08-27  SAVED: Last saved 27-August-2007 at 14:32:17 - [#4]
//
//##############################################################################################################

#define KITHARA_CPP

//--------------------------------------------------------------------------------------------------------------
// KITHARA
//--------------------------------------------------------------------------------------------------------------

   /*=====================================================================*\
   |                    *** DISCLAIMER OF WARRANTY ***                     |
   |                                                                       |
   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
   \*=====================================================================*/

//##############################################################################################################
//
// Purpose:
// Insert this file in your project, if you wish to use logging or debugging with variable arguments.
//
//##############################################################################################################

#include <stdarg.h>

#include "..\..\dev\KrtsDemo.h"

//--------------------------------------------------------------------------------------------------------------
// The following function allows calling KS_vprintK with a variable parameter list. It cannot be delivered in
// the Kithara library, due to the necessary "C" calling convention, which is not portable between different
// compilers.
//--------------------------------------------------------------------------------------------------------------

Error __cdecl KS_printK(const char* format, ...) {
  va_list args;
  va_start(args, format);
  Error error = KS_vprintK(format, args);
  va_end(args);
  return error;
}

//--------------------------------------------------------------------------------------------------------------
// The following function allows calling KS_vdebugK with a variable parameter list. It cannot be delivered in
// the Kithara library, due to the necessary "C" calling convention, which is not portable between different
// compilers.
//--------------------------------------------------------------------------------------------------------------

Error __cdecl KS_debugK(const char* file, int line, const char* format, ...) {
  va_list args;
  va_start(args, format);
  Error error = KS_vdebugK(file, line, format, args);
  va_end(args);
  return error;
}

#ifdef KS_INCL_RTL_MODULE

//--------------------------------------------------------------------------------------------------------------
// The following function allows calling KSRTL_vsprintf with a variable parameter list. It cannot be delivered
// in the Kithara library, due to the necessary "C" calling convention, which is not portable between different
// compilers.
//--------------------------------------------------------------------------------------------------------------

int __cdecl KSRTL_sprintf(char* pBuf, const char* pFormat, ...) {
  va_list args;
  va_start(args, pFormat);
  int ret = KSRTL_vsprintf(pBuf, pFormat, args);
  va_end(args);
  return ret;
}

//--------------------------------------------------------------------------------------------------------------
// The following function allows calling KSRTL_snprintf with a variable parameter list. It cannot be delivered
// in the Kithara library, due to the necessary "C" calling convention, which is not portable between different
// compilers.
//--------------------------------------------------------------------------------------------------------------

int __cdecl KSRTL_snprintf(char* pBuf, uint size, const char* pFormat, ...) {
  va_list args;
  va_start(args, pFormat);
  int ret = KSRTL_vsnprintf(pBuf, size, pFormat, args);
  va_end(args);
  return ret;
}

#endif // KS_INCL_RTL_MODULE
