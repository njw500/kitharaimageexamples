/* Copyright (c) 2003 by Kithara Software. All rights reserved. */

//##############################################################################################################
//
// Module:           Kithara.cs
//
// Descript.:        C# source for Kithara C# interface
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ----------------------------------------------------
// a.lun 2003-03-14  CREAT: Original - (file created)
// ***** 2013-02-07  SAVED: This file was generated on 2013-02-07 at 14:22:06
//
//##############################################################################################################

   /*=====================================================================*\
   |                    *** DISCLAIMER OF WARRANTY ***                     |
   |                                                                       |
   |       THIS  CODE AND INFORMATION IS PROVIDED "AS IS"  WITHOUT         |
   |       A  WARRANTY OF  ANY KIND, EITHER  EXPRESSED OR  IMPLIED,        |
   |       INCLUDING  BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF         |
   |       MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.        |
   \*=====================================================================*/

//##############################################################################################################
//
// Purpose:
// In order to use the Kithara tools for C# in a simple way, always use this proxy class to have simplified
// access to the delivered functionality. Kithara supports more than one kernel, e.g. normal run-time kernel as
// well as demo kernel and debug kernel. By using this simple proxy class you are not required to change your
// code if you want to change the kernel version for any reason. Everything ever changes is the base class of
// this proxy. This means, that any API functionality not delivered in a special kernel will create a compile
// time error instead of a run-time error.
//
// There are generally two different possible ways to use the Kithara functions:
//
// METHOD 1:
// ===> You define a class which inherits from class 'Kithara'.
//
//      You can call the functions of the Kithara tools simply as you would do in other languages, where these
//      functions are 'global'. This means you don't need an object for using the functions (because all
//      functions are already in the base class of your's).
//
//      public class MyClass : Kithara {
//        ...
//        error = KS_openDriver(...);
//        if (error != KS_OK)
//          ...
//      }
//
// METHOD 2:
// ===> You prepend 'Kithara' at all places you use any of its members.
//
//      You can call the functions of the Kithara tools by prepending 'Kithara' everywhere you use any of its
//      members, including all functions and constants.
//
//      public class MyClass {
//        ...
//        error = Kithara.KS_openDriver(...);
//        if (error != Kithara.KS_OK)
//          ...
//      }
//
//##############################################################################################################

//--------------------------------------------------------------------------------------------------------------
// Kithara
//--------------------------------------------------------------------------------------------------------------

public class Kithara : KrtsDemo {
}
