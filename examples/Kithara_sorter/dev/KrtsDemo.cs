// Copyright (c) 2003-2016 by Kithara Software GmbH. All rights reserved.

//##############################################################################################################
//
// File:             KrtsDemo.cs (v10.00d)
//
// Description:      C# API for Kithara �RealTime Suite�
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// a.lun 2003-03-10  CREAT: Original - (file created)
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:01
//
//##############################################################################################################

//--------------------------------------------------------------------------------------------------------------
// KrtsDemo
//--------------------------------------------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;

public class KrtsDemo {

  //------------------------------------------------------------------------------------------------------------
  // Base Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Code Extraction ------
  public static int KSERROR_CATEGORY(int err)           { return err & 0x3f000000; }
  public static int KSERROR_VALUE(int err)              { return err & 0x00ff0000; }
  public static int KSERROR_CODE(int err)               { return err & 0x3fff0000; }
  public static int KSERROR_SYSTEM_CATEGORY(int err)    { return unchecked((int)(err & 0xc0000000)); }
  public static int KSERROR_SYSTEM_VALUE(int err)       { return err & 0x0000ffff; }
  public static int KSERROR_SYSTEM_CODE(int err)        { return unchecked((int)(err & 0xc000ffff)); }

  //------ Common Constants ------
  public const int KS_INVALID_HANDLE                    = unchecked((int)0x00000000);

  //------ Error Categories ------
  // Operating system errors:
  public const int KSERROR_CATEGORY_OPERATING_SYSTEM    = unchecked((int)0x00000000);

  // Categories 0x01000000 ... 0x0f000000 are free for customer usage!
  public const int KSERROR_CATEGORY_USER                = unchecked((int)0x0f000000);
  public const int KSERROR_CATEGORY_BASE                = unchecked((int)0x10000000);
  public const int KSERROR_CATEGORY_DEAL                = unchecked((int)0x11000000);
  public const int KSERROR_CATEGORY_HANDLING            = unchecked((int)0x17000000);
  public const int KSERROR_CATEGORY_IOPORT              = unchecked((int)0x14000000);
  public const int KSERROR_CATEGORY_MEMORY              = unchecked((int)0x12000000);
  public const int KSERROR_CATEGORY_KERNEL              = unchecked((int)0x18000000);
  public const int KSERROR_CATEGORY_KEYBOARD            = unchecked((int)0x1a000000);
  public const int KSERROR_CATEGORY_INTERRUPT           = unchecked((int)0x13000000);
  public const int KSERROR_CATEGORY_TIMER               = unchecked((int)0x16000000);
  public const int KSERROR_CATEGORY_USB                 = unchecked((int)0x19000000);
  public const int KSERROR_CATEGORY_IDT                 = unchecked((int)0x1b000000);
  public const int KSERROR_CATEGORY_NODE                = unchecked((int)0x1c000000);
  public const int KSERROR_CATEGORY_SOCKET              = unchecked((int)0x1d000000);
  public const int KSERROR_CATEGORY_SYSTEM              = unchecked((int)0x1e000000);
  public const int KSERROR_CATEGORY_ETHERCAT            = unchecked((int)0x1f000000);
  public const int KSERROR_CATEGORY_MF                  = unchecked((int)0x20000000);
  public const int KSERROR_CATEGORY_CAN                 = unchecked((int)0x21000000);
  public const int KSERROR_CATEGORY_PROFIBUS            = unchecked((int)0x22000000);
  public const int KSERROR_CATEGORY_PLC                 = unchecked((int)0x23000000);
  public const int KSERROR_CATEGORY_CANOPEN             = unchecked((int)0x24000000);
  public const int KSERROR_CATEGORY_FLEXRAY             = unchecked((int)0x25000000);
  public const int KSERROR_CATEGORY_PACKET              = unchecked((int)0x26000000);
  public const int KSERROR_CATEGORY_CAMERA              = unchecked((int)0x27000000);
  public const int KSERROR_CATEGORY_TASK                = unchecked((int)0x28000000);
  public const int KSERROR_CATEGORY_SPECIAL             = unchecked((int)0x29000000);
  public const int KSERROR_CATEGORY_XHCI                = unchecked((int)0x2a000000);
  public const int KSERROR_CATEGORY_DISK                = unchecked((int)0x2b000000);
  public const int KSERROR_CATEGORY_FILE                = unchecked((int)0x2c000000);
  public const int KSERROR_CATEGORY_NEXT                = unchecked((int)0x2d000000);
  public const int KSERROR_CATEGORY_EXCEPTION           = unchecked((int)0x3f000000);

  //------ Error Codes ------
  public const int KSERROR_OPERATING_SYSTEM             = unchecked((int)(KSERROR_CATEGORY_BASE+0x00000000));
  public const int KSERROR_UNKNOWN_ERROR_CODE           = unchecked((int)(KSERROR_CATEGORY_BASE+0x00010000));
  public const int KSERROR_UNKNOWN_ERROR_CATEGORY       = unchecked((int)(KSERROR_CATEGORY_BASE+0x00020000));
  public const int KSERROR_UNKNOWN_ERROR_LANGUAGE       = unchecked((int)(KSERROR_CATEGORY_BASE+0x00030000));
  public const int KSERROR_CANNOT_FIND_LIBRARY          = unchecked((int)(KSERROR_CATEGORY_BASE+0x00040000));
  public const int KSERROR_CANNOT_FIND_ADDRESS          = unchecked((int)(KSERROR_CATEGORY_BASE+0x00050000));
  public const int KSERROR_BAD_PARAM                    = unchecked((int)(KSERROR_CATEGORY_BASE+0x00060000));
  public const int KSERROR_BAD_VERSION                  = unchecked((int)(KSERROR_CATEGORY_BASE+0x00070000));
  public const int KSERROR_INTERNAL                     = unchecked((int)(KSERROR_CATEGORY_BASE+0x00080000));
  public const int KSERROR_UNKNOWN                      = unchecked((int)(KSERROR_CATEGORY_BASE+0x00090000));
  public const int KSERROR_FUNCTION_NOT_AVAILABLE       = unchecked((int)(KSERROR_CATEGORY_BASE+0x000a0000));
  public const int KSERROR_DRIVER_NOT_OPENED            = unchecked((int)(KSERROR_CATEGORY_BASE+0x000b0000));
  public const int KSERROR_NOT_ENOUGH_MEMORY            = unchecked((int)(KSERROR_CATEGORY_BASE+0x000c0000));
  public const int KSERROR_CANNOT_OPEN_KERNEL           = unchecked((int)(KSERROR_CATEGORY_BASE+0x000d0000));
  public const int KSERROR_CANNOT_CLOSE_KERNEL          = unchecked((int)(KSERROR_CATEGORY_BASE+0x000e0000));
  public const int KSERROR_BAD_KERNEL_VERSION           = unchecked((int)(KSERROR_CATEGORY_BASE+0x000f0000));
  public const int KSERROR_CANNOT_FIND_KERNEL           = unchecked((int)(KSERROR_CATEGORY_BASE+0x00100000));
  public const int KSERROR_INVALID_HANDLE               = unchecked((int)(KSERROR_CATEGORY_BASE+0x00110000));
  public const int KSERROR_DEVICE_IO_FAILED             = unchecked((int)(KSERROR_CATEGORY_BASE+0x00120000));
  public const int KSERROR_REGISTRY_FAILURE             = unchecked((int)(KSERROR_CATEGORY_BASE+0x00130000));
  public const int KSERROR_CANNOT_START_KERNEL          = unchecked((int)(KSERROR_CATEGORY_BASE+0x00140000));
  public const int KSERROR_CANNOT_STOP_KERNEL           = unchecked((int)(KSERROR_CATEGORY_BASE+0x00150000));
  public const int KSERROR_ACCESS_DENIED                = unchecked((int)(KSERROR_CATEGORY_BASE+0x00160000));
  public const int KSERROR_DEVICE_NOT_OPENED            = unchecked((int)(KSERROR_CATEGORY_BASE+0x00170000));
  public const int KSERROR_FUNCTION_DENIED              = unchecked((int)(KSERROR_CATEGORY_BASE+0x00180000));
  public const int KSERROR_DEVICE_ALREADY_USED          = unchecked((int)(KSERROR_CATEGORY_BASE+0x00190000));
  public const int KSERROR_OUT_OF_MEMORY                = unchecked((int)(KSERROR_CATEGORY_BASE+0x001a0000));
  public const int KSERROR_TOO_MANY_OPEN_PROCESSES      = unchecked((int)(KSERROR_CATEGORY_BASE+0x001b0000));
  public const int KSERROR_SIGNAL_OVERFLOW              = unchecked((int)(KSERROR_CATEGORY_BASE+0x001c0000));
  public const int KSERROR_CANNOT_SIGNAL                = unchecked((int)(KSERROR_CATEGORY_BASE+0x001d0000));
  public const int KSERROR_FILE_NOT_FOUND               = unchecked((int)(KSERROR_CATEGORY_BASE+0x001e0000));
  public const int KSERROR_DEVICE_NOT_FOUND             = unchecked((int)(KSERROR_CATEGORY_BASE+0x001f0000));
  public const int KSERROR_ASSERTION_FAILED             = unchecked((int)(KSERROR_CATEGORY_BASE+0x00200000));
  public const int KSERROR_FUNCTION_FAILED              = unchecked((int)(KSERROR_CATEGORY_BASE+0x00210000));
  public const int KSERROR_OPERATION_ABORTED            = unchecked((int)(KSERROR_CATEGORY_BASE+0x00220000));
  public const int KSERROR_TIMEOUT                      = unchecked((int)(KSERROR_CATEGORY_BASE+0x00230000));
  public const int KSERROR_REBOOT_NECESSARY             = unchecked((int)(KSERROR_CATEGORY_BASE+0x00240000));
  public const int KSERROR_DEVICE_DISABLED              = unchecked((int)(KSERROR_CATEGORY_BASE+0x00250000));
  public const int KSERROR_ATTACH_FAILED                = unchecked((int)(KSERROR_CATEGORY_BASE+0x00260000));
  public const int KSERROR_DEVICE_REMOVED               = unchecked((int)(KSERROR_CATEGORY_BASE+0x00270000));
  public const int KSERROR_TYPE_MISMATCH                = unchecked((int)(KSERROR_CATEGORY_BASE+0x00280000));
  public const int KSERROR_FUNCTION_NOT_IMPLEMENTED     = unchecked((int)(KSERROR_CATEGORY_BASE+0x00290000));
  public const int KSERROR_COMMUNICATION_BROKEN         = unchecked((int)(KSERROR_CATEGORY_BASE+0x002a0000));
  public const int KSERROR_DEVICE_NOT_READY             = unchecked((int)(KSERROR_CATEGORY_BASE+0x00370000));
  public const int KSERROR_NO_RESPONSE                  = unchecked((int)(KSERROR_CATEGORY_BASE+0x00380000));
  public const int KSERROR_OPERATION_PENDING            = unchecked((int)(KSERROR_CATEGORY_BASE+0x00390000));
  public const int KSERROR_PORT_BUSY                    = unchecked((int)(KSERROR_CATEGORY_BASE+0x003a0000));
  public const int KSERROR_UNKNOWN_SYSTEM               = unchecked((int)(KSERROR_CATEGORY_BASE+0x003b0000));
  public const int KSERROR_BAD_CONTEXT                  = unchecked((int)(KSERROR_CATEGORY_BASE+0x003c0000));
  public const int KSERROR_END_OF_FILE                  = unchecked((int)(KSERROR_CATEGORY_BASE+0x003d0000));
  public const int KSERROR_INTERRUPT_ACTIVATION_FAILED  = unchecked((int)(KSERROR_CATEGORY_BASE+0x003e0000));
  public const int KSERROR_INTERRUPT_IS_SHARED          = unchecked((int)(KSERROR_CATEGORY_BASE+0x003f0000));
  public const int KSERROR_NO_REALTIME_ACCESS           = unchecked((int)(KSERROR_CATEGORY_BASE+0x00400000));
  public const int KSERROR_HARDWARE_NOT_SUPPORTED       = unchecked((int)(KSERROR_CATEGORY_BASE+0x00410000));
  public const int KSERROR_TIMER_ACTIVATION_FAILED      = unchecked((int)(KSERROR_CATEGORY_BASE+0x00420000));
  public const int KSERROR_SOCKET_FAILURE               = unchecked((int)(KSERROR_CATEGORY_BASE+0x00430000));
  public const int KSERROR_LINE_ERROR                   = unchecked((int)(KSERROR_CATEGORY_BASE+0x00440000));
  public const int KSERROR_STACK_CORRUPTION             = unchecked((int)(KSERROR_CATEGORY_BASE+0x00450000));
  public const int KSERROR_INVALID_ARGUMENT             = unchecked((int)(KSERROR_CATEGORY_BASE+0x00460000));
  public const int KSERROR_PARSE_ERROR                  = unchecked((int)(KSERROR_CATEGORY_BASE+0x00470000));
  public const int KSERROR_INVALID_IDENTIFIER           = unchecked((int)(KSERROR_CATEGORY_BASE+0x00480000));
  public const int KSERROR_REALTIME_ALREADY_USED        = unchecked((int)(KSERROR_CATEGORY_BASE+0x00490000));
  public const int KSERROR_COMMUNICATION_FAILED         = unchecked((int)(KSERROR_CATEGORY_BASE+0x004a0000));
  public const int KSERROR_INVALID_SIZE                 = unchecked((int)(KSERROR_CATEGORY_BASE+0x004b0000));
  public const int KSERROR_OBJECT_NOT_FOUND             = unchecked((int)(KSERROR_CATEGORY_BASE+0x004c0000));

  //------ Error codes of handling category ------
  public const int KSERROR_CANNOT_CREATE_EVENT          = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00000000));
  public const int KSERROR_CANNOT_CREATE_THREAD         = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00010000));
  public const int KSERROR_CANNOT_BLOCK_THREAD          = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00020000));
  public const int KSERROR_CANNOT_SIGNAL_EVENT          = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00030000));
  public const int KSERROR_CANNOT_POST_MESSAGE          = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00040000));
  public const int KSERROR_CANNOT_CREATE_SHARED         = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00050000));
  public const int KSERROR_SHARED_DIFFERENT_SIZE        = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00060000));
  public const int KSERROR_BAD_SHARED_MEM               = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00070000));
  public const int KSERROR_WAIT_TIMEOUT                 = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00080000));
  public const int KSERROR_EVENT_NOT_SIGNALED           = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x00090000));
  public const int KSERROR_CANNOT_CREATE_CALLBACK       = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x000a0000));
  public const int KSERROR_NO_DATA_AVAILABLE            = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x000b0000));
  public const int KSERROR_REQUEST_NESTED               = unchecked((int)(KSERROR_CATEGORY_HANDLING+0x000c0000));

  //------ Deal error codes ------
  public const int KSERROR_BAD_CUSTNUM                  = unchecked((int)(KSERROR_CATEGORY_DEAL+0x00030000));
  public const int KSERROR_KEY_BAD_FORMAT               = unchecked((int)(KSERROR_CATEGORY_DEAL+0x00060000));
  public const int KSERROR_KEY_BAD_DATA                 = unchecked((int)(KSERROR_CATEGORY_DEAL+0x00080000));
  public const int KSERROR_KEY_PERMISSION_EXPIRED       = unchecked((int)(KSERROR_CATEGORY_DEAL+0x00090000));
  public const int KSERROR_BAD_LICENCE                  = unchecked((int)(KSERROR_CATEGORY_DEAL+0x000a0000));
  public const int KSERROR_CANNOT_LOAD_KEY              = unchecked((int)(KSERROR_CATEGORY_DEAL+0x000c0000));
  public const int KSERROR_BAD_NAME_OR_FIRM             = unchecked((int)(KSERROR_CATEGORY_DEAL+0x000f0000));
  public const int KSERROR_NO_LICENCES_FOUND            = unchecked((int)(KSERROR_CATEGORY_DEAL+0x00100000));
  public const int KSERROR_LICENCE_CAPACITY_EXHAUSTED   = unchecked((int)(KSERROR_CATEGORY_DEAL+0x00110000));
  public const int KSERROR_FEATURE_NOT_LICENSED         = unchecked((int)(KSERROR_CATEGORY_DEAL+0x00120000));

  //------ Exception Codes ------
  public const int KSERROR_DIVIDE_BY_ZERO               = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00000000));
  public const int KSERROR_DEBUG_EXCEPTION              = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00010000));
  public const int KSERROR_NONMASKABLE_INTERRUPT        = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00020000));
  public const int KSERROR_BREAKPOINT                   = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00030000));
  public const int KSERROR_OVERFLOW                     = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00040000));
  public const int KSERROR_BOUND_RANGE                  = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00050000));
  public const int KSERROR_INVALID_OPCODE               = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00060000));
  public const int KSERROR_NO_MATH                      = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00070000));
  public const int KSERROR_DOUBLE_FAULT                 = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00080000));
  public const int KSERROR_COPROCESSOR_SEGMENT_OVERRUN  = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00090000));
  public const int KSERROR_INVALID_TSS                  = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x000a0000));
  public const int KSERROR_SEGMENT_NOT_PRESENT          = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x000b0000));
  public const int KSERROR_STACK_FAULT                  = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x000c0000));
  public const int KSERROR_PROTECTION_FAULT             = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x000d0000));
  public const int KSERROR_PAGE_FAULT                   = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x000e0000));
  public const int KSERROR_FPU_FAULT                    = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00100000));
  public const int KSERROR_ALIGNMENT_CHECK              = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00110000));
  public const int KSERROR_MACHINE_CHECK                = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00120000));
  public const int KSERROR_SIMD_FAULT                   = unchecked((int)(KSERROR_CATEGORY_EXCEPTION+0x00130000));

  //------ Flags ------
  public const int KSF_OPENED                           = unchecked((int)0x01000000);
  public const int KSF_ACTIVE                           = unchecked((int)0x02000000);
  public const int KSF_RUNNING                          = unchecked((int)0x02000000);
  public const int KSF_FINISHED                         = unchecked((int)0x04000000);
  public const int KSF_CANCELED                         = unchecked((int)0x08000000);
  public const int KSF_DISABLED                         = unchecked((int)0x10000000);
  public const int KSF_REQUESTED                        = unchecked((int)0x20000000);
  public const int KSF_TERMINATE                        = unchecked((int)0x00000000);
  public const int KSF_DONT_WAIT                        = unchecked((int)0x00000001);
  public const int KSF_KERNEL_EXEC                      = unchecked((int)0x00000004);
  public const int KSF_ASYNC_EXEC                       = unchecked((int)0x0000000c);
  public const int KSF_DIRECT_EXEC                      = unchecked((int)0x0000000c);
  public const int KSF_DONT_START                       = unchecked((int)0x00000010);
  public const int KSF_ONE_SHOT                         = unchecked((int)0x00000020);
  public const int KSF_SINGLE_SHOT                      = unchecked((int)0x00000020);
  public const int KSF_SAVE_FPU                         = unchecked((int)0x00000040);
  public const int KSF_USE_SSE                          = unchecked((int)0x00000001);
  public const int KSF_USER_EXEC                        = unchecked((int)0x00000080);
  public const int KSF_REALTIME_EXEC                    = unchecked((int)0x00000100);
  public const int KSF_WAIT                             = unchecked((int)0x00000200);
  public const int KSF_ACCEPT_INCOMPLETE                = unchecked((int)0x00000200);
  public const int KSF_REPORT_RESOURCE                  = unchecked((int)0x00000400);
  public const int KSF_USE_TIMEOUTS                     = unchecked((int)0x00000400);
  public const int KSF_FORCE_OVERRIDE                   = unchecked((int)0x00001000);
  public const int KSF_OPTION_ON                        = unchecked((int)0x00001000);
  public const int KSF_OPTION_OFF                       = unchecked((int)0x00002000);
  public const int KSF_FORCE_OPEN                       = unchecked((int)0x00002000);
  public const int KSF_UNLIMITED                        = unchecked((int)0x00004000);
  public const int KSF_DONT_USE_SMP                     = unchecked((int)0x00004000);
  public const int KSF_NO_CONTEXT                       = unchecked((int)0x00008000);
  public const int KSF_HIGH_PRECISION                   = unchecked((int)0x00008000);
  public const int KSF_CONTINUOUS                       = unchecked((int)0x00010000);
  public const int KSF_ZERO_INIT                        = unchecked((int)0x00020000);
  public const int KSF_TX_PDO                           = unchecked((int)0x00010000);
  public const int KSF_RX_PDO                           = unchecked((int)0x00020000);
  public const int KSF_READ                             = unchecked((int)0x00040000);
  public const int KSF_WRITE                            = unchecked((int)0x00080000);
  public const int KSF_USE_SMP                          = unchecked((int)0x00080000);
  public const int KSF_ALTERNATIVE                      = unchecked((int)0x00200000);
  public const int KSF_PDO                              = unchecked((int)0x00040000);
  public const int KSF_SDO                              = unchecked((int)0x00080000);
  public const int KSF_IDN                              = unchecked((int)0x00100000);
  public const int KSF_MANUAL_RESET                     = unchecked((int)0x00200000);
  public const int KSF_RESET_STATE                      = unchecked((int)0x00800000);
  public const int KSF_ISA_BUS                          = unchecked((int)0x00000001);
  public const int KSF_PCI_BUS                          = unchecked((int)0x00000005);
  public const int KSF_PRIO_NORMAL                      = unchecked((int)0x00000000);
  public const int KSF_PRIO_LOW                         = unchecked((int)0x00000001);
  public const int KSF_PRIO_LOWER                       = unchecked((int)0x00000002);
  public const int KSF_PRIO_LOWEST                      = unchecked((int)0x00000003);
  public const int KSF_MESSAGE_PIPE                     = unchecked((int)0x00001000);
  public const int KSF_PIPE_NOTIFY_GET                  = unchecked((int)0x00002000);
  public const int KSF_PIPE_NOTIFY_PUT                  = unchecked((int)0x00004000);
  public const int KSF_ALL_CPUS                         = unchecked((int)0x00000000);
  public const int KSF_READ_ACCESS                      = unchecked((int)0x00000001);
  public const int KSF_WRITE_ACCESS                     = unchecked((int)0x00000002);
  public const int KSF_SIZE_8_BIT                       = unchecked((int)0x00010000);
  public const int KSF_SIZE_16_BIT                      = unchecked((int)0x00020000);
  public const int KSF_SIZE_32_BIT                      = unchecked((int)0x00040000);

  //------ Obsolete flags - do not use! ------
  public const int KSF_ACCEPT_PENDING                   = unchecked((int)0x00000002);
  public const int KSF_DONT_RAISE                       = unchecked((int)0x00200000);

  //------ Constant for returning no-error ------
  public const int KS_OK                                = unchecked((int)0x00000000);

  //------ Language constants ------
  public const int KSLNG_DEFAULT                        = unchecked((int)0x00000000);

  //------ Context type values ------
  public const int USER_CONTEXT                         = unchecked((int)0x00000000);
  public const int INTERRUPT_CONTEXT                    = unchecked((int)0x00000100);
  public const int TIMER_CONTEXT                        = unchecked((int)0x00000200);
  public const int KEYBOARD_CONTEXT                     = unchecked((int)0x00000300);
  public const int USB_BASE                             = unchecked((int)0x00000506);
  public const int PACKET_BASE                          = unchecked((int)0x00000700);
  public const int DEVICE_BASE                          = unchecked((int)0x00000800);
  public const int ETHERCAT_BASE                        = unchecked((int)0x00000900);
  public const int SOCKET_BASE                          = unchecked((int)0x00000a00);
  public const int TASK_BASE                            = unchecked((int)0x00000b00);
  public const int MULTIFUNCTION_BASE                   = unchecked((int)0x00000c00);
  public const int CAN_BASE                             = unchecked((int)0x00000d00);
  public const int PROFIBUS_BASE                        = unchecked((int)0x00000e00);
  public const int CANOPEN_BASE                         = unchecked((int)0x00000f00);
  public const int FLEXRAY_BASE                         = unchecked((int)0x00001000);
  public const int XHCI_BASE                            = unchecked((int)0x00001100);
  public const int V86_BASE                             = unchecked((int)0x00001200);
  public const int PLC_BASE                             = unchecked((int)0x00002000);

  //------ Config code for "Driver" ------
  public const int KSCONFIG_DRIVER_NAME                 = unchecked((int)0x00000001);
  public const int KSCONFIG_FUNCTION_LOGGING            = unchecked((int)0x00000002);

  //------ Config code for "Base" ------
  public const int KSCONFIG_DISABLE_MESSAGE_LOGGING     = unchecked((int)0x00000001);
  public const int KSCONFIG_ENABLE_MESSAGE_LOGGING      = unchecked((int)0x00000002);
  public const int KSCONFIG_FLUSH_MESSAGE_PIPE          = unchecked((int)0x00000003);

  //------ Config code for "Debug" ------

  //------ Commonly used commands ------
  public const int KS_ABORT_XMIT_CMD                    = unchecked((int)0x00000001);
  public const int KS_ABORT_RECV_CMD                    = unchecked((int)0x00000002);
  public const int KS_FLUSH_XMIT_BUF                    = unchecked((int)0x00000003);
  public const int KS_FLUSH_RECV_BUF                    = unchecked((int)0x00000009);
  public const int KS_SET_BAUD_RATE                     = unchecked((int)0x0000000c);

  //------ Pipe contexts ------
  public const int PIPE_PUT_CONTEXT                     = unchecked((int)(USER_CONTEXT+0x00000040));
  public const int PIPE_GET_CONTEXT                     = unchecked((int)(USER_CONTEXT+0x00000041));

  //------ Quick mutex levels ------
  public const int KS_APP_LEVEL                         = unchecked((int)0x00000000);
  public const int KS_DPC_LEVEL                         = unchecked((int)0x00000001);
  public const int KS_ISR_LEVEL                         = unchecked((int)0x00000002);
  public const int KS_RTX_LEVEL                         = unchecked((int)0x00000003);
  public const int KS_CPU_LEVEL                         = unchecked((int)0x00000004);

  //------ Data types ------
  public const int KS_DATATYPE_UNKNOWN                  = unchecked((int)0x00000000);
  public const int KS_DATATYPE_BOOLEAN                  = unchecked((int)0x00000001);
  public const int KS_DATATYPE_INTEGER8                 = unchecked((int)0x00000002);
  public const int KS_DATATYPE_UINTEGER8                = unchecked((int)0x00000005);
  public const int KS_DATATYPE_INTEGER16                = unchecked((int)0x00000003);
  public const int KS_DATATYPE_UINTEGER16               = unchecked((int)0x00000006);
  public const int KS_DATATYPE_INTEGER32                = unchecked((int)0x00000004);
  public const int KS_DATATYPE_UINTEGER32               = unchecked((int)0x00000007);
  public const int KS_DATATYPE_INTEGER64                = unchecked((int)0x00000015);
  public const int KS_DATATYPE_UNSIGNED64               = unchecked((int)0x0000001b);
  public const int KS_DATATYPE_REAL32                   = unchecked((int)0x00000008);
  public const int KS_DATATYPE_REAL64                   = unchecked((int)0x00000011);
  public const int KS_DATATYPE_TINY                     = unchecked((int)0x00000002);
  public const int KS_DATATYPE_BYTE                     = unchecked((int)0x00000005);
  public const int KS_DATATYPE_SHORT                    = unchecked((int)0x00000003);
  public const int KS_DATATYPE_USHORT                   = unchecked((int)0x00000006);
  public const int KS_DATATYPE_INT                      = unchecked((int)0x00000004);
  public const int KS_DATATYPE_UINT                     = unchecked((int)0x00000007);
  public const int KS_DATATYPE_LLONG                    = unchecked((int)0x00000015);
  public const int KS_DATATYPE_ULONG                    = unchecked((int)0x0000001b);
  public const int KS_DATATYPE_VISIBLE_STRING           = unchecked((int)0x00000009);
  public const int KS_DATATYPE_OCTET_STRING             = unchecked((int)0x0000000a);
  public const int KS_DATATYPE_UNICODE_STRING           = unchecked((int)0x0000000b);
  public const int KS_DATATYPE_TIME_OF_DAY              = unchecked((int)0x0000000c);
  public const int KS_DATATYPE_TIME_DIFFERENCE          = unchecked((int)0x0000000d);
  public const int KS_DATATYPE_DOMAIN                   = unchecked((int)0x0000000f);
  public const int KS_DATATYPE_TIME_OF_DAY32            = unchecked((int)0x0000001c);
  public const int KS_DATATYPE_DATE32                   = unchecked((int)0x0000001d);
  public const int KS_DATATYPE_DATE_AND_TIME64          = unchecked((int)0x0000001e);
  public const int KS_DATATYPE_TIME_DIFFERENCE64        = unchecked((int)0x0000001f);
  public const int KS_DATATYPE_BIT1                     = unchecked((int)0x00000030);
  public const int KS_DATATYPE_BIT2                     = unchecked((int)0x00000031);
  public const int KS_DATATYPE_BIT3                     = unchecked((int)0x00000032);
  public const int KS_DATATYPE_BIT4                     = unchecked((int)0x00000033);
  public const int KS_DATATYPE_BIT5                     = unchecked((int)0x00000034);
  public const int KS_DATATYPE_BIT6                     = unchecked((int)0x00000035);
  public const int KS_DATATYPE_BIT7                     = unchecked((int)0x00000036);
  public const int KS_DATATYPE_BIT8                     = unchecked((int)0x00000037);
  public const int KS_DATATYPE_INTEGER24                = unchecked((int)0x00000010);
  public const int KS_DATATYPE_UNSIGNED24               = unchecked((int)0x00000016);
  public const int KS_DATATYPE_INTEGER40                = unchecked((int)0x00000012);
  public const int KS_DATATYPE_UNSIGNED40               = unchecked((int)0x00000018);
  public const int KS_DATATYPE_INTEGER48                = unchecked((int)0x00000013);
  public const int KS_DATATYPE_UNSIGNED48               = unchecked((int)0x00000019);
  public const int KS_DATATYPE_INTEGER56                = unchecked((int)0x00000014);
  public const int KS_DATATYPE_UNSIGNED56               = unchecked((int)0x0000001a);
  public const int KS_DATATYPE_KSHANDLE                 = unchecked((int)0x00000040);

  //------ DataObj type flags ------
  public const int KS_DATAOBJ_READABLE                  = unchecked((int)0x00010000);
  public const int KS_DATAOBJ_WRITEABLE                 = unchecked((int)0x00020000);
  public const int KS_DATAOBJ_ARRAY                     = unchecked((int)0x01000000);
  public const int KS_DATAOBJ_ENUM                      = unchecked((int)0x02000000);
  public const int KS_DATAOBJ_STRUCT                    = unchecked((int)0x04000000);

  //------ Common types and structures ------
  /// <summary>
  /// Buffer structure for functions with buffer access
  /// </summary>
  public class NativeBuffer {
    /// <summary>
    /// Inits buffer with size bytes
    /// </summary>
    /// <param name="size">No. of bytes allocated</param>
    public unsafe NativeBuffer(int size) {
      pBuffer_ = (byte*)Marshal.AllocCoTaskMem(size);
    }

    /// <summary>
    /// Inits buffer with text
    /// </summary>
    /// <param name="text">Text to init</param>
    public unsafe NativeBuffer(string text) {
      int size = text.Length + 1;
      pBuffer_ = (byte*)Marshal.AllocCoTaskMem(size);

      for (int i = 0; i < text.Length; i++)
        pBuffer_[i] = (byte)text[i];
      pBuffer_[text.Length] = 0;
    }

    /// <summary>
    /// Destroys the buffer
    /// </summary>
    ~NativeBuffer() {
      free();
    }

    /// <summary>
    /// Free the memory allocated
    /// </summary>
    public unsafe void free() {
      if (pBuffer_ != null) {
        Marshal.FreeCoTaskMem((IntPtr)pBuffer_);
        pBuffer_ = null;
      }
    }

    /// <summary>
    /// Returns buffer in byte format
    /// </summary>
    /// <returns>Array of bytes representing the buffer</returns>
    public unsafe byte* buffer() {
      return pBuffer_;
    }

    /// <summary>
    /// Returns the buffer in string representation
    /// </summary>
    /// <returns>String representing the buffer</returns>
    public unsafe String asString() {
      return new String((sbyte*)pBuffer_);
    }

    private unsafe byte* pBuffer_;
  }

  //------ Create a Handle type ------
  /// <summary>
  /// Legacy Handle Type used for all handles in KS_xxx functions
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct Handle {
    /// <summary>
    /// Inits Handle with a given value
    /// </summary>
    /// <param name="value">Adress for init</param>
    public Handle(int value)                            { value_ = value; }
    /// <summary>
    /// Inits Handle with a given pointer
    /// </summary>
    /// <param name="value"></param>
    public Handle(void* value)                          { value_ = (int)value; }
    /// <summary>
    /// Returns pointer of this Handle instance
    /// </summary>
    /// <returns>void* pointer</returns>
    public int value()                                  { return value_; }
    /// <summary>
    /// Holds the Handle value
    /// </summary>
    private Int32 value_;
  };

  /// <summary>
  /// Handle Type used for all handles in KS_xxx functions
  /// </summary>
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct KSHandle {
    /// <summary>
    /// Inits Handle with a given value
    /// </summary>
    /// <param name="value">Adress for init</param>
    public KSHandle(int value)                          { value_ = value; }
    /// <summary>
    /// Holds the Handle value
    /// </summary>
    private Int32 value_;
  };

  //------ Delegate for using callbacks ------
  public unsafe delegate int CallBackRoutine(void* pArgs, void* pContext);

  //------ Delegate for creating threads ------
  public unsafe delegate int CallBackProc(void* pArgs);

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct HandlerState {
    public int requested;
    public int performed;
    public int state;
    public int error;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSTimeOut {
    public int baseTime;
    public int charTime;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSDeviceInfo {
    public int structSize;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pDeviceName;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pDeviceDesc;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pDeviceVendor;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pFriendlyName;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pHardwareId;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pClassName;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pInfPath;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pDriverDesc;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pDriverVendor;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pDriverDate;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pDriverVersion;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pServiceName;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pServiceDisp;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pServiceDesc;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] pServicePath;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSSystemInformation {
    public int structSize;
    public int numberOfCPUs;
    public int numberOfSharedCPUs;
    public int kiloBytesOfRAM;
    public byte isDll64Bit;
    public byte isSys64Bit;
    public byte isStableVersion;
    public byte isLogVersion;
    public int dllVersion;
    public int dllBuildNumber;
    public int sysVersion;
    public int sysBuildNumber;
    public int systemVersion;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
    public byte[] pSystemName;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
    public byte[] pServicePack;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
    public byte[] pDllPath;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
    public byte[] pSysPath;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSProcessorInformation {
    public int structSize;
    public int index;
    public int group;
    public int number;
    public int currentTemperature;
    public int allowedTemperature;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSSharedMemInfo {
    public int structSize;
    public Handle hHandle;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
    public byte[] pName;
    public int size;
    public void* pThisPtr;
  };

  //------ Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PipeUserContext {
    public int ctxType;
    public Handle hPipe;
  };

  //------ Common functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openDriver(
    string customerNumber);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeDriver(
    );
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getDriverVersion(
    int* pVersion);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getErrorString(
    int code, byte** msg, uint language);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_addErrorString(
    int code, string msg, uint language);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getDriverConfig(
    string module, int configType, void* pData);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setDriverConfig(
    string module, int configType, void* pData);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getSystemInformation(
    ref KSSystemInformation pSystemInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getProcessorInformation(
    int index, ref KSProcessorInformation pProcessorInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeHandle(
    Handle handle, int flags);

  //------ Debugging & test ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_showMessage(
    int ksError, string msg);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_logMessage(
    int msgType, string msgText);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_bufMessage(
    int msgType, void* pBuffer, int length, string msgText);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_dbgMessage(
    string fileName, int line, string msgText);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_beep(
    int freq, int duration);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_throwException(
    int error, string pText, string pFile, int line);

  //------ Device handling ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumDevices(
    string deviceType, int index, byte* pDeviceNameBuf, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumDevicesEx(
    string className, string busType, Handle hEnumerator, int index, byte* pDeviceNameBuf, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getDevices(
    string className, string busType, Handle hEnumerator, int* pCount, void** pBuf, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getDeviceInfo(
    string deviceName, ref KSDeviceInfo pDeviceInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_updateDriver(
    string deviceName, string fileName, int flags);

  //------ Threads & priorities ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createThread(
    CallBackProc procName, void* pArgs, Handle* phThread);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeThread(
    );
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setThreadPrio(
    int prio);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getThreadPrio(
    int* pPrio);

  //------ Shared memory ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createSharedMem(
    void** ppAppPtr, void** ppSysPtr, string name, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_freeSharedMem(
    void* pAppPtr);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getSharedMem(
    void** ppPtr, string name);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_readMem(
    void* pBuffer, void* pAppPtr, int size, int offset);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_writeMem(
    void* pBuffer, void* pAppPtr, int size, int offset);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createSharedMemEx(
    Handle* phHandle, string name, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_freeSharedMemEx(
    Handle hHandle, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getSharedMemEx(
    Handle hHandle, void** pPtr, int flags);

  //------ Events ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createEvent(
    Handle* phEvent, string name, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeEvent(
    Handle hEvent);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setEvent(
    Handle hEvent);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_resetEvent(
    Handle hEvent);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_pulseEvent(
    Handle hEvent);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_waitForEvent(
    Handle hEvent, int flags, int timeout);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getEventState(
    Handle hEvent, int* pState);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_postMessage(
    Handle hWnd, uint msg, uint wP, uint lP);

  //------ CallBacks ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createCallBack(
    Handle* phCallBack, CallBackRoutine routine, void* pArgs, int flags, int prio);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeCallBack(
    Handle hCallBack);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execCallBack(
    Handle hCallBack, void* pContext, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getCallState(
    Handle hSignal, ref HandlerState pState);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_signalObject(
    Handle hObject, void* pContext, int flags);

  //------ Pipes ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createPipe(
    Handle* phPipe, string name, int itemSize, int itemCount, Handle hNotify, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removePipe(
    Handle hPipe);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_flushPipe(
    Handle hPipe, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getPipe(
    Handle hPipe, void* pBuffer, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_putPipe(
    Handle hPipe, void* pBuffer, int length, int* pLength, int flags);

  //------ Quick mutexes ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createQuickMutex(
    Handle* phMutex, int level, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeQuickMutex(
    Handle hMutex);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_requestQuickMutex(
    Handle hMutex);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_releaseQuickMutex(
    Handle hMutex);

  //------------------------------------------------------------------------------------------------------------
  // Kernel Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_RTX_TOO_MANY_FUNCTIONS       = unchecked((int)(KSERROR_CATEGORY_KERNEL+0x00000000));
  public const int KSERROR_CANNOT_EXECUTE_RTX_CALL      = unchecked((int)(KSERROR_CATEGORY_KERNEL+0x00010000));
  public const int KSERROR_CANNOT_PREPARE_RTX           = unchecked((int)(KSERROR_CATEGORY_KERNEL+0x00020000));
  public const int KSERROR_RTX_BAD_SYSTEM_CALL          = unchecked((int)(KSERROR_CATEGORY_KERNEL+0x00030000));
  public const int KSERROR_CANNOT_LOAD_KERNEL           = unchecked((int)(KSERROR_CATEGORY_KERNEL+0x00040000));
  public const int KSERROR_CANNOT_RELOCATE_KERNEL       = unchecked((int)(KSERROR_CATEGORY_KERNEL+0x00050000));
  public const int KSERROR_KERNEL_ALREADY_LOADED        = unchecked((int)(KSERROR_CATEGORY_KERNEL+0x00060000));
  public const int KSERROR_KERNEL_NOT_LOADED            = unchecked((int)(KSERROR_CATEGORY_KERNEL+0x00070000));

  //------ Flags ------
  public const int KSF_ANALYSE_ONLY                     = unchecked((int)0x00010000);
  public const int KSF_FUNC_TABLE_GIVEN                 = unchecked((int)0x00020000);
  public const int KSF_FORCE_RELOC                      = unchecked((int)0x00040000);
  public const int KSF_LOAD_DEPENDENCIES                = unchecked((int)0x00001000);
  public const int KSF_EXEC_ENTRY_POINT                 = unchecked((int)0x00002000);

  //------ Kernel commands ------
  public const int KS_GET_KERNEL_BASE_ADDRESS           = unchecked((int)0x00000001);

  //------ Relocation by byte-wise instruction mapping ------

  //------ Relocation by loading DLL into kernel-space ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_registerKernelAddress(
    string name, CallBackProc appFunction, CallBackProc sysFunction, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_loadKernel(
    Handle* phKernel, string dllName, string initProcName, void* pArgs, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_loadKernelFromBuffer(
    Handle* phKernel, string dllName, void* pBuffer, int length, string initProcName, void* pArgs, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_freeKernel(
    Handle hKernel);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execKernelFunction(
    Handle hKernel, string name, void* pArgs, void* pContext, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getKernelFunction(
    Handle hKernel, string name, void** pRelocated, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumKernelFunctions(
    Handle hKernel, int index, byte* pName, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execKernelCommand(
    Handle hKernel, int command, void* pParam, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createKernelCallBack(
    Handle* phCallBack, Handle hKernel, string name, void* pArgs, int flags, int prio);

  //------ Helper functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execSyncFunction(
    Handle hHandler, CallBackProc proc, void* pArgs, int flags);

  //------ Memory functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_memCpy(
    void* pDst, void* pSrc, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_memMove(
    void* pDst, void* pSrc, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_memSet(
    void* pMem, int value, int size, int flags);

  //------ Interlocked functions ------

  //------------------------------------------------------------------------------------------------------------
  // IoPort Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_CANNOT_ENABLE_IO_RANGE       = unchecked((int)(KSERROR_CATEGORY_IOPORT+0x00000000));
  public const int KSERROR_BUS_NOT_FOUND                = unchecked((int)(KSERROR_CATEGORY_IOPORT+0x00010000));
  public const int KSERROR_BAD_SLOT_NUMBER              = unchecked((int)(KSERROR_CATEGORY_IOPORT+0x00020000));

  //------ Number of address entries in PCI data ------
  public const int KS_PCI_TYPE0_ADDRESSES               = unchecked((int)0x00000006);
  public const int KS_PCI_TYPE1_ADDRESSES               = unchecked((int)0x00000002);
  public const int KS_PCI_TYPE2_ADDRESSES               = unchecked((int)0x00000004);

  //------ Bit encodings for KSPciBusData.headerType ------
  public const int KSF_PCI_MULTIFUNCTION                = unchecked((int)0x00000080);
  public const int KSF_PCI_DEVICE_TYPE                  = unchecked((int)0x00000000);
  public const int KSF_PCI_PCI_BRIDGE_TYPE              = unchecked((int)0x00000001);
  public const int KSF_PCI_CARDBUS_BRIDGE_TYPE          = unchecked((int)0x00000002);

  //------ Bit encodings for KSPciBusData.command ------
  public const int KSF_PCI_ENABLE_IO_SPACE              = unchecked((int)0x00000001);
  public const int KSF_PCI_ENABLE_MEMORY_SPACE          = unchecked((int)0x00000002);
  public const int KSF_PCI_ENABLE_BUS_MASTER            = unchecked((int)0x00000004);
  public const int KSF_PCI_ENABLE_SPECIAL_CYCLES        = unchecked((int)0x00000008);
  public const int KSF_PCI_ENABLE_WRITE_AND_INVALIDATE  = unchecked((int)0x00000010);
  public const int KSF_PCI_ENABLE_VGA_COMPATIBLE_PALETTE 
                                                        = unchecked((int)0x00000020);
  public const int KSF_PCI_ENABLE_PARITY                = unchecked((int)0x00000040);
  public const int KSF_PCI_ENABLE_WAIT_CYCLE            = unchecked((int)0x00000080);
  public const int KSF_PCI_ENABLE_SERR                  = unchecked((int)0x00000100);
  public const int KSF_PCI_ENABLE_FAST_BACK_TO_BACK     = unchecked((int)0x00000200);

  //------ Bit encodings for KSPciBusData.status ------
  public const int KSF_PCI_STATUS_CAPABILITIES_LIST     = unchecked((int)0x00000010);
  public const int KSF_PCI_STATUS_FAST_BACK_TO_BACK     = unchecked((int)0x00000080);
  public const int KSF_PCI_STATUS_DATA_PARITY_DETECTED  = unchecked((int)0x00000100);
  public const int KSF_PCI_STATUS_DEVSEL                = unchecked((int)0x00000600);
  public const int KSF_PCI_STATUS_SIGNALED_TARGET_ABORT = unchecked((int)0x00000800);
  public const int KSF_PCI_STATUS_RECEIVED_TARGET_ABORT = unchecked((int)0x00001000);
  public const int KSF_PCI_STATUS_RECEIVED_MASTER_ABORT = unchecked((int)0x00002000);
  public const int KSF_PCI_STATUS_SIGNALED_SYSTEM_ERROR = unchecked((int)0x00004000);
  public const int KSF_PCI_STATUS_DETECTED_PARITY_ERROR = unchecked((int)0x00008000);

  //------ Bit encodings for KSPciBusData.baseAddresses ------
  public const int KSF_PCI_ADDRESS_IO_SPACE             = unchecked((int)0x00000001);
  public const int KSF_PCI_ADDRESS_MEMORY_TYPE_MASK     = unchecked((int)0x00000006);
  public const int KSF_PCI_ADDRESS_MEMORY_PREFETCHABLE  = unchecked((int)0x00000008);

  //------ PCI types ------
  public const int KSF_PCI_TYPE_32BIT                   = unchecked((int)0x00000000);
  public const int KSF_PCI_TYPE_20BIT                   = unchecked((int)0x00000002);
  public const int KSF_PCI_TYPE_64BIT                   = unchecked((int)0x00000004);

  //------ Resource types ------
  public const int KSRESOURCE_TYPE_NONE                 = unchecked((int)0x00000000);
  public const int KSRESOURCE_TYPE_PORT                 = unchecked((int)0x00000001);
  public const int KSRESOURCE_TYPE_INTERRUPT            = unchecked((int)0x00000002);
  public const int KSRESOURCE_TYPE_MEMORY               = unchecked((int)0x00000003);
  public const int KSRESOURCE_TYPE_STRING               = unchecked((int)0x000000ff);

  //------ KSRESOURCE_SHARE ------
  public const int KSRESOURCE_SHARE_UNDETERMINED        = unchecked((int)0x00000000);
  public const int KSRESOURCE_SHARE_DEVICEEXCLUSIVE     = unchecked((int)0x00000001);
  public const int KSRESOURCE_SHARE_DRIVEREXCLUSIVE     = unchecked((int)0x00000002);
  public const int KSRESOURCE_SHARE_SHARED              = unchecked((int)0x00000003);

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_PORT ------
  public const int KSRESOURCE_MODE_MEMORY               = unchecked((int)0x00000000);
  public const int KSRESOURCE_MODE_IO                   = unchecked((int)0x00000001);

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_INTERRUPT ------
  public const int KSRESOURCE_MODE_LEVELSENSITIVE       = unchecked((int)0x00000000);
  public const int KSRESOURCE_MODE_LATCHED              = unchecked((int)0x00000001);

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_MEMORY ------
  public const int KSRESOURCE_MODE_READ_WRITE           = unchecked((int)0x00000000);
  public const int KSRESOURCE_MODE_READ_ONLY            = unchecked((int)0x00000001);
  public const int KSRESOURCE_MODE_WRITE_ONLY           = unchecked((int)0x00000002);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSPciBusData {
    public ushort vendorID;
    public ushort deviceID;
    public ushort command;
    public ushort status;
    public byte revisionID;
    public byte progIf;
    public byte subClass;
    public byte baseClass;
    public byte cacheLineSize;
    public byte latencyTimer;
    public byte headerType;
    public byte BIST;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public uint[] pBaseAddresses;
    public uint CIS;
    public ushort subVendorID;
    public ushort subSystemID;
    public uint baseROMAddress;
    public uint capabilityList;
    public uint reserved;
    public byte interruptLine;
    public byte interruptPin;
    public byte minimumGrant;
    public byte maximumLatency;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 192)]
    public byte[] pDeviceSpecific;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSResourceItem {
    public uint flags;
    public uint data0;
    public uint data1;
    public uint data2;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSResourceInfo {
    public uint busType;
    public uint busNumber;
    public uint version;
    public uint itemCount;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
    public KSResourceItem[] pItems;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSRangeResourceItem {
    public uint flags;
    public uint address;
    public uint reserved;
    public uint length;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSInterruptResourceItem {
    public uint flags;
    public uint level;
    public uint vector;
    public uint affinity;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSResourceInfoEx {
    public int busType;
    public int busNumber;
    public int version;
    public int itemCount;
    public KSRangeResourceItem range0;
    public KSRangeResourceItem range1;
    public KSRangeResourceItem range2;
    public KSRangeResourceItem range3;
    public KSRangeResourceItem range4;
    public KSRangeResourceItem range5;
    public KSInterruptResourceItem interruptItem;
    public KSResourceItem reserved;
  };

  //------ I/O port access functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enableIoRange(
    uint ioPortBase, int count, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_readIoPort(
    uint ioPort, int* pValue, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_writeIoPort(
    uint ioPort, uint value, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe uint KS_inpb(
    uint ioPort);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe uint KS_inpw(
    uint ioPort);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe uint KS_inpd(
    uint ioPort);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe void KS_outpb(
    uint ioPort, uint value);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe void KS_outpw(
    uint ioPort, uint value);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe void KS_outpd(
    uint ioPort, uint value);

  //------ Bus data & resource info ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getBusData(
    ref KSPciBusData pBusData, int busNumber, int slotNumber, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setBusData(
    ref KSPciBusData pBusData, int busNumber, int slotNumber, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getResourceInfoEx(
    string name, ref KSResourceInfoEx pInfo);

  //------ Quiet section (obsolete) ------

  //------------------------------------------------------------------------------------------------------------
  // Memory Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_CANNOT_MAP_PHYSMEM           = unchecked((int)(KSERROR_CATEGORY_MEMORY+0x00000000));
  public const int KSERROR_CANNOT_UNMAP_PHYSMEM         = unchecked((int)(KSERROR_CATEGORY_MEMORY+0x00010000));
  public const int KSERROR_PHYSMEM_DIFFERENT_SIZE       = unchecked((int)(KSERROR_CATEGORY_MEMORY+0x00020000));
  public const int KSERROR_CANNOT_ALLOC_PHYSMEM         = unchecked((int)(KSERROR_CATEGORY_MEMORY+0x00030000));
  public const int KSERROR_CANNOT_LOCK_MEM              = unchecked((int)(KSERROR_CATEGORY_MEMORY+0x00040000));
  public const int KSERROR_CANNOT_UNLOCK_MEM            = unchecked((int)(KSERROR_CATEGORY_MEMORY+0x00050000));

  //------ Flags ------
  public const int KSF_MAP_INTERNAL                     = unchecked((int)0x00000000);

  //------ Physical memory management functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_mapDeviceMem(
    void** ppAppPtr, void** ppSysPtr, ref KSResourceInfoEx pInfo, int index, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_mapPhysMem(
    void** ppAppPtr, void** ppSysPtr, uint physAddr, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_unmapPhysMem(
    void* pAppPtr);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_allocPhysMem(
    void** ppAppPtr, void** ppSysPtr, int* physAddr, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_freePhysMem(
    void* pAppPtr);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_copyPhysMem(
    uint physAddr, void* pBuffer, uint size, uint offset, int flags);

  // ATTENTION: functions for getting shared memory: see 'Base Module'

  //------------------------------------------------------------------------------------------------------------
  // Clock Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_NO_RDTSC_AVAILABLE           = unchecked((int)(KSERROR_CATEGORY_SPECIAL+0x00010000));

  //------ Flags ------
  public const int KSF_USE_GIVEN_FREQ                   = unchecked((int)0x00010000);
  public const int KSF_CLOCK_DELEGATE                   = unchecked((int)0x00000001);
  public const int KSF_CLOCK_CONVERT_ONLY               = unchecked((int)0x00000002);

  //------ Clock source constants (for 'clockId') ------
  public const int KS_CLOCK_DEFAULT                     = unchecked((int)0x00000000);
  public const int KS_CLOCK_MEASURE_SYSTEM_TIMER        = unchecked((int)0x00000001);
  public const int KS_CLOCK_MEASURE_CPU_COUNTER         = unchecked((int)0x00000002);
  public const int KS_CLOCK_MEASURE_APIC_TIMER          = unchecked((int)0x00000003);
  public const int KS_CLOCK_MEASURE_PM_TIMER            = unchecked((int)0x00000004);
  public const int KS_CLOCK_MEASURE_PC_TIMER            = unchecked((int)0x00000005);
  public const int KS_CLOCK_MEASURE_INTERVAL_COUNTER    = unchecked((int)0x00000006);
  public const int KS_CLOCK_MEASURE_HPET                = unchecked((int)0x00000007);
  public const int KS_CLOCK_MEASURE_HIGHEST             = unchecked((int)0x00000008);
  public const int KS_CLOCK_CONVERT_HIGHEST             = unchecked((int)0x00000009);
  public const int KS_CLOCK_PC_TIMER                    = unchecked((int)0x0000000a);
  public const int KS_CLOCK_MACHINE_TIME                = unchecked((int)0x0000000b);
  public const int KS_CLOCK_ABSOLUTE_TIME               = unchecked((int)0x0000000c);
  public const int KS_CLOCK_ABSOLUTE_TIME_LOCAL         = unchecked((int)0x0000000d);
  public const int KS_CLOCK_MICROSECONDS                = unchecked((int)0x0000000e);
  public const int KS_CLOCK_UNIX_TIME                   = unchecked((int)0x0000000f);
  public const int KS_CLOCK_UNIX_TIME_LOCAL             = unchecked((int)0x00000010);
  public const int KS_CLOCK_MILLISECONDS                = unchecked((int)0x00000011);
  public const int KS_CLOCK_DOS_TIME                    = unchecked((int)0x00000012);
  public const int KS_CLOCK_DOS_TIME_LOCAL              = unchecked((int)0x00000013);
  public const int KS_CLOCK_IEEE1588_TIME               = unchecked((int)0x00000014);
  public const int KS_CLOCK_IEEE1588_CONVERT            = unchecked((int)0x00000015);
  public const int KS_CLOCK_TIME_OF_DAY                 = unchecked((int)0x00000016);
  public const int KS_CLOCK_TIME_OF_DAY_LOCAL           = unchecked((int)0x00000017);
  public const int KS_CLOCK_FIRST_USER                  = unchecked((int)0x00000018);

  //------ Obsolete clock source constant (use KS_CLOCK_MACHINE_TIME instead!) ------
  public const int KS_CLOCK_SYSTEM_TIME                 = unchecked((int)0x0000000b);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSClockSource {
    public int structSize;
    public int flags;
    public ulong frequency;
    public ulong offset;
    public uint baseClockId;
  };

  //------ Clock functions - Obsolete! Do not use! ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_calibrateMachineTime(
    int* pFrequency, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getSystemTicks(
    long* pSystemTicks);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getSystemTime(
    long* pSystemTime);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getMachineTime(
    long* pMachineTime);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getAbsTime(
    long* pAbsTime);

  //------ Clock functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getClock(
    long* pClock, int clockId);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getClockSource(
    ref KSClockSource pClockSource, int clockId);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setClockSource(
    ref KSClockSource pClockSource, int clockId);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_microDelay(
    int delay);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_convertClock(
    long* pValue, int clockIdFrom, int clockIdTo, int flags);

  //------------------------------------------------------------------------------------------------------------
  // System Module
  //------------------------------------------------------------------------------------------------------------

  //------ System contexts ------
  public const int SYSTEMHANDLER_CONTEXT                = unchecked((int)(USER_CONTEXT+0x00000030));

  //------ System events ------
  public const int KS_SYSTEM_CRASH                      = unchecked((int)0x00000001);
  public const int KS_SYSTEM_SHUTDOWN                   = unchecked((int)0x00000002);
  public const int KS_SYSTEM_PROCESS_ATTACH             = unchecked((int)0x00000010);
  public const int KS_SYSTEM_PROCESS_DETACH             = unchecked((int)0x00000020);
  public const int KS_SYSTEM_PROCESS_ABORT              = unchecked((int)0x00000040);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct SystemHandlerUserContext {
    public int ctxType;
    public int eventType;
    public int handlerProcessId;
    public int currentProcessId;
  };

  //------ System handling functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installSystemHandler(
    int eventMask, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeSystemHandler(
    int eventMask);

  //------------------------------------------------------------------------------------------------------------
  // Device Module
  //------------------------------------------------------------------------------------------------------------

  //------ Flags ------
  public const int KSF_SERIAL_PORT                      = unchecked((int)0x00200000);

  //------ Function codes ------
  public const int KS_DEVICE_CREATE                     = unchecked((int)0x00000000);
  public const int KS_DEVICE_CLOSE                      = unchecked((int)0x00000001);
  public const int KS_DEVICE_READ                       = unchecked((int)0x00000002);
  public const int KS_DEVICE_WRITE                      = unchecked((int)0x00000003);
  public const int KS_DEVICE_CONTROL                    = unchecked((int)0x00000004);
  public const int KS_DEVICE_CLEANUP                    = unchecked((int)0x00000005);
  public const int KS_DEVICE_CANCEL_REQUEST             = unchecked((int)0x00010000);

  //------ Return codes ------
  public const int KS_RETURN_SUCCESS                    = unchecked((int)0x00000000);
  public const int KS_RETURN_UNSUCCESSFUL               = unchecked((int)0x00000001);
  public const int KS_RETURN_INVALID_DEVICE_REQUEST     = unchecked((int)0x00000002);
  public const int KS_RETURN_INVALID_PARAMETER          = unchecked((int)0x00000003);
  public const int KS_RETURN_TIMEOUT                    = unchecked((int)0x00000004);
  public const int KS_RETURN_CANCELLED                  = unchecked((int)0x00000005);
  public const int KS_RETURN_PENDING                    = unchecked((int)0x00000006);
  public const int KS_RETURN_DELETE_PENDING             = unchecked((int)0x00000007);
  public const int KS_RETURN_BUFFER_TOO_SMALL           = unchecked((int)0x00000008);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct DeviceUserContext {
    public int ctxType;
    public Handle hRequest;
    public int functionCode;
    public int controlCode;
    public byte* pBuffer;
    public uint size;
    public int returnCode;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSDeviceFunctions {
    public Handle hOnCreate;
    public Handle hOnClose;
    public Handle hOnRead;
    public Handle hOnWrite;
    public Handle hOnControl;
    public Handle hOnCleanup;
  };

  //------ Device functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createDevice(
    Handle* phDevice, string name, ref KSDeviceFunctions pFunctions, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeDevice(
    Handle hDevice);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_resumeDevice(
    Handle hDevice, Handle hRequest, byte* pBuffer, int size, int returnCode, int flags);

  //------------------------------------------------------------------------------------------------------------
  // Keyboard Module
  //------------------------------------------------------------------------------------------------------------

  //------ Flags ------
  public const int KSF_SIGNAL_MAKE                      = unchecked((int)0x00000001);
  public const int KSF_SIGNAL_BREAK                     = unchecked((int)0x00000002);
  public const int KSF_IGNORE_KEY                       = unchecked((int)0x00000010);
  public const int KSF_MODIFY_KEY                       = unchecked((int)0x00000020);

  //------ Key mask modifier ------
  public const int KSF_KEY_SHIFT                        = unchecked((int)0x00000011);
  public const int KSF_KEY_SHIFTR                       = unchecked((int)0x00000001);
  public const int KSF_KEY_SHIFTL                       = unchecked((int)0x00000010);
  public const int KSF_KEY_CTRL                         = unchecked((int)0x00000022);
  public const int KSF_KEY_CTRLR                        = unchecked((int)0x00000002);
  public const int KSF_KEY_CTRLL                        = unchecked((int)0x00000020);
  public const int KSF_KEY_ALT                          = unchecked((int)0x00000044);
  public const int KSF_KEY_ALTR                         = unchecked((int)0x00000004);
  public const int KSF_KEY_ALTL                         = unchecked((int)0x00000040);
  public const int KSF_KEY_WIN                          = unchecked((int)0x00000088);
  public const int KSF_KEY_WINR                         = unchecked((int)0x00000008);
  public const int KSF_KEY_WINL                         = unchecked((int)0x00000080);

  //------ Key mask groups ------
  public const int KSF_KEY_ALPHA_NUM                    = unchecked((int)0x00000100);
  public const int KSF_KEY_NUM_PAD                      = unchecked((int)0x00000200);
  public const int KSF_KEY_CONTROL                      = unchecked((int)0x00000400);
  public const int KSF_KEY_FUNCTION                     = unchecked((int)0x00000800);

  //------ Key mask combinations ------
  public const int KSF_KEY_CTRL_ALT_DEL                 = unchecked((int)0x00010000);
  public const int KSF_KEY_CTRL_ESC                     = unchecked((int)0x00020000);
  public const int KSF_KEY_ALT_ESC                      = unchecked((int)0x00040000);
  public const int KSF_KEY_ALT_TAB                      = unchecked((int)0x00080000);
  public const int KSF_KEY_ALT_ENTER                    = unchecked((int)0x00100000);
  public const int KSF_KEY_ALT_PRINT                    = unchecked((int)0x00200000);
  public const int KSF_KEY_ALT_SPACE                    = unchecked((int)0x00400000);
  public const int KSF_KEY_ESC                          = unchecked((int)0x00800000);
  public const int KSF_KEY_TAB                          = unchecked((int)0x01000000);
  public const int KSF_KEY_ENTER                        = unchecked((int)0x02000000);
  public const int KSF_KEY_PRINT                        = unchecked((int)0x04000000);
  public const int KSF_KEY_SPACE                        = unchecked((int)0x08000000);
  public const int KSF_KEY_BACKSPACE                    = unchecked((int)0x10000000);
  public const int KSF_KEY_PAUSE                        = unchecked((int)0x20000000);
  public const int KSF_KEY_APP                          = unchecked((int)0x40000000);
  public const int KSF_KEY_ALL                          = unchecked((int)0x7fffffff);

  public const int KSF_KEY_MODIFIER                     = KSF_KEY_CTRL | KSF_KEY_ALT |
                                                          KSF_KEY_SHIFT | KSF_KEY_WIN;

  public const int KSF_KEY_SYSTEM                       = KSF_KEY_WIN |
                                                          KSF_KEY_CTRL_ALT_DEL | KSF_KEY_CTRL_ESC |
                                                          KSF_KEY_ALT_ESC | KSF_KEY_ALT_TAB;

  public const int KSF_KEY_SYSTEM_DOS                   = KSF_KEY_SYSTEM |
                                                          KSF_KEY_ALT_ENTER | KSF_KEY_ALT_PRINT |
                                                          KSF_KEY_ALT_SPACE | KSF_KEY_PRINT;

  //------ Keyboard events ------
  public const int KS_KEYBOARD_CONTEXT                  = unchecked((int)0x00000300);

  //------ Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KeyboardUserContext {
    public int ctxType;
    public int mask;
    public ushort modifier;
    public ushort port;
    public ushort state;
    public ushort key;
  };

  //------ Keyboard functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createKeyHandler(
    uint mask, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeKeyHandler(
    uint mask);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_simulateKeyStroke(
    uint[] pKeys);

  //------------------------------------------------------------------------------------------------------------
  // Interrupt Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_IRQ_ALREADY_USED             = unchecked((int)(KSERROR_CATEGORY_INTERRUPT+0x00000000));
  public const int KSERROR_CANNOT_INSTALL_HANDLER       = unchecked((int)(KSERROR_CATEGORY_INTERRUPT+0x00010000));
  public const int KSERROR_BAD_IRQ_INSTALLATION         = unchecked((int)(KSERROR_CATEGORY_INTERRUPT+0x00020000));
  public const int KSERROR_NO_HANDLER_INSTALLED         = unchecked((int)(KSERROR_CATEGORY_INTERRUPT+0x00030000));
  public const int KSERROR_NOBODY_IS_WAITING            = unchecked((int)(KSERROR_CATEGORY_INTERRUPT+0x00040000));
  public const int KSERROR_IRQ_DISABLED                 = unchecked((int)(KSERROR_CATEGORY_INTERRUPT+0x00050000));
  public const int KSERROR_IRQ_ENABLED                  = unchecked((int)(KSERROR_CATEGORY_INTERRUPT+0x00060000));
  public const int KSERROR_IRQ_IN_SERVICE               = unchecked((int)(KSERROR_CATEGORY_INTERRUPT+0x00070000));
  public const int KSERROR_IRQ_HANDLER_REMOVED          = unchecked((int)(KSERROR_CATEGORY_INTERRUPT+0x00080000));

  //------ Flags ------
  public const int KSF_DONT_PASS_NEXT                   = unchecked((int)0x00020000);
  public const int KSF_PCI_INTERRUPT                    = unchecked((int)0x00040000);

  //------ Interrupt events ------
  public const int KS_INTERRUPT_CONTEXT                 = unchecked((int)0x00000100);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct InterruptUserContext {
    public int ctxType;
    public Handle hInterrupt;
    public uint irq;
    public uint consumed;
  };

  //------ Interrupt handling functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createDeviceInterrupt(
    Handle* phInterrupt, ref KSResourceInfoEx pInfo, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createDeviceInterruptEx(
    Handle* phInterrupt, string deviceName, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createInterrupt(
    Handle* phInterrupt, int irq, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeInterrupt(
    Handle hInterrupt);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_disableInterrupt(
    Handle hInterrupt);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enableInterrupt(
    Handle hInterrupt);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_simulateInterrupt(
    Handle hInterrupt);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getInterruptState(
    Handle hInterrupt, ref HandlerState pState);

  //------------------------------------------------------------------------------------------------------------
  // UART Module
  //------------------------------------------------------------------------------------------------------------

  //------ Flags ------
  public const int KSF_CHECK_LINE_ERROR                 = unchecked((int)0x00008000);
  public const int KSF_NO_FIFO                          = unchecked((int)0x00001000);
  public const int KSF_USE_FIFO                         = unchecked((int)0x00040000);
  public const int KSF_9_BIT_MODE                       = unchecked((int)0x00080000);
  public const int KSF_TOGGLE_DTR                       = unchecked((int)0x00100000);
  public const int KSF_TOGGLE_RTS                       = unchecked((int)0x00200000);
  public const int KSF_TOGGLE_INVERSE                   = unchecked((int)0x00400000);

  //------ Uart commands ------
  public const int KS_CLR_DTR                           = unchecked((int)0x00000004);
  public const int KS_SET_DTR                           = unchecked((int)0x00000005);
  public const int KS_CLR_RTS                           = unchecked((int)0x00000006);
  public const int KS_SET_RTS                           = unchecked((int)0x00000007);
  public const int KS_CLR_BREAK                         = unchecked((int)0x00000008);
  public const int KS_SET_BREAK                         = unchecked((int)0x0000000a);
  public const int KS_SKIP_NEXT                         = unchecked((int)0x0000000b);
  public const int KS_CHANGE_LINE_CONTROL               = unchecked((int)0x0000000d);
  public const int KS_SET_BAUD_RATE_MAX                 = unchecked((int)0x0000000e);
  public const int KS_ENABLE_FIFO                       = unchecked((int)0x0000000f);
  public const int KS_DISABLE_FIFO                      = unchecked((int)0x00000010);
  public const int KS_ENABLE_INTERRUPTS                 = unchecked((int)0x00000011);
  public const int KS_DISABLE_INTERRUPTS                = unchecked((int)0x00000012);
  public const int KS_SET_RECV_FIFO_SIZE                = unchecked((int)0x00000013);

  //------ Line Errors ------
  public const int KS_UART_ERROR_PARITY                 = unchecked((int)0x00000001);
  public const int KS_UART_ERROR_FRAME                  = unchecked((int)0x00000002);
  public const int KS_UART_ERROR_OVERRUN                = unchecked((int)0x00000004);
  public const int KS_UART_ERROR_BREAK                  = unchecked((int)0x00000008);
  public const int KS_UART_ERROR_FIFO                   = unchecked((int)0x00000010);

  //------ UART events ------
  public const int KS_UART_RECV                         = unchecked((int)0x00000000);
  public const int KS_UART_RECV_ERROR                   = unchecked((int)0x00000001);
  public const int KS_UART_XMIT                         = unchecked((int)0x00000002);
  public const int KS_UART_XMIT_EMPTY                   = unchecked((int)0x00000003);
  public const int KS_UART_LSR_CHANGE                   = unchecked((int)0x00000004);
  public const int KS_UART_MSR_CHANGE                   = unchecked((int)0x00000005);
  public const int KS_UART_BREAK                        = unchecked((int)0x00000006);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct UartUserContext {
    public int ctxType;
    public Handle hUart;
    public int value;
    public int lineError;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct UartRegisterUserContext {
    public int ctxType;
    public Handle hUart;
    public byte reg;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSUartProperties {
    public int baudRate;
    public byte parity;
    public byte dataBit;
    public byte stopBit;
    public byte reserved;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSUartState {
    public int structSize;
    public KSUartProperties properties;
    public byte modemState;
    public byte lineState;
    public ushort reserved;
    public int xmitCount;
    public int xmitFree;
    public int recvCount;
    public int recvAvail;
    public int recvErrorCount;
    public int recvLostCount;
  };

  //------ Common functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openUart(
    Handle* phUart, string name, int port, ref KSUartProperties pProperties, int recvBufferLength, int xmitBufferLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openUartEx(
    Handle* phUart, Handle hConnection, int port, ref KSUartProperties pProperties, int recvBufferLength, int xmitBufferLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeUart(
    Handle hUart, int flags);

  //------ Communication functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitUart(
    Handle hUart, int value, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvUart(
    Handle hUart, int* pValue, int* pLineError, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitUartBlock(
    Handle hUart, void* pBytes, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvUartBlock(
    Handle hUart, void* pBytes, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installUartHandler(
    Handle hUart, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getUartState(
    Handle hUart, ref KSUartState pUartState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execUartCommand(
    Handle hUart, int command, void* pParam, int flags);

  //------------------------------------------------------------------------------------------------------------
  // COMM Module
  //------------------------------------------------------------------------------------------------------------

  //------ Line Errors ------
  public const int KS_COMM_ERROR_PARITY                 = unchecked((int)0x00000001);
  public const int KS_COMM_ERROR_FRAME                  = unchecked((int)0x00000002);
  public const int KS_COMM_ERROR_OVERRUN                = unchecked((int)0x00000004);
  public const int KS_COMM_ERROR_BREAK                  = unchecked((int)0x00000008);
  public const int KS_COMM_ERROR_FIFO                   = unchecked((int)0x00000010);

  //------ COMM events ------
  public const int KS_COMM_RECV                         = unchecked((int)0x00000000);
  public const int KS_COMM_RECV_ERROR                   = unchecked((int)0x00000001);
  public const int KS_COMM_XMIT                         = unchecked((int)0x00000002);
  public const int KS_COMM_XMIT_EMPTY                   = unchecked((int)0x00000003);
  public const int KS_COMM_LSR_CHANGE                   = unchecked((int)0x00000004);
  public const int KS_COMM_MSR_CHANGE                   = unchecked((int)0x00000005);
  public const int KS_COMM_BREAK                        = unchecked((int)0x00000006);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct CommUserContext {
    public int ctxType;
    public Handle hComm;
    public int value;
    public int lineError;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct CommRegisterUserContext {
    public int ctxType;
    public Handle hComm;
    public byte reg;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCommProperties {
    public int baudRate;
    public byte parity;
    public byte dataBit;
    public byte stopBit;
    public byte reserved;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCommState {
    public int structSize;
    public KSCommProperties properties;
    public byte modemState;
    public byte lineState;
    public ushort reserved;
    public int xmitCount;
    public int xmitFree;
    public int recvCount;
    public int recvAvail;
    public int recvErrorCount;
    public int recvLostCount;
  };

  //------ COMM functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openComm(
    Handle* phComm, string name, ref KSCommProperties pProperties, int recvBufferLength, int xmitBufferLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeComm(
    Handle hComm, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitComm(
    Handle hComm, int value, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvComm(
    Handle hComm, int* pValue, int* pLineError, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitCommBlock(
    Handle hComm, void* pBytes, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvCommBlock(
    Handle hComm, void* pBytes, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installCommHandler(
    Handle hComm, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getCommState(
    Handle hComm, ref KSCommState pCommState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execCommCommand(
    Handle hComm, int command, void* pParam, int flags);

  //------------------------------------------------------------------------------------------------------------
  // USB Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_USB_REQUEST_FAILED           = unchecked((int)(KSERROR_CATEGORY_USB+0x00000000));

  //------ Flags ------
  public const int KSF_SOCKET_MODE                      = unchecked((int)0x00080000);

  //------ USB endpoint commands ------
  public const int KS_USB_RESET_PORT                    = unchecked((int)0x00000004);
  public const int KS_USB_SET_ISO_PACKET_SIZE           = unchecked((int)0x00000005);
  public const int KS_USB_GET_PACKET_SIZE               = unchecked((int)0x00000007);
  public const int KS_USB_SET_PACKET_TIMEOUT            = unchecked((int)0x00000008);

  //------ USB device commands ------
  public const int KS_USB_GET_DEVICE_DESCRIPTOR         = unchecked((int)0x00020000);
  public const int KS_USB_GET_CONFIG_DESCRIPTOR         = unchecked((int)0x00030000);
  public const int KS_USB_GET_STRING_DESCRIPTOR         = unchecked((int)0x00040000);
  public const int KS_USB_SELECT_CONFIG                 = unchecked((int)0x00050000);
  public const int KS_USB_SELECT_INTERFACE              = unchecked((int)0x00060000);
  public const int KS_USB_GET_CONFIG                    = unchecked((int)0x00070000);
  public const int KS_USB_GET_INTERFACE                 = unchecked((int)0x00080000);
  public const int KS_USB_CLR_DEVICE_FEATURE            = unchecked((int)0x00090000);
  public const int KS_USB_SET_DEVICE_FEATURE            = unchecked((int)0x000a0000);
  public const int KS_USB_CLR_INTERFACE_FEATURE         = unchecked((int)0x000b0000);
  public const int KS_USB_SET_INTERFACE_FEATURE         = unchecked((int)0x000c0000);
  public const int KS_USB_CLR_ENDPOINT_FEATURE          = unchecked((int)0x000d0000);
  public const int KS_USB_SET_ENDPOINT_FEATURE          = unchecked((int)0x000e0000);
  public const int KS_USB_GET_STATUS_FROM_DEVICE        = unchecked((int)0x000f0000);
  public const int KS_USB_GET_STATUS_FROM_INTERFACE     = unchecked((int)0x00100000);
  public const int KS_USB_GET_STATUS_FROM_ENDPOINT      = unchecked((int)0x00110000);
  public const int KS_USB_VENDOR_DEVICE_REQUEST         = unchecked((int)0x00120000);
  public const int KS_USB_VENDOR_INTERFACE_REQUEST      = unchecked((int)0x00130000);
  public const int KS_USB_VENDOR_ENDPOINT_REQUEST       = unchecked((int)0x00140000);
  public const int KS_USB_CLASS_DEVICE_REQUEST          = unchecked((int)0x00150000);
  public const int KS_USB_CLASS_INTERFACE_REQUEST       = unchecked((int)0x00160000);
  public const int KS_USB_CLASS_ENDPOINT_REQUEST        = unchecked((int)0x00170000);

  //------ USB descriptor types ------
  public const int KS_USB_DEVICE                        = unchecked((int)0x00000001);
  public const int KS_USB_CONFIGURATION                 = unchecked((int)0x00000002);
  public const int KS_USB_STRING                        = unchecked((int)0x00000003);
  public const int KS_USB_INTERFACE                     = unchecked((int)0x00000004);
  public const int KS_USB_ENDPOINT                      = unchecked((int)0x00000005);

  //------ USB end point types ------
  public const int KS_USB_CONTROL                       = unchecked((int)0x00000000);
  public const int KS_USB_ISOCHRONOUS                   = unchecked((int)0x00000001);
  public const int KS_USB_BULK                          = unchecked((int)0x00000002);
  public const int KS_USB_INTERRUPT                     = unchecked((int)0x00000003);

  //------ USB port events ------
  public const int KS_USB_SURPRISE_REMOVAL              = unchecked((int)(USB_BASE+0x00000000));
  public const int KS_USB_RECV                          = unchecked((int)(USB_BASE+0x00000001));
  public const int KS_USB_RECV_ERROR                    = unchecked((int)(USB_BASE+0x00000002));
  public const int KS_USB_XMIT                          = unchecked((int)(USB_BASE+0x00000003));
  public const int KS_USB_XMIT_EMPTY                    = unchecked((int)(USB_BASE+0x00000004));
  public const int KS_USB_XMIT_ERROR                    = unchecked((int)(USB_BASE+0x00000005));
  public const int KS_USB_TIMEOUT                       = unchecked((int)(USB_BASE+0x00000006));

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct UsbUserContext {
    public int ctxType;
    public Handle hUsb;
    public int error;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct UsbClassOrVendorRequest {
    public int request;
    public int value;
    public int index;
    public void* pData;
    public int size;
    public int write;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KS_USB_DEVICE_DESCRIPTOR {
    public byte bLength;
    public byte bDescriptorType;
    public ushort bcdUSB;
    public byte bDeviceClass;
    public byte bDeviceSubClass;
    public byte bDeviceProtocol;
    public byte bMaxPacketSize0;
    public ushort idVendor;
    public ushort idProduct;
    public ushort bcdDevice;
    public byte iManufacturer;
    public byte iProduct;
    public byte iSerialNumber;
    public byte bNumConfigurations;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KS_USB_CONFIGURATION_DESCRIPTOR {
    public byte bLength;
    public byte bDescriptorType;
    public ushort wTotalLength;
    public byte bNumInterfaces;
    public byte bConfigurationValue;
    public byte iConfiguration;
    public byte bmAttributes;
    public byte MaxPower;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KS_USB_INTERFACE_DESCRIPTOR {
    public byte bLength;
    public byte bDescriptorType;
    public byte bInterfaceNumber;
    public byte bAlternateSetting;
    public byte bNumEndpoints;
    public byte bInterfaceClass;
    public byte bInterfaceSubClass;
    public byte bInterfaceProtocol;
    public byte iInterface;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KS_USB_ENDPOINT_DESCRIPTOR {
    public byte bLength;
    public byte bDescriptorType;
    public byte bEndpointAddress;
    public byte bmAttributes;
    public ushort wMaxPacketSize;
    public byte bInterval;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KS_USB_STRING_DESCRIPTOR {
    public byte bLength;
    public byte bDescriptorType;
    public ushort bString;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KS_USB_COMMON_DESCRIPTOR {
    public byte bLength;
    public byte bDescriptorType;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KS_USB_DEVICE_QUALIFIER_DESCRIPTOR {
    public byte bLength;
    public byte bDescriptorType;
    public ushort bcdUSB;
    public byte bDeviceClass;
    public byte bDeviceSubClass;
    public byte bDeviceProtocol;
    public byte bMaxPacketSize0;
    public byte bNumConfigurations;
    public byte bReserved;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSUsbState {
    public int structSize;
    public int xmitCount;
    public int xmitFree;
    public int xmitErrorCount;
    public int recvCount;
    public int recvAvail;
    public int recvErrorCount;
    public int packetSize;
    public int isoPacketSize;
    public int packetTimeout;
  };

  //------ Common functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openUsb(
    Handle* phUsbDevice, string deviceName, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openUsbEndPoint(
    Handle hUsbDevice, Handle* phUsbEndPoint, int endPointAddress, int packetSize, int packetCount, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeUsb(
    Handle hUsb, int flags);

  //------ Communication functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitUsb(
    Handle hUsb, void* pData, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvUsb(
    Handle hUsb, void* pData, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installUsbHandler(
    Handle hUsb, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getUsbState(
    Handle hUsb, ref KSUsbState pUsbState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execUsbCommand(
    Handle hUsb, int command, int index, void* pData, int size, int flags);

  //------------------------------------------------------------------------------------------------------------
  // XHCI Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_XHCI_REQUEST_FAILED          = unchecked((int)(KSERROR_CATEGORY_XHCI+0x00000000));
  public const int KSERROR_XHCI_HOST_CONTROLLER_ERROR   = unchecked((int)(KSERROR_CATEGORY_XHCI+0x00010000));
  public const int KSERROR_XHCI_CONFIGURATION_ERROR     = unchecked((int)(KSERROR_CATEGORY_XHCI+0x00020000));
  public const int KSERROR_XHCI_INTERFACE_ERROR         = unchecked((int)(KSERROR_CATEGORY_XHCI+0x00030000));
  public const int KSERROR_XHCI_PACKET_DROPPED          = unchecked((int)(KSERROR_CATEGORY_XHCI+0x00040000));
  public const int KSERROR_XHCI_PACKET_ERROR            = unchecked((int)(KSERROR_CATEGORY_XHCI+0x00050000));

  //------ XHCI endpoint commands ------
  public const int KS_XHCI_RESET_ENDPOINT               = unchecked((int)0x00000001);
  public const int KS_XHCI_GET_PACKET_SIZE              = unchecked((int)0x00000002);
  public const int KS_XHCI_CLEAR_RECEIVE_QUEUE          = unchecked((int)0x00000003);

  //------ XHCI device commands ------
  public const int KS_XHCI_GET_DEVICE_DESCRIPTOR        = unchecked((int)0x00010001);
  public const int KS_XHCI_GET_CONFIG_DESCRIPTOR        = unchecked((int)0x00010002);
  public const int KS_XHCI_GET_STRING_DESCRIPTOR        = unchecked((int)0x00010003);
  public const int KS_XHCI_SET_CONFIG                   = unchecked((int)0x00010004);
  public const int KS_XHCI_GET_CONFIG                   = unchecked((int)0x00010005);
  public const int KS_XHCI_SET_INTERFACE                = unchecked((int)0x00010006);
  public const int KS_XHCI_GET_INTERFACE                = unchecked((int)0x00010007);

  //------ XHCI controller commands ------
  public const int KS_XHCI_SET_POWER_ROOT_HUB           = unchecked((int)0x00020001);

  //------ XHCI descriptor types ------
  public const int KS_XHCI_DEVICE_DESCRIPTOR            = unchecked((int)0x00000001);
  public const int KS_XHCI_CONFIGURATION_DESCRIPTOR     = unchecked((int)0x00000002);
  public const int KS_XHCI_STRING_DESCRIPTOR            = unchecked((int)0x00000003);
  public const int KS_XHCI_INTERFACE_DESCRIPTOR         = unchecked((int)0x00000004);
  public const int KS_XHCI_ENDPOINT_DESCRIPTOR          = unchecked((int)0x00000005);
  public const int KS_XHCI_INTERFACE_ASSOCIATION_DESCRIPTOR 
                                                        = unchecked((int)0x0000000b);
  public const int KS_XHCI_ENDPOINT_COMPANION_DESCRIPTOR 
                                                        = unchecked((int)0x00000030);

  //------ XHCI endpoint types ------
  public const int KS_XHCI_CONTROL_ENDPOINT             = unchecked((int)0x00000000);
  public const int KS_XHCI_ISOCHRONOUS_ENDPOINT         = unchecked((int)0x00000001);
  public const int KS_XHCI_BULK_ENDPOINT                = unchecked((int)0x00000002);
  public const int KS_XHCI_INTERRUPT_ENDPOINT           = unchecked((int)0x00000003);

  //------ XHCI events ------
  public const int KS_XHCI_ADD_DEVICE                   = unchecked((int)(XHCI_BASE+0x00000000));
  public const int KS_XHCI_REMOVE_DEVICE                = unchecked((int)(XHCI_BASE+0x00000001));
  public const int KS_XHCI_RECV                         = unchecked((int)(XHCI_BASE+0x00000002));
  public const int KS_XHCI_RECV_ERROR                   = unchecked((int)(XHCI_BASE+0x00000003));
  public const int KS_XHCI_XMIT_LOW                     = unchecked((int)(XHCI_BASE+0x00000004));
  public const int KS_XHCI_XMIT_EMPTY                   = unchecked((int)(XHCI_BASE+0x00000005));
  public const int KS_XHCI_XMIT_ERROR                   = unchecked((int)(XHCI_BASE+0x00000006));

  //------ Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciContext {
    public int ctxType;
    public Handle hXhci;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciAddDeviceContext {
    public int ctxType;
    public Handle hXhciController;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
    public byte[] deviceName;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciRecvContext {
    public int ctxType;
    public Handle hXhciEndPoint;
    public void* pXhciPacketApp;
    public void* pXhciPacketSys;
    public int packetSize;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciErrorContext {
    public int ctxType;
    public Handle hXhci;
    public int error;
  };

  //------ XHCI common structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciControlRequest {
    public int requestType;
    public int request;
    public int value;
    public int index;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciEndPointState {
    public int structSize;
    public int endPointType;
    public int packetSize;
    public int xmitCount;
    public int xmitFree;
    public int xmitErrorCount;
    public int recvCount;
    public int recvAvail;
    public int recvErrorCount;
    public int recvDroppedCount;
  };

  //------ XHCI Descriptors ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciCommonDescriptor {
    public byte bLength;
    public byte bDescriptorType;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciDeviceDescriptor {
    public byte bLength;
    public byte bDescriptorType;
    public ushort bcdUSB;
    public byte bDeviceClass;
    public byte bDeviceSubClass;
    public byte bDeviceProtocol;
    public byte bMaxPacketSize0;
    public ushort idVendor;
    public ushort idProduct;
    public ushort bcdDevice;
    public byte iManufacturer;
    public byte iProduct;
    public byte iSerialNumber;
    public byte bNumConfigurations;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciConfigurationDescriptor {
    public byte bLength;
    public byte bDescriptorType;
    public ushort wTotalLength;
    public byte bNumInterfaces;
    public byte bConfigurationValue;
    public byte iConfiguration;
    public byte bmAttributes;
    public byte MaxPower;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciInterfaceDescriptor {
    public byte bLength;
    public byte bDescriptorType;
    public byte bInterfaceNumber;
    public byte bAlternateSetting;
    public byte bNumEndpoints;
    public byte bInterfaceClass;
    public byte bInterfaceSubClass;
    public byte bInterfaceProtocol;
    public byte iInterface;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciInterfaceAssociationDescriptor {
    public byte bLength;
    public byte bDescriptorType;
    public byte bFirstInterface;
    public byte bNumInterfaces;
    public byte bFunctionClass;
    public byte bFunctionSubClass;
    public byte bFunctionProtocol;
    public byte iFunction;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciEndpointDescriptor {
    public byte bLength;
    public byte bDescriptorType;
    public byte bEndpointAddress;
    public byte bmAttributes;
    public ushort wMaxPacketSize;
    public byte bInterval;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciEndpointCompanionDescriptor {
    public byte bLength;
    public byte bDescriptorType;
    public byte bMaxBurst;
    public byte bmAttributes;
    public ushort wBytesPerInterval;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSXhciStringDescriptor {
    public byte bLength;
    public byte bDescriptorType;
    public ushort bString;
  };

  //------ Common functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openXhciController(
    Handle* phXhciController, string deviceName, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openXhciDevice(
    Handle* phXhciDevice, string deviceName, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openXhciEndPoint(
    Handle hXhciDevice, Handle* phXhciEndPoint, int endPointAddress, int packetSize, int packetCount, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeXhci(
    Handle hXhci, int flags);

  //------ Communication functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_requestXhciPacket(
    Handle hXhciEndPoint, void** ppXhciPacket, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_releaseXhciPacket(
    Handle hXhciEndPoint, void* pXhciPacket, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitXhciPacket(
    Handle hXhciEndPoint, void* pXhciPacket, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvXhciPacket(
    Handle hXhciEndPoint, void** ppXhciPacket, int* pSize, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_sendXhciControlRequest(
    Handle hXhciDevice, ref KSXhciControlRequest pRequestData, void* pData, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installXhciHandler(
    Handle hXhci, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getXhciEndPointState(
    Handle hXhciEndPoint, ref KSXhciEndPointState pState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execXhciCommand(
    Handle hXhci, int command, int index, void* pParam, int flags);

  //------------------------------------------------------------------------------------------------------------
  // RealTime Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_TIMER_NOT_INSTALLED          = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00000000));
  public const int KSERROR_TIMER_REMOVED                = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00010000));
  public const int KSERROR_TIMER_DISABLED               = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00020000));
  public const int KSERROR_TIMER_NOT_DISABLED           = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00030000));
  public const int KSERROR_CANNOT_INSTALL_TIMER         = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00040000));
  public const int KSERROR_NO_TIMER_AVAILABLE           = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00050000));
  public const int KSERROR_WAIT_CANCELLED               = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00060000));
  public const int KSERROR_TIMER_ALREADY_RUNNING        = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00070000));
  public const int KSERROR_TIMER_ALREADY_STOPPED        = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00080000));
  public const int KSERROR_CANNOT_START_TIMER           = unchecked((int)(KSERROR_CATEGORY_TIMER+0x00090000));

  //------ Flags ------
  public const int KSF_DISTINCT_MODE                    = unchecked((int)0x00040000);
  public const int KSF_DRIFT_CORRECTION                 = unchecked((int)0x00010000);
  public const int KSF_DISABLE_PROTECTION               = unchecked((int)0x00020000);
  public const int KSF_USE_IEEE1588_TIME                = unchecked((int)0x00040000);

  //------ Timer module config ------
  public const int KSCONFIG_SOFTWARE_TIMER              = unchecked((int)0x00000001);

  //------ Timer events ------
  public const int KS_TIMER_CONTEXT                     = unchecked((int)0x00000200);

  //------ Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct TimerUserContext {
    public int ctxType;
    public Handle hTimer;
    public int delay;
  };

  //------ Real-time functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createTimer(
    Handle* phTimer, int delay, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeTimer(
    Handle hTimer);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_cancelTimer(
    Handle hTimer);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_startTimer(
    Handle hTimer, int flags, int delay);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_stopTimer(
    Handle hTimer);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_startTimerDelayed(
    Handle hTimer, ref long start, int period, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_adjustTimer(
    Handle hTimer, int period, int flags);

  //------ Obsolete timer functions - do not use! ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_disableTimer(
    Handle hTimer);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enableTimer(
    Handle hTimer);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getTimerState(
    Handle hTimer, ref HandlerState pState);

  //------ Obsolete timer resolution functions - do not use! ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setTimerResolution(
    uint resolution, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getTimerResolution(
    int* pResolution, int flags);

  //------------------------------------------------------------------------------------------------------------
  // Task Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_CPU_BOOT_FAILURE             = unchecked((int)(KSERROR_CATEGORY_TASK+0x00000000));

  //------ Task states ------
  public const int KS_TASK_DORMANT                      = unchecked((int)0x00000001);
  public const int KS_TASK_READY                        = unchecked((int)0x00000002);
  public const int KS_TASK_RUNNING                      = unchecked((int)0x00000004);
  public const int KS_TASK_WAITING                      = unchecked((int)0x00000008);
  public const int KS_TASK_SUSPENDED                    = unchecked((int)0x00000010);
  public const int KS_TASK_TERMINATED                   = unchecked((int)0x00000020);

  //------ Flags ------
  public const int KSF_NUM_PROCESSORS                   = unchecked((int)0x00000040);
  public const int KSF_NON_INTERRUPTABLE                = unchecked((int)0x00040000);
  public const int KSF_CUSTOM_STACK_SIZE                = unchecked((int)0x00004000);

  //------ Task contexts ------
  public const int TASK_CONTEXT                         = unchecked((int)(TASK_BASE+0x00000000));

  //------ Common structure ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSContextInformation {
    public int structSize;
    public int currentLevel;
    public Handle taskHandle;
    public int taskPriority;
    public int currentCpuNumber;
    public int stackSize;
    public int stackUsage;
  };

  //------ Context structure ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct TaskUserContext {
    public int ctxType;
    public Handle hTask;
    public int priority;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct TaskErrorUserContext {
    public int ctxType;
    public int error;
    public void* param1;
    public void* param2;
    public void* param3;
    public void* param4;
    public Handle hTask;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
    public byte[] kernel;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
    public byte[] functionName;
    public int functionOffset;
  };

  //------ Task functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createTask(
    Handle* phTask, Handle hCallBack, int priority, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeTask(
    Handle hTask);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_exitTask(
    Handle hTask, int exitCode);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_killTask(
    Handle hTask, int exitCode);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_terminateTask(
    Handle hTask, int timeout, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_suspendTask(
    Handle hTask);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_resumeTask(
    Handle hTask);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_triggerTask(
    Handle hTask);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_yieldTask(
    );
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_sleepTask(
    int delay);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setTaskPrio(
    Handle hTask, int newPrio, int* pOldPrio);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setTaskPriority(
    Handle hObject, int priority, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setTaskPriorityRelative(
    Handle hObject, Handle hRelative, int distance, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getTaskPriority(
    Handle hObject, int* pPriority, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getTaskState(
    Handle hTask, int* pTaskState, int* pExitCode);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setTaskStackSize(
    int size, int flags);

  //------ MultiCore & information functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setTargetProcessor(
    int processor, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getCurrentProcessor(
    int* pProcessor, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getCurrentContext(
    ref KSContextInformation pContextInfo, int flags);

  //------ Semaphore functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createSemaphore(
    Handle* phSemaphore, int maxCount, int initCount, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeSemaphore(
    Handle hSemaphore);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_requestSemaphore(
    Handle hSemaphore, int timeout);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_releaseSemaphore(
    Handle hSemaphore);

  //------------------------------------------------------------------------------------------------------------
  // Dedicated Module
  //------------------------------------------------------------------------------------------------------------

  //------ Dedicated functions ------

  //------------------------------------------------------------------------------------------------------------
  // Packet Module
  //------------------------------------------------------------------------------------------------------------

  //----------------------------------------------------------------------------
  // Packet Module
  //----------------------------------------------------------------------------

  public static byte lo(ushort val)                      { return (byte) val; }
  public static byte hi(ushort val)                      { return (byte) (val >> 8); }

  public static ushort lo(uint val)                      { return (ushort) val; }
  public static ushort hi(uint val)                      { return (ushort) (val >> 16); }

  public static ushort mk16(byte lo, byte hi)            { return (ushort) (lo | ((ushort)((hi) << 8))); }
  public static uint mk32(ushort lo, ushort hi)          { return (uint) (lo | ((uint)(hi) << 16)); }
  public static ushort mk16(byte lo)                     { return (ushort) (lo); }
  public static uint mk32(ushort lo)                     { return (uint) (lo); }

  public static ushort KS_htons(ushort val)              { return mk16(hi(val), lo(val)); }
  public static uint KS_htonl(uint val)                  { return mk32(KS_htons(hi(val)), KS_htons(lo(val))); }

  public static ushort KS_ntohs(ushort val)              { return KS_htons(val); }
  public static uint KS_ntohl(uint val)                  { return KS_htonl(val); }

  public static void KS_makeIPv4(ref uint pAddr, byte b3, byte b2, byte b1, byte b0)                  
                                                { pAddr = (uint) ((b0 << 24) | (b1 << 16) | (b2 << 8) | b3); }

  //------ Error Codes ------
  public const int KSERROR_INIT_PHY_ERROR               = unchecked((int)(KSERROR_CATEGORY_PACKET+0x00000000));
  public const int KSERROR_INIT_MAC_ERROR               = unchecked((int)(KSERROR_CATEGORY_PACKET+0x00010000));
  public const int KSERROR_MAC_STARTUP_ERROR            = unchecked((int)(KSERROR_CATEGORY_PACKET+0x00020000));

  //------ Flags ------
  public const int KSF_SUPPORT_JUMBO_FRAMES             = unchecked((int)0x00008000);
  public const int KSF_DELAY_RECEIVE_INTERRUPT          = unchecked((int)0x00080000);
  public const int KSF_NO_RECEIVE_INTERRUPT             = unchecked((int)0x00000020);
  public const int KSF_NO_TRANSMIT_INTERRUPT            = unchecked((int)0x00000040);
  public const int KSF_RAW_ETHERNET_PACKET              = unchecked((int)0x00100000);
  public const int KSF_SAFE_START                       = unchecked((int)0x00200000);
  public const int KSF_CHECK_IP_CHECKSUM                = unchecked((int)0x00400000);
  public const int KSF_ACCEPT_ALL                       = unchecked((int)0x00800000);
  public const int KSF_FORCE_MASTER                     = unchecked((int)0x00000001);
  public const int KSF_FORCE_100_MBIT                   = unchecked((int)0x00000002);
  public const int KSF_WINDOWS_CONNECTION               = unchecked((int)0x00004000);

  //------ Packet events ------
  public const int KS_PACKET_RECV                       = unchecked((int)(PACKET_BASE+0x00000000));
  public const int KS_PACKET_LINK_CHANGE                = unchecked((int)(PACKET_BASE+0x00000001));
  public const int KS_PACKET_SEND_EMPTY                 = unchecked((int)(PACKET_BASE+0x00000002));
  public const int KS_PACKET_SEND_LOW                   = unchecked((int)(PACKET_BASE+0x00000003));

  //------ Adapter commands ------
  public const int KS_PACKET_ABORT_SEND                 = unchecked((int)0x00000001);
  public const int KS_PACKET_ADD_ARP_TABLE_ENTRY        = unchecked((int)0x00000002);
  public const int KS_PACKET_SET_IP_CONFIG              = unchecked((int)0x00000003);
  public const int KS_PACKET_SET_MAC_MULTICAST          = unchecked((int)0x00000004);
  public const int KS_PACKET_SET_IP_MULTICAST           = unchecked((int)0x00000005);
  public const int KS_PACKET_SET_MAC_CONFIG             = unchecked((int)0x00000006);
  public const int KS_PACKET_CLEAR_RECEIVE_QUEUE        = unchecked((int)0x00000007);
  public const int KS_PACKET_GET_IP_CONFIG              = unchecked((int)0x00000008);
  public const int KS_PACKET_GET_MAC_CONFIG             = unchecked((int)0x00000009);
  public const int KS_PACKET_GET_MAXIMUM_SIZE           = unchecked((int)0x0000000a);

  //------ Connection flags ------
  public const int KS_PACKET_SPEED_10_MBIT              = unchecked((int)0x00010000);
  public const int KS_PACKET_SPEED_100_MBIT             = unchecked((int)0x00020000);
  public const int KS_PACKET_SPEED_1000_MBIT            = unchecked((int)0x00040000);
  public const int KS_PACKET_SPEED_10_GBIT              = unchecked((int)0x00080000);
  public const int KS_PACKET_DUPLEX_FULL                = unchecked((int)0x00000001);
  public const int KS_PACKET_DUPLEX_HALF                = unchecked((int)0x00000002);
  public const int KS_PACKET_MASTER                     = unchecked((int)0x00000004);
  public const int KS_PACKET_SLAVE                      = unchecked((int)0x00000008);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PacketUserContext {
    public int ctxType;
    public Handle hAdapter;
    public void* pPacketApp;
    public void* pPacketSys;
    public int length;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PacketAdapterUserContext {
    public int ctxType;
    public Handle hAdapter;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PacketLinkChangeUserContext {
    public int ctxType;
    public Handle hAdapter;
    public uint connection;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSMACHeader {
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public byte[] destination;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public byte[] source;
    public ushort typeOrLength;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSIPHeader {
    public uint header1;
    public uint header2;
    public uint header3;
    public uint srcIpAddress;
    public uint dstIpAddress;
    public uint header6;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSUDPHeader {
    public ushort srcPort;
    public ushort dstPort;
    public ushort msgLength;
    public ushort checkSum;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSIPConfig {
    public uint localAddress;
    public uint subnetMask;
    public uint gatewayAddress;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSARPTableEntry {
    public uint ipAddress;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public byte[] macAddress;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSAdapterState {
    public int structSize;
    public int numPacketsSendOk;
    public int numPacketsSendError;
    public int numPacketsSendARPTimeout;
    public int numPacketsWaitingForARP;
    public int numPacketsRecvOk;
    public int numPacketsRecvError;
    public int numPacketsRecvDropped;
    public uint connection;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public byte[] macAddress;
    public byte connected;
  };

  //------ Helper functions ------

  //------ Common functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openAdapter(
    Handle* phAdapter, string name, int recvPoolLength, int sendPoolLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openAdapterEx(
    Handle* phAdapter, Handle hConnection, int recvPoolLength, int sendPoolLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeAdapter(
    Handle hAdapter);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getAdapterState(
    Handle hAdapter, ref KSAdapterState pAdapterState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execAdapterCommand(
    Handle hAdapter, int command, void* pParam, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_aggregateAdapter(
    Handle hAdapter, Handle hLink, int flags);

  //------ Communication functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_requestPacket(
    Handle hAdapter, void** ppPacket, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_releasePacket(
    Handle hAdapter, void* pPacket, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_sendPacket(
    Handle hAdapter, void* pPacket, uint size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvPacket(
    Handle hAdapter, void** ppPacket, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvPacketEx(
    Handle hAdapter, void** ppPacket, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installPacketHandler(
    Handle hAdapter, int eventCode, Handle hSignal, int flags);

  //------------------------------------------------------------------------------------------------------------
  // Socket Module
  //------------------------------------------------------------------------------------------------------------

  //------ Flags ------
  public const int KSF_CLIENT                           = unchecked((int)0x00000020);
  public const int KSF_SERVER                           = unchecked((int)0x00000040);
  public const int KSF_ACCEPT_BROADCAST                 = unchecked((int)0x00800000);
  public const int KSF_INSERT_CHECKSUM                  = unchecked((int)0x00200000);
  public const int KSF_CHECK_CHECKSUM                   = unchecked((int)0x00400000);

  //------ Family types ------
  public const int KS_FAMILY_IPV4                       = unchecked((int)0x00000004);

  //------ Protocol types ------
  public const int KS_PROTOCOL_RAW                      = unchecked((int)0x00000000);
  public const int KS_PROTOCOL_UDP                      = unchecked((int)0x00000001);
  public const int KS_PROTOCOL_TCP                      = unchecked((int)0x00000002);

  //------ Socket events ------
  public const int KS_SOCKET_CONNECTED                  = unchecked((int)(SOCKET_BASE+0x00000000));
  public const int KS_SOCKET_DISCONNECTED               = unchecked((int)(SOCKET_BASE+0x00000001));
  public const int KS_SOCKET_RECV                       = unchecked((int)(SOCKET_BASE+0x00000002));
  public const int KS_SOCKET_CLEAR_TO_SEND              = unchecked((int)(SOCKET_BASE+0x00000003));
  public const int KS_SOCKET_SEND_EMPTY                 = unchecked((int)(SOCKET_BASE+0x00000004));
  public const int KS_SOCKET_SEND_LOW                   = unchecked((int)(SOCKET_BASE+0x00000005));

  //------ Socket commands ------
  public const int KS_SOCKET_SET_MTU                    = unchecked((int)0x00000001);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSSocketAddr {
    public ushort family;
    public ushort port;
    public uint address;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct SocketUserContext {
    public int ctxType;
    public Handle hSocket;
  };

  //------ Socket functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openSocket(
    Handle* phSocket, Handle hAdapter, ref KSSocketAddr pSocketAddr, int protocol, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeSocket(
    Handle hSocket);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvFromSocket(
    Handle hSocket, ref KSSocketAddr pSourceAddr, void* pBuffer, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_sendToSocket(
    Handle hSocket, ref KSSocketAddr pTargetAddr, void* pBuffer, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvSocket(
    Handle hSocket, void* pBuffer, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_sendSocket(
    Handle hSocket, void* pBuffer, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installSocketHandler(
    Handle hSocket, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_connectSocket(
    Handle hClient, ref KSSocketAddr pServerAddr, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_acceptSocket(
    Handle hServer, ref KSSocketAddr pClientAddr, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execSocketCommand(
    Handle hSocket, int command, void* pParam, int flags);

  //------------------------------------------------------------------------------------------------------------
  // EtherCAT Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_EMERGENCY_REQUEST            = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00000000));
  public const int KSERROR_LINK_CHANGE                  = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00010000));
  public const int KSERROR_TOPOLOGY_CHANGE              = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00020000));
  public const int KSERROR_SLAVE_ERROR                  = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00030000));
  public const int KSERROR_DATA_INCOMPLETE              = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00040000));
  public const int KSERROR_PHYSICAL_ACCESS_FAILED       = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00050000));
  public const int KSERROR_FMMU_ERROR                   = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00060000));
  public const int KSERROR_DATA_LINK_ERROR              = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00070000));
  public const int KSERROR_SII_ERROR                    = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00080000));
  public const int KSERROR_MAILBOX_ERROR                = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00090000));
  public const int KSERROR_APPLICATION_LAYER_ERROR      = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x000a0000));
  public const int KSERROR_SYNC_MANAGER_ERROR           = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x000b0000));
  public const int KSERROR_DC_CONTROL_ERROR             = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x000c0000));
  public const int KSERROR_SOE_REQUEST_ERROR            = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x000e0000));
  public const int KSERROR_MISSING_XML_INFORMATION      = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x000f0000));
  public const int KSERROR_SAFETY_ERROR                 = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00100000));
  public const int KSERROR_DC_CONFIGURATION_ERROR       = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00110000));
  public const int KSERROR_COE_ERROR                    = unchecked((int)(KSERROR_CATEGORY_ETHERCAT+0x00120000));

  //------ Flags ------
  public const int KSF_FORCE_UPDATE                     = unchecked((int)0x00000008);
  public const int KSF_SECONDARY                        = unchecked((int)0x00000020);
  public const int KSF_SYNC_DC_GLOBAL                   = unchecked((int)0x00000040);
  public const int KSF_SYNC_DC_LOCAL                    = unchecked((int)0x00800000);
  public const int KSF_BIG_DATASET                      = unchecked((int)0x00004000);
  public const int KSF_STARTUP_WITHOUT_INIT_STATE       = unchecked((int)0x00008000);
  public const int KSF_COMPLETE_ACCESS                  = unchecked((int)0x00200000);
  public const int KSF_IGNORE_INIT_CMDS                 = unchecked((int)0x01000000);
  public const int KSF_STRICT_XML_CHECK                 = unchecked((int)0x00000040);

  //------ DataObj type flags ------
  public const int KS_DATAOBJ_PDO_TYPE                  = unchecked((int)0x00000001);
  public const int KS_DATAOBJ_SDO_TYPE                  = unchecked((int)0x00000002);
  public const int KS_DATAOBJ_IDN_TYPE                  = unchecked((int)0x00000003);
  public const int KS_DATAOBJ_ACTIVE                    = unchecked((int)0x00040000);
  public const int KS_DATAOBJ_MAPPABLE                  = unchecked((int)0x00080000);
  public const int KS_DATAOBJ_SAFETY                    = unchecked((int)0x00100000);

  //------ State type ------
  public const int KS_ECAT_STATE_INIT                   = unchecked((int)0x00000001);
  public const int KS_ECAT_STATE_PREOP                  = unchecked((int)0x00000002);
  public const int KS_ECAT_STATE_BOOT                   = unchecked((int)0x00000003);
  public const int KS_ECAT_STATE_SAFEOP                 = unchecked((int)0x00000004);
  public const int KS_ECAT_STATE_OP                     = unchecked((int)0x00000008);

  //------ State type (legacy, do not use anymore) ------
  public const int KS_STATE_INIT                        = unchecked((int)0x00000001);
  public const int KS_STATE_PREOP                       = unchecked((int)0x00000002);
  public const int KS_STATE_BOOT                        = unchecked((int)0x00000003);
  public const int KS_STATE_SAFEOP                      = unchecked((int)0x00000004);
  public const int KS_STATE_OP                          = unchecked((int)0x00000008);

  //------ Sync manager index commons ------
  public const int KS_ECAT_SYNC_ALL                     = unchecked((int)0xffffffff);
  public const int KS_ECAT_SYNC_INPUT                   = unchecked((int)0xfffffffe);
  public const int KS_ECAT_SYNC_OUTPUT                  = unchecked((int)0xfffffffd);

  //------ EtherCAT events (legacy, do not use anymore) ------
  public const int KS_ETHERCAT_ERROR                    = unchecked((int)(ETHERCAT_BASE+0x00000000));
  public const int KS_DATASET_SIGNAL                    = unchecked((int)(ETHERCAT_BASE+0x00000001));
  public const int KS_TOPOLOGY_CHANGE                   = unchecked((int)(ETHERCAT_BASE+0x00000002));

  //------ EtherCAT events ------
  public const int KS_ECAT_ERROR                        = unchecked((int)(ETHERCAT_BASE+0x00000000));
  public const int KS_ECAT_DATASET_SIGNAL               = unchecked((int)(ETHERCAT_BASE+0x00000001));
  public const int KS_ECAT_TOPOLOGY_CHANGE              = unchecked((int)(ETHERCAT_BASE+0x00000002));
  public const int KS_ECAT_STATE_CHANGE                 = unchecked((int)(ETHERCAT_BASE+0x00000003));
  public const int KS_ECAT_OBJECT_ACCESS                = unchecked((int)(ETHERCAT_BASE+0x00000004));
  public const int KS_ECAT_FILE_ACCESS                  = unchecked((int)(ETHERCAT_BASE+0x00000005));
  public const int KS_ECAT_PROCESS_DATA_ACCESS          = unchecked((int)(ETHERCAT_BASE+0x00000006));
  public const int KS_ECAT_REDUNDANCY_EFFECTIVE         = unchecked((int)(ETHERCAT_BASE+0x00000007));

  //------ Topology change reasons ------
  public const int KS_ECAT_TOPOLOGY_MASTER_CONNECTED    = unchecked((int)0x00000000);
  public const int KS_ECAT_TOPOLOGY_MASTER_DISCONNECTED = unchecked((int)0x00000001);
  public const int KS_ECAT_TOPOLOGY_SLAVE_COUNT_CHANGED = unchecked((int)0x00000002);
  public const int KS_ECAT_TOPOLOGY_SLAVE_ONLINE        = unchecked((int)0x00000003);
  public const int KS_ECAT_TOPOLOGY_SLAVE_OFFLINE       = unchecked((int)0x00000004);

  //------ DataSet commands ------
  public const int KS_ECAT_GET_BUFFER_SIZE              = unchecked((int)0x00000001);

  //------ Slave commands ------
  public const int KS_ECAT_READ_SYNC0_STATUS            = unchecked((int)0x00000001);
  public const int KS_ECAT_READ_SYNC1_STATUS            = unchecked((int)0x00000002);
  public const int KS_ECAT_READ_LATCH0_STATUS           = unchecked((int)0x00000003);
  public const int KS_ECAT_READ_LATCH1_STATUS           = unchecked((int)0x00000004);
  public const int KS_ECAT_READ_LATCH_TIMESTAMPS        = unchecked((int)0x00000005);
  public const int KS_ECAT_ENABLE_CYCLIC_MAILBOX_CHECK  = unchecked((int)0x00000006);
  public const int KS_ECAT_GET_PDO_OVERSAMPLING_FACTOR  = unchecked((int)0x00000007);
  public const int KS_ECAT_SET_SYNCMANAGER_WATCHDOGS    = unchecked((int)0x00000008);
  public const int KS_ECAT_GET_SYNCMANAGER_WATCHDOGS    = unchecked((int)0x00000009);
  public const int KS_ECAT_HAS_XML_INFORMATION          = unchecked((int)0x0000000a);

  //------ Slave Device / EAP-Node commands ------
  public const int KS_ECAT_SIGNAL_AL_ERROR              = unchecked((int)0x00000001);

  //------ EtherCAT data structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatMasterState {
    public int structSize;
    public int connected;
    public int slavesOnline;
    public int slavesCreated;
    public int slavesCreatedAndOnline;
    public int masterState;
    public int lastError;
    public int redundancyEffective;
    public int framesSent;
    public int framesLost;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatSlaveState {
    public int structSize;
    public int online;
    public int created;
    public int vendor;
    public int product;
    public int revision;
    public int id;
    public int relPosition;
    public int absPosition;
    public int linkState;
    public int slaveState;
    public int statusCode;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatDataVarInfo {
    public int objType;
    public char* name;
    public int dataType;
    public int bitLength;
    public int subIndex;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatDataObjInfo {
    public int objType;
    public char* name;
    public int bitLength;
    public int index;
    public int syncIndex;
    public int varCount;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatSlaveInfo {
    public int vendorId;
    public int productId;
    public int revision;
    public int serial;
    public char* group;
    public char* image;
    public char* order;
    public char* name;
    public int objCount;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatDcParams {
    public ushort assignActivate;
    public byte latch0Control;
    public byte latch1Control;
    public int pulseLength;
    public int cycleTimeSync0;
    public int cycleTimeSync0Factor;
    public int shiftTimeSync0;
    public int shiftTimeSync0Factor;
    public int cycleTimeSync1;
    public int cycleTimeSync1Factor;
    public int shiftTimeSync1;
    public int shiftTimeSync1Factor;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatDcLatchTimeStamps {
    public long latch0PositivEdge;
    public long latch0NegativEdge;
    public long latch1PositivEdge;
    public long latch1NegativEdge;
    public long smBufferChange;
    public long smPdiBufferStart;
    public long smPdiBufferChange;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatEapNodeState {
    public int structSize;
    public int connected;
    public int proxiesOnline;
    public int proxiesCreated;
    public int proxiesCreatedAndOnline;
    public int nodeState;
    public int statusCode;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatEapProxyState {
    public int structSize;
    public int online;
    public int created;
    public uint ip;
    public int proxyState;
    public int statusCode;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatEapNodeId {
    public int structSize;
    public uint ip;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSEcatSlaveDeviceState {
    public int structSize;
    public int slaveState;
    public int statusCode;
  };

  //------ Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct EcatErrorUserContext {
    public int ctxType;
    public Handle hMaster;
    public Handle hSlave;
    public int error;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct EcatDataSetUserContext {
    public int ctxType;
    public Handle hDataSet;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct EcatTopologyUserContext {
    public int ctxType;
    public Handle hMaster;
    public int slavesOnline;
    public int slavesCreated;
    public int slavesCreatedAndOnline;
    public int reason;
    public Handle hSlave;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct EcatRedundancyUserContext {
    public int ctxType;
    public Handle hMaster;
    public int redundancyEffective;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct EcatStateChangeUserContext {
    public int ctxType;
    public Handle hObject;
    public int currentState;
    public int requestedState;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct EcatObjectAccessUserContext {
    public int ctxType;
    public Handle hObject;
    public int index;
    public int subIndex;
    public int writeAccess;
    public int completeAccess;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct EcatFileAccessUserContext {
    public int ctxType;
    public Handle hObject;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
    public byte[] fileName;
    public int password;
    public int writeAccess;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct EcatProcessDataAccessUserContext {
    public int ctxType;
    public Handle hObject;
  };

  //------ Master functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createEcatMaster(
    Handle* phMaster, Handle hConnection, string libraryPath, string topologyFile, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeEcatMaster(
    Handle hMaster);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installEcatMasterHandler(
    Handle hMaster, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryEcatMasterState(
    Handle hMaster, ref KSEcatMasterState pMasterState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_changeEcatMasterState(
    Handle hMaster, int state, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_addEcatRedundancy(
    Handle hMaster, Handle hConnection, int flags);

  //------ Slave functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createEcatSlave(
    Handle hMaster, Handle* phSlave, int id, int position, int vendor, int product, int revision, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createEcatSlaveIndirect(
    Handle hMaster, Handle* phSlave, ref KSEcatSlaveState pSlaveState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumEcatSlaves(
    Handle hMaster, int index, ref KSEcatSlaveState pSlaveState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_deleteEcatSlave(
    Handle hSlave);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_writeEcatSlaveId(
    Handle hSlave, int id, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryEcatSlaveState(
    Handle hSlave, ref KSEcatSlaveState pSlaveState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_changeEcatSlaveState(
    Handle hSlave, int state, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryEcatSlaveInfo(
    Handle hSlave, ref KSEcatSlaveInfo* ppSlaveInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryEcatDataObjInfo(
    Handle hSlave, int objIndex, ref KSEcatDataObjInfo* ppDataObjInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryEcatDataVarInfo(
    Handle hSlave, int objIndex, int varIndex, ref KSEcatDataVarInfo* ppDataVarInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumEcatDataObjInfo(
    Handle hSlave, int objEnumIndex, ref KSEcatDataObjInfo* ppDataObjInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumEcatDataVarInfo(
    Handle hSlave, int objEnumIndex, int varEnumIndex, ref KSEcatDataVarInfo* ppDataVarInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_loadEcatInitCommands(
    Handle hSlave, string filename, int flags);

  //------ DataSet related functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createEcatDataSet(
    Handle hObject, Handle* phSet, void** ppAppPtr, void** ppSysPtr, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_deleteEcatDataSet(
    Handle hSet);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_assignEcatDataSet(
    Handle hSet, Handle hObject, int syncIndex, int placement, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_readEcatDataSet(
    Handle hSet, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_postEcatDataSet(
    Handle hSet, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installEcatDataSetHandler(
    Handle hSet, int eventCode, Handle hSignal, int flags);

  //------ Common functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_changeEcatState(
    Handle hObject, int state, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installEcatHandler(
    Handle hObject, int eventCode, Handle hSignal, int flags);

  //------ Data exchange functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getEcatDataObjAddress(
    Handle hSet, Handle hObject, int pdoIndex, int varIndex, void** ppAppPtr, void** ppSysPtr, int* pBitOffset, int* pBitLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_readEcatDataObj(
    Handle hObject, int objIndex, int varIndex, void* pData, int* pSize, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_postEcatDataObj(
    Handle hObject, int objIndex, int varIndex, void* pData, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setEcatPdoAssign(
    Handle hObject, int syncIndex, int pdoIndex, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setEcatPdoMapping(
    Handle hObject, int pdoIndex, int objIndex, int varIndex, int bitLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_uploadEcatFile(
    Handle hObject, string fileName, int password, void* pData, int* pSize, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_downloadEcatFile(
    Handle hObject, string fileName, int password, void* pData, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_readEcatSlaveMem(
    Handle hSlave, int address, void* pData, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_writeEcatSlaveMem(
    Handle hSlave, int address, void* pData, int size, int flags);

  //------ DC functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_activateEcatDcMode(
    Handle hObject, ref long startTime, int cycleTime, int shiftTime, Handle hTimer, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumEcatDcOpModes(
    Handle hSlave, int index, byte* pDcOpModeBuf, byte* pDescriptionBuf, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_lookupEcatDcOpMode(
    Handle hSlave, string dcOpMode, ref KSEcatDcParams pDcParams, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_configEcatDcOpMode(
    Handle hSlave, string dcOpMode, ref KSEcatDcParams pDcParams, int flags);

  //------ Auxiliary functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execEcatCommand(
    Handle hObject, int command, void* pParam, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getEcatAlias(
    Handle hObject, int objIndex, int varIndex, byte* pAliasBuf, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setEcatAlias(
    Handle hObject, int objIndex, int varIndex, string alias, int flags);

  //------ EtherCAT Slave Device functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openEcatSlaveDevice(
    Handle* phSlaveDevice, string name, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeEcatSlaveDevice(
    Handle hSlaveDevice, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryEcatSlaveDeviceState(
    Handle hSlaveDevice, ref KSEcatSlaveDeviceState pSlaveState, int flags);

  //------ EtherCAT Automation Protocol functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createEcatEapNode(
    Handle* phNode, Handle hConnection, string networkFile, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeEcatEapNode(
    Handle hNode, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryEcatEapNodeState(
    Handle hNode, ref KSEcatEapNodeState pNodeState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createEcatEapProxy(
    Handle hNode, Handle* phProxy, ref KSEcatEapNodeId pNodeId, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_deleteEcatEapProxy(
    Handle hProxy, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryEcatEapProxyState(
    Handle hProxy, ref KSEcatEapProxyState pProxyState, int flags);

  //------------------------------------------------------------------------------------------------------------
  // MultiFunction Module
  //------------------------------------------------------------------------------------------------------------

  //------ Flags ------
  public const int KSF_UNIPOLAR                         = unchecked((int)0x00000001);
  public const int KSF_BIPOLAR                          = unchecked((int)0x00000000);
  public const int KSF_DIFFERENTIAL                     = unchecked((int)0x00000002);
  public const int KSF_SINGLE_ENDED                     = unchecked((int)0x00000000);
  public const int KSF_RISING_EDGE                      = unchecked((int)0x00001000);
  public const int KSF_FALLING_EDGE                     = unchecked((int)0x00000000);
  public const int KSF_NORMALIZE                        = unchecked((int)0x00000800);
  public const int KSF_STOP_ANALOG_STREAM               = unchecked((int)0x00000400);

  //------ Multi function events ------
  public const int KS_MULTIFUNCTION_INTERRUPT           = unchecked((int)(MULTIFUNCTION_BASE+0x00000000));
  public const int KS_MULTIFUNCTION_ANALOGWATCHDOG      = unchecked((int)(MULTIFUNCTION_BASE+0x00000001));
  public const int KS_MULTIFUNCTION_DIGITALWATCHDOG     = unchecked((int)(MULTIFUNCTION_BASE+0x00000002));
  public const int KS_MULTIFUNCTION_TIMER               = unchecked((int)(MULTIFUNCTION_BASE+0x00000003));
  public const int KS_MULTIFUNCTION_ANALOG_SEQUENCE     = unchecked((int)(MULTIFUNCTION_BASE+0x00000004));
  public const int KS_MULTIFUNCTION_ANALOG_STREAM       = unchecked((int)(MULTIFUNCTION_BASE+0x00000006));
  public const int KS_MULTIFUNCTION_DIGITAL_IN          = unchecked((int)(MULTIFUNCTION_BASE+0x00000007));
  public const int KS_MULTIFUNCTION_BUFFER_FULL         = unchecked((int)(MULTIFUNCTION_BASE+0x00000008));

  //------ Board commands ------
  public const int KS_BOARD_RESET                       = unchecked((int)0x00000000);
  public const int KS_BOARD_SEQUENCE_READY              = unchecked((int)0x00000001);

  //------ Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct BoardUserContext {
    public int ctxType;
    public Handle hBoard;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct BoardStreamErrorContext {
    public int ctxType;
    public Handle hBoard;
    public int lostDataCount;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSMfBoardInfo {
    public int structSize;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
    public byte[] pBoardName;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
    public byte[] pBoardSpec;
    public int analogInp;
    public int analogOut;
    public int digitalInOut;
    public int digitalInp;
    public int digitalOut;
    public int digitalPortSize;
    public int analogValueSize;
  };

  //------ Basic functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openBoard(
    Handle* phBoard, string name, Handle hKernel, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeBoard(
    Handle hBoard, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getBoardInfo(
    Handle hBoard, ref KSMfBoardInfo pBoardInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setBoardObject(
    Handle hBoard, void* pAppAddr, void* pSysAddr);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getBoardObject(
    Handle hBoard, void** ppAddr);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installBoardHandler(
    Handle hBoard, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execBoardCommand(
    Handle hBoard, int command, void* pParam, int flags);

  //------ Low-level functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_readBoardRegister(
    Handle hBoard, int rangeIndex, int offset, int* pValue, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_writeBoardRegister(
    Handle hBoard, int rangeIndex, int offset, int value, int flags);

  //------ Digital input functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getDigitalBit(
    Handle hBoard, int channel, int* pValue, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getDigitalPort(
    Handle hBoard, int port, int* pValue, int flags);

  //------ Digital output functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setDigitalBit(
    Handle hBoard, int channel, uint value, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setDigitalPort(
    Handle hBoard, int port, uint value, uint mask, int flags);

  //------ Analog input functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_configAnalogChannel(
    Handle hBoard, int channel, int gain, int mode, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getAnalogValue(
    Handle hBoard, int channel, int* pValue, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_initAnalogStream(
    Handle hBoard, int* pChannelSequence, int sequenceLength, int sequenceCount, int period, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_startAnalogStream(
    Handle hBoard, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getAnalogStream(
    Handle hBoard, void* pBuffer, int length, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_valueToVolt(
    Handle hBoard, int gain, int mode, uint value, float pVolt, int flags);

  //------ Analog output functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_setAnalogValue(
    Handle hBoard, int channel, int mode, int value, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_voltToValue(
    Handle hBoard, int mode, float volt, int* pValue, int flags);

  // -------------------------------------------------------------------------------------------------------------
  // MultiFunction Module Driver Interface
  // -------------------------------------------------------------------------------------------------------------

  // You only need KSDRV functions, if you develop your own driver DLL!

  //------------------------------------------------------------------------------------------------------------
  // CAN Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_BAD_IDENTIFIER               = unchecked((int)(KSERROR_CATEGORY_CAN+0x00000000));
  public const int KSERROR_RECV_BUFFER_FULL             = unchecked((int)(KSERROR_CATEGORY_CAN+0x00010000));
  public const int KSERROR_XMIT_BUFFER_FULL             = unchecked((int)(KSERROR_CATEGORY_CAN+0x00020000));
  public const int KSERROR_BUS_OFF                      = unchecked((int)(KSERROR_CATEGORY_CAN+0x00030000));
  public const int KSERROR_BUS_PASSIVE                  = unchecked((int)(KSERROR_CATEGORY_CAN+0x00040000));
  public const int KSERROR_ERROR_COUNTER_WARNING        = unchecked((int)(KSERROR_CATEGORY_CAN+0x00050000));
  public const int KSERROR_PORT_DATA_BUFFER_OVERRUN     = unchecked((int)(KSERROR_CATEGORY_CAN+0x00060000));
  public const int KSERROR_BUS_ERROR                    = unchecked((int)(KSERROR_CATEGORY_CAN+0x00070000));

  //------ CAN mode ------
  public const int KS_CAN_NORMAL_MODE                   = unchecked((int)0x00000001);
  public const int KS_CAN_RESET_MODE                    = unchecked((int)0x00000002);
  public const int KS_CAN_LISTEN_ONLY_MODE              = unchecked((int)0x00000004);

  //------ CAN state ------
  public const int KS_CAN_BUS_ON                        = unchecked((int)0x00000001);
  public const int KS_CAN_BUS_HEAVY                     = unchecked((int)0x00000002);
  public const int KS_CAN_BUS_OFF                       = unchecked((int)0x00000004);
  public const int KS_CAN_RECV_AVAIL                    = unchecked((int)0x00000008);
  public const int KS_CAN_XMIT_PENDING                  = unchecked((int)0x00000010);
  public const int KS_CAN_WARN_LIMIT_REACHED            = unchecked((int)0x00000020);
  public const int KS_CAN_DATA_BUFFER_OVERRUN           = unchecked((int)0x00000040);

  //------ CAN commands ------
  public const int KS_SIGNAL_XMIT_EMPTY                 = unchecked((int)0x00000004);
  public const int KS_SET_LISTEN_ONLY                   = unchecked((int)0x00000005);
  public const int KS_RESET_PORT                        = unchecked((int)0x00000006);
  public const int KS_GET_ERROR_CODE_CAPTURE            = unchecked((int)0x00000007);
  public const int KS_CAN_SET_XMIT_TIMEOUT              = unchecked((int)0x00000008);
  public const int KS_CAN_CLEAR_BUS_OFF                 = unchecked((int)0x0000000a);

  //------ CAN baud rates ------
  public const int KS_CAN_BAUD_1M                       = unchecked((int)0x000f4240);
  public const int KS_CAN_BAUD_800K                     = unchecked((int)0x000c3500);
  public const int KS_CAN_BAUD_666K                     = unchecked((int)0x000a2c2a);
  public const int KS_CAN_BAUD_500K                     = unchecked((int)0x0007a120);
  public const int KS_CAN_BAUD_250K                     = unchecked((int)0x0003d090);
  public const int KS_CAN_BAUD_125K                     = unchecked((int)0x0001e848);
  public const int KS_CAN_BAUD_100K                     = unchecked((int)0x000186a0);
  public const int KS_CAN_BAUD_50K                      = unchecked((int)0x0000c350);
  public const int KS_CAN_BAUD_20K                      = unchecked((int)0x00004e20);
  public const int KS_CAN_BAUD_10K                      = unchecked((int)0x00002710);

  //------ CAN-FD baud rates ------
  public const int KS_CAN_FD_BAUD_12M                   = unchecked((int)0x00b71b00);
  public const int KS_CAN_FD_BAUD_10M                   = unchecked((int)0x00989680);
  public const int KS_CAN_FD_BAUD_8M                    = unchecked((int)0x007a1200);
  public const int KS_CAN_FD_BAUD_6M                    = unchecked((int)0x005b8d80);
  public const int KS_CAN_FD_BAUD_4M                    = unchecked((int)0x003d0900);
  public const int KS_CAN_FD_BAUD_2M                    = unchecked((int)0x001e8480);

  //------ Common CAN and CAN-FD baud rates ------
  public const int KS_CAN_BAUD_CUSTOM                   = unchecked((int)0x00000000);

  //------ CAN message types ------
  public const int KS_CAN_MSGTYPE_STANDARD              = unchecked((int)0x00000001);
  public const int KS_CAN_MSGTYPE_EXTENDED              = unchecked((int)0x00000002);
  public const int KS_CAN_MSGTYPE_RTR                   = unchecked((int)0x00000004);
  public const int KS_CAN_MSGTYPE_FD                    = unchecked((int)0x00000008);
  public const int KS_CAN_MSGTYPE_BRS                   = unchecked((int)0x00000010);

  //------ Legacy CAN events ------
  public const int KS_CAN_RECV                          = unchecked((int)(CAN_BASE+0x00000000));
  public const int KS_CAN_XMIT_EMPTY                    = unchecked((int)(CAN_BASE+0x00000001));
  public const int KS_CAN_XMIT_RTR                      = unchecked((int)(CAN_BASE+0x00000002));
  public const int KS_CAN_ERROR                         = unchecked((int)(CAN_BASE+0x00000003));
  public const int KS_CAN_FILTER                        = unchecked((int)(CAN_BASE+0x00000004));
  public const int KS_CAN_TIMEOUT                       = unchecked((int)(CAN_BASE+0x00000005));

  //------ CAN-FD events ------
  public const int KS_CAN_FD_RECV                       = unchecked((int)(CAN_BASE+0x00000006));
  public const int KS_CAN_FD_XMIT_EMPTY                 = unchecked((int)(CAN_BASE+0x00000007));
  public const int KS_CAN_FD_ERROR                      = unchecked((int)(CAN_BASE+0x00000008));

  //------ CAN-FD data structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCanFdMsg {
    public int structSize;
    public int identifier;
    public uint msgType;
    public int dataLength;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
    public byte[] msgData;
    public long timestamp;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCanFdState {
    public int structSize;
    public uint state;
    public int waitingForXmit;
    public int waitingForRecv;
    public int xmitErrCounter;
    public int recvErrCounter;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCanFdConfig {
    public int structSize;
    public int clockFrequency;
    public int canBaudRate;
    public int canBrp;
    public int canTseg1;
    public int canTseg2;
    public int canSjw;
    public int fdBaudRate;
    public int fdBrp;
    public int fdTseg1;
    public int fdTseg2;
    public int fdSjw;
  };

  //------ CAN-FD Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCanFdContext {
    public int ctxType;
    public Handle hCanFd;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCanFdErrorContext {
    public int ctxType;
    public Handle hCanFd;
    public int errorValue;
    public long timestamp;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCanFdMsgContext {
    public int ctxType;
    public Handle hCanFd;
    public KSCanFdMsg msg;
  };

  //------ CAN data structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCanMsg {
    public int identifier;
    public ushort msgType;
    public ushort dataLength;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
    public byte[] msgData;
    public long timestamp;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCanState {
    public int structSize;
    public int mode;
    public int state;
    public int baudRate;
    public int waitingForXmit;
    public int waitingForRecv;
    public int errorWarnLimit;
    public int recvErrCounter;
    public int xmitErrCounter;
    public int lastError;
  };

  //------ CAN Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct CanUserContext {
    public int ctxType;
    public Handle hCan;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct CanErrorUserContext {
    public int ctxType;
    public Handle hCan;
    public int errorValue;
    public long timestamp;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct CanMsgUserContext {
    public int ctxType;
    public Handle hCan;
    public KSCanMsg msg;
    public int discarded;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct CanTimeoutUserContext {
    public int ctxType;
    public Handle hCan;
    public KSCanMsg msg;
  };

  //------ Common CAN-FD functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openCanFd(
    Handle* phCanFd, string name, int port, ref KSCanFdConfig pCanFdConfig, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeCanFd(
    Handle hCanFd, int flags);

  //------ CAN-FD messaging functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitCanFdMsg(
    Handle hCanFd, ref KSCanFdMsg pCanFdMsg, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvCanFdMsg(
    Handle hCanFd, ref KSCanFdMsg pCanFdMsg, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installCanFdHandler(
    Handle hCanFd, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getCanFdState(
    Handle hCanFd, ref KSCanFdState pCanFdState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execCanFdCommand(
    Handle hCanFd, int command, void* pParam, int flags);

  //------ Common CAN functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openCan(
    Handle* phCan, string name, int port, int baudRate, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openCanEx(
    Handle* phCan, Handle hConnection, int port, int baudRate, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeCan(
    Handle hCan);

  //------ CAN messaging functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitCanMsg(
    Handle hCan, ref KSCanMsg pCanMsg, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvCanMsg(
    Handle hCan, ref KSCanMsg pCanMsg, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installCanHandler(
    Handle hCan, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getCanState(
    Handle hCan, ref KSCanState pCanState);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execCanCommand(
    Handle hCan, int command, void* pParam, int flags);

  //------------------------------------------------------------------------------------------------------------
  // LIN Module
  //------------------------------------------------------------------------------------------------------------

  //------ LIN commands ------
  public const int KS_LIN_RESET                         = unchecked((int)0x00000000);
  public const int KS_LIN_SLEEP                         = unchecked((int)0x00000001);
  public const int KS_LIN_WAKEUP                        = unchecked((int)0x00000002);
  public const int KS_LIN_SET_BAUD_RATE                 = unchecked((int)0x00000004);
  public const int KS_LIN_SET_VERSION                   = unchecked((int)0x00000005);
  public const int KS_LIN_DISABLE_QUEUE_ERROR           = unchecked((int)0x00000006);
  public const int KS_LIN_ENABLE_QUEUE_ERROR            = unchecked((int)0x00000007);

  //------ LIN bit rates ------
  public const int KS_LIN_BAUD_19200                    = unchecked((int)0x00004b00);
  public const int KS_LIN_BAUD_10417                    = unchecked((int)0x000028b1);
  public const int KS_LIN_BAUD_9600                     = unchecked((int)0x00002580);
  public const int KS_LIN_BAUD_4800                     = unchecked((int)0x000012c0);
  public const int KS_LIN_BAUD_2400                     = unchecked((int)0x00000960);
  public const int KS_LIN_BAUD_1200                     = unchecked((int)0x000004b0);

  //------ LIN Errors ------
  public const int KS_LIN_ERROR_PHYSICAL                = unchecked((int)0x00000001);
  public const int KS_LIN_ERROR_TRANSPORT               = unchecked((int)0x00000002);
  public const int KS_LIN_ERROR_BUS_COLLISION           = unchecked((int)0x00000003);
  public const int KS_LIN_ERROR_PARITY                  = unchecked((int)0x00000004);
  public const int KS_LIN_ERROR_CHECKSUM                = unchecked((int)0x00000005);
  public const int KS_LIN_ERROR_BREAK_EXPECTED          = unchecked((int)0x00000006);
  public const int KS_LIN_ERROR_RESPONSE_TIMEOUT        = unchecked((int)0x00000007);
  public const int KS_LIN_ERROR_PID_TIMEOUT             = unchecked((int)0x00000009);
  public const int KS_LIN_ERROR_RESPONSE_TOO_SHORT      = unchecked((int)0x00000010);
  public const int KS_LIN_ERROR_RECV_QUEUE_FULL         = unchecked((int)0x00000011);
  public const int KS_LIN_ERROR_RESPONSE_WITHOUT_HEADER = unchecked((int)0x00000012);

  //------ LIN events ------
  public const int KS_LIN_ERROR                         = unchecked((int)0x00000000);
  public const int KS_LIN_RECV_HEADER                   = unchecked((int)0x00000001);
  public const int KS_LIN_RECV_RESPONSE                 = unchecked((int)0x00000002);

  //------ LIN data structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSLinProperties {
    public int structSize;
    public int linVersion;
    public int baudRate;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSLinState {
    public int structSize;
    public KSLinProperties properties;
    public int xmitHdrCount;
    public int xmitRspCount;
    public int recvCount;
    public int recvAvail;
    public int recvErrorCount;
    public int recvNoRspCount;
    public int collisionCount;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSLinHeader {
    public int identifier;
    public byte parity;
    public byte parityOk;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSLinResponse {
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
    public byte[] data;
    public int dataLen;
    public byte checksum;
    public byte checksumOk;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSLinMsg {
    public KSLinHeader header;
    public KSLinResponse response;
    public long timestamp;
  };

  //------ LIN Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct LinUserContext {
    public int ctxType;
    public Handle hLin;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct LinErrorUserContext {
    public int ctxType;
    public Handle hLin;
    public long timestamp;
    public int errorCode;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct LinHeaderUserContext {
    public int ctxType;
    public Handle hLin;
    public long timestamp;
    public KSLinHeader header;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct LinResponseUserContext {
    public int ctxType;
    public Handle hLin;
    public long timestamp;
    public KSLinMsg msg;
  };

  //------ Common functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openLin(
    Handle* phLin, string pDeviceName, int port, ref KSLinProperties pProperties, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openLinEx(
    Handle* phLin, Handle hLinDevice, ref KSLinProperties pProperties, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeLin(
    Handle hLin, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installLinHandler(
    Handle hLin, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execLinCommand(
    Handle hLin, int command, void* pParam, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitLinHeader(
    Handle hLin, int identifier, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_xmitLinResponse(
    Handle hLin, void* pData, int size, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvLinMsg(
    Handle hLin, ref KSLinMsg pLinMsg, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getLinState(
    Handle hLin, ref KSLinState pLinState, int flags);

  //------------------------------------------------------------------------------------------------------------
  // Profibus Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public const int KSERROR_PBUS_CONNECTION_CHANGE       = unchecked((int)(KSERROR_CATEGORY_PROFIBUS+0x00010000));
  public const int KSERROR_PBUS_TOPOLOGY_CHANGE         = unchecked((int)(KSERROR_CATEGORY_PROFIBUS+0x00020000));
  public const int KSERROR_PBUS_SLAVE_ERROR             = unchecked((int)(KSERROR_CATEGORY_PROFIBUS+0x00030000));
  public const int KSERROR_PBUS_ALARM_BUFFER_FULL       = unchecked((int)(KSERROR_CATEGORY_PROFIBUS+0x00040000));
  public const int KSERROR_BAD_PARAMETERIZATION_SET     = unchecked((int)(KSERROR_CATEGORY_PROFIBUS+0x00050000));
  public const int KSERROR_PBUS_MASTER_ERROR            = unchecked((int)(KSERROR_CATEGORY_PROFIBUS+0x00060000));
  public const int KSERROR_HARDWARE_INTERFACE           = unchecked((int)(KSERROR_CATEGORY_PROFIBUS+0x00070000));
  public const int KSERROR_AUTO_CLEAR                   = unchecked((int)(KSERROR_CATEGORY_PROFIBUS+0x00080000));

  //------ Flags ------
  public const int KSF_ACCEPT_OFFLINE                   = unchecked((int)0x00000001);
  public const int KSF_SLOT                             = unchecked((int)0x00000002);

  //------ Master State ------
  public const int KS_PBUS_OFFLINE                      = unchecked((int)0x00000001);
  public const int KS_PBUS_STOP                         = unchecked((int)0x00000002);
  public const int KS_PBUS_CLEAR                        = unchecked((int)0x00000003);
  public const int KS_PBUS_OPERATE                      = unchecked((int)0x00000004);

  //------ Profibus special indices ------
  public const int KS_PBUS_INDEX_ALL                    = unchecked((int)0xffffffff);
  public const int KS_PBUS_INDEX_INPUT                  = unchecked((int)0xfffffffe);
  public const int KS_PBUS_INDEX_OUTPUT                 = unchecked((int)0xfffffffd);

  //------ Profibus commands ------
  public const int KS_UNFREEZE                          = unchecked((int)0x00000001);
  public const int KS_FREEZE                            = unchecked((int)0x00000002);
  public const int KS_UNSYNC                            = unchecked((int)0x00000004);
  public const int KS_SYNC                              = unchecked((int)0x00000008);

  //------ Profibus command params ------
  public const int KS_SELECT_GROUP_1                    = unchecked((int)0x00000100);
  public const int KS_SELECT_GROUP_2                    = unchecked((int)0x00000200);
  public const int KS_SELECT_GROUP_3                    = unchecked((int)0x00000400);
  public const int KS_SELECT_GROUP_4                    = unchecked((int)0x00000800);
  public const int KS_SELECT_GROUP_5                    = unchecked((int)0x00001000);
  public const int KS_SELECT_GROUP_6                    = unchecked((int)0x00002000);
  public const int KS_SELECT_GROUP_7                    = unchecked((int)0x00004000);
  public const int KS_SELECT_GROUP_8                    = unchecked((int)0x00008000);

  //------ Profibus bit rates ------
  public const int KS_PBUS_BAUD_12M                     = unchecked((int)0x00b71b00);
  public const int KS_PBUS_BAUD_6M                      = unchecked((int)0x005b8d80);
  public const int KS_PBUS_BAUD_3M                      = unchecked((int)0x002dc6c0);
  public const int KS_PBUS_BAUD_1500K                   = unchecked((int)0x0016e360);
  public const int KS_PBUS_BAUD_500K                    = unchecked((int)0x0007a120);
  public const int KS_PBUS_BAUD_187500                  = unchecked((int)0x0002dc6c);
  public const int KS_PBUS_BAUD_93750                   = unchecked((int)0x00016e36);
  public const int KS_PBUS_BAUD_45450                   = unchecked((int)0x0000b18a);
  public const int KS_PBUS_BAUD_31250                   = unchecked((int)0x00007a12);
  public const int KS_PBUS_BAUD_19200                   = unchecked((int)0x00004b00);
  public const int KS_PBUS_BAUD_9600                    = unchecked((int)0x00002580);

  //------ Profibus events ------
  public const int KS_PBUS_ERROR                        = unchecked((int)(PROFIBUS_BASE+0x00000000));
  public const int KS_PBUS_DATASET_SIGNAL               = unchecked((int)(PROFIBUS_BASE+0x00000001));
  public const int KS_PBUS_TOPOLOGY_CHANGE              = unchecked((int)(PROFIBUS_BASE+0x00000002));
  public const int KS_PBUS_ALARM                        = unchecked((int)(PROFIBUS_BASE+0x00000003));
  public const int KS_PBUS_DIAGNOSTIC                   = unchecked((int)(PROFIBUS_BASE+0x00000004));

  //------ Profibus alarm types ------
  public const int KS_PBUS_DIAGNOSTIC_ALARM             = unchecked((int)0x00000001);
  public const int KS_PBUS_PROCESS_ALARM                = unchecked((int)0x00000002);
  public const int KS_PBUS_PULL_ALARM                   = unchecked((int)0x00000003);
  public const int KS_PBUS_PLUG_ALARM                   = unchecked((int)0x00000004);
  public const int KS_PBUS_STATUS_ALARM                 = unchecked((int)0x00000005);
  public const int KS_PBUS_UPDATE_ALARM                 = unchecked((int)0x00000006);

  //------ Profibus topology change reasons ------
  public const int KS_PBUS_TOPOLOGY_MASTER_CONNECTED    = unchecked((int)0x00000000);
  public const int KS_PBUS_TOPOLOGY_MASTER_DISCONNECTED = unchecked((int)0x00000001);
  public const int KS_PBUS_TOPOLOGY_SLAVE_COUNT_CHANGED = unchecked((int)0x00000002);
  public const int KS_PBUS_TOPOLOGY_SLAVE_ONLINE        = unchecked((int)0x00000003);
  public const int KS_PBUS_TOPOLOGY_SLAVE_OFFLINE       = unchecked((int)0x00000004);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PbusErrorUserContext {
    public int ctxType;
    public Handle hMaster;
    public Handle hSlave;
    public int error;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PbusDataSetUserContext {
    public int ctxType;
    public Handle hDataSet;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PbusTopologyUserContext {
    public int ctxType;
    public Handle hMaster;
    public Handle hSlave;
    public int slavesOnline;
    public int slavesConfigured;
    public int reason;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PbusAlarmUserContext {
    public int ctxType;
    public Handle hMaster;
    public Handle hSlave;
    public int slaveAddress;
    public int alarmType;
    public int slotNumber;
    public int specifier;
    public int diagnosticLength;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 60)]
    public int[] diagnosticData;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PbusDiagnosticUserContext {
    public int ctxType;
    public Handle hMaster;
    public Handle hSlave;
    public int slaveAddress;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSPbusMasterState {
    public int structSize;
    public int connected;
    public int slavesOnline;
    public int highestStationAddr;
    public int slavesConfigured;
    public int masterState;
    public int busErrorCounter;
    public int timeoutCounter;
    public int lastError;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSPbusSlaveState {
    public int structSize;
    public int address;
    public int identNumber;
    public int configured;
    public int assigned;
    public int state;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public byte[] stdStationStatus;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 238)]
    public byte[] extendedDiag;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSPbusDataVarInfo {
    public int dataType;
    public int subIndex;
    public int byteLength;
    public char* name;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSPbusSlotInfo {
    public int slotNumber;
    public int varCount;
    public int byteLength;
    public char* name;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSPbusSlaveInfo {
    public int address;
    public int identNumber;
    public int slotCount;
    public int byteLength;
    public char* name;
  };

  //------ Master functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createPbusMaster(
    Handle* phMaster, string name, string libraryPath, string topologyFile, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closePbusMaster(
    Handle hMaster);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryPbusMasterState(
    Handle hMaster, ref KSPbusMasterState pMasterState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_changePbusMasterState(
    Handle hMaster, int state, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumPbusSlaves(
    Handle hMaster, int index, ref KSPbusSlaveState pSlaveState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installPbusMasterHandler(
    Handle hMaster, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execPbusMasterCommand(
    Handle hMaster, int command, void* pParam, int flags);

  //------ Slave functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createPbusSlave(
    Handle hMaster, Handle* phSlave, int address, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_deletePbusSlave(
    Handle hSlave);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryPbusSlaveState(
    Handle hSlave, ref KSPbusSlaveState pSlaveState, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_changePbusSlaveState(
    Handle hSlave, int state, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryPbusSlaveInfo(
    Handle hSlave, ref KSPbusSlaveInfo* ppSlaveInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryPbusSlotInfo(
    Handle hSlave, int objIndex, ref KSPbusSlotInfo* ppSlotInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_queryPbusDataVarInfo(
    Handle hSlave, int objIndex, int varIndex, ref KSPbusDataVarInfo* ppDataVarInfo, int flags);

  //------ Data exchange functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createPbusDataSet(
    Handle hMaster, Handle* phSet, void** ppAppPtr, void** ppSysPtr, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_deletePbusDataSet(
    Handle hSet);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_assignPbusDataSet(
    Handle hSet, Handle hSlave, int slot, int index, int placement, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_readPbusDataSet(
    Handle hSet, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_postPbusDataSet(
    Handle hSet, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installPbusDataSetHandler(
    Handle hSet, int eventCode, Handle hSignal, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getPbusDataObjAddress(
    Handle hSet, Handle hSlave, int slot, int index, void** ppAppPtr, void** ppSysPtr, int* pBitOffset, int* pBitLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_readPbusDataObj(
    Handle hSlave, int slot, int index, void* pData, int* pSize, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_postPbusDataObj(
    Handle hSlave, int slot, int index, void* pData, int size, int flags);

  //------------------------------------------------------------------------------------------------------------
  // Camera Module
  //------------------------------------------------------------------------------------------------------------

  //------ Flags ------
  public const int KSF_NO_DISCOVERY                     = unchecked((int)0x00000800);
  public const int KSF_CAMERA_NO_GENICAM                = unchecked((int)0x00001000);
  public const int KSF_CAMERA_BROADCAST_DISCOVERY       = unchecked((int)0x00800000);

  //------ Types & Structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraInfo {
    public int structSize;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] hardwareId;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] vendor;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] name;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] serialNumber;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 80)]
    public byte[] userName;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraTimeouts {
    public int structSize;
    public int commandTimeout;
    public int commandRetries;
    public int heartbeatTimeout;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraForceIp {
    public int structSize;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
    public byte[] macAddress;
    public uint address;
    public uint subnetMask;
    public uint gatewayAddress;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraInfoGev {
    public int structSize;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
    public byte[] vendor;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
    public byte[] name;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
    public byte[] serialNumber;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
    public byte[] userName;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
    public byte[] macAddress;
    public int instance;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
    public byte[] instanceName;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraEvent {
    public int structSize;
    public int eventId;
    public ulong timestamp;
    public ulong blockId;
    public int channel;
    public int size;
    public void* pAppPtr;
    public void* pSysPtr;
  };

  //------ Block type structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraBlock {
    public int blockType;
    public void* pAppPtr;
    public void* pSysPtr;
    public ulong id;
    public ulong timestamp;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraImage {
    public int blockType;
    public void* pAppPtr;
    public void* pSysPtr;
    public ulong id;
    public ulong timestamp;
    public int fieldCount;
    public int fieldId;
    public uint pixelFormat;
    public uint width;
    public uint height;
    public uint offsetX;
    public uint offsetY;
    public uint linePadding;
    public uint imagePadding;
    public uint chunkDataPayloadLength;
    public uint chunkLayoutId;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraJpeg {
    public int blockType;
    public void* pAppPtr;
    public void* pSysPtr;
    public ulong id;
    public ulong timestamp;
    public int fieldCount;
    public int fieldId;
    public ulong jpegPayloadSize;
    public ulong tickFrequency;
    public uint dataFormat;
    public int isColorSpace;
    public uint chunkDataPayloadLength;
    public uint chunkLayoutId;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraChunk {
    public int blockType;
    public void* pAppPtr;
    public void* pSysPtr;
    public ulong id;
    public ulong timestamp;
    public uint chunkDataPayloadLength;
    public uint chunkLayoutId;
  };

  //------ Block types ------
  public const int KS_CAMERA_BLOCKTYPE_NONE             = unchecked((int)0x00000000);
  public const int KS_CAMERA_BLOCKTYPE_IMAGE            = unchecked((int)0x00000001);
  public const int KS_CAMERA_BLOCKTYPE_CHUNK            = unchecked((int)0x00000002);
  public const int KS_CAMERA_BLOCKTYPE_JPEG             = unchecked((int)0x00000003);
  public const int KS_CAMERA_BLOCKTYPE_JPEG2000         = unchecked((int)0x00000004);

  //------ Camera commands ------
  public const int KS_CAMERA_READREG                    = unchecked((int)0x00000001);
  public const int KS_CAMERA_READMEM                    = unchecked((int)0x00000002);
  public const int KS_CAMERA_WRITEREG                   = unchecked((int)0x00000003);
  public const int KS_CAMERA_SET_TIMEOUTS               = unchecked((int)0x00000004);
  public const int KS_CAMERA_GET_TIMEOUTS               = unchecked((int)0x00000005);
  public const int KS_CAMERA_GET_XML_FILE               = unchecked((int)0x00000006);
  public const int KS_CAMERA_GET_XML_INFO               = unchecked((int)0x00000007);
  public const int KS_CAMERA_SET_DISCOVERY_INTERVAL     = unchecked((int)0x00000008);
  public const int KS_CAMERA_RESIZE_BUFFERS             = unchecked((int)0x00000009);
  public const int KS_CAMERA_GET_INFO                   = unchecked((int)0x0000000a);
  public const int KS_CAMERA_SET_CHUNK_DATA             = unchecked((int)0x0000000b);
  public const int KS_CAMERA_SEND_TO_SOURCE_PORT        = unchecked((int)0x0000000c);
  public const int KS_CAMERA_FORCEIP                    = unchecked((int)0x0000000d);
  public const int KS_CAMERA_WRITEMEM                   = unchecked((int)0x0000000e);
  public const int KS_CAMERA_RECV_EVENT                 = unchecked((int)0x00000011);

  //------ Acquisition modes ------
  public const int KS_CAMERA_CONTINUOUS                 = unchecked((int)0xffffffff);
  public const int KS_CAMERA_PRESERVE                   = unchecked((int)0x00000000);
  public const int KS_CAMERA_SINGLE_FRAME               = unchecked((int)0x00000001);
  public const int KS_CAMERA_MULTI_FRAME                = unchecked((int)0x00000000);

  //------ Configuration commands ------
  public const int KS_CAMERA_CONFIG_GET                 = unchecked((int)0x00000001);
  public const int KS_CAMERA_CONFIG_SET                 = unchecked((int)0x00000002);
  public const int KS_CAMERA_CONFIG_TYPE                = unchecked((int)0x00000003);
  public const int KS_CAMERA_CONFIG_ENUMERATE           = unchecked((int)0x00000004);
  public const int KS_CAMERA_CONFIG_MIN                 = unchecked((int)0x00000005);
  public const int KS_CAMERA_CONFIG_MAX                 = unchecked((int)0x00000006);
  public const int KS_CAMERA_CONFIG_INC                 = unchecked((int)0x00000007);
  public const int KS_CAMERA_CONFIG_LENGTH              = unchecked((int)0x00010000);

  //------ Configuration types ------
  public const int KS_CAMERA_CONFIG_CATEGORY            = unchecked((int)0x00000001);
  public const int KS_CAMERA_CONFIG_BOOLEAN             = unchecked((int)0x00000002);
  public const int KS_CAMERA_CONFIG_ENUMERATION         = unchecked((int)0x00000003);
  public const int KS_CAMERA_CONFIG_INTEGER             = unchecked((int)0x00000004);
  public const int KS_CAMERA_CONFIG_COMMAND             = unchecked((int)0x00000005);
  public const int KS_CAMERA_CONFIG_FLOAT               = unchecked((int)0x00000006);
  public const int KS_CAMERA_CONFIG_STRING              = unchecked((int)0x00000007);
  public const int KS_CAMERA_CONFIG_REGISTER            = unchecked((int)0x00000008);

  //------ Receive and error handler types ------
  public const int KS_CAMERA_IMAGE_RECEIVED             = unchecked((int)0x00000001);
  public const int KS_CAMERA_IMAGE_DROPPED              = unchecked((int)0x00000002);
  public const int KS_CAMERA_ERROR                      = unchecked((int)0x00000003);
  public const int KS_CAMERA_GEV_ATTACHED               = unchecked((int)0x00000004);
  public const int KS_CAMERA_GEV_DETACHED               = unchecked((int)0x00000005);
  public const int KS_CAMERA_ATTACHED                   = unchecked((int)0x00000006);
  public const int KS_CAMERA_DETACHED                   = unchecked((int)0x00000007);
  public const int KS_CAMERA_EVENT                      = unchecked((int)0x00000008);

  //------ Error codes ------
  public const int KSERROR_CAMERA_COMMAND_ERROR         = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00010000));
  public const int KSERROR_CAMERA_COMMAND_TIMEOUT       = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00020000));
  public const int KSERROR_CAMERA_COMMAND_FAILED        = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00030000));
  public const int KSERROR_CAMERA_COMMAND_BAD_ALIGNMENT = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00040000));
  public const int KSERROR_CAMERA_RECONNECTED           = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x000f0000));
  public const int KSERROR_CAMERA_STREAM_INVALID_DATA   = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00100000));
  public const int KSERROR_CAMERA_STREAM_NO_BUFFER      = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00110000));
  public const int KSERROR_CAMERA_STREAM_NOT_TRANSMITTED 
                                                        = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00120000));
  public const int KSERROR_CAMERA_STREAM_INCOMPLETE_TRANSMISSION 
                                                        = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00130000));
  public const int KSERROR_CAMERA_STREAM_BUFFER_OVERRUN = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00140000));
  public const int KSERROR_CAMERA_STREAM_BUFFER_QUEUED  = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00150000));
  public const int KSERROR_GENICAM_ERROR                = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00800000));
  public const int KSERROR_GENICAM_FEATURE_NOT_IMPLEMENTED 
                                                        = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00810000));
  public const int KSERROR_GENICAM_FEATURE_NOT_AVAILABLE 
                                                        = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00820000));
  public const int KSERROR_GENICAM_FEATURE_ACCESS_DENIED 
                                                        = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00830000));
  public const int KSERROR_GENICAM_FEATURE_NOT_FOUND    = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00840000));
  public const int KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00850000));
  public const int KSERROR_GENICAM_SUBNODE_NOT_REACHABLE 
                                                        = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00860000));
  public const int KSERROR_GENICAM_PORT_NOT_REACHABLE   = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00870000));
  public const int KSERROR_GENICAM_FEATURE_NOT_SUPPORTED 
                                                        = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00880000));
  public const int KSERROR_GENICAM_VALUE_TOO_LARGE      = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x00890000));
  public const int KSERROR_GENICAM_VALUE_TOO_SMALL      = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x008a0000));
  public const int KSERROR_GENICAM_VALUE_BAD_INCREMENT  = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x008b0000));
  public const int KSERROR_GENICAM_BAD_REGISTER_DESCRIPTION 
                                                        = unchecked((int)(KSERROR_CATEGORY_CAMERA+0x008c0000));

  //------ GigE Vision<sup>®</sup> registers ------
  public const int KS_CAMERA_REGISTER_STREAM_CHANNELS   = unchecked((int)0x00000904);

  //------ Camera module config ------
  public const int KSCONFIG_OVERRIDE_CAMERA_XML_FILE    = unchecked((int)0x00000001);

  //------ GigE Vision<sup>®</sup> pixel formats ------
  public const int KS_CAMERA_PIXEL_MONO_8               = unchecked((int)0x01080001);
  public const int KS_CAMERA_PIXEL_MONO_8S              = unchecked((int)0x01080002);
  public const int KS_CAMERA_PIXEL_MONO_10              = unchecked((int)0x01100003);
  public const int KS_CAMERA_PIXEL_MONO_10_PACKED       = unchecked((int)0x010c0004);
  public const int KS_CAMERA_PIXEL_MONO_12              = unchecked((int)0x01100005);
  public const int KS_CAMERA_PIXEL_MONO_12_PACKED       = unchecked((int)0x010c0006);
  public const int KS_CAMERA_PIXEL_MONO_16              = unchecked((int)0x01100007);
  public const int KS_CAMERA_PIXEL_BAYER_GR_8           = unchecked((int)0x01080008);
  public const int KS_CAMERA_PIXEL_BAYER_RG_8           = unchecked((int)0x01080009);
  public const int KS_CAMERA_PIXEL_BAYER_GB_8           = unchecked((int)0x0108000a);
  public const int KS_CAMERA_PIXEL_BAYER_BG_8           = unchecked((int)0x0108000b);
  public const int KS_CAMERA_PIXEL_BAYER_GR_10          = unchecked((int)0x0110000c);
  public const int KS_CAMERA_PIXEL_BAYER_RG_10          = unchecked((int)0x0110000d);
  public const int KS_CAMERA_PIXEL_BAYER_GB_10          = unchecked((int)0x0110000e);
  public const int KS_CAMERA_PIXEL_BAYER_BG_10          = unchecked((int)0x0110000f);
  public const int KS_CAMERA_PIXEL_BAYER_GR_12          = unchecked((int)0x01100010);
  public const int KS_CAMERA_PIXEL_BAYER_RG_12          = unchecked((int)0x01100011);
  public const int KS_CAMERA_PIXEL_BAYER_GB_12          = unchecked((int)0x01100012);
  public const int KS_CAMERA_PIXEL_BAYER_BG_12          = unchecked((int)0x01100013);
  public const int KS_CAMERA_PIXEL_RGB_8                = unchecked((int)0x02180014);
  public const int KS_CAMERA_PIXEL_BGR_8                = unchecked((int)0x02180015);
  public const int KS_CAMERA_PIXEL_RGBA_8               = unchecked((int)0x02200016);
  public const int KS_CAMERA_PIXEL_BGRA_8               = unchecked((int)0x02200017);
  public const int KS_CAMERA_PIXEL_RGB_10               = unchecked((int)0x02300018);
  public const int KS_CAMERA_PIXEL_BGR_10               = unchecked((int)0x02300019);
  public const int KS_CAMERA_PIXEL_RGB_12               = unchecked((int)0x0230001a);
  public const int KS_CAMERA_PIXEL_BGR_12               = unchecked((int)0x0230001b);
  public const int KS_CAMERA_PIXEL_RGB_10_V1_PACKED     = unchecked((int)0x0220001c);
  public const int KS_CAMERA_PIXEL_RGB_10_P_32          = unchecked((int)0x0220001d);
  public const int KS_CAMERA_PIXEL_YUV_411_8_UYYVYY     = unchecked((int)0x020c001e);
  public const int KS_CAMERA_PIXEL_YUV_422_8_UYVY       = unchecked((int)0x0210001f);
  public const int KS_CAMERA_PIXEL_YUV_8_UYV            = unchecked((int)0x02180020);
  public const int KS_CAMERA_PIXEL_RGB_8_PLANAR         = unchecked((int)0x02180021);
  public const int KS_CAMERA_PIXEL_RGB_10_PLANAR        = unchecked((int)0x02300022);
  public const int KS_CAMERA_PIXEL_RGB_12_PLANAR        = unchecked((int)0x02300023);
  public const int KS_CAMERA_PIXEL_RGB_16_PLANAR        = unchecked((int)0x02300024);
  public const int KS_CAMERA_PIXEL_MONO_14              = unchecked((int)0x01100025);
  public const int KS_CAMERA_PIXEL_BAYER_GR_10_PACKED   = unchecked((int)0x010c0026);
  public const int KS_CAMERA_PIXEL_BAYER_RG_10_PACKED   = unchecked((int)0x010c0027);
  public const int KS_CAMERA_PIXEL_BAYER_GB_10_PACKED   = unchecked((int)0x010c0028);
  public const int KS_CAMERA_PIXEL_BAYER_BG_10_PACKED   = unchecked((int)0x010c0029);
  public const int KS_CAMERA_PIXEL_BAYER_GR_12_PACKED   = unchecked((int)0x010c002a);
  public const int KS_CAMERA_PIXEL_BAYER_RG_12_PACKED   = unchecked((int)0x010c002b);
  public const int KS_CAMERA_PIXEL_BAYER_GB_12_PACKED   = unchecked((int)0x010c002c);
  public const int KS_CAMERA_PIXEL_BAYER_BG_12_PACKED   = unchecked((int)0x010c002d);
  public const int KS_CAMERA_PIXEL_BAYER_GR_16          = unchecked((int)0x0110002e);
  public const int KS_CAMERA_PIXEL_BAYER_RG_16          = unchecked((int)0x0110002f);
  public const int KS_CAMERA_PIXEL_BAYER_GB_16          = unchecked((int)0x01100030);
  public const int KS_CAMERA_PIXEL_BAYER_BG_16          = unchecked((int)0x01100031);
  public const int KS_CAMERA_PIXEL_YUV_422_8            = unchecked((int)0x020c0032);
  public const int KS_CAMERA_PIXEL_RGB_16               = unchecked((int)0x02300033);
  public const int KS_CAMERA_PIXEL_RGB_12_V1_PACKED     = unchecked((int)0x02240034);
  public const int KS_CAMERA_PIXEL_RGB_565_P            = unchecked((int)0x02100035);
  public const int KS_CAMERA_PIXEL_BGR_565_P            = unchecked((int)0x02100036);
  public const int KS_CAMERA_PIXEL_MONO_1P              = unchecked((int)0x01010037);
  public const int KS_CAMERA_PIXEL_MONO_2P              = unchecked((int)0x01020038);
  public const int KS_CAMERA_PIXEL_MONO_4P              = unchecked((int)0x01040039);
  public const int KS_CAMERA_PIXEL_YCBCR_8_CBYCR        = unchecked((int)0x0218003a);
  public const int KS_CAMERA_PIXEL_YCBCR_422_8          = unchecked((int)0x0210003b);
  public const int KS_CAMERA_PIXEL_YCBCR_411_8_CBYYCRYY = unchecked((int)0x020c003c);
  public const int KS_CAMERA_PIXEL_YCBCR_601_8_CBYCR    = unchecked((int)0x0218003d);
  public const int KS_CAMERA_PIXEL_YCBCR_601_422_8      = unchecked((int)0x0210003e);
  public const int KS_CAMERA_PIXEL_YCBCR_601_411_8_CBYYCRYY 
                                                        = unchecked((int)0x020c003f);
  public const int KS_CAMERA_PIXEL_YCBCR_709_8_CBYCR    = unchecked((int)0x02180040);
  public const int KS_CAMERA_PIXEL_YCBCR_709_422_8      = unchecked((int)0x02100041);
  public const int KS_CAMERA_PIXEL_YCBCR_709_411_8_CBYYCRYY 
                                                        = unchecked((int)0x020c0042);
  public const int KS_CAMERA_PIXEL_YCBCR_422_8_CBYCRY   = unchecked((int)0x02100043);
  public const int KS_CAMERA_PIXEL_YCBCR_601_422_8_CBYCRY 
                                                        = unchecked((int)0x02100044);
  public const int KS_CAMERA_PIXEL_YCBCR_709_422_8_CBYCRY 
                                                        = unchecked((int)0x02100045);

  //------ Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraRecvImageContext {
    public int ctxType;
    public Handle hStream;
    public Handle hCamera;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraDroppedImageContext {
    public int ctxType;
    public Handle hStream;
    public Handle hCamera;
    public ulong imageId;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraErrorContext {
    public int ctxType;
    public Handle hCamera;
    public int error;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraAttachContext {
    public int ctxType;
    public Handle hController;
    public KSCameraInfo cameraInfo;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraGevAttachContext {
    public int ctxType;
    public Handle hAdapter;
    public KSCameraInfoGev cameraInfo;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSCameraEventContext {
    public int ctxType;
    public Handle hCamera;
  };

  //------ Basic functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumCameras(
    Handle hObject, int index, ref KSCameraInfo pCameraInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execCameraCommand(
    Handle hObject, int command, uint address, void* pData, int* pLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installCameraHandler(
    Handle hHandle, int eventType, Handle hSignal, int flags);

  //------ Camera connection functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openCamera(
    Handle* phCamera, Handle hObject, string pHardwareId, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_openCameraEx(
    Handle* phCamera, Handle hObject, ref KSCameraInfo pCameraInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeCamera(
    Handle hCamera, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_readCameraMem(
    Handle hCamera, ref long address, void* pData, int length, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_writeCameraMem(
    Handle hCamera, ref long address, void* pData, int length, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_configCamera(
    Handle hCamera, int configCommand, string pName, int index, void* pData, int length, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_startCameraAcquisition(
    Handle hCamera, int acquisitionMode, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_stopCameraAcquisition(
    Handle hCamera, int flags);

  //------ Stream functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createCameraStream(
    Handle* phStream, Handle hCamera, int channelNumber, int numBuffers, int bufferSize, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closeCameraStream(
    Handle hStream, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_recvCameraImage(
    Handle hStream, void** ppData, ref KSCameraBlock* ppBlockInfo, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_releaseCameraImage(
    Handle hStream, void* pData, int flags);

  //------------------------------------------------------------------------------------------------------------
  // PLC Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error codes ------
  public const int KSERROR_PLC_SCAN_ERROR               = unchecked((int)(KSERROR_CATEGORY_PLC+0x00000000));
  public const int KSERROR_PLC_TYPE_ERROR               = unchecked((int)(KSERROR_CATEGORY_PLC+0x00010000));
  public const int KSERROR_PLC_BACKEND                  = unchecked((int)(KSERROR_CATEGORY_PLC+0x00020000));
  public const int KSERROR_PLC_LINKER_ERROR             = unchecked((int)(KSERROR_CATEGORY_PLC+0x00030000));
  public const int KSERROR_PLC_INVALID_CONFIG_NUMBER    = unchecked((int)(KSERROR_CATEGORY_PLC+0x00040000));
  public const int KSERROR_PLC_INVALID_RESOURCE_NUMBER  = unchecked((int)(KSERROR_CATEGORY_PLC+0x00060000));
  public const int KSERROR_PLC_INVALID_TASK_NUMBER      = unchecked((int)(KSERROR_CATEGORY_PLC+0x00070000));

  //------ PLC languages ------
  public const int KS_PLC_BYTECODE                      = unchecked((int)0x00000000);
  public const int KS_PLC_INSTRUCTION_LIST              = unchecked((int)0x00000001);
  public const int KS_PLC_STRUCTURED_TEXT               = unchecked((int)0x00000002);

  //------ Flags ------
  public const int KSF_USE_KITHARA_PRIORITY             = unchecked((int)0x00002000);
  public const int KSF_32BIT                            = unchecked((int)0x00000400);
  public const int KSF_64BIT                            = unchecked((int)0x00000800);
  public const int KSF_DEBUG                            = unchecked((int)0x10000000);
  public const int KSF_PLC_START                        = unchecked((int)0x00004000);
  public const int KSF_PLC_STOP                         = unchecked((int)0x00008000);
  public const int KSF_IGNORE_CASE                      = unchecked((int)0x00000010);

  //------ PLC commands ------
  public const int KS_PLC_ABORT_COMPILE                 = unchecked((int)0x00000001);

  //------ Error levels ------
  public const int KS_ERROR_LEVEL_IGNORE                = unchecked((int)0x00000000);
  public const int KS_ERROR_LEVEL_WARNING               = unchecked((int)0x00000001);
  public const int KS_ERROR_LEVEL_ERROR                 = unchecked((int)0x00000002);
  public const int KS_ERROR_LEVEL_FATAL                 = unchecked((int)0x00000003);

  //------ PLC compiler errors ------
  public const int KS_PLCERROR_NONE                     = unchecked((int)0x00000000);
  public const int KS_PLCERROR_SYNTAX                   = unchecked((int)0x00000001);
  public const int KS_PLCERROR_EXPECTED_ID              = unchecked((int)0x00000002);
  public const int KS_PLCERROR_EXPECTED_RESCOURCE_ID    = unchecked((int)0x00000003);
  public const int KS_PLCERROR_EXPECTED_PROG_ID         = unchecked((int)0x00000004);
  public const int KS_PLCERROR_EXPECTED_FB_ID           = unchecked((int)0x00000005);
  public const int KS_PLCERROR_EXPECTED_FB_INSTANCE     = unchecked((int)0x00000006);
  public const int KS_PLCERROR_EXPECTED_FUNCTION_ID     = unchecked((int)0x00000007);
  public const int KS_PLCERROR_EXPECTED_SINGLE_ID       = unchecked((int)0x00000008);
  public const int KS_PLCERROR_EXPECTED_INTEGER         = unchecked((int)0x00000009);
  public const int KS_PLCERROR_EXPECTED_STRUCT          = unchecked((int)0x0000000a);
  public const int KS_PLCERROR_EXPECTED_STMT            = unchecked((int)0x0000000b);
  public const int KS_PLCERROR_EXPECTED_OPERAND         = unchecked((int)0x0000000c);
  public const int KS_PLCERROR_EXPECTED_VAR             = unchecked((int)0x0000000d);
  public const int KS_PLCERROR_EXPECTED_TYPE            = unchecked((int)0x0000000e);
  public const int KS_PLCERROR_EXPECTED_STRING          = unchecked((int)0x0000000f);
  public const int KS_PLCERROR_EXPECTED_VALUE           = unchecked((int)0x00000010);
  public const int KS_PLCERROR_EXPECTED_NUMERAL         = unchecked((int)0x00000011);
  public const int KS_PLCERROR_EXPECTED_BOOL            = unchecked((int)0x00000012);
  public const int KS_PLCERROR_EXPECTED_CONST           = unchecked((int)0x00000013);
  public const int KS_PLCERROR_EXPECTED_READ_ONLY       = unchecked((int)0x00000014);
  public const int KS_PLCERROR_EXPECTED_VAR_INPUT       = unchecked((int)0x00000015);
  public const int KS_PLCERROR_EXPECTED_VAR_OUTPUT      = unchecked((int)0x00000016);
  public const int KS_PLCERROR_EXPECTED_SIMPLE_TYPE     = unchecked((int)0x00000017);
  public const int KS_PLCERROR_EXPECTED_TIME            = unchecked((int)0x00000018);
  public const int KS_PLCERROR_EXPECTED_GLOBAL          = unchecked((int)0x00000019);
  public const int KS_PLCERROR_EXPECTED_LABEL           = unchecked((int)0x0000001a);
  public const int KS_PLCERROR_EXPECTED_INCOMPLETE_DECL = unchecked((int)0x0000001b);
  public const int KS_PLCERROR_EXPECTED_VAR_CONFIG      = unchecked((int)0x0000001c);
  public const int KS_PLCERROR_ID_DEFINED               = unchecked((int)0x0000001d);
  public const int KS_PLCERROR_ID_UNKNOWN               = unchecked((int)0x0000001e);
  public const int KS_PLCERROR_ID_CONST                 = unchecked((int)0x0000001f);
  public const int KS_PLCERROR_ID_TOO_MANY              = unchecked((int)0x00000020);
  public const int KS_PLCERROR_UNKNOWN_LOCATION         = unchecked((int)0x00000021);
  public const int KS_PLCERROR_UNKNOWN_SIZE             = unchecked((int)0x00000022);
  public const int KS_PLCERROR_TYPE_MISMATCH            = unchecked((int)0x00000023);
  public const int KS_PLCERROR_INDEX_VIOLATION          = unchecked((int)0x00000024);
  public const int KS_PLCERROR_FORMAT_MISMATCH          = unchecked((int)0x00000025);
  public const int KS_PLCERROR_OVERFLOW                 = unchecked((int)0x00000026);
  public const int KS_PLCERROR_DATA_LOSS                = unchecked((int)0x00000027);
  public const int KS_PLCERROR_NOT_SUPPORTED            = unchecked((int)0x00000028);
  public const int KS_PLCERROR_NEGATIVE_SIZE            = unchecked((int)0x00000029);
  public const int KS_PLCERROR_MISSING_ARRAY_ELEM_TYPE  = unchecked((int)0x0000002a);
  public const int KS_PLCERROR_MISSING_RETURN_VALUE     = unchecked((int)0x0000002b);
  public const int KS_PLCERROR_BAD_VAR_USE              = unchecked((int)0x0000002c);
  public const int KS_PLCERROR_BAD_DIRECT_VAR_INOUT     = unchecked((int)0x0000002d);
  public const int KS_PLCERROR_BAD_LABEL                = unchecked((int)0x0000002e);
  public const int KS_PLCERROR_BAD_SUBRANGE             = unchecked((int)0x0000002f);
  public const int KS_PLCERROR_BAD_DATETIME             = unchecked((int)0x00000030);
  public const int KS_PLCERROR_BAD_ARRAY_SIZE           = unchecked((int)0x00000031);
  public const int KS_PLCERROR_BAD_CASE                 = unchecked((int)0x00000032);
  public const int KS_PLCERROR_DIV_BY_ZERO              = unchecked((int)0x00000033);
  public const int KS_PLCERROR_AMBIGUOUS                = unchecked((int)0x00000034);
  public const int KS_PLCERROR_INTERNAL                 = unchecked((int)0x00000035);
  public const int KS_PLCERROR_PARAMETER_MISMATCH       = unchecked((int)0x00000036);
  public const int KS_PLCERROR_PARAMETER_TOO_MANY       = unchecked((int)0x00000037);
  public const int KS_PLCERROR_PARAMETER_FEW            = unchecked((int)0x00000038);
  public const int KS_PLCERROR_INIT_MANY                = unchecked((int)0x00000039);
  public const int KS_PLCERROR_BAD_ACCESS               = unchecked((int)0x0000003a);
  public const int KS_PLCERROR_BAD_CONFIG               = unchecked((int)0x0000003b);
  public const int KS_PLCERROR_READ_ONLY                = unchecked((int)0x0000003c);
  public const int KS_PLCERROR_WRITE_ONLY               = unchecked((int)0x0000003d);
  public const int KS_PLCERROR_BAD_EXIT                 = unchecked((int)0x0000003e);
  public const int KS_PLCERROR_BAD_RECURSION            = unchecked((int)0x0000003f);
  public const int KS_PLCERROR_BAD_PRIORITY             = unchecked((int)0x00000040);
  public const int KS_PLCERROR_BAD_FB_OPERATOR          = unchecked((int)0x00000041);
  public const int KS_PLCERROR_BAD_INIT                 = unchecked((int)0x00000042);
  public const int KS_PLCERROR_BAD_CONTINUE             = unchecked((int)0x00000043);
  public const int KS_PLCERROR_DUPLICATE_CASE           = unchecked((int)0x00000044);
  public const int KS_PLCERROR_UNEXPECTED_TYPE          = unchecked((int)0x00000045);
  public const int KS_PLCERROR_CASE_MISSING             = unchecked((int)0x00000046);
  public const int KS_PLCERROR_DEAD_INSTRUCTION         = unchecked((int)0x00000047);
  public const int KS_PLCERROR_FOLD_EXPRESSION          = unchecked((int)0x00000048);
  public const int KS_PLCERROR_NOT_IMPLEMENTED          = unchecked((int)0x00000049);
  public const int KS_PLCERROR_BAD_CONNECTED_PARAM_VAR  = unchecked((int)0x0000004a);
  public const int KS_PLCERROR_PARAMETER_MIX            = unchecked((int)0x0000004b);
  public const int KS_PLCERROR_EXPECTED_PROGRAM_DECL    = unchecked((int)0x0000004c);
  public const int KS_PLCERROR_EXPECTED_INTERFACE       = unchecked((int)0x0000004d);
  public const int KS_PLCERROR_EXPECTED_METHOD          = unchecked((int)0x0000004e);
  public const int KS_PLCERROR_ABSTRACT                 = unchecked((int)0x0000004f);
  public const int KS_PLCERROR_FINAL                    = unchecked((int)0x00000050);
  public const int KS_PLCERROR_MISSING_ABSTRACT_METHOD  = unchecked((int)0x00000051);
  public const int KS_PLCERROR_ABSTRACT_INCOMPLETE      = unchecked((int)0x00000052);
  public const int KS_PLCERROR_BAD_ACCESS_MODIFIER      = unchecked((int)0x00000053);
  public const int KS_PLCERROR_BAD_METHOD_ACCESS        = unchecked((int)0x00000054);
  public const int KS_PLCERROR_ID_INPUT                 = unchecked((int)0x00000055);
  public const int KS_PLCERROR_BAD_THIS                 = unchecked((int)0x00000056);
  public const int KS_PLCERROR_MISSING_MODULE           = unchecked((int)0x00000057);
  public const int KS_PLCERROR_EXPECTED_DNV             = unchecked((int)0x00000058);
  public const int KS_PLCERROR_EXPECTED_DIRECT_VAR      = unchecked((int)0x00000059);
  public const int KS_PLCERROR_BAD_VAR_DECL             = unchecked((int)0x0000005a);
  public const int KS_PLCERROR_HAS_ABSTRACT_BODY        = unchecked((int)0x0000005b);
  public const int KS_PLCERROR_BAD_MEMBER_ACCESS        = unchecked((int)0x0000005c);
  public const int KS_PLCERROR_EXPECTED_FORMAL_CALL     = unchecked((int)0x0000005d);
  public const int KS_PLCERROR_EXPECTED_INTERFACE_VAR   = unchecked((int)0x0000005e);

  //------ PLC runtime errors ------
  public const int KS_PLCERROR_DIVIDED_BY_ZERO          = unchecked((int)0x0000012c);
  public const int KS_PLCERROR_ARRAY_INDEX_VIOLATION    = unchecked((int)0x0000012d);
  public const int KS_PLCERROR_MEMORY_ACCESS_VIOLATION  = unchecked((int)0x0000012e);
  public const int KS_PLCERROR_IO_VARIABLE_ACCESS       = unchecked((int)0x0000012f);
  public const int KS_PLCERROR_BAD_VAR_EXTERNAL         = unchecked((int)0x00000130);
  public const int KS_PLCERROR_BAD_SUB_RANGE            = unchecked((int)0x00000131);

  //------ PLC events ------
  public const int KS_PLC_COMPILE_ERROR                 = unchecked((int)(PLC_BASE+0x00000001));
  public const int KS_PLC_COMPILE_FINISHED              = unchecked((int)(PLC_BASE+0x00000002));
  public const int KS_PLC_RUNTIME_ERROR                 = unchecked((int)(PLC_BASE+0x00000003));
  public const int KS_PLC_CONFIG_RESSOURCE              = unchecked((int)(PLC_BASE+0x00000004));

  //------ Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PlcErrorMsgContext {
    public int ctxType;
    public Handle hCompiler;
    public int line;
    public int column;
    public int errorCode;
    public int errorLevel;
    public int flags;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 200)]
    public byte[] pMsg;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PlcCompileContext {
    public int ctxType;
    public Handle hCompiler;
    public int ksError;
    public int id;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PlcRuntimeErrorContext {
    public int ctxType;
    public Handle hPlc;
    public int line;
    public int column;
    public int errorCode;
    public int errorLevel;
    public int flags;
    public int ksError;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct PlcConfigContext {
    public int ctxType;
    public Handle hPlc;
    public int configIndex;
    public int resourceIndex;
    public int flags;
  };

  //------ PLC compiler functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createPlcCompiler(
    Handle* phCompiler, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closePlcCompiler(
    Handle hCompiler, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_compilePlcFile(
    Handle hCompiler, string sourcePath, int sourceLanguage, string destinationPath, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_compilePlcBuffer(
    Handle hCompiler, void* pSourceBuffer, int sourceLength, int sourceLang, void* pDestBuffer, int* pDestLength, int flags);

  //------ PLC functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createPlcFromFile(
    Handle* phPlc, string binaryPath, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createPlcFromBuffer(
    Handle* phPlc, void* pBinaryBuffer, int bufferLength, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_closePlc(
    Handle hPlc, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_startPlc(
    Handle hPlc, int configIndex, ref long start, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_stopPlc(
    Handle hPlc, int flags);

  //------ Information functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumPlcConfigurations(
    Handle hPlc, int configIndex, byte* pNameBuf, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumPlcResources(
    Handle hPlc, int configIndex, int resourceIndex, byte* pNameBuf, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_enumPlcTasks(
    Handle hPlc, int configIndex, int resourceIndex, int taskIndex, byte* pNameBuf, int flags);

  //------ Generic functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_execPlcCommand(
    Handle hPlc, int command, void* pData, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installPlcHandler(
    Handle hPlc, int eventCode, Handle hSignal, int flags);

  //------------------------------------------------------------------------------------------------------------
  // Vision Module
  //------------------------------------------------------------------------------------------------------------

  //------ Basic functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_loadVisionKernel(
    Handle* phKernel, string dllName, string initProcName, void* pArgs, int flags);

  //------ Control functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_installVisionHandler(
    int eventType, Handle hSignal, int flags);

  //------ Context structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSVisionTaskContext {
    public int ctxType;
    public Handle hTask;
  };

  //------ Handler events ------
  public const int KS_VISION_TASK_CREATED               = unchecked((int)0x00000001);
  public const int KS_VISION_TASK_CLOSED                = unchecked((int)0x00000002);

  //------------------------------------------------------------------------------------------------------------
  // SigProc Module
  //------------------------------------------------------------------------------------------------------------

  //------ Filter types ------
  public const int KS_FIR_LOWPASS_HAMMING               = unchecked((int)0x00000000);
  public const int KS_FIR_LOWPASS_HANNING               = unchecked((int)0x00000001);
  public const int KS_FIR_LOWPASS_RECTANGULAR           = unchecked((int)0x00000002);
  public const int KS_FIR_HIGHPASS_HAMMING              = unchecked((int)0x00000003);
  public const int KS_FIR_HIGHPASS_HANNING              = unchecked((int)0x00000004);
  public const int KS_FIR_HIGHPASS_RECTANGULAR          = unchecked((int)0x00000005);
  public const int KS_FIR_BANDPASS_HAMMING              = unchecked((int)0x00000006);
  public const int KS_FIR_BANDPASS_HANNING              = unchecked((int)0x00000007);
  public const int KS_FIR_BANDPASS_RECTANGULAR          = unchecked((int)0x00000008);
  public const int KS_FIR_BANDSTOP_HAMMING              = unchecked((int)0x00000009);
  public const int KS_FIR_BANDSTOP_HANNING              = unchecked((int)0x0000000a);
  public const int KS_FIR_BANDSTOP_RECTANGULAR          = unchecked((int)0x0000000b);
  public const int KS_IIR_LOWPASS_CHEBYSHEV_I           = unchecked((int)0x00000014);
  public const int KS_IIR_LOWPASS_BUTTERWORTH           = unchecked((int)0x00000015);
  public const int KS_IIR_LOWPASS_CHEBYSHEV_II          = unchecked((int)0x00000016);
  public const int KS_IIR_HIGHPASS_CHEBYSHEV_I          = unchecked((int)0x00000017);
  public const int KS_IIR_HIGHPASS_BUTTERWORTH          = unchecked((int)0x00000018);
  public const int KS_IIR_HIGHPASS_CHEBYSHEV_II         = unchecked((int)0x00000019);
  public const int KS_IIR_BANDPASS_CHEBYSHEV_I          = unchecked((int)0x0000001a);
  public const int KS_IIR_BANDPASS_BUTTERWORTH          = unchecked((int)0x0000001b);
  public const int KS_IIR_BANDPASS_CHEBYSHEV_II         = unchecked((int)0x0000001c);
  public const int KS_IIR_BANDSTOP_CHEBYSHEV_I          = unchecked((int)0x0000001d);
  public const int KS_IIR_BANDSTOP_BUTTERWORTH          = unchecked((int)0x0000001e);
  public const int KS_IIR_BANDSTOP_CHEBYSHEV_II         = unchecked((int)0x0000001f);
  public const int KS_IIR_INDIVIDUAL                    = unchecked((int)0x00000020);

  //------ Standard ripples ------
  public const double KS_CHEBYSHEV_3DB                  = unchecked((int)unchecked((double)0.707106781));
  public const double KS_CHEBYSHEV_1DB                  = unchecked((int)unchecked((double)0.891250938));

  //------ Some filter specified errorcodes ------
  public const int KSERROR_SP_SHANNON_THEOREM_MISMATCH  = unchecked((int)(KSERROR_CATEGORY_SPECIAL+0x00020000));

  //------ Paramters structures ------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSSignalFilterParams {
    public int structSize;
    public int filterType;
    public int order;
    public double maxFrequency;
    public double frequency;
    public double sampleRate;
    public double width;
    public double delta;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSIndividualSignalFilterParams {
    public int structSize;
    public double pXCoeffs;
    public int xOrder;
    public double pYCoeffs;
    public int yOrder;
    public double maxFrequency;
    public double sampleRate;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public unsafe struct KSPIDControllerParams {
    public int structSize;
    public double proportional;
    public double integral;
    public double derivative;
    public double setpoint;
    public double sampleRate;
  };

  //------ Digital Filter functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createSignalFilter(
    Handle* phFilter, ref KSSignalFilterParams pParams, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createIndividualSignalFilter(
    Handle* phFilter, ref KSIndividualSignalFilterParams pParams, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_getFrequencyResponse(
    Handle hFilter, double frequency, double pAmplitude, double pPhase, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_scaleSignalFilter(
    Handle hFilter, double frequency, double value, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_resetSignalFilter(
    Handle hFilter, int flags);

  //------ PID Controller functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_createPIDController(
    Handle* phController, ref KSPIDControllerParams pParams, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_changeSetpoint(
    Handle hController, double setpoint, int flags);

  //------ General signal processing functions ------
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_removeSignalProcessor(
    Handle hSigProc, int flags);
  [DllImport("KrtsDemo.dll", CallingConvention = CallingConvention.StdCall)]
  public static extern unsafe int KS_processSignal(
    Handle hSigProc, double input, double pOutput, int flags);

}
