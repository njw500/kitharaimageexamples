// Copyright (c) 1998-2016 by Kithara Software GmbH. All rights reserved.

//##############################################################################################################
//
// File:             KrtsDemo.java (v10.00d)
//
// Description:      Java API for Kithara �RealTime Suite�
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// a.lun 1998-08-25  CREAT: Original - (file created)
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:01
//
//##############################################################################################################

//--------------------------------------------------------------------------------------------------------------
// KrtsDemo
//--------------------------------------------------------------------------------------------------------------

public class KrtsDemo {
  static {
    System.loadLibrary("KrtsDemo");
    KS_initModule("KrtsDemo");
  }

  private static native void KS_initModule(String module);

  //----------------------------------------------------------------------------
  // Class definitions for use with the Kithara Toolkits
  //----------------------------------------------------------------------------

  public static class IntPtr {
    public IntPtr()                             {}
    public int value()                          { return value_; }
    private int value_ = 0;
  }

  public static class Handle extends IntPtr {

  }

  public static class LongPtr {
    public LongPtr()                            {}
    public long value()                         { return value_; }
    private long value_;
  }

  public static class StrPtr {
    public StrPtr()                             {}
    public String value()                       { return value_; }
    private String value_;
  }

  //------------------------------------------------------------------------------------------------------------
  // Base Module
  //------------------------------------------------------------------------------------------------------------

  //------ Common Constants ------
  public static final int KS_INVALID_HANDLE                     = 0x00000000;

  //------ Error Categories ------
  // Operating system errors:
  public static final int KSERROR_CATEGORY_OPERATING_SYSTEM     = 0x00000000;

  // Categories 0x01000000 ... 0x0f000000 are free for customer usage!
  public static final int KSERROR_CATEGORY_USER                 = 0x0f000000;
  public static final int KSERROR_CATEGORY_BASE                 = 0x10000000;
  public static final int KSERROR_CATEGORY_DEAL                 = 0x11000000;
  public static final int KSERROR_CATEGORY_HANDLING             = 0x17000000;
  public static final int KSERROR_CATEGORY_IOPORT               = 0x14000000;
  public static final int KSERROR_CATEGORY_MEMORY               = 0x12000000;
  public static final int KSERROR_CATEGORY_KERNEL               = 0x18000000;
  public static final int KSERROR_CATEGORY_KEYBOARD             = 0x1a000000;
  public static final int KSERROR_CATEGORY_INTERRUPT            = 0x13000000;
  public static final int KSERROR_CATEGORY_TIMER                = 0x16000000;
  public static final int KSERROR_CATEGORY_USB                  = 0x19000000;
  public static final int KSERROR_CATEGORY_IDT                  = 0x1b000000;
  public static final int KSERROR_CATEGORY_NODE                 = 0x1c000000;
  public static final int KSERROR_CATEGORY_SOCKET               = 0x1d000000;
  public static final int KSERROR_CATEGORY_SYSTEM               = 0x1e000000;
  public static final int KSERROR_CATEGORY_ETHERCAT             = 0x1f000000;
  public static final int KSERROR_CATEGORY_MF                   = 0x20000000;
  public static final int KSERROR_CATEGORY_CAN                  = 0x21000000;
  public static final int KSERROR_CATEGORY_PROFIBUS             = 0x22000000;
  public static final int KSERROR_CATEGORY_PLC                  = 0x23000000;
  public static final int KSERROR_CATEGORY_CANOPEN              = 0x24000000;
  public static final int KSERROR_CATEGORY_FLEXRAY              = 0x25000000;
  public static final int KSERROR_CATEGORY_PACKET               = 0x26000000;
  public static final int KSERROR_CATEGORY_CAMERA               = 0x27000000;
  public static final int KSERROR_CATEGORY_TASK                 = 0x28000000;
  public static final int KSERROR_CATEGORY_SPECIAL              = 0x29000000;
  public static final int KSERROR_CATEGORY_XHCI                 = 0x2a000000;
  public static final int KSERROR_CATEGORY_DISK                 = 0x2b000000;
  public static final int KSERROR_CATEGORY_FILE                 = 0x2c000000;
  public static final int KSERROR_CATEGORY_NEXT                 = 0x2d000000;
  public static final int KSERROR_CATEGORY_EXCEPTION            = 0x3f000000;

  //------ Error Codes ------
  public static final int KSERROR_OPERATING_SYSTEM              = (KSERROR_CATEGORY_BASE+0x00000000);
  public static final int KSERROR_UNKNOWN_ERROR_CODE            = (KSERROR_CATEGORY_BASE+0x00010000);
  public static final int KSERROR_UNKNOWN_ERROR_CATEGORY        = (KSERROR_CATEGORY_BASE+0x00020000);
  public static final int KSERROR_UNKNOWN_ERROR_LANGUAGE        = (KSERROR_CATEGORY_BASE+0x00030000);
  public static final int KSERROR_CANNOT_FIND_LIBRARY           = (KSERROR_CATEGORY_BASE+0x00040000);
  public static final int KSERROR_CANNOT_FIND_ADDRESS           = (KSERROR_CATEGORY_BASE+0x00050000);
  public static final int KSERROR_BAD_PARAM                     = (KSERROR_CATEGORY_BASE+0x00060000);
  public static final int KSERROR_BAD_VERSION                   = (KSERROR_CATEGORY_BASE+0x00070000);
  public static final int KSERROR_INTERNAL                      = (KSERROR_CATEGORY_BASE+0x00080000);
  public static final int KSERROR_UNKNOWN                       = (KSERROR_CATEGORY_BASE+0x00090000);
  public static final int KSERROR_FUNCTION_NOT_AVAILABLE        = (KSERROR_CATEGORY_BASE+0x000a0000);
  public static final int KSERROR_DRIVER_NOT_OPENED             = (KSERROR_CATEGORY_BASE+0x000b0000);
  public static final int KSERROR_NOT_ENOUGH_MEMORY             = (KSERROR_CATEGORY_BASE+0x000c0000);
  public static final int KSERROR_CANNOT_OPEN_KERNEL            = (KSERROR_CATEGORY_BASE+0x000d0000);
  public static final int KSERROR_CANNOT_CLOSE_KERNEL           = (KSERROR_CATEGORY_BASE+0x000e0000);
  public static final int KSERROR_BAD_KERNEL_VERSION            = (KSERROR_CATEGORY_BASE+0x000f0000);
  public static final int KSERROR_CANNOT_FIND_KERNEL            = (KSERROR_CATEGORY_BASE+0x00100000);
  public static final int KSERROR_INVALID_HANDLE                = (KSERROR_CATEGORY_BASE+0x00110000);
  public static final int KSERROR_DEVICE_IO_FAILED              = (KSERROR_CATEGORY_BASE+0x00120000);
  public static final int KSERROR_REGISTRY_FAILURE              = (KSERROR_CATEGORY_BASE+0x00130000);
  public static final int KSERROR_CANNOT_START_KERNEL           = (KSERROR_CATEGORY_BASE+0x00140000);
  public static final int KSERROR_CANNOT_STOP_KERNEL            = (KSERROR_CATEGORY_BASE+0x00150000);
  public static final int KSERROR_ACCESS_DENIED                 = (KSERROR_CATEGORY_BASE+0x00160000);
  public static final int KSERROR_DEVICE_NOT_OPENED             = (KSERROR_CATEGORY_BASE+0x00170000);
  public static final int KSERROR_FUNCTION_DENIED               = (KSERROR_CATEGORY_BASE+0x00180000);
  public static final int KSERROR_DEVICE_ALREADY_USED           = (KSERROR_CATEGORY_BASE+0x00190000);
  public static final int KSERROR_OUT_OF_MEMORY                 = (KSERROR_CATEGORY_BASE+0x001a0000);
  public static final int KSERROR_TOO_MANY_OPEN_PROCESSES       = (KSERROR_CATEGORY_BASE+0x001b0000);
  public static final int KSERROR_SIGNAL_OVERFLOW               = (KSERROR_CATEGORY_BASE+0x001c0000);
  public static final int KSERROR_CANNOT_SIGNAL                 = (KSERROR_CATEGORY_BASE+0x001d0000);
  public static final int KSERROR_FILE_NOT_FOUND                = (KSERROR_CATEGORY_BASE+0x001e0000);
  public static final int KSERROR_DEVICE_NOT_FOUND              = (KSERROR_CATEGORY_BASE+0x001f0000);
  public static final int KSERROR_ASSERTION_FAILED              = (KSERROR_CATEGORY_BASE+0x00200000);
  public static final int KSERROR_FUNCTION_FAILED               = (KSERROR_CATEGORY_BASE+0x00210000);
  public static final int KSERROR_OPERATION_ABORTED             = (KSERROR_CATEGORY_BASE+0x00220000);
  public static final int KSERROR_TIMEOUT                       = (KSERROR_CATEGORY_BASE+0x00230000);
  public static final int KSERROR_REBOOT_NECESSARY              = (KSERROR_CATEGORY_BASE+0x00240000);
  public static final int KSERROR_DEVICE_DISABLED               = (KSERROR_CATEGORY_BASE+0x00250000);
  public static final int KSERROR_ATTACH_FAILED                 = (KSERROR_CATEGORY_BASE+0x00260000);
  public static final int KSERROR_DEVICE_REMOVED                = (KSERROR_CATEGORY_BASE+0x00270000);
  public static final int KSERROR_TYPE_MISMATCH                 = (KSERROR_CATEGORY_BASE+0x00280000);
  public static final int KSERROR_FUNCTION_NOT_IMPLEMENTED      = (KSERROR_CATEGORY_BASE+0x00290000);
  public static final int KSERROR_COMMUNICATION_BROKEN          = (KSERROR_CATEGORY_BASE+0x002a0000);
  public static final int KSERROR_DEVICE_NOT_READY              = (KSERROR_CATEGORY_BASE+0x00370000);
  public static final int KSERROR_NO_RESPONSE                   = (KSERROR_CATEGORY_BASE+0x00380000);
  public static final int KSERROR_OPERATION_PENDING             = (KSERROR_CATEGORY_BASE+0x00390000);
  public static final int KSERROR_PORT_BUSY                     = (KSERROR_CATEGORY_BASE+0x003a0000);
  public static final int KSERROR_UNKNOWN_SYSTEM                = (KSERROR_CATEGORY_BASE+0x003b0000);
  public static final int KSERROR_BAD_CONTEXT                   = (KSERROR_CATEGORY_BASE+0x003c0000);
  public static final int KSERROR_END_OF_FILE                   = (KSERROR_CATEGORY_BASE+0x003d0000);
  public static final int KSERROR_INTERRUPT_ACTIVATION_FAILED   = (KSERROR_CATEGORY_BASE+0x003e0000);
  public static final int KSERROR_INTERRUPT_IS_SHARED           = (KSERROR_CATEGORY_BASE+0x003f0000);
  public static final int KSERROR_NO_REALTIME_ACCESS            = (KSERROR_CATEGORY_BASE+0x00400000);
  public static final int KSERROR_HARDWARE_NOT_SUPPORTED        = (KSERROR_CATEGORY_BASE+0x00410000);
  public static final int KSERROR_TIMER_ACTIVATION_FAILED       = (KSERROR_CATEGORY_BASE+0x00420000);
  public static final int KSERROR_SOCKET_FAILURE                = (KSERROR_CATEGORY_BASE+0x00430000);
  public static final int KSERROR_LINE_ERROR                    = (KSERROR_CATEGORY_BASE+0x00440000);
  public static final int KSERROR_STACK_CORRUPTION              = (KSERROR_CATEGORY_BASE+0x00450000);
  public static final int KSERROR_INVALID_ARGUMENT              = (KSERROR_CATEGORY_BASE+0x00460000);
  public static final int KSERROR_PARSE_ERROR                   = (KSERROR_CATEGORY_BASE+0x00470000);
  public static final int KSERROR_INVALID_IDENTIFIER            = (KSERROR_CATEGORY_BASE+0x00480000);
  public static final int KSERROR_REALTIME_ALREADY_USED         = (KSERROR_CATEGORY_BASE+0x00490000);
  public static final int KSERROR_COMMUNICATION_FAILED          = (KSERROR_CATEGORY_BASE+0x004a0000);
  public static final int KSERROR_INVALID_SIZE                  = (KSERROR_CATEGORY_BASE+0x004b0000);
  public static final int KSERROR_OBJECT_NOT_FOUND              = (KSERROR_CATEGORY_BASE+0x004c0000);

  //------ Error codes of handling category ------
  public static final int KSERROR_CANNOT_CREATE_EVENT           = (KSERROR_CATEGORY_HANDLING+0x00000000);
  public static final int KSERROR_CANNOT_CREATE_THREAD          = (KSERROR_CATEGORY_HANDLING+0x00010000);
  public static final int KSERROR_CANNOT_BLOCK_THREAD           = (KSERROR_CATEGORY_HANDLING+0x00020000);
  public static final int KSERROR_CANNOT_SIGNAL_EVENT           = (KSERROR_CATEGORY_HANDLING+0x00030000);
  public static final int KSERROR_CANNOT_POST_MESSAGE           = (KSERROR_CATEGORY_HANDLING+0x00040000);
  public static final int KSERROR_CANNOT_CREATE_SHARED          = (KSERROR_CATEGORY_HANDLING+0x00050000);
  public static final int KSERROR_SHARED_DIFFERENT_SIZE         = (KSERROR_CATEGORY_HANDLING+0x00060000);
  public static final int KSERROR_BAD_SHARED_MEM                = (KSERROR_CATEGORY_HANDLING+0x00070000);
  public static final int KSERROR_WAIT_TIMEOUT                  = (KSERROR_CATEGORY_HANDLING+0x00080000);
  public static final int KSERROR_EVENT_NOT_SIGNALED            = (KSERROR_CATEGORY_HANDLING+0x00090000);
  public static final int KSERROR_CANNOT_CREATE_CALLBACK        = (KSERROR_CATEGORY_HANDLING+0x000a0000);
  public static final int KSERROR_NO_DATA_AVAILABLE             = (KSERROR_CATEGORY_HANDLING+0x000b0000);
  public static final int KSERROR_REQUEST_NESTED                = (KSERROR_CATEGORY_HANDLING+0x000c0000);

  //------ Deal error codes ------
  public static final int KSERROR_BAD_CUSTNUM                   = (KSERROR_CATEGORY_DEAL+0x00030000);
  public static final int KSERROR_KEY_BAD_FORMAT                = (KSERROR_CATEGORY_DEAL+0x00060000);
  public static final int KSERROR_KEY_BAD_DATA                  = (KSERROR_CATEGORY_DEAL+0x00080000);
  public static final int KSERROR_KEY_PERMISSION_EXPIRED        = (KSERROR_CATEGORY_DEAL+0x00090000);
  public static final int KSERROR_BAD_LICENCE                   = (KSERROR_CATEGORY_DEAL+0x000a0000);
  public static final int KSERROR_CANNOT_LOAD_KEY               = (KSERROR_CATEGORY_DEAL+0x000c0000);
  public static final int KSERROR_BAD_NAME_OR_FIRM              = (KSERROR_CATEGORY_DEAL+0x000f0000);
  public static final int KSERROR_NO_LICENCES_FOUND             = (KSERROR_CATEGORY_DEAL+0x00100000);
  public static final int KSERROR_LICENCE_CAPACITY_EXHAUSTED    = (KSERROR_CATEGORY_DEAL+0x00110000);
  public static final int KSERROR_FEATURE_NOT_LICENSED          = (KSERROR_CATEGORY_DEAL+0x00120000);

  //------ Exception Codes ------
  public static final int KSERROR_DIVIDE_BY_ZERO                = (KSERROR_CATEGORY_EXCEPTION+0x00000000);
  public static final int KSERROR_DEBUG_EXCEPTION               = (KSERROR_CATEGORY_EXCEPTION+0x00010000);
  public static final int KSERROR_NONMASKABLE_INTERRUPT         = (KSERROR_CATEGORY_EXCEPTION+0x00020000);
  public static final int KSERROR_BREAKPOINT                    = (KSERROR_CATEGORY_EXCEPTION+0x00030000);
  public static final int KSERROR_OVERFLOW                      = (KSERROR_CATEGORY_EXCEPTION+0x00040000);
  public static final int KSERROR_BOUND_RANGE                   = (KSERROR_CATEGORY_EXCEPTION+0x00050000);
  public static final int KSERROR_INVALID_OPCODE                = (KSERROR_CATEGORY_EXCEPTION+0x00060000);
  public static final int KSERROR_NO_MATH                       = (KSERROR_CATEGORY_EXCEPTION+0x00070000);
  public static final int KSERROR_DOUBLE_FAULT                  = (KSERROR_CATEGORY_EXCEPTION+0x00080000);
  public static final int KSERROR_COPROCESSOR_SEGMENT_OVERRUN   = (KSERROR_CATEGORY_EXCEPTION+0x00090000);
  public static final int KSERROR_INVALID_TSS                   = (KSERROR_CATEGORY_EXCEPTION+0x000a0000);
  public static final int KSERROR_SEGMENT_NOT_PRESENT           = (KSERROR_CATEGORY_EXCEPTION+0x000b0000);
  public static final int KSERROR_STACK_FAULT                   = (KSERROR_CATEGORY_EXCEPTION+0x000c0000);
  public static final int KSERROR_PROTECTION_FAULT              = (KSERROR_CATEGORY_EXCEPTION+0x000d0000);
  public static final int KSERROR_PAGE_FAULT                    = (KSERROR_CATEGORY_EXCEPTION+0x000e0000);
  public static final int KSERROR_FPU_FAULT                     = (KSERROR_CATEGORY_EXCEPTION+0x00100000);
  public static final int KSERROR_ALIGNMENT_CHECK               = (KSERROR_CATEGORY_EXCEPTION+0x00110000);
  public static final int KSERROR_MACHINE_CHECK                 = (KSERROR_CATEGORY_EXCEPTION+0x00120000);
  public static final int KSERROR_SIMD_FAULT                    = (KSERROR_CATEGORY_EXCEPTION+0x00130000);

  //------ Flags ------
  public static final int KSF_OPENED                            = 0x01000000;
  public static final int KSF_ACTIVE                            = 0x02000000;
  public static final int KSF_RUNNING                           = 0x02000000;
  public static final int KSF_FINISHED                          = 0x04000000;
  public static final int KSF_CANCELED                          = 0x08000000;
  public static final int KSF_DISABLED                          = 0x10000000;
  public static final int KSF_REQUESTED                         = 0x20000000;
  public static final int KSF_TERMINATE                         = 0x00000000;
  public static final int KSF_DONT_WAIT                         = 0x00000001;
  public static final int KSF_KERNEL_EXEC                       = 0x00000004;
  public static final int KSF_ASYNC_EXEC                        = 0x0000000c;
  public static final int KSF_DIRECT_EXEC                       = 0x0000000c;
  public static final int KSF_DONT_START                        = 0x00000010;
  public static final int KSF_ONE_SHOT                          = 0x00000020;
  public static final int KSF_SINGLE_SHOT                       = 0x00000020;
  public static final int KSF_SAVE_FPU                          = 0x00000040;
  public static final int KSF_USE_SSE                           = 0x00000001;
  public static final int KSF_USER_EXEC                         = 0x00000080;
  public static final int KSF_REALTIME_EXEC                     = 0x00000100;
  public static final int KSF_WAIT                              = 0x00000200;
  public static final int KSF_ACCEPT_INCOMPLETE                 = 0x00000200;
  public static final int KSF_REPORT_RESOURCE                   = 0x00000400;
  public static final int KSF_USE_TIMEOUTS                      = 0x00000400;
  public static final int KSF_FORCE_OVERRIDE                    = 0x00001000;
  public static final int KSF_OPTION_ON                         = 0x00001000;
  public static final int KSF_OPTION_OFF                        = 0x00002000;
  public static final int KSF_FORCE_OPEN                        = 0x00002000;
  public static final int KSF_UNLIMITED                         = 0x00004000;
  public static final int KSF_DONT_USE_SMP                      = 0x00004000;
  public static final int KSF_NO_CONTEXT                        = 0x00008000;
  public static final int KSF_HIGH_PRECISION                    = 0x00008000;
  public static final int KSF_CONTINUOUS                        = 0x00010000;
  public static final int KSF_ZERO_INIT                         = 0x00020000;
  public static final int KSF_TX_PDO                            = 0x00010000;
  public static final int KSF_RX_PDO                            = 0x00020000;
  public static final int KSF_READ                              = 0x00040000;
  public static final int KSF_WRITE                             = 0x00080000;
  public static final int KSF_USE_SMP                           = 0x00080000;
  public static final int KSF_ALTERNATIVE                       = 0x00200000;
  public static final int KSF_PDO                               = 0x00040000;
  public static final int KSF_SDO                               = 0x00080000;
  public static final int KSF_IDN                               = 0x00100000;
  public static final int KSF_MANUAL_RESET                      = 0x00200000;
  public static final int KSF_RESET_STATE                       = 0x00800000;
  public static final int KSF_ISA_BUS                           = 0x00000001;
  public static final int KSF_PCI_BUS                           = 0x00000005;
  public static final int KSF_PRIO_NORMAL                       = 0x00000000;
  public static final int KSF_PRIO_LOW                          = 0x00000001;
  public static final int KSF_PRIO_LOWER                        = 0x00000002;
  public static final int KSF_PRIO_LOWEST                       = 0x00000003;
  public static final int KSF_MESSAGE_PIPE                      = 0x00001000;
  public static final int KSF_PIPE_NOTIFY_GET                   = 0x00002000;
  public static final int KSF_PIPE_NOTIFY_PUT                   = 0x00004000;
  public static final int KSF_ALL_CPUS                          = 0x00000000;
  public static final int KSF_READ_ACCESS                       = 0x00000001;
  public static final int KSF_WRITE_ACCESS                      = 0x00000002;
  public static final int KSF_SIZE_8_BIT                        = 0x00010000;
  public static final int KSF_SIZE_16_BIT                       = 0x00020000;
  public static final int KSF_SIZE_32_BIT                       = 0x00040000;

  //------ Obsolete flags - do not use! ------
  public static final int KSF_ACCEPT_PENDING                    = 0x00000002;
  public static final int KSF_DONT_RAISE                        = 0x00200000;

  //------ Constant for returning no-error ------
  public static final int KS_OK                                 = 0x00000000;

  //------ Language constants ------
  public static final int KSLNG_DEFAULT                         = 0x00000000;

  //------ Context type values ------
  public static final int USER_CONTEXT                          = 0x00000000;
  public static final int INTERRUPT_CONTEXT                     = 0x00000100;
  public static final int TIMER_CONTEXT                         = 0x00000200;
  public static final int KEYBOARD_CONTEXT                      = 0x00000300;
  public static final int USB_BASE                              = 0x00000506;
  public static final int PACKET_BASE                           = 0x00000700;
  public static final int DEVICE_BASE                           = 0x00000800;
  public static final int ETHERCAT_BASE                         = 0x00000900;
  public static final int SOCKET_BASE                           = 0x00000a00;
  public static final int TASK_BASE                             = 0x00000b00;
  public static final int MULTIFUNCTION_BASE                    = 0x00000c00;
  public static final int CAN_BASE                              = 0x00000d00;
  public static final int PROFIBUS_BASE                         = 0x00000e00;
  public static final int CANOPEN_BASE                          = 0x00000f00;
  public static final int FLEXRAY_BASE                          = 0x00001000;
  public static final int XHCI_BASE                             = 0x00001100;
  public static final int V86_BASE                              = 0x00001200;
  public static final int PLC_BASE                              = 0x00002000;

  //------ Config code for "Driver" ------
  public static final int KSCONFIG_DRIVER_NAME                  = 0x00000001;
  public static final int KSCONFIG_FUNCTION_LOGGING             = 0x00000002;

  //------ Config code for "Base" ------
  public static final int KSCONFIG_DISABLE_MESSAGE_LOGGING      = 0x00000001;
  public static final int KSCONFIG_ENABLE_MESSAGE_LOGGING       = 0x00000002;
  public static final int KSCONFIG_FLUSH_MESSAGE_PIPE           = 0x00000003;

  //------ Config code for "Debug" ------

  //------ Commonly used commands ------
  public static final int KS_ABORT_XMIT_CMD                     = 0x00000001;
  public static final int KS_ABORT_RECV_CMD                     = 0x00000002;
  public static final int KS_FLUSH_XMIT_BUF                     = 0x00000003;
  public static final int KS_FLUSH_RECV_BUF                     = 0x00000009;
  public static final int KS_SET_BAUD_RATE                      = 0x0000000c;

  //------ Pipe contexts ------
  public static final int PIPE_PUT_CONTEXT                      = (USER_CONTEXT+0x00000040);
  public static final int PIPE_GET_CONTEXT                      = (USER_CONTEXT+0x00000041);

  //------ Quick mutex levels ------
  public static final int KS_APP_LEVEL                          = 0x00000000;
  public static final int KS_DPC_LEVEL                          = 0x00000001;
  public static final int KS_ISR_LEVEL                          = 0x00000002;
  public static final int KS_RTX_LEVEL                          = 0x00000003;
  public static final int KS_CPU_LEVEL                          = 0x00000004;

  //------ Data types ------
  public static final int KS_DATATYPE_UNKNOWN                   = 0x00000000;
  public static final int KS_DATATYPE_BOOLEAN                   = 0x00000001;
  public static final int KS_DATATYPE_INTEGER8                  = 0x00000002;
  public static final int KS_DATATYPE_UINTEGER8                 = 0x00000005;
  public static final int KS_DATATYPE_INTEGER16                 = 0x00000003;
  public static final int KS_DATATYPE_UINTEGER16                = 0x00000006;
  public static final int KS_DATATYPE_INTEGER32                 = 0x00000004;
  public static final int KS_DATATYPE_UINTEGER32                = 0x00000007;
  public static final int KS_DATATYPE_INTEGER64                 = 0x00000015;
  public static final int KS_DATATYPE_UNSIGNED64                = 0x0000001b;
  public static final int KS_DATATYPE_REAL32                    = 0x00000008;
  public static final int KS_DATATYPE_REAL64                    = 0x00000011;
  public static final int KS_DATATYPE_TINY                      = 0x00000002;
  public static final int KS_DATATYPE_BYTE                      = 0x00000005;
  public static final int KS_DATATYPE_SHORT                     = 0x00000003;
  public static final int KS_DATATYPE_USHORT                    = 0x00000006;
  public static final int KS_DATATYPE_INT                       = 0x00000004;
  public static final int KS_DATATYPE_UINT                      = 0x00000007;
  public static final int KS_DATATYPE_LLONG                     = 0x00000015;
  public static final int KS_DATATYPE_ULONG                     = 0x0000001b;
  public static final int KS_DATATYPE_VISIBLE_STRING            = 0x00000009;
  public static final int KS_DATATYPE_OCTET_STRING              = 0x0000000a;
  public static final int KS_DATATYPE_UNICODE_STRING            = 0x0000000b;
  public static final int KS_DATATYPE_TIME_OF_DAY               = 0x0000000c;
  public static final int KS_DATATYPE_TIME_DIFFERENCE           = 0x0000000d;
  public static final int KS_DATATYPE_DOMAIN                    = 0x0000000f;
  public static final int KS_DATATYPE_TIME_OF_DAY32             = 0x0000001c;
  public static final int KS_DATATYPE_DATE32                    = 0x0000001d;
  public static final int KS_DATATYPE_DATE_AND_TIME64           = 0x0000001e;
  public static final int KS_DATATYPE_TIME_DIFFERENCE64         = 0x0000001f;
  public static final int KS_DATATYPE_BIT1                      = 0x00000030;
  public static final int KS_DATATYPE_BIT2                      = 0x00000031;
  public static final int KS_DATATYPE_BIT3                      = 0x00000032;
  public static final int KS_DATATYPE_BIT4                      = 0x00000033;
  public static final int KS_DATATYPE_BIT5                      = 0x00000034;
  public static final int KS_DATATYPE_BIT6                      = 0x00000035;
  public static final int KS_DATATYPE_BIT7                      = 0x00000036;
  public static final int KS_DATATYPE_BIT8                      = 0x00000037;
  public static final int KS_DATATYPE_INTEGER24                 = 0x00000010;
  public static final int KS_DATATYPE_UNSIGNED24                = 0x00000016;
  public static final int KS_DATATYPE_INTEGER40                 = 0x00000012;
  public static final int KS_DATATYPE_UNSIGNED40                = 0x00000018;
  public static final int KS_DATATYPE_INTEGER48                 = 0x00000013;
  public static final int KS_DATATYPE_UNSIGNED48                = 0x00000019;
  public static final int KS_DATATYPE_INTEGER56                 = 0x00000014;
  public static final int KS_DATATYPE_UNSIGNED56                = 0x0000001a;
  public static final int KS_DATATYPE_KSHANDLE                  = 0x00000040;

  //------ DataObj type flags ------
  public static final int KS_DATAOBJ_READABLE                   = 0x00010000;
  public static final int KS_DATAOBJ_WRITEABLE                  = 0x00020000;
  public static final int KS_DATAOBJ_ARRAY                      = 0x01000000;
  public static final int KS_DATAOBJ_ENUM                       = 0x02000000;
  public static final int KS_DATAOBJ_STRUCT                     = 0x04000000;

  //------ Common types and structures ------
  public static class HandlerState {
    public int requested;
    public int performed;
    public int state;
    public int error;
  }

  public static class KSTimeOut {
    public int baseTime;
    public int charTime;
  }

  public static class KSDeviceInfo {
    public int structSize;
    public byte[] pDeviceName = new byte[80];
    public byte[] pDeviceDesc = new byte[80];
    public byte[] pDeviceVendor = new byte[80];
    public byte[] pFriendlyName = new byte[80];
    public byte[] pHardwareId = new byte[80];
    public byte[] pClassName = new byte[80];
    public byte[] pInfPath = new byte[80];
    public byte[] pDriverDesc = new byte[80];
    public byte[] pDriverVendor = new byte[80];
    public byte[] pDriverDate = new byte[80];
    public byte[] pDriverVersion = new byte[80];
    public byte[] pServiceName = new byte[80];
    public byte[] pServiceDisp = new byte[80];
    public byte[] pServiceDesc = new byte[80];
    public byte[] pServicePath = new byte[80];
  }

  public static class KSSystemInformation {
    public int structSize;
    public int numberOfCPUs;
    public int numberOfSharedCPUs;
    public int kiloBytesOfRAM;
    public byte isDll64Bit;
    public byte isSys64Bit;
    public byte isStableVersion;
    public byte isLogVersion;
    public int dllVersion;
    public int dllBuildNumber;
    public int sysVersion;
    public int sysBuildNumber;
    public int systemVersion;
    public byte[] pSystemName = new byte[64];
    public byte[] pServicePack = new byte[64];
    public byte[] pDllPath = new byte[256];
    public byte[] pSysPath = new byte[256];
  }

  public static class KSProcessorInformation {
    public int structSize;
    public int index;
    public int group;
    public int number;
    public int currentTemperature;
    public int allowedTemperature;
  }

  public static class KSSharedMemInfo {
    public int structSize;
    public Handle hHandle;
    public byte[] pName = new byte[256];
    public int size;
    public IntPtr pThisPtr;
  }

  //------ Context structures ------
  public static class PipeUserContext {
    public int ctxType;
    public Handle hPipe;
  }

  //------ Class for saving callback information ------
  public static class CallBackRoutine {
    public CallBackRoutine(Object object, String method) {
      object_ = object; method_ = method;
    }
    private Object object_;
    private String method_;
  }
  //------ Common functions ------
  public static native int KS_openDriver(
    string customerNumber);
  public static native int KS_closeDriver(
    );
  public static native int KS_getDriverVersion(
    IntPtr pVersion);
  public static native int KS_getErrorString(
    int code, StrPtr msg, int language);
  public static native int KS_addErrorString(
    int code, string msg, int language);
  public static native int KS_getDriverConfig(
    string module, int configType, byte[] pData);
  public static native int KS_setDriverConfig(
    string module, int configType, byte[] pData);
  public static native int KS_getSystemInformation(
    KSSystemInformation pSystemInfo, int flags);
  public static native int KS_getProcessorInformation(
    int index, KSProcessorInformation pProcessorInfo, int flags);
  public static native int KS_closeHandle(
    Handle handle, int flags);

  //------ Debugging & test ------
  public static native int KS_showMessage(
    int ksError, string msg);
  public static native int KS_logMessage(
    int msgType, string msgText);
  public static native int KS_bufMessage(
    int msgType, byte[] pBuffer, int length, string msgText);
  public static native int KS_dbgMessage(
    string fileName, int line, string msgText);
  public static native int KS_beep(
    int freq, int duration);
  public static native int KS_throwException(
    int error, string pText, string pFile, int line);

  //------ Device handling ------
  public static native int KS_enumDevices(
    string deviceType, int index, StrPtr pDeviceNameBuf, int flags);
  public static native int KS_enumDevicesEx(
    string className, string busType, Handle hEnumerator, int index, StrPtr pDeviceNameBuf, int flags);
  public static native int KS_getDevices(
    string className, string busType, Handle hEnumerator, IntPtr pCount, IntPtr pBuf, int size, int flags);
  public static native int KS_getDeviceInfo(
    string deviceName, KSDeviceInfo pDeviceInfo, int flags);
  public static native int KS_updateDriver(
    string deviceName, string fileName, int flags);

  //------ Threads & priorities ------
  public static native int KS_setThreadPrio(
    int prio);
  public static native int KS_getThreadPrio(
    IntPtr pPrio);

  //------ Shared memory ------
  public static native int KS_createSharedMem(
    IntPtr ppAppPtr, IntPtr ppSysPtr, string name, int size, int flags);
  public static native int KS_freeSharedMem(
    IntPtr pAppPtr);
  public static native int KS_getSharedMem(
    IntPtr ppPtr, string name);
  public static native int KS_readMem(
    byte[] pBuffer, IntPtr pAppPtr, int size, int offset);
  public static native int KS_writeMem(
    byte[] pBuffer, IntPtr pAppPtr, int size, int offset);
  public static native int KS_createSharedMemEx(
    Handle phHandle, string name, int size, int flags);
  public static native int KS_freeSharedMemEx(
    Handle hHandle, int flags);
  public static native int KS_getSharedMemEx(
    Handle hHandle, IntPtr pPtr, int flags);

  //------ Events ------
  public static native int KS_createEvent(
    Handle phEvent, string name, int flags);
  public static native int KS_closeEvent(
    Handle hEvent);
  public static native int KS_setEvent(
    Handle hEvent);
  public static native int KS_resetEvent(
    Handle hEvent);
  public static native int KS_pulseEvent(
    Handle hEvent);
  public static native int KS_waitForEvent(
    Handle hEvent, int flags, int timeout);
  public static native int KS_getEventState(
    Handle hEvent, IntPtr pState);
  public static native int KS_postMessage(
    Handle hWnd, int msg, int wP, int lP);

  //------ CallBacks ------
  public static native int KS_createCallBack(
    Handle phCallBack, CallBackRoutine routine, Object pArgs, int flags, int prio);
  public static native int KS_removeCallBack(
    Handle hCallBack);
  public static native int KS_execCallBack(
    Handle hCallBack, Object pContext, int flags);
  public static native int KS_getCallState(
    Handle hSignal, HandlerState pState);
  public static native int KS_signalObject(
    Handle hObject, Object pContext, int flags);

  //------ Pipes ------
  public static native int KS_createPipe(
    Handle phPipe, string name, int itemSize, int itemCount, Handle hNotify, int flags);
  public static native int KS_removePipe(
    Handle hPipe);
  public static native int KS_flushPipe(
    Handle hPipe, int flags);
  public static native int KS_getPipe(
    Handle hPipe, byte[] pBuffer, int length, IntPtr pLength, int flags);
  public static native int KS_putPipe(
    Handle hPipe, byte[] pBuffer, int length, IntPtr pLength, int flags);

  //------ Quick mutexes ------
  public static native int KS_createQuickMutex(
    Handle phMutex, int level, int flags);
  public static native int KS_removeQuickMutex(
    Handle hMutex);
  public static native int KS_requestQuickMutex(
    Handle hMutex);
  public static native int KS_releaseQuickMutex(
    Handle hMutex);

  //------------------------------------------------------------------------------------------------------------
  // Kernel Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public static final int KSERROR_RTX_TOO_MANY_FUNCTIONS        = (KSERROR_CATEGORY_KERNEL+0x00000000);
  public static final int KSERROR_CANNOT_EXECUTE_RTX_CALL       = (KSERROR_CATEGORY_KERNEL+0x00010000);
  public static final int KSERROR_CANNOT_PREPARE_RTX            = (KSERROR_CATEGORY_KERNEL+0x00020000);
  public static final int KSERROR_RTX_BAD_SYSTEM_CALL           = (KSERROR_CATEGORY_KERNEL+0x00030000);
  public static final int KSERROR_CANNOT_LOAD_KERNEL            = (KSERROR_CATEGORY_KERNEL+0x00040000);
  public static final int KSERROR_CANNOT_RELOCATE_KERNEL        = (KSERROR_CATEGORY_KERNEL+0x00050000);
  public static final int KSERROR_KERNEL_ALREADY_LOADED         = (KSERROR_CATEGORY_KERNEL+0x00060000);
  public static final int KSERROR_KERNEL_NOT_LOADED             = (KSERROR_CATEGORY_KERNEL+0x00070000);

  //------ Flags ------
  public static final int KSF_ANALYSE_ONLY                      = 0x00010000;
  public static final int KSF_FUNC_TABLE_GIVEN                  = 0x00020000;
  public static final int KSF_FORCE_RELOC                       = 0x00040000;
  public static final int KSF_LOAD_DEPENDENCIES                 = 0x00001000;
  public static final int KSF_EXEC_ENTRY_POINT                  = 0x00002000;

  //------ Kernel commands ------
  public static final int KS_GET_KERNEL_BASE_ADDRESS            = 0x00000001;

  //------ Relocation by byte-wise instruction mapping ------

  //------ Relocation by loading DLL into kernel-space ------
  public static native int KS_loadKernel(
    Handle phKernel, string dllName, string initProcName, Object pArgs, int flags);
  public static native int KS_loadKernelFromBuffer(
    Handle phKernel, string dllName, IntPtr pBuffer, int length, string initProcName, Object pArgs, int flags);
  public static native int KS_freeKernel(
    Handle hKernel);
  public static native int KS_execKernelFunction(
    Handle hKernel, string name, Object pArgs, Object pContext, int flags);
  public static native int KS_getKernelFunction(
    Handle hKernel, string name, ??? pRelocated, int flags);
  public static native int KS_enumKernelFunctions(
    Handle hKernel, int index, StrPtr pName, int flags);
  public static native int KS_execKernelCommand(
    Handle hKernel, int command, Object pParam, int flags);
  public static native int KS_createKernelCallBack(
    Handle phCallBack, Handle hKernel, string name, Object pArgs, int flags, int prio);

  //------ Helper functions ------

  //------ Memory functions ------
  public static native int KS_memCpy(
    byte[] pDst, byte[] pSrc, int size, int flags);
  public static native int KS_memMove(
    byte[] pDst, byte[] pSrc, int size, int flags);
  public static native int KS_memSet(
    byte[] pMem, int value, int size, int flags);

  //------ Interlocked functions ------

  //------------------------------------------------------------------------------------------------------------
  // IoPort Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public static final int KSERROR_CANNOT_ENABLE_IO_RANGE        = (KSERROR_CATEGORY_IOPORT+0x00000000);
  public static final int KSERROR_BUS_NOT_FOUND                 = (KSERROR_CATEGORY_IOPORT+0x00010000);
  public static final int KSERROR_BAD_SLOT_NUMBER               = (KSERROR_CATEGORY_IOPORT+0x00020000);

  //------ Number of address entries in PCI data ------
  public static final int KS_PCI_TYPE0_ADDRESSES                = 0x00000006;
  public static final int KS_PCI_TYPE1_ADDRESSES                = 0x00000002;
  public static final int KS_PCI_TYPE2_ADDRESSES                = 0x00000004;

  //------ Bit encodings for KSPciBusData.headerType ------
  public static final int KSF_PCI_MULTIFUNCTION                 = 0x00000080;
  public static final int KSF_PCI_DEVICE_TYPE                   = 0x00000000;
  public static final int KSF_PCI_PCI_BRIDGE_TYPE               = 0x00000001;
  public static final int KSF_PCI_CARDBUS_BRIDGE_TYPE           = 0x00000002;

  //------ Bit encodings for KSPciBusData.command ------
  public static final int KSF_PCI_ENABLE_IO_SPACE               = 0x00000001;
  public static final int KSF_PCI_ENABLE_MEMORY_SPACE           = 0x00000002;
  public static final int KSF_PCI_ENABLE_BUS_MASTER             = 0x00000004;
  public static final int KSF_PCI_ENABLE_SPECIAL_CYCLES         = 0x00000008;
  public static final int KSF_PCI_ENABLE_WRITE_AND_INVALIDATE   = 0x00000010;
  public static final int KSF_PCI_ENABLE_VGA_COMPATIBLE_PALETTE = 0x00000020;
  public static final int KSF_PCI_ENABLE_PARITY                 = 0x00000040;
  public static final int KSF_PCI_ENABLE_WAIT_CYCLE             = 0x00000080;
  public static final int KSF_PCI_ENABLE_SERR                   = 0x00000100;
  public static final int KSF_PCI_ENABLE_FAST_BACK_TO_BACK      = 0x00000200;

  //------ Bit encodings for KSPciBusData.status ------
  public static final int KSF_PCI_STATUS_CAPABILITIES_LIST      = 0x00000010;
  public static final int KSF_PCI_STATUS_FAST_BACK_TO_BACK      = 0x00000080;
  public static final int KSF_PCI_STATUS_DATA_PARITY_DETECTED   = 0x00000100;
  public static final int KSF_PCI_STATUS_DEVSEL                 = 0x00000600;
  public static final int KSF_PCI_STATUS_SIGNALED_TARGET_ABORT  = 0x00000800;
  public static final int KSF_PCI_STATUS_RECEIVED_TARGET_ABORT  = 0x00001000;
  public static final int KSF_PCI_STATUS_RECEIVED_MASTER_ABORT  = 0x00002000;
  public static final int KSF_PCI_STATUS_SIGNALED_SYSTEM_ERROR  = 0x00004000;
  public static final int KSF_PCI_STATUS_DETECTED_PARITY_ERROR  = 0x00008000;

  //------ Bit encodings for KSPciBusData.baseAddresses ------
  public static final int KSF_PCI_ADDRESS_IO_SPACE              = 0x00000001;
  public static final int KSF_PCI_ADDRESS_MEMORY_TYPE_MASK      = 0x00000006;
  public static final int KSF_PCI_ADDRESS_MEMORY_PREFETCHABLE   = 0x00000008;

  //------ PCI types ------
  public static final int KSF_PCI_TYPE_32BIT                    = 0x00000000;
  public static final int KSF_PCI_TYPE_20BIT                    = 0x00000002;
  public static final int KSF_PCI_TYPE_64BIT                    = 0x00000004;

  //------ Resource types ------
  public static final int KSRESOURCE_TYPE_NONE                  = 0x00000000;
  public static final int KSRESOURCE_TYPE_PORT                  = 0x00000001;
  public static final int KSRESOURCE_TYPE_INTERRUPT             = 0x00000002;
  public static final int KSRESOURCE_TYPE_MEMORY                = 0x00000003;
  public static final int KSRESOURCE_TYPE_STRING                = 0x000000ff;

  //------ KSRESOURCE_SHARE ------
  public static final int KSRESOURCE_SHARE_UNDETERMINED         = 0x00000000;
  public static final int KSRESOURCE_SHARE_DEVICEEXCLUSIVE      = 0x00000001;
  public static final int KSRESOURCE_SHARE_DRIVEREXCLUSIVE      = 0x00000002;
  public static final int KSRESOURCE_SHARE_SHARED               = 0x00000003;

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_PORT ------
  public static final int KSRESOURCE_MODE_MEMORY                = 0x00000000;
  public static final int KSRESOURCE_MODE_IO                    = 0x00000001;

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_INTERRUPT ------
  public static final int KSRESOURCE_MODE_LEVELSENSITIVE        = 0x00000000;
  public static final int KSRESOURCE_MODE_LATCHED               = 0x00000001;

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_MEMORY ------
  public static final int KSRESOURCE_MODE_READ_WRITE            = 0x00000000;
  public static final int KSRESOURCE_MODE_READ_ONLY             = 0x00000001;
  public static final int KSRESOURCE_MODE_WRITE_ONLY            = 0x00000002;

  //------ Types & Structures ------
  public static class KSPciBusData {
    public short vendorID;
    public short deviceID;
    public short command;
    public short status;
    public byte revisionID;
    public byte progIf;
    public byte subClass;
    public byte baseClass;
    public byte cacheLineSize;
    public byte latencyTimer;
    public byte headerType;
    public byte BIST;
    public int[] pBaseAddresses = new int[6];
    public int CIS;
    public short subVendorID;
    public short subSystemID;
    public int baseROMAddress;
    public int capabilityList;
    public int reserved;
    public byte interruptLine;
    public byte interruptPin;
    public byte minimumGrant;
    public byte maximumLatency;
    public byte[] pDeviceSpecific = new byte[192];
  }

  public static class KSResourceItem {
    public int flags;
    public int data0;
    public int data1;
    public int data2;
  }

  public static class KSResourceInfo {
    public int busType;
    public int busNumber;
    public int version;
    public int itemCount;
    public KSResourceItem[] pItems = new KSResourceItem[8];
  }

  public static class KSRangeResourceItem {
    public int flags;
    public int address;
    public int reserved;
    public int length;
  }

  public static class KSInterruptResourceItem {
    public int flags;
    public int level;
    public int vector;
    public int affinity;
  }

  public static class KSResourceInfoEx {
    public int busType;
    public int busNumber;
    public int version;
    public int itemCount;
    public KSRangeResourceItem range0;
    public KSRangeResourceItem range1;
    public KSRangeResourceItem range2;
    public KSRangeResourceItem range3;
    public KSRangeResourceItem range4;
    public KSRangeResourceItem range5;
    public KSInterruptResourceItem interruptItem;
    public KSResourceItem reserved;
  }

  //------ I/O port access functions ------
  public static native int KS_enableIoRange(
    int ioPortBase, int count, int flags);
  public static native int KS_readIoPort(
    int ioPort, IntPtr pValue, int flags);
  public static native int KS_writeIoPort(
    int ioPort, int value, int flags);
  public static native int KS_inpb(
    int ioPort);
  public static native int KS_inpw(
    int ioPort);
  public static native int KS_inpd(
    int ioPort);
  public static native void KS_outpb(
    int ioPort, int value);
  public static native void KS_outpw(
    int ioPort, int value);
  public static native void KS_outpd(
    int ioPort, int value);

  //------ Bus data & resource info ------
  public static native int KS_getBusData(
    KSPciBusData pBusData, int busNumber, int slotNumber, int flags);
  public static native int KS_setBusData(
    KSPciBusData pBusData, int busNumber, int slotNumber, int flags);
  // DO NOT USE! - OBSOLETE! - USE 'KS_getResourceInfoEx' instead!
  public static native int KS_getResourceInfo(
    string name, KSResourceInfo pInfo);
  public static native int KS_getResourceInfoEx(
    string name, KSResourceInfoEx pInfo);

  //------ Quiet section (obsolete) ------

  //------------------------------------------------------------------------------------------------------------
  // Memory Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public static final int KSERROR_CANNOT_MAP_PHYSMEM            = (KSERROR_CATEGORY_MEMORY+0x00000000);
  public static final int KSERROR_CANNOT_UNMAP_PHYSMEM          = (KSERROR_CATEGORY_MEMORY+0x00010000);
  public static final int KSERROR_PHYSMEM_DIFFERENT_SIZE        = (KSERROR_CATEGORY_MEMORY+0x00020000);
  public static final int KSERROR_CANNOT_ALLOC_PHYSMEM          = (KSERROR_CATEGORY_MEMORY+0x00030000);
  public static final int KSERROR_CANNOT_LOCK_MEM               = (KSERROR_CATEGORY_MEMORY+0x00040000);
  public static final int KSERROR_CANNOT_UNLOCK_MEM             = (KSERROR_CATEGORY_MEMORY+0x00050000);

  //------ Flags ------
  public static final int KSF_MAP_INTERNAL                      = 0x00000000;

  //------ Physical memory management functions ------
  public static native int KS_mapDeviceMem(
    IntPtr ppAppPtr, IntPtr ppSysPtr, KSResourceInfoEx pInfo, int index, int flags);
  public static native int KS_mapPhysMem(
    IntPtr ppAppPtr, IntPtr ppSysPtr, int physAddr, int size, int flags);
  public static native int KS_unmapPhysMem(
    IntPtr pAppPtr);
  public static native int KS_allocPhysMem(
    IntPtr ppAppPtr, IntPtr ppSysPtr, IntPtr physAddr, int size, int flags);
  public static native int KS_freePhysMem(
    IntPtr pAppPtr);
  public static native int KS_copyPhysMem(
    int physAddr, byte[] pBuffer, int size, int offset, int flags);

  // ATTENTION: functions for getting shared memory: see 'Base Module'

  //------------------------------------------------------------------------------------------------------------
  // Clock Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public static final int KSERROR_NO_RDTSC_AVAILABLE            = (KSERROR_CATEGORY_SPECIAL+0x00010000);

  //------ Flags ------
  public static final int KSF_USE_GIVEN_FREQ                    = 0x00010000;
  public static final int KSF_CLOCK_DELEGATE                    = 0x00000001;
  public static final int KSF_CLOCK_CONVERT_ONLY                = 0x00000002;

  //------ Clock source constants (for 'clockId') ------
  public static final int KS_CLOCK_DEFAULT                      = 0x00000000;
  public static final int KS_CLOCK_MEASURE_SYSTEM_TIMER         = 0x00000001;
  public static final int KS_CLOCK_MEASURE_CPU_COUNTER          = 0x00000002;
  public static final int KS_CLOCK_MEASURE_APIC_TIMER           = 0x00000003;
  public static final int KS_CLOCK_MEASURE_PM_TIMER             = 0x00000004;
  public static final int KS_CLOCK_MEASURE_PC_TIMER             = 0x00000005;
  public static final int KS_CLOCK_MEASURE_INTERVAL_COUNTER     = 0x00000006;
  public static final int KS_CLOCK_MEASURE_HPET                 = 0x00000007;
  public static final int KS_CLOCK_MEASURE_HIGHEST              = 0x00000008;
  public static final int KS_CLOCK_CONVERT_HIGHEST              = 0x00000009;
  public static final int KS_CLOCK_PC_TIMER                     = 0x0000000a;
  public static final int KS_CLOCK_MACHINE_TIME                 = 0x0000000b;
  public static final int KS_CLOCK_ABSOLUTE_TIME                = 0x0000000c;
  public static final int KS_CLOCK_ABSOLUTE_TIME_LOCAL          = 0x0000000d;
  public static final int KS_CLOCK_MICROSECONDS                 = 0x0000000e;
  public static final int KS_CLOCK_UNIX_TIME                    = 0x0000000f;
  public static final int KS_CLOCK_UNIX_TIME_LOCAL              = 0x00000010;
  public static final int KS_CLOCK_MILLISECONDS                 = 0x00000011;
  public static final int KS_CLOCK_DOS_TIME                     = 0x00000012;
  public static final int KS_CLOCK_DOS_TIME_LOCAL               = 0x00000013;
  public static final int KS_CLOCK_IEEE1588_TIME                = 0x00000014;
  public static final int KS_CLOCK_IEEE1588_CONVERT             = 0x00000015;
  public static final int KS_CLOCK_TIME_OF_DAY                  = 0x00000016;
  public static final int KS_CLOCK_TIME_OF_DAY_LOCAL            = 0x00000017;
  public static final int KS_CLOCK_FIRST_USER                   = 0x00000018;

  //------ Obsolete clock source constant (use KS_CLOCK_MACHINE_TIME instead!) ------
  public static final int KS_CLOCK_SYSTEM_TIME                  = 0x0000000b;

  //------ Types & Structures ------
  public static class KSClockSource {
    public int structSize;
    public int flags;
    public long frequency;
    public long offset;
    public int baseClockId;
  }

  //------ Clock functions - Obsolete! Do not use! ------
  public static native int KS_calibrateMachineTime(
    IntPtr pFrequency, int flags);
  public static native int KS_getSystemTicks(
    LongPtr pSystemTicks);
  public static native int KS_getSystemTime(
    LongPtr pSystemTime);
  public static native int KS_getMachineTime(
    LongPtr pMachineTime);
  public static native int KS_getAbsTime(
    LongPtr pAbsTime);

  //------ Clock functions ------
  public static native int KS_getClock(
    LongPtr pClock, int clockId);
  public static native int KS_getClockSource(
    KSClockSource pClockSource, int clockId);
  public static native int KS_setClockSource(
    KSClockSource pClockSource, int clockId);
  public static native int KS_microDelay(
    int delay);
  public static native int KS_convertClock(
    LongPtr pValue, int clockIdFrom, int clockIdTo, int flags);

  //------------------------------------------------------------------------------------------------------------
  // Keyboard Module
  //------------------------------------------------------------------------------------------------------------

  //------ Flags ------
  public static final int KSF_SIGNAL_MAKE                       = 0x00000001;
  public static final int KSF_SIGNAL_BREAK                      = 0x00000002;
  public static final int KSF_IGNORE_KEY                        = 0x00000010;
  public static final int KSF_MODIFY_KEY                        = 0x00000020;

  //------ Key mask modifier ------
  public static final int KSF_KEY_SHIFT                         = 0x00000011;
  public static final int KSF_KEY_SHIFTR                        = 0x00000001;
  public static final int KSF_KEY_SHIFTL                        = 0x00000010;
  public static final int KSF_KEY_CTRL                          = 0x00000022;
  public static final int KSF_KEY_CTRLR                         = 0x00000002;
  public static final int KSF_KEY_CTRLL                         = 0x00000020;
  public static final int KSF_KEY_ALT                           = 0x00000044;
  public static final int KSF_KEY_ALTR                          = 0x00000004;
  public static final int KSF_KEY_ALTL                          = 0x00000040;
  public static final int KSF_KEY_WIN                           = 0x00000088;
  public static final int KSF_KEY_WINR                          = 0x00000008;
  public static final int KSF_KEY_WINL                          = 0x00000080;

  //------ Key mask groups ------
  public static final int KSF_KEY_ALPHA_NUM                     = 0x00000100;
  public static final int KSF_KEY_NUM_PAD                       = 0x00000200;
  public static final int KSF_KEY_CONTROL                       = 0x00000400;
  public static final int KSF_KEY_FUNCTION                      = 0x00000800;

  //------ Key mask combinations ------
  public static final int KSF_KEY_CTRL_ALT_DEL                  = 0x00010000;
  public static final int KSF_KEY_CTRL_ESC                      = 0x00020000;
  public static final int KSF_KEY_ALT_ESC                       = 0x00040000;
  public static final int KSF_KEY_ALT_TAB                       = 0x00080000;
  public static final int KSF_KEY_ALT_ENTER                     = 0x00100000;
  public static final int KSF_KEY_ALT_PRINT                     = 0x00200000;
  public static final int KSF_KEY_ALT_SPACE                     = 0x00400000;
  public static final int KSF_KEY_ESC                           = 0x00800000;
  public static final int KSF_KEY_TAB                           = 0x01000000;
  public static final int KSF_KEY_ENTER                         = 0x02000000;
  public static final int KSF_KEY_PRINT                         = 0x04000000;
  public static final int KSF_KEY_SPACE                         = 0x08000000;
  public static final int KSF_KEY_BACKSPACE                     = 0x10000000;
  public static final int KSF_KEY_PAUSE                         = 0x20000000;
  public static final int KSF_KEY_APP                           = 0x40000000;
  public static final int KSF_KEY_ALL                           = 0x7fffffff;

  public static final int KSF_KEY_MODIFIER                      = KSF_KEY_CTRL | KSF_KEY_ALT |
                                                                  KSF_KEY_SHIFT | KSF_KEY_WIN;

  public static final int KSF_KEY_SYSTEM                        = KSF_KEY_WIN |
                                                                  KSF_KEY_CTRL_ALT_DEL | KSF_KEY_CTRL_ESC |
                                                                  KSF_KEY_ALT_ESC | KSF_KEY_ALT_TAB;

  public static final int KSF_KEY_SYSTEM_DOS                    = KSF_KEY_SYSTEM |
                                                                  KSF_KEY_ALT_ENTER | KSF_KEY_ALT_PRINT |
                                                                  KSF_KEY_ALT_SPACE | KSF_KEY_PRINT;

  //------ Keyboard events ------
  public static final int KS_KEYBOARD_CONTEXT                   = 0x00000300;

  //------ Context structures ------
  public static class KeyboardUserContext {
    public int ctxType;
    public int mask;
    public short modifier;
    public short port;
    public short state;
    public short key;
  }

  //------ Keyboard functions ------
  public static native int KS_createKeyHandler(
    int mask, Handle hSignal, int flags);
  public static native int KS_removeKeyHandler(
    int mask);
  public static native int KS_simulateKeyStroke(
    int[] pKeys);

  //------------------------------------------------------------------------------------------------------------
  // Interrupt Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public static final int KSERROR_IRQ_ALREADY_USED              = (KSERROR_CATEGORY_INTERRUPT+0x00000000);
  public static final int KSERROR_CANNOT_INSTALL_HANDLER        = (KSERROR_CATEGORY_INTERRUPT+0x00010000);
  public static final int KSERROR_BAD_IRQ_INSTALLATION          = (KSERROR_CATEGORY_INTERRUPT+0x00020000);
  public static final int KSERROR_NO_HANDLER_INSTALLED          = (KSERROR_CATEGORY_INTERRUPT+0x00030000);
  public static final int KSERROR_NOBODY_IS_WAITING             = (KSERROR_CATEGORY_INTERRUPT+0x00040000);
  public static final int KSERROR_IRQ_DISABLED                  = (KSERROR_CATEGORY_INTERRUPT+0x00050000);
  public static final int KSERROR_IRQ_ENABLED                   = (KSERROR_CATEGORY_INTERRUPT+0x00060000);
  public static final int KSERROR_IRQ_IN_SERVICE                = (KSERROR_CATEGORY_INTERRUPT+0x00070000);
  public static final int KSERROR_IRQ_HANDLER_REMOVED           = (KSERROR_CATEGORY_INTERRUPT+0x00080000);

  //------ Flags ------
  public static final int KSF_DONT_PASS_NEXT                    = 0x00020000;
  public static final int KSF_PCI_INTERRUPT                     = 0x00040000;

  //------ Interrupt events ------
  public static final int KS_INTERRUPT_CONTEXT                  = 0x00000100;

  //------ Types & Structures ------
  public static class InterruptUserContext {
    public int ctxType;
    public Handle hInterrupt;
    public int irq;
    public int consumed;
  }

  //------ Interrupt handling functions ------
  public static native int KS_createDeviceInterrupt(
    Handle phInterrupt, KSResourceInfoEx pInfo, Handle hSignal, int flags);
  public static native int KS_createDeviceInterruptEx(
    Handle phInterrupt, string deviceName, Handle hSignal, int flags);
  public static native int KS_createInterrupt(
    Handle phInterrupt, int irq, Handle hSignal, int flags);
  public static native int KS_removeInterrupt(
    Handle hInterrupt);
  public static native int KS_disableInterrupt(
    Handle hInterrupt);
  public static native int KS_enableInterrupt(
    Handle hInterrupt);
  public static native int KS_simulateInterrupt(
    Handle hInterrupt);
  public static native int KS_getInterruptState(
    Handle hInterrupt, HandlerState pState);

  //------------------------------------------------------------------------------------------------------------
  // UART Module
  //------------------------------------------------------------------------------------------------------------

  //------ Flags ------
  public static final int KSF_CHECK_LINE_ERROR                  = 0x00008000;
  public static final int KSF_NO_FIFO                           = 0x00001000;
  public static final int KSF_USE_FIFO                          = 0x00040000;
  public static final int KSF_9_BIT_MODE                        = 0x00080000;
  public static final int KSF_TOGGLE_DTR                        = 0x00100000;
  public static final int KSF_TOGGLE_RTS                        = 0x00200000;
  public static final int KSF_TOGGLE_INVERSE                    = 0x00400000;

  //------ Uart commands ------
  public static final int KS_CLR_DTR                            = 0x00000004;
  public static final int KS_SET_DTR                            = 0x00000005;
  public static final int KS_CLR_RTS                            = 0x00000006;
  public static final int KS_SET_RTS                            = 0x00000007;
  public static final int KS_CLR_BREAK                          = 0x00000008;
  public static final int KS_SET_BREAK                          = 0x0000000a;
  public static final int KS_SKIP_NEXT                          = 0x0000000b;
  public static final int KS_CHANGE_LINE_CONTROL                = 0x0000000d;
  public static final int KS_SET_BAUD_RATE_MAX                  = 0x0000000e;
  public static final int KS_ENABLE_FIFO                        = 0x0000000f;
  public static final int KS_DISABLE_FIFO                       = 0x00000010;
  public static final int KS_ENABLE_INTERRUPTS                  = 0x00000011;
  public static final int KS_DISABLE_INTERRUPTS                 = 0x00000012;
  public static final int KS_SET_RECV_FIFO_SIZE                 = 0x00000013;

  //------ Line Errors ------
  public static final int KS_UART_ERROR_PARITY                  = 0x00000001;
  public static final int KS_UART_ERROR_FRAME                   = 0x00000002;
  public static final int KS_UART_ERROR_OVERRUN                 = 0x00000004;
  public static final int KS_UART_ERROR_BREAK                   = 0x00000008;
  public static final int KS_UART_ERROR_FIFO                    = 0x00000010;

  //------ UART events ------
  public static final int KS_UART_RECV                          = 0x00000000;
  public static final int KS_UART_RECV_ERROR                    = 0x00000001;
  public static final int KS_UART_XMIT                          = 0x00000002;
  public static final int KS_UART_XMIT_EMPTY                    = 0x00000003;
  public static final int KS_UART_LSR_CHANGE                    = 0x00000004;
  public static final int KS_UART_MSR_CHANGE                    = 0x00000005;
  public static final int KS_UART_BREAK                         = 0x00000006;

  //------ Types & Structures ------
  public static class UartUserContext {
    public int ctxType;
    public Handle hUart;
    public int value;
    public int lineError;
  }

  public static class UartRegisterUserContext {
    public int ctxType;
    public Handle hUart;
    public byte reg;
  }

  public static class KSUartProperties {
    public int baudRate;
    public byte parity;
    public byte dataBit;
    public byte stopBit;
    public byte reserved;
  }

  public static class KSUartState {
    public int structSize;
    public KSUartProperties properties;
    public byte modemState;
    public byte lineState;
    public short reserved;
    public int xmitCount;
    public int xmitFree;
    public int recvCount;
    public int recvAvail;
    public int recvErrorCount;
    public int recvLostCount;
  }

  //------ Common functions ------
  public static native int KS_openUart(
    Handle phUart, string name, int port, KSUartProperties pProperties, int recvBufferLength, int xmitBufferLength, int flags);
  public static native int KS_openUartEx(
    Handle phUart, Handle hConnection, int port, KSUartProperties pProperties, int recvBufferLength, int xmitBufferLength, int flags);
  public static native int KS_closeUart(
    Handle hUart, int flags);

  //------ Communication functions ------
  public static native int KS_xmitUart(
    Handle hUart, int value, int flags);
  public static native int KS_recvUart(
    Handle hUart, IntPtr pValue, IntPtr pLineError, int flags);
  public static native int KS_xmitUartBlock(
    Handle hUart, byte[] pBytes, int length, IntPtr pLength, int flags);
  public static native int KS_recvUartBlock(
    Handle hUart, byte[] pBytes, int length, IntPtr pLength, int flags);
  public static native int KS_installUartHandler(
    Handle hUart, int eventCode, Handle hSignal, int flags);
  public static native int KS_getUartState(
    Handle hUart, KSUartState pUartState, int flags);
  public static native int KS_execUartCommand(
    Handle hUart, int command, Object pParam, int flags);

  //------------------------------------------------------------------------------------------------------------
  // COMM Module
  //------------------------------------------------------------------------------------------------------------

  //------ Line Errors ------
  public static final int KS_COMM_ERROR_PARITY                  = 0x00000001;
  public static final int KS_COMM_ERROR_FRAME                   = 0x00000002;
  public static final int KS_COMM_ERROR_OVERRUN                 = 0x00000004;
  public static final int KS_COMM_ERROR_BREAK                   = 0x00000008;
  public static final int KS_COMM_ERROR_FIFO                    = 0x00000010;

  //------ COMM events ------
  public static final int KS_COMM_RECV                          = 0x00000000;
  public static final int KS_COMM_RECV_ERROR                    = 0x00000001;
  public static final int KS_COMM_XMIT                          = 0x00000002;
  public static final int KS_COMM_XMIT_EMPTY                    = 0x00000003;
  public static final int KS_COMM_LSR_CHANGE                    = 0x00000004;
  public static final int KS_COMM_MSR_CHANGE                    = 0x00000005;
  public static final int KS_COMM_BREAK                         = 0x00000006;

  //------ Types & Structures ------
  public static class CommUserContext {
    public int ctxType;
    public Handle hComm;
    public int value;
    public int lineError;
  }

  public static class CommRegisterUserContext {
    public int ctxType;
    public Handle hComm;
    public byte reg;
  }

  public static class KSCommProperties {
    public int baudRate;
    public byte parity;
    public byte dataBit;
    public byte stopBit;
    public byte reserved;
  }

  public static class KSCommState {
    public int structSize;
    public KSCommProperties properties;
    public byte modemState;
    public byte lineState;
    public short reserved;
    public int xmitCount;
    public int xmitFree;
    public int recvCount;
    public int recvAvail;
    public int recvErrorCount;
    public int recvLostCount;
  }

  //------ COMM functions ------
  public static native int KS_openComm(
    Handle phComm, string name, KSCommProperties pProperties, int recvBufferLength, int xmitBufferLength, int flags);
  public static native int KS_closeComm(
    Handle hComm, int flags);
  public static native int KS_xmitComm(
    Handle hComm, int value, int flags);
  public static native int KS_recvComm(
    Handle hComm, IntPtr pValue, IntPtr pLineError, int flags);
  public static native int KS_xmitCommBlock(
    Handle hComm, byte[] pBytes, int length, IntPtr pLength, int flags);
  public static native int KS_recvCommBlock(
    Handle hComm, byte[] pBytes, int length, IntPtr pLength, int flags);
  public static native int KS_installCommHandler(
    Handle hComm, int eventCode, Handle hSignal, int flags);
  public static native int KS_getCommState(
    Handle hComm, KSCommState pCommState, int flags);
  public static native int KS_execCommCommand(
    Handle hComm, int command, Object pParam, int flags);

  //------------------------------------------------------------------------------------------------------------
  // RealTime Module
  //------------------------------------------------------------------------------------------------------------

  //------ Error Codes ------
  public static final int KSERROR_TIMER_NOT_INSTALLED           = (KSERROR_CATEGORY_TIMER+0x00000000);
  public static final int KSERROR_TIMER_REMOVED                 = (KSERROR_CATEGORY_TIMER+0x00010000);
  public static final int KSERROR_TIMER_DISABLED                = (KSERROR_CATEGORY_TIMER+0x00020000);
  public static final int KSERROR_TIMER_NOT_DISABLED            = (KSERROR_CATEGORY_TIMER+0x00030000);
  public static final int KSERROR_CANNOT_INSTALL_TIMER          = (KSERROR_CATEGORY_TIMER+0x00040000);
  public static final int KSERROR_NO_TIMER_AVAILABLE            = (KSERROR_CATEGORY_TIMER+0x00050000);
  public static final int KSERROR_WAIT_CANCELLED                = (KSERROR_CATEGORY_TIMER+0x00060000);
  public static final int KSERROR_TIMER_ALREADY_RUNNING         = (KSERROR_CATEGORY_TIMER+0x00070000);
  public static final int KSERROR_TIMER_ALREADY_STOPPED         = (KSERROR_CATEGORY_TIMER+0x00080000);
  public static final int KSERROR_CANNOT_START_TIMER            = (KSERROR_CATEGORY_TIMER+0x00090000);

  //------ Flags ------
  public static final int KSF_DISTINCT_MODE                     = 0x00040000;
  public static final int KSF_DRIFT_CORRECTION                  = 0x00010000;
  public static final int KSF_DISABLE_PROTECTION                = 0x00020000;
  public static final int KSF_USE_IEEE1588_TIME                 = 0x00040000;

  //------ Timer module config ------
  public static final int KSCONFIG_SOFTWARE_TIMER               = 0x00000001;

  //------ Timer events ------
  public static final int KS_TIMER_CONTEXT                      = 0x00000200;

  //------ Context structures ------
  public static class TimerUserContext {
    public int ctxType;
    public Handle hTimer;
    public int delay;
  }

  //------ Real-time functions ------
  public static native int KS_createTimer(
    Handle phTimer, int delay, Handle hSignal, int flags);
  public static native int KS_removeTimer(
    Handle hTimer);
  public static native int KS_cancelTimer(
    Handle hTimer);
  public static native int KS_startTimer(
    Handle hTimer, int flags, int delay);
  public static native int KS_stopTimer(
    Handle hTimer);
  public static native int KS_startTimerDelayed(
    Handle hTimer, long start, int period, int flags);
  public static native int KS_adjustTimer(
    Handle hTimer, int period, int flags);

  //------ Obsolete timer functions - do not use! ------
  public static native int KS_disableTimer(
    Handle hTimer);
  public static native int KS_enableTimer(
    Handle hTimer);
  public static native int KS_getTimerState(
    Handle hTimer, HandlerState pState);

  //------ Obsolete timer resolution functions - do not use! ------
  public static native int KS_setTimerResolution(
    int resolution, int flags);
  public static native int KS_getTimerResolution(
    IntPtr pResolution, int flags);

  //------------------------------------------------------------------------------------------------------------
  // LIN Module
  //------------------------------------------------------------------------------------------------------------

  //------ LIN commands ------
  public static final int KS_LIN_RESET                          = 0x00000000;
  public static final int KS_LIN_SLEEP                          = 0x00000001;
  public static final int KS_LIN_WAKEUP                         = 0x00000002;
  public static final int KS_LIN_SET_BAUD_RATE                  = 0x00000004;
  public static final int KS_LIN_SET_VERSION                    = 0x00000005;
  public static final int KS_LIN_DISABLE_QUEUE_ERROR            = 0x00000006;
  public static final int KS_LIN_ENABLE_QUEUE_ERROR             = 0x00000007;

  //------ LIN bit rates ------
  public static final int KS_LIN_BAUD_19200                     = 0x00004b00;
  public static final int KS_LIN_BAUD_10417                     = 0x000028b1;
  public static final int KS_LIN_BAUD_9600                      = 0x00002580;
  public static final int KS_LIN_BAUD_4800                      = 0x000012c0;
  public static final int KS_LIN_BAUD_2400                      = 0x00000960;
  public static final int KS_LIN_BAUD_1200                      = 0x000004b0;

  //------ LIN Errors ------
  public static final int KS_LIN_ERROR_PHYSICAL                 = 0x00000001;
  public static final int KS_LIN_ERROR_TRANSPORT                = 0x00000002;
  public static final int KS_LIN_ERROR_BUS_COLLISION            = 0x00000003;
  public static final int KS_LIN_ERROR_PARITY                   = 0x00000004;
  public static final int KS_LIN_ERROR_CHECKSUM                 = 0x00000005;
  public static final int KS_LIN_ERROR_BREAK_EXPECTED           = 0x00000006;
  public static final int KS_LIN_ERROR_RESPONSE_TIMEOUT         = 0x00000007;
  public static final int KS_LIN_ERROR_PID_TIMEOUT              = 0x00000009;
  public static final int KS_LIN_ERROR_RESPONSE_TOO_SHORT       = 0x00000010;
  public static final int KS_LIN_ERROR_RECV_QUEUE_FULL          = 0x00000011;
  public static final int KS_LIN_ERROR_RESPONSE_WITHOUT_HEADER  = 0x00000012;

  //------ LIN events ------
  public static final int KS_LIN_ERROR                          = 0x00000000;
  public static final int KS_LIN_RECV_HEADER                    = 0x00000001;
  public static final int KS_LIN_RECV_RESPONSE                  = 0x00000002;

  //------ LIN data structures ------
  public static class KSLinProperties {
    public int structSize;
    public int linVersion;
    public int baudRate;
  }

  public static class KSLinState {
    public int structSize;
    public KSLinProperties properties;
    public int xmitHdrCount;
    public int xmitRspCount;
    public int recvCount;
    public int recvAvail;
    public int recvErrorCount;
    public int recvNoRspCount;
    public int collisionCount;
  }

  public static class KSLinHeader {
    public int identifier;
    public byte parity;
    public byte parityOk;
  }

  public static class KSLinResponse {
    public byte[] data = new byte[8];
    public int dataLen;
    public byte checksum;
    public byte checksumOk;
  }

  public static class KSLinMsg {
    public KSLinHeader header;
    public KSLinResponse response;
    public long timestamp;
  }

  //------ LIN Context structures ------
  public static class LinUserContext {
    public int ctxType;
    public Handle hLin;
  }

  public static class LinErrorUserContext {
    public int ctxType;
    public Handle hLin;
    public long timestamp;
    public int errorCode;
  }

  public static class LinHeaderUserContext {
    public int ctxType;
    public Handle hLin;
    public long timestamp;
    public KSLinHeader header;
  }

  public static class LinResponseUserContext {
    public int ctxType;
    public Handle hLin;
    public long timestamp;
    public KSLinMsg msg;
  }

  //------ Common functions ------
  public static native int KS_openLin(
    Handle phLin, string pDeviceName, int port, KSLinProperties pProperties, int flags);
  public static native int KS_openLinEx(
    Handle phLin, Handle hLinDevice, KSLinProperties pProperties, int flags);
  public static native int KS_closeLin(
    Handle hLin, int flags);
  public static native int KS_installLinHandler(
    Handle hLin, int eventCode, Handle hSignal, int flags);
  public static native int KS_execLinCommand(
    Handle hLin, int command, Object pParam, int flags);
  public static native int KS_xmitLinHeader(
    Handle hLin, int identifier, int flags);
  public static native int KS_xmitLinResponse(
    Handle hLin, byte[] pData, int size, int flags);
  public static native int KS_recvLinMsg(
    Handle hLin, KSLinMsg pLinMsg, int flags);
  public static native int KS_getLinState(
    Handle hLin, KSLinState pLinState, int flags);

}
