// Copyright (c) 1996-2016 by Kithara Software GmbH. All rights reserved.

//##############################################################################################################
//
// File:             KrtsDemo_dyn.cpp (v10.00d)
//
// Description:      C/C++ API for Kithara �RealTime Suite�
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// u.jes 1996-07-11  CREAT: Original - (file created)
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:01
//
//##############################################################################################################

//--------------------------------------------------------------------------------------------------------------
// KrtsDemo_dyn
//--------------------------------------------------------------------------------------------------------------

#define WIN32_LEAN_AND_MEAN
#define WIN32_EXTRA_LEAN
#define NO_STRICT

#include <windows.h>

#include "..\dev\KrtsDemo.h"

#if defined(_MSC_VER) && _MSC_VER >= 1310
#pragma auto_inline(off)
#endif

HMODULE _hKitharaKrtsDll = NULL;
const char* _kitharaKrtsDllToLoad = "KrtsDemo.dll";
static void _initModules();
static void _registerKernelAddress(char* pName, CallBackProc pProc);

//--------------------------------------------------------------------------------------------------------------
// Base Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openDriver ------
Error __stdcall KS_openDriver(const char* pCustNum) {
  typedef Error (__stdcall* KS_openDriverProc)(const char*);
  static KS_openDriverProc _pProc;

  if (!_pProc) {
    // first load the driver library
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll)
        return KSERROR_CANNOT_FIND_LIBRARY | 0x1000 | GetLastError();
    }

    // now query the procedure address
    _pProc = (KS_openDriverProc)GetProcAddress(_hKitharaKrtsDll, "KS_openDriver");
    if (!_pProc)
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
  }

  // if the function call fails, we close the driver
  Error error = (*_pProc)(pCustNum);
  if (error) {
    FreeLibrary(_hKitharaKrtsDll);
    _hKitharaKrtsDll = NULL;
    return error;
  }

  // now we register some functions for kernel execution (only available for 32 bit processes)
#ifndef _WIN64
  _initModules();
#endif

  return KS_OK;
}

//------ KS_closeDriver ------
Error __stdcall KS_closeDriver() {
  typedef Error (__stdcall* KS_closeDriverProc)();
  static KS_closeDriverProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeDriverProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeDriver");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeDriver not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  Error error = (*_pProc)();

  // WE DON'T RELEASE THE DRIVER LIBRARY, BECAUSE ALL PROCEDURE ADDRESSES WOULD BE INVALID!
  // FreeLibrary(_hKitharaKrtsDll);
  // _hKitharaKrtsDll = NULL;

  return error;
}

//------ KS_getDriverVersion ------
Error __stdcall KS_getDriverVersion(uint* pVersion) {
  typedef Error (__stdcall* KS_getDriverVersionProc)(uint*);
  static KS_getDriverVersionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getDriverVersionProc)GetProcAddress(_hKitharaKrtsDll, "KS_getDriverVersion");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getDriverVersion not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pVersion);
}

//------ KS_getErrorString ------
Error __stdcall KS_getErrorString(Error code, char** msg, uint language) {
  typedef Error (__stdcall* KS_getErrorStringProc)(Error, char**, uint);
  static KS_getErrorStringProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getErrorStringProc)GetProcAddress(_hKitharaKrtsDll, "KS_getErrorString");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getErrorString not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(code, msg, language);
}

//------ KS_getErrorStringEx ------
Error __stdcall KS_getErrorStringEx(Error code, char* pMsgBuf, uint language) {
  typedef Error (__stdcall* KS_getErrorStringExProc)(Error, char*, uint);
  static KS_getErrorStringExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getErrorStringExProc)GetProcAddress(_hKitharaKrtsDll, "KS_getErrorStringEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getErrorStringEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(code, pMsgBuf, language);
}

//------ KS_addErrorString ------
Error __stdcall KS_addErrorString(Error code, const char* msg, uint language) {
  typedef Error (__stdcall* KS_addErrorStringProc)(Error, const char*, uint);
  static KS_addErrorStringProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_addErrorStringProc)GetProcAddress(_hKitharaKrtsDll, "KS_addErrorString");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_addErrorString not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(code, msg, language);
}

//------ KS_getDriverConfig ------
Error __stdcall KS_getDriverConfig(const char* module, int configType, void* pData) {
  typedef Error (__stdcall* KS_getDriverConfigProc)(const char*, int, void*);
  static KS_getDriverConfigProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getDriverConfigProc)GetProcAddress(_hKitharaKrtsDll, "KS_getDriverConfig");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getDriverConfig not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(module, configType, pData);
}

//------ KS_setDriverConfig ------
Error __stdcall KS_setDriverConfig(const char* module, int configType, const void* pData) {
  typedef Error (__stdcall* KS_setDriverConfigProc)(const char*, int, const void*);
  static KS_setDriverConfigProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setDriverConfigProc)GetProcAddress(_hKitharaKrtsDll, "KS_setDriverConfig");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setDriverConfig not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(module, configType, pData);
}

//------ KS_getSystemInformation ------
Error __stdcall KS_getSystemInformation(KSSystemInformation* pSystemInfo, int flags) {
  typedef Error (__stdcall* KS_getSystemInformationProc)(KSSystemInformation*, int);
  static KS_getSystemInformationProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getSystemInformationProc)GetProcAddress(_hKitharaKrtsDll, "KS_getSystemInformation");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getSystemInformation not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pSystemInfo, flags);
}

//------ KS_getProcessorInformation ------
Error __stdcall KS_getProcessorInformation(int index, KSProcessorInformation* pProcessorInfo, int flags) {
  typedef Error (__stdcall* KS_getProcessorInformationProc)(int, KSProcessorInformation*, int);
  static KS_getProcessorInformationProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getProcessorInformationProc)GetProcAddress(_hKitharaKrtsDll, "KS_getProcessorInformation");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getProcessorInformation not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(index, pProcessorInfo, flags);
}

//------ KS_closeHandle ------
Error __stdcall KS_closeHandle(KSHandle handle, int flags) {
  typedef Error (__stdcall* KS_closeHandleProc)(KSHandle, int);
  static KS_closeHandleProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeHandleProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeHandle");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeHandle not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(handle, flags);
}

//------ KS_showMessage ------
Error __stdcall KS_showMessage(Error ksError, const char* msg) {
  typedef Error (__stdcall* KS_showMessageProc)(Error, const char*);
  static KS_showMessageProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_showMessageProc)GetProcAddress(_hKitharaKrtsDll, "KS_showMessage");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_showMessage not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ksError, msg);
}

//------ KS_logMessage ------
Error __stdcall KS_logMessage(int msgType, const char* msgText) {
  typedef Error (__stdcall* KS_logMessageProc)(int, const char*);
  static KS_logMessageProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_logMessageProc)GetProcAddress(_hKitharaKrtsDll, "KS_logMessage");
    if (!_pProc) {
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(msgType, msgText);
}

//------ KS_bufMessage ------
Error __stdcall KS_bufMessage(int msgType, const void* pBuffer, int length, const char* msgText) {
  typedef Error (__stdcall* KS_bufMessageProc)(int, const void*, int, const char*);
  static KS_bufMessageProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_bufMessageProc)GetProcAddress(_hKitharaKrtsDll, "KS_bufMessage");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_bufMessage not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(msgType, pBuffer, length, msgText);
}

//------ KS_dbgMessage ------
Error __stdcall KS_dbgMessage(const char* fileName, int line, const char* msgText) {
  typedef Error (__stdcall* KS_dbgMessageProc)(const char*, int, const char*);
  static KS_dbgMessageProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_dbgMessageProc)GetProcAddress(_hKitharaKrtsDll, "KS_dbgMessage");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_dbgMessage not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(fileName, line, msgText);
}

//------ KS_beep ------
Error __stdcall KS_beep(int freq, int duration) {
  typedef Error (__stdcall* KS_beepProc)(int, int);
  static KS_beepProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_beepProc)GetProcAddress(_hKitharaKrtsDll, "KS_beep");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_beep not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(freq, duration);
}

//------ KS_vdebugK ------
Error __stdcall KS_vdebugK(const char* fileName, int line, const char* format, void* pArgs) {
  typedef Error (__stdcall* KS_vdebugKProc)(const char*, int, const char*, void*);
  static KS_vdebugKProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_vdebugKProc)GetProcAddress(_hKitharaKrtsDll, "KS_vdebugK");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_vdebugK not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(fileName, line, format, pArgs);
}

//------ KS_vprintK ------
Error __stdcall KS_vprintK(const char* format, void* pArgs) {
  typedef Error (__stdcall* KS_vprintKProc)(const char*, void*);
  static KS_vprintKProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_vprintKProc)GetProcAddress(_hKitharaKrtsDll, "KS_vprintK");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_vprintK not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(format, pArgs);
}

//------ KS_throwException ------
Error __stdcall KS_throwException(int error, const char* pText, const char* pFile, int line) {
  typedef Error (__stdcall* KS_throwExceptionProc)(int, const char*, const char*, int);
  static KS_throwExceptionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_throwExceptionProc)GetProcAddress(_hKitharaKrtsDll, "KS_throwException");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_throwException not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(error, pText, pFile, line);
}

//------ KS_enumDevices ------
Error __stdcall KS_enumDevices(const char* deviceType, int index, char* pDeviceNameBuf, int flags) {
  typedef Error (__stdcall* KS_enumDevicesProc)(const char*, int, char*, int);
  static KS_enumDevicesProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumDevicesProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumDevices");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumDevices not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(deviceType, index, pDeviceNameBuf, flags);
}

//------ KS_enumDevicesEx ------
Error __stdcall KS_enumDevicesEx(const char* className, const char* busType, KSHandle hEnumerator, int index, char* pDeviceNameBuf, int flags) {
  typedef Error (__stdcall* KS_enumDevicesExProc)(const char*, const char*, KSHandle, int, char*, int);
  static KS_enumDevicesExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumDevicesExProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumDevicesEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumDevicesEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(className, busType, hEnumerator, index, pDeviceNameBuf, flags);
}

//------ KS_getDevices ------
Error __stdcall KS_getDevices(const char* className, const char* busType, KSHandle hEnumerator, int* pCount, void** pBuf, int size, int flags) {
  typedef Error (__stdcall* KS_getDevicesProc)(const char*, const char*, KSHandle, int*, void**, int, int);
  static KS_getDevicesProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getDevicesProc)GetProcAddress(_hKitharaKrtsDll, "KS_getDevices");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getDevices not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(className, busType, hEnumerator, pCount, pBuf, size, flags);
}

//------ KS_getDeviceInfo ------
Error __stdcall KS_getDeviceInfo(const char* deviceName, KSDeviceInfo* pDeviceInfo, int flags) {
  typedef Error (__stdcall* KS_getDeviceInfoProc)(const char*, KSDeviceInfo*, int);
  static KS_getDeviceInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getDeviceInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_getDeviceInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getDeviceInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(deviceName, pDeviceInfo, flags);
}

//------ KS_updateDriver ------
Error __stdcall KS_updateDriver(const char* deviceName, const char* fileName, int flags) {
  typedef Error (__stdcall* KS_updateDriverProc)(const char*, const char*, int);
  static KS_updateDriverProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_updateDriverProc)GetProcAddress(_hKitharaKrtsDll, "KS_updateDriver");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_updateDriver not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(deviceName, fileName, flags);
}

//------ KS_createThread ------
Error __stdcall KS_createThread(CallBackProc procName, void* pArgs, Win32Handle* phThread) {
  typedef Error (__stdcall* KS_createThreadProc)(CallBackProc, void*, Win32Handle*);
  static KS_createThreadProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createThreadProc)GetProcAddress(_hKitharaKrtsDll, "KS_createThread");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createThread not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(procName, pArgs, phThread);
}

//------ KS_removeThread ------
Error __stdcall KS_removeThread() {
  typedef Error (__stdcall* KS_removeThreadProc)();
  static KS_removeThreadProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeThreadProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeThread");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeThread not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)();
}

//------ KS_setThreadPrio ------
Error __stdcall KS_setThreadPrio(int prio) {
  typedef Error (__stdcall* KS_setThreadPrioProc)(int);
  static KS_setThreadPrioProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setThreadPrioProc)GetProcAddress(_hKitharaKrtsDll, "KS_setThreadPrio");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setThreadPrio not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(prio);
}

//------ KS_getThreadPrio ------
Error __stdcall KS_getThreadPrio(uint* pPrio) {
  typedef Error (__stdcall* KS_getThreadPrioProc)(uint*);
  static KS_getThreadPrioProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getThreadPrioProc)GetProcAddress(_hKitharaKrtsDll, "KS_getThreadPrio");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getThreadPrio not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pPrio);
}

//------ KS_createSharedMem ------
Error __stdcall KS_createSharedMem(void** ppAppPtr, void** ppSysPtr, const char* name, int size, int flags) {
  typedef Error (__stdcall* KS_createSharedMemProc)(void**, void**, const char*, int, int);
  static KS_createSharedMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createSharedMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_createSharedMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createSharedMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ppAppPtr, ppSysPtr, name, size, flags);
}

//------ KS_freeSharedMem ------
Error __stdcall KS_freeSharedMem(void* pAppPtr) {
  typedef Error (__stdcall* KS_freeSharedMemProc)(void*);
  static KS_freeSharedMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_freeSharedMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_freeSharedMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_freeSharedMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pAppPtr);
}

//------ KS_getSharedMem ------
Error __stdcall KS_getSharedMem(void** ppPtr, const char* name) {
  typedef Error (__stdcall* KS_getSharedMemProc)(void**, const char*);
  static KS_getSharedMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getSharedMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_getSharedMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getSharedMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ppPtr, name);
}

//------ KS_readMem ------
Error __stdcall KS_readMem(void* pBuffer, void* pAppPtr, int size, int offset) {
  typedef Error (__stdcall* KS_readMemProc)(void*, void*, int, int);
  static KS_readMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_readMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pBuffer, pAppPtr, size, offset);
}

//------ KS_writeMem ------
Error __stdcall KS_writeMem(const void* pBuffer, void* pAppPtr, int size, int offset) {
  typedef Error (__stdcall* KS_writeMemProc)(const void*, void*, int, int);
  static KS_writeMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_writeMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_writeMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_writeMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pBuffer, pAppPtr, size, offset);
}

//------ KS_createSharedMemEx ------
Error __stdcall KS_createSharedMemEx(KSHandle* phHandle, const char* name, int size, int flags) {
  typedef Error (__stdcall* KS_createSharedMemExProc)(KSHandle*, const char*, int, int);
  static KS_createSharedMemExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createSharedMemExProc)GetProcAddress(_hKitharaKrtsDll, "KS_createSharedMemEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createSharedMemEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phHandle, name, size, flags);
}

//------ KS_freeSharedMemEx ------
Error __stdcall KS_freeSharedMemEx(KSHandle hHandle, int flags) {
  typedef Error (__stdcall* KS_freeSharedMemExProc)(KSHandle, int);
  static KS_freeSharedMemExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_freeSharedMemExProc)GetProcAddress(_hKitharaKrtsDll, "KS_freeSharedMemEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_freeSharedMemEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hHandle, flags);
}

//------ KS_getSharedMemEx ------
Error __stdcall KS_getSharedMemEx(KSHandle hHandle, void** pPtr, int flags) {
  typedef Error (__stdcall* KS_getSharedMemExProc)(KSHandle, void**, int);
  static KS_getSharedMemExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getSharedMemExProc)GetProcAddress(_hKitharaKrtsDll, "KS_getSharedMemEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getSharedMemEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hHandle, pPtr, flags);
}

//------ KS_createEvent ------
Error __stdcall KS_createEvent(KSHandle* phEvent, const char* name, int flags) {
  typedef Error (__stdcall* KS_createEventProc)(KSHandle*, const char*, int);
  static KS_createEventProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createEventProc)GetProcAddress(_hKitharaKrtsDll, "KS_createEvent");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createEvent not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phEvent, name, flags);
}

//------ KS_closeEvent ------
Error __stdcall KS_closeEvent(KSHandle hEvent) {
  typedef Error (__stdcall* KS_closeEventProc)(KSHandle);
  static KS_closeEventProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeEventProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeEvent");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeEvent not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hEvent);
}

//------ KS_setEvent ------
Error __stdcall KS_setEvent(KSHandle hEvent) {
  typedef Error (__stdcall* KS_setEventProc)(KSHandle);
  static KS_setEventProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setEventProc)GetProcAddress(_hKitharaKrtsDll, "KS_setEvent");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setEvent not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hEvent);
}

//------ KS_resetEvent ------
Error __stdcall KS_resetEvent(KSHandle hEvent) {
  typedef Error (__stdcall* KS_resetEventProc)(KSHandle);
  static KS_resetEventProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_resetEventProc)GetProcAddress(_hKitharaKrtsDll, "KS_resetEvent");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_resetEvent not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hEvent);
}

//------ KS_pulseEvent ------
Error __stdcall KS_pulseEvent(KSHandle hEvent) {
  typedef Error (__stdcall* KS_pulseEventProc)(KSHandle);
  static KS_pulseEventProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_pulseEventProc)GetProcAddress(_hKitharaKrtsDll, "KS_pulseEvent");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_pulseEvent not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hEvent);
}

//------ KS_waitForEvent ------
Error __stdcall KS_waitForEvent(KSHandle hEvent, int flags, int timeout) {
  typedef Error (__stdcall* KS_waitForEventProc)(KSHandle, int, int);
  static KS_waitForEventProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_waitForEventProc)GetProcAddress(_hKitharaKrtsDll, "KS_waitForEvent");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_waitForEvent not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hEvent, flags, timeout);
}

//------ KS_getEventState ------
Error __stdcall KS_getEventState(KSHandle hEvent, uint* pState) {
  typedef Error (__stdcall* KS_getEventStateProc)(KSHandle, uint*);
  static KS_getEventStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getEventStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getEventState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getEventState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hEvent, pState);
}

//------ KS_postMessage ------
Error __stdcall KS_postMessage(Win32Handle hWnd, uint msg, uint wP, uint lP) {
  typedef Error (__stdcall* KS_postMessageProc)(Win32Handle, uint, uint, uint);
  static KS_postMessageProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_postMessageProc)GetProcAddress(_hKitharaKrtsDll, "KS_postMessage");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_postMessage not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hWnd, msg, wP, lP);
}

//------ KS_createCallBack ------
Error __stdcall KS_createCallBack(KSHandle* phCallBack, CallBackRoutine routine, void* pArgs, int flags, int prio) {
  typedef Error (__stdcall* KS_createCallBackProc)(KSHandle*, CallBackRoutine, void*, int, int);
  static KS_createCallBackProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createCallBackProc)GetProcAddress(_hKitharaKrtsDll, "KS_createCallBack");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createCallBack not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phCallBack, routine, pArgs, flags, prio);
}

//------ KS_removeCallBack ------
Error __stdcall KS_removeCallBack(KSHandle hCallBack) {
  typedef Error (__stdcall* KS_removeCallBackProc)(KSHandle);
  static KS_removeCallBackProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeCallBackProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeCallBack");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeCallBack not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCallBack);
}

//------ KS_execCallBack ------
Error __stdcall KS_execCallBack(KSHandle hCallBack, void* pContext, int flags) {
  typedef Error (__stdcall* KS_execCallBackProc)(KSHandle, void*, int);
  static KS_execCallBackProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execCallBackProc)GetProcAddress(_hKitharaKrtsDll, "KS_execCallBack");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execCallBack not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCallBack, pContext, flags);
}

//------ KS_getCallState ------
Error __stdcall KS_getCallState(KSHandle hSignal, HandlerState* pState) {
  typedef Error (__stdcall* KS_getCallStateProc)(KSHandle, HandlerState*);
  static KS_getCallStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getCallStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getCallState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getCallState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSignal, pState);
}

//------ KS_signalObject ------
Error __stdcall KS_signalObject(KSHandle hObject, void* pContext, int flags) {
  typedef Error (__stdcall* KS_signalObjectProc)(KSHandle, void*, int);
  static KS_signalObjectProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_signalObjectProc)GetProcAddress(_hKitharaKrtsDll, "KS_signalObject");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_signalObject not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, pContext, flags);
}

//------ KS_createPipe ------
Error __stdcall KS_createPipe(KSHandle* phPipe, const char* name, int itemSize, int itemCount, KSHandle hNotify, int flags) {
  typedef Error (__stdcall* KS_createPipeProc)(KSHandle*, const char*, int, int, KSHandle, int);
  static KS_createPipeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createPipeProc)GetProcAddress(_hKitharaKrtsDll, "KS_createPipe");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createPipe not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phPipe, name, itemSize, itemCount, hNotify, flags);
}

//------ KS_removePipe ------
Error __stdcall KS_removePipe(KSHandle hPipe) {
  typedef Error (__stdcall* KS_removePipeProc)(KSHandle);
  static KS_removePipeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removePipeProc)GetProcAddress(_hKitharaKrtsDll, "KS_removePipe");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removePipe not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPipe);
}

//------ KS_flushPipe ------
Error __stdcall KS_flushPipe(KSHandle hPipe, int flags) {
  typedef Error (__stdcall* KS_flushPipeProc)(KSHandle, int);
  static KS_flushPipeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_flushPipeProc)GetProcAddress(_hKitharaKrtsDll, "KS_flushPipe");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_flushPipe not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPipe, flags);
}

//------ KS_getPipe ------
Error __stdcall KS_getPipe(KSHandle hPipe, void* pBuffer, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_getPipeProc)(KSHandle, void*, int, int*, int);
  static KS_getPipeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getPipeProc)GetProcAddress(_hKitharaKrtsDll, "KS_getPipe");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getPipe not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPipe, pBuffer, length, pLength, flags);
}

//------ KS_putPipe ------
Error __stdcall KS_putPipe(KSHandle hPipe, const void* pBuffer, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_putPipeProc)(KSHandle, const void*, int, int*, int);
  static KS_putPipeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_putPipeProc)GetProcAddress(_hKitharaKrtsDll, "KS_putPipe");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_putPipe not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPipe, pBuffer, length, pLength, flags);
}

//------ KS_createQuickMutex ------
Error __stdcall KS_createQuickMutex(KSHandle* phMutex, int level, int flags) {
  typedef Error (__stdcall* KS_createQuickMutexProc)(KSHandle*, int, int);
  static KS_createQuickMutexProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createQuickMutexProc)GetProcAddress(_hKitharaKrtsDll, "KS_createQuickMutex");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createQuickMutex not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phMutex, level, flags);
}

//------ KS_removeQuickMutex ------
Error __stdcall KS_removeQuickMutex(KSHandle hMutex) {
  typedef Error (__stdcall* KS_removeQuickMutexProc)(KSHandle);
  static KS_removeQuickMutexProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeQuickMutexProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeQuickMutex");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeQuickMutex not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMutex);
}

//------ KS_requestQuickMutex ------
Error __stdcall KS_requestQuickMutex(KSHandle hMutex) {
  typedef Error (__stdcall* KS_requestQuickMutexProc)(KSHandle);
  static KS_requestQuickMutexProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_requestQuickMutexProc)GetProcAddress(_hKitharaKrtsDll, "KS_requestQuickMutex");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_requestQuickMutex not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMutex);
}

//------ KS_releaseQuickMutex ------
Error __stdcall KS_releaseQuickMutex(KSHandle hMutex) {
  typedef Error (__stdcall* KS_releaseQuickMutexProc)(KSHandle);
  static KS_releaseQuickMutexProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_releaseQuickMutexProc)GetProcAddress(_hKitharaKrtsDll, "KS_releaseQuickMutex");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_releaseQuickMutex not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMutex);
}

//------ _initBaseModule ------
static void _initBaseModule() {
  _registerKernelAddress("KS_getDriverConfig", (CallBackProc)KS_getDriverConfig);
  _registerKernelAddress("KS_setDriverConfig", (CallBackProc)KS_setDriverConfig);
  _registerKernelAddress("KS_getProcessorInformation", (CallBackProc)KS_getProcessorInformation);
  _registerKernelAddress("KS_closeHandle", (CallBackProc)KS_closeHandle);
  _registerKernelAddress("KS_showMessage", (CallBackProc)KS_showMessage);
  _registerKernelAddress("KS_logMessage", (CallBackProc)KS_logMessage);
  _registerKernelAddress("KS_bufMessage", (CallBackProc)KS_bufMessage);
  _registerKernelAddress("KS_dbgMessage", (CallBackProc)KS_dbgMessage);
  _registerKernelAddress("KS_beep", (CallBackProc)KS_beep);
  _registerKernelAddress("KS_vdebugK", (CallBackProc)KS_vdebugK);
  _registerKernelAddress("KS_vprintK", (CallBackProc)KS_vprintK);
  _registerKernelAddress("KS_throwException", (CallBackProc)KS_throwException);
  _registerKernelAddress("KS_getSharedMem", (CallBackProc)KS_getSharedMem);
  _registerKernelAddress("KS_readMem", (CallBackProc)KS_readMem);
  _registerKernelAddress("KS_createSharedMemEx", (CallBackProc)KS_createSharedMemEx);
  _registerKernelAddress("KS_getSharedMemEx", (CallBackProc)KS_getSharedMemEx);
  _registerKernelAddress("KS_setEvent", (CallBackProc)KS_setEvent);
  _registerKernelAddress("KS_resetEvent", (CallBackProc)KS_resetEvent);
  _registerKernelAddress("KS_pulseEvent", (CallBackProc)KS_pulseEvent);
  _registerKernelAddress("KS_waitForEvent", (CallBackProc)KS_waitForEvent);
  _registerKernelAddress("KS_postMessage", (CallBackProc)KS_postMessage);
  _registerKernelAddress("KS_createCallBack", (CallBackProc)KS_createCallBack);
  _registerKernelAddress("KS_removeCallBack", (CallBackProc)KS_removeCallBack);
  _registerKernelAddress("KS_execCallBack", (CallBackProc)KS_execCallBack);
  _registerKernelAddress("KS_signalObject", (CallBackProc)KS_signalObject);
  _registerKernelAddress("KS_flushPipe", (CallBackProc)KS_flushPipe);
  _registerKernelAddress("KS_getPipe", (CallBackProc)KS_getPipe);
  _registerKernelAddress("KS_putPipe", (CallBackProc)KS_putPipe);
  _registerKernelAddress("KS_createQuickMutex", (CallBackProc)KS_createQuickMutex);
  _registerKernelAddress("KS_removeQuickMutex", (CallBackProc)KS_removeQuickMutex);
  _registerKernelAddress("KS_requestQuickMutex", (CallBackProc)KS_requestQuickMutex);
  _registerKernelAddress("KS_releaseQuickMutex", (CallBackProc)KS_releaseQuickMutex);
}

#define KS_BASE_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Kernel Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_prepareKernelExec ------
Error __stdcall KS_prepareKernelExec(CallBackProc callback, CallBackProc* pRelocated, int flags) {
  typedef Error (__stdcall* KS_prepareKernelExecProc)(CallBackProc, CallBackProc*, int);
  static KS_prepareKernelExecProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_prepareKernelExecProc)GetProcAddress(_hKitharaKrtsDll, "KS_prepareKernelExec");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_prepareKernelExec not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(callback, pRelocated, flags);
}

//------ KS_endKernelExec ------
Error __stdcall KS_endKernelExec(CallBackProc relocated) {
  typedef Error (__stdcall* KS_endKernelExecProc)(CallBackProc);
  static KS_endKernelExecProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_endKernelExecProc)GetProcAddress(_hKitharaKrtsDll, "KS_endKernelExec");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_endKernelExec not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(relocated);
}

//------ KS_testKernelExec ------
Error __stdcall KS_testKernelExec(CallBackProc relocated, void* pArgs) {
  typedef Error (__stdcall* KS_testKernelExecProc)(CallBackProc, void*);
  static KS_testKernelExecProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_testKernelExecProc)GetProcAddress(_hKitharaKrtsDll, "KS_testKernelExec");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_testKernelExec not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(relocated, pArgs);
}

//------ KS_registerKernelAddress ------
Error __stdcall KS_registerKernelAddress(const char* name, CallBackProc appFunction, CallBackProc sysFunction, int flags) {
  typedef Error (__stdcall* KS_registerKernelAddressProc)(const char*, CallBackProc, CallBackProc, int);
  static KS_registerKernelAddressProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_registerKernelAddressProc)GetProcAddress(_hKitharaKrtsDll, "KS_registerKernelAddress");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_registerKernelAddress not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(name, appFunction, sysFunction, flags);
}

//------ KS_loadKernel ------
Error __stdcall KS_loadKernel(KSHandle* phKernel, const char* dllName, const char* initProcName, void* pArgs, int flags) {
  typedef Error (__stdcall* KS_loadKernelProc)(KSHandle*, const char*, const char*, void*, int);
  static KS_loadKernelProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_loadKernelProc)GetProcAddress(_hKitharaKrtsDll, "KS_loadKernel");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_loadKernel not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phKernel, dllName, initProcName, pArgs, flags);
}

//------ KS_loadKernelFromBuffer ------
Error __stdcall KS_loadKernelFromBuffer(KSHandle* phKernel, const char* dllName, const void* pBuffer, int length, const char* initProcName, void* pArgs, int flags) {
  typedef Error (__stdcall* KS_loadKernelFromBufferProc)(KSHandle*, const char*, const void*, int, const char*, void*, int);
  static KS_loadKernelFromBufferProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_loadKernelFromBufferProc)GetProcAddress(_hKitharaKrtsDll, "KS_loadKernelFromBuffer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_loadKernelFromBuffer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phKernel, dllName, pBuffer, length, initProcName, pArgs, flags);
}

//------ KS_freeKernel ------
Error __stdcall KS_freeKernel(KSHandle hKernel) {
  typedef Error (__stdcall* KS_freeKernelProc)(KSHandle);
  static KS_freeKernelProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_freeKernelProc)GetProcAddress(_hKitharaKrtsDll, "KS_freeKernel");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_freeKernel not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hKernel);
}

//------ KS_execKernelFunction ------
Error __stdcall KS_execKernelFunction(KSHandle hKernel, const char* name, void* pArgs, void* pContext, int flags) {
  typedef Error (__stdcall* KS_execKernelFunctionProc)(KSHandle, const char*, void*, void*, int);
  static KS_execKernelFunctionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execKernelFunctionProc)GetProcAddress(_hKitharaKrtsDll, "KS_execKernelFunction");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execKernelFunction not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hKernel, name, pArgs, pContext, flags);
}

//------ KS_getKernelFunction ------
Error __stdcall KS_getKernelFunction(KSHandle hKernel, const char* name, CallBackProc* pRelocated, int flags) {
  typedef Error (__stdcall* KS_getKernelFunctionProc)(KSHandle, const char*, CallBackProc*, int);
  static KS_getKernelFunctionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getKernelFunctionProc)GetProcAddress(_hKitharaKrtsDll, "KS_getKernelFunction");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getKernelFunction not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hKernel, name, pRelocated, flags);
}

//------ KS_enumKernelFunctions ------
Error __stdcall KS_enumKernelFunctions(KSHandle hKernel, int index, char* pName, int flags) {
  typedef Error (__stdcall* KS_enumKernelFunctionsProc)(KSHandle, int, char*, int);
  static KS_enumKernelFunctionsProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumKernelFunctionsProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumKernelFunctions");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumKernelFunctions not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hKernel, index, pName, flags);
}

//------ KS_execKernelCommand ------
Error __stdcall KS_execKernelCommand(KSHandle hKernel, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execKernelCommandProc)(KSHandle, int, void*, int);
  static KS_execKernelCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execKernelCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execKernelCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execKernelCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hKernel, command, pParam, flags);
}

//------ KS_createKernelCallBack ------
Error __stdcall KS_createKernelCallBack(KSHandle* phCallBack, KSHandle hKernel, const char* name, void* pArgs, int flags, int prio) {
  typedef Error (__stdcall* KS_createKernelCallBackProc)(KSHandle*, KSHandle, const char*, void*, int, int);
  static KS_createKernelCallBackProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createKernelCallBackProc)GetProcAddress(_hKitharaKrtsDll, "KS_createKernelCallBack");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createKernelCallBack not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phCallBack, hKernel, name, pArgs, flags, prio);
}

//------ KS_execSyncFunction ------
Error __stdcall KS_execSyncFunction(KSHandle hHandler, CallBackProc proc, void* pArgs, int flags) {
  typedef Error (__stdcall* KS_execSyncFunctionProc)(KSHandle, CallBackProc, void*, int);
  static KS_execSyncFunctionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execSyncFunctionProc)GetProcAddress(_hKitharaKrtsDll, "KS_execSyncFunction");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execSyncFunction not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hHandler, proc, pArgs, flags);
}

//------ KS_execSystemCall ------
Error __stdcall KS_execSystemCall(const char* fileName, const char* funcName, void* pData, int flags) {
  typedef Error (__stdcall* KS_execSystemCallProc)(const char*, const char*, void*, int);
  static KS_execSystemCallProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execSystemCallProc)GetProcAddress(_hKitharaKrtsDll, "KS_execSystemCall");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execSystemCall not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(fileName, funcName, pData, flags);
}

//------ KS_memCpy ------
Error __stdcall KS_memCpy(void* pDst, const void* pSrc, int size, int flags) {
  typedef Error (__stdcall* KS_memCpyProc)(void*, const void*, int, int);
  static KS_memCpyProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_memCpyProc)GetProcAddress(_hKitharaKrtsDll, "KS_memCpy");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_memCpy not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pDst, pSrc, size, flags);
}

//------ KS_memMove ------
Error __stdcall KS_memMove(void* pDst, const void* pSrc, int size, int flags) {
  typedef Error (__stdcall* KS_memMoveProc)(void*, const void*, int, int);
  static KS_memMoveProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_memMoveProc)GetProcAddress(_hKitharaKrtsDll, "KS_memMove");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_memMove not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pDst, pSrc, size, flags);
}

//------ KS_memSet ------
Error __stdcall KS_memSet(void* pMem, int value, int size, int flags) {
  typedef Error (__stdcall* KS_memSetProc)(void*, int, int, int);
  static KS_memSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_memSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_memSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_memSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pMem, value, size, flags);
}

//------ KS_malloc ------
Error __stdcall KS_malloc(void** ppAllocated, int bytes, int flags) {
  typedef Error (__stdcall* KS_mallocProc)(void**, int, int);
  static KS_mallocProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_mallocProc)GetProcAddress(_hKitharaKrtsDll, "KS_malloc");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_malloc not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ppAllocated, bytes, flags);
}

//------ KS_free ------
Error __stdcall KS_free(void* pAllocated, int flags) {
  typedef Error (__stdcall* KS_freeProc)(void*, int);
  static KS_freeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_freeProc)GetProcAddress(_hKitharaKrtsDll, "KS_free");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_free not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pAllocated, flags);
}

//------ KS_interlocked ------
Error __stdcall KS_interlocked(void* pInterlocked, int operation, void* pParameter1, void* pParameter2, int flags) {
  typedef Error (__stdcall* KS_interlockedProc)(void*, int, void*, void*, int);
  static KS_interlockedProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_interlockedProc)GetProcAddress(_hKitharaKrtsDll, "KS_interlocked");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_interlocked not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pInterlocked, operation, pParameter1, pParameter2, flags);
}

//------ _initKernelModule ------
static void _initKernelModule() {
  _registerKernelAddress("KS_loadKernel", (CallBackProc)KS_loadKernel);
  _registerKernelAddress("KS_execKernelFunction", (CallBackProc)KS_execKernelFunction);
  _registerKernelAddress("KS_getKernelFunction", (CallBackProc)KS_getKernelFunction);
  _registerKernelAddress("KS_enumKernelFunctions", (CallBackProc)KS_enumKernelFunctions);
  _registerKernelAddress("KS_createKernelCallBack", (CallBackProc)KS_createKernelCallBack);
  _registerKernelAddress("KS_execSyncFunction", (CallBackProc)KS_execSyncFunction);
  _registerKernelAddress("KS_execSystemCall", (CallBackProc)KS_execSystemCall);
  _registerKernelAddress("KS_memCpy", (CallBackProc)KS_memCpy);
  _registerKernelAddress("KS_memMove", (CallBackProc)KS_memMove);
  _registerKernelAddress("KS_memSet", (CallBackProc)KS_memSet);
  _registerKernelAddress("KS_malloc", (CallBackProc)KS_malloc);
  _registerKernelAddress("KS_free", (CallBackProc)KS_free);
  _registerKernelAddress("KS_interlocked", (CallBackProc)KS_interlocked);
}

#define KS_KERNEL_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// IoPort Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_enableIoRange ------
Error __stdcall KS_enableIoRange(uint ioPortBase, int count, int flags) {
  typedef Error (__stdcall* KS_enableIoRangeProc)(uint, int, int);
  static KS_enableIoRangeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enableIoRangeProc)GetProcAddress(_hKitharaKrtsDll, "KS_enableIoRange");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enableIoRange not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ioPortBase, count, flags);
}

//------ KS_readIoPort ------
Error __stdcall KS_readIoPort(uint ioPort, uint* pValue, int flags) {
  typedef Error (__stdcall* KS_readIoPortProc)(uint, uint*, int);
  static KS_readIoPortProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readIoPortProc)GetProcAddress(_hKitharaKrtsDll, "KS_readIoPort");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readIoPort not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ioPort, pValue, flags);
}

//------ KS_writeIoPort ------
Error __stdcall KS_writeIoPort(uint ioPort, uint value, int flags) {
  typedef Error (__stdcall* KS_writeIoPortProc)(uint, uint, int);
  static KS_writeIoPortProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_writeIoPortProc)GetProcAddress(_hKitharaKrtsDll, "KS_writeIoPort");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_writeIoPort not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ioPort, value, flags);
}

//------ KS_inpb ------
uint __stdcall KS_inpb(uint ioPort) {
  typedef uint (__stdcall* KS_inpbProc)(uint);
  static KS_inpbProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KS_inpbProc)GetProcAddress(_hKitharaKrtsDll, "KS_inpb");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_inpb not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(ioPort);
}

//------ KS_inpw ------
uint __stdcall KS_inpw(uint ioPort) {
  typedef uint (__stdcall* KS_inpwProc)(uint);
  static KS_inpwProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KS_inpwProc)GetProcAddress(_hKitharaKrtsDll, "KS_inpw");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_inpw not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(ioPort);
}

//------ KS_inpd ------
uint __stdcall KS_inpd(uint ioPort) {
  typedef uint (__stdcall* KS_inpdProc)(uint);
  static KS_inpdProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KS_inpdProc)GetProcAddress(_hKitharaKrtsDll, "KS_inpd");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_inpd not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(ioPort);
}

//------ KS_outpb ------
void __stdcall KS_outpb(uint ioPort, uint value) {
  typedef void (__stdcall* KS_outpbProc)(uint, uint);
  static KS_outpbProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return;      }
    }

    _pProc = (KS_outpbProc)GetProcAddress(_hKitharaKrtsDll, "KS_outpb");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_outpb not found in DLL");
      return;    }
  }

  return (*_pProc)(ioPort, value);
}

//------ KS_outpw ------
void __stdcall KS_outpw(uint ioPort, uint value) {
  typedef void (__stdcall* KS_outpwProc)(uint, uint);
  static KS_outpwProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return;      }
    }

    _pProc = (KS_outpwProc)GetProcAddress(_hKitharaKrtsDll, "KS_outpw");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_outpw not found in DLL");
      return;    }
  }

  return (*_pProc)(ioPort, value);
}

//------ KS_outpd ------
void __stdcall KS_outpd(uint ioPort, uint value) {
  typedef void (__stdcall* KS_outpdProc)(uint, uint);
  static KS_outpdProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return;      }
    }

    _pProc = (KS_outpdProc)GetProcAddress(_hKitharaKrtsDll, "KS_outpd");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_outpd not found in DLL");
      return;    }
  }

  return (*_pProc)(ioPort, value);
}

//------ KS_getBusData ------
Error __stdcall KS_getBusData(KSPciBusData* pBusData, int busNumber, int slotNumber, int flags) {
  typedef Error (__stdcall* KS_getBusDataProc)(KSPciBusData*, int, int, int);
  static KS_getBusDataProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getBusDataProc)GetProcAddress(_hKitharaKrtsDll, "KS_getBusData");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getBusData not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pBusData, busNumber, slotNumber, flags);
}

//------ KS_setBusData ------
Error __stdcall KS_setBusData(KSPciBusData* pBusData, int busNumber, int slotNumber, int flags) {
  typedef Error (__stdcall* KS_setBusDataProc)(KSPciBusData*, int, int, int);
  static KS_setBusDataProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setBusDataProc)GetProcAddress(_hKitharaKrtsDll, "KS_setBusData");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setBusData not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pBusData, busNumber, slotNumber, flags);
}

//------ KS_getResourceInfo ------
Error __stdcall KS_getResourceInfo(const char* name, KSResourceInfo* pInfo) {
  typedef Error (__stdcall* KS_getResourceInfoProc)(const char*, KSResourceInfo*);
  static KS_getResourceInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getResourceInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_getResourceInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getResourceInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(name, pInfo);
}

//------ KS_getResourceInfoEx ------
Error __stdcall KS_getResourceInfoEx(const char* name, KSResourceInfoEx* pInfo) {
  typedef Error (__stdcall* KS_getResourceInfoExProc)(const char*, KSResourceInfoEx*);
  static KS_getResourceInfoExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getResourceInfoExProc)GetProcAddress(_hKitharaKrtsDll, "KS_getResourceInfoEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getResourceInfoEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(name, pInfo);
}

//------ KS_enterQuietSection ------
Error __stdcall KS_enterQuietSection(int flags) {
  typedef Error (__stdcall* KS_enterQuietSectionProc)(int);
  static KS_enterQuietSectionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enterQuietSectionProc)GetProcAddress(_hKitharaKrtsDll, "KS_enterQuietSection");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enterQuietSection not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(flags);
}

//------ KS_releaseQuietSection ------
Error __stdcall KS_releaseQuietSection(int flags) {
  typedef Error (__stdcall* KS_releaseQuietSectionProc)(int);
  static KS_releaseQuietSectionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_releaseQuietSectionProc)GetProcAddress(_hKitharaKrtsDll, "KS_releaseQuietSection");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_releaseQuietSection not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(flags);
}

//------ _initIoPortModule ------
static void _initIoPortModule() {
  _registerKernelAddress("KS_enableIoRange", (CallBackProc)KS_enableIoRange);
  _registerKernelAddress("KS_readIoPort", (CallBackProc)KS_readIoPort);
  _registerKernelAddress("KS_writeIoPort", (CallBackProc)KS_writeIoPort);
  _registerKernelAddress("KS_inpb", (CallBackProc)KS_inpb);
  _registerKernelAddress("KS_inpw", (CallBackProc)KS_inpw);
  _registerKernelAddress("KS_inpd", (CallBackProc)KS_inpd);
  _registerKernelAddress("KS_outpb", (CallBackProc)KS_outpb);
  _registerKernelAddress("KS_outpw", (CallBackProc)KS_outpw);
  _registerKernelAddress("KS_outpd", (CallBackProc)KS_outpd);
}

#define KS_IOPORT_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Memory Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_mapDeviceMem ------
Error __stdcall KS_mapDeviceMem(void** ppAppPtr, void** ppSysPtr, KSResourceInfoEx* pInfo, int index, int flags) {
  typedef Error (__stdcall* KS_mapDeviceMemProc)(void**, void**, KSResourceInfoEx*, int, int);
  static KS_mapDeviceMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_mapDeviceMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_mapDeviceMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_mapDeviceMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ppAppPtr, ppSysPtr, pInfo, index, flags);
}

//------ KS_mapPhysMem ------
Error __stdcall KS_mapPhysMem(void** ppAppPtr, void** ppSysPtr, uint physAddr, int size, int flags) {
  typedef Error (__stdcall* KS_mapPhysMemProc)(void**, void**, uint, int, int);
  static KS_mapPhysMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_mapPhysMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_mapPhysMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_mapPhysMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ppAppPtr, ppSysPtr, physAddr, size, flags);
}

//------ KS_unmapPhysMem ------
Error __stdcall KS_unmapPhysMem(void* pAppPtr) {
  typedef Error (__stdcall* KS_unmapPhysMemProc)(void*);
  static KS_unmapPhysMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_unmapPhysMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_unmapPhysMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_unmapPhysMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pAppPtr);
}

//------ KS_allocPhysMem ------
Error __stdcall KS_allocPhysMem(void** ppAppPtr, void** ppSysPtr, uint* physAddr, int size, int flags) {
  typedef Error (__stdcall* KS_allocPhysMemProc)(void**, void**, uint*, int, int);
  static KS_allocPhysMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_allocPhysMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_allocPhysMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_allocPhysMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(ppAppPtr, ppSysPtr, physAddr, size, flags);
}

//------ KS_freePhysMem ------
Error __stdcall KS_freePhysMem(void* pAppPtr) {
  typedef Error (__stdcall* KS_freePhysMemProc)(void*);
  static KS_freePhysMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_freePhysMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_freePhysMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_freePhysMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pAppPtr);
}

//------ KS_copyPhysMem ------
Error __stdcall KS_copyPhysMem(uint physAddr, void* pBuffer, uint size, uint offset, int flags) {
  typedef Error (__stdcall* KS_copyPhysMemProc)(uint, void*, uint, uint, int);
  static KS_copyPhysMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_copyPhysMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_copyPhysMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_copyPhysMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(physAddr, pBuffer, size, offset, flags);
}

//------ KS_getPhysAddr ------
Error __stdcall KS_getPhysAddr(void** pPhysAddr, void* pDosLinAddr) {
  typedef Error (__stdcall* KS_getPhysAddrProc)(void**, void*);
  static KS_getPhysAddrProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getPhysAddrProc)GetProcAddress(_hKitharaKrtsDll, "KS_getPhysAddr");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getPhysAddr not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pPhysAddr, pDosLinAddr);
}

//------ _initMemoryModule ------
static void _initMemoryModule() {
  _registerKernelAddress("KS_allocPhysMem", (CallBackProc)KS_allocPhysMem);
}

#define KS_MEMORY_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Clock Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_calibrateMachineTime ------
Error __stdcall KS_calibrateMachineTime(uint* pFrequency, int flags) {
  typedef Error (__stdcall* KS_calibrateMachineTimeProc)(uint*, int);
  static KS_calibrateMachineTimeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_calibrateMachineTimeProc)GetProcAddress(_hKitharaKrtsDll, "KS_calibrateMachineTime");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_calibrateMachineTime not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pFrequency, flags);
}

//------ KS_getSystemTicks ------
Error __stdcall KS_getSystemTicks(UInt64* pSystemTicks) {
  typedef Error (__stdcall* KS_getSystemTicksProc)(UInt64*);
  static KS_getSystemTicksProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getSystemTicksProc)GetProcAddress(_hKitharaKrtsDll, "KS_getSystemTicks");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getSystemTicks not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pSystemTicks);
}

//------ KS_getSystemTime ------
Error __stdcall KS_getSystemTime(UInt64* pSystemTime) {
  typedef Error (__stdcall* KS_getSystemTimeProc)(UInt64*);
  static KS_getSystemTimeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getSystemTimeProc)GetProcAddress(_hKitharaKrtsDll, "KS_getSystemTime");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getSystemTime not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pSystemTime);
}

//------ KS_getMachineTime ------
Error __stdcall KS_getMachineTime(UInt64* pMachineTime) {
  typedef Error (__stdcall* KS_getMachineTimeProc)(UInt64*);
  static KS_getMachineTimeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getMachineTimeProc)GetProcAddress(_hKitharaKrtsDll, "KS_getMachineTime");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getMachineTime not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pMachineTime);
}

//------ KS_getAbsTime ------
Error __stdcall KS_getAbsTime(UInt64* pAbsTime) {
  typedef Error (__stdcall* KS_getAbsTimeProc)(UInt64*);
  static KS_getAbsTimeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getAbsTimeProc)GetProcAddress(_hKitharaKrtsDll, "KS_getAbsTime");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getAbsTime not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pAbsTime);
}

//------ KS_getClock ------
Error __stdcall KS_getClock(UInt64* pClock, int clockId) {
  typedef Error (__stdcall* KS_getClockProc)(UInt64*, int);
  static KS_getClockProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getClockProc)GetProcAddress(_hKitharaKrtsDll, "KS_getClock");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getClock not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pClock, clockId);
}

//------ KS_getClockSource ------
Error __stdcall KS_getClockSource(KSClockSource* pClockSource, int clockId) {
  typedef Error (__stdcall* KS_getClockSourceProc)(KSClockSource*, int);
  static KS_getClockSourceProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getClockSourceProc)GetProcAddress(_hKitharaKrtsDll, "KS_getClockSource");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getClockSource not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pClockSource, clockId);
}

//------ KS_setClockSource ------
Error __stdcall KS_setClockSource(const KSClockSource* pClockSource, int clockId) {
  typedef Error (__stdcall* KS_setClockSourceProc)(const KSClockSource*, int);
  static KS_setClockSourceProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setClockSourceProc)GetProcAddress(_hKitharaKrtsDll, "KS_setClockSource");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setClockSource not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pClockSource, clockId);
}

//------ KS_microDelay ------
Error __stdcall KS_microDelay(int delay) {
  typedef Error (__stdcall* KS_microDelayProc)(int);
  static KS_microDelayProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_microDelayProc)GetProcAddress(_hKitharaKrtsDll, "KS_microDelay");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_microDelay not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(delay);
}

//------ KS_convertClock ------
Error __stdcall KS_convertClock(UInt64* pValue, int clockIdFrom, int clockIdTo, int flags) {
  typedef Error (__stdcall* KS_convertClockProc)(UInt64*, int, int, int);
  static KS_convertClockProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_convertClockProc)GetProcAddress(_hKitharaKrtsDll, "KS_convertClock");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_convertClock not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pValue, clockIdFrom, clockIdTo, flags);
}

//------ _initClockModule ------
static void _initClockModule() {
  _registerKernelAddress("KS_calibrateMachineTime", (CallBackProc)KS_calibrateMachineTime);
  _registerKernelAddress("KS_getSystemTicks", (CallBackProc)KS_getSystemTicks);
  _registerKernelAddress("KS_getSystemTime", (CallBackProc)KS_getSystemTime);
  _registerKernelAddress("KS_getMachineTime", (CallBackProc)KS_getMachineTime);
  _registerKernelAddress("KS_getAbsTime", (CallBackProc)KS_getAbsTime);
  _registerKernelAddress("KS_getClock", (CallBackProc)KS_getClock);
  _registerKernelAddress("KS_getClockSource", (CallBackProc)KS_getClockSource);
  _registerKernelAddress("KS_setClockSource", (CallBackProc)KS_setClockSource);
  _registerKernelAddress("KS_microDelay", (CallBackProc)KS_microDelay);
  _registerKernelAddress("KS_convertClock", (CallBackProc)KS_convertClock);
}

#define KS_CLOCK_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// System Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_installSystemHandler ------
Error __stdcall KS_installSystemHandler(int eventMask, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installSystemHandlerProc)(int, KSHandle, int);
  static KS_installSystemHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installSystemHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installSystemHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installSystemHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(eventMask, hSignal, flags);
}

//------ KS_removeSystemHandler ------
Error __stdcall KS_removeSystemHandler(int eventMask) {
  typedef Error (__stdcall* KS_removeSystemHandlerProc)(int);
  static KS_removeSystemHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeSystemHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeSystemHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeSystemHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(eventMask);
}

//--------------------------------------------------------------------------------------------------------------
// Device Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createDevice ------
Error __stdcall KS_createDevice(KSHandle* phDevice, const char* name, KSDeviceFunctions* pFunctions, int flags) {
  typedef Error (__stdcall* KS_createDeviceProc)(KSHandle*, const char*, KSDeviceFunctions*, int);
  static KS_createDeviceProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createDeviceProc)GetProcAddress(_hKitharaKrtsDll, "KS_createDevice");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createDevice not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phDevice, name, pFunctions, flags);
}

//------ KS_closeDevice ------
Error __stdcall KS_closeDevice(KSHandle hDevice) {
  typedef Error (__stdcall* KS_closeDeviceProc)(KSHandle);
  static KS_closeDeviceProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeDeviceProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeDevice");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeDevice not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hDevice);
}

//------ KS_resumeDevice ------
Error __stdcall KS_resumeDevice(KSHandle hDevice, KSHandle hRequest, byte* pBuffer, int size, int returnCode, int flags) {
  typedef Error (__stdcall* KS_resumeDeviceProc)(KSHandle, KSHandle, byte*, int, int, int);
  static KS_resumeDeviceProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_resumeDeviceProc)GetProcAddress(_hKitharaKrtsDll, "KS_resumeDevice");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_resumeDevice not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hDevice, hRequest, pBuffer, size, returnCode, flags);
}

//------ _initDeviceModule ------
static void _initDeviceModule() {
  _registerKernelAddress("KS_resumeDevice", (CallBackProc)KS_resumeDevice);
}

#define KS_DEVICE_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Keyboard Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createKeyHandler ------
Error __stdcall KS_createKeyHandler(uint mask, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_createKeyHandlerProc)(uint, KSHandle, int);
  static KS_createKeyHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createKeyHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_createKeyHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createKeyHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(mask, hSignal, flags);
}

//------ KS_removeKeyHandler ------
Error __stdcall KS_removeKeyHandler(uint mask) {
  typedef Error (__stdcall* KS_removeKeyHandlerProc)(uint);
  static KS_removeKeyHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeKeyHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeKeyHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeKeyHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(mask);
}

//------ KS_simulateKeyStroke ------
Error __stdcall KS_simulateKeyStroke(uint* pKeys) {
  typedef Error (__stdcall* KS_simulateKeyStrokeProc)(uint*);
  static KS_simulateKeyStrokeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_simulateKeyStrokeProc)GetProcAddress(_hKitharaKrtsDll, "KS_simulateKeyStroke");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_simulateKeyStroke not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pKeys);
}

//------ _initKeyboardModule ------
static void _initKeyboardModule() {
  _registerKernelAddress("KS_simulateKeyStroke", (CallBackProc)KS_simulateKeyStroke);
}

#define KS_KEYBOARD_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Interrupt Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createDeviceInterrupt ------
Error __stdcall KS_createDeviceInterrupt(KSHandle* phInterrupt, KSResourceInfoEx* pInfo, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_createDeviceInterruptProc)(KSHandle*, KSResourceInfoEx*, KSHandle, int);
  static KS_createDeviceInterruptProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createDeviceInterruptProc)GetProcAddress(_hKitharaKrtsDll, "KS_createDeviceInterrupt");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createDeviceInterrupt not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phInterrupt, pInfo, hSignal, flags);
}

//------ KS_createDeviceInterruptEx ------
Error __stdcall KS_createDeviceInterruptEx(KSHandle* phInterrupt, const char* deviceName, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_createDeviceInterruptExProc)(KSHandle*, const char*, KSHandle, int);
  static KS_createDeviceInterruptExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createDeviceInterruptExProc)GetProcAddress(_hKitharaKrtsDll, "KS_createDeviceInterruptEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createDeviceInterruptEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phInterrupt, deviceName, hSignal, flags);
}

//------ KS_createInterrupt ------
Error __stdcall KS_createInterrupt(KSHandle* phInterrupt, int irq, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_createInterruptProc)(KSHandle*, int, KSHandle, int);
  static KS_createInterruptProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createInterruptProc)GetProcAddress(_hKitharaKrtsDll, "KS_createInterrupt");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createInterrupt not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phInterrupt, irq, hSignal, flags);
}

//------ KS_removeInterrupt ------
Error __stdcall KS_removeInterrupt(KSHandle hInterrupt) {
  typedef Error (__stdcall* KS_removeInterruptProc)(KSHandle);
  static KS_removeInterruptProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeInterruptProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeInterrupt");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeInterrupt not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hInterrupt);
}

//------ KS_disableInterrupt ------
Error __stdcall KS_disableInterrupt(KSHandle hInterrupt) {
  typedef Error (__stdcall* KS_disableInterruptProc)(KSHandle);
  static KS_disableInterruptProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_disableInterruptProc)GetProcAddress(_hKitharaKrtsDll, "KS_disableInterrupt");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_disableInterrupt not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hInterrupt);
}

//------ KS_enableInterrupt ------
Error __stdcall KS_enableInterrupt(KSHandle hInterrupt) {
  typedef Error (__stdcall* KS_enableInterruptProc)(KSHandle);
  static KS_enableInterruptProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enableInterruptProc)GetProcAddress(_hKitharaKrtsDll, "KS_enableInterrupt");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enableInterrupt not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hInterrupt);
}

//------ KS_simulateInterrupt ------
Error __stdcall KS_simulateInterrupt(KSHandle hInterrupt) {
  typedef Error (__stdcall* KS_simulateInterruptProc)(KSHandle);
  static KS_simulateInterruptProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_simulateInterruptProc)GetProcAddress(_hKitharaKrtsDll, "KS_simulateInterrupt");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_simulateInterrupt not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hInterrupt);
}

//------ KS_getInterruptState ------
Error __stdcall KS_getInterruptState(KSHandle hInterrupt, HandlerState* pState) {
  typedef Error (__stdcall* KS_getInterruptStateProc)(KSHandle, HandlerState*);
  static KS_getInterruptStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getInterruptStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getInterruptState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getInterruptState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hInterrupt, pState);
}

//--------------------------------------------------------------------------------------------------------------
// UART Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openUart ------
Error __stdcall KS_openUart(KSHandle* phUart, const char* name, int port, const KSUartProperties* pProperties, int recvBufferLength, int xmitBufferLength, int flags) {
  typedef Error (__stdcall* KS_openUartProc)(KSHandle*, const char*, int, const KSUartProperties*, int, int, int);
  static KS_openUartProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openUartProc)GetProcAddress(_hKitharaKrtsDll, "KS_openUart");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openUart not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phUart, name, port, pProperties, recvBufferLength, xmitBufferLength, flags);
}

//------ KS_openUartEx ------
Error __stdcall KS_openUartEx(KSHandle* phUart, KSHandle hConnection, int port, KSUartProperties* pProperties, int recvBufferLength, int xmitBufferLength, int flags) {
  typedef Error (__stdcall* KS_openUartExProc)(KSHandle*, KSHandle, int, KSUartProperties*, int, int, int);
  static KS_openUartExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openUartExProc)GetProcAddress(_hKitharaKrtsDll, "KS_openUartEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openUartEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phUart, hConnection, port, pProperties, recvBufferLength, xmitBufferLength, flags);
}

//------ KS_closeUart ------
Error __stdcall KS_closeUart(KSHandle hUart, int flags) {
  typedef Error (__stdcall* KS_closeUartProc)(KSHandle, int);
  static KS_closeUartProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeUartProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeUart");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeUart not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUart, flags);
}

//------ KS_xmitUart ------
Error __stdcall KS_xmitUart(KSHandle hUart, int value, int flags) {
  typedef Error (__stdcall* KS_xmitUartProc)(KSHandle, int, int);
  static KS_xmitUartProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitUartProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitUart");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitUart not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUart, value, flags);
}

//------ KS_recvUart ------
Error __stdcall KS_recvUart(KSHandle hUart, int* pValue, int* pLineError, int flags) {
  typedef Error (__stdcall* KS_recvUartProc)(KSHandle, int*, int*, int);
  static KS_recvUartProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvUartProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvUart");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvUart not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUart, pValue, pLineError, flags);
}

//------ KS_xmitUartBlock ------
Error __stdcall KS_xmitUartBlock(KSHandle hUart, const void* pBytes, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_xmitUartBlockProc)(KSHandle, const void*, int, int*, int);
  static KS_xmitUartBlockProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitUartBlockProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitUartBlock");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitUartBlock not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUart, pBytes, length, pLength, flags);
}

//------ KS_recvUartBlock ------
Error __stdcall KS_recvUartBlock(KSHandle hUart, void* pBytes, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_recvUartBlockProc)(KSHandle, void*, int, int*, int);
  static KS_recvUartBlockProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvUartBlockProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvUartBlock");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvUartBlock not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUart, pBytes, length, pLength, flags);
}

//------ KS_installUartHandler ------
Error __stdcall KS_installUartHandler(KSHandle hUart, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installUartHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installUartHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installUartHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installUartHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installUartHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUart, eventCode, hSignal, flags);
}

//------ KS_getUartState ------
Error __stdcall KS_getUartState(KSHandle hUart, KSUartState* pUartState, int flags) {
  typedef Error (__stdcall* KS_getUartStateProc)(KSHandle, KSUartState*, int);
  static KS_getUartStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getUartStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getUartState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getUartState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUart, pUartState, flags);
}

//------ KS_execUartCommand ------
Error __stdcall KS_execUartCommand(KSHandle hUart, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execUartCommandProc)(KSHandle, int, void*, int);
  static KS_execUartCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execUartCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execUartCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execUartCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUart, command, pParam, flags);
}

//------ _initUARTModule ------
static void _initUARTModule() {
  _registerKernelAddress("KS_xmitUart", (CallBackProc)KS_xmitUart);
  _registerKernelAddress("KS_recvUart", (CallBackProc)KS_recvUart);
  _registerKernelAddress("KS_xmitUartBlock", (CallBackProc)KS_xmitUartBlock);
  _registerKernelAddress("KS_recvUartBlock", (CallBackProc)KS_recvUartBlock);
  _registerKernelAddress("KS_installUartHandler", (CallBackProc)KS_installUartHandler);
  _registerKernelAddress("KS_getUartState", (CallBackProc)KS_getUartState);
  _registerKernelAddress("KS_execUartCommand", (CallBackProc)KS_execUartCommand);
}

#define KS_UART_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// COMM Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openComm ------
Error __stdcall KS_openComm(KSHandle* phComm, const char* name, const KSCommProperties* pProperties, int recvBufferLength, int xmitBufferLength, int flags) {
  typedef Error (__stdcall* KS_openCommProc)(KSHandle*, const char*, const KSCommProperties*, int, int, int);
  static KS_openCommProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openCommProc)GetProcAddress(_hKitharaKrtsDll, "KS_openComm");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openComm not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phComm, name, pProperties, recvBufferLength, xmitBufferLength, flags);
}

//------ KS_closeComm ------
Error __stdcall KS_closeComm(KSHandle hComm, int flags) {
  typedef Error (__stdcall* KS_closeCommProc)(KSHandle, int);
  static KS_closeCommProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeCommProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeComm");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeComm not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hComm, flags);
}

//------ KS_xmitComm ------
Error __stdcall KS_xmitComm(KSHandle hComm, int value, int flags) {
  typedef Error (__stdcall* KS_xmitCommProc)(KSHandle, int, int);
  static KS_xmitCommProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitCommProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitComm");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitComm not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hComm, value, flags);
}

//------ KS_recvComm ------
Error __stdcall KS_recvComm(KSHandle hComm, int* pValue, int* pLineError, int flags) {
  typedef Error (__stdcall* KS_recvCommProc)(KSHandle, int*, int*, int);
  static KS_recvCommProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvCommProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvComm");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvComm not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hComm, pValue, pLineError, flags);
}

//------ KS_xmitCommBlock ------
Error __stdcall KS_xmitCommBlock(KSHandle hComm, const void* pBytes, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_xmitCommBlockProc)(KSHandle, const void*, int, int*, int);
  static KS_xmitCommBlockProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitCommBlockProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitCommBlock");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitCommBlock not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hComm, pBytes, length, pLength, flags);
}

//------ KS_recvCommBlock ------
Error __stdcall KS_recvCommBlock(KSHandle hComm, void* pBytes, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_recvCommBlockProc)(KSHandle, void*, int, int*, int);
  static KS_recvCommBlockProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvCommBlockProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvCommBlock");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvCommBlock not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hComm, pBytes, length, pLength, flags);
}

//------ KS_installCommHandler ------
Error __stdcall KS_installCommHandler(KSHandle hComm, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installCommHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installCommHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installCommHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installCommHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installCommHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hComm, eventCode, hSignal, flags);
}

//------ KS_getCommState ------
Error __stdcall KS_getCommState(KSHandle hComm, KSCommState* pCommState, int flags) {
  typedef Error (__stdcall* KS_getCommStateProc)(KSHandle, KSCommState*, int);
  static KS_getCommStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getCommStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getCommState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getCommState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hComm, pCommState, flags);
}

//------ KS_execCommCommand ------
Error __stdcall KS_execCommCommand(KSHandle hComm, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execCommCommandProc)(KSHandle, int, void*, int);
  static KS_execCommCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execCommCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execCommCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execCommCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hComm, command, pParam, flags);
}

//--------------------------------------------------------------------------------------------------------------
// USB Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openUsb ------
Error __stdcall KS_openUsb(KSHandle* phUsbDevice, const char* deviceName, int flags) {
  typedef Error (__stdcall* KS_openUsbProc)(KSHandle*, const char*, int);
  static KS_openUsbProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openUsbProc)GetProcAddress(_hKitharaKrtsDll, "KS_openUsb");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openUsb not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phUsbDevice, deviceName, flags);
}

//------ KS_openUsbEndPoint ------
Error __stdcall KS_openUsbEndPoint(KSHandle hUsbDevice, KSHandle* phUsbEndPoint, int endPointAddress, int packetSize, int packetCount, int flags) {
  typedef Error (__stdcall* KS_openUsbEndPointProc)(KSHandle, KSHandle*, int, int, int, int);
  static KS_openUsbEndPointProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openUsbEndPointProc)GetProcAddress(_hKitharaKrtsDll, "KS_openUsbEndPoint");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openUsbEndPoint not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUsbDevice, phUsbEndPoint, endPointAddress, packetSize, packetCount, flags);
}

//------ KS_closeUsb ------
Error __stdcall KS_closeUsb(KSHandle hUsb, int flags) {
  typedef Error (__stdcall* KS_closeUsbProc)(KSHandle, int);
  static KS_closeUsbProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeUsbProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeUsb");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeUsb not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUsb, flags);
}

//------ KS_xmitUsb ------
Error __stdcall KS_xmitUsb(KSHandle hUsb, void* pData, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_xmitUsbProc)(KSHandle, void*, int, int*, int);
  static KS_xmitUsbProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitUsbProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitUsb");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitUsb not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUsb, pData, length, pLength, flags);
}

//------ KS_recvUsb ------
Error __stdcall KS_recvUsb(KSHandle hUsb, void* pData, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_recvUsbProc)(KSHandle, void*, int, int*, int);
  static KS_recvUsbProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvUsbProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvUsb");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvUsb not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUsb, pData, length, pLength, flags);
}

//------ KS_installUsbHandler ------
Error __stdcall KS_installUsbHandler(KSHandle hUsb, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installUsbHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installUsbHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installUsbHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installUsbHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installUsbHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUsb, eventCode, hSignal, flags);
}

//------ KS_getUsbState ------
Error __stdcall KS_getUsbState(KSHandle hUsb, KSUsbState* pUsbState, int flags) {
  typedef Error (__stdcall* KS_getUsbStateProc)(KSHandle, KSUsbState*, int);
  static KS_getUsbStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getUsbStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getUsbState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getUsbState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUsb, pUsbState, flags);
}

//------ KS_execUsbCommand ------
Error __stdcall KS_execUsbCommand(KSHandle hUsb, int command, int index, void* pData, int size, int flags) {
  typedef Error (__stdcall* KS_execUsbCommandProc)(KSHandle, int, int, void*, int, int);
  static KS_execUsbCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execUsbCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execUsbCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execUsbCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hUsb, command, index, pData, size, flags);
}

//------ _initUSBModule ------
static void _initUSBModule() {
  _registerKernelAddress("KS_xmitUsb", (CallBackProc)KS_xmitUsb);
  _registerKernelAddress("KS_recvUsb", (CallBackProc)KS_recvUsb);
  _registerKernelAddress("KS_installUsbHandler", (CallBackProc)KS_installUsbHandler);
  _registerKernelAddress("KS_getUsbState", (CallBackProc)KS_getUsbState);
  _registerKernelAddress("KS_execUsbCommand", (CallBackProc)KS_execUsbCommand);
}

#define KS_USB_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// XHCI Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openXhciController ------
Error __stdcall KS_openXhciController(KSHandle* phXhciController, const char* deviceName, int flags) {
  typedef Error (__stdcall* KS_openXhciControllerProc)(KSHandle*, const char*, int);
  static KS_openXhciControllerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openXhciControllerProc)GetProcAddress(_hKitharaKrtsDll, "KS_openXhciController");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openXhciController not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phXhciController, deviceName, flags);
}

//------ KS_openXhciDevice ------
Error __stdcall KS_openXhciDevice(KSHandle* phXhciDevice, const char* deviceName, int flags) {
  typedef Error (__stdcall* KS_openXhciDeviceProc)(KSHandle*, const char*, int);
  static KS_openXhciDeviceProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openXhciDeviceProc)GetProcAddress(_hKitharaKrtsDll, "KS_openXhciDevice");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openXhciDevice not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phXhciDevice, deviceName, flags);
}

//------ KS_openXhciEndPoint ------
Error __stdcall KS_openXhciEndPoint(KSHandle hXhciDevice, KSHandle* phXhciEndPoint, int endPointAddress, int packetSize, int packetCount, int flags) {
  typedef Error (__stdcall* KS_openXhciEndPointProc)(KSHandle, KSHandle*, int, int, int, int);
  static KS_openXhciEndPointProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openXhciEndPointProc)GetProcAddress(_hKitharaKrtsDll, "KS_openXhciEndPoint");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openXhciEndPoint not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhciDevice, phXhciEndPoint, endPointAddress, packetSize, packetCount, flags);
}

//------ KS_closeXhci ------
Error __stdcall KS_closeXhci(KSHandle hXhci, int flags) {
  typedef Error (__stdcall* KS_closeXhciProc)(KSHandle, int);
  static KS_closeXhciProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeXhciProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeXhci");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeXhci not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhci, flags);
}

//------ KS_requestXhciPacket ------
Error __stdcall KS_requestXhciPacket(KSHandle hXhciEndPoint, void** ppXhciPacket, int flags) {
  typedef Error (__stdcall* KS_requestXhciPacketProc)(KSHandle, void**, int);
  static KS_requestXhciPacketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_requestXhciPacketProc)GetProcAddress(_hKitharaKrtsDll, "KS_requestXhciPacket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_requestXhciPacket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhciEndPoint, ppXhciPacket, flags);
}

//------ KS_releaseXhciPacket ------
Error __stdcall KS_releaseXhciPacket(KSHandle hXhciEndPoint, void* pXhciPacket, int flags) {
  typedef Error (__stdcall* KS_releaseXhciPacketProc)(KSHandle, void*, int);
  static KS_releaseXhciPacketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_releaseXhciPacketProc)GetProcAddress(_hKitharaKrtsDll, "KS_releaseXhciPacket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_releaseXhciPacket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhciEndPoint, pXhciPacket, flags);
}

//------ KS_xmitXhciPacket ------
Error __stdcall KS_xmitXhciPacket(KSHandle hXhciEndPoint, void* pXhciPacket, int size, int flags) {
  typedef Error (__stdcall* KS_xmitXhciPacketProc)(KSHandle, void*, int, int);
  static KS_xmitXhciPacketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitXhciPacketProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitXhciPacket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitXhciPacket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhciEndPoint, pXhciPacket, size, flags);
}

//------ KS_recvXhciPacket ------
Error __stdcall KS_recvXhciPacket(KSHandle hXhciEndPoint, void** ppXhciPacket, int* pSize, int flags) {
  typedef Error (__stdcall* KS_recvXhciPacketProc)(KSHandle, void**, int*, int);
  static KS_recvXhciPacketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvXhciPacketProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvXhciPacket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvXhciPacket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhciEndPoint, ppXhciPacket, pSize, flags);
}

//------ KS_sendXhciControlRequest ------
Error __stdcall KS_sendXhciControlRequest(KSHandle hXhciDevice, const KSXhciControlRequest* pRequestData, void* pData, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_sendXhciControlRequestProc)(KSHandle, const KSXhciControlRequest*, void*, int, int*, int);
  static KS_sendXhciControlRequestProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_sendXhciControlRequestProc)GetProcAddress(_hKitharaKrtsDll, "KS_sendXhciControlRequest");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_sendXhciControlRequest not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhciDevice, pRequestData, pData, length, pLength, flags);
}

//------ KS_installXhciHandler ------
Error __stdcall KS_installXhciHandler(KSHandle hXhci, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installXhciHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installXhciHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installXhciHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installXhciHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installXhciHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhci, eventCode, hSignal, flags);
}

//------ KS_getXhciEndPointState ------
Error __stdcall KS_getXhciEndPointState(KSHandle hXhciEndPoint, KSXhciEndPointState* pState, int flags) {
  typedef Error (__stdcall* KS_getXhciEndPointStateProc)(KSHandle, KSXhciEndPointState*, int);
  static KS_getXhciEndPointStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getXhciEndPointStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getXhciEndPointState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getXhciEndPointState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhciEndPoint, pState, flags);
}

//------ KS_execXhciCommand ------
Error __stdcall KS_execXhciCommand(KSHandle hXhci, int command, int index, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execXhciCommandProc)(KSHandle, int, int, void*, int);
  static KS_execXhciCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execXhciCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execXhciCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execXhciCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hXhci, command, index, pParam, flags);
}

//------ _initXHCIModule ------
static void _initXHCIModule() {
  _registerKernelAddress("KS_requestXhciPacket", (CallBackProc)KS_requestXhciPacket);
  _registerKernelAddress("KS_releaseXhciPacket", (CallBackProc)KS_releaseXhciPacket);
  _registerKernelAddress("KS_xmitXhciPacket", (CallBackProc)KS_xmitXhciPacket);
  _registerKernelAddress("KS_recvXhciPacket", (CallBackProc)KS_recvXhciPacket);
  _registerKernelAddress("KS_sendXhciControlRequest", (CallBackProc)KS_sendXhciControlRequest);
  _registerKernelAddress("KS_installXhciHandler", (CallBackProc)KS_installXhciHandler);
  _registerKernelAddress("KS_getXhciEndPointState", (CallBackProc)KS_getXhciEndPointState);
  _registerKernelAddress("KS_execXhciCommand", (CallBackProc)KS_execXhciCommand);
}

#define KS_XHCI_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// RealTime Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createTimer ------
Error __stdcall KS_createTimer(KSHandle* phTimer, int delay, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_createTimerProc)(KSHandle*, int, KSHandle, int);
  static KS_createTimerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createTimerProc)GetProcAddress(_hKitharaKrtsDll, "KS_createTimer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createTimer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phTimer, delay, hSignal, flags);
}

//------ KS_removeTimer ------
Error __stdcall KS_removeTimer(KSHandle hTimer) {
  typedef Error (__stdcall* KS_removeTimerProc)(KSHandle);
  static KS_removeTimerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeTimerProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeTimer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeTimer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTimer);
}

//------ KS_cancelTimer ------
Error __stdcall KS_cancelTimer(KSHandle hTimer) {
  typedef Error (__stdcall* KS_cancelTimerProc)(KSHandle);
  static KS_cancelTimerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_cancelTimerProc)GetProcAddress(_hKitharaKrtsDll, "KS_cancelTimer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_cancelTimer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTimer);
}

//------ KS_startTimer ------
Error __stdcall KS_startTimer(KSHandle hTimer, int flags, int delay) {
  typedef Error (__stdcall* KS_startTimerProc)(KSHandle, int, int);
  static KS_startTimerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_startTimerProc)GetProcAddress(_hKitharaKrtsDll, "KS_startTimer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_startTimer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTimer, flags, delay);
}

//------ KS_stopTimer ------
Error __stdcall KS_stopTimer(KSHandle hTimer) {
  typedef Error (__stdcall* KS_stopTimerProc)(KSHandle);
  static KS_stopTimerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_stopTimerProc)GetProcAddress(_hKitharaKrtsDll, "KS_stopTimer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_stopTimer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTimer);
}

//------ KS_startTimerDelayed ------
Error __stdcall KS_startTimerDelayed(KSHandle hTimer, int64ref start, int period, int flags) {
  typedef Error (__stdcall* KS_startTimerDelayedProc)(KSHandle, int64ref, int, int);
  static KS_startTimerDelayedProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_startTimerDelayedProc)GetProcAddress(_hKitharaKrtsDll, "KS_startTimerDelayed");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_startTimerDelayed not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTimer, start, period, flags);
}

//------ KS_adjustTimer ------
Error __stdcall KS_adjustTimer(KSHandle hTimer, int period, int flags) {
  typedef Error (__stdcall* KS_adjustTimerProc)(KSHandle, int, int);
  static KS_adjustTimerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_adjustTimerProc)GetProcAddress(_hKitharaKrtsDll, "KS_adjustTimer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_adjustTimer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTimer, period, flags);
}

//------ KS_disableTimer ------
Error __stdcall KS_disableTimer(KSHandle hTimer) {
  typedef Error (__stdcall* KS_disableTimerProc)(KSHandle);
  static KS_disableTimerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_disableTimerProc)GetProcAddress(_hKitharaKrtsDll, "KS_disableTimer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_disableTimer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTimer);
}

//------ KS_enableTimer ------
Error __stdcall KS_enableTimer(KSHandle hTimer) {
  typedef Error (__stdcall* KS_enableTimerProc)(KSHandle);
  static KS_enableTimerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enableTimerProc)GetProcAddress(_hKitharaKrtsDll, "KS_enableTimer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enableTimer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTimer);
}

//------ KS_getTimerState ------
Error __stdcall KS_getTimerState(KSHandle hTimer, HandlerState* pState) {
  typedef Error (__stdcall* KS_getTimerStateProc)(KSHandle, HandlerState*);
  static KS_getTimerStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getTimerStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getTimerState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getTimerState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTimer, pState);
}

//------ KS_setTimerResolution ------
Error __stdcall KS_setTimerResolution(uint resolution, int flags) {
  typedef Error (__stdcall* KS_setTimerResolutionProc)(uint, int);
  static KS_setTimerResolutionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setTimerResolutionProc)GetProcAddress(_hKitharaKrtsDll, "KS_setTimerResolution");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setTimerResolution not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(resolution, flags);
}

//------ KS_getTimerResolution ------
Error __stdcall KS_getTimerResolution(uint* pResolution, int flags) {
  typedef Error (__stdcall* KS_getTimerResolutionProc)(uint*, int);
  static KS_getTimerResolutionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getTimerResolutionProc)GetProcAddress(_hKitharaKrtsDll, "KS_getTimerResolution");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getTimerResolution not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pResolution, flags);
}

//------ _initRealTimeModule ------
static void _initRealTimeModule() {
  _registerKernelAddress("KS_createTimer", (CallBackProc)KS_createTimer);
  _registerKernelAddress("KS_removeTimer", (CallBackProc)KS_removeTimer);
  _registerKernelAddress("KS_cancelTimer", (CallBackProc)KS_cancelTimer);
  _registerKernelAddress("KS_startTimer", (CallBackProc)KS_startTimer);
  _registerKernelAddress("KS_stopTimer", (CallBackProc)KS_stopTimer);
  _registerKernelAddress("KS_startTimerDelayed", (CallBackProc)KS_startTimerDelayed);
  _registerKernelAddress("KS_adjustTimer", (CallBackProc)KS_adjustTimer);
  _registerKernelAddress("KS_disableTimer", (CallBackProc)KS_disableTimer);
  _registerKernelAddress("KS_enableTimer", (CallBackProc)KS_enableTimer);
  _registerKernelAddress("KS_getTimerState", (CallBackProc)KS_getTimerState);
  _registerKernelAddress("KS_setTimerResolution", (CallBackProc)KS_setTimerResolution);
  _registerKernelAddress("KS_getTimerResolution", (CallBackProc)KS_getTimerResolution);
}

#define KS_REALTIME_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Task Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createTask ------
Error __stdcall KS_createTask(KSHandle* phTask, KSHandle hCallBack, int priority, int flags) {
  typedef Error (__stdcall* KS_createTaskProc)(KSHandle*, KSHandle, int, int);
  static KS_createTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_createTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phTask, hCallBack, priority, flags);
}

//------ KS_removeTask ------
Error __stdcall KS_removeTask(KSHandle hTask) {
  typedef Error (__stdcall* KS_removeTaskProc)(KSHandle);
  static KS_removeTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTask);
}

//------ KS_exitTask ------
Error __stdcall KS_exitTask(KSHandle hTask, int exitCode) {
  typedef Error (__stdcall* KS_exitTaskProc)(KSHandle, int);
  static KS_exitTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_exitTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_exitTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_exitTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTask, exitCode);
}

//------ KS_killTask ------
Error __stdcall KS_killTask(KSHandle hTask, int exitCode) {
  typedef Error (__stdcall* KS_killTaskProc)(KSHandle, int);
  static KS_killTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_killTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_killTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_killTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTask, exitCode);
}

//------ KS_terminateTask ------
Error __stdcall KS_terminateTask(KSHandle hTask, int timeout, int flags) {
  typedef Error (__stdcall* KS_terminateTaskProc)(KSHandle, int, int);
  static KS_terminateTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_terminateTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_terminateTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_terminateTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTask, timeout, flags);
}

//------ KS_suspendTask ------
Error __stdcall KS_suspendTask(KSHandle hTask) {
  typedef Error (__stdcall* KS_suspendTaskProc)(KSHandle);
  static KS_suspendTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_suspendTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_suspendTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_suspendTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTask);
}

//------ KS_resumeTask ------
Error __stdcall KS_resumeTask(KSHandle hTask) {
  typedef Error (__stdcall* KS_resumeTaskProc)(KSHandle);
  static KS_resumeTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_resumeTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_resumeTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_resumeTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTask);
}

//------ KS_triggerTask ------
Error __stdcall KS_triggerTask(KSHandle hTask) {
  typedef Error (__stdcall* KS_triggerTaskProc)(KSHandle);
  static KS_triggerTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_triggerTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_triggerTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_triggerTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTask);
}

//------ KS_yieldTask ------
Error __stdcall KS_yieldTask() {
  typedef Error (__stdcall* KS_yieldTaskProc)();
  static KS_yieldTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_yieldTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_yieldTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_yieldTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)();
}

//------ KS_sleepTask ------
Error __stdcall KS_sleepTask(int delay) {
  typedef Error (__stdcall* KS_sleepTaskProc)(int);
  static KS_sleepTaskProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_sleepTaskProc)GetProcAddress(_hKitharaKrtsDll, "KS_sleepTask");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_sleepTask not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(delay);
}

//------ KS_setTaskPrio ------
Error __stdcall KS_setTaskPrio(KSHandle hTask, int newPrio, int* pOldPrio) {
  typedef Error (__stdcall* KS_setTaskPrioProc)(KSHandle, int, int*);
  static KS_setTaskPrioProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setTaskPrioProc)GetProcAddress(_hKitharaKrtsDll, "KS_setTaskPrio");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setTaskPrio not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTask, newPrio, pOldPrio);
}

//------ KS_setTaskPriority ------
Error __stdcall KS_setTaskPriority(KSHandle hObject, int priority, int flags) {
  typedef Error (__stdcall* KS_setTaskPriorityProc)(KSHandle, int, int);
  static KS_setTaskPriorityProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setTaskPriorityProc)GetProcAddress(_hKitharaKrtsDll, "KS_setTaskPriority");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setTaskPriority not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, priority, flags);
}

//------ KS_setTaskPriorityRelative ------
Error __stdcall KS_setTaskPriorityRelative(KSHandle hObject, KSHandle hRelative, int distance, int flags) {
  typedef Error (__stdcall* KS_setTaskPriorityRelativeProc)(KSHandle, KSHandle, int, int);
  static KS_setTaskPriorityRelativeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setTaskPriorityRelativeProc)GetProcAddress(_hKitharaKrtsDll, "KS_setTaskPriorityRelative");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setTaskPriorityRelative not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, hRelative, distance, flags);
}

//------ KS_getTaskPriority ------
Error __stdcall KS_getTaskPriority(KSHandle hObject, int* pPriority, int flags) {
  typedef Error (__stdcall* KS_getTaskPriorityProc)(KSHandle, int*, int);
  static KS_getTaskPriorityProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getTaskPriorityProc)GetProcAddress(_hKitharaKrtsDll, "KS_getTaskPriority");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getTaskPriority not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, pPriority, flags);
}

//------ KS_getTaskState ------
Error __stdcall KS_getTaskState(KSHandle hTask, uint* pTaskState, Error* pExitCode) {
  typedef Error (__stdcall* KS_getTaskStateProc)(KSHandle, uint*, Error*);
  static KS_getTaskStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getTaskStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getTaskState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getTaskState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hTask, pTaskState, pExitCode);
}

//------ KS_setTaskStackSize ------
Error __stdcall KS_setTaskStackSize(int size, int flags) {
  typedef Error (__stdcall* KS_setTaskStackSizeProc)(int, int);
  static KS_setTaskStackSizeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setTaskStackSizeProc)GetProcAddress(_hKitharaKrtsDll, "KS_setTaskStackSize");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setTaskStackSize not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(size, flags);
}

//------ KS_setTargetProcessor ------
Error __stdcall KS_setTargetProcessor(int processor, int flags) {
  typedef Error (__stdcall* KS_setTargetProcessorProc)(int, int);
  static KS_setTargetProcessorProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setTargetProcessorProc)GetProcAddress(_hKitharaKrtsDll, "KS_setTargetProcessor");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setTargetProcessor not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(processor, flags);
}

//------ KS_getCurrentProcessor ------
Error __stdcall KS_getCurrentProcessor(int* pProcessor, int flags) {
  typedef Error (__stdcall* KS_getCurrentProcessorProc)(int*, int);
  static KS_getCurrentProcessorProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getCurrentProcessorProc)GetProcAddress(_hKitharaKrtsDll, "KS_getCurrentProcessor");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getCurrentProcessor not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pProcessor, flags);
}

//------ KS_getCurrentContext ------
Error __stdcall KS_getCurrentContext(KSContextInformation* pContextInfo, int flags) {
  typedef Error (__stdcall* KS_getCurrentContextProc)(KSContextInformation*, int);
  static KS_getCurrentContextProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getCurrentContextProc)GetProcAddress(_hKitharaKrtsDll, "KS_getCurrentContext");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getCurrentContext not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(pContextInfo, flags);
}

//------ KS_createSemaphore ------
Error __stdcall KS_createSemaphore(KSHandle* phSemaphore, int maxCount, int initCount, int flags) {
  typedef Error (__stdcall* KS_createSemaphoreProc)(KSHandle*, int, int, int);
  static KS_createSemaphoreProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createSemaphoreProc)GetProcAddress(_hKitharaKrtsDll, "KS_createSemaphore");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createSemaphore not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phSemaphore, maxCount, initCount, flags);
}

//------ KS_removeSemaphore ------
Error __stdcall KS_removeSemaphore(KSHandle hSemaphore) {
  typedef Error (__stdcall* KS_removeSemaphoreProc)(KSHandle);
  static KS_removeSemaphoreProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeSemaphoreProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeSemaphore");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeSemaphore not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSemaphore);
}

//------ KS_requestSemaphore ------
Error __stdcall KS_requestSemaphore(KSHandle hSemaphore, int timeout) {
  typedef Error (__stdcall* KS_requestSemaphoreProc)(KSHandle, int);
  static KS_requestSemaphoreProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_requestSemaphoreProc)GetProcAddress(_hKitharaKrtsDll, "KS_requestSemaphore");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_requestSemaphore not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSemaphore, timeout);
}

//------ KS_releaseSemaphore ------
Error __stdcall KS_releaseSemaphore(KSHandle hSemaphore) {
  typedef Error (__stdcall* KS_releaseSemaphoreProc)(KSHandle);
  static KS_releaseSemaphoreProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_releaseSemaphoreProc)GetProcAddress(_hKitharaKrtsDll, "KS_releaseSemaphore");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_releaseSemaphore not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSemaphore);
}

//------ _initTaskModule ------
static void _initTaskModule() {
  _registerKernelAddress("KS_createTask", (CallBackProc)KS_createTask);
  _registerKernelAddress("KS_removeTask", (CallBackProc)KS_removeTask);
  _registerKernelAddress("KS_exitTask", (CallBackProc)KS_exitTask);
  _registerKernelAddress("KS_killTask", (CallBackProc)KS_killTask);
  _registerKernelAddress("KS_terminateTask", (CallBackProc)KS_terminateTask);
  _registerKernelAddress("KS_suspendTask", (CallBackProc)KS_suspendTask);
  _registerKernelAddress("KS_resumeTask", (CallBackProc)KS_resumeTask);
  _registerKernelAddress("KS_triggerTask", (CallBackProc)KS_triggerTask);
  _registerKernelAddress("KS_yieldTask", (CallBackProc)KS_yieldTask);
  _registerKernelAddress("KS_sleepTask", (CallBackProc)KS_sleepTask);
  _registerKernelAddress("KS_setTaskPrio", (CallBackProc)KS_setTaskPrio);
  _registerKernelAddress("KS_setTaskPriority", (CallBackProc)KS_setTaskPriority);
  _registerKernelAddress("KS_setTaskPriorityRelative", (CallBackProc)KS_setTaskPriorityRelative);
  _registerKernelAddress("KS_getTaskPriority", (CallBackProc)KS_getTaskPriority);
  _registerKernelAddress("KS_getTaskState", (CallBackProc)KS_getTaskState);
  _registerKernelAddress("KS_setTaskStackSize", (CallBackProc)KS_setTaskStackSize);
  _registerKernelAddress("KS_setTargetProcessor", (CallBackProc)KS_setTargetProcessor);
  _registerKernelAddress("KS_getCurrentProcessor", (CallBackProc)KS_getCurrentProcessor);
  _registerKernelAddress("KS_getCurrentContext", (CallBackProc)KS_getCurrentContext);
  _registerKernelAddress("KS_createSemaphore", (CallBackProc)KS_createSemaphore);
  _registerKernelAddress("KS_removeSemaphore", (CallBackProc)KS_removeSemaphore);
  _registerKernelAddress("KS_requestSemaphore", (CallBackProc)KS_requestSemaphore);
  _registerKernelAddress("KS_releaseSemaphore", (CallBackProc)KS_releaseSemaphore);
}

#define KS_TASK_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Dedicated Module
//--------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------
// Packet Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openAdapter ------
Error __stdcall KS_openAdapter(KSHandle* phAdapter, const char* name, int recvPoolLength, int sendPoolLength, int flags) {
  typedef Error (__stdcall* KS_openAdapterProc)(KSHandle*, const char*, int, int, int);
  static KS_openAdapterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openAdapterProc)GetProcAddress(_hKitharaKrtsDll, "KS_openAdapter");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openAdapter not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phAdapter, name, recvPoolLength, sendPoolLength, flags);
}

//------ KS_openAdapterEx ------
Error __stdcall KS_openAdapterEx(KSHandle* phAdapter, KSHandle hConnection, int recvPoolLength, int sendPoolLength, int flags) {
  typedef Error (__stdcall* KS_openAdapterExProc)(KSHandle*, KSHandle, int, int, int);
  static KS_openAdapterExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openAdapterExProc)GetProcAddress(_hKitharaKrtsDll, "KS_openAdapterEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openAdapterEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phAdapter, hConnection, recvPoolLength, sendPoolLength, flags);
}

//------ KS_closeAdapter ------
Error __stdcall KS_closeAdapter(KSHandle hAdapter) {
  typedef Error (__stdcall* KS_closeAdapterProc)(KSHandle);
  static KS_closeAdapterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeAdapterProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeAdapter");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeAdapter not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter);
}

//------ KS_getAdapterState ------
Error __stdcall KS_getAdapterState(KSHandle hAdapter, KSAdapterState* pAdapterState, int flags) {
  typedef Error (__stdcall* KS_getAdapterStateProc)(KSHandle, KSAdapterState*, int);
  static KS_getAdapterStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getAdapterStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getAdapterState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getAdapterState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter, pAdapterState, flags);
}

//------ KS_execAdapterCommand ------
Error __stdcall KS_execAdapterCommand(KSHandle hAdapter, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execAdapterCommandProc)(KSHandle, int, void*, int);
  static KS_execAdapterCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execAdapterCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execAdapterCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execAdapterCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter, command, pParam, flags);
}

//------ KS_aggregateAdapter ------
Error __stdcall KS_aggregateAdapter(KSHandle hAdapter, KSHandle hLink, int flags) {
  typedef Error (__stdcall* KS_aggregateAdapterProc)(KSHandle, KSHandle, int);
  static KS_aggregateAdapterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_aggregateAdapterProc)GetProcAddress(_hKitharaKrtsDll, "KS_aggregateAdapter");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_aggregateAdapter not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter, hLink, flags);
}

//------ KS_requestPacket ------
Error __stdcall KS_requestPacket(KSHandle hAdapter, void** ppPacket, int flags) {
  typedef Error (__stdcall* KS_requestPacketProc)(KSHandle, void**, int);
  static KS_requestPacketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_requestPacketProc)GetProcAddress(_hKitharaKrtsDll, "KS_requestPacket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_requestPacket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter, ppPacket, flags);
}

//------ KS_releasePacket ------
Error __stdcall KS_releasePacket(KSHandle hAdapter, void* pPacket, int flags) {
  typedef Error (__stdcall* KS_releasePacketProc)(KSHandle, void*, int);
  static KS_releasePacketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_releasePacketProc)GetProcAddress(_hKitharaKrtsDll, "KS_releasePacket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_releasePacket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter, pPacket, flags);
}

//------ KS_sendPacket ------
Error __stdcall KS_sendPacket(KSHandle hAdapter, void* pPacket, uint size, int flags) {
  typedef Error (__stdcall* KS_sendPacketProc)(KSHandle, void*, uint, int);
  static KS_sendPacketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_sendPacketProc)GetProcAddress(_hKitharaKrtsDll, "KS_sendPacket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_sendPacket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter, pPacket, size, flags);
}

//------ KS_recvPacket ------
Error __stdcall KS_recvPacket(KSHandle hAdapter, void** ppPacket, int flags) {
  typedef Error (__stdcall* KS_recvPacketProc)(KSHandle, void**, int);
  static KS_recvPacketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvPacketProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvPacket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvPacket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter, ppPacket, flags);
}

//------ KS_recvPacketEx ------
Error __stdcall KS_recvPacketEx(KSHandle hAdapter, void** ppPacket, int* pLength, int flags) {
  typedef Error (__stdcall* KS_recvPacketExProc)(KSHandle, void**, int*, int);
  static KS_recvPacketExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvPacketExProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvPacketEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvPacketEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter, ppPacket, pLength, flags);
}

//------ KS_installPacketHandler ------
Error __stdcall KS_installPacketHandler(KSHandle hAdapter, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installPacketHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installPacketHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installPacketHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installPacketHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installPacketHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hAdapter, eventCode, hSignal, flags);
}

//------ _initPacketModule ------
static void _initPacketModule() {
  _registerKernelAddress("KS_openAdapter", (CallBackProc)KS_openAdapter);
  _registerKernelAddress("KS_closeAdapter", (CallBackProc)KS_closeAdapter);
  _registerKernelAddress("KS_getAdapterState", (CallBackProc)KS_getAdapterState);
  _registerKernelAddress("KS_execAdapterCommand", (CallBackProc)KS_execAdapterCommand);
  _registerKernelAddress("KS_aggregateAdapter", (CallBackProc)KS_aggregateAdapter);
  _registerKernelAddress("KS_requestPacket", (CallBackProc)KS_requestPacket);
  _registerKernelAddress("KS_releasePacket", (CallBackProc)KS_releasePacket);
  _registerKernelAddress("KS_sendPacket", (CallBackProc)KS_sendPacket);
  _registerKernelAddress("KS_recvPacket", (CallBackProc)KS_recvPacket);
  _registerKernelAddress("KS_recvPacketEx", (CallBackProc)KS_recvPacketEx);
  _registerKernelAddress("KS_installPacketHandler", (CallBackProc)KS_installPacketHandler);
}

#define KS_PACKET_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Socket Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openSocket ------
Error __stdcall KS_openSocket(KSHandle* phSocket, KSHandle hAdapter, const KSSocketAddr* pSocketAddr, int protocol, int flags) {
  typedef Error (__stdcall* KS_openSocketProc)(KSHandle*, KSHandle, const KSSocketAddr*, int, int);
  static KS_openSocketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openSocketProc)GetProcAddress(_hKitharaKrtsDll, "KS_openSocket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openSocket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phSocket, hAdapter, pSocketAddr, protocol, flags);
}

//------ KS_closeSocket ------
Error __stdcall KS_closeSocket(KSHandle hSocket) {
  typedef Error (__stdcall* KS_closeSocketProc)(KSHandle);
  static KS_closeSocketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeSocketProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeSocket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeSocket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSocket);
}

//------ KS_recvFromSocket ------
Error __stdcall KS_recvFromSocket(KSHandle hSocket, KSSocketAddr* pSourceAddr, void* pBuffer, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_recvFromSocketProc)(KSHandle, KSSocketAddr*, void*, int, int*, int);
  static KS_recvFromSocketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvFromSocketProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvFromSocket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvFromSocket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSocket, pSourceAddr, pBuffer, length, pLength, flags);
}

//------ KS_sendToSocket ------
Error __stdcall KS_sendToSocket(KSHandle hSocket, const KSSocketAddr* pTargetAddr, const void* pBuffer, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_sendToSocketProc)(KSHandle, const KSSocketAddr*, const void*, int, int*, int);
  static KS_sendToSocketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_sendToSocketProc)GetProcAddress(_hKitharaKrtsDll, "KS_sendToSocket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_sendToSocket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSocket, pTargetAddr, pBuffer, length, pLength, flags);
}

//------ KS_recvSocket ------
Error __stdcall KS_recvSocket(KSHandle hSocket, void* pBuffer, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_recvSocketProc)(KSHandle, void*, int, int*, int);
  static KS_recvSocketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvSocketProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvSocket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvSocket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSocket, pBuffer, length, pLength, flags);
}

//------ KS_sendSocket ------
Error __stdcall KS_sendSocket(KSHandle hSocket, const void* pBuffer, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_sendSocketProc)(KSHandle, const void*, int, int*, int);
  static KS_sendSocketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_sendSocketProc)GetProcAddress(_hKitharaKrtsDll, "KS_sendSocket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_sendSocket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSocket, pBuffer, length, pLength, flags);
}

//------ KS_installSocketHandler ------
Error __stdcall KS_installSocketHandler(KSHandle hSocket, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installSocketHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installSocketHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installSocketHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installSocketHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installSocketHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSocket, eventCode, hSignal, flags);
}

//------ KS_connectSocket ------
Error __stdcall KS_connectSocket(KSHandle hClient, const KSSocketAddr* pServerAddr, int flags) {
  typedef Error (__stdcall* KS_connectSocketProc)(KSHandle, const KSSocketAddr*, int);
  static KS_connectSocketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_connectSocketProc)GetProcAddress(_hKitharaKrtsDll, "KS_connectSocket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_connectSocket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hClient, pServerAddr, flags);
}

//------ KS_acceptSocket ------
Error __stdcall KS_acceptSocket(KSHandle hServer, KSSocketAddr* pClientAddr, int flags) {
  typedef Error (__stdcall* KS_acceptSocketProc)(KSHandle, KSSocketAddr*, int);
  static KS_acceptSocketProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_acceptSocketProc)GetProcAddress(_hKitharaKrtsDll, "KS_acceptSocket");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_acceptSocket not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hServer, pClientAddr, flags);
}

//------ KS_execSocketCommand ------
Error __stdcall KS_execSocketCommand(KSHandle hSocket, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execSocketCommandProc)(KSHandle, int, void*, int);
  static KS_execSocketCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execSocketCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execSocketCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execSocketCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSocket, command, pParam, flags);
}

//------ _initSocketModule ------
static void _initSocketModule() {
  _registerKernelAddress("KS_recvFromSocket", (CallBackProc)KS_recvFromSocket);
  _registerKernelAddress("KS_sendToSocket", (CallBackProc)KS_sendToSocket);
  _registerKernelAddress("KS_recvSocket", (CallBackProc)KS_recvSocket);
  _registerKernelAddress("KS_sendSocket", (CallBackProc)KS_sendSocket);
  _registerKernelAddress("KS_connectSocket", (CallBackProc)KS_connectSocket);
  _registerKernelAddress("KS_acceptSocket", (CallBackProc)KS_acceptSocket);
  _registerKernelAddress("KS_execSocketCommand", (CallBackProc)KS_execSocketCommand);
}

#define KS_SOCKET_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// EtherCAT Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createEcatMaster ------
Error __stdcall KS_createEcatMaster(KSHandle* phMaster, KSHandle hConnection, const char* libraryPath, const char* topologyFile, int flags) {
  typedef Error (__stdcall* KS_createEcatMasterProc)(KSHandle*, KSHandle, const char*, const char*, int);
  static KS_createEcatMasterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createEcatMasterProc)GetProcAddress(_hKitharaKrtsDll, "KS_createEcatMaster");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createEcatMaster not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phMaster, hConnection, libraryPath, topologyFile, flags);
}

//------ KS_closeEcatMaster ------
Error __stdcall KS_closeEcatMaster(KSHandle hMaster) {
  typedef Error (__stdcall* KS_closeEcatMasterProc)(KSHandle);
  static KS_closeEcatMasterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeEcatMasterProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeEcatMaster");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeEcatMaster not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster);
}

//------ KS_installEcatMasterHandler ------
Error __stdcall KS_installEcatMasterHandler(KSHandle hMaster, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installEcatMasterHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installEcatMasterHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installEcatMasterHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installEcatMasterHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installEcatMasterHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, eventCode, hSignal, flags);
}

//------ KS_queryEcatMasterState ------
Error __stdcall KS_queryEcatMasterState(KSHandle hMaster, KSEcatMasterState* pMasterState, int flags) {
  typedef Error (__stdcall* KS_queryEcatMasterStateProc)(KSHandle, KSEcatMasterState*, int);
  static KS_queryEcatMasterStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryEcatMasterStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryEcatMasterState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryEcatMasterState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, pMasterState, flags);
}

//------ KS_changeEcatMasterState ------
Error __stdcall KS_changeEcatMasterState(KSHandle hMaster, int state, int flags) {
  typedef Error (__stdcall* KS_changeEcatMasterStateProc)(KSHandle, int, int);
  static KS_changeEcatMasterStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_changeEcatMasterStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_changeEcatMasterState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_changeEcatMasterState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, state, flags);
}

//------ KS_addEcatRedundancy ------
Error __stdcall KS_addEcatRedundancy(KSHandle hMaster, KSHandle hConnection, int flags) {
  typedef Error (__stdcall* KS_addEcatRedundancyProc)(KSHandle, KSHandle, int);
  static KS_addEcatRedundancyProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_addEcatRedundancyProc)GetProcAddress(_hKitharaKrtsDll, "KS_addEcatRedundancy");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_addEcatRedundancy not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, hConnection, flags);
}

//------ KS_createEcatSlave ------
Error __stdcall KS_createEcatSlave(KSHandle hMaster, KSHandle* phSlave, int id, int position, int vendor, int product, int revision, int flags) {
  typedef Error (__stdcall* KS_createEcatSlaveProc)(KSHandle, KSHandle*, int, int, int, int, int, int);
  static KS_createEcatSlaveProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createEcatSlaveProc)GetProcAddress(_hKitharaKrtsDll, "KS_createEcatSlave");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createEcatSlave not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, phSlave, id, position, vendor, product, revision, flags);
}

//------ KS_createEcatSlaveIndirect ------
Error __stdcall KS_createEcatSlaveIndirect(KSHandle hMaster, KSHandle* phSlave, const KSEcatSlaveState* pSlaveState, int flags) {
  typedef Error (__stdcall* KS_createEcatSlaveIndirectProc)(KSHandle, KSHandle*, const KSEcatSlaveState*, int);
  static KS_createEcatSlaveIndirectProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createEcatSlaveIndirectProc)GetProcAddress(_hKitharaKrtsDll, "KS_createEcatSlaveIndirect");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createEcatSlaveIndirect not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, phSlave, pSlaveState, flags);
}

//------ KS_enumEcatSlaves ------
Error __stdcall KS_enumEcatSlaves(KSHandle hMaster, int index, KSEcatSlaveState* pSlaveState, int flags) {
  typedef Error (__stdcall* KS_enumEcatSlavesProc)(KSHandle, int, KSEcatSlaveState*, int);
  static KS_enumEcatSlavesProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumEcatSlavesProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumEcatSlaves");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumEcatSlaves not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, index, pSlaveState, flags);
}

//------ KS_deleteEcatSlave ------
Error __stdcall KS_deleteEcatSlave(KSHandle hSlave) {
  typedef Error (__stdcall* KS_deleteEcatSlaveProc)(KSHandle);
  static KS_deleteEcatSlaveProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_deleteEcatSlaveProc)GetProcAddress(_hKitharaKrtsDll, "KS_deleteEcatSlave");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_deleteEcatSlave not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave);
}

//------ KS_writeEcatSlaveId ------
Error __stdcall KS_writeEcatSlaveId(KSHandle hSlave, int id, int flags) {
  typedef Error (__stdcall* KS_writeEcatSlaveIdProc)(KSHandle, int, int);
  static KS_writeEcatSlaveIdProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_writeEcatSlaveIdProc)GetProcAddress(_hKitharaKrtsDll, "KS_writeEcatSlaveId");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_writeEcatSlaveId not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, id, flags);
}

//------ KS_queryEcatSlaveState ------
Error __stdcall KS_queryEcatSlaveState(KSHandle hSlave, KSEcatSlaveState* pSlaveState, int flags) {
  typedef Error (__stdcall* KS_queryEcatSlaveStateProc)(KSHandle, KSEcatSlaveState*, int);
  static KS_queryEcatSlaveStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryEcatSlaveStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryEcatSlaveState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryEcatSlaveState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, pSlaveState, flags);
}

//------ KS_changeEcatSlaveState ------
Error __stdcall KS_changeEcatSlaveState(KSHandle hSlave, int state, int flags) {
  typedef Error (__stdcall* KS_changeEcatSlaveStateProc)(KSHandle, int, int);
  static KS_changeEcatSlaveStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_changeEcatSlaveStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_changeEcatSlaveState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_changeEcatSlaveState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, state, flags);
}

//------ KS_queryEcatSlaveInfo ------
Error __stdcall KS_queryEcatSlaveInfo(KSHandle hSlave, KSEcatSlaveInfo** ppSlaveInfo, int flags) {
  typedef Error (__stdcall* KS_queryEcatSlaveInfoProc)(KSHandle, KSEcatSlaveInfo**, int);
  static KS_queryEcatSlaveInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryEcatSlaveInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryEcatSlaveInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryEcatSlaveInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, ppSlaveInfo, flags);
}

//------ KS_queryEcatDataObjInfo ------
Error __stdcall KS_queryEcatDataObjInfo(KSHandle hSlave, int objIndex, KSEcatDataObjInfo** ppDataObjInfo, int flags) {
  typedef Error (__stdcall* KS_queryEcatDataObjInfoProc)(KSHandle, int, KSEcatDataObjInfo**, int);
  static KS_queryEcatDataObjInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryEcatDataObjInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryEcatDataObjInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryEcatDataObjInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objIndex, ppDataObjInfo, flags);
}

//------ KS_queryEcatDataVarInfo ------
Error __stdcall KS_queryEcatDataVarInfo(KSHandle hSlave, int objIndex, int varIndex, KSEcatDataVarInfo** ppDataVarInfo, int flags) {
  typedef Error (__stdcall* KS_queryEcatDataVarInfoProc)(KSHandle, int, int, KSEcatDataVarInfo**, int);
  static KS_queryEcatDataVarInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryEcatDataVarInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryEcatDataVarInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryEcatDataVarInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objIndex, varIndex, ppDataVarInfo, flags);
}

//------ KS_enumEcatDataObjInfo ------
Error __stdcall KS_enumEcatDataObjInfo(KSHandle hSlave, int objEnumIndex, KSEcatDataObjInfo** ppDataObjInfo, int flags) {
  typedef Error (__stdcall* KS_enumEcatDataObjInfoProc)(KSHandle, int, KSEcatDataObjInfo**, int);
  static KS_enumEcatDataObjInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumEcatDataObjInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumEcatDataObjInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumEcatDataObjInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objEnumIndex, ppDataObjInfo, flags);
}

//------ KS_enumEcatDataVarInfo ------
Error __stdcall KS_enumEcatDataVarInfo(KSHandle hSlave, int objEnumIndex, int varEnumIndex, KSEcatDataVarInfo** ppDataVarInfo, int flags) {
  typedef Error (__stdcall* KS_enumEcatDataVarInfoProc)(KSHandle, int, int, KSEcatDataVarInfo**, int);
  static KS_enumEcatDataVarInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumEcatDataVarInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumEcatDataVarInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumEcatDataVarInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objEnumIndex, varEnumIndex, ppDataVarInfo, flags);
}

//------ KS_loadEcatInitCommands ------
Error __stdcall KS_loadEcatInitCommands(KSHandle hSlave, const char* filename, int flags) {
  typedef Error (__stdcall* KS_loadEcatInitCommandsProc)(KSHandle, const char*, int);
  static KS_loadEcatInitCommandsProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_loadEcatInitCommandsProc)GetProcAddress(_hKitharaKrtsDll, "KS_loadEcatInitCommands");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_loadEcatInitCommands not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, filename, flags);
}

//------ KS_createEcatDataSet ------
Error __stdcall KS_createEcatDataSet(KSHandle hObject, KSHandle* phSet, void** ppAppPtr, void** ppSysPtr, int flags) {
  typedef Error (__stdcall* KS_createEcatDataSetProc)(KSHandle, KSHandle*, void**, void**, int);
  static KS_createEcatDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createEcatDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_createEcatDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createEcatDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, phSet, ppAppPtr, ppSysPtr, flags);
}

//------ KS_deleteEcatDataSet ------
Error __stdcall KS_deleteEcatDataSet(KSHandle hSet) {
  typedef Error (__stdcall* KS_deleteEcatDataSetProc)(KSHandle);
  static KS_deleteEcatDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_deleteEcatDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_deleteEcatDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_deleteEcatDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet);
}

//------ KS_assignEcatDataSet ------
Error __stdcall KS_assignEcatDataSet(KSHandle hSet, KSHandle hObject, int syncIndex, int placement, int flags) {
  typedef Error (__stdcall* KS_assignEcatDataSetProc)(KSHandle, KSHandle, int, int, int);
  static KS_assignEcatDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_assignEcatDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_assignEcatDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_assignEcatDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, hObject, syncIndex, placement, flags);
}

//------ KS_readEcatDataSet ------
Error __stdcall KS_readEcatDataSet(KSHandle hSet, int flags) {
  typedef Error (__stdcall* KS_readEcatDataSetProc)(KSHandle, int);
  static KS_readEcatDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readEcatDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_readEcatDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readEcatDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, flags);
}

//------ KS_postEcatDataSet ------
Error __stdcall KS_postEcatDataSet(KSHandle hSet, int flags) {
  typedef Error (__stdcall* KS_postEcatDataSetProc)(KSHandle, int);
  static KS_postEcatDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_postEcatDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_postEcatDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_postEcatDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, flags);
}

//------ KS_installEcatDataSetHandler ------
Error __stdcall KS_installEcatDataSetHandler(KSHandle hSet, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installEcatDataSetHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installEcatDataSetHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installEcatDataSetHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installEcatDataSetHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installEcatDataSetHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, eventCode, hSignal, flags);
}

//------ KS_changeEcatState ------
Error __stdcall KS_changeEcatState(KSHandle hObject, int state, int flags) {
  typedef Error (__stdcall* KS_changeEcatStateProc)(KSHandle, int, int);
  static KS_changeEcatStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_changeEcatStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_changeEcatState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_changeEcatState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, state, flags);
}

//------ KS_installEcatHandler ------
Error __stdcall KS_installEcatHandler(KSHandle hObject, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installEcatHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installEcatHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installEcatHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installEcatHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installEcatHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, eventCode, hSignal, flags);
}

//------ KS_getEcatDataObjAddress ------
Error __stdcall KS_getEcatDataObjAddress(KSHandle hSet, KSHandle hObject, int pdoIndex, int varIndex, void** ppAppPtr, void** ppSysPtr, int* pBitOffset, int* pBitLength, int flags) {
  typedef Error (__stdcall* KS_getEcatDataObjAddressProc)(KSHandle, KSHandle, int, int, void**, void**, int*, int*, int);
  static KS_getEcatDataObjAddressProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getEcatDataObjAddressProc)GetProcAddress(_hKitharaKrtsDll, "KS_getEcatDataObjAddress");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getEcatDataObjAddress not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, hObject, pdoIndex, varIndex, ppAppPtr, ppSysPtr, pBitOffset, pBitLength, flags);
}

//------ KS_readEcatDataObj ------
Error __stdcall KS_readEcatDataObj(KSHandle hObject, int objIndex, int varIndex, void* pData, int* pSize, int flags) {
  typedef Error (__stdcall* KS_readEcatDataObjProc)(KSHandle, int, int, void*, int*, int);
  static KS_readEcatDataObjProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readEcatDataObjProc)GetProcAddress(_hKitharaKrtsDll, "KS_readEcatDataObj");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readEcatDataObj not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, objIndex, varIndex, pData, pSize, flags);
}

//------ KS_postEcatDataObj ------
Error __stdcall KS_postEcatDataObj(KSHandle hObject, int objIndex, int varIndex, const void* pData, int size, int flags) {
  typedef Error (__stdcall* KS_postEcatDataObjProc)(KSHandle, int, int, const void*, int, int);
  static KS_postEcatDataObjProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_postEcatDataObjProc)GetProcAddress(_hKitharaKrtsDll, "KS_postEcatDataObj");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_postEcatDataObj not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, objIndex, varIndex, pData, size, flags);
}

//------ KS_setEcatPdoAssign ------
Error __stdcall KS_setEcatPdoAssign(KSHandle hObject, int syncIndex, int pdoIndex, int flags) {
  typedef Error (__stdcall* KS_setEcatPdoAssignProc)(KSHandle, int, int, int);
  static KS_setEcatPdoAssignProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setEcatPdoAssignProc)GetProcAddress(_hKitharaKrtsDll, "KS_setEcatPdoAssign");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setEcatPdoAssign not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, syncIndex, pdoIndex, flags);
}

//------ KS_setEcatPdoMapping ------
Error __stdcall KS_setEcatPdoMapping(KSHandle hObject, int pdoIndex, int objIndex, int varIndex, int bitLength, int flags) {
  typedef Error (__stdcall* KS_setEcatPdoMappingProc)(KSHandle, int, int, int, int, int);
  static KS_setEcatPdoMappingProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setEcatPdoMappingProc)GetProcAddress(_hKitharaKrtsDll, "KS_setEcatPdoMapping");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setEcatPdoMapping not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, pdoIndex, objIndex, varIndex, bitLength, flags);
}

//------ KS_uploadEcatFile ------
Error __stdcall KS_uploadEcatFile(KSHandle hObject, const char* fileName, int password, void* pData, int* pSize, int flags) {
  typedef Error (__stdcall* KS_uploadEcatFileProc)(KSHandle, const char*, int, void*, int*, int);
  static KS_uploadEcatFileProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_uploadEcatFileProc)GetProcAddress(_hKitharaKrtsDll, "KS_uploadEcatFile");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_uploadEcatFile not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, fileName, password, pData, pSize, flags);
}

//------ KS_downloadEcatFile ------
Error __stdcall KS_downloadEcatFile(KSHandle hObject, const char* fileName, int password, const void* pData, int size, int flags) {
  typedef Error (__stdcall* KS_downloadEcatFileProc)(KSHandle, const char*, int, const void*, int, int);
  static KS_downloadEcatFileProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_downloadEcatFileProc)GetProcAddress(_hKitharaKrtsDll, "KS_downloadEcatFile");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_downloadEcatFile not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, fileName, password, pData, size, flags);
}

//------ KS_readEcatSlaveMem ------
Error __stdcall KS_readEcatSlaveMem(KSHandle hSlave, int address, void* pData, int size, int flags) {
  typedef Error (__stdcall* KS_readEcatSlaveMemProc)(KSHandle, int, void*, int, int);
  static KS_readEcatSlaveMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readEcatSlaveMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_readEcatSlaveMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readEcatSlaveMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, address, pData, size, flags);
}

//------ KS_writeEcatSlaveMem ------
Error __stdcall KS_writeEcatSlaveMem(KSHandle hSlave, int address, const void* pData, int size, int flags) {
  typedef Error (__stdcall* KS_writeEcatSlaveMemProc)(KSHandle, int, const void*, int, int);
  static KS_writeEcatSlaveMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_writeEcatSlaveMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_writeEcatSlaveMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_writeEcatSlaveMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, address, pData, size, flags);
}

//------ KS_activateEcatDcMode ------
Error __stdcall KS_activateEcatDcMode(KSHandle hObject, int64ref startTime, int cycleTime, int shiftTime, KSHandle hTimer, int flags) {
  typedef Error (__stdcall* KS_activateEcatDcModeProc)(KSHandle, int64ref, int, int, KSHandle, int);
  static KS_activateEcatDcModeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_activateEcatDcModeProc)GetProcAddress(_hKitharaKrtsDll, "KS_activateEcatDcMode");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_activateEcatDcMode not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, startTime, cycleTime, shiftTime, hTimer, flags);
}

//------ KS_enumEcatDcOpModes ------
Error __stdcall KS_enumEcatDcOpModes(KSHandle hSlave, int index, char* pDcOpModeBuf, char* pDescriptionBuf, int flags) {
  typedef Error (__stdcall* KS_enumEcatDcOpModesProc)(KSHandle, int, char*, char*, int);
  static KS_enumEcatDcOpModesProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumEcatDcOpModesProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumEcatDcOpModes");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumEcatDcOpModes not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, index, pDcOpModeBuf, pDescriptionBuf, flags);
}

//------ KS_lookupEcatDcOpMode ------
Error __stdcall KS_lookupEcatDcOpMode(KSHandle hSlave, const char* dcOpMode, KSEcatDcParams* pDcParams, int flags) {
  typedef Error (__stdcall* KS_lookupEcatDcOpModeProc)(KSHandle, const char*, KSEcatDcParams*, int);
  static KS_lookupEcatDcOpModeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_lookupEcatDcOpModeProc)GetProcAddress(_hKitharaKrtsDll, "KS_lookupEcatDcOpMode");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_lookupEcatDcOpMode not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, dcOpMode, pDcParams, flags);
}

//------ KS_configEcatDcOpMode ------
Error __stdcall KS_configEcatDcOpMode(KSHandle hSlave, const char* dcOpMode, const KSEcatDcParams* pDcParams, int flags) {
  typedef Error (__stdcall* KS_configEcatDcOpModeProc)(KSHandle, const char*, const KSEcatDcParams*, int);
  static KS_configEcatDcOpModeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_configEcatDcOpModeProc)GetProcAddress(_hKitharaKrtsDll, "KS_configEcatDcOpMode");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_configEcatDcOpMode not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, dcOpMode, pDcParams, flags);
}

//------ KS_execEcatCommand ------
Error __stdcall KS_execEcatCommand(KSHandle hObject, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execEcatCommandProc)(KSHandle, int, void*, int);
  static KS_execEcatCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execEcatCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execEcatCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execEcatCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, command, pParam, flags);
}

//------ KS_getEcatAlias ------
Error __stdcall KS_getEcatAlias(KSHandle hObject, int objIndex, int varIndex, char* pAliasBuf, int flags) {
  typedef Error (__stdcall* KS_getEcatAliasProc)(KSHandle, int, int, char*, int);
  static KS_getEcatAliasProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getEcatAliasProc)GetProcAddress(_hKitharaKrtsDll, "KS_getEcatAlias");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getEcatAlias not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, objIndex, varIndex, pAliasBuf, flags);
}

//------ KS_setEcatAlias ------
Error __stdcall KS_setEcatAlias(KSHandle hObject, int objIndex, int varIndex, const char* alias, int flags) {
  typedef Error (__stdcall* KS_setEcatAliasProc)(KSHandle, int, int, const char*, int);
  static KS_setEcatAliasProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setEcatAliasProc)GetProcAddress(_hKitharaKrtsDll, "KS_setEcatAlias");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setEcatAlias not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, objIndex, varIndex, alias, flags);
}

//------ KS_openEcatSlaveDevice ------
Error __stdcall KS_openEcatSlaveDevice(KSHandle* phSlaveDevice, const char* name, int flags) {
  typedef Error (__stdcall* KS_openEcatSlaveDeviceProc)(KSHandle*, const char*, int);
  static KS_openEcatSlaveDeviceProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openEcatSlaveDeviceProc)GetProcAddress(_hKitharaKrtsDll, "KS_openEcatSlaveDevice");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openEcatSlaveDevice not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phSlaveDevice, name, flags);
}

//------ KS_closeEcatSlaveDevice ------
Error __stdcall KS_closeEcatSlaveDevice(KSHandle hSlaveDevice, int flags) {
  typedef Error (__stdcall* KS_closeEcatSlaveDeviceProc)(KSHandle, int);
  static KS_closeEcatSlaveDeviceProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeEcatSlaveDeviceProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeEcatSlaveDevice");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeEcatSlaveDevice not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlaveDevice, flags);
}

//------ KS_queryEcatSlaveDeviceState ------
Error __stdcall KS_queryEcatSlaveDeviceState(KSHandle hSlaveDevice, KSEcatSlaveDeviceState* pSlaveState, int flags) {
  typedef Error (__stdcall* KS_queryEcatSlaveDeviceStateProc)(KSHandle, KSEcatSlaveDeviceState*, int);
  static KS_queryEcatSlaveDeviceStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryEcatSlaveDeviceStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryEcatSlaveDeviceState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryEcatSlaveDeviceState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlaveDevice, pSlaveState, flags);
}

//------ KS_createEcatEapNode ------
Error __stdcall KS_createEcatEapNode(KSHandle* phNode, KSHandle hConnection, const char* networkFile, int flags) {
  typedef Error (__stdcall* KS_createEcatEapNodeProc)(KSHandle*, KSHandle, const char*, int);
  static KS_createEcatEapNodeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createEcatEapNodeProc)GetProcAddress(_hKitharaKrtsDll, "KS_createEcatEapNode");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createEcatEapNode not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phNode, hConnection, networkFile, flags);
}

//------ KS_closeEcatEapNode ------
Error __stdcall KS_closeEcatEapNode(KSHandle hNode, int flags) {
  typedef Error (__stdcall* KS_closeEcatEapNodeProc)(KSHandle, int);
  static KS_closeEcatEapNodeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeEcatEapNodeProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeEcatEapNode");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeEcatEapNode not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hNode, flags);
}

//------ KS_queryEcatEapNodeState ------
Error __stdcall KS_queryEcatEapNodeState(KSHandle hNode, KSEcatEapNodeState* pNodeState, int flags) {
  typedef Error (__stdcall* KS_queryEcatEapNodeStateProc)(KSHandle, KSEcatEapNodeState*, int);
  static KS_queryEcatEapNodeStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryEcatEapNodeStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryEcatEapNodeState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryEcatEapNodeState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hNode, pNodeState, flags);
}

//------ KS_createEcatEapProxy ------
Error __stdcall KS_createEcatEapProxy(KSHandle hNode, KSHandle* phProxy, const KSEcatEapNodeId* pNodeId, int flags) {
  typedef Error (__stdcall* KS_createEcatEapProxyProc)(KSHandle, KSHandle*, const KSEcatEapNodeId*, int);
  static KS_createEcatEapProxyProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createEcatEapProxyProc)GetProcAddress(_hKitharaKrtsDll, "KS_createEcatEapProxy");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createEcatEapProxy not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hNode, phProxy, pNodeId, flags);
}

//------ KS_deleteEcatEapProxy ------
Error __stdcall KS_deleteEcatEapProxy(KSHandle hProxy, int flags) {
  typedef Error (__stdcall* KS_deleteEcatEapProxyProc)(KSHandle, int);
  static KS_deleteEcatEapProxyProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_deleteEcatEapProxyProc)GetProcAddress(_hKitharaKrtsDll, "KS_deleteEcatEapProxy");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_deleteEcatEapProxy not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hProxy, flags);
}

//------ KS_queryEcatEapProxyState ------
Error __stdcall KS_queryEcatEapProxyState(KSHandle hProxy, KSEcatEapProxyState* pProxyState, int flags) {
  typedef Error (__stdcall* KS_queryEcatEapProxyStateProc)(KSHandle, KSEcatEapProxyState*, int);
  static KS_queryEcatEapProxyStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryEcatEapProxyStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryEcatEapProxyState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryEcatEapProxyState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hProxy, pProxyState, flags);
}

//------ _initEtherCATModule ------
static void _initEtherCATModule() {
  _registerKernelAddress("KS_queryEcatMasterState", (CallBackProc)KS_queryEcatMasterState);
  _registerKernelAddress("KS_changeEcatMasterState", (CallBackProc)KS_changeEcatMasterState);
  _registerKernelAddress("KS_addEcatRedundancy", (CallBackProc)KS_addEcatRedundancy);
  _registerKernelAddress("KS_enumEcatSlaves", (CallBackProc)KS_enumEcatSlaves);
  _registerKernelAddress("KS_writeEcatSlaveId", (CallBackProc)KS_writeEcatSlaveId);
  _registerKernelAddress("KS_queryEcatSlaveState", (CallBackProc)KS_queryEcatSlaveState);
  _registerKernelAddress("KS_changeEcatSlaveState", (CallBackProc)KS_changeEcatSlaveState);
  _registerKernelAddress("KS_queryEcatSlaveInfo", (CallBackProc)KS_queryEcatSlaveInfo);
  _registerKernelAddress("KS_queryEcatDataObjInfo", (CallBackProc)KS_queryEcatDataObjInfo);
  _registerKernelAddress("KS_queryEcatDataVarInfo", (CallBackProc)KS_queryEcatDataVarInfo);
  _registerKernelAddress("KS_enumEcatDataObjInfo", (CallBackProc)KS_enumEcatDataObjInfo);
  _registerKernelAddress("KS_enumEcatDataVarInfo", (CallBackProc)KS_enumEcatDataVarInfo);
  _registerKernelAddress("KS_assignEcatDataSet", (CallBackProc)KS_assignEcatDataSet);
  _registerKernelAddress("KS_readEcatDataSet", (CallBackProc)KS_readEcatDataSet);
  _registerKernelAddress("KS_postEcatDataSet", (CallBackProc)KS_postEcatDataSet);
  _registerKernelAddress("KS_installEcatDataSetHandler", (CallBackProc)KS_installEcatDataSetHandler);
  _registerKernelAddress("KS_changeEcatState", (CallBackProc)KS_changeEcatState);
  _registerKernelAddress("KS_installEcatHandler", (CallBackProc)KS_installEcatHandler);
  _registerKernelAddress("KS_getEcatDataObjAddress", (CallBackProc)KS_getEcatDataObjAddress);
  _registerKernelAddress("KS_readEcatDataObj", (CallBackProc)KS_readEcatDataObj);
  _registerKernelAddress("KS_postEcatDataObj", (CallBackProc)KS_postEcatDataObj);
  _registerKernelAddress("KS_readEcatSlaveMem", (CallBackProc)KS_readEcatSlaveMem);
  _registerKernelAddress("KS_writeEcatSlaveMem", (CallBackProc)KS_writeEcatSlaveMem);
  _registerKernelAddress("KS_activateEcatDcMode", (CallBackProc)KS_activateEcatDcMode);
  _registerKernelAddress("KS_enumEcatDcOpModes", (CallBackProc)KS_enumEcatDcOpModes);
  _registerKernelAddress("KS_lookupEcatDcOpMode", (CallBackProc)KS_lookupEcatDcOpMode);
  _registerKernelAddress("KS_configEcatDcOpMode", (CallBackProc)KS_configEcatDcOpMode);
  _registerKernelAddress("KS_execEcatCommand", (CallBackProc)KS_execEcatCommand);
  _registerKernelAddress("KS_getEcatAlias", (CallBackProc)KS_getEcatAlias);
  _registerKernelAddress("KS_setEcatAlias", (CallBackProc)KS_setEcatAlias);
  _registerKernelAddress("KS_openEcatSlaveDevice", (CallBackProc)KS_openEcatSlaveDevice);
  _registerKernelAddress("KS_closeEcatSlaveDevice", (CallBackProc)KS_closeEcatSlaveDevice);
  _registerKernelAddress("KS_queryEcatSlaveDeviceState", (CallBackProc)KS_queryEcatSlaveDeviceState);
  _registerKernelAddress("KS_createEcatEapNode", (CallBackProc)KS_createEcatEapNode);
  _registerKernelAddress("KS_closeEcatEapNode", (CallBackProc)KS_closeEcatEapNode);
  _registerKernelAddress("KS_queryEcatEapNodeState", (CallBackProc)KS_queryEcatEapNodeState);
  _registerKernelAddress("KS_createEcatEapProxy", (CallBackProc)KS_createEcatEapProxy);
  _registerKernelAddress("KS_deleteEcatEapProxy", (CallBackProc)KS_deleteEcatEapProxy);
  _registerKernelAddress("KS_queryEcatEapProxyState", (CallBackProc)KS_queryEcatEapProxyState);
}

#define KS_ETHERCAT_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// MultiFunction Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openBoard ------
Error __stdcall KS_openBoard(KSHandle* phBoard, const char* name, KSHandle hKernel, int flags) {
  typedef Error (__stdcall* KS_openBoardProc)(KSHandle*, const char*, KSHandle, int);
  static KS_openBoardProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openBoardProc)GetProcAddress(_hKitharaKrtsDll, "KS_openBoard");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openBoard not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phBoard, name, hKernel, flags);
}

//------ KS_closeBoard ------
Error __stdcall KS_closeBoard(KSHandle hBoard, int flags) {
  typedef Error (__stdcall* KS_closeBoardProc)(KSHandle, int);
  static KS_closeBoardProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeBoardProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeBoard");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeBoard not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, flags);
}

//------ KS_getBoardInfo ------
Error __stdcall KS_getBoardInfo(KSHandle hBoard, KSMfBoardInfo* pBoardInfo, int flags) {
  typedef Error (__stdcall* KS_getBoardInfoProc)(KSHandle, KSMfBoardInfo*, int);
  static KS_getBoardInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getBoardInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_getBoardInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getBoardInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, pBoardInfo, flags);
}

//------ KS_setBoardObject ------
Error __stdcall KS_setBoardObject(KSHandle hBoard, void* pAppAddr, void* pSysAddr) {
  typedef Error (__stdcall* KS_setBoardObjectProc)(KSHandle, void*, void*);
  static KS_setBoardObjectProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setBoardObjectProc)GetProcAddress(_hKitharaKrtsDll, "KS_setBoardObject");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setBoardObject not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, pAppAddr, pSysAddr);
}

//------ KS_getBoardObject ------
Error __stdcall KS_getBoardObject(KSHandle hBoard, void** ppAddr) {
  typedef Error (__stdcall* KS_getBoardObjectProc)(KSHandle, void**);
  static KS_getBoardObjectProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getBoardObjectProc)GetProcAddress(_hKitharaKrtsDll, "KS_getBoardObject");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getBoardObject not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, ppAddr);
}

//------ KS_installBoardHandler ------
Error __stdcall KS_installBoardHandler(KSHandle hBoard, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installBoardHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installBoardHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installBoardHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installBoardHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installBoardHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, eventCode, hSignal, flags);
}

//------ KS_execBoardCommand ------
Error __stdcall KS_execBoardCommand(KSHandle hBoard, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execBoardCommandProc)(KSHandle, int, void*, int);
  static KS_execBoardCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execBoardCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execBoardCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execBoardCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, command, pParam, flags);
}

//------ KS_readBoardRegister ------
Error __stdcall KS_readBoardRegister(KSHandle hBoard, int rangeIndex, int offset, int* pValue, int flags) {
  typedef Error (__stdcall* KS_readBoardRegisterProc)(KSHandle, int, int, int*, int);
  static KS_readBoardRegisterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readBoardRegisterProc)GetProcAddress(_hKitharaKrtsDll, "KS_readBoardRegister");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readBoardRegister not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, rangeIndex, offset, pValue, flags);
}

//------ KS_writeBoardRegister ------
Error __stdcall KS_writeBoardRegister(KSHandle hBoard, int rangeIndex, int offset, int value, int flags) {
  typedef Error (__stdcall* KS_writeBoardRegisterProc)(KSHandle, int, int, int, int);
  static KS_writeBoardRegisterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_writeBoardRegisterProc)GetProcAddress(_hKitharaKrtsDll, "KS_writeBoardRegister");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_writeBoardRegister not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, rangeIndex, offset, value, flags);
}

//------ KS_getDigitalBit ------
Error __stdcall KS_getDigitalBit(KSHandle hBoard, int channel, uint* pValue, int flags) {
  typedef Error (__stdcall* KS_getDigitalBitProc)(KSHandle, int, uint*, int);
  static KS_getDigitalBitProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getDigitalBitProc)GetProcAddress(_hKitharaKrtsDll, "KS_getDigitalBit");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getDigitalBit not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, channel, pValue, flags);
}

//------ KS_getDigitalPort ------
Error __stdcall KS_getDigitalPort(KSHandle hBoard, int port, uint* pValue, int flags) {
  typedef Error (__stdcall* KS_getDigitalPortProc)(KSHandle, int, uint*, int);
  static KS_getDigitalPortProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getDigitalPortProc)GetProcAddress(_hKitharaKrtsDll, "KS_getDigitalPort");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getDigitalPort not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, port, pValue, flags);
}

//------ KS_setDigitalBit ------
Error __stdcall KS_setDigitalBit(KSHandle hBoard, int channel, uint value, int flags) {
  typedef Error (__stdcall* KS_setDigitalBitProc)(KSHandle, int, uint, int);
  static KS_setDigitalBitProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setDigitalBitProc)GetProcAddress(_hKitharaKrtsDll, "KS_setDigitalBit");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setDigitalBit not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, channel, value, flags);
}

//------ KS_setDigitalPort ------
Error __stdcall KS_setDigitalPort(KSHandle hBoard, int port, uint value, uint mask, int flags) {
  typedef Error (__stdcall* KS_setDigitalPortProc)(KSHandle, int, uint, uint, int);
  static KS_setDigitalPortProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setDigitalPortProc)GetProcAddress(_hKitharaKrtsDll, "KS_setDigitalPort");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setDigitalPort not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, port, value, mask, flags);
}

//------ KS_configAnalogChannel ------
Error __stdcall KS_configAnalogChannel(KSHandle hBoard, int channel, int gain, int mode, int flags) {
  typedef Error (__stdcall* KS_configAnalogChannelProc)(KSHandle, int, int, int, int);
  static KS_configAnalogChannelProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_configAnalogChannelProc)GetProcAddress(_hKitharaKrtsDll, "KS_configAnalogChannel");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_configAnalogChannel not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, channel, gain, mode, flags);
}

//------ KS_getAnalogValue ------
Error __stdcall KS_getAnalogValue(KSHandle hBoard, int channel, uint* pValue, int flags) {
  typedef Error (__stdcall* KS_getAnalogValueProc)(KSHandle, int, uint*, int);
  static KS_getAnalogValueProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getAnalogValueProc)GetProcAddress(_hKitharaKrtsDll, "KS_getAnalogValue");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getAnalogValue not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, channel, pValue, flags);
}

//------ KS_initAnalogStream ------
Error __stdcall KS_initAnalogStream(KSHandle hBoard, int* pChannelSequence, int sequenceLength, int sequenceCount, int period, int flags) {
  typedef Error (__stdcall* KS_initAnalogStreamProc)(KSHandle, int*, int, int, int, int);
  static KS_initAnalogStreamProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_initAnalogStreamProc)GetProcAddress(_hKitharaKrtsDll, "KS_initAnalogStream");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_initAnalogStream not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, pChannelSequence, sequenceLength, sequenceCount, period, flags);
}

//------ KS_startAnalogStream ------
Error __stdcall KS_startAnalogStream(KSHandle hBoard, int flags) {
  typedef Error (__stdcall* KS_startAnalogStreamProc)(KSHandle, int);
  static KS_startAnalogStreamProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_startAnalogStreamProc)GetProcAddress(_hKitharaKrtsDll, "KS_startAnalogStream");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_startAnalogStream not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, flags);
}

//------ KS_getAnalogStream ------
Error __stdcall KS_getAnalogStream(KSHandle hBoard, void* pBuffer, int length, int* pLength, int flags) {
  typedef Error (__stdcall* KS_getAnalogStreamProc)(KSHandle, void*, int, int*, int);
  static KS_getAnalogStreamProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getAnalogStreamProc)GetProcAddress(_hKitharaKrtsDll, "KS_getAnalogStream");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getAnalogStream not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, pBuffer, length, pLength, flags);
}

//------ KS_valueToVolt ------
Error __stdcall KS_valueToVolt(KSHandle hBoard, int gain, int mode, uint value, float* pVolt, int flags) {
  typedef Error (__stdcall* KS_valueToVoltProc)(KSHandle, int, int, uint, float*, int);
  static KS_valueToVoltProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_valueToVoltProc)GetProcAddress(_hKitharaKrtsDll, "KS_valueToVolt");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_valueToVolt not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, gain, mode, value, pVolt, flags);
}

//------ KS_setAnalogValue ------
Error __stdcall KS_setAnalogValue(KSHandle hBoard, int channel, int mode, int value, int flags) {
  typedef Error (__stdcall* KS_setAnalogValueProc)(KSHandle, int, int, int, int);
  static KS_setAnalogValueProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_setAnalogValueProc)GetProcAddress(_hKitharaKrtsDll, "KS_setAnalogValue");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_setAnalogValue not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, channel, mode, value, flags);
}

//------ KS_voltToValue ------
Error __stdcall KS_voltToValue(KSHandle hBoard, int mode, float volt, uint* pValue, int flags) {
  typedef Error (__stdcall* KS_voltToValueProc)(KSHandle, int, float, uint*, int);
  static KS_voltToValueProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_voltToValueProc)GetProcAddress(_hKitharaKrtsDll, "KS_voltToValue");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_voltToValue not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hBoard, mode, volt, pValue, flags);
}

//------ _initMultiFunctionModule ------
static void _initMultiFunctionModule() {
  _registerKernelAddress("KS_getBoardInfo", (CallBackProc)KS_getBoardInfo);
  _registerKernelAddress("KS_setBoardObject", (CallBackProc)KS_setBoardObject);
  _registerKernelAddress("KS_getBoardObject", (CallBackProc)KS_getBoardObject);
  _registerKernelAddress("KS_installBoardHandler", (CallBackProc)KS_installBoardHandler);
  _registerKernelAddress("KS_execBoardCommand", (CallBackProc)KS_execBoardCommand);
  _registerKernelAddress("KS_readBoardRegister", (CallBackProc)KS_readBoardRegister);
  _registerKernelAddress("KS_writeBoardRegister", (CallBackProc)KS_writeBoardRegister);
  _registerKernelAddress("KS_getDigitalBit", (CallBackProc)KS_getDigitalBit);
  _registerKernelAddress("KS_getDigitalPort", (CallBackProc)KS_getDigitalPort);
  _registerKernelAddress("KS_setDigitalBit", (CallBackProc)KS_setDigitalBit);
  _registerKernelAddress("KS_setDigitalPort", (CallBackProc)KS_setDigitalPort);
  _registerKernelAddress("KS_configAnalogChannel", (CallBackProc)KS_configAnalogChannel);
  _registerKernelAddress("KS_getAnalogValue", (CallBackProc)KS_getAnalogValue);
  _registerKernelAddress("KS_initAnalogStream", (CallBackProc)KS_initAnalogStream);
  _registerKernelAddress("KS_startAnalogStream", (CallBackProc)KS_startAnalogStream);
  _registerKernelAddress("KS_getAnalogStream", (CallBackProc)KS_getAnalogStream);
  _registerKernelAddress("KS_valueToVolt", (CallBackProc)KS_valueToVolt);
  _registerKernelAddress("KS_setAnalogValue", (CallBackProc)KS_setAnalogValue);
  _registerKernelAddress("KS_voltToValue", (CallBackProc)KS_voltToValue);
}

#define KS_MULTIFUNCTION_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// CAN Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openCanFd ------
Error __stdcall KS_openCanFd(KSHandle* phCanFd, const char* name, int port, const KSCanFdConfig* pCanFdConfig, int flags) {
  typedef Error (__stdcall* KS_openCanFdProc)(KSHandle*, const char*, int, const KSCanFdConfig*, int);
  static KS_openCanFdProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openCanFdProc)GetProcAddress(_hKitharaKrtsDll, "KS_openCanFd");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openCanFd not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phCanFd, name, port, pCanFdConfig, flags);
}

//------ KS_closeCanFd ------
Error __stdcall KS_closeCanFd(KSHandle hCanFd, int flags) {
  typedef Error (__stdcall* KS_closeCanFdProc)(KSHandle, int);
  static KS_closeCanFdProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeCanFdProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeCanFd");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeCanFd not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCanFd, flags);
}

//------ KS_xmitCanFdMsg ------
Error __stdcall KS_xmitCanFdMsg(KSHandle hCanFd, const KSCanFdMsg* pCanFdMsg, int flags) {
  typedef Error (__stdcall* KS_xmitCanFdMsgProc)(KSHandle, const KSCanFdMsg*, int);
  static KS_xmitCanFdMsgProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitCanFdMsgProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitCanFdMsg");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitCanFdMsg not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCanFd, pCanFdMsg, flags);
}

//------ KS_recvCanFdMsg ------
Error __stdcall KS_recvCanFdMsg(KSHandle hCanFd, KSCanFdMsg* pCanFdMsg, int flags) {
  typedef Error (__stdcall* KS_recvCanFdMsgProc)(KSHandle, KSCanFdMsg*, int);
  static KS_recvCanFdMsgProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvCanFdMsgProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvCanFdMsg");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvCanFdMsg not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCanFd, pCanFdMsg, flags);
}

//------ KS_installCanFdHandler ------
Error __stdcall KS_installCanFdHandler(KSHandle hCanFd, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installCanFdHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installCanFdHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installCanFdHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installCanFdHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installCanFdHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCanFd, eventCode, hSignal, flags);
}

//------ KS_getCanFdState ------
Error __stdcall KS_getCanFdState(KSHandle hCanFd, KSCanFdState* pCanFdState, int flags) {
  typedef Error (__stdcall* KS_getCanFdStateProc)(KSHandle, KSCanFdState*, int);
  static KS_getCanFdStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getCanFdStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getCanFdState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getCanFdState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCanFd, pCanFdState, flags);
}

//------ KS_execCanFdCommand ------
Error __stdcall KS_execCanFdCommand(KSHandle hCanFd, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execCanFdCommandProc)(KSHandle, int, void*, int);
  static KS_execCanFdCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execCanFdCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execCanFdCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execCanFdCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCanFd, command, pParam, flags);
}

//------ KS_openCan ------
Error __stdcall KS_openCan(KSHandle* phCan, const char* name, int port, int baudRate, int flags) {
  typedef Error (__stdcall* KS_openCanProc)(KSHandle*, const char*, int, int, int);
  static KS_openCanProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openCanProc)GetProcAddress(_hKitharaKrtsDll, "KS_openCan");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openCan not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phCan, name, port, baudRate, flags);
}

//------ KS_openCanEx ------
Error __stdcall KS_openCanEx(KSHandle* phCan, KSHandle hConnection, int port, int baudRate, int flags) {
  typedef Error (__stdcall* KS_openCanExProc)(KSHandle*, KSHandle, int, int, int);
  static KS_openCanExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openCanExProc)GetProcAddress(_hKitharaKrtsDll, "KS_openCanEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openCanEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phCan, hConnection, port, baudRate, flags);
}

//------ KS_closeCan ------
Error __stdcall KS_closeCan(KSHandle hCan) {
  typedef Error (__stdcall* KS_closeCanProc)(KSHandle);
  static KS_closeCanProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeCanProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeCan");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeCan not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCan);
}

//------ KS_xmitCanMsg ------
Error __stdcall KS_xmitCanMsg(KSHandle hCan, const KSCanMsg* pCanMsg, int flags) {
  typedef Error (__stdcall* KS_xmitCanMsgProc)(KSHandle, const KSCanMsg*, int);
  static KS_xmitCanMsgProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitCanMsgProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitCanMsg");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitCanMsg not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCan, pCanMsg, flags);
}

//------ KS_recvCanMsg ------
Error __stdcall KS_recvCanMsg(KSHandle hCan, KSCanMsg* pCanMsg, int flags) {
  typedef Error (__stdcall* KS_recvCanMsgProc)(KSHandle, KSCanMsg*, int);
  static KS_recvCanMsgProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvCanMsgProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvCanMsg");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvCanMsg not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCan, pCanMsg, flags);
}

//------ KS_installCanHandler ------
Error __stdcall KS_installCanHandler(KSHandle hCan, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installCanHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installCanHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installCanHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installCanHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installCanHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCan, eventCode, hSignal, flags);
}

//------ KS_getCanState ------
Error __stdcall KS_getCanState(KSHandle hCan, KSCanState* pCanState) {
  typedef Error (__stdcall* KS_getCanStateProc)(KSHandle, KSCanState*);
  static KS_getCanStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getCanStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getCanState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getCanState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCan, pCanState);
}

//------ KS_execCanCommand ------
Error __stdcall KS_execCanCommand(KSHandle hCan, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execCanCommandProc)(KSHandle, int, void*, int);
  static KS_execCanCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execCanCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execCanCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execCanCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCan, command, pParam, flags);
}

//------ _initCANModule ------
static void _initCANModule() {
  _registerKernelAddress("KS_xmitCanFdMsg", (CallBackProc)KS_xmitCanFdMsg);
  _registerKernelAddress("KS_recvCanFdMsg", (CallBackProc)KS_recvCanFdMsg);
  _registerKernelAddress("KS_installCanFdHandler", (CallBackProc)KS_installCanFdHandler);
  _registerKernelAddress("KS_getCanFdState", (CallBackProc)KS_getCanFdState);
  _registerKernelAddress("KS_execCanFdCommand", (CallBackProc)KS_execCanFdCommand);
  _registerKernelAddress("KS_xmitCanMsg", (CallBackProc)KS_xmitCanMsg);
  _registerKernelAddress("KS_recvCanMsg", (CallBackProc)KS_recvCanMsg);
  _registerKernelAddress("KS_installCanHandler", (CallBackProc)KS_installCanHandler);
  _registerKernelAddress("KS_getCanState", (CallBackProc)KS_getCanState);
  _registerKernelAddress("KS_execCanCommand", (CallBackProc)KS_execCanCommand);
}

#define KS_CAN_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// LIN Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openLin ------
Error __stdcall KS_openLin(KSHandle* phLin, const char* pDeviceName, int port, const KSLinProperties* pProperties, int flags) {
  typedef Error (__stdcall* KS_openLinProc)(KSHandle*, const char*, int, const KSLinProperties*, int);
  static KS_openLinProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openLinProc)GetProcAddress(_hKitharaKrtsDll, "KS_openLin");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openLin not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phLin, pDeviceName, port, pProperties, flags);
}

//------ KS_openLinEx ------
Error __stdcall KS_openLinEx(KSHandle* phLin, KSHandle hLinDevice, const KSLinProperties* pProperties, int flags) {
  typedef Error (__stdcall* KS_openLinExProc)(KSHandle*, KSHandle, const KSLinProperties*, int);
  static KS_openLinExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openLinExProc)GetProcAddress(_hKitharaKrtsDll, "KS_openLinEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openLinEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phLin, hLinDevice, pProperties, flags);
}

//------ KS_closeLin ------
Error __stdcall KS_closeLin(KSHandle hLin, int flags) {
  typedef Error (__stdcall* KS_closeLinProc)(KSHandle, int);
  static KS_closeLinProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeLinProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeLin");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeLin not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hLin, flags);
}

//------ KS_installLinHandler ------
Error __stdcall KS_installLinHandler(KSHandle hLin, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installLinHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installLinHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installLinHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installLinHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installLinHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hLin, eventCode, hSignal, flags);
}

//------ KS_execLinCommand ------
Error __stdcall KS_execLinCommand(KSHandle hLin, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execLinCommandProc)(KSHandle, int, void*, int);
  static KS_execLinCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execLinCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execLinCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execLinCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hLin, command, pParam, flags);
}

//------ KS_xmitLinHeader ------
Error __stdcall KS_xmitLinHeader(KSHandle hLin, int identifier, int flags) {
  typedef Error (__stdcall* KS_xmitLinHeaderProc)(KSHandle, int, int);
  static KS_xmitLinHeaderProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitLinHeaderProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitLinHeader");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitLinHeader not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hLin, identifier, flags);
}

//------ KS_xmitLinResponse ------
Error __stdcall KS_xmitLinResponse(KSHandle hLin, const void* pData, int size, int flags) {
  typedef Error (__stdcall* KS_xmitLinResponseProc)(KSHandle, const void*, int, int);
  static KS_xmitLinResponseProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_xmitLinResponseProc)GetProcAddress(_hKitharaKrtsDll, "KS_xmitLinResponse");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_xmitLinResponse not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hLin, pData, size, flags);
}

//------ KS_recvLinMsg ------
Error __stdcall KS_recvLinMsg(KSHandle hLin, KSLinMsg* pLinMsg, int flags) {
  typedef Error (__stdcall* KS_recvLinMsgProc)(KSHandle, KSLinMsg*, int);
  static KS_recvLinMsgProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvLinMsgProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvLinMsg");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvLinMsg not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hLin, pLinMsg, flags);
}

//------ KS_getLinState ------
Error __stdcall KS_getLinState(KSHandle hLin, KSLinState* pLinState, int flags) {
  typedef Error (__stdcall* KS_getLinStateProc)(KSHandle, KSLinState*, int);
  static KS_getLinStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getLinStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_getLinState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getLinState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hLin, pLinState, flags);
}

//------ _initLINModule ------
static void _initLINModule() {
  _registerKernelAddress("KS_execLinCommand", (CallBackProc)KS_execLinCommand);
  _registerKernelAddress("KS_xmitLinHeader", (CallBackProc)KS_xmitLinHeader);
  _registerKernelAddress("KS_xmitLinResponse", (CallBackProc)KS_xmitLinResponse);
  _registerKernelAddress("KS_recvLinMsg", (CallBackProc)KS_recvLinMsg);
}

#define KS_LIN_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// CANopen Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createCanoMaster ------
Error __stdcall KS_createCanoMaster(KSHandle* phMaster, KSHandle hConnection, const char* libraryPath, const char* topologyFile, int flags) {
  typedef Error (__stdcall* KS_createCanoMasterProc)(KSHandle*, KSHandle, const char*, const char*, int);
  static KS_createCanoMasterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createCanoMasterProc)GetProcAddress(_hKitharaKrtsDll, "KS_createCanoMaster");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createCanoMaster not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phMaster, hConnection, libraryPath, topologyFile, flags);
}

//------ KS_closeCanoMaster ------
Error __stdcall KS_closeCanoMaster(KSHandle hMaster) {
  typedef Error (__stdcall* KS_closeCanoMasterProc)(KSHandle);
  static KS_closeCanoMasterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeCanoMasterProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeCanoMaster");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeCanoMaster not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster);
}

//------ KS_installCanoMasterHandler ------
Error __stdcall KS_installCanoMasterHandler(KSHandle hMaster, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installCanoMasterHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installCanoMasterHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installCanoMasterHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installCanoMasterHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installCanoMasterHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, eventCode, hSignal, flags);
}

//------ KS_queryCanoMasterState ------
Error __stdcall KS_queryCanoMasterState(KSHandle hMaster, KSCanoMasterState* pMasterState, int flags) {
  typedef Error (__stdcall* KS_queryCanoMasterStateProc)(KSHandle, KSCanoMasterState*, int);
  static KS_queryCanoMasterStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryCanoMasterStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryCanoMasterState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryCanoMasterState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, pMasterState, flags);
}

//------ KS_changeCanoMasterState ------
Error __stdcall KS_changeCanoMasterState(KSHandle hMaster, int state, int flags) {
  typedef Error (__stdcall* KS_changeCanoMasterStateProc)(KSHandle, int, int);
  static KS_changeCanoMasterStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_changeCanoMasterStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_changeCanoMasterState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_changeCanoMasterState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, state, flags);
}

//------ KS_createCanoSlave ------
Error __stdcall KS_createCanoSlave(KSHandle hMaster, KSHandle* phSlave, int id, int position, uint vendor, uint product, uint revision, int flags) {
  typedef Error (__stdcall* KS_createCanoSlaveProc)(KSHandle, KSHandle*, int, int, uint, uint, uint, int);
  static KS_createCanoSlaveProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createCanoSlaveProc)GetProcAddress(_hKitharaKrtsDll, "KS_createCanoSlave");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createCanoSlave not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, phSlave, id, position, vendor, product, revision, flags);
}

//------ KS_createCanoSlaveIndirect ------
Error __stdcall KS_createCanoSlaveIndirect(KSHandle hMaster, KSHandle* phSlave, const KSCanoSlaveState* pSlaveState, int flags) {
  typedef Error (__stdcall* KS_createCanoSlaveIndirectProc)(KSHandle, KSHandle*, const KSCanoSlaveState*, int);
  static KS_createCanoSlaveIndirectProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createCanoSlaveIndirectProc)GetProcAddress(_hKitharaKrtsDll, "KS_createCanoSlaveIndirect");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createCanoSlaveIndirect not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, phSlave, pSlaveState, flags);
}

//------ KS_enumCanoSlaves ------
Error __stdcall KS_enumCanoSlaves(KSHandle hMaster, int index, KSCanoSlaveState* pSlaveState, int flags) {
  typedef Error (__stdcall* KS_enumCanoSlavesProc)(KSHandle, int, KSCanoSlaveState*, int);
  static KS_enumCanoSlavesProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumCanoSlavesProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumCanoSlaves");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumCanoSlaves not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, index, pSlaveState, flags);
}

//------ KS_deleteCanoSlave ------
Error __stdcall KS_deleteCanoSlave(KSHandle hSlave) {
  typedef Error (__stdcall* KS_deleteCanoSlaveProc)(KSHandle);
  static KS_deleteCanoSlaveProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_deleteCanoSlaveProc)GetProcAddress(_hKitharaKrtsDll, "KS_deleteCanoSlave");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_deleteCanoSlave not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave);
}

//------ KS_queryCanoSlaveState ------
Error __stdcall KS_queryCanoSlaveState(KSHandle hSlave, KSCanoSlaveState* pSlaveState, int flags) {
  typedef Error (__stdcall* KS_queryCanoSlaveStateProc)(KSHandle, KSCanoSlaveState*, int);
  static KS_queryCanoSlaveStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryCanoSlaveStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryCanoSlaveState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryCanoSlaveState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, pSlaveState, flags);
}

//------ KS_changeCanoSlaveState ------
Error __stdcall KS_changeCanoSlaveState(KSHandle hSlave, int state, int flags) {
  typedef Error (__stdcall* KS_changeCanoSlaveStateProc)(KSHandle, int, int);
  static KS_changeCanoSlaveStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_changeCanoSlaveStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_changeCanoSlaveState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_changeCanoSlaveState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, state, flags);
}

//------ KS_queryCanoSlaveInfo ------
Error __stdcall KS_queryCanoSlaveInfo(KSHandle hSlave, KSCanoSlaveInfo** ppSlaveInfo, int flags) {
  typedef Error (__stdcall* KS_queryCanoSlaveInfoProc)(KSHandle, KSCanoSlaveInfo**, int);
  static KS_queryCanoSlaveInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryCanoSlaveInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryCanoSlaveInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryCanoSlaveInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, ppSlaveInfo, flags);
}

//------ KS_queryCanoDataObjInfo ------
Error __stdcall KS_queryCanoDataObjInfo(KSHandle hSlave, int objIndex, KSCanoDataObjInfo** ppDataObjInfo, int flags) {
  typedef Error (__stdcall* KS_queryCanoDataObjInfoProc)(KSHandle, int, KSCanoDataObjInfo**, int);
  static KS_queryCanoDataObjInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryCanoDataObjInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryCanoDataObjInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryCanoDataObjInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objIndex, ppDataObjInfo, flags);
}

//------ KS_queryCanoDataVarInfo ------
Error __stdcall KS_queryCanoDataVarInfo(KSHandle hSlave, int objIndex, int varIndex, KSCanoDataVarInfo** ppDataVarInfo, int flags) {
  typedef Error (__stdcall* KS_queryCanoDataVarInfoProc)(KSHandle, int, int, KSCanoDataVarInfo**, int);
  static KS_queryCanoDataVarInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryCanoDataVarInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryCanoDataVarInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryCanoDataVarInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objIndex, varIndex, ppDataVarInfo, flags);
}

//------ KS_enumCanoDataObjInfo ------
Error __stdcall KS_enumCanoDataObjInfo(KSHandle hSlave, int objIndex, KSCanoDataObjInfo** ppDataObjInfo, int flags) {
  typedef Error (__stdcall* KS_enumCanoDataObjInfoProc)(KSHandle, int, KSCanoDataObjInfo**, int);
  static KS_enumCanoDataObjInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumCanoDataObjInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumCanoDataObjInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumCanoDataObjInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objIndex, ppDataObjInfo, flags);
}

//------ KS_enumCanoDataVarInfo ------
Error __stdcall KS_enumCanoDataVarInfo(KSHandle hSlave, int objIndex, int varIndex, KSCanoDataVarInfo** ppDataVarInfo, int flags) {
  typedef Error (__stdcall* KS_enumCanoDataVarInfoProc)(KSHandle, int, int, KSCanoDataVarInfo**, int);
  static KS_enumCanoDataVarInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumCanoDataVarInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumCanoDataVarInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumCanoDataVarInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objIndex, varIndex, ppDataVarInfo, flags);
}

//------ KS_createCanoDataSet ------
Error __stdcall KS_createCanoDataSet(KSHandle hMaster, KSHandle* phSet, void** ppAppPtr, void** ppSysPtr, int flags) {
  typedef Error (__stdcall* KS_createCanoDataSetProc)(KSHandle, KSHandle*, void**, void**, int);
  static KS_createCanoDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createCanoDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_createCanoDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createCanoDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, phSet, ppAppPtr, ppSysPtr, flags);
}

//------ KS_deleteCanoDataSet ------
Error __stdcall KS_deleteCanoDataSet(KSHandle hSet) {
  typedef Error (__stdcall* KS_deleteCanoDataSetProc)(KSHandle);
  static KS_deleteCanoDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_deleteCanoDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_deleteCanoDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_deleteCanoDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet);
}

//------ KS_assignCanoDataSet ------
Error __stdcall KS_assignCanoDataSet(KSHandle hSet, KSHandle hSlave, int pdoIndex, int placement, int flags) {
  typedef Error (__stdcall* KS_assignCanoDataSetProc)(KSHandle, KSHandle, int, int, int);
  static KS_assignCanoDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_assignCanoDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_assignCanoDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_assignCanoDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, hSlave, pdoIndex, placement, flags);
}

//------ KS_readCanoDataSet ------
Error __stdcall KS_readCanoDataSet(KSHandle hSet, int flags) {
  typedef Error (__stdcall* KS_readCanoDataSetProc)(KSHandle, int);
  static KS_readCanoDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readCanoDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_readCanoDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readCanoDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, flags);
}

//------ KS_postCanoDataSet ------
Error __stdcall KS_postCanoDataSet(KSHandle hSet, int flags) {
  typedef Error (__stdcall* KS_postCanoDataSetProc)(KSHandle, int);
  static KS_postCanoDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_postCanoDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_postCanoDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_postCanoDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, flags);
}

//------ KS_installCanoDataSetHandler ------
Error __stdcall KS_installCanoDataSetHandler(KSHandle hSet, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installCanoDataSetHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installCanoDataSetHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installCanoDataSetHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installCanoDataSetHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installCanoDataSetHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, eventCode, hSignal, flags);
}

//------ KS_getCanoDataObjAddress ------
Error __stdcall KS_getCanoDataObjAddress(KSHandle hSet, KSHandle hSlave, int pdoIndex, int subIndex, void** ppAppPtr, void** ppSysPtr, int* pBitOffset, int* pBitLength, int flags) {
  typedef Error (__stdcall* KS_getCanoDataObjAddressProc)(KSHandle, KSHandle, int, int, void**, void**, int*, int*, int);
  static KS_getCanoDataObjAddressProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getCanoDataObjAddressProc)GetProcAddress(_hKitharaKrtsDll, "KS_getCanoDataObjAddress");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getCanoDataObjAddress not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, hSlave, pdoIndex, subIndex, ppAppPtr, ppSysPtr, pBitOffset, pBitLength, flags);
}

//------ KS_readCanoDataObj ------
Error __stdcall KS_readCanoDataObj(KSHandle hSlave, int index, int subIndex, void* pData, int* pSize, int flags) {
  typedef Error (__stdcall* KS_readCanoDataObjProc)(KSHandle, int, int, void*, int*, int);
  static KS_readCanoDataObjProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readCanoDataObjProc)GetProcAddress(_hKitharaKrtsDll, "KS_readCanoDataObj");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readCanoDataObj not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, index, subIndex, pData, pSize, flags);
}

//------ KS_postCanoDataObj ------
Error __stdcall KS_postCanoDataObj(KSHandle hSlave, int index, int subIndex, const void* pData, int size, int flags) {
  typedef Error (__stdcall* KS_postCanoDataObjProc)(KSHandle, int, int, const void*, int, int);
  static KS_postCanoDataObjProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_postCanoDataObjProc)GetProcAddress(_hKitharaKrtsDll, "KS_postCanoDataObj");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_postCanoDataObj not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, index, subIndex, pData, size, flags);
}

//------ KS_changeCanoState ------
Error __stdcall KS_changeCanoState(KSHandle hObject, int state, int flags) {
  typedef Error (__stdcall* KS_changeCanoStateProc)(KSHandle, int, int);
  static KS_changeCanoStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_changeCanoStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_changeCanoState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_changeCanoState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, state, flags);
}

//------ KS_execCanoCommand ------
Error __stdcall KS_execCanoCommand(KSHandle hObject, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execCanoCommandProc)(KSHandle, int, void*, int);
  static KS_execCanoCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execCanoCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execCanoCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execCanoCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, command, pParam, flags);
}

//------ _initCANopenModule ------
static void _initCANopenModule() {
  _registerKernelAddress("KS_queryCanoMasterState", (CallBackProc)KS_queryCanoMasterState);
  _registerKernelAddress("KS_changeCanoMasterState", (CallBackProc)KS_changeCanoMasterState);
  _registerKernelAddress("KS_queryCanoSlaveState", (CallBackProc)KS_queryCanoSlaveState);
  _registerKernelAddress("KS_changeCanoSlaveState", (CallBackProc)KS_changeCanoSlaveState);
  _registerKernelAddress("KS_readCanoDataSet", (CallBackProc)KS_readCanoDataSet);
  _registerKernelAddress("KS_postCanoDataSet", (CallBackProc)KS_postCanoDataSet);
  _registerKernelAddress("KS_getCanoDataObjAddress", (CallBackProc)KS_getCanoDataObjAddress);
  _registerKernelAddress("KS_readCanoDataObj", (CallBackProc)KS_readCanoDataObj);
  _registerKernelAddress("KS_postCanoDataObj", (CallBackProc)KS_postCanoDataObj);
  _registerKernelAddress("KS_changeCanoState", (CallBackProc)KS_changeCanoState);
  _registerKernelAddress("KS_execCanoCommand", (CallBackProc)KS_execCanoCommand);
}

#define KS_CANOPEN_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Profibus Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createPbusMaster ------
Error __stdcall KS_createPbusMaster(KSHandle* phMaster, const char* name, const char* libraryPath, const char* topologyFile, int flags) {
  typedef Error (__stdcall* KS_createPbusMasterProc)(KSHandle*, const char*, const char*, const char*, int);
  static KS_createPbusMasterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createPbusMasterProc)GetProcAddress(_hKitharaKrtsDll, "KS_createPbusMaster");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createPbusMaster not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phMaster, name, libraryPath, topologyFile, flags);
}

//------ KS_closePbusMaster ------
Error __stdcall KS_closePbusMaster(KSHandle hMaster) {
  typedef Error (__stdcall* KS_closePbusMasterProc)(KSHandle);
  static KS_closePbusMasterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closePbusMasterProc)GetProcAddress(_hKitharaKrtsDll, "KS_closePbusMaster");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closePbusMaster not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster);
}

//------ KS_queryPbusMasterState ------
Error __stdcall KS_queryPbusMasterState(KSHandle hMaster, KSPbusMasterState* pMasterState, int flags) {
  typedef Error (__stdcall* KS_queryPbusMasterStateProc)(KSHandle, KSPbusMasterState*, int);
  static KS_queryPbusMasterStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryPbusMasterStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryPbusMasterState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryPbusMasterState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, pMasterState, flags);
}

//------ KS_changePbusMasterState ------
Error __stdcall KS_changePbusMasterState(KSHandle hMaster, int state, int flags) {
  typedef Error (__stdcall* KS_changePbusMasterStateProc)(KSHandle, int, int);
  static KS_changePbusMasterStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_changePbusMasterStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_changePbusMasterState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_changePbusMasterState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, state, flags);
}

//------ KS_enumPbusSlaves ------
Error __stdcall KS_enumPbusSlaves(KSHandle hMaster, int index, KSPbusSlaveState* pSlaveState, int flags) {
  typedef Error (__stdcall* KS_enumPbusSlavesProc)(KSHandle, int, KSPbusSlaveState*, int);
  static KS_enumPbusSlavesProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumPbusSlavesProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumPbusSlaves");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumPbusSlaves not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, index, pSlaveState, flags);
}

//------ KS_installPbusMasterHandler ------
Error __stdcall KS_installPbusMasterHandler(KSHandle hMaster, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installPbusMasterHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installPbusMasterHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installPbusMasterHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installPbusMasterHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installPbusMasterHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, eventCode, hSignal, flags);
}

//------ KS_execPbusMasterCommand ------
Error __stdcall KS_execPbusMasterCommand(KSHandle hMaster, int command, void* pParam, int flags) {
  typedef Error (__stdcall* KS_execPbusMasterCommandProc)(KSHandle, int, void*, int);
  static KS_execPbusMasterCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execPbusMasterCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execPbusMasterCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execPbusMasterCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, command, pParam, flags);
}

//------ KS_createPbusSlave ------
Error __stdcall KS_createPbusSlave(KSHandle hMaster, KSHandle* phSlave, int address, int flags) {
  typedef Error (__stdcall* KS_createPbusSlaveProc)(KSHandle, KSHandle*, int, int);
  static KS_createPbusSlaveProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createPbusSlaveProc)GetProcAddress(_hKitharaKrtsDll, "KS_createPbusSlave");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createPbusSlave not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, phSlave, address, flags);
}

//------ KS_deletePbusSlave ------
Error __stdcall KS_deletePbusSlave(KSHandle hSlave) {
  typedef Error (__stdcall* KS_deletePbusSlaveProc)(KSHandle);
  static KS_deletePbusSlaveProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_deletePbusSlaveProc)GetProcAddress(_hKitharaKrtsDll, "KS_deletePbusSlave");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_deletePbusSlave not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave);
}

//------ KS_queryPbusSlaveState ------
Error __stdcall KS_queryPbusSlaveState(KSHandle hSlave, KSPbusSlaveState* pSlaveState, int flags) {
  typedef Error (__stdcall* KS_queryPbusSlaveStateProc)(KSHandle, KSPbusSlaveState*, int);
  static KS_queryPbusSlaveStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryPbusSlaveStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryPbusSlaveState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryPbusSlaveState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, pSlaveState, flags);
}

//------ KS_changePbusSlaveState ------
Error __stdcall KS_changePbusSlaveState(KSHandle hSlave, int state, int flags) {
  typedef Error (__stdcall* KS_changePbusSlaveStateProc)(KSHandle, int, int);
  static KS_changePbusSlaveStateProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_changePbusSlaveStateProc)GetProcAddress(_hKitharaKrtsDll, "KS_changePbusSlaveState");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_changePbusSlaveState not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, state, flags);
}

//------ KS_queryPbusSlaveInfo ------
Error __stdcall KS_queryPbusSlaveInfo(KSHandle hSlave, KSPbusSlaveInfo** ppSlaveInfo, int flags) {
  typedef Error (__stdcall* KS_queryPbusSlaveInfoProc)(KSHandle, KSPbusSlaveInfo**, int);
  static KS_queryPbusSlaveInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryPbusSlaveInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryPbusSlaveInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryPbusSlaveInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, ppSlaveInfo, flags);
}

//------ KS_queryPbusSlotInfo ------
Error __stdcall KS_queryPbusSlotInfo(KSHandle hSlave, int objIndex, KSPbusSlotInfo** ppSlotInfo, int flags) {
  typedef Error (__stdcall* KS_queryPbusSlotInfoProc)(KSHandle, int, KSPbusSlotInfo**, int);
  static KS_queryPbusSlotInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryPbusSlotInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryPbusSlotInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryPbusSlotInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objIndex, ppSlotInfo, flags);
}

//------ KS_queryPbusDataVarInfo ------
Error __stdcall KS_queryPbusDataVarInfo(KSHandle hSlave, int objIndex, int varIndex, KSPbusDataVarInfo** ppDataVarInfo, int flags) {
  typedef Error (__stdcall* KS_queryPbusDataVarInfoProc)(KSHandle, int, int, KSPbusDataVarInfo**, int);
  static KS_queryPbusDataVarInfoProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_queryPbusDataVarInfoProc)GetProcAddress(_hKitharaKrtsDll, "KS_queryPbusDataVarInfo");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_queryPbusDataVarInfo not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, objIndex, varIndex, ppDataVarInfo, flags);
}

//------ KS_createPbusDataSet ------
Error __stdcall KS_createPbusDataSet(KSHandle hMaster, KSHandle* phSet, void** ppAppPtr, void** ppSysPtr, int flags) {
  typedef Error (__stdcall* KS_createPbusDataSetProc)(KSHandle, KSHandle*, void**, void**, int);
  static KS_createPbusDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createPbusDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_createPbusDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createPbusDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hMaster, phSet, ppAppPtr, ppSysPtr, flags);
}

//------ KS_deletePbusDataSet ------
Error __stdcall KS_deletePbusDataSet(KSHandle hSet) {
  typedef Error (__stdcall* KS_deletePbusDataSetProc)(KSHandle);
  static KS_deletePbusDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_deletePbusDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_deletePbusDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_deletePbusDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet);
}

//------ KS_assignPbusDataSet ------
Error __stdcall KS_assignPbusDataSet(KSHandle hSet, KSHandle hSlave, int slot, int index, int placement, int flags) {
  typedef Error (__stdcall* KS_assignPbusDataSetProc)(KSHandle, KSHandle, int, int, int, int);
  static KS_assignPbusDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_assignPbusDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_assignPbusDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_assignPbusDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, hSlave, slot, index, placement, flags);
}

//------ KS_readPbusDataSet ------
Error __stdcall KS_readPbusDataSet(KSHandle hSet, int flags) {
  typedef Error (__stdcall* KS_readPbusDataSetProc)(KSHandle, int);
  static KS_readPbusDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readPbusDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_readPbusDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readPbusDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, flags);
}

//------ KS_postPbusDataSet ------
Error __stdcall KS_postPbusDataSet(KSHandle hSet, int flags) {
  typedef Error (__stdcall* KS_postPbusDataSetProc)(KSHandle, int);
  static KS_postPbusDataSetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_postPbusDataSetProc)GetProcAddress(_hKitharaKrtsDll, "KS_postPbusDataSet");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_postPbusDataSet not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, flags);
}

//------ KS_installPbusDataSetHandler ------
Error __stdcall KS_installPbusDataSetHandler(KSHandle hSet, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installPbusDataSetHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installPbusDataSetHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installPbusDataSetHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installPbusDataSetHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installPbusDataSetHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, eventCode, hSignal, flags);
}

//------ KS_getPbusDataObjAddress ------
Error __stdcall KS_getPbusDataObjAddress(KSHandle hSet, KSHandle hSlave, int slot, int index, void** ppAppPtr, void** ppSysPtr, int* pBitOffset, int* pBitLength, int flags) {
  typedef Error (__stdcall* KS_getPbusDataObjAddressProc)(KSHandle, KSHandle, int, int, void**, void**, int*, int*, int);
  static KS_getPbusDataObjAddressProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getPbusDataObjAddressProc)GetProcAddress(_hKitharaKrtsDll, "KS_getPbusDataObjAddress");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getPbusDataObjAddress not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSet, hSlave, slot, index, ppAppPtr, ppSysPtr, pBitOffset, pBitLength, flags);
}

//------ KS_readPbusDataObj ------
Error __stdcall KS_readPbusDataObj(KSHandle hSlave, int slot, int index, void* pData, int* pSize, int flags) {
  typedef Error (__stdcall* KS_readPbusDataObjProc)(KSHandle, int, int, void*, int*, int);
  static KS_readPbusDataObjProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readPbusDataObjProc)GetProcAddress(_hKitharaKrtsDll, "KS_readPbusDataObj");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readPbusDataObj not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, slot, index, pData, pSize, flags);
}

//------ KS_postPbusDataObj ------
Error __stdcall KS_postPbusDataObj(KSHandle hSlave, int slot, int index, void* pData, int size, int flags) {
  typedef Error (__stdcall* KS_postPbusDataObjProc)(KSHandle, int, int, void*, int, int);
  static KS_postPbusDataObjProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_postPbusDataObjProc)GetProcAddress(_hKitharaKrtsDll, "KS_postPbusDataObj");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_postPbusDataObj not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSlave, slot, index, pData, size, flags);
}

//------ _initProfibusModule ------
static void _initProfibusModule() {
  _registerKernelAddress("KS_queryPbusMasterState", (CallBackProc)KS_queryPbusMasterState);
  _registerKernelAddress("KS_changePbusMasterState", (CallBackProc)KS_changePbusMasterState);
  _registerKernelAddress("KS_execPbusMasterCommand", (CallBackProc)KS_execPbusMasterCommand);
  _registerKernelAddress("KS_queryPbusSlaveState", (CallBackProc)KS_queryPbusSlaveState);
  _registerKernelAddress("KS_changePbusSlaveState", (CallBackProc)KS_changePbusSlaveState);
  _registerKernelAddress("KS_readPbusDataSet", (CallBackProc)KS_readPbusDataSet);
  _registerKernelAddress("KS_postPbusDataSet", (CallBackProc)KS_postPbusDataSet);
  _registerKernelAddress("KS_getPbusDataObjAddress", (CallBackProc)KS_getPbusDataObjAddress);
  _registerKernelAddress("KS_readPbusDataObj", (CallBackProc)KS_readPbusDataObj);
  _registerKernelAddress("KS_postPbusDataObj", (CallBackProc)KS_postPbusDataObj);
}

#define KS_PROFIBUS_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// RTL Module
//--------------------------------------------------------------------------------------------------------------

//------ KSRTL_calloc ------
void* __stdcall KSRTL_calloc(uint num, uint size) {
  typedef void* (__stdcall* KSRTL_callocProc)(uint, uint);
  static KSRTL_callocProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_callocProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_calloc");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_calloc not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(num, size);
}

//------ KSRTL_free ------
void __stdcall KSRTL_free(void* ptr) {
  typedef void (__stdcall* KSRTL_freeProc)(void*);
  static KSRTL_freeProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return;      }
    }

    _pProc = (KSRTL_freeProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_free");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_free not found in DLL");
      return;    }
  }

  return (*_pProc)(ptr);
}

//------ KSRTL_malloc ------
void* __stdcall KSRTL_malloc(uint size) {
  typedef void* (__stdcall* KSRTL_mallocProc)(uint);
  static KSRTL_mallocProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_mallocProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_malloc");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_malloc not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(size);
}

//------ KSRTL_sin ------
double __stdcall KSRTL_sin(double x) {
  typedef double (__stdcall* KSRTL_sinProc)(double);
  static KSRTL_sinProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_sinProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_sin");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_sin not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_cos ------
double __stdcall KSRTL_cos(double x) {
  typedef double (__stdcall* KSRTL_cosProc)(double);
  static KSRTL_cosProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_cosProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_cos");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_cos not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_tan ------
double __stdcall KSRTL_tan(double x) {
  typedef double (__stdcall* KSRTL_tanProc)(double);
  static KSRTL_tanProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_tanProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_tan");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_tan not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_asin ------
double __stdcall KSRTL_asin(double x) {
  typedef double (__stdcall* KSRTL_asinProc)(double);
  static KSRTL_asinProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_asinProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_asin");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_asin not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_acos ------
double __stdcall KSRTL_acos(double x) {
  typedef double (__stdcall* KSRTL_acosProc)(double);
  static KSRTL_acosProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_acosProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_acos");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_acos not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_atan ------
double __stdcall KSRTL_atan(double x) {
  typedef double (__stdcall* KSRTL_atanProc)(double);
  static KSRTL_atanProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_atanProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_atan");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_atan not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_atan2 ------
double __stdcall KSRTL_atan2(double y, double x) {
  typedef double (__stdcall* KSRTL_atan2Proc)(double, double);
  static KSRTL_atan2Proc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_atan2Proc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_atan2");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_atan2 not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(y, x);
}

//------ KSRTL_sinh ------
double __stdcall KSRTL_sinh(double x) {
  typedef double (__stdcall* KSRTL_sinhProc)(double);
  static KSRTL_sinhProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_sinhProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_sinh");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_sinh not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_cosh ------
double __stdcall KSRTL_cosh(double x) {
  typedef double (__stdcall* KSRTL_coshProc)(double);
  static KSRTL_coshProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_coshProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_cosh");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_cosh not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_tanh ------
double __stdcall KSRTL_tanh(double x) {
  typedef double (__stdcall* KSRTL_tanhProc)(double);
  static KSRTL_tanhProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_tanhProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_tanh");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_tanh not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_exp ------
double __stdcall KSRTL_exp(double x) {
  typedef double (__stdcall* KSRTL_expProc)(double);
  static KSRTL_expProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_expProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_exp");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_exp not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_frexp ------
double __stdcall KSRTL_frexp(double x, int* exp) {
  typedef double (__stdcall* KSRTL_frexpProc)(double, int*);
  static KSRTL_frexpProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_frexpProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_frexp");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_frexp not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x, exp);
}

//------ KSRTL_ldexp ------
double __stdcall KSRTL_ldexp(double x, int exp) {
  typedef double (__stdcall* KSRTL_ldexpProc)(double, int);
  static KSRTL_ldexpProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_ldexpProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_ldexp");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_ldexp not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x, exp);
}

//------ KSRTL_log ------
double __stdcall KSRTL_log(double x) {
  typedef double (__stdcall* KSRTL_logProc)(double);
  static KSRTL_logProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_logProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_log");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_log not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_log10 ------
double __stdcall KSRTL_log10(double x) {
  typedef double (__stdcall* KSRTL_log10Proc)(double);
  static KSRTL_log10Proc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_log10Proc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_log10");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_log10 not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_modf ------
double __stdcall KSRTL_modf(double x, double* intpart) {
  typedef double (__stdcall* KSRTL_modfProc)(double, double*);
  static KSRTL_modfProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_modfProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_modf");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_modf not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x, intpart);
}

//------ KSRTL_pow ------
double __stdcall KSRTL_pow(double base, double exponent) {
  typedef double (__stdcall* KSRTL_powProc)(double, double);
  static KSRTL_powProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_powProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_pow");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_pow not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(base, exponent);
}

//------ KSRTL_sqrt ------
double __stdcall KSRTL_sqrt(double x) {
  typedef double (__stdcall* KSRTL_sqrtProc)(double);
  static KSRTL_sqrtProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_sqrtProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_sqrt");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_sqrt not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_fabs ------
double __stdcall KSRTL_fabs(double x) {
  typedef double (__stdcall* KSRTL_fabsProc)(double);
  static KSRTL_fabsProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_fabsProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_fabs");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_fabs not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_ceil ------
double __stdcall KSRTL_ceil(double x) {
  typedef double (__stdcall* KSRTL_ceilProc)(double);
  static KSRTL_ceilProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_ceilProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_ceil");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_ceil not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_floor ------
double __stdcall KSRTL_floor(double x) {
  typedef double (__stdcall* KSRTL_floorProc)(double);
  static KSRTL_floorProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_floorProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_floor");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_floor not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(x);
}

//------ KSRTL_fmod ------
double __stdcall KSRTL_fmod(double numerator, double denominator) {
  typedef double (__stdcall* KSRTL_fmodProc)(double, double);
  static KSRTL_fmodProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0.0;
      }
    }

    _pProc = (KSRTL_fmodProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_fmod");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_fmod not found in DLL");
      return 0.0;
    }
  }

  return (*_pProc)(numerator, denominator);
}

//------ KSRTL_memchr ------
const void* __stdcall KSRTL_memchr(const void* ptr, int value, uint num) {
  typedef const void* (__stdcall* KSRTL_memchrProc)(const void*, int, uint);
  static KSRTL_memchrProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_memchrProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_memchr");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_memchr not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(ptr, value, num);
}

//------ KSRTL_memcmp ------
int __stdcall KSRTL_memcmp(const void* ptr1, const void* ptr2, uint num) {
  typedef int (__stdcall* KSRTL_memcmpProc)(const void*, const void*, uint);
  static KSRTL_memcmpProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_memcmpProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_memcmp");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_memcmp not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(ptr1, ptr2, num);
}

//------ KSRTL_memcpy ------
void* __stdcall KSRTL_memcpy(void* destination, const void* source, uint num) {
  typedef void* (__stdcall* KSRTL_memcpyProc)(void*, const void*, uint);
  static KSRTL_memcpyProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_memcpyProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_memcpy");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_memcpy not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(destination, source, num);
}

//------ KSRTL_memmove ------
void* __stdcall KSRTL_memmove(void* destination, const void* source, uint num) {
  typedef void* (__stdcall* KSRTL_memmoveProc)(void*, const void*, uint);
  static KSRTL_memmoveProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_memmoveProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_memmove");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_memmove not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(destination, source, num);
}

//------ KSRTL_memset ------
void* __stdcall KSRTL_memset(void* ptr, int value, uint num) {
  typedef void* (__stdcall* KSRTL_memsetProc)(void*, int, uint);
  static KSRTL_memsetProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_memsetProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_memset");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_memset not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(ptr, value, num);
}

//------ KSRTL_strlen ------
uint __stdcall KSRTL_strlen(const char* str) {
  typedef uint (__stdcall* KSRTL_strlenProc)(const char*);
  static KSRTL_strlenProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_strlenProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strlen");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strlen not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(str);
}

//------ KSRTL_strcmp ------
int __stdcall KSRTL_strcmp(const char* str1, const char* str2) {
  typedef int (__stdcall* KSRTL_strcmpProc)(const char*, const char*);
  static KSRTL_strcmpProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_strcmpProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strcmp");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strcmp not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(str1, str2);
}

//------ KSRTL_strncmp ------
int __stdcall KSRTL_strncmp(const char* str1, const char* str2, uint num) {
  typedef int (__stdcall* KSRTL_strncmpProc)(const char*, const char*, uint);
  static KSRTL_strncmpProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_strncmpProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strncmp");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strncmp not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(str1, str2, num);
}

//------ KSRTL_strcpy ------
char* __stdcall KSRTL_strcpy(char* destination, const char* source) {
  typedef char* (__stdcall* KSRTL_strcpyProc)(char*, const char*);
  static KSRTL_strcpyProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_strcpyProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strcpy");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strcpy not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(destination, source);
}

//------ KSRTL_strncpy ------
char* __stdcall KSRTL_strncpy(char* destination, const char* source, uint num) {
  typedef char* (__stdcall* KSRTL_strncpyProc)(char*, const char*, uint);
  static KSRTL_strncpyProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_strncpyProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strncpy");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strncpy not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(destination, source, num);
}

//------ KSRTL_strcat ------
char* __stdcall KSRTL_strcat(char* destination, const char* source) {
  typedef char* (__stdcall* KSRTL_strcatProc)(char*, const char*);
  static KSRTL_strcatProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_strcatProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strcat");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strcat not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(destination, source);
}

//------ KSRTL_strncat ------
char* __stdcall KSRTL_strncat(char* destination, const char* source, uint num) {
  typedef char* (__stdcall* KSRTL_strncatProc)(char*, const char*, uint);
  static KSRTL_strncatProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_strncatProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strncat");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strncat not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(destination, source, num);
}

//------ KSRTL_strchr ------
const char* __stdcall KSRTL_strchr(const char* str, int character) {
  typedef const char* (__stdcall* KSRTL_strchrProc)(const char*, int);
  static KSRTL_strchrProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_strchrProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strchr");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strchr not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(str, character);
}

//------ KSRTL_strcoll ------
int __stdcall KSRTL_strcoll(const char* str1, const char* str2) {
  typedef int (__stdcall* KSRTL_strcollProc)(const char*, const char*);
  static KSRTL_strcollProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_strcollProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strcoll");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strcoll not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(str1, str2);
}

//------ KSRTL_strrchr ------
const char* __stdcall KSRTL_strrchr(const char* str1, int character) {
  typedef const char* (__stdcall* KSRTL_strrchrProc)(const char*, int);
  static KSRTL_strrchrProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_strrchrProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strrchr");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strrchr not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(str1, character);
}

//------ KSRTL_strstr ------
const char* __stdcall KSRTL_strstr(const char* str1, const char* str2) {
  typedef const char* (__stdcall* KSRTL_strstrProc)(const char*, const char*);
  static KSRTL_strstrProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_strstrProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strstr");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strstr not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(str1, str2);
}

//------ KSRTL_strspn ------
uint __stdcall KSRTL_strspn(const char* str1, const char* str2) {
  typedef uint (__stdcall* KSRTL_strspnProc)(const char*, const char*);
  static KSRTL_strspnProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_strspnProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strspn");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strspn not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(str1, str2);
}

//------ KSRTL_strcspn ------
uint __stdcall KSRTL_strcspn(const char* str1, const char* str2) {
  typedef uint (__stdcall* KSRTL_strcspnProc)(const char*, const char*);
  static KSRTL_strcspnProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_strcspnProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strcspn");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strcspn not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(str1, str2);
}

//------ KSRTL_strpbrk ------
const char* __stdcall KSRTL_strpbrk(const char* str1, const char* str2) {
  typedef const char* (__stdcall* KSRTL_strpbrkProc)(const char*, const char*);
  static KSRTL_strpbrkProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_strpbrkProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strpbrk");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strpbrk not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(str1, str2);
}

//------ KSRTL_strtok ------
char* __stdcall KSRTL_strtok(char* str, const char* delimiters) {
  typedef char* (__stdcall* KSRTL_strtokProc)(char*, const char*);
  static KSRTL_strtokProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return NULL;
      }
    }

    _pProc = (KSRTL_strtokProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strtok");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strtok not found in DLL");
      return NULL;
    }
  }

  return (*_pProc)(str, delimiters);
}

//------ KSRTL_strxfrm ------
uint __stdcall KSRTL_strxfrm(char* destination, const char* source, uint num) {
  typedef uint (__stdcall* KSRTL_strxfrmProc)(char*, const char*, uint);
  static KSRTL_strxfrmProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_strxfrmProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_strxfrm");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_strxfrm not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(destination, source, num);
}

//------ KSRTL_vsprintf ------
int __stdcall KSRTL_vsprintf(char* buffer, const char* format, void* pArgs) {
  typedef int (__stdcall* KSRTL_vsprintfProc)(char*, const char*, void*);
  static KSRTL_vsprintfProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_vsprintfProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_vsprintf");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_vsprintf not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(buffer, format, pArgs);
}

//------ KSRTL_vsnprintf ------
int __stdcall KSRTL_vsnprintf(char* buffer, uint n, const char* format, void* pArgs) {
  typedef int (__stdcall* KSRTL_vsnprintfProc)(char*, uint, const char*, void*);
  static KSRTL_vsnprintfProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return 0;
      }
    }

    _pProc = (KSRTL_vsnprintfProc)GetProcAddress(_hKitharaKrtsDll, "KSRTL_vsnprintf");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KSRTL_vsnprintf not found in DLL");
      return 0;
    }
  }

  return (*_pProc)(buffer, n, format, pArgs);
}

//------ _initRTLModule ------
static void _initRTLModule() {
  _registerKernelAddress("KSRTL_calloc", (CallBackProc)KSRTL_calloc);
  _registerKernelAddress("KSRTL_free", (CallBackProc)KSRTL_free);
  _registerKernelAddress("KSRTL_malloc", (CallBackProc)KSRTL_malloc);
  _registerKernelAddress("KSRTL_sin", (CallBackProc)KSRTL_sin);
  _registerKernelAddress("KSRTL_cos", (CallBackProc)KSRTL_cos);
  _registerKernelAddress("KSRTL_tan", (CallBackProc)KSRTL_tan);
  _registerKernelAddress("KSRTL_asin", (CallBackProc)KSRTL_asin);
  _registerKernelAddress("KSRTL_acos", (CallBackProc)KSRTL_acos);
  _registerKernelAddress("KSRTL_atan", (CallBackProc)KSRTL_atan);
  _registerKernelAddress("KSRTL_atan2", (CallBackProc)KSRTL_atan2);
  _registerKernelAddress("KSRTL_sinh", (CallBackProc)KSRTL_sinh);
  _registerKernelAddress("KSRTL_cosh", (CallBackProc)KSRTL_cosh);
  _registerKernelAddress("KSRTL_tanh", (CallBackProc)KSRTL_tanh);
  _registerKernelAddress("KSRTL_exp", (CallBackProc)KSRTL_exp);
  _registerKernelAddress("KSRTL_frexp", (CallBackProc)KSRTL_frexp);
  _registerKernelAddress("KSRTL_ldexp", (CallBackProc)KSRTL_ldexp);
  _registerKernelAddress("KSRTL_log", (CallBackProc)KSRTL_log);
  _registerKernelAddress("KSRTL_log10", (CallBackProc)KSRTL_log10);
  _registerKernelAddress("KSRTL_modf", (CallBackProc)KSRTL_modf);
  _registerKernelAddress("KSRTL_pow", (CallBackProc)KSRTL_pow);
  _registerKernelAddress("KSRTL_sqrt", (CallBackProc)KSRTL_sqrt);
  _registerKernelAddress("KSRTL_fabs", (CallBackProc)KSRTL_fabs);
  _registerKernelAddress("KSRTL_ceil", (CallBackProc)KSRTL_ceil);
  _registerKernelAddress("KSRTL_floor", (CallBackProc)KSRTL_floor);
  _registerKernelAddress("KSRTL_fmod", (CallBackProc)KSRTL_fmod);
  _registerKernelAddress("KSRTL_memchr", (CallBackProc)KSRTL_memchr);
  _registerKernelAddress("KSRTL_memcmp", (CallBackProc)KSRTL_memcmp);
  _registerKernelAddress("KSRTL_memcpy", (CallBackProc)KSRTL_memcpy);
  _registerKernelAddress("KSRTL_memmove", (CallBackProc)KSRTL_memmove);
  _registerKernelAddress("KSRTL_memset", (CallBackProc)KSRTL_memset);
  _registerKernelAddress("KSRTL_strlen", (CallBackProc)KSRTL_strlen);
  _registerKernelAddress("KSRTL_strcmp", (CallBackProc)KSRTL_strcmp);
  _registerKernelAddress("KSRTL_strncmp", (CallBackProc)KSRTL_strncmp);
  _registerKernelAddress("KSRTL_strcpy", (CallBackProc)KSRTL_strcpy);
  _registerKernelAddress("KSRTL_strncpy", (CallBackProc)KSRTL_strncpy);
  _registerKernelAddress("KSRTL_strcat", (CallBackProc)KSRTL_strcat);
  _registerKernelAddress("KSRTL_strncat", (CallBackProc)KSRTL_strncat);
  _registerKernelAddress("KSRTL_strchr", (CallBackProc)KSRTL_strchr);
  _registerKernelAddress("KSRTL_strcoll", (CallBackProc)KSRTL_strcoll);
  _registerKernelAddress("KSRTL_strrchr", (CallBackProc)KSRTL_strrchr);
  _registerKernelAddress("KSRTL_strstr", (CallBackProc)KSRTL_strstr);
  _registerKernelAddress("KSRTL_strspn", (CallBackProc)KSRTL_strspn);
  _registerKernelAddress("KSRTL_strcspn", (CallBackProc)KSRTL_strcspn);
  _registerKernelAddress("KSRTL_strpbrk", (CallBackProc)KSRTL_strpbrk);
  _registerKernelAddress("KSRTL_strtok", (CallBackProc)KSRTL_strtok);
  _registerKernelAddress("KSRTL_strxfrm", (CallBackProc)KSRTL_strxfrm);
  _registerKernelAddress("KSRTL_vsprintf", (CallBackProc)KSRTL_vsprintf);
  _registerKernelAddress("KSRTL_vsnprintf", (CallBackProc)KSRTL_vsnprintf);
}

#define KS_RTL_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Camera Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_enumCameras ------
Error __stdcall KS_enumCameras(KSHandle hObject, int index, KSCameraInfo* pCameraInfo, int flags) {
  typedef Error (__stdcall* KS_enumCamerasProc)(KSHandle, int, KSCameraInfo*, int);
  static KS_enumCamerasProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumCamerasProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumCameras");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumCameras not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, index, pCameraInfo, flags);
}

//------ KS_execCameraCommand ------
Error __stdcall KS_execCameraCommand(KSHandle hObject, int command, uint address, void* pData, int* pLength, int flags) {
  typedef Error (__stdcall* KS_execCameraCommandProc)(KSHandle, int, uint, void*, int*, int);
  static KS_execCameraCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execCameraCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execCameraCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execCameraCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hObject, command, address, pData, pLength, flags);
}

//------ KS_installCameraHandler ------
Error __stdcall KS_installCameraHandler(KSHandle hHandle, int eventType, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installCameraHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installCameraHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installCameraHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installCameraHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installCameraHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hHandle, eventType, hSignal, flags);
}

//------ KS_openCamera ------
Error __stdcall KS_openCamera(KSHandle* phCamera, KSHandle hObject, const char* pHardwareId, int flags) {
  typedef Error (__stdcall* KS_openCameraProc)(KSHandle*, KSHandle, const char*, int);
  static KS_openCameraProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openCameraProc)GetProcAddress(_hKitharaKrtsDll, "KS_openCamera");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openCamera not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phCamera, hObject, pHardwareId, flags);
}

//------ KS_openCameraEx ------
Error __stdcall KS_openCameraEx(KSHandle* phCamera, KSHandle hObject, const KSCameraInfo* pCameraInfo, int flags) {
  typedef Error (__stdcall* KS_openCameraExProc)(KSHandle*, KSHandle, const KSCameraInfo*, int);
  static KS_openCameraExProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_openCameraExProc)GetProcAddress(_hKitharaKrtsDll, "KS_openCameraEx");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_openCameraEx not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phCamera, hObject, pCameraInfo, flags);
}

//------ KS_closeCamera ------
Error __stdcall KS_closeCamera(KSHandle hCamera, int flags) {
  typedef Error (__stdcall* KS_closeCameraProc)(KSHandle, int);
  static KS_closeCameraProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeCameraProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeCamera");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeCamera not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCamera, flags);
}

//------ KS_readCameraMem ------
Error __stdcall KS_readCameraMem(KSHandle hCamera, int64ref address, void* pData, int length, int flags) {
  typedef Error (__stdcall* KS_readCameraMemProc)(KSHandle, int64ref, void*, int, int);
  static KS_readCameraMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_readCameraMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_readCameraMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_readCameraMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCamera, address, pData, length, flags);
}

//------ KS_writeCameraMem ------
Error __stdcall KS_writeCameraMem(KSHandle hCamera, int64ref address, const void* pData, int length, int flags) {
  typedef Error (__stdcall* KS_writeCameraMemProc)(KSHandle, int64ref, const void*, int, int);
  static KS_writeCameraMemProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_writeCameraMemProc)GetProcAddress(_hKitharaKrtsDll, "KS_writeCameraMem");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_writeCameraMem not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCamera, address, pData, length, flags);
}

//------ KS_configCamera ------
Error __stdcall KS_configCamera(KSHandle hCamera, int configCommand, const char* pName, int index, void* pData, int length, int flags) {
  typedef Error (__stdcall* KS_configCameraProc)(KSHandle, int, const char*, int, void*, int, int);
  static KS_configCameraProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_configCameraProc)GetProcAddress(_hKitharaKrtsDll, "KS_configCamera");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_configCamera not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCamera, configCommand, pName, index, pData, length, flags);
}

//------ KS_startCameraAcquisition ------
Error __stdcall KS_startCameraAcquisition(KSHandle hCamera, int acquisitionMode, int flags) {
  typedef Error (__stdcall* KS_startCameraAcquisitionProc)(KSHandle, int, int);
  static KS_startCameraAcquisitionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_startCameraAcquisitionProc)GetProcAddress(_hKitharaKrtsDll, "KS_startCameraAcquisition");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_startCameraAcquisition not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCamera, acquisitionMode, flags);
}

//------ KS_stopCameraAcquisition ------
Error __stdcall KS_stopCameraAcquisition(KSHandle hCamera, int flags) {
  typedef Error (__stdcall* KS_stopCameraAcquisitionProc)(KSHandle, int);
  static KS_stopCameraAcquisitionProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_stopCameraAcquisitionProc)GetProcAddress(_hKitharaKrtsDll, "KS_stopCameraAcquisition");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_stopCameraAcquisition not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCamera, flags);
}

//------ KS_createCameraStream ------
Error __stdcall KS_createCameraStream(KSHandle* phStream, KSHandle hCamera, int channelNumber, int numBuffers, int bufferSize, int flags) {
  typedef Error (__stdcall* KS_createCameraStreamProc)(KSHandle*, KSHandle, int, int, int, int);
  static KS_createCameraStreamProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createCameraStreamProc)GetProcAddress(_hKitharaKrtsDll, "KS_createCameraStream");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createCameraStream not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phStream, hCamera, channelNumber, numBuffers, bufferSize, flags);
}

//------ KS_closeCameraStream ------
Error __stdcall KS_closeCameraStream(KSHandle hStream, int flags) {
  typedef Error (__stdcall* KS_closeCameraStreamProc)(KSHandle, int);
  static KS_closeCameraStreamProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closeCameraStreamProc)GetProcAddress(_hKitharaKrtsDll, "KS_closeCameraStream");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closeCameraStream not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hStream, flags);
}

//------ KS_recvCameraImage ------
Error __stdcall KS_recvCameraImage(KSHandle hStream, void** ppData, KSCameraBlock** ppBlockInfo, int flags) {
  typedef Error (__stdcall* KS_recvCameraImageProc)(KSHandle, void**, KSCameraBlock**, int);
  static KS_recvCameraImageProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_recvCameraImageProc)GetProcAddress(_hKitharaKrtsDll, "KS_recvCameraImage");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_recvCameraImage not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hStream, ppData, ppBlockInfo, flags);
}

//------ KS_releaseCameraImage ------
Error __stdcall KS_releaseCameraImage(KSHandle hStream, void* pData, int flags) {
  typedef Error (__stdcall* KS_releaseCameraImageProc)(KSHandle, void*, int);
  static KS_releaseCameraImageProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_releaseCameraImageProc)GetProcAddress(_hKitharaKrtsDll, "KS_releaseCameraImage");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_releaseCameraImage not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hStream, pData, flags);
}

//------ _initCameraModule ------
static void _initCameraModule() {
  _registerKernelAddress("KS_execCameraCommand", (CallBackProc)KS_execCameraCommand);
  _registerKernelAddress("KS_readCameraMem", (CallBackProc)KS_readCameraMem);
  _registerKernelAddress("KS_writeCameraMem", (CallBackProc)KS_writeCameraMem);
  _registerKernelAddress("KS_configCamera", (CallBackProc)KS_configCamera);
  _registerKernelAddress("KS_recvCameraImage", (CallBackProc)KS_recvCameraImage);
  _registerKernelAddress("KS_releaseCameraImage", (CallBackProc)KS_releaseCameraImage);
}

#define KS_CAMERA_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// PLC Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createPlcCompiler ------
Error __stdcall KS_createPlcCompiler(KSHandle* phCompiler, int flags) {
  typedef Error (__stdcall* KS_createPlcCompilerProc)(KSHandle*, int);
  static KS_createPlcCompilerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createPlcCompilerProc)GetProcAddress(_hKitharaKrtsDll, "KS_createPlcCompiler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createPlcCompiler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phCompiler, flags);
}

//------ KS_closePlcCompiler ------
Error __stdcall KS_closePlcCompiler(KSHandle hCompiler, int flags) {
  typedef Error (__stdcall* KS_closePlcCompilerProc)(KSHandle, int);
  static KS_closePlcCompilerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closePlcCompilerProc)GetProcAddress(_hKitharaKrtsDll, "KS_closePlcCompiler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closePlcCompiler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCompiler, flags);
}

//------ KS_compilePlcFile ------
Error __stdcall KS_compilePlcFile(KSHandle hCompiler, const char* sourcePath, int sourceLanguage, const char* destinationPath, int flags) {
  typedef Error (__stdcall* KS_compilePlcFileProc)(KSHandle, const char*, int, const char*, int);
  static KS_compilePlcFileProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_compilePlcFileProc)GetProcAddress(_hKitharaKrtsDll, "KS_compilePlcFile");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_compilePlcFile not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCompiler, sourcePath, sourceLanguage, destinationPath, flags);
}

//------ KS_compilePlcBuffer ------
Error __stdcall KS_compilePlcBuffer(KSHandle hCompiler, const void* pSourceBuffer, int sourceLength, int sourceLang, void* pDestBuffer, int* pDestLength, int flags) {
  typedef Error (__stdcall* KS_compilePlcBufferProc)(KSHandle, const void*, int, int, void*, int*, int);
  static KS_compilePlcBufferProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_compilePlcBufferProc)GetProcAddress(_hKitharaKrtsDll, "KS_compilePlcBuffer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_compilePlcBuffer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hCompiler, pSourceBuffer, sourceLength, sourceLang, pDestBuffer, pDestLength, flags);
}

//------ KS_createPlcFromFile ------
Error __stdcall KS_createPlcFromFile(KSHandle* phPlc, const char* binaryPath, int flags) {
  typedef Error (__stdcall* KS_createPlcFromFileProc)(KSHandle*, const char*, int);
  static KS_createPlcFromFileProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createPlcFromFileProc)GetProcAddress(_hKitharaKrtsDll, "KS_createPlcFromFile");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createPlcFromFile not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phPlc, binaryPath, flags);
}

//------ KS_createPlcFromBuffer ------
Error __stdcall KS_createPlcFromBuffer(KSHandle* phPlc, const void* pBinaryBuffer, int bufferLength, int flags) {
  typedef Error (__stdcall* KS_createPlcFromBufferProc)(KSHandle*, const void*, int, int);
  static KS_createPlcFromBufferProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createPlcFromBufferProc)GetProcAddress(_hKitharaKrtsDll, "KS_createPlcFromBuffer");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createPlcFromBuffer not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phPlc, pBinaryBuffer, bufferLength, flags);
}

//------ KS_closePlc ------
Error __stdcall KS_closePlc(KSHandle hPlc, int flags) {
  typedef Error (__stdcall* KS_closePlcProc)(KSHandle, int);
  static KS_closePlcProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_closePlcProc)GetProcAddress(_hKitharaKrtsDll, "KS_closePlc");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_closePlc not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPlc, flags);
}

//------ KS_startPlc ------
Error __stdcall KS_startPlc(KSHandle hPlc, int configIndex, int64ref start, int flags) {
  typedef Error (__stdcall* KS_startPlcProc)(KSHandle, int, int64ref, int);
  static KS_startPlcProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_startPlcProc)GetProcAddress(_hKitharaKrtsDll, "KS_startPlc");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_startPlc not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPlc, configIndex, start, flags);
}

//------ KS_stopPlc ------
Error __stdcall KS_stopPlc(KSHandle hPlc, int flags) {
  typedef Error (__stdcall* KS_stopPlcProc)(KSHandle, int);
  static KS_stopPlcProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_stopPlcProc)GetProcAddress(_hKitharaKrtsDll, "KS_stopPlc");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_stopPlc not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPlc, flags);
}

//------ KS_enumPlcConfigurations ------
Error __stdcall KS_enumPlcConfigurations(KSHandle hPlc, int configIndex, char* pNameBuf, int flags) {
  typedef Error (__stdcall* KS_enumPlcConfigurationsProc)(KSHandle, int, char*, int);
  static KS_enumPlcConfigurationsProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumPlcConfigurationsProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumPlcConfigurations");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumPlcConfigurations not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPlc, configIndex, pNameBuf, flags);
}

//------ KS_enumPlcResources ------
Error __stdcall KS_enumPlcResources(KSHandle hPlc, int configIndex, int resourceIndex, char* pNameBuf, int flags) {
  typedef Error (__stdcall* KS_enumPlcResourcesProc)(KSHandle, int, int, char*, int);
  static KS_enumPlcResourcesProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumPlcResourcesProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumPlcResources");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumPlcResources not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPlc, configIndex, resourceIndex, pNameBuf, flags);
}

//------ KS_enumPlcTasks ------
Error __stdcall KS_enumPlcTasks(KSHandle hPlc, int configIndex, int resourceIndex, int taskIndex, char* pNameBuf, int flags) {
  typedef Error (__stdcall* KS_enumPlcTasksProc)(KSHandle, int, int, int, char*, int);
  static KS_enumPlcTasksProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_enumPlcTasksProc)GetProcAddress(_hKitharaKrtsDll, "KS_enumPlcTasks");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_enumPlcTasks not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPlc, configIndex, resourceIndex, taskIndex, pNameBuf, flags);
}

//------ KS_execPlcCommand ------
Error __stdcall KS_execPlcCommand(KSHandle hPlc, int command, void* pData, int flags) {
  typedef Error (__stdcall* KS_execPlcCommandProc)(KSHandle, int, void*, int);
  static KS_execPlcCommandProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_execPlcCommandProc)GetProcAddress(_hKitharaKrtsDll, "KS_execPlcCommand");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_execPlcCommand not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPlc, command, pData, flags);
}

//------ KS_installPlcHandler ------
Error __stdcall KS_installPlcHandler(KSHandle hPlc, int eventCode, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installPlcHandlerProc)(KSHandle, int, KSHandle, int);
  static KS_installPlcHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installPlcHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installPlcHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installPlcHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hPlc, eventCode, hSignal, flags);
}

//------ _initPLCModule ------
static void _initPLCModule() {
  _registerKernelAddress("KS_closePlc", (CallBackProc)KS_closePlc);
  _registerKernelAddress("KS_startPlc", (CallBackProc)KS_startPlc);
  _registerKernelAddress("KS_stopPlc", (CallBackProc)KS_stopPlc);
  _registerKernelAddress("KS_enumPlcConfigurations", (CallBackProc)KS_enumPlcConfigurations);
  _registerKernelAddress("KS_enumPlcResources", (CallBackProc)KS_enumPlcResources);
  _registerKernelAddress("KS_enumPlcTasks", (CallBackProc)KS_enumPlcTasks);
  _registerKernelAddress("KS_execPlcCommand", (CallBackProc)KS_execPlcCommand);
  _registerKernelAddress("KS_installPlcHandler", (CallBackProc)KS_installPlcHandler);
}

#define KS_PLC_MODULE_HAS_RTX

//--------------------------------------------------------------------------------------------------------------
// Vision Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_loadVisionKernel ------
Error __stdcall KS_loadVisionKernel(KSHandle* phKernel, const char* dllName, const char* initProcName, void* pArgs, int flags) {
  typedef Error (__stdcall* KS_loadVisionKernelProc)(KSHandle*, const char*, const char*, void*, int);
  static KS_loadVisionKernelProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_loadVisionKernelProc)GetProcAddress(_hKitharaKrtsDll, "KS_loadVisionKernel");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_loadVisionKernel not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phKernel, dllName, initProcName, pArgs, flags);
}

//------ KS_installVisionHandler ------
Error __stdcall KS_installVisionHandler(int eventType, KSHandle hSignal, int flags) {
  typedef Error (__stdcall* KS_installVisionHandlerProc)(int, KSHandle, int);
  static KS_installVisionHandlerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_installVisionHandlerProc)GetProcAddress(_hKitharaKrtsDll, "KS_installVisionHandler");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_installVisionHandler not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(eventType, hSignal, flags);
}

//--------------------------------------------------------------------------------------------------------------
// SigProc Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createSignalFilter ------
Error __stdcall KS_createSignalFilter(KSHandle* phFilter, const KSSignalFilterParams* pParams, int flags) {
  typedef Error (__stdcall* KS_createSignalFilterProc)(KSHandle*, const KSSignalFilterParams*, int);
  static KS_createSignalFilterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createSignalFilterProc)GetProcAddress(_hKitharaKrtsDll, "KS_createSignalFilter");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createSignalFilter not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phFilter, pParams, flags);
}

//------ KS_createIndividualSignalFilter ------
Error __stdcall KS_createIndividualSignalFilter(KSHandle* phFilter, KSIndividualSignalFilterParams* pParams, int flags) {
  typedef Error (__stdcall* KS_createIndividualSignalFilterProc)(KSHandle*, KSIndividualSignalFilterParams*, int);
  static KS_createIndividualSignalFilterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createIndividualSignalFilterProc)GetProcAddress(_hKitharaKrtsDll, "KS_createIndividualSignalFilter");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createIndividualSignalFilter not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phFilter, pParams, flags);
}

//------ KS_getFrequencyResponse ------
Error __stdcall KS_getFrequencyResponse(KSHandle hFilter, double frequency, double* pAmplitude, double* pPhase, int flags) {
  typedef Error (__stdcall* KS_getFrequencyResponseProc)(KSHandle, double, double*, double*, int);
  static KS_getFrequencyResponseProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_getFrequencyResponseProc)GetProcAddress(_hKitharaKrtsDll, "KS_getFrequencyResponse");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_getFrequencyResponse not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hFilter, frequency, pAmplitude, pPhase, flags);
}

//------ KS_scaleSignalFilter ------
Error __stdcall KS_scaleSignalFilter(KSHandle hFilter, double frequency, double value, int flags) {
  typedef Error (__stdcall* KS_scaleSignalFilterProc)(KSHandle, double, double, int);
  static KS_scaleSignalFilterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_scaleSignalFilterProc)GetProcAddress(_hKitharaKrtsDll, "KS_scaleSignalFilter");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_scaleSignalFilter not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hFilter, frequency, value, flags);
}

//------ KS_resetSignalFilter ------
Error __stdcall KS_resetSignalFilter(KSHandle hFilter, int flags) {
  typedef Error (__stdcall* KS_resetSignalFilterProc)(KSHandle, int);
  static KS_resetSignalFilterProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_resetSignalFilterProc)GetProcAddress(_hKitharaKrtsDll, "KS_resetSignalFilter");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_resetSignalFilter not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hFilter, flags);
}

//------ KS_createPIDController ------
Error __stdcall KS_createPIDController(KSHandle* phController, KSPIDControllerParams* pParams, int flags) {
  typedef Error (__stdcall* KS_createPIDControllerProc)(KSHandle*, KSPIDControllerParams*, int);
  static KS_createPIDControllerProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_createPIDControllerProc)GetProcAddress(_hKitharaKrtsDll, "KS_createPIDController");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_createPIDController not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(phController, pParams, flags);
}

//------ KS_changeSetpoint ------
Error __stdcall KS_changeSetpoint(KSHandle hController, double setpoint, int flags) {
  typedef Error (__stdcall* KS_changeSetpointProc)(KSHandle, double, int);
  static KS_changeSetpointProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_changeSetpointProc)GetProcAddress(_hKitharaKrtsDll, "KS_changeSetpoint");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_changeSetpoint not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hController, setpoint, flags);
}

//------ KS_removeSignalProcessor ------
Error __stdcall KS_removeSignalProcessor(KSHandle hSigProc, int flags) {
  typedef Error (__stdcall* KS_removeSignalProcessorProc)(KSHandle, int);
  static KS_removeSignalProcessorProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_removeSignalProcessorProc)GetProcAddress(_hKitharaKrtsDll, "KS_removeSignalProcessor");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_removeSignalProcessor not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSigProc, flags);
}

//------ KS_processSignal ------
Error __stdcall KS_processSignal(KSHandle hSigProc, double input, double* pOutput, int flags) {
  typedef Error (__stdcall* KS_processSignalProc)(KSHandle, double, double*, int);
  static KS_processSignalProc _pProc;

  if (!_pProc) {
    if (!_hKitharaKrtsDll) {
      _hKitharaKrtsDll = LoadLibrary(_kitharaKrtsDllToLoad);
      if (!_hKitharaKrtsDll) {
        KS_logMessage(0, "ERROR: Failed to load DLL");
        return KSERROR_CANNOT_FIND_LIBRARY | GetLastError();
      }
    }

    _pProc = (KS_processSignalProc)GetProcAddress(_hKitharaKrtsDll, "KS_processSignal");
    if (!_pProc) {
      KS_logMessage(0, "ERROR: KS_processSignal not found in DLL");
      return KSERROR_CANNOT_FIND_ADDRESS | 0x1000 | GetLastError();
    }
  }

  return (*_pProc)(hSigProc, input, pOutput, flags);
}

//------ _initSigProcModule ------
static void _initSigProcModule() {
  _registerKernelAddress("KS_createSignalFilter", (CallBackProc)KS_createSignalFilter);
  _registerKernelAddress("KS_createIndividualSignalFilter", (CallBackProc)KS_createIndividualSignalFilter);
  _registerKernelAddress("KS_getFrequencyResponse", (CallBackProc)KS_getFrequencyResponse);
  _registerKernelAddress("KS_scaleSignalFilter", (CallBackProc)KS_scaleSignalFilter);
  _registerKernelAddress("KS_resetSignalFilter", (CallBackProc)KS_resetSignalFilter);
  _registerKernelAddress("KS_createPIDController", (CallBackProc)KS_createPIDController);
  _registerKernelAddress("KS_changeSetpoint", (CallBackProc)KS_changeSetpoint);
  _registerKernelAddress("KS_removeSignalProcessor", (CallBackProc)KS_removeSignalProcessor);
  _registerKernelAddress("KS_processSignal", (CallBackProc)KS_processSignal);
}

#define KS_SIGPROC_MODULE_HAS_RTX

//------ _registerKernelAddress ------
static void _registerKernelAddress(char* pName, CallBackProc pProc) {
#ifdef KS_KERNEL_MODULE_HAS_RTX
  KS_registerKernelAddress(pName, pProc, NULL, KSF_INTERNAL);
#else
  // nothing's done, because there is no Kernel Module
#endif
}

//------ _initModules ------
static void _initModules() {

#ifdef KS_BASE_MODULE_HAS_RTX
  _initBaseModule();
#endif
#ifdef KS_KERNEL_MODULE_HAS_RTX
  _initKernelModule();
#endif
#ifdef KS_IOPORT_MODULE_HAS_RTX
  _initIoPortModule();
#endif
#ifdef KS_MEMORY_MODULE_HAS_RTX
  _initMemoryModule();
#endif
#ifdef KS_CLOCK_MODULE_HAS_RTX
  _initClockModule();
#endif
#ifdef KS_SYSTEM_MODULE_HAS_RTX
  _initSystemModule();
#endif
#ifdef KS_DEVICE_MODULE_HAS_RTX
  _initDeviceModule();
#endif
#ifdef KS_KEYBOARD_MODULE_HAS_RTX
  _initKeyboardModule();
#endif
#ifdef KS_INTERRUPT_MODULE_HAS_RTX
  _initInterruptModule();
#endif
#ifdef KS_UART_MODULE_HAS_RTX
  _initUARTModule();
#endif
#ifdef KS_COMM_MODULE_HAS_RTX
  _initCOMMModule();
#endif
#ifdef KS_USB_MODULE_HAS_RTX
  _initUSBModule();
#endif
#ifdef KS_XHCI_MODULE_HAS_RTX
  _initXHCIModule();
#endif
#ifdef KS_TIMER_MODULE_HAS_RTX
  _initTimerModule();
#endif
#ifdef KS_REALTIME_MODULE_HAS_RTX
  _initRealTimeModule();
#endif
#ifdef KS_TASK_MODULE_HAS_RTX
  _initTaskModule();
#endif
#ifdef KS_PACKET_MODULE_HAS_RTX
  _initPacketModule();
#endif
#ifdef KS_SOCKET_MODULE_HAS_RTX
  _initSocketModule();
#endif
#ifdef KS_ETHERCAT_MODULE_HAS_RTX
  _initEtherCATModule();
#endif
#ifdef KS_MULTIFUNCTION_MODULE_HAS_RTX
  _initMultiFunctionModule();
#endif
#ifdef KS_LIN_MODULE_HAS_RTX
  _initLINModule();
#endif
#ifdef KS_CAN_MODULE_HAS_RTX
  _initCANModule();
#endif
#ifdef KS_CANOPEN_MODULE_HAS_RTX
  _initCANopenModule();
#endif
#ifdef KS_PROFIBUS_MODULE_HAS_RTX
  _initProfibusModule();
#endif
#ifdef KS_PLC_MODULE_HAS_RTX
  _initPLCModule();
#endif
#ifdef KS_FLEXRAY_MODULE_HAS_RTX
  _initFlexRayModule();
#endif
#ifdef KS_CAMERA_MODULE_HAS_RTX
  _initCameraModule();
#endif
#ifdef KS_RTL_MODULE_HAS_RTX
  _initRTLModule();
#endif
#ifdef KS_DEDICATED_MODULE_HAS_RTX
  _initDedicatedModule();
#endif
#ifdef KS_SIGPROC_MODULE_HAS_RTX
  _initSigProcModule();
#endif
#ifdef KS_VISION_MODULE_HAS_RTX
  _initVisionModule();
#endif
#ifdef KS_DISK_MODULE_HAS_RTX
  _initDiskModule();
#endif
#ifdef KS_FILE_MODULE_HAS_RTX
  _initFileModule();
#endif
#ifdef KS_VARIABLE_MODULE_HAS_RTX
  _initVariableModule();
#endif

}

