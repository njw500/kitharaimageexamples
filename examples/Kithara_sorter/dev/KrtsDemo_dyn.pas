// Copyright (c) 1996-2016 by Kithara Software GmbH. All rights reserved.

//##############################################################################################################
//
// File:             KrtsDemo_dyn.pas (v10.00d)
//
// Description:      Delphi API for Kithara �RealTime Suite�
//
// REV   DATE        LOG    DESCRIPTION
// ----- ----------  ------ ------------------------------------------------------------------------------------
// u.jes 1996-12-16  CREAT: Original - (file created)
// ***** 2016-07-08  SAVED: This file was generated on 2016-07-08 at 12:51:01
//
//##############################################################################################################

//--------------------------------------------------------------------------------------------------------------
// KrtsDemo
//--------------------------------------------------------------------------------------------------------------

unit KrtsDemo_dyn;

//--------------------------------------------------------------------------------------------------------------
// interface
//--------------------------------------------------------------------------------------------------------------

interface

//--------------------------------------------------------------------------------------------------------------
// Base Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Categories ------
  // Operating system errors:
  KSERROR_CATEGORY_OPERATING_SYSTEM             = $00000000;

  // Categories 0x01000000 ... 0x0f000000 are free for customer usage!
  KSERROR_CATEGORY_USER                         = $0f000000;
  KSERROR_CATEGORY_BASE                         = $10000000;
  KSERROR_CATEGORY_DEAL                         = $11000000;
  KSERROR_CATEGORY_HANDLING                     = $17000000;
  KSERROR_CATEGORY_IOPORT                       = $14000000;
  KSERROR_CATEGORY_MEMORY                       = $12000000;
  KSERROR_CATEGORY_KERNEL                       = $18000000;
  KSERROR_CATEGORY_KEYBOARD                     = $1a000000;
  KSERROR_CATEGORY_INTERRUPT                    = $13000000;
  KSERROR_CATEGORY_TIMER                        = $16000000;
  KSERROR_CATEGORY_USB                          = $19000000;
  KSERROR_CATEGORY_IDT                          = $1b000000;
  KSERROR_CATEGORY_NODE                         = $1c000000;
  KSERROR_CATEGORY_SOCKET                       = $1d000000;
  KSERROR_CATEGORY_SYSTEM                       = $1e000000;
  KSERROR_CATEGORY_ETHERCAT                     = $1f000000;
  KSERROR_CATEGORY_MF                           = $20000000;
  KSERROR_CATEGORY_CAN                          = $21000000;
  KSERROR_CATEGORY_PROFIBUS                     = $22000000;
  KSERROR_CATEGORY_PLC                          = $23000000;
  KSERROR_CATEGORY_CANOPEN                      = $24000000;
  KSERROR_CATEGORY_FLEXRAY                      = $25000000;
  KSERROR_CATEGORY_PACKET                       = $26000000;
  KSERROR_CATEGORY_CAMERA                       = $27000000;
  KSERROR_CATEGORY_TASK                         = $28000000;
  KSERROR_CATEGORY_SPECIAL                      = $29000000;
  KSERROR_CATEGORY_XHCI                         = $2a000000;
  KSERROR_CATEGORY_DISK                         = $2b000000;
  KSERROR_CATEGORY_FILE                         = $2c000000;
  KSERROR_CATEGORY_NEXT                         = $2d000000;
  KSERROR_CATEGORY_EXCEPTION                    = $3f000000;

  //------ Error Codes ------
  KSERROR_OPERATING_SYSTEM                      = (KSERROR_CATEGORY_BASE+$00000000);
  KSERROR_UNKNOWN_ERROR_CODE                    = (KSERROR_CATEGORY_BASE+$00010000);
  KSERROR_UNKNOWN_ERROR_CATEGORY                = (KSERROR_CATEGORY_BASE+$00020000);
  KSERROR_UNKNOWN_ERROR_LANGUAGE                = (KSERROR_CATEGORY_BASE+$00030000);
  KSERROR_CANNOT_FIND_LIBRARY                   = (KSERROR_CATEGORY_BASE+$00040000);
  KSERROR_CANNOT_FIND_ADDRESS                   = (KSERROR_CATEGORY_BASE+$00050000);
  KSERROR_BAD_PARAM                             = (KSERROR_CATEGORY_BASE+$00060000);
  KSERROR_BAD_VERSION                           = (KSERROR_CATEGORY_BASE+$00070000);
  KSERROR_INTERNAL                              = (KSERROR_CATEGORY_BASE+$00080000);
  KSERROR_UNKNOWN                               = (KSERROR_CATEGORY_BASE+$00090000);
  KSERROR_FUNCTION_NOT_AVAILABLE                = (KSERROR_CATEGORY_BASE+$000a0000);
  KSERROR_DRIVER_NOT_OPENED                     = (KSERROR_CATEGORY_BASE+$000b0000);
  KSERROR_NOT_ENOUGH_MEMORY                     = (KSERROR_CATEGORY_BASE+$000c0000);
  KSERROR_CANNOT_OPEN_KERNEL                    = (KSERROR_CATEGORY_BASE+$000d0000);
  KSERROR_CANNOT_CLOSE_KERNEL                   = (KSERROR_CATEGORY_BASE+$000e0000);
  KSERROR_BAD_KERNEL_VERSION                    = (KSERROR_CATEGORY_BASE+$000f0000);
  KSERROR_CANNOT_FIND_KERNEL                    = (KSERROR_CATEGORY_BASE+$00100000);
  KSERROR_INVALID_HANDLE                        = (KSERROR_CATEGORY_BASE+$00110000);
  KSERROR_DEVICE_IO_FAILED                      = (KSERROR_CATEGORY_BASE+$00120000);
  KSERROR_REGISTRY_FAILURE                      = (KSERROR_CATEGORY_BASE+$00130000);
  KSERROR_CANNOT_START_KERNEL                   = (KSERROR_CATEGORY_BASE+$00140000);
  KSERROR_CANNOT_STOP_KERNEL                    = (KSERROR_CATEGORY_BASE+$00150000);
  KSERROR_ACCESS_DENIED                         = (KSERROR_CATEGORY_BASE+$00160000);
  KSERROR_DEVICE_NOT_OPENED                     = (KSERROR_CATEGORY_BASE+$00170000);
  KSERROR_FUNCTION_DENIED                       = (KSERROR_CATEGORY_BASE+$00180000);
  KSERROR_DEVICE_ALREADY_USED                   = (KSERROR_CATEGORY_BASE+$00190000);
  KSERROR_OUT_OF_MEMORY                         = (KSERROR_CATEGORY_BASE+$001a0000);
  KSERROR_TOO_MANY_OPEN_PROCESSES               = (KSERROR_CATEGORY_BASE+$001b0000);
  KSERROR_SIGNAL_OVERFLOW                       = (KSERROR_CATEGORY_BASE+$001c0000);
  KSERROR_CANNOT_SIGNAL                         = (KSERROR_CATEGORY_BASE+$001d0000);
  KSERROR_FILE_NOT_FOUND                        = (KSERROR_CATEGORY_BASE+$001e0000);
  KSERROR_DEVICE_NOT_FOUND                      = (KSERROR_CATEGORY_BASE+$001f0000);
  KSERROR_ASSERTION_FAILED                      = (KSERROR_CATEGORY_BASE+$00200000);
  KSERROR_FUNCTION_FAILED                       = (KSERROR_CATEGORY_BASE+$00210000);
  KSERROR_OPERATION_ABORTED                     = (KSERROR_CATEGORY_BASE+$00220000);
  KSERROR_TIMEOUT                               = (KSERROR_CATEGORY_BASE+$00230000);
  KSERROR_REBOOT_NECESSARY                      = (KSERROR_CATEGORY_BASE+$00240000);
  KSERROR_DEVICE_DISABLED                       = (KSERROR_CATEGORY_BASE+$00250000);
  KSERROR_ATTACH_FAILED                         = (KSERROR_CATEGORY_BASE+$00260000);
  KSERROR_DEVICE_REMOVED                        = (KSERROR_CATEGORY_BASE+$00270000);
  KSERROR_TYPE_MISMATCH                         = (KSERROR_CATEGORY_BASE+$00280000);
  KSERROR_FUNCTION_NOT_IMPLEMENTED              = (KSERROR_CATEGORY_BASE+$00290000);
  KSERROR_COMMUNICATION_BROKEN                  = (KSERROR_CATEGORY_BASE+$002a0000);
  KSERROR_DEVICE_NOT_READY                      = (KSERROR_CATEGORY_BASE+$00370000);
  KSERROR_NO_RESPONSE                           = (KSERROR_CATEGORY_BASE+$00380000);
  KSERROR_OPERATION_PENDING                     = (KSERROR_CATEGORY_BASE+$00390000);
  KSERROR_PORT_BUSY                             = (KSERROR_CATEGORY_BASE+$003a0000);
  KSERROR_UNKNOWN_SYSTEM                        = (KSERROR_CATEGORY_BASE+$003b0000);
  KSERROR_BAD_CONTEXT                           = (KSERROR_CATEGORY_BASE+$003c0000);
  KSERROR_END_OF_FILE                           = (KSERROR_CATEGORY_BASE+$003d0000);
  KSERROR_INTERRUPT_ACTIVATION_FAILED           = (KSERROR_CATEGORY_BASE+$003e0000);
  KSERROR_INTERRUPT_IS_SHARED                   = (KSERROR_CATEGORY_BASE+$003f0000);
  KSERROR_NO_REALTIME_ACCESS                    = (KSERROR_CATEGORY_BASE+$00400000);
  KSERROR_HARDWARE_NOT_SUPPORTED                = (KSERROR_CATEGORY_BASE+$00410000);
  KSERROR_TIMER_ACTIVATION_FAILED               = (KSERROR_CATEGORY_BASE+$00420000);
  KSERROR_SOCKET_FAILURE                        = (KSERROR_CATEGORY_BASE+$00430000);
  KSERROR_LINE_ERROR                            = (KSERROR_CATEGORY_BASE+$00440000);
  KSERROR_STACK_CORRUPTION                      = (KSERROR_CATEGORY_BASE+$00450000);
  KSERROR_INVALID_ARGUMENT                      = (KSERROR_CATEGORY_BASE+$00460000);
  KSERROR_PARSE_ERROR                           = (KSERROR_CATEGORY_BASE+$00470000);
  KSERROR_INVALID_IDENTIFIER                    = (KSERROR_CATEGORY_BASE+$00480000);
  KSERROR_REALTIME_ALREADY_USED                 = (KSERROR_CATEGORY_BASE+$00490000);
  KSERROR_COMMUNICATION_FAILED                  = (KSERROR_CATEGORY_BASE+$004a0000);
  KSERROR_INVALID_SIZE                          = (KSERROR_CATEGORY_BASE+$004b0000);
  KSERROR_OBJECT_NOT_FOUND                      = (KSERROR_CATEGORY_BASE+$004c0000);

  //------ Error codes of handling category ------
  KSERROR_CANNOT_CREATE_EVENT                   = (KSERROR_CATEGORY_HANDLING+$00000000);
  KSERROR_CANNOT_CREATE_THREAD                  = (KSERROR_CATEGORY_HANDLING+$00010000);
  KSERROR_CANNOT_BLOCK_THREAD                   = (KSERROR_CATEGORY_HANDLING+$00020000);
  KSERROR_CANNOT_SIGNAL_EVENT                   = (KSERROR_CATEGORY_HANDLING+$00030000);
  KSERROR_CANNOT_POST_MESSAGE                   = (KSERROR_CATEGORY_HANDLING+$00040000);
  KSERROR_CANNOT_CREATE_SHARED                  = (KSERROR_CATEGORY_HANDLING+$00050000);
  KSERROR_SHARED_DIFFERENT_SIZE                 = (KSERROR_CATEGORY_HANDLING+$00060000);
  KSERROR_BAD_SHARED_MEM                        = (KSERROR_CATEGORY_HANDLING+$00070000);
  KSERROR_WAIT_TIMEOUT                          = (KSERROR_CATEGORY_HANDLING+$00080000);
  KSERROR_EVENT_NOT_SIGNALED                    = (KSERROR_CATEGORY_HANDLING+$00090000);
  KSERROR_CANNOT_CREATE_CALLBACK                = (KSERROR_CATEGORY_HANDLING+$000a0000);
  KSERROR_NO_DATA_AVAILABLE                     = (KSERROR_CATEGORY_HANDLING+$000b0000);
  KSERROR_REQUEST_NESTED                        = (KSERROR_CATEGORY_HANDLING+$000c0000);

  //------ Deal error codes ------
  KSERROR_BAD_CUSTNUM                           = (KSERROR_CATEGORY_DEAL+$00030000);
  KSERROR_KEY_BAD_FORMAT                        = (KSERROR_CATEGORY_DEAL+$00060000);
  KSERROR_KEY_BAD_DATA                          = (KSERROR_CATEGORY_DEAL+$00080000);
  KSERROR_KEY_PERMISSION_EXPIRED                = (KSERROR_CATEGORY_DEAL+$00090000);
  KSERROR_BAD_LICENCE                           = (KSERROR_CATEGORY_DEAL+$000a0000);
  KSERROR_CANNOT_LOAD_KEY                       = (KSERROR_CATEGORY_DEAL+$000c0000);
  KSERROR_BAD_NAME_OR_FIRM                      = (KSERROR_CATEGORY_DEAL+$000f0000);
  KSERROR_NO_LICENCES_FOUND                     = (KSERROR_CATEGORY_DEAL+$00100000);
  KSERROR_LICENCE_CAPACITY_EXHAUSTED            = (KSERROR_CATEGORY_DEAL+$00110000);
  KSERROR_FEATURE_NOT_LICENSED                  = (KSERROR_CATEGORY_DEAL+$00120000);

  //------ Exception Codes ------
  KSERROR_DIVIDE_BY_ZERO                        = (KSERROR_CATEGORY_EXCEPTION+$00000000);
  KSERROR_DEBUG_EXCEPTION                       = (KSERROR_CATEGORY_EXCEPTION+$00010000);
  KSERROR_NONMASKABLE_INTERRUPT                 = (KSERROR_CATEGORY_EXCEPTION+$00020000);
  KSERROR_BREAKPOINT                            = (KSERROR_CATEGORY_EXCEPTION+$00030000);
  KSERROR_OVERFLOW                              = (KSERROR_CATEGORY_EXCEPTION+$00040000);
  KSERROR_BOUND_RANGE                           = (KSERROR_CATEGORY_EXCEPTION+$00050000);
  KSERROR_INVALID_OPCODE                        = (KSERROR_CATEGORY_EXCEPTION+$00060000);
  KSERROR_NO_MATH                               = (KSERROR_CATEGORY_EXCEPTION+$00070000);
  KSERROR_DOUBLE_FAULT                          = (KSERROR_CATEGORY_EXCEPTION+$00080000);
  KSERROR_COPROCESSOR_SEGMENT_OVERRUN           = (KSERROR_CATEGORY_EXCEPTION+$00090000);
  KSERROR_INVALID_TSS                           = (KSERROR_CATEGORY_EXCEPTION+$000a0000);
  KSERROR_SEGMENT_NOT_PRESENT                   = (KSERROR_CATEGORY_EXCEPTION+$000b0000);
  KSERROR_STACK_FAULT                           = (KSERROR_CATEGORY_EXCEPTION+$000c0000);
  KSERROR_PROTECTION_FAULT                      = (KSERROR_CATEGORY_EXCEPTION+$000d0000);
  KSERROR_PAGE_FAULT                            = (KSERROR_CATEGORY_EXCEPTION+$000e0000);
  KSERROR_FPU_FAULT                             = (KSERROR_CATEGORY_EXCEPTION+$00100000);
  KSERROR_ALIGNMENT_CHECK                       = (KSERROR_CATEGORY_EXCEPTION+$00110000);
  KSERROR_MACHINE_CHECK                         = (KSERROR_CATEGORY_EXCEPTION+$00120000);
  KSERROR_SIMD_FAULT                            = (KSERROR_CATEGORY_EXCEPTION+$00130000);

  //------ Flags ------
  KSF_OPENED                                    = $01000000;
  KSF_ACTIVE                                    = $02000000;
  KSF_RUNNING                                   = $02000000;
  KSF_FINISHED                                  = $04000000;
  KSF_CANCELED                                  = $08000000;
  KSF_DISABLED                                  = $10000000;
  KSF_REQUESTED                                 = $20000000;
  KSF_TERMINATE                                 = $00000000;
  KSF_DONT_WAIT                                 = $00000001;
  KSF_KERNEL_EXEC                               = $00000004;
  KSF_ASYNC_EXEC                                = $0000000c;
  KSF_DIRECT_EXEC                               = $0000000c;
  KSF_DONT_START                                = $00000010;
  KSF_ONE_SHOT                                  = $00000020;
  KSF_SINGLE_SHOT                               = $00000020;
  KSF_SAVE_FPU                                  = $00000040;
  KSF_USE_SSE                                   = $00000001;
  KSF_USER_EXEC                                 = $00000080;
  KSF_REALTIME_EXEC                             = $00000100;
  KSF_WAIT                                      = $00000200;
  KSF_ACCEPT_INCOMPLETE                         = $00000200;
  KSF_REPORT_RESOURCE                           = $00000400;
  KSF_USE_TIMEOUTS                              = $00000400;
  KSF_FORCE_OVERRIDE                            = $00001000;
  KSF_OPTION_ON                                 = $00001000;
  KSF_OPTION_OFF                                = $00002000;
  KSF_FORCE_OPEN                                = $00002000;
  KSF_UNLIMITED                                 = $00004000;
  KSF_DONT_USE_SMP                              = $00004000;
  KSF_NO_CONTEXT                                = $00008000;
  KSF_HIGH_PRECISION                            = $00008000;
  KSF_CONTINUOUS                                = $00010000;
  KSF_ZERO_INIT                                 = $00020000;
  KSF_TX_PDO                                    = $00010000;
  KSF_RX_PDO                                    = $00020000;
  KSF_READ                                      = $00040000;
  KSF_WRITE                                     = $00080000;
  KSF_USE_SMP                                   = $00080000;
  KSF_ALTERNATIVE                               = $00200000;
  KSF_PDO                                       = $00040000;
  KSF_SDO                                       = $00080000;
  KSF_IDN                                       = $00100000;
  KSF_MANUAL_RESET                              = $00200000;
  KSF_RESET_STATE                               = $00800000;
  KSF_ISA_BUS                                   = $00000001;
  KSF_PCI_BUS                                   = $00000005;
  KSF_PRIO_NORMAL                               = $00000000;
  KSF_PRIO_LOW                                  = $00000001;
  KSF_PRIO_LOWER                                = $00000002;
  KSF_PRIO_LOWEST                               = $00000003;
  KSF_MESSAGE_PIPE                              = $00001000;
  KSF_PIPE_NOTIFY_GET                           = $00002000;
  KSF_PIPE_NOTIFY_PUT                           = $00004000;
  KSF_ALL_CPUS                                  = $00000000;
  KSF_READ_ACCESS                               = $00000001;
  KSF_WRITE_ACCESS                              = $00000002;
  KSF_SIZE_8_BIT                                = $00010000;
  KSF_SIZE_16_BIT                               = $00020000;
  KSF_SIZE_32_BIT                               = $00040000;

  //------ Obsolete flags - do not use! ------
  KSF_ACCEPT_PENDING                            = $00000002;
  KSF_DONT_RAISE                                = $00200000;

  //------ Constant for returning no-error ------
  KS_OK                                         = $00000000;

  //------ Language constants ------
  KSLNG_DEFAULT                                 = $00000000;

  //------ Context type values ------
  USER_CONTEXT                                  = $00000000;
  INTERRUPT_CONTEXT                             = $00000100;
  TIMER_CONTEXT                                 = $00000200;
  KEYBOARD_CONTEXT                              = $00000300;
  USB_BASE                                      = $00000506;
  PACKET_BASE                                   = $00000700;
  DEVICE_BASE                                   = $00000800;
  ETHERCAT_BASE                                 = $00000900;
  SOCKET_BASE                                   = $00000a00;
  TASK_BASE                                     = $00000b00;
  MULTIFUNCTION_BASE                            = $00000c00;
  CAN_BASE                                      = $00000d00;
  PROFIBUS_BASE                                 = $00000e00;
  CANOPEN_BASE                                  = $00000f00;
  FLEXRAY_BASE                                  = $00001000;
  XHCI_BASE                                     = $00001100;
  V86_BASE                                      = $00001200;
  PLC_BASE                                      = $00002000;

  //------ Config code for "Driver" ------
  KSCONFIG_DRIVER_NAME                          = $00000001;
  KSCONFIG_FUNCTION_LOGGING                     = $00000002;

  //------ Config code for "Base" ------
  KSCONFIG_DISABLE_MESSAGE_LOGGING              = $00000001;
  KSCONFIG_ENABLE_MESSAGE_LOGGING               = $00000002;
  KSCONFIG_FLUSH_MESSAGE_PIPE                   = $00000003;

  //------ Config code for "Debug" ------

  //------ Commonly used commands ------
  KS_ABORT_XMIT_CMD                             = $00000001;
  KS_ABORT_RECV_CMD                             = $00000002;
  KS_FLUSH_XMIT_BUF                             = $00000003;
  KS_FLUSH_RECV_BUF                             = $00000009;
  KS_SET_BAUD_RATE                              = $0000000c;

  //------ Pipe contexts ------
  PIPE_PUT_CONTEXT                              = (USER_CONTEXT+$00000040);
  PIPE_GET_CONTEXT                              = (USER_CONTEXT+$00000041);

  //------ Quick mutex levels ------
  KS_APP_LEVEL                                  = $00000000;
  KS_DPC_LEVEL                                  = $00000001;
  KS_ISR_LEVEL                                  = $00000002;
  KS_RTX_LEVEL                                  = $00000003;
  KS_CPU_LEVEL                                  = $00000004;

  //------ Data types ------
  KS_DATATYPE_UNKNOWN                           = $00000000;
  KS_DATATYPE_BOOLEAN                           = $00000001;
  KS_DATATYPE_INTEGER8                          = $00000002;
  KS_DATATYPE_UINTEGER8                         = $00000005;
  KS_DATATYPE_INTEGER16                         = $00000003;
  KS_DATATYPE_UINTEGER16                        = $00000006;
  KS_DATATYPE_INTEGER32                         = $00000004;
  KS_DATATYPE_UINTEGER32                        = $00000007;
  KS_DATATYPE_INTEGER64                         = $00000015;
  KS_DATATYPE_UNSIGNED64                        = $0000001b;
  KS_DATATYPE_REAL32                            = $00000008;
  KS_DATATYPE_REAL64                            = $00000011;
  KS_DATATYPE_TINY                              = $00000002;
  KS_DATATYPE_BYTE                              = $00000005;
  KS_DATATYPE_SHORT                             = $00000003;
  KS_DATATYPE_USHORT                            = $00000006;
  KS_DATATYPE_INT                               = $00000004;
  KS_DATATYPE_UINT                              = $00000007;
  KS_DATATYPE_LLONG                             = $00000015;
  KS_DATATYPE_ULONG                             = $0000001b;
  KS_DATATYPE_VISIBLE_STRING                    = $00000009;
  KS_DATATYPE_OCTET_STRING                      = $0000000a;
  KS_DATATYPE_UNICODE_STRING                    = $0000000b;
  KS_DATATYPE_TIME_OF_DAY                       = $0000000c;
  KS_DATATYPE_TIME_DIFFERENCE                   = $0000000d;
  KS_DATATYPE_DOMAIN                            = $0000000f;
  KS_DATATYPE_TIME_OF_DAY32                     = $0000001c;
  KS_DATATYPE_DATE32                            = $0000001d;
  KS_DATATYPE_DATE_AND_TIME64                   = $0000001e;
  KS_DATATYPE_TIME_DIFFERENCE64                 = $0000001f;
  KS_DATATYPE_BIT1                              = $00000030;
  KS_DATATYPE_BIT2                              = $00000031;
  KS_DATATYPE_BIT3                              = $00000032;
  KS_DATATYPE_BIT4                              = $00000033;
  KS_DATATYPE_BIT5                              = $00000034;
  KS_DATATYPE_BIT6                              = $00000035;
  KS_DATATYPE_BIT7                              = $00000036;
  KS_DATATYPE_BIT8                              = $00000037;
  KS_DATATYPE_INTEGER24                         = $00000010;
  KS_DATATYPE_UNSIGNED24                        = $00000016;
  KS_DATATYPE_INTEGER40                         = $00000012;
  KS_DATATYPE_UNSIGNED40                        = $00000018;
  KS_DATATYPE_INTEGER48                         = $00000013;
  KS_DATATYPE_UNSIGNED48                        = $00000019;
  KS_DATATYPE_INTEGER56                         = $00000014;
  KS_DATATYPE_UNSIGNED56                        = $0000001a;
  KS_DATATYPE_KSHANDLE                          = $00000040;

  //------ DataObj type flags ------
  KS_DATAOBJ_READABLE                           = $00010000;
  KS_DATAOBJ_WRITEABLE                          = $00020000;
  KS_DATAOBJ_ARRAY                              = $01000000;
  KS_DATAOBJ_ENUM                               = $02000000;
  KS_DATAOBJ_STRUCT                             = $04000000;

  //------ Common Constants ------
  {$IFNDEF WIN64}
  KS_INVALID_HANDLE                             = NIL;
  {$ELSE}
  KS_INVALID_HANDLE                             = $00000000;
  {$ENDIF}

type
  //------ Common types and structures ------
  Int8         = ShortInt;
  Int16        = SmallInt;
  Int32        = LongInt;
  Int          = Int32;
  UInt8        = Byte;
  UInt16       = Word;
  UInt32       = LongWord;
  UInt         = UInt32;

  Error        = Int32;

  {$IFNDEF WIN64}
  KSHandle     = Pointer;
  {$ELSE}
  KSHandle     = Int32;
  {$ENDIF}
  Win32Handle  = Pointer;

  PByte        = ^Byte;
  PInt         = ^Int;
  PUInt        = ^UInt;
  PInt64       = ^Int64;
  PSingle      = ^Single;
  PDouble      = ^Double;
  PKSHandle    = ^KSHandle;
  PWin32Handle = ^Win32Handle;
  PPointer     = ^Pointer;

  {$IFNDEF KS_NO_KITHARA_LEGACY}
  Handle       = KSHandle;
  PHandle      = ^Handle;
  {$ENDIF}

  UInt64 = packed record
    lo: uint;
    hi: uint;
  end;
  PUInt64 = ^UInt64;

  HandlerState = packed record
    requested: int;
    performed: int;
    state: int;
    error: Error;
  end;
  PHandlerState = ^HandlerState;

  KSTimeOut = packed record
    baseTime: int;
    charTime: int;
  end;
  PKSTimeOut = ^KSTimeOut;

  KSDeviceInfo = packed record
    structSize: int;
    pDeviceName: array [0..79] of AnsiChar;
    pDeviceDesc: array [0..79] of AnsiChar;
    pDeviceVendor: array [0..79] of AnsiChar;
    pFriendlyName: array [0..79] of AnsiChar;
    pHardwareId: array [0..79] of AnsiChar;
    pClassName: array [0..79] of AnsiChar;
    pInfPath: array [0..79] of AnsiChar;
    pDriverDesc: array [0..79] of AnsiChar;
    pDriverVendor: array [0..79] of AnsiChar;
    pDriverDate: array [0..79] of AnsiChar;
    pDriverVersion: array [0..79] of AnsiChar;
    pServiceName: array [0..79] of AnsiChar;
    pServiceDisp: array [0..79] of AnsiChar;
    pServiceDesc: array [0..79] of AnsiChar;
    pServicePath: array [0..79] of AnsiChar;
  end;
  PKSDeviceInfo = ^KSDeviceInfo;

  KSSystemInformation = packed record
    structSize: int;
    numberOfCPUs: int;
    numberOfSharedCPUs: int;
    kiloBytesOfRAM: int;
    isDll64Bit: byte;
    isSys64Bit: byte;
    isStableVersion: byte;
    isLogVersion: byte;
    dllVersion: int;
    dllBuildNumber: int;
    sysVersion: int;
    sysBuildNumber: int;
    systemVersion: int;
    pSystemName: array [0..63] of AnsiChar;
    pServicePack: array [0..63] of AnsiChar;
    pDllPath: array [0..255] of AnsiChar;
    pSysPath: array [0..255] of AnsiChar;
  end;
  PKSSystemInformation = ^KSSystemInformation;

  KSProcessorInformation = packed record
    structSize: int;
    index: int;
    group: int;
    number: int;
    currentTemperature: int;
    allowedTemperature: int;
  end;
  PKSProcessorInformation = ^KSProcessorInformation;

  KSSharedMemInfo = packed record
    structSize: int;
    hHandle: KSHandle;
    pName: array [0..255] of AnsiChar;
    size: int;
    pThisPtr: Pointer;
  end;
  PKSSharedMemInfo = ^KSSharedMemInfo;

  //------ Context structures ------
  PipeUserContext = packed record
    ctxType: int;
    hPipe: KSHandle;
  end;
  PPipeUserContext = ^PipeUserContext;

//------ Helper functions ------
function KSERROR_CODE(error: Error):
  Error;

//------ Common functions ------
function KS_startKernel:
  Error; stdcall;
function KS_stopKernel:
  Error; stdcall;
function KS_resetKernel(flags: int):
  Error; stdcall;
function KS_openDriver(customerNumber: PAnsiChar):
  Error; stdcall;
function KS_closeDriver:
  Error; stdcall;
function KS_getDriverVersion(pVersion: PUInt):
  Error; stdcall;
function KS_getErrorString(code: Error; var msg: PAnsiChar; language: uint):
  Error; stdcall;
function KS_addErrorString(code: Error; msg: PAnsiChar; language: uint):
  Error; stdcall;
function KS_getDriverConfig(module: PAnsiChar; configType: int; pData: Pointer):
  Error; stdcall;
function KS_setDriverConfig(module: PAnsiChar; configType: int; pData: Pointer):
  Error; stdcall;
function KS_getSystemInformation(pSystemInfo: PKSSystemInformation; flags: int):
  Error; stdcall;
function KS_getProcessorInformation(index: int; pProcessorInfo: PKSProcessorInformation; flags: int):
  Error; stdcall;
function KS_closeHandle(handle: KSHandle; flags: int):
  Error; stdcall;

//------ Debugging & test ------
function KS_showMessage(ksError: Error; msg: PAnsiChar):
  Error; stdcall;
function KS_logMessage(msgType: int; msgText: PAnsiChar):
  Error; stdcall;
function KS_bufMessage(msgType: int; pBuffer: Pointer; length: int; msgText: PAnsiChar):
  Error; stdcall;
function KS_dbgMessage(fileName: PAnsiChar; line: int; msgText: PAnsiChar):
  Error; stdcall;
function KS_beep(freq: int; duration: int):
  Error; stdcall;
function KS_throwException(error: int; pText: PAnsiChar; pFile: PAnsiChar; line: int):
  Error; stdcall;

//------ Device handling ------
function KS_enumDevices(deviceType: PAnsiChar; index: int; pDeviceNameBuf: PAnsiChar; flags: int):
  Error; stdcall;
function KS_enumDevicesEx(className: PAnsiChar; busType: PAnsiChar; hEnumerator: KSHandle; index: int; pDeviceNameBuf: PAnsiChar; flags: int):
  Error; stdcall;
function KS_getDevices(className: PAnsiChar; busType: PAnsiChar; hEnumerator: KSHandle; pCount: PInt; pBuf: PPointer; size: int; flags: int):
  Error; stdcall;
function KS_getDeviceInfo(deviceName: PAnsiChar; pDeviceInfo: PKSDeviceInfo; flags: int):
  Error; stdcall;
function KS_updateDriver(deviceName: PAnsiChar; fileName: PAnsiChar; flags: int):
  Error; stdcall;

//------ Threads & priorities ------
function KS_createThread(procName: Pointer; pArgs: Pointer; phThread: PWin32Handle):
  Error; stdcall;
function KS_removeThread:
  Error; stdcall;
function KS_setThreadPrio(prio: int):
  Error; stdcall;
function KS_getThreadPrio(pPrio: PUInt):
  Error; stdcall;

//------ Shared memory ------
function KS_createSharedMem(ppAppPtr: PPointer; ppSysPtr: PPointer; name: PAnsiChar; size: int; flags: int):
  Error; stdcall;
function KS_freeSharedMem(pAppPtr: Pointer):
  Error; stdcall;
function KS_getSharedMem(ppPtr: PPointer; name: PAnsiChar):
  Error; stdcall;
function KS_readMem(pBuffer: Pointer; pAppPtr: Pointer; size: int; offset: int):
  Error; stdcall;
function KS_writeMem(pBuffer: Pointer; pAppPtr: Pointer; size: int; offset: int):
  Error; stdcall;
function KS_createSharedMemEx(phHandle: PKSHandle; name: PAnsiChar; size: int; flags: int):
  Error; stdcall;
function KS_freeSharedMemEx(hHandle: KSHandle; flags: int):
  Error; stdcall;
function KS_getSharedMemEx(hHandle: KSHandle; pPtr: PPointer; flags: int):
  Error; stdcall;

//------ Events ------
function KS_createEvent(phEvent: PKSHandle; name: PAnsiChar; flags: int):
  Error; stdcall;
function KS_closeEvent(hEvent: KSHandle):
  Error; stdcall;
function KS_setEvent(hEvent: KSHandle):
  Error; stdcall;
function KS_resetEvent(hEvent: KSHandle):
  Error; stdcall;
function KS_pulseEvent(hEvent: KSHandle):
  Error; stdcall;
function KS_waitForEvent(hEvent: KSHandle; flags: int; timeout: int):
  Error; stdcall;
function KS_getEventState(hEvent: KSHandle; pState: PUInt):
  Error; stdcall;
function KS_postMessage(hWnd: Win32Handle; msg: uint; wP: uint; lP: uint):
  Error; stdcall;

//------ CallBacks ------
function KS_createCallBack(phCallBack: PKSHandle; routine: Pointer; pArgs: Pointer; flags: int; prio: int):
  Error; stdcall;
function KS_removeCallBack(hCallBack: KSHandle):
  Error; stdcall;
function KS_execCallBack(hCallBack: KSHandle; pContext: Pointer; flags: int):
  Error; stdcall;
function KS_getCallState(hSignal: KSHandle; pState: PHandlerState):
  Error; stdcall;
function KS_signalObject(hObject: KSHandle; pContext: Pointer; flags: int):
  Error; stdcall;

//------ Pipes ------
function KS_createPipe(phPipe: PKSHandle; name: PAnsiChar; itemSize: int; itemCount: int; hNotify: KSHandle; flags: int):
  Error; stdcall;
function KS_removePipe(hPipe: KSHandle):
  Error; stdcall;
function KS_flushPipe(hPipe: KSHandle; flags: int):
  Error; stdcall;
function KS_getPipe(hPipe: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_putPipe(hPipe: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;

//------ Quick mutexes ------
function KS_createQuickMutex(phMutex: PKSHandle; level: int; flags: int):
  Error; stdcall;
function KS_removeQuickMutex(hMutex: KSHandle):
  Error; stdcall;
function KS_requestQuickMutex(hMutex: KSHandle):
  Error; stdcall;
function KS_releaseQuickMutex(hMutex: KSHandle):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Kernel Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_RTX_TOO_MANY_FUNCTIONS                = (KSERROR_CATEGORY_KERNEL+$00000000);
  KSERROR_CANNOT_EXECUTE_RTX_CALL               = (KSERROR_CATEGORY_KERNEL+$00010000);
  KSERROR_CANNOT_PREPARE_RTX                    = (KSERROR_CATEGORY_KERNEL+$00020000);
  KSERROR_RTX_BAD_SYSTEM_CALL                   = (KSERROR_CATEGORY_KERNEL+$00030000);
  KSERROR_CANNOT_LOAD_KERNEL                    = (KSERROR_CATEGORY_KERNEL+$00040000);
  KSERROR_CANNOT_RELOCATE_KERNEL                = (KSERROR_CATEGORY_KERNEL+$00050000);
  KSERROR_KERNEL_ALREADY_LOADED                 = (KSERROR_CATEGORY_KERNEL+$00060000);
  KSERROR_KERNEL_NOT_LOADED                     = (KSERROR_CATEGORY_KERNEL+$00070000);

  //------ Flags ------
  KSF_ANALYSE_ONLY                              = $00010000;
  KSF_FUNC_TABLE_GIVEN                          = $00020000;
  KSF_FORCE_RELOC                               = $00040000;
  KSF_LOAD_DEPENDENCIES                         = $00001000;
  KSF_EXEC_ENTRY_POINT                          = $00002000;

  //------ Kernel commands ------
  KS_GET_KERNEL_BASE_ADDRESS                    = $00000001;

//------ Relocation by byte-wise instruction mapping ------
function KS_prepareKernelExec(callback: Pointer; pRelocated: PPointer; flags: int):
  Error; stdcall;
function KS_endKernelExec(relocated: Pointer):
  Error; stdcall;
function KS_testKernelExec(relocated: Pointer; pArgs: Pointer):
  Error; stdcall;

//------ Relocation by loading DLL into kernel-space ------
function KS_registerKernelAddress(name: PAnsiChar; appFunction: Pointer; sysFunction: Pointer; flags: int):
  Error; stdcall;
function KS_loadKernel(phKernel: PKSHandle; dllName: PAnsiChar; initProcName: PAnsiChar; pArgs: Pointer; flags: int):
  Error; stdcall;
function KS_loadKernelFromBuffer(phKernel: PKSHandle; dllName: PAnsiChar; pBuffer: Pointer; length: int; initProcName: PAnsiChar; pArgs: Pointer; flags: int):
  Error; stdcall;
function KS_freeKernel(hKernel: KSHandle):
  Error; stdcall;
function KS_execKernelFunction(hKernel: KSHandle; name: PAnsiChar; pArgs: Pointer; pContext: Pointer; flags: int):
  Error; stdcall;
function KS_getKernelFunction(hKernel: KSHandle; name: PAnsiChar; pRelocated: PPointer; flags: int):
  Error; stdcall;
function KS_enumKernelFunctions(hKernel: KSHandle; index: int; pName: PAnsiChar; flags: int):
  Error; stdcall;
function KS_execKernelCommand(hKernel: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;
function KS_createKernelCallBack(phCallBack: PKSHandle; hKernel: KSHandle; name: PAnsiChar; pArgs: Pointer; flags: int; prio: int):
  Error; stdcall;

//------ Helper functions ------
function KS_execSyncFunction(hHandler: KSHandle; proc: Pointer; pArgs: Pointer; flags: int):
  Error; stdcall;
function KS_execSystemCall(fileName: PAnsiChar; funcName: PAnsiChar; pData: Pointer; flags: int):
  Error; stdcall;

//------ Memory functions ------
function KS_memCpy(pDst: Pointer; pSrc: Pointer; size: int; flags: int):
  Error; stdcall;
function KS_memMove(pDst: Pointer; pSrc: Pointer; size: int; flags: int):
  Error; stdcall;
function KS_memSet(pMem: Pointer; value: int; size: int; flags: int):
  Error; stdcall;
function KS_malloc(ppAllocated: PPointer; bytes: int; flags: int):
  Error; stdcall;
function KS_free(pAllocated: Pointer; flags: int):
  Error; stdcall;

//------ Interlocked functions ------
function KS_interlocked(pInterlocked: Pointer; operation: int; pParameter1: Pointer; pParameter2: Pointer; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// IoPort Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_CANNOT_ENABLE_IO_RANGE                = (KSERROR_CATEGORY_IOPORT+$00000000);
  KSERROR_BUS_NOT_FOUND                         = (KSERROR_CATEGORY_IOPORT+$00010000);
  KSERROR_BAD_SLOT_NUMBER                       = (KSERROR_CATEGORY_IOPORT+$00020000);

  //------ Number of address entries in PCI data ------
  KS_PCI_TYPE0_ADDRESSES                        = $00000006;
  KS_PCI_TYPE1_ADDRESSES                        = $00000002;
  KS_PCI_TYPE2_ADDRESSES                        = $00000004;

  //------ Bit encodings for KSPciBusData.headerType ------
  KSF_PCI_MULTIFUNCTION                         = $00000080;
  KSF_PCI_DEVICE_TYPE                           = $00000000;
  KSF_PCI_PCI_BRIDGE_TYPE                       = $00000001;
  KSF_PCI_CARDBUS_BRIDGE_TYPE                   = $00000002;

  //------ Bit encodings for KSPciBusData.command ------
  KSF_PCI_ENABLE_IO_SPACE                       = $00000001;
  KSF_PCI_ENABLE_MEMORY_SPACE                   = $00000002;
  KSF_PCI_ENABLE_BUS_MASTER                     = $00000004;
  KSF_PCI_ENABLE_SPECIAL_CYCLES                 = $00000008;
  KSF_PCI_ENABLE_WRITE_AND_INVALIDATE           = $00000010;
  KSF_PCI_ENABLE_VGA_COMPATIBLE_PALETTE         = $00000020;
  KSF_PCI_ENABLE_PARITY                         = $00000040;
  KSF_PCI_ENABLE_WAIT_CYCLE                     = $00000080;
  KSF_PCI_ENABLE_SERR                           = $00000100;
  KSF_PCI_ENABLE_FAST_BACK_TO_BACK              = $00000200;

  //------ Bit encodings for KSPciBusData.status ------
  KSF_PCI_STATUS_CAPABILITIES_LIST              = $00000010;
  KSF_PCI_STATUS_FAST_BACK_TO_BACK              = $00000080;
  KSF_PCI_STATUS_DATA_PARITY_DETECTED           = $00000100;
  KSF_PCI_STATUS_DEVSEL                         = $00000600;
  KSF_PCI_STATUS_SIGNALED_TARGET_ABORT          = $00000800;
  KSF_PCI_STATUS_RECEIVED_TARGET_ABORT          = $00001000;
  KSF_PCI_STATUS_RECEIVED_MASTER_ABORT          = $00002000;
  KSF_PCI_STATUS_SIGNALED_SYSTEM_ERROR          = $00004000;
  KSF_PCI_STATUS_DETECTED_PARITY_ERROR          = $00008000;

  //------ Bit encodings for KSPciBusData.baseAddresses ------
  KSF_PCI_ADDRESS_IO_SPACE                      = $00000001;
  KSF_PCI_ADDRESS_MEMORY_TYPE_MASK              = $00000006;
  KSF_PCI_ADDRESS_MEMORY_PREFETCHABLE           = $00000008;

  //------ PCI types ------
  KSF_PCI_TYPE_32BIT                            = $00000000;
  KSF_PCI_TYPE_20BIT                            = $00000002;
  KSF_PCI_TYPE_64BIT                            = $00000004;

  //------ Resource types ------
  KSRESOURCE_TYPE_NONE                          = $00000000;
  KSRESOURCE_TYPE_PORT                          = $00000001;
  KSRESOURCE_TYPE_INTERRUPT                     = $00000002;
  KSRESOURCE_TYPE_MEMORY                        = $00000003;
  KSRESOURCE_TYPE_STRING                        = $000000ff;

  //------ KSRESOURCE_SHARE ------
  KSRESOURCE_SHARE_UNDETERMINED                 = $00000000;
  KSRESOURCE_SHARE_DEVICEEXCLUSIVE              = $00000001;
  KSRESOURCE_SHARE_DRIVEREXCLUSIVE              = $00000002;
  KSRESOURCE_SHARE_SHARED                       = $00000003;

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_PORT ------
  KSRESOURCE_MODE_MEMORY                        = $00000000;
  KSRESOURCE_MODE_IO                            = $00000001;

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_INTERRUPT ------
  KSRESOURCE_MODE_LEVELSENSITIVE                = $00000000;
  KSRESOURCE_MODE_LATCHED                       = $00000001;

  //------ KSRESOURCE_MODE / KSRESOURCE_TYPE_MEMORY ------
  KSRESOURCE_MODE_READ_WRITE                    = $00000000;
  KSRESOURCE_MODE_READ_ONLY                     = $00000001;
  KSRESOURCE_MODE_WRITE_ONLY                    = $00000002;

type
  //------ Types & Structures ------
  KSPciBusData = packed record
    vendorID: Word;
    deviceID: Word;
    command: Word;
    status: Word;
    revisionID: byte;
    progIf: byte;
    subClass: byte;
    baseClass: byte;
    cacheLineSize: byte;
    latencyTimer: byte;
    headerType: byte;
    BIST: byte;
    pBaseAddresses: array [0..5] of uint;
    CIS: uint;
    subVendorID: Word;
    subSystemID: Word;
    baseROMAddress: uint;
    capabilityList: uint;
    reserved: uint;
    interruptLine: byte;
    interruptPin: byte;
    minimumGrant: byte;
    maximumLatency: byte;
    pDeviceSpecific: array [0..191] of byte;
  end;
  PKSPciBusData = ^KSPciBusData;

  KSResourceItem = packed record
    flags: uint;
    data0: uint;
    data1: uint;
    data2: uint;
  end;
  PKSResourceItem = ^KSResourceItem;

  KSResourceInfo = packed record
    busType: uint;
    busNumber: uint;
    version: uint;
    itemCount: uint;
    pItems: array [0..7] of KSResourceItem;
  end;
  PKSResourceInfo = ^KSResourceInfo;

  KSRangeResourceItem = packed record
    flags: uint;
    address: uint;
    reserved: uint;
    length: uint;
  end;
  PKSRangeResourceItem = ^KSRangeResourceItem;

  KSInterruptResourceItem = packed record
    flags: uint;
    level: uint;
    vector: uint;
    affinity: uint;
  end;
  PKSInterruptResourceItem = ^KSInterruptResourceItem;

  KSResourceInfoEx = packed record
    busType: int;
    busNumber: int;
    version: int;
    itemCount: int;
    range0: KSRangeResourceItem;
    range1: KSRangeResourceItem;
    range2: KSRangeResourceItem;
    range3: KSRangeResourceItem;
    range4: KSRangeResourceItem;
    range5: KSRangeResourceItem;
    interruptItem: KSInterruptResourceItem;
    reserved: KSResourceItem;
  end;
  PKSResourceInfoEx = ^KSResourceInfoEx;

//------ I/O port access functions ------
function KS_enableIoRange(ioPortBase: uint; count: int; flags: int):
  Error; stdcall;
function KS_readIoPort(ioPort: uint; pValue: PUInt; flags: int):
  Error; stdcall;
function KS_writeIoPort(ioPort: uint; value: uint; flags: int):
  Error; stdcall;
function KS_inpb(ioPort: uint):
  uint; stdcall;
function KS_inpw(ioPort: uint):
  uint; stdcall;
function KS_inpd(ioPort: uint):
  uint; stdcall;
procedure KS_outpb(ioPort: uint; value: uint)
  stdcall;
procedure KS_outpw(ioPort: uint; value: uint)
  stdcall;
procedure KS_outpd(ioPort: uint; value: uint)
  stdcall;

//------ Bus data & resource info ------
function KS_getBusData(pBusData: PKSPciBusData; busNumber: int; slotNumber: int; flags: int):
  Error; stdcall;
function KS_setBusData(pBusData: PKSPciBusData; busNumber: int; slotNumber: int; flags: int):
  Error; stdcall;
// DO NOT USE! - OBSOLETE! - USE 'KS_getResourceInfoEx' instead!
function KS_getResourceInfo(name: PAnsiChar; pInfo: PKSResourceInfo):
  Error; stdcall;
function KS_getResourceInfoEx(name: PAnsiChar; pInfo: PKSResourceInfoEx):
  Error; stdcall;

//------ Quiet section (obsolete) ------
function KS_enterQuietSection(flags: int):
  Error; stdcall;
function KS_releaseQuietSection(flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Memory Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_CANNOT_MAP_PHYSMEM                    = (KSERROR_CATEGORY_MEMORY+$00000000);
  KSERROR_CANNOT_UNMAP_PHYSMEM                  = (KSERROR_CATEGORY_MEMORY+$00010000);
  KSERROR_PHYSMEM_DIFFERENT_SIZE                = (KSERROR_CATEGORY_MEMORY+$00020000);
  KSERROR_CANNOT_ALLOC_PHYSMEM                  = (KSERROR_CATEGORY_MEMORY+$00030000);
  KSERROR_CANNOT_LOCK_MEM                       = (KSERROR_CATEGORY_MEMORY+$00040000);
  KSERROR_CANNOT_UNLOCK_MEM                     = (KSERROR_CATEGORY_MEMORY+$00050000);

  //------ Flags ------
  KSF_MAP_INTERNAL                              = $00000000;

//------ Physical memory management functions ------
function KS_mapDeviceMem(ppAppPtr: PPointer; ppSysPtr: PPointer; pInfo: PKSResourceInfoEx; index: int; flags: int):
  Error; stdcall;
function KS_mapPhysMem(ppAppPtr: PPointer; ppSysPtr: PPointer; physAddr: uint; size: int; flags: int):
  Error; stdcall;
function KS_unmapPhysMem(pAppPtr: Pointer):
  Error; stdcall;
function KS_allocPhysMem(ppAppPtr: PPointer; ppSysPtr: PPointer; physAddr: PUInt; size: int; flags: int):
  Error; stdcall;
function KS_freePhysMem(pAppPtr: Pointer):
  Error; stdcall;
function KS_copyPhysMem(physAddr: uint; pBuffer: Pointer; size: uint; offset: uint; flags: int):
  Error; stdcall;
function KS_getPhysAddr(pPhysAddr: PPointer; pDosLinAddr: Pointer):
  Error; stdcall;

// ATTENTION: functions for getting shared memory: see 'Base Module'

//--------------------------------------------------------------------------------------------------------------
// Clock Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_NO_RDTSC_AVAILABLE                    = (KSERROR_CATEGORY_SPECIAL+$00010000);

  //------ Flags ------
  KSF_USE_GIVEN_FREQ                            = $00010000;
  KSF_CLOCK_DELEGATE                            = $00000001;
  KSF_CLOCK_CONVERT_ONLY                        = $00000002;

  //------ Clock source constants (for 'clockId') ------
  KS_CLOCK_DEFAULT                              = $00000000;
  KS_CLOCK_MEASURE_SYSTEM_TIMER                 = $00000001;
  KS_CLOCK_MEASURE_CPU_COUNTER                  = $00000002;
  KS_CLOCK_MEASURE_APIC_TIMER                   = $00000003;
  KS_CLOCK_MEASURE_PM_TIMER                     = $00000004;
  KS_CLOCK_MEASURE_PC_TIMER                     = $00000005;
  KS_CLOCK_MEASURE_INTERVAL_COUNTER             = $00000006;
  KS_CLOCK_MEASURE_HPET                         = $00000007;
  KS_CLOCK_MEASURE_HIGHEST                      = $00000008;
  KS_CLOCK_CONVERT_HIGHEST                      = $00000009;
  KS_CLOCK_PC_TIMER                             = $0000000a;
  KS_CLOCK_MACHINE_TIME                         = $0000000b;
  KS_CLOCK_ABSOLUTE_TIME                        = $0000000c;
  KS_CLOCK_ABSOLUTE_TIME_LOCAL                  = $0000000d;
  KS_CLOCK_MICROSECONDS                         = $0000000e;
  KS_CLOCK_UNIX_TIME                            = $0000000f;
  KS_CLOCK_UNIX_TIME_LOCAL                      = $00000010;
  KS_CLOCK_MILLISECONDS                         = $00000011;
  KS_CLOCK_DOS_TIME                             = $00000012;
  KS_CLOCK_DOS_TIME_LOCAL                       = $00000013;
  KS_CLOCK_IEEE1588_TIME                        = $00000014;
  KS_CLOCK_IEEE1588_CONVERT                     = $00000015;
  KS_CLOCK_TIME_OF_DAY                          = $00000016;
  KS_CLOCK_TIME_OF_DAY_LOCAL                    = $00000017;
  KS_CLOCK_FIRST_USER                           = $00000018;

  //------ Obsolete clock source constant (use KS_CLOCK_MACHINE_TIME instead!) ------
  KS_CLOCK_SYSTEM_TIME                          = $0000000b;

type
  //------ Types & Structures ------
  KSClockSource = packed record
    structSize: int;
    flags: int;
    frequency: UInt64;
    offset: UInt64;
    baseClockId: uint;
  end;
  PKSClockSource = ^KSClockSource;

//------ Clock functions - Obsolete! Do not use! ------
function KS_calibrateMachineTime(pFrequency: PUInt; flags: int):
  Error; stdcall;
function KS_getSystemTicks(pSystemTicks: PUInt64):
  Error; stdcall;
function KS_getSystemTime(pSystemTime: PUInt64):
  Error; stdcall;
function KS_getMachineTime(pMachineTime: PUInt64):
  Error; stdcall;
function KS_getAbsTime(pAbsTime: PUInt64):
  Error; stdcall;

//------ Clock functions ------
function KS_getClock(pClock: PUInt64; clockId: int):
  Error; stdcall;
function KS_getClockSource(pClockSource: PKSClockSource; clockId: int):
  Error; stdcall;
function KS_setClockSource(pClockSource: PKSClockSource; clockId: int):
  Error; stdcall;
function KS_microDelay(delay: int):
  Error; stdcall;
function KS_convertClock(pValue: PUInt64; clockIdFrom: int; clockIdTo: int; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// System Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ System contexts ------
  SYSTEMHANDLER_CONTEXT                         = (USER_CONTEXT+$00000030);

  //------ System events ------
  KS_SYSTEM_CRASH                               = $00000001;
  KS_SYSTEM_SHUTDOWN                            = $00000002;
  KS_SYSTEM_PROCESS_ATTACH                      = $00000010;
  KS_SYSTEM_PROCESS_DETACH                      = $00000020;
  KS_SYSTEM_PROCESS_ABORT                       = $00000040;

type
  //------ Types & Structures ------
  SystemHandlerUserContext = packed record
    ctxType: int;
    eventType: int;
    handlerProcessId: int;
    currentProcessId: int;
  end;
  PSystemHandlerUserContext = ^SystemHandlerUserContext;

//------ System handling functions ------
function KS_installSystemHandler(eventMask: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_removeSystemHandler(eventMask: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Device Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_SERIAL_PORT                               = $00200000;

  //------ Function codes ------
  KS_DEVICE_CREATE                              = $00000000;
  KS_DEVICE_CLOSE                               = $00000001;
  KS_DEVICE_READ                                = $00000002;
  KS_DEVICE_WRITE                               = $00000003;
  KS_DEVICE_CONTROL                             = $00000004;
  KS_DEVICE_CLEANUP                             = $00000005;
  KS_DEVICE_CANCEL_REQUEST                      = $00010000;

  //------ Return codes ------
  KS_RETURN_SUCCESS                             = $00000000;
  KS_RETURN_UNSUCCESSFUL                        = $00000001;
  KS_RETURN_INVALID_DEVICE_REQUEST              = $00000002;
  KS_RETURN_INVALID_PARAMETER                   = $00000003;
  KS_RETURN_TIMEOUT                             = $00000004;
  KS_RETURN_CANCELLED                           = $00000005;
  KS_RETURN_PENDING                             = $00000006;
  KS_RETURN_DELETE_PENDING                      = $00000007;
  KS_RETURN_BUFFER_TOO_SMALL                    = $00000008;

type
  //------ Types & Structures ------
  DeviceUserContext = packed record
    ctxType: int;
    hRequest: KSHandle;
    functionCode: int;
    controlCode: int;
    pBuffer: PByte;
    size: uint;
    returnCode: int;
  end;
  PDeviceUserContext = ^DeviceUserContext;

  KSDeviceFunctions = packed record
    hOnCreate: KSHandle;
    hOnClose: KSHandle;
    hOnRead: KSHandle;
    hOnWrite: KSHandle;
    hOnControl: KSHandle;
    hOnCleanup: KSHandle;
  end;
  PKSDeviceFunctions = ^KSDeviceFunctions;

//------ Device functions ------
function KS_createDevice(phDevice: PKSHandle; name: PAnsiChar; pFunctions: PKSDeviceFunctions; flags: int):
  Error; stdcall;
function KS_closeDevice(hDevice: KSHandle):
  Error; stdcall;
function KS_resumeDevice(hDevice: KSHandle; hRequest: KSHandle; pBuffer: PByte; size: int; returnCode: int; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Keyboard Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_SIGNAL_MAKE                               = $00000001;
  KSF_SIGNAL_BREAK                              = $00000002;
  KSF_IGNORE_KEY                                = $00000010;
  KSF_MODIFY_KEY                                = $00000020;

  //------ Key mask modifier ------
  KSF_KEY_SHIFT                                 = $00000011;
  KSF_KEY_SHIFTR                                = $00000001;
  KSF_KEY_SHIFTL                                = $00000010;
  KSF_KEY_CTRL                                  = $00000022;
  KSF_KEY_CTRLR                                 = $00000002;
  KSF_KEY_CTRLL                                 = $00000020;
  KSF_KEY_ALT                                   = $00000044;
  KSF_KEY_ALTR                                  = $00000004;
  KSF_KEY_ALTL                                  = $00000040;
  KSF_KEY_WIN                                   = $00000088;
  KSF_KEY_WINR                                  = $00000008;
  KSF_KEY_WINL                                  = $00000080;

  //------ Key mask groups ------
  KSF_KEY_ALPHA_NUM                             = $00000100;
  KSF_KEY_NUM_PAD                               = $00000200;
  KSF_KEY_CONTROL                               = $00000400;
  KSF_KEY_FUNCTION                              = $00000800;

  //------ Key mask combinations ------
  KSF_KEY_CTRL_ALT_DEL                          = $00010000;
  KSF_KEY_CTRL_ESC                              = $00020000;
  KSF_KEY_ALT_ESC                               = $00040000;
  KSF_KEY_ALT_TAB                               = $00080000;
  KSF_KEY_ALT_ENTER                             = $00100000;
  KSF_KEY_ALT_PRINT                             = $00200000;
  KSF_KEY_ALT_SPACE                             = $00400000;
  KSF_KEY_ESC                                   = $00800000;
  KSF_KEY_TAB                                   = $01000000;
  KSF_KEY_ENTER                                 = $02000000;
  KSF_KEY_PRINT                                 = $04000000;
  KSF_KEY_SPACE                                 = $08000000;
  KSF_KEY_BACKSPACE                             = $10000000;
  KSF_KEY_PAUSE                                 = $20000000;
  KSF_KEY_APP                                   = $40000000;
  KSF_KEY_ALL                                   = $7fffffff;

  KSF_KEY_MODIFIER                              = KSF_KEY_CTRL or KSF_KEY_ALT or
                                                  KSF_KEY_SHIFT or KSF_KEY_WIN;

  KSF_KEY_SYSTEM                                = KSF_KEY_WIN or
                                                  KSF_KEY_CTRL_ALT_DEL or KSF_KEY_CTRL_ESC or
                                                  KSF_KEY_ALT_ESC or KSF_KEY_ALT_TAB;

  KSF_KEY_SYSTEM_DOS                            = KSF_KEY_SYSTEM or
                                                  KSF_KEY_ALT_ENTER or KSF_KEY_ALT_PRINT or
                                                  KSF_KEY_ALT_SPACE or KSF_KEY_PRINT;

  KSF_KEY_ALL_SINGLE                            = KSF_KEY_ALL and $ff80ffff;

  //------ Keyboard events ------
  KS_KEYBOARD_CONTEXT                           = $00000300;

type
  //------ Context structures ------
  KeyboardUserContext = packed record
    ctxType: int;
    mask: int;
    modifier: Word;
    port: Word;
    state: Word;
    key: Word;
  end;
  PKeyboardUserContext = ^KeyboardUserContext;

//------ Keyboard functions ------
function KS_createKeyHandler(mask: uint; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_removeKeyHandler(mask: uint):
  Error; stdcall;
function KS_simulateKeyStroke(pKeys: PUInt):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Interrupt Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_IRQ_ALREADY_USED                      = (KSERROR_CATEGORY_INTERRUPT+$00000000);
  KSERROR_CANNOT_INSTALL_HANDLER                = (KSERROR_CATEGORY_INTERRUPT+$00010000);
  KSERROR_BAD_IRQ_INSTALLATION                  = (KSERROR_CATEGORY_INTERRUPT+$00020000);
  KSERROR_NO_HANDLER_INSTALLED                  = (KSERROR_CATEGORY_INTERRUPT+$00030000);
  KSERROR_NOBODY_IS_WAITING                     = (KSERROR_CATEGORY_INTERRUPT+$00040000);
  KSERROR_IRQ_DISABLED                          = (KSERROR_CATEGORY_INTERRUPT+$00050000);
  KSERROR_IRQ_ENABLED                           = (KSERROR_CATEGORY_INTERRUPT+$00060000);
  KSERROR_IRQ_IN_SERVICE                        = (KSERROR_CATEGORY_INTERRUPT+$00070000);
  KSERROR_IRQ_HANDLER_REMOVED                   = (KSERROR_CATEGORY_INTERRUPT+$00080000);

  //------ Flags ------
  KSF_DONT_PASS_NEXT                            = $00020000;
  KSF_PCI_INTERRUPT                             = $00040000;

  //------ Interrupt events ------
  KS_INTERRUPT_CONTEXT                          = $00000100;

type
  //------ Types & Structures ------
  InterruptUserContext = packed record
    ctxType: int;
    hInterrupt: KSHandle;
    irq: uint;
    consumed: uint;
  end;
  PInterruptUserContext = ^InterruptUserContext;

//------ Interrupt handling functions ------
function KS_createDeviceInterrupt(phInterrupt: PKSHandle; pInfo: PKSResourceInfoEx; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_createDeviceInterruptEx(phInterrupt: PKSHandle; deviceName: PAnsiChar; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_createInterrupt(phInterrupt: PKSHandle; irq: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_removeInterrupt(hInterrupt: KSHandle):
  Error; stdcall;
function KS_disableInterrupt(hInterrupt: KSHandle):
  Error; stdcall;
function KS_enableInterrupt(hInterrupt: KSHandle):
  Error; stdcall;
function KS_simulateInterrupt(hInterrupt: KSHandle):
  Error; stdcall;
function KS_getInterruptState(hInterrupt: KSHandle; pState: PHandlerState):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// UART Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_CHECK_LINE_ERROR                          = $00008000;
  KSF_NO_FIFO                                   = $00001000;
  KSF_USE_FIFO                                  = $00040000;
  KSF_9_BIT_MODE                                = $00080000;
  KSF_TOGGLE_DTR                                = $00100000;
  KSF_TOGGLE_RTS                                = $00200000;
  KSF_TOGGLE_INVERSE                            = $00400000;

  //------ Uart commands ------
  KS_CLR_DTR                                    = $00000004;
  KS_SET_DTR                                    = $00000005;
  KS_CLR_RTS                                    = $00000006;
  KS_SET_RTS                                    = $00000007;
  KS_CLR_BREAK                                  = $00000008;
  KS_SET_BREAK                                  = $0000000a;
  KS_SKIP_NEXT                                  = $0000000b;
  KS_CHANGE_LINE_CONTROL                        = $0000000d;
  KS_SET_BAUD_RATE_MAX                          = $0000000e;
  KS_ENABLE_FIFO                                = $0000000f;
  KS_DISABLE_FIFO                               = $00000010;
  KS_ENABLE_INTERRUPTS                          = $00000011;
  KS_DISABLE_INTERRUPTS                         = $00000012;
  KS_SET_RECV_FIFO_SIZE                         = $00000013;

  //------ Line Errors ------
  KS_UART_ERROR_PARITY                          = $00000001;
  KS_UART_ERROR_FRAME                           = $00000002;
  KS_UART_ERROR_OVERRUN                         = $00000004;
  KS_UART_ERROR_BREAK                           = $00000008;
  KS_UART_ERROR_FIFO                            = $00000010;

  //------ UART events ------
  KS_UART_RECV                                  = $00000000;
  KS_UART_RECV_ERROR                            = $00000001;
  KS_UART_XMIT                                  = $00000002;
  KS_UART_XMIT_EMPTY                            = $00000003;
  KS_UART_LSR_CHANGE                            = $00000004;
  KS_UART_MSR_CHANGE                            = $00000005;
  KS_UART_BREAK                                 = $00000006;

type
  //------ Types & Structures ------
  UartUserContext = packed record
    ctxType: int;
    hUart: KSHandle;
    value: int;
    lineError: int;
  end;
  PUartUserContext = ^UartUserContext;

  UartRegisterUserContext = packed record
    ctxType: int;
    hUart: KSHandle;
    reg: byte;
  end;
  PUartRegisterUserContext = ^UartRegisterUserContext;

  KSUartProperties = packed record
    baudRate: int;
    parity: byte;
    dataBit: byte;
    stopBit: byte;
    reserved: byte;
  end;
  PKSUartProperties = ^KSUartProperties;

  KSUartState = packed record
    structSize: int;
    properties: KSUartProperties;
    modemState: byte;
    lineState: byte;
    reserved: Word;
    xmitCount: int;
    xmitFree: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    recvLostCount: int;
  end;
  PKSUartState = ^KSUartState;

//------ Common functions ------
function KS_openUart(phUart: PKSHandle; name: PAnsiChar; port: int; pProperties: PKSUartProperties; recvBufferLength: int; xmitBufferLength: int; flags: int):
  Error; stdcall;
function KS_openUartEx(phUart: PKSHandle; hConnection: KSHandle; port: int; pProperties: PKSUartProperties; recvBufferLength: int; xmitBufferLength: int; flags: int):
  Error; stdcall;
function KS_closeUart(hUart: KSHandle; flags: int):
  Error; stdcall;

//------ Communication functions ------
function KS_xmitUart(hUart: KSHandle; value: int; flags: int):
  Error; stdcall;
function KS_recvUart(hUart: KSHandle; pValue: PInt; pLineError: PInt; flags: int):
  Error; stdcall;
function KS_xmitUartBlock(hUart: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_recvUartBlock(hUart: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_installUartHandler(hUart: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_getUartState(hUart: KSHandle; pUartState: PKSUartState; flags: int):
  Error; stdcall;
function KS_execUartCommand(hUart: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// COMM Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Line Errors ------
  KS_COMM_ERROR_PARITY                          = $00000001;
  KS_COMM_ERROR_FRAME                           = $00000002;
  KS_COMM_ERROR_OVERRUN                         = $00000004;
  KS_COMM_ERROR_BREAK                           = $00000008;
  KS_COMM_ERROR_FIFO                            = $00000010;

  //------ COMM events ------
  KS_COMM_RECV                                  = $00000000;
  KS_COMM_RECV_ERROR                            = $00000001;
  KS_COMM_XMIT                                  = $00000002;
  KS_COMM_XMIT_EMPTY                            = $00000003;
  KS_COMM_LSR_CHANGE                            = $00000004;
  KS_COMM_MSR_CHANGE                            = $00000005;
  KS_COMM_BREAK                                 = $00000006;

type
  //------ Types & Structures ------
  CommUserContext = packed record
    ctxType: int;
    hComm: KSHandle;
    value: int;
    lineError: int;
  end;
  PCommUserContext = ^CommUserContext;

  CommRegisterUserContext = packed record
    ctxType: int;
    hComm: KSHandle;
    reg: byte;
  end;
  PCommRegisterUserContext = ^CommRegisterUserContext;

  KSCommProperties = packed record
    baudRate: int;
    parity: byte;
    dataBit: byte;
    stopBit: byte;
    reserved: byte;
  end;
  PKSCommProperties = ^KSCommProperties;

  KSCommState = packed record
    structSize: int;
    properties: KSCommProperties;
    modemState: byte;
    lineState: byte;
    reserved: Word;
    xmitCount: int;
    xmitFree: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    recvLostCount: int;
  end;
  PKSCommState = ^KSCommState;

//------ COMM functions ------
function KS_openComm(phComm: PKSHandle; name: PAnsiChar; pProperties: PKSCommProperties; recvBufferLength: int; xmitBufferLength: int; flags: int):
  Error; stdcall;
function KS_closeComm(hComm: KSHandle; flags: int):
  Error; stdcall;
function KS_xmitComm(hComm: KSHandle; value: int; flags: int):
  Error; stdcall;
function KS_recvComm(hComm: KSHandle; pValue: PInt; pLineError: PInt; flags: int):
  Error; stdcall;
function KS_xmitCommBlock(hComm: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_recvCommBlock(hComm: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_installCommHandler(hComm: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_getCommState(hComm: KSHandle; pCommState: PKSCommState; flags: int):
  Error; stdcall;
function KS_execCommCommand(hComm: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// USB Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_USB_REQUEST_FAILED                    = (KSERROR_CATEGORY_USB+$00000000);

  //------ Flags ------
  KSF_SOCKET_MODE                               = $00080000;

  //------ USB endpoint commands ------
  KS_USB_RESET_PORT                             = $00000004;
  KS_USB_SET_ISO_PACKET_SIZE                    = $00000005;
  KS_USB_GET_PACKET_SIZE                        = $00000007;
  KS_USB_SET_PACKET_TIMEOUT                     = $00000008;

  //------ USB device commands ------
  KS_USB_GET_DEVICE_DESCRIPTOR                  = $00020000;
  KS_USB_GET_CONFIG_DESCRIPTOR                  = $00030000;
  KS_USB_GET_STRING_DESCRIPTOR                  = $00040000;
  KS_USB_SELECT_CONFIG                          = $00050000;
  KS_USB_SELECT_INTERFACE                       = $00060000;
  KS_USB_GET_CONFIG                             = $00070000;
  KS_USB_GET_INTERFACE                          = $00080000;
  KS_USB_CLR_DEVICE_FEATURE                     = $00090000;
  KS_USB_SET_DEVICE_FEATURE                     = $000a0000;
  KS_USB_CLR_INTERFACE_FEATURE                  = $000b0000;
  KS_USB_SET_INTERFACE_FEATURE                  = $000c0000;
  KS_USB_CLR_ENDPOINT_FEATURE                   = $000d0000;
  KS_USB_SET_ENDPOINT_FEATURE                   = $000e0000;
  KS_USB_GET_STATUS_FROM_DEVICE                 = $000f0000;
  KS_USB_GET_STATUS_FROM_INTERFACE              = $00100000;
  KS_USB_GET_STATUS_FROM_ENDPOINT               = $00110000;
  KS_USB_VENDOR_DEVICE_REQUEST                  = $00120000;
  KS_USB_VENDOR_INTERFACE_REQUEST               = $00130000;
  KS_USB_VENDOR_ENDPOINT_REQUEST                = $00140000;
  KS_USB_CLASS_DEVICE_REQUEST                   = $00150000;
  KS_USB_CLASS_INTERFACE_REQUEST                = $00160000;
  KS_USB_CLASS_ENDPOINT_REQUEST                 = $00170000;

  //------ USB descriptor types ------
  KS_USB_DEVICE                                 = $00000001;
  KS_USB_CONFIGURATION                          = $00000002;
  KS_USB_STRING                                 = $00000003;
  KS_USB_INTERFACE                              = $00000004;
  KS_USB_ENDPOINT                               = $00000005;

  //------ USB end point types ------
  KS_USB_CONTROL                                = $00000000;
  KS_USB_ISOCHRONOUS                            = $00000001;
  KS_USB_BULK                                   = $00000002;
  KS_USB_INTERRUPT                              = $00000003;

  //------ USB port events ------
  KS_USB_SURPRISE_REMOVAL                       = (USB_BASE+$00000000);
  KS_USB_RECV                                   = (USB_BASE+$00000001);
  KS_USB_RECV_ERROR                             = (USB_BASE+$00000002);
  KS_USB_XMIT                                   = (USB_BASE+$00000003);
  KS_USB_XMIT_EMPTY                             = (USB_BASE+$00000004);
  KS_USB_XMIT_ERROR                             = (USB_BASE+$00000005);
  KS_USB_TIMEOUT                                = (USB_BASE+$00000006);

type
  //------ Types & Structures ------
  UsbUserContext = packed record
    ctxType: int;
    hUsb: KSHandle;
    error: int;
  end;
  PUsbUserContext = ^UsbUserContext;

  UsbClassOrVendorRequest = packed record
    request: int;
    value: int;
    index: int;
    pData: Pointer;
    size: int;
    write: int;
  end;
  PUsbClassOrVendorRequest = ^UsbClassOrVendorRequest;

  KS_USB_DEVICE_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bcdUSB: Word;
    bDeviceClass: byte;
    bDeviceSubClass: byte;
    bDeviceProtocol: byte;
    bMaxPacketSize0: byte;
    idVendor: Word;
    idProduct: Word;
    bcdDevice: Word;
    iManufacturer: byte;
    iProduct: byte;
    iSerialNumber: byte;
    bNumConfigurations: byte;
  end;
  PKS_USB_DEVICE_DESCRIPTOR = ^KS_USB_DEVICE_DESCRIPTOR;

  KS_USB_CONFIGURATION_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    wTotalLength: Word;
    bNumInterfaces: byte;
    bConfigurationValue: byte;
    iConfiguration: byte;
    bmAttributes: byte;
    MaxPower: byte;
  end;
  PKS_USB_CONFIGURATION_DESCRIPTOR = ^KS_USB_CONFIGURATION_DESCRIPTOR;

  KS_USB_INTERFACE_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bInterfaceNumber: byte;
    bAlternateSetting: byte;
    bNumEndpoints: byte;
    bInterfaceClass: byte;
    bInterfaceSubClass: byte;
    bInterfaceProtocol: byte;
    iInterface: byte;
  end;
  PKS_USB_INTERFACE_DESCRIPTOR = ^KS_USB_INTERFACE_DESCRIPTOR;

  KS_USB_ENDPOINT_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bEndpointAddress: byte;
    bmAttributes: byte;
    wMaxPacketSize: Word;
    bInterval: byte;
  end;
  PKS_USB_ENDPOINT_DESCRIPTOR = ^KS_USB_ENDPOINT_DESCRIPTOR;

  KS_USB_STRING_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bString: array [0..0] of Word;
  end;
  PKS_USB_STRING_DESCRIPTOR = ^KS_USB_STRING_DESCRIPTOR;

  KS_USB_COMMON_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
  end;
  PKS_USB_COMMON_DESCRIPTOR = ^KS_USB_COMMON_DESCRIPTOR;

  KS_USB_DEVICE_QUALIFIER_DESCRIPTOR = packed record
    bLength: byte;
    bDescriptorType: byte;
    bcdUSB: Word;
    bDeviceClass: byte;
    bDeviceSubClass: byte;
    bDeviceProtocol: byte;
    bMaxPacketSize0: byte;
    bNumConfigurations: byte;
    bReserved: byte;
  end;
  PKS_USB_DEVICE_QUALIFIER_DESCRIPTOR = ^KS_USB_DEVICE_QUALIFIER_DESCRIPTOR;

  KSUsbState = packed record
    structSize: int;
    xmitCount: int;
    xmitFree: int;
    xmitErrorCount: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    packetSize: int;
    isoPacketSize: int;
    packetTimeout: int;
  end;
  PKSUsbState = ^KSUsbState;

//------ Common functions ------
function KS_openUsb(phUsbDevice: PKSHandle; deviceName: PAnsiChar; flags: int):
  Error; stdcall;
function KS_openUsbEndPoint(hUsbDevice: KSHandle; phUsbEndPoint: PKSHandle; endPointAddress: int; packetSize: int; packetCount: int; flags: int):
  Error; stdcall;
function KS_closeUsb(hUsb: KSHandle; flags: int):
  Error; stdcall;

//------ Communication functions ------
function KS_xmitUsb(hUsb: KSHandle; pData: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_recvUsb(hUsb: KSHandle; pData: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_installUsbHandler(hUsb: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_getUsbState(hUsb: KSHandle; pUsbState: PKSUsbState; flags: int):
  Error; stdcall;
function KS_execUsbCommand(hUsb: KSHandle; command: int; index: int; pData: Pointer; size: int; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// XHCI Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_XHCI_REQUEST_FAILED                   = (KSERROR_CATEGORY_XHCI+$00000000);
  KSERROR_XHCI_HOST_CONTROLLER_ERROR            = (KSERROR_CATEGORY_XHCI+$00010000);
  KSERROR_XHCI_CONFIGURATION_ERROR              = (KSERROR_CATEGORY_XHCI+$00020000);
  KSERROR_XHCI_INTERFACE_ERROR                  = (KSERROR_CATEGORY_XHCI+$00030000);
  KSERROR_XHCI_PACKET_DROPPED                   = (KSERROR_CATEGORY_XHCI+$00040000);
  KSERROR_XHCI_PACKET_ERROR                     = (KSERROR_CATEGORY_XHCI+$00050000);

  //------ XHCI endpoint commands ------
  KS_XHCI_RESET_ENDPOINT                        = $00000001;
  KS_XHCI_GET_PACKET_SIZE                       = $00000002;
  KS_XHCI_CLEAR_RECEIVE_QUEUE                   = $00000003;

  //------ XHCI device commands ------
  KS_XHCI_GET_DEVICE_DESCRIPTOR                 = $00010001;
  KS_XHCI_GET_CONFIG_DESCRIPTOR                 = $00010002;
  KS_XHCI_GET_STRING_DESCRIPTOR                 = $00010003;
  KS_XHCI_SET_CONFIG                            = $00010004;
  KS_XHCI_GET_CONFIG                            = $00010005;
  KS_XHCI_SET_INTERFACE                         = $00010006;
  KS_XHCI_GET_INTERFACE                         = $00010007;

  //------ XHCI controller commands ------
  KS_XHCI_SET_POWER_ROOT_HUB                    = $00020001;

  //------ XHCI descriptor types ------
  KS_XHCI_DEVICE_DESCRIPTOR                     = $00000001;
  KS_XHCI_CONFIGURATION_DESCRIPTOR              = $00000002;
  KS_XHCI_STRING_DESCRIPTOR                     = $00000003;
  KS_XHCI_INTERFACE_DESCRIPTOR                  = $00000004;
  KS_XHCI_ENDPOINT_DESCRIPTOR                   = $00000005;
  KS_XHCI_INTERFACE_ASSOCIATION_DESCRIPTOR      = $0000000b;
  KS_XHCI_ENDPOINT_COMPANION_DESCRIPTOR         = $00000030;

  //------ XHCI endpoint types ------
  KS_XHCI_CONTROL_ENDPOINT                      = $00000000;
  KS_XHCI_ISOCHRONOUS_ENDPOINT                  = $00000001;
  KS_XHCI_BULK_ENDPOINT                         = $00000002;
  KS_XHCI_INTERRUPT_ENDPOINT                    = $00000003;

  //------ XHCI events ------
  KS_XHCI_ADD_DEVICE                            = (XHCI_BASE+$00000000);
  KS_XHCI_REMOVE_DEVICE                         = (XHCI_BASE+$00000001);
  KS_XHCI_RECV                                  = (XHCI_BASE+$00000002);
  KS_XHCI_RECV_ERROR                            = (XHCI_BASE+$00000003);
  KS_XHCI_XMIT_LOW                              = (XHCI_BASE+$00000004);
  KS_XHCI_XMIT_EMPTY                            = (XHCI_BASE+$00000005);
  KS_XHCI_XMIT_ERROR                            = (XHCI_BASE+$00000006);

type
  //------ Context structures ------
  KSXhciContext = packed record
    ctxType: int;
    hXhci: KSHandle;
  end;
  PKSXhciContext = ^KSXhciContext;

  KSXhciAddDeviceContext = packed record
    ctxType: int;
    hXhciController: KSHandle;
    deviceName: array [0..255] of AnsiChar;
  end;
  PKSXhciAddDeviceContext = ^KSXhciAddDeviceContext;

  KSXhciRecvContext = packed record
    ctxType: int;
    hXhciEndPoint: KSHandle;
    pXhciPacketApp: Pointer;
    pXhciPacketSys: Pointer;
    packetSize: int;
  end;
  PKSXhciRecvContext = ^KSXhciRecvContext;

  KSXhciErrorContext = packed record
    ctxType: int;
    hXhci: KSHandle;
    error: int;
  end;
  PKSXhciErrorContext = ^KSXhciErrorContext;

  //------ XHCI common structures ------
  KSXhciControlRequest = packed record
    requestType: int;
    request: int;
    value: int;
    index: int;
  end;
  PKSXhciControlRequest = ^KSXhciControlRequest;

  KSXhciEndPointState = packed record
    structSize: int;
    endPointType: int;
    packetSize: int;
    xmitCount: int;
    xmitFree: int;
    xmitErrorCount: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    recvDroppedCount: int;
  end;
  PKSXhciEndPointState = ^KSXhciEndPointState;

  //------ XHCI Descriptors ------
  KSXhciCommonDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
  end;
  PKSXhciCommonDescriptor = ^KSXhciCommonDescriptor;

  KSXhciDeviceDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bcdUSB: Word;
    bDeviceClass: byte;
    bDeviceSubClass: byte;
    bDeviceProtocol: byte;
    bMaxPacketSize0: byte;
    idVendor: Word;
    idProduct: Word;
    bcdDevice: Word;
    iManufacturer: byte;
    iProduct: byte;
    iSerialNumber: byte;
    bNumConfigurations: byte;
  end;
  PKSXhciDeviceDescriptor = ^KSXhciDeviceDescriptor;

  KSXhciConfigurationDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    wTotalLength: Word;
    bNumInterfaces: byte;
    bConfigurationValue: byte;
    iConfiguration: byte;
    bmAttributes: byte;
    MaxPower: byte;
  end;
  PKSXhciConfigurationDescriptor = ^KSXhciConfigurationDescriptor;

  KSXhciInterfaceDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bInterfaceNumber: byte;
    bAlternateSetting: byte;
    bNumEndpoints: byte;
    bInterfaceClass: byte;
    bInterfaceSubClass: byte;
    bInterfaceProtocol: byte;
    iInterface: byte;
  end;
  PKSXhciInterfaceDescriptor = ^KSXhciInterfaceDescriptor;

  KSXhciInterfaceAssociationDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bFirstInterface: byte;
    bNumInterfaces: byte;
    bFunctionClass: byte;
    bFunctionSubClass: byte;
    bFunctionProtocol: byte;
    iFunction: byte;
  end;
  PKSXhciInterfaceAssociationDescriptor = ^KSXhciInterfaceAssociationDescriptor;

  KSXhciEndpointDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bEndpointAddress: byte;
    bmAttributes: byte;
    wMaxPacketSize: Word;
    bInterval: byte;
  end;
  PKSXhciEndpointDescriptor = ^KSXhciEndpointDescriptor;

  KSXhciEndpointCompanionDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bMaxBurst: byte;
    bmAttributes: byte;
    wBytesPerInterval: Word;
  end;
  PKSXhciEndpointCompanionDescriptor = ^KSXhciEndpointCompanionDescriptor;

  KSXhciStringDescriptor = packed record
    bLength: byte;
    bDescriptorType: byte;
    bString: array [0..0] of Word;
  end;
  PKSXhciStringDescriptor = ^KSXhciStringDescriptor;

//------ Common functions ------
function KS_openXhciController(phXhciController: PKSHandle; deviceName: PAnsiChar; flags: int):
  Error; stdcall;
function KS_openXhciDevice(phXhciDevice: PKSHandle; deviceName: PAnsiChar; flags: int):
  Error; stdcall;
function KS_openXhciEndPoint(hXhciDevice: KSHandle; phXhciEndPoint: PKSHandle; endPointAddress: int; packetSize: int; packetCount: int; flags: int):
  Error; stdcall;
function KS_closeXhci(hXhci: KSHandle; flags: int):
  Error; stdcall;

//------ Communication functions ------
function KS_requestXhciPacket(hXhciEndPoint: KSHandle; ppXhciPacket: PPointer; flags: int):
  Error; stdcall;
function KS_releaseXhciPacket(hXhciEndPoint: KSHandle; pXhciPacket: Pointer; flags: int):
  Error; stdcall;
function KS_xmitXhciPacket(hXhciEndPoint: KSHandle; pXhciPacket: Pointer; size: int; flags: int):
  Error; stdcall;
function KS_recvXhciPacket(hXhciEndPoint: KSHandle; ppXhciPacket: PPointer; pSize: PInt; flags: int):
  Error; stdcall;
function KS_sendXhciControlRequest(hXhciDevice: KSHandle; pRequestData: PKSXhciControlRequest; pData: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_installXhciHandler(hXhci: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_getXhciEndPointState(hXhciEndPoint: KSHandle; pState: PKSXhciEndPointState; flags: int):
  Error; stdcall;
function KS_execXhciCommand(hXhci: KSHandle; command: int; index: int; pParam: Pointer; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// RealTime Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_TIMER_NOT_INSTALLED                   = (KSERROR_CATEGORY_TIMER+$00000000);
  KSERROR_TIMER_REMOVED                         = (KSERROR_CATEGORY_TIMER+$00010000);
  KSERROR_TIMER_DISABLED                        = (KSERROR_CATEGORY_TIMER+$00020000);
  KSERROR_TIMER_NOT_DISABLED                    = (KSERROR_CATEGORY_TIMER+$00030000);
  KSERROR_CANNOT_INSTALL_TIMER                  = (KSERROR_CATEGORY_TIMER+$00040000);
  KSERROR_NO_TIMER_AVAILABLE                    = (KSERROR_CATEGORY_TIMER+$00050000);
  KSERROR_WAIT_CANCELLED                        = (KSERROR_CATEGORY_TIMER+$00060000);
  KSERROR_TIMER_ALREADY_RUNNING                 = (KSERROR_CATEGORY_TIMER+$00070000);
  KSERROR_TIMER_ALREADY_STOPPED                 = (KSERROR_CATEGORY_TIMER+$00080000);
  KSERROR_CANNOT_START_TIMER                    = (KSERROR_CATEGORY_TIMER+$00090000);

  //------ Flags ------
  KSF_DISTINCT_MODE                             = $00040000;
  KSF_DRIFT_CORRECTION                          = $00010000;
  KSF_DISABLE_PROTECTION                        = $00020000;
  KSF_USE_IEEE1588_TIME                         = $00040000;

  //------ Timer module config ------
  KSCONFIG_SOFTWARE_TIMER                       = $00000001;

  //------ Timer events ------
  KS_TIMER_CONTEXT                              = $00000200;

type
  //------ Context structures ------
  TimerUserContext = packed record
    ctxType: int;
    hTimer: KSHandle;
    delay: int;
  end;
  PTimerUserContext = ^TimerUserContext;

//------ Real-time functions ------
function KS_createTimer(phTimer: PKSHandle; delay: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_removeTimer(hTimer: KSHandle):
  Error; stdcall;
function KS_cancelTimer(hTimer: KSHandle):
  Error; stdcall;
function KS_startTimer(hTimer: KSHandle; flags: int; delay: int):
  Error; stdcall;
function KS_stopTimer(hTimer: KSHandle):
  Error; stdcall;
function KS_startTimerDelayed(hTimer: KSHandle; var start: Int64; period: int; flags: int):
  Error; stdcall;
function KS_adjustTimer(hTimer: KSHandle; period: int; flags: int):
  Error; stdcall;

//------ Obsolete timer functions - do not use! ------
function KS_disableTimer(hTimer: KSHandle):
  Error; stdcall;
function KS_enableTimer(hTimer: KSHandle):
  Error; stdcall;
function KS_getTimerState(hTimer: KSHandle; pState: PHandlerState):
  Error; stdcall;

//------ Obsolete timer resolution functions - do not use! ------
function KS_setTimerResolution(resolution: uint; flags: int):
  Error; stdcall;
function KS_getTimerResolution(pResolution: PUInt; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Task Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_CPU_BOOT_FAILURE                      = (KSERROR_CATEGORY_TASK+$00000000);

  //------ Task states ------
  KS_TASK_DORMANT                               = $00000001;
  KS_TASK_READY                                 = $00000002;
  KS_TASK_RUNNING                               = $00000004;
  KS_TASK_WAITING                               = $00000008;
  KS_TASK_SUSPENDED                             = $00000010;
  KS_TASK_TERMINATED                            = $00000020;

  //------ Flags ------
  KSF_NUM_PROCESSORS                            = $00000040;
  KSF_NON_INTERRUPTABLE                         = $00040000;
  KSF_CUSTOM_STACK_SIZE                         = $00004000;

  //------ Task contexts ------
  TASK_CONTEXT                                  = (TASK_BASE+$00000000);

type
  //------ Common structure ------
  KSContextInformation = packed record
    structSize: int;
    currentLevel: int;
    taskHandle: KSHandle;
    taskPriority: int;
    currentCpuNumber: int;
    stackSize: int;
    stackUsage: int;
  end;
  PKSContextInformation = ^KSContextInformation;

  //------ Context structure ------
  TaskUserContext = packed record
    ctxType: int;
    hTask: KSHandle;
    priority: int;
  end;
  PTaskUserContext = ^TaskUserContext;

  TaskErrorUserContext = packed record
    ctxType: int;
    error: Error;
    param1: Pointer;
    param2: Pointer;
    param3: Pointer;
    param4: Pointer;
    hTask: KSHandle;
    kernel: array [0..63] of AnsiChar;
    functionName: array [0..63] of AnsiChar;
    functionOffset: int;
  end;
  PTaskErrorUserContext = ^TaskErrorUserContext;

//------ Task functions ------
function KS_createTask(phTask: PKSHandle; hCallBack: KSHandle; priority: int; flags: int):
  Error; stdcall;
function KS_removeTask(hTask: KSHandle):
  Error; stdcall;
function KS_exitTask(hTask: KSHandle; exitCode: int):
  Error; stdcall;
function KS_killTask(hTask: KSHandle; exitCode: int):
  Error; stdcall;
function KS_terminateTask(hTask: KSHandle; timeout: int; flags: int):
  Error; stdcall;
function KS_suspendTask(hTask: KSHandle):
  Error; stdcall;
function KS_resumeTask(hTask: KSHandle):
  Error; stdcall;
function KS_triggerTask(hTask: KSHandle):
  Error; stdcall;
function KS_yieldTask:
  Error; stdcall;
function KS_sleepTask(delay: int):
  Error; stdcall;
function KS_setTaskPrio(hTask: KSHandle; newPrio: int; pOldPrio: PInt):
  Error; stdcall;
function KS_setTaskPriority(hObject: KSHandle; priority: int; flags: int):
  Error; stdcall;
function KS_setTaskPriorityRelative(hObject: KSHandle; hRelative: KSHandle; distance: int; flags: int):
  Error; stdcall;
function KS_getTaskPriority(hObject: KSHandle; pPriority: PInt; flags: int):
  Error; stdcall;
function KS_getTaskState(hTask: KSHandle; pTaskState: PUInt; pExitCode: PInt):
  Error; stdcall;
function KS_setTaskStackSize(size: int; flags: int):
  Error; stdcall;

//------ MultiCore & information functions ------
function KS_setTargetProcessor(processor: int; flags: int):
  Error; stdcall;
function KS_getCurrentProcessor(pProcessor: PInt; flags: int):
  Error; stdcall;
function KS_getCurrentContext(pContextInfo: PKSContextInformation; flags: int):
  Error; stdcall;

//------ Semaphore functions ------
function KS_createSemaphore(phSemaphore: PKSHandle; maxCount: int; initCount: int; flags: int):
  Error; stdcall;
function KS_removeSemaphore(hSemaphore: KSHandle):
  Error; stdcall;
function KS_requestSemaphore(hSemaphore: KSHandle; timeout: int):
  Error; stdcall;
function KS_releaseSemaphore(hSemaphore: KSHandle):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Dedicated Module
//--------------------------------------------------------------------------------------------------------------

//------ Dedicated functions ------

//--------------------------------------------------------------------------------------------------------------
// Packet Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_INIT_PHY_ERROR                        = (KSERROR_CATEGORY_PACKET+$00000000);
  KSERROR_INIT_MAC_ERROR                        = (KSERROR_CATEGORY_PACKET+$00010000);
  KSERROR_MAC_STARTUP_ERROR                     = (KSERROR_CATEGORY_PACKET+$00020000);

  //------ Flags ------
  KSF_SUPPORT_JUMBO_FRAMES                      = $00008000;
  KSF_DELAY_RECEIVE_INTERRUPT                   = $00080000;
  KSF_NO_RECEIVE_INTERRUPT                      = $00000020;
  KSF_NO_TRANSMIT_INTERRUPT                     = $00000040;
  KSF_RAW_ETHERNET_PACKET                       = $00100000;
  KSF_SAFE_START                                = $00200000;
  KSF_CHECK_IP_CHECKSUM                         = $00400000;
  KSF_ACCEPT_ALL                                = $00800000;
  KSF_FORCE_MASTER                              = $00000001;
  KSF_FORCE_100_MBIT                            = $00000002;
  KSF_WINDOWS_CONNECTION                        = $00004000;

  //------ Packet events ------
  KS_PACKET_RECV                                = (PACKET_BASE+$00000000);
  KS_PACKET_LINK_CHANGE                         = (PACKET_BASE+$00000001);
  KS_PACKET_SEND_EMPTY                          = (PACKET_BASE+$00000002);
  KS_PACKET_SEND_LOW                            = (PACKET_BASE+$00000003);

  //------ Adapter commands ------
  KS_PACKET_ABORT_SEND                          = $00000001;
  KS_PACKET_ADD_ARP_TABLE_ENTRY                 = $00000002;
  KS_PACKET_SET_IP_CONFIG                       = $00000003;
  KS_PACKET_SET_MAC_MULTICAST                   = $00000004;
  KS_PACKET_SET_IP_MULTICAST                    = $00000005;
  KS_PACKET_SET_MAC_CONFIG                      = $00000006;
  KS_PACKET_CLEAR_RECEIVE_QUEUE                 = $00000007;
  KS_PACKET_GET_IP_CONFIG                       = $00000008;
  KS_PACKET_GET_MAC_CONFIG                      = $00000009;
  KS_PACKET_GET_MAXIMUM_SIZE                    = $0000000a;

  //------ Connection flags ------
  KS_PACKET_SPEED_10_MBIT                       = $00010000;
  KS_PACKET_SPEED_100_MBIT                      = $00020000;
  KS_PACKET_SPEED_1000_MBIT                     = $00040000;
  KS_PACKET_SPEED_10_GBIT                       = $00080000;
  KS_PACKET_DUPLEX_FULL                         = $00000001;
  KS_PACKET_DUPLEX_HALF                         = $00000002;
  KS_PACKET_MASTER                              = $00000004;
  KS_PACKET_SLAVE                               = $00000008;

type
  //------ Types & Structures ------
  PacketUserContext = packed record
    ctxType: int;
    hAdapter: KSHandle;
    pPacketApp: Pointer;
    pPacketSys: Pointer;
    length: int;
  end;
  PPacketUserContext = ^PacketUserContext;

  PacketAdapterUserContext = packed record
    ctxType: int;
    hAdapter: KSHandle;
  end;
  PPacketAdapterUserContext = ^PacketAdapterUserContext;

  PacketLinkChangeUserContext = packed record
    ctxType: int;
    hAdapter: KSHandle;
    connection: uint;
  end;
  PPacketLinkChangeUserContext = ^PacketLinkChangeUserContext;

  KSMACHeader = packed record
    destination: array [0..5] of byte;
    source: array [0..5] of byte;
    typeOrLength: Word;
  end;
  PKSMACHeader = ^KSMACHeader;

  KSIPHeader = packed record
    header1: uint;
    header2: uint;
    header3: uint;
    srcIpAddress: uint;
    dstIpAddress: uint;
    header6: uint;
  end;
  PKSIPHeader = ^KSIPHeader;

  KSUDPHeader = packed record
    srcPort: Word;
    dstPort: Word;
    msgLength: Word;
    checkSum: Word;
  end;
  PKSUDPHeader = ^KSUDPHeader;

  KSIPConfig = packed record
    localAddress: uint;
    subnetMask: uint;
    gatewayAddress: uint;
  end;
  PKSIPConfig = ^KSIPConfig;

  KSARPTableEntry = packed record
    ipAddress: uint;
    macAddress: array [0..5] of byte;
  end;
  PKSARPTableEntry = ^KSARPTableEntry;

  KSAdapterState = packed record
    structSize: int;
    numPacketsSendOk: int;
    numPacketsSendError: int;
    numPacketsSendARPTimeout: int;
    numPacketsWaitingForARP: int;
    numPacketsRecvOk: int;
    numPacketsRecvError: int;
    numPacketsRecvDropped: int;
    connection: uint;
    macAddress: array [0..5] of byte;
    connected: byte;
  end;
  PKSAdapterState = ^KSAdapterState;

//------ Helper functions ------
function mk16(lo, hi : byte):
  word;
function mk32(lo, hi : word):
  uint;
function KS_htons(val : word):
  word;
function KS_htonl(val : uint):
  uint;
function KS_ntohs(val : word):
  word;
function KS_ntohl(val : uint):
  uint;
procedure KS_makeIPv4(pAddr : PUint; b3, b2, b1, b0 : byte);

//------ Common functions ------
function KS_openAdapter(phAdapter: PKSHandle; name: PAnsiChar; recvPoolLength: int; sendPoolLength: int; flags: int):
  Error; stdcall;
function KS_openAdapterEx(phAdapter: PKSHandle; hConnection: KSHandle; recvPoolLength: int; sendPoolLength: int; flags: int):
  Error; stdcall;
function KS_closeAdapter(hAdapter: KSHandle):
  Error; stdcall;
function KS_getAdapterState(hAdapter: KSHandle; pAdapterState: PKSAdapterState; flags: int):
  Error; stdcall;
function KS_execAdapterCommand(hAdapter: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;
function KS_aggregateAdapter(hAdapter: KSHandle; hLink: KSHandle; flags: int):
  Error; stdcall;

//------ Communication functions ------
function KS_requestPacket(hAdapter: KSHandle; ppPacket: PPointer; flags: int):
  Error; stdcall;
function KS_releasePacket(hAdapter: KSHandle; pPacket: Pointer; flags: int):
  Error; stdcall;
function KS_sendPacket(hAdapter: KSHandle; pPacket: Pointer; size: uint; flags: int):
  Error; stdcall;
function KS_recvPacket(hAdapter: KSHandle; ppPacket: PPointer; flags: int):
  Error; stdcall;
function KS_recvPacketEx(hAdapter: KSHandle; ppPacket: PPointer; pLength: PInt; flags: int):
  Error; stdcall;
function KS_installPacketHandler(hAdapter: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Socket Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_CLIENT                                    = $00000020;
  KSF_SERVER                                    = $00000040;
  KSF_ACCEPT_BROADCAST                          = $00800000;
  KSF_INSERT_CHECKSUM                           = $00200000;
  KSF_CHECK_CHECKSUM                            = $00400000;

  //------ Family types ------
  KS_FAMILY_IPV4                                = $00000004;

  //------ Protocol types ------
  KS_PROTOCOL_RAW                               = $00000000;
  KS_PROTOCOL_UDP                               = $00000001;
  KS_PROTOCOL_TCP                               = $00000002;

  //------ Socket events ------
  KS_SOCKET_CONNECTED                           = (SOCKET_BASE+$00000000);
  KS_SOCKET_DISCONNECTED                        = (SOCKET_BASE+$00000001);
  KS_SOCKET_RECV                                = (SOCKET_BASE+$00000002);
  KS_SOCKET_CLEAR_TO_SEND                       = (SOCKET_BASE+$00000003);
  KS_SOCKET_SEND_EMPTY                          = (SOCKET_BASE+$00000004);
  KS_SOCKET_SEND_LOW                            = (SOCKET_BASE+$00000005);

  //------ Socket commands ------
  KS_SOCKET_SET_MTU                             = $00000001;

type
  //------ Types & Structures ------
  KSSocketAddr = packed record
    family: Word;
    port: Word;
    address: array [0..0] of uint;
  end;
  PKSSocketAddr = ^KSSocketAddr;

  SocketUserContext = packed record
    ctxType: int;
    hSocket: KSHandle;
  end;
  PSocketUserContext = ^SocketUserContext;

//------ Socket functions ------
function KS_openSocket(phSocket: PKSHandle; hAdapter: KSHandle; pSocketAddr: PKSSocketAddr; protocol: int; flags: int):
  Error; stdcall;
function KS_closeSocket(hSocket: KSHandle):
  Error; stdcall;
function KS_recvFromSocket(hSocket: KSHandle; pSourceAddr: PKSSocketAddr; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_sendToSocket(hSocket: KSHandle; pTargetAddr: PKSSocketAddr; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_recvSocket(hSocket: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_sendSocket(hSocket: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_installSocketHandler(hSocket: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_connectSocket(hClient: KSHandle; pServerAddr: PKSSocketAddr; flags: int):
  Error; stdcall;
function KS_acceptSocket(hServer: KSHandle; pClientAddr: PKSSocketAddr; flags: int):
  Error; stdcall;
function KS_execSocketCommand(hSocket: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// EtherCAT Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_EMERGENCY_REQUEST                     = (KSERROR_CATEGORY_ETHERCAT+$00000000);
  KSERROR_LINK_CHANGE                           = (KSERROR_CATEGORY_ETHERCAT+$00010000);
  KSERROR_TOPOLOGY_CHANGE                       = (KSERROR_CATEGORY_ETHERCAT+$00020000);
  KSERROR_SLAVE_ERROR                           = (KSERROR_CATEGORY_ETHERCAT+$00030000);
  KSERROR_DATA_INCOMPLETE                       = (KSERROR_CATEGORY_ETHERCAT+$00040000);
  KSERROR_PHYSICAL_ACCESS_FAILED                = (KSERROR_CATEGORY_ETHERCAT+$00050000);
  KSERROR_FMMU_ERROR                            = (KSERROR_CATEGORY_ETHERCAT+$00060000);
  KSERROR_DATA_LINK_ERROR                       = (KSERROR_CATEGORY_ETHERCAT+$00070000);
  KSERROR_SII_ERROR                             = (KSERROR_CATEGORY_ETHERCAT+$00080000);
  KSERROR_MAILBOX_ERROR                         = (KSERROR_CATEGORY_ETHERCAT+$00090000);
  KSERROR_APPLICATION_LAYER_ERROR               = (KSERROR_CATEGORY_ETHERCAT+$000a0000);
  KSERROR_SYNC_MANAGER_ERROR                    = (KSERROR_CATEGORY_ETHERCAT+$000b0000);
  KSERROR_DC_CONTROL_ERROR                      = (KSERROR_CATEGORY_ETHERCAT+$000c0000);
  KSERROR_SOE_REQUEST_ERROR                     = (KSERROR_CATEGORY_ETHERCAT+$000e0000);
  KSERROR_MISSING_XML_INFORMATION               = (KSERROR_CATEGORY_ETHERCAT+$000f0000);
  KSERROR_SAFETY_ERROR                          = (KSERROR_CATEGORY_ETHERCAT+$00100000);
  KSERROR_DC_CONFIGURATION_ERROR                = (KSERROR_CATEGORY_ETHERCAT+$00110000);
  KSERROR_COE_ERROR                             = (KSERROR_CATEGORY_ETHERCAT+$00120000);

  //------ Flags ------
  KSF_FORCE_UPDATE                              = $00000008;
  KSF_SECONDARY                                 = $00000020;
  KSF_SYNC_DC_GLOBAL                            = $00000040;
  KSF_SYNC_DC_LOCAL                             = $00800000;
  KSF_BIG_DATASET                               = $00004000;
  KSF_STARTUP_WITHOUT_INIT_STATE                = $00008000;
  KSF_COMPLETE_ACCESS                           = $00200000;
  KSF_IGNORE_INIT_CMDS                          = $01000000;
  KSF_STRICT_XML_CHECK                          = $00000040;

  //------ DataObj type flags ------
  KS_DATAOBJ_PDO_TYPE                           = $00000001;
  KS_DATAOBJ_SDO_TYPE                           = $00000002;
  KS_DATAOBJ_IDN_TYPE                           = $00000003;
  KS_DATAOBJ_ACTIVE                             = $00040000;
  KS_DATAOBJ_MAPPABLE                           = $00080000;
  KS_DATAOBJ_SAFETY                             = $00100000;

  //------ State type ------
  KS_ECAT_STATE_INIT                            = $00000001;
  KS_ECAT_STATE_PREOP                           = $00000002;
  KS_ECAT_STATE_BOOT                            = $00000003;
  KS_ECAT_STATE_SAFEOP                          = $00000004;
  KS_ECAT_STATE_OP                              = $00000008;

  //------ State type (legacy, do not use anymore) ------
  KS_STATE_INIT                                 = $00000001;
  KS_STATE_PREOP                                = $00000002;
  KS_STATE_BOOT                                 = $00000003;
  KS_STATE_SAFEOP                               = $00000004;
  KS_STATE_OP                                   = $00000008;

  //------ Sync manager index commons ------
  KS_ECAT_SYNC_ALL                              = Integer($ffffffff);
  KS_ECAT_SYNC_INPUT                            = Integer($fffffffe);
  KS_ECAT_SYNC_OUTPUT                           = Integer($fffffffd);

  //------ EtherCAT events (legacy, do not use anymore) ------
  KS_ETHERCAT_ERROR                             = (ETHERCAT_BASE+$00000000);
  KS_DATASET_SIGNAL                             = (ETHERCAT_BASE+$00000001);
  KS_TOPOLOGY_CHANGE                            = (ETHERCAT_BASE+$00000002);

  //------ EtherCAT events ------
  KS_ECAT_ERROR                                 = (ETHERCAT_BASE+$00000000);
  KS_ECAT_DATASET_SIGNAL                        = (ETHERCAT_BASE+$00000001);
  KS_ECAT_TOPOLOGY_CHANGE                       = (ETHERCAT_BASE+$00000002);
  KS_ECAT_STATE_CHANGE                          = (ETHERCAT_BASE+$00000003);
  KS_ECAT_OBJECT_ACCESS                         = (ETHERCAT_BASE+$00000004);
  KS_ECAT_FILE_ACCESS                           = (ETHERCAT_BASE+$00000005);
  KS_ECAT_PROCESS_DATA_ACCESS                   = (ETHERCAT_BASE+$00000006);
  KS_ECAT_REDUNDANCY_EFFECTIVE                  = (ETHERCAT_BASE+$00000007);

  //------ Topology change reasons ------
  KS_ECAT_TOPOLOGY_MASTER_CONNECTED             = $00000000;
  KS_ECAT_TOPOLOGY_MASTER_DISCONNECTED          = $00000001;
  KS_ECAT_TOPOLOGY_SLAVE_COUNT_CHANGED          = $00000002;
  KS_ECAT_TOPOLOGY_SLAVE_ONLINE                 = $00000003;
  KS_ECAT_TOPOLOGY_SLAVE_OFFLINE                = $00000004;

  //------ DataSet commands ------
  KS_ECAT_GET_BUFFER_SIZE                       = $00000001;

  //------ Slave commands ------
  KS_ECAT_READ_SYNC0_STATUS                     = $00000001;
  KS_ECAT_READ_SYNC1_STATUS                     = $00000002;
  KS_ECAT_READ_LATCH0_STATUS                    = $00000003;
  KS_ECAT_READ_LATCH1_STATUS                    = $00000004;
  KS_ECAT_READ_LATCH_TIMESTAMPS                 = $00000005;
  KS_ECAT_ENABLE_CYCLIC_MAILBOX_CHECK           = $00000006;
  KS_ECAT_GET_PDO_OVERSAMPLING_FACTOR           = $00000007;
  KS_ECAT_SET_SYNCMANAGER_WATCHDOGS             = $00000008;
  KS_ECAT_GET_SYNCMANAGER_WATCHDOGS             = $00000009;
  KS_ECAT_HAS_XML_INFORMATION                   = $0000000a;

  //------ Slave Device / EAP-Node commands ------
  KS_ECAT_SIGNAL_AL_ERROR                       = $00000001;

type
  //------ EtherCAT data structures ------
  KSEcatMasterState = packed record
    structSize: int;
    connected: int;
    slavesOnline: int;
    slavesCreated: int;
    slavesCreatedAndOnline: int;
    masterState: int;
    lastError: int;
    redundancyEffective: int;
    framesSent: int;
    framesLost: int;
  end;
  PKSEcatMasterState = ^KSEcatMasterState;

  KSEcatSlaveState = packed record
    structSize: int;
    online: int;
    created: int;
    vendor: int;
    product: int;
    revision: int;
    id: int;
    relPosition: int;
    absPosition: int;
    linkState: int;
    slaveState: int;
    statusCode: int;
  end;
  PKSEcatSlaveState = ^KSEcatSlaveState;

  KSEcatDataVarInfo = packed record
    objType: int;
    name: PAnsiChar;
    dataType: int;
    bitLength: int;
    subIndex: int;
  end;
  PKSEcatDataVarInfo = ^KSEcatDataVarInfo;

  PPKSEcatDataVarInfo = ^PKSEcatDataVarInfo;

  KSEcatDataObjInfo = packed record
    objType: int;
    name: PAnsiChar;
    bitLength: int;
    index: int;
    syncIndex: int;
    varCount: int;
    vars: array [0..0] of PKSEcatDataVarInfo;
  end;
  PKSEcatDataObjInfo = ^KSEcatDataObjInfo;

  PPKSEcatDataObjInfo = ^PKSEcatDataObjInfo;

  KSEcatSlaveInfo = packed record
    vendorId: int;
    productId: int;
    revision: int;
    serial: int;
    group: PAnsiChar;
    image: PAnsiChar;
    order: PAnsiChar;
    name: PAnsiChar;
    objCount: int;
    objs: array [0..0] of PKSEcatDataObjInfo;
  end;
  PKSEcatSlaveInfo = ^KSEcatSlaveInfo;

  PPKSEcatSlaveInfo = ^PKSEcatSlaveInfo;

  KSEcatDcParams = packed record
    assignActivate: Word;
    latch0Control: byte;
    latch1Control: byte;
    pulseLength: int;
    cycleTimeSync0: int;
    cycleTimeSync0Factor: int;
    shiftTimeSync0: int;
    shiftTimeSync0Factor: int;
    cycleTimeSync1: int;
    cycleTimeSync1Factor: int;
    shiftTimeSync1: int;
    shiftTimeSync1Factor: int;
  end;
  PKSEcatDcParams = ^KSEcatDcParams;

  KSEcatDcLatchTimeStamps = packed record
    latch0PositivEdge: Int64;
    latch0NegativEdge: Int64;
    latch1PositivEdge: Int64;
    latch1NegativEdge: Int64;
    smBufferChange: Int64;
    smPdiBufferStart: Int64;
    smPdiBufferChange: Int64;
  end;
  PKSEcatDcLatchTimeStamps = ^KSEcatDcLatchTimeStamps;

  KSEcatEapNodeState = packed record
    structSize: int;
    connected: int;
    proxiesOnline: int;
    proxiesCreated: int;
    proxiesCreatedAndOnline: int;
    nodeState: int;
    statusCode: int;
  end;
  PKSEcatEapNodeState = ^KSEcatEapNodeState;

  KSEcatEapProxyState = packed record
    structSize: int;
    online: int;
    created: int;
    ip: uint;
    proxyState: int;
    statusCode: int;
  end;
  PKSEcatEapProxyState = ^KSEcatEapProxyState;

  KSEcatEapNodeId = packed record
    structSize: int;
    ip: uint;
  end;
  PKSEcatEapNodeId = ^KSEcatEapNodeId;

  KSEcatSlaveDeviceState = packed record
    structSize: int;
    slaveState: int;
    statusCode: int;
  end;
  PKSEcatSlaveDeviceState = ^KSEcatSlaveDeviceState;

  //------ Context structures ------
  EcatErrorUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    error: int;
  end;
  PEcatErrorUserContext = ^EcatErrorUserContext;

  EcatDataSetUserContext = packed record
    ctxType: int;
    hDataSet: KSHandle;
  end;
  PEcatDataSetUserContext = ^EcatDataSetUserContext;

  EcatTopologyUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    slavesOnline: int;
    slavesCreated: int;
    slavesCreatedAndOnline: int;
    reason: int;
    hSlave: KSHandle;
  end;
  PEcatTopologyUserContext = ^EcatTopologyUserContext;

  EcatRedundancyUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    redundancyEffective: int;
  end;
  PEcatRedundancyUserContext = ^EcatRedundancyUserContext;

  EcatStateChangeUserContext = packed record
    ctxType: int;
    hObject: KSHandle;
    currentState: int;
    requestedState: int;
  end;
  PEcatStateChangeUserContext = ^EcatStateChangeUserContext;

  EcatObjectAccessUserContext = packed record
    ctxType: int;
    hObject: KSHandle;
    index: int;
    subIndex: int;
    writeAccess: int;
    completeAccess: int;
  end;
  PEcatObjectAccessUserContext = ^EcatObjectAccessUserContext;

  EcatFileAccessUserContext = packed record
    ctxType: int;
    hObject: KSHandle;
    fileName: array [0..255] of AnsiChar;
    password: int;
    writeAccess: int;
  end;
  PEcatFileAccessUserContext = ^EcatFileAccessUserContext;

  EcatProcessDataAccessUserContext = packed record
    ctxType: int;
    hObject: KSHandle;
  end;
  PEcatProcessDataAccessUserContext = ^EcatProcessDataAccessUserContext;

//------ Master functions ------
function KS_createEcatMaster(phMaster: PKSHandle; hConnection: KSHandle; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int):
  Error; stdcall;
function KS_closeEcatMaster(hMaster: KSHandle):
  Error; stdcall;
function KS_installEcatMasterHandler(hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_queryEcatMasterState(hMaster: KSHandle; pMasterState: PKSEcatMasterState; flags: int):
  Error; stdcall;
function KS_changeEcatMasterState(hMaster: KSHandle; state: int; flags: int):
  Error; stdcall;
function KS_addEcatRedundancy(hMaster: KSHandle; hConnection: KSHandle; flags: int):
  Error; stdcall;

//------ Slave functions ------
function KS_createEcatSlave(hMaster: KSHandle; phSlave: PKSHandle; id: int; position: int; vendor: int; product: int; revision: int; flags: int):
  Error; stdcall;
function KS_createEcatSlaveIndirect(hMaster: KSHandle; phSlave: PKSHandle; pSlaveState: PKSEcatSlaveState; flags: int):
  Error; stdcall;
function KS_enumEcatSlaves(hMaster: KSHandle; index: int; pSlaveState: PKSEcatSlaveState; flags: int):
  Error; stdcall;
function KS_deleteEcatSlave(hSlave: KSHandle):
  Error; stdcall;
function KS_writeEcatSlaveId(hSlave: KSHandle; id: int; flags: int):
  Error; stdcall;
function KS_queryEcatSlaveState(hSlave: KSHandle; pSlaveState: PKSEcatSlaveState; flags: int):
  Error; stdcall;
function KS_changeEcatSlaveState(hSlave: KSHandle; state: int; flags: int):
  Error; stdcall;
function KS_queryEcatSlaveInfo(hSlave: KSHandle; ppSlaveInfo: PPKSEcatSlaveInfo; flags: int):
  Error; stdcall;
function KS_queryEcatDataObjInfo(hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSEcatDataObjInfo; flags: int):
  Error; stdcall;
function KS_queryEcatDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSEcatDataVarInfo; flags: int):
  Error; stdcall;
function KS_enumEcatDataObjInfo(hSlave: KSHandle; objEnumIndex: int; ppDataObjInfo: PPKSEcatDataObjInfo; flags: int):
  Error; stdcall;
function KS_enumEcatDataVarInfo(hSlave: KSHandle; objEnumIndex: int; varEnumIndex: int; ppDataVarInfo: PPKSEcatDataVarInfo; flags: int):
  Error; stdcall;
function KS_loadEcatInitCommands(hSlave: KSHandle; filename: PAnsiChar; flags: int):
  Error; stdcall;

//------ DataSet related functions ------
function KS_createEcatDataSet(hObject: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int):
  Error; stdcall;
function KS_deleteEcatDataSet(hSet: KSHandle):
  Error; stdcall;
function KS_assignEcatDataSet(hSet: KSHandle; hObject: KSHandle; syncIndex: int; placement: int; flags: int):
  Error; stdcall;
function KS_readEcatDataSet(hSet: KSHandle; flags: int):
  Error; stdcall;
function KS_postEcatDataSet(hSet: KSHandle; flags: int):
  Error; stdcall;
function KS_installEcatDataSetHandler(hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;

//------ Common functions ------
function KS_changeEcatState(hObject: KSHandle; state: int; flags: int):
  Error; stdcall;
function KS_installEcatHandler(hObject: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;

//------ Data exchange functions ------
function KS_getEcatDataObjAddress(hSet: KSHandle; hObject: KSHandle; pdoIndex: int; varIndex: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int):
  Error; stdcall;
function KS_readEcatDataObj(hObject: KSHandle; objIndex: int; varIndex: int; pData: Pointer; pSize: PInt; flags: int):
  Error; stdcall;
function KS_postEcatDataObj(hObject: KSHandle; objIndex: int; varIndex: int; pData: Pointer; size: int; flags: int):
  Error; stdcall;
function KS_setEcatPdoAssign(hObject: KSHandle; syncIndex: int; pdoIndex: int; flags: int):
  Error; stdcall;
function KS_setEcatPdoMapping(hObject: KSHandle; pdoIndex: int; objIndex: int; varIndex: int; bitLength: int; flags: int):
  Error; stdcall;
function KS_uploadEcatFile(hObject: KSHandle; fileName: PAnsiChar; password: int; pData: Pointer; pSize: PInt; flags: int):
  Error; stdcall;
function KS_downloadEcatFile(hObject: KSHandle; fileName: PAnsiChar; password: int; pData: Pointer; size: int; flags: int):
  Error; stdcall;
function KS_readEcatSlaveMem(hSlave: KSHandle; address: int; pData: Pointer; size: int; flags: int):
  Error; stdcall;
function KS_writeEcatSlaveMem(hSlave: KSHandle; address: int; pData: Pointer; size: int; flags: int):
  Error; stdcall;

//------ DC functions ------
function KS_activateEcatDcMode(hObject: KSHandle; var startTime: Int64; cycleTime: int; shiftTime: int; hTimer: KSHandle; flags: int):
  Error; stdcall;
function KS_enumEcatDcOpModes(hSlave: KSHandle; index: int; pDcOpModeBuf: PAnsiChar; pDescriptionBuf: PAnsiChar; flags: int):
  Error; stdcall;
function KS_lookupEcatDcOpMode(hSlave: KSHandle; dcOpMode: PAnsiChar; pDcParams: PKSEcatDcParams; flags: int):
  Error; stdcall;
function KS_configEcatDcOpMode(hSlave: KSHandle; dcOpMode: PAnsiChar; pDcParams: PKSEcatDcParams; flags: int):
  Error; stdcall;

//------ Auxiliary functions ------
function KS_execEcatCommand(hObject: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;
function KS_getEcatAlias(hObject: KSHandle; objIndex: int; varIndex: int; pAliasBuf: PAnsiChar; flags: int):
  Error; stdcall;
function KS_setEcatAlias(hObject: KSHandle; objIndex: int; varIndex: int; alias: PAnsiChar; flags: int):
  Error; stdcall;

//------ EtherCAT Slave Device functions ------
function KS_openEcatSlaveDevice(phSlaveDevice: PKSHandle; name: PAnsiChar; flags: int):
  Error; stdcall;
function KS_closeEcatSlaveDevice(hSlaveDevice: KSHandle; flags: int):
  Error; stdcall;
function KS_queryEcatSlaveDeviceState(hSlaveDevice: KSHandle; pSlaveState: PKSEcatSlaveDeviceState; flags: int):
  Error; stdcall;

//------ EtherCAT Automation Protocol functions ------
function KS_createEcatEapNode(phNode: PKSHandle; hConnection: KSHandle; networkFile: PAnsiChar; flags: int):
  Error; stdcall;
function KS_closeEcatEapNode(hNode: KSHandle; flags: int):
  Error; stdcall;
function KS_queryEcatEapNodeState(hNode: KSHandle; pNodeState: PKSEcatEapNodeState; flags: int):
  Error; stdcall;
function KS_createEcatEapProxy(hNode: KSHandle; phProxy: PKSHandle; pNodeId: PKSEcatEapNodeId; flags: int):
  Error; stdcall;
function KS_deleteEcatEapProxy(hProxy: KSHandle; flags: int):
  Error; stdcall;
function KS_queryEcatEapProxyState(hProxy: KSHandle; pProxyState: PKSEcatEapProxyState; flags: int):
  Error; stdcall;

// ATTENTION! The API of the older version 1.0 of the Kithara EtherCAT Master is not contained here! Please
// download the header file _ethercat_v1.pas from www.kithara.de and include this file additionally!

//--------------------------------------------------------------------------------------------------------------
// MultiFunction Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_UNIPOLAR                                  = $00000001;
  KSF_BIPOLAR                                   = $00000000;
  KSF_DIFFERENTIAL                              = $00000002;
  KSF_SINGLE_ENDED                              = $00000000;
  KSF_RISING_EDGE                               = $00001000;
  KSF_FALLING_EDGE                              = $00000000;
  KSF_NORMALIZE                                 = $00000800;
  KSF_STOP_ANALOG_STREAM                        = $00000400;

  //------ Multi function events ------
  KS_MULTIFUNCTION_INTERRUPT                    = (MULTIFUNCTION_BASE+$00000000);
  KS_MULTIFUNCTION_ANALOGWATCHDOG               = (MULTIFUNCTION_BASE+$00000001);
  KS_MULTIFUNCTION_DIGITALWATCHDOG              = (MULTIFUNCTION_BASE+$00000002);
  KS_MULTIFUNCTION_TIMER                        = (MULTIFUNCTION_BASE+$00000003);
  KS_MULTIFUNCTION_ANALOG_SEQUENCE              = (MULTIFUNCTION_BASE+$00000004);
  KS_MULTIFUNCTION_ANALOG_STREAM                = (MULTIFUNCTION_BASE+$00000006);
  KS_MULTIFUNCTION_DIGITAL_IN                   = (MULTIFUNCTION_BASE+$00000007);
  KS_MULTIFUNCTION_BUFFER_FULL                  = (MULTIFUNCTION_BASE+$00000008);

  //------ Board commands ------
  KS_BOARD_RESET                                = $00000000;
  KS_BOARD_SEQUENCE_READY                       = $00000001;

type
  //------ Context structures ------
  BoardUserContext = packed record
    ctxType: int;
    hBoard: KSHandle;
  end;
  PBoardUserContext = ^BoardUserContext;

  BoardStreamErrorContext = packed record
    ctxType: int;
    hBoard: KSHandle;
    lostDataCount: int;
  end;
  PBoardStreamErrorContext = ^BoardStreamErrorContext;

  KSMfBoardInfo = packed record
    structSize: int;
    pBoardName: array [0..63] of AnsiChar;
    pBoardSpec: array [0..255] of AnsiChar;
    analogInp: int;
    analogOut: int;
    digitalInOut: int;
    digitalInp: int;
    digitalOut: int;
    digitalPortSize: int;
    analogValueSize: int;
  end;
  PKSMfBoardInfo = ^KSMfBoardInfo;

//------ Basic functions ------
function KS_openBoard(phBoard: PKSHandle; name: PAnsiChar; hKernel: KSHandle; flags: int):
  Error; stdcall;
function KS_closeBoard(hBoard: KSHandle; flags: int):
  Error; stdcall;
function KS_getBoardInfo(hBoard: KSHandle; pBoardInfo: PKSMfBoardInfo; flags: int):
  Error; stdcall;
function KS_setBoardObject(hBoard: KSHandle; pAppAddr: Pointer; pSysAddr: Pointer):
  Error; stdcall;
function KS_getBoardObject(hBoard: KSHandle; ppAddr: PPointer):
  Error; stdcall;
function KS_installBoardHandler(hBoard: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_execBoardCommand(hBoard: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;

//------ Low-level functions ------
function KS_readBoardRegister(hBoard: KSHandle; rangeIndex: int; offset: int; pValue: PInt; flags: int):
  Error; stdcall;
function KS_writeBoardRegister(hBoard: KSHandle; rangeIndex: int; offset: int; value: int; flags: int):
  Error; stdcall;

//------ Digital input functions ------
function KS_getDigitalBit(hBoard: KSHandle; channel: int; pValue: PUInt; flags: int):
  Error; stdcall;
function KS_getDigitalPort(hBoard: KSHandle; port: int; pValue: PUInt; flags: int):
  Error; stdcall;

//------ Digital output functions ------
function KS_setDigitalBit(hBoard: KSHandle; channel: int; value: uint; flags: int):
  Error; stdcall;
function KS_setDigitalPort(hBoard: KSHandle; port: int; value: uint; mask: uint; flags: int):
  Error; stdcall;

//------ Analog input functions ------
function KS_configAnalogChannel(hBoard: KSHandle; channel: int; gain: int; mode: int; flags: int):
  Error; stdcall;
function KS_getAnalogValue(hBoard: KSHandle; channel: int; pValue: PUInt; flags: int):
  Error; stdcall;
function KS_initAnalogStream(hBoard: KSHandle; pChannelSequence: PInt; sequenceLength: int; sequenceCount: int; period: int; flags: int):
  Error; stdcall;
function KS_startAnalogStream(hBoard: KSHandle; flags: int):
  Error; stdcall;
function KS_getAnalogStream(hBoard: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int):
  Error; stdcall;
function KS_valueToVolt(hBoard: KSHandle; gain: int; mode: int; value: uint; pVolt: PSingle; flags: int):
  Error; stdcall;

//------ Analog output functions ------
function KS_setAnalogValue(hBoard: KSHandle; channel: int; mode: int; value: int; flags: int):
  Error; stdcall;
function KS_voltToValue(hBoard: KSHandle; mode: int; volt: Single; pValue: PUInt; flags: int):
  Error; stdcall;

// -------------------------------------------------------------------------------------------------------------
// MultiFunction Module Driver Interface
// -------------------------------------------------------------------------------------------------------------

// You only need KSDRV functions, if you develop your own driver DLL!

//--------------------------------------------------------------------------------------------------------------
// CAN Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_BAD_IDENTIFIER                        = (KSERROR_CATEGORY_CAN+$00000000);
  KSERROR_RECV_BUFFER_FULL                      = (KSERROR_CATEGORY_CAN+$00010000);
  KSERROR_XMIT_BUFFER_FULL                      = (KSERROR_CATEGORY_CAN+$00020000);
  KSERROR_BUS_OFF                               = (KSERROR_CATEGORY_CAN+$00030000);
  KSERROR_BUS_PASSIVE                           = (KSERROR_CATEGORY_CAN+$00040000);
  KSERROR_ERROR_COUNTER_WARNING                 = (KSERROR_CATEGORY_CAN+$00050000);
  KSERROR_PORT_DATA_BUFFER_OVERRUN              = (KSERROR_CATEGORY_CAN+$00060000);
  KSERROR_BUS_ERROR                             = (KSERROR_CATEGORY_CAN+$00070000);

  //------ CAN mode ------
  KS_CAN_NORMAL_MODE                            = $00000001;
  KS_CAN_RESET_MODE                             = $00000002;
  KS_CAN_LISTEN_ONLY_MODE                       = $00000004;

  //------ CAN state ------
  KS_CAN_BUS_ON                                 = $00000001;
  KS_CAN_BUS_HEAVY                              = $00000002;
  KS_CAN_BUS_OFF                                = $00000004;
  KS_CAN_RECV_AVAIL                             = $00000008;
  KS_CAN_XMIT_PENDING                           = $00000010;
  KS_CAN_WARN_LIMIT_REACHED                     = $00000020;
  KS_CAN_DATA_BUFFER_OVERRUN                    = $00000040;

  //------ CAN commands ------
  KS_SIGNAL_XMIT_EMPTY                          = $00000004;
  KS_SET_LISTEN_ONLY                            = $00000005;
  KS_RESET_PORT                                 = $00000006;
  KS_GET_ERROR_CODE_CAPTURE                     = $00000007;
  KS_CAN_SET_XMIT_TIMEOUT                       = $00000008;
  KS_CAN_CLEAR_BUS_OFF                          = $0000000a;

  //------ CAN baud rates ------
  KS_CAN_BAUD_1M                                = $000f4240;
  KS_CAN_BAUD_800K                              = $000c3500;
  KS_CAN_BAUD_666K                              = $000a2c2a;
  KS_CAN_BAUD_500K                              = $0007a120;
  KS_CAN_BAUD_250K                              = $0003d090;
  KS_CAN_BAUD_125K                              = $0001e848;
  KS_CAN_BAUD_100K                              = $000186a0;
  KS_CAN_BAUD_50K                               = $0000c350;
  KS_CAN_BAUD_20K                               = $00004e20;
  KS_CAN_BAUD_10K                               = $00002710;

  //------ CAN-FD baud rates ------
  KS_CAN_FD_BAUD_12M                            = $00b71b00;
  KS_CAN_FD_BAUD_10M                            = $00989680;
  KS_CAN_FD_BAUD_8M                             = $007a1200;
  KS_CAN_FD_BAUD_6M                             = $005b8d80;
  KS_CAN_FD_BAUD_4M                             = $003d0900;
  KS_CAN_FD_BAUD_2M                             = $001e8480;

  //------ Common CAN and CAN-FD baud rates ------
  KS_CAN_BAUD_CUSTOM                            = $00000000;

  //------ CAN message types ------
  KS_CAN_MSGTYPE_STANDARD                       = $00000001;
  KS_CAN_MSGTYPE_EXTENDED                       = $00000002;
  KS_CAN_MSGTYPE_RTR                            = $00000004;
  KS_CAN_MSGTYPE_FD                             = $00000008;
  KS_CAN_MSGTYPE_BRS                            = $00000010;

  //------ Legacy CAN events ------
  KS_CAN_RECV                                   = (CAN_BASE+$00000000);
  KS_CAN_XMIT_EMPTY                             = (CAN_BASE+$00000001);
  KS_CAN_XMIT_RTR                               = (CAN_BASE+$00000002);
  KS_CAN_ERROR                                  = (CAN_BASE+$00000003);
  KS_CAN_FILTER                                 = (CAN_BASE+$00000004);
  KS_CAN_TIMEOUT                                = (CAN_BASE+$00000005);

  //------ CAN-FD events ------
  KS_CAN_FD_RECV                                = (CAN_BASE+$00000006);
  KS_CAN_FD_XMIT_EMPTY                          = (CAN_BASE+$00000007);
  KS_CAN_FD_ERROR                               = (CAN_BASE+$00000008);

type
  //------ CAN-FD data structures ------
  KSCanFdMsg = packed record
    structSize: int;
    identifier: int;
    msgType: uint;
    dataLength: int;
    msgData: array [0..63] of byte;
    timestamp: Int64;
  end;
  PKSCanFdMsg = ^KSCanFdMsg;

  KSCanFdState = packed record
    structSize: int;
    state: uint;
    waitingForXmit: int;
    waitingForRecv: int;
    xmitErrCounter: int;
    recvErrCounter: int;
  end;
  PKSCanFdState = ^KSCanFdState;

  KSCanFdConfig = packed record
    structSize: int;
    clockFrequency: int;
    canBaudRate: int;
    canBrp: int;
    canTseg1: int;
    canTseg2: int;
    canSjw: int;
    fdBaudRate: int;
    fdBrp: int;
    fdTseg1: int;
    fdTseg2: int;
    fdSjw: int;
  end;
  PKSCanFdConfig = ^KSCanFdConfig;

  //------ CAN-FD Context structures ------
  KSCanFdContext = packed record
    ctxType: int;
    hCanFd: KSHandle;
  end;
  PKSCanFdContext = ^KSCanFdContext;

  KSCanFdErrorContext = packed record
    ctxType: int;
    hCanFd: KSHandle;
    errorValue: Error;
    timestamp: Int64;
  end;
  PKSCanFdErrorContext = ^KSCanFdErrorContext;

  KSCanFdMsgContext = packed record
    ctxType: int;
    hCanFd: KSHandle;
    msg: KSCanFdMsg;
  end;
  PKSCanFdMsgContext = ^KSCanFdMsgContext;

  //------ CAN data structures ------
  KSCanMsg = packed record
    identifier: int;
    msgType: Word;
    dataLength: Word;
    msgData: array [0..7] of byte;
    timestamp: Int64;
  end;
  PKSCanMsg = ^KSCanMsg;

  KSCanState = packed record
    structSize: int;
    mode: int;
    state: int;
    baudRate: int;
    waitingForXmit: int;
    waitingForRecv: int;
    errorWarnLimit: int;
    recvErrCounter: int;
    xmitErrCounter: int;
    lastError: int;
  end;
  PKSCanState = ^KSCanState;

  //------ CAN Context structures ------
  CanUserContext = packed record
    ctxType: int;
    hCan: KSHandle;
  end;
  PCanUserContext = ^CanUserContext;

  CanErrorUserContext = packed record
    ctxType: int;
    hCan: KSHandle;
    errorValue: Error;
    timestamp: Int64;
  end;
  PCanErrorUserContext = ^CanErrorUserContext;

  CanMsgUserContext = packed record
    ctxType: int;
    hCan: KSHandle;
    msg: KSCanMsg;
    discarded: int;
  end;
  PCanMsgUserContext = ^CanMsgUserContext;

  CanTimeoutUserContext = packed record
    ctxType: int;
    hCan: KSHandle;
    msg: KSCanMsg;
  end;
  PCanTimeoutUserContext = ^CanTimeoutUserContext;

//------ Common CAN-FD functions ------
function KS_openCanFd(phCanFd: PKSHandle; name: PAnsiChar; port: int; pCanFdConfig: PKSCanFdConfig; flags: int):
  Error; stdcall;
function KS_closeCanFd(hCanFd: KSHandle; flags: int):
  Error; stdcall;

//------ CAN-FD messaging functions ------
function KS_xmitCanFdMsg(hCanFd: KSHandle; pCanFdMsg: PKSCanFdMsg; flags: int):
  Error; stdcall;
function KS_recvCanFdMsg(hCanFd: KSHandle; pCanFdMsg: PKSCanFdMsg; flags: int):
  Error; stdcall;
function KS_installCanFdHandler(hCanFd: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_getCanFdState(hCanFd: KSHandle; pCanFdState: PKSCanFdState; flags: int):
  Error; stdcall;
function KS_execCanFdCommand(hCanFd: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;

//------ Common CAN functions ------
function KS_openCan(phCan: PKSHandle; name: PAnsiChar; port: int; baudRate: int; flags: int):
  Error; stdcall;
function KS_openCanEx(phCan: PKSHandle; hConnection: KSHandle; port: int; baudRate: int; flags: int):
  Error; stdcall;
function KS_closeCan(hCan: KSHandle):
  Error; stdcall;

//------ CAN messaging functions ------
function KS_xmitCanMsg(hCan: KSHandle; pCanMsg: PKSCanMsg; flags: int):
  Error; stdcall;
function KS_recvCanMsg(hCan: KSHandle; pCanMsg: PKSCanMsg; flags: int):
  Error; stdcall;
function KS_installCanHandler(hCan: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_getCanState(hCan: KSHandle; pCanState: PKSCanState):
  Error; stdcall;
function KS_execCanCommand(hCan: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// LIN Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ LIN commands ------
  KS_LIN_RESET                                  = $00000000;
  KS_LIN_SLEEP                                  = $00000001;
  KS_LIN_WAKEUP                                 = $00000002;
  KS_LIN_SET_BAUD_RATE                          = $00000004;
  KS_LIN_SET_VERSION                            = $00000005;
  KS_LIN_DISABLE_QUEUE_ERROR                    = $00000006;
  KS_LIN_ENABLE_QUEUE_ERROR                     = $00000007;

  //------ LIN bit rates ------
  KS_LIN_BAUD_19200                             = $00004b00;
  KS_LIN_BAUD_10417                             = $000028b1;
  KS_LIN_BAUD_9600                              = $00002580;
  KS_LIN_BAUD_4800                              = $000012c0;
  KS_LIN_BAUD_2400                              = $00000960;
  KS_LIN_BAUD_1200                              = $000004b0;

  //------ LIN Errors ------
  KS_LIN_ERROR_PHYSICAL                         = $00000001;
  KS_LIN_ERROR_TRANSPORT                        = $00000002;
  KS_LIN_ERROR_BUS_COLLISION                    = $00000003;
  KS_LIN_ERROR_PARITY                           = $00000004;
  KS_LIN_ERROR_CHECKSUM                         = $00000005;
  KS_LIN_ERROR_BREAK_EXPECTED                   = $00000006;
  KS_LIN_ERROR_RESPONSE_TIMEOUT                 = $00000007;
  KS_LIN_ERROR_PID_TIMEOUT                      = $00000009;
  KS_LIN_ERROR_RESPONSE_TOO_SHORT               = $00000010;
  KS_LIN_ERROR_RECV_QUEUE_FULL                  = $00000011;
  KS_LIN_ERROR_RESPONSE_WITHOUT_HEADER          = $00000012;

  //------ LIN events ------
  KS_LIN_ERROR                                  = $00000000;
  KS_LIN_RECV_HEADER                            = $00000001;
  KS_LIN_RECV_RESPONSE                          = $00000002;

type
  //------ LIN data structures ------
  KSLinProperties = packed record
    structSize: int;
    linVersion: int;
    baudRate: int;
  end;
  PKSLinProperties = ^KSLinProperties;

  KSLinState = packed record
    structSize: int;
    properties: KSLinProperties;
    xmitHdrCount: int;
    xmitRspCount: int;
    recvCount: int;
    recvAvail: int;
    recvErrorCount: int;
    recvNoRspCount: int;
    collisionCount: int;
  end;
  PKSLinState = ^KSLinState;

  KSLinHeader = packed record
    identifier: int;
    parity: byte;
    parityOk: byte;
  end;
  PKSLinHeader = ^KSLinHeader;

  KSLinResponse = packed record
    data: array [0..7] of byte;
    dataLen: int;
    checksum: byte;
    checksumOk: byte;
  end;
  PKSLinResponse = ^KSLinResponse;

  KSLinMsg = packed record
    header: KSLinHeader;
    response: KSLinResponse;
    timestamp: Int64;
  end;
  PKSLinMsg = ^KSLinMsg;

  //------ LIN Context structures ------
  LinUserContext = packed record
    ctxType: int;
    hLin: KSHandle;
  end;
  PLinUserContext = ^LinUserContext;

  LinErrorUserContext = packed record
    ctxType: int;
    hLin: KSHandle;
    timestamp: Int64;
    errorCode: int;
  end;
  PLinErrorUserContext = ^LinErrorUserContext;

  LinHeaderUserContext = packed record
    ctxType: int;
    hLin: KSHandle;
    timestamp: Int64;
    header: KSLinHeader;
  end;
  PLinHeaderUserContext = ^LinHeaderUserContext;

  LinResponseUserContext = packed record
    ctxType: int;
    hLin: KSHandle;
    timestamp: Int64;
    msg: KSLinMsg;
  end;
  PLinResponseUserContext = ^LinResponseUserContext;

//------ Common functions ------
function KS_openLin(phLin: PKSHandle; pDeviceName: PAnsiChar; port: int; pProperties: PKSLinProperties; flags: int):
  Error; stdcall;
function KS_openLinEx(phLin: PKSHandle; hLinDevice: KSHandle; pProperties: PKSLinProperties; flags: int):
  Error; stdcall;
function KS_closeLin(hLin: KSHandle; flags: int):
  Error; stdcall;
function KS_installLinHandler(hLin: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_execLinCommand(hLin: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;
function KS_xmitLinHeader(hLin: KSHandle; identifier: int; flags: int):
  Error; stdcall;
function KS_xmitLinResponse(hLin: KSHandle; pData: Pointer; size: int; flags: int):
  Error; stdcall;
function KS_recvLinMsg(hLin: KSHandle; pLinMsg: PKSLinMsg; flags: int):
  Error; stdcall;
function KS_getLinState(hLin: KSHandle; pLinState: PKSLinState; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// CANopen Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_CANO_EMERGENCY_REQUEST                = (KSERROR_CATEGORY_CANOPEN+$00000000);
  KSERROR_CANO_TOPOLOGY_CHANGE                  = (KSERROR_CATEGORY_CANOPEN+$00020000);
  KSERROR_CANO_SLAVE_ERROR                      = (KSERROR_CATEGORY_CANOPEN+$00030000);
  KSERROR_CANO_DATA_INCOMPLETE                  = (KSERROR_CATEGORY_CANOPEN+$00040000);
  KSERROR_CANO_RECEIVED_SYNC                    = (KSERROR_CATEGORY_CANOPEN+$00050000);
  KSERROR_CANO_PDO_NOT_VALID                    = (KSERROR_CATEGORY_CANOPEN+$00060000);

  //------ DataObj type flags ------
  KS_CANO_DATAOBJ_PDO_TYPE                      = $00000001;
  KS_CANO_DATAOBJ_SDO_TYPE                      = $00000002;
  KS_CANO_DATAOBJ_READABLE                      = $00010000;
  KS_CANO_DATAOBJ_WRITEABLE                     = $00020000;
  KS_CANO_DATAOBJ_ACTIVE                        = $00040000;
  KS_CANO_DATAOBJ_MAPPABLE                      = $00080000;

  //------ Functionality flags ------
  KS_CANO_SIMPLE_BOOTUP_MASTER                  = $00000001;
  KS_CANO_SIMPLE_BOOTUP_SLAVE                   = $00000002;
  KS_CANO_DYNAMIC_CHANNELS                      = $00000004;
  KS_CANO_GROUP_MESSAGING                       = $00000008;
  KS_CANO_LAYER_SETTINGS                        = $00000010;

  //------ State type ------
  KS_CANO_STATE_INIT                            = $00000001;
  KS_CANO_STATE_PREOP                           = $00000002;
  KS_CANO_STATE_STOP                            = $00000003;
  KS_CANO_STATE_OP                              = $00000004;

  //------ CANopen commands ------
  KS_CANO_XMIT_TIME_STAMP                       = $00000001;

  //------ common index ------
  KS_CANO_INDEX_ALL                             = Integer($ffffffff);
  KS_CANO_INDEX_INPUT                           = Integer($fffffffe);
  KS_CANO_INDEX_OUTPUT                          = Integer($fffffffd);

  //------ CANopen events ------
  KS_CANO_ERROR                                 = (CANOPEN_BASE+$00000000);
  KS_CANO_DATASET_SIGNAL                        = (CANOPEN_BASE+$00000001);
  KS_CANO_TOPOLOGY_CHANGE                       = (CANOPEN_BASE+$00000002);

  //------ Topology change reasons ------
  KS_CANO_TOPOLOGY_MASTER_CONNECTED             = $00000000;
  KS_CANO_TOPOLOGY_MASTER_DISCONNECTED          = $00000001;
  KS_CANO_TOPOLOGY_SLAVE_COUNT_CHANGED          = $00000002;
  KS_CANO_TOPOLOGY_SLAVE_ONLINE                 = $00000003;
  KS_CANO_TOPOLOGY_SLAVE_OFFLINE                = $00000004;

type
  //------ Types & Structures ------
  CanoErrorUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    error: int;
  end;
  PCanoErrorUserContext = ^CanoErrorUserContext;

  CanoDataSetUserContext = packed record
    ctxType: int;
    hDataSet: KSHandle;
  end;
  PCanoDataSetUserContext = ^CanoDataSetUserContext;

  CanoTopologyUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    slavesOnline: int;
    slavesCreated: int;
    slavesCreatedAndOnline: int;
    reason: int;
    hSlave: KSHandle;
  end;
  PCanoTopologyUserContext = ^CanoTopologyUserContext;

  KSCanoEmergencyObj = packed record
    errorCode: Word;
    errorRegister: byte;
    additionalCode: array [0..4] of byte;
  end;
  PKSCanoEmergencyObj = ^KSCanoEmergencyObj;

  KSCanoMasterState = packed record
    structSize: int;
    connected: int;
    slavesOnline: int;
    slaveCreated: int;
    slavesCreatedAndOnline: int;
    masterState: int;
    lastError: int;
  end;
  PKSCanoMasterState = ^KSCanoMasterState;

  KSCanoSlaveState = packed record
    structSize: int;
    online: int;
    created: int;
    vendor: int;
    product: int;
    revision: int;
    id: int;
    slaveState: int;
    emergencyObj: KSCanoEmergencyObj;
  end;
  PKSCanoSlaveState = ^KSCanoSlaveState;

  KSCanoDataVarInfo = packed record
    objType: int;
    name: PAnsiChar;
    dataType: int;
    bitLength: int;
    subIndex: int;
    defaultValue: int;
    minValue: int;
    maxValue: int;
  end;
  PKSCanoDataVarInfo = ^KSCanoDataVarInfo;

  PPKSCanoDataVarInfo = ^PKSCanoDataVarInfo;

  KSCanoDataObjInfo = packed record
    objType: int;
    name: PAnsiChar;
    bitLength: int;
    index: int;
    cobId: int;
    mappingIndex: int;
    transmissionType: int;
    varCount: int;
    vars: array [0..0] of PKSCanoDataVarInfo;
  end;
  PKSCanoDataObjInfo = ^KSCanoDataObjInfo;

  PPKSCanoDataObjInfo = ^PKSCanoDataObjInfo;

  KSCanoSlaveInfo = packed record
    vendorId: int;
    productId: int;
    revision: int;
    name: PAnsiChar;
    orderCode: PAnsiChar;
    rxPdoCount: int;
    txPdoCount: int;
    granularity: int;
    functionality: int;
    objCount: int;
    objs: array [0..0] of PKSCanoDataObjInfo;
  end;
  PKSCanoSlaveInfo = ^KSCanoSlaveInfo;

  PPKSCanoSlaveInfo = ^PKSCanoSlaveInfo;

//------ Master functions ------
function KS_createCanoMaster(phMaster: PKSHandle; hConnection: KSHandle; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int):
  Error; stdcall;
function KS_closeCanoMaster(hMaster: KSHandle):
  Error; stdcall;
function KS_installCanoMasterHandler(hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_queryCanoMasterState(hMaster: KSHandle; pMasterState: PKSCanoMasterState; flags: int):
  Error; stdcall;
function KS_changeCanoMasterState(hMaster: KSHandle; state: int; flags: int):
  Error; stdcall;

//------ Slave functions ------
function KS_createCanoSlave(hMaster: KSHandle; phSlave: PKSHandle; id: int; position: int; vendor: uint; product: uint; revision: uint; flags: int):
  Error; stdcall;
function KS_createCanoSlaveIndirect(hMaster: KSHandle; phSlave: PKSHandle; pSlaveState: PKSCanoSlaveState; flags: int):
  Error; stdcall;
function KS_enumCanoSlaves(hMaster: KSHandle; index: int; pSlaveState: PKSCanoSlaveState; flags: int):
  Error; stdcall;
function KS_deleteCanoSlave(hSlave: KSHandle):
  Error; stdcall;
function KS_queryCanoSlaveState(hSlave: KSHandle; pSlaveState: PKSCanoSlaveState; flags: int):
  Error; stdcall;
function KS_changeCanoSlaveState(hSlave: KSHandle; state: int; flags: int):
  Error; stdcall;
function KS_queryCanoSlaveInfo(hSlave: KSHandle; ppSlaveInfo: PPKSCanoSlaveInfo; flags: int):
  Error; stdcall;
function KS_queryCanoDataObjInfo(hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSCanoDataObjInfo; flags: int):
  Error; stdcall;
function KS_queryCanoDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSCanoDataVarInfo; flags: int):
  Error; stdcall;
function KS_enumCanoDataObjInfo(hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSCanoDataObjInfo; flags: int):
  Error; stdcall;
function KS_enumCanoDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSCanoDataVarInfo; flags: int):
  Error; stdcall;

//------ DataSet related functions ------
function KS_createCanoDataSet(hMaster: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int):
  Error; stdcall;
function KS_deleteCanoDataSet(hSet: KSHandle):
  Error; stdcall;
function KS_assignCanoDataSet(hSet: KSHandle; hSlave: KSHandle; pdoIndex: int; placement: int; flags: int):
  Error; stdcall;
function KS_readCanoDataSet(hSet: KSHandle; flags: int):
  Error; stdcall;
function KS_postCanoDataSet(hSet: KSHandle; flags: int):
  Error; stdcall;
function KS_installCanoDataSetHandler(hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;

//------ Data exchange functions ------
function KS_getCanoDataObjAddress(hSet: KSHandle; hSlave: KSHandle; pdoIndex: int; subIndex: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int):
  Error; stdcall;
function KS_readCanoDataObj(hSlave: KSHandle; index: int; subIndex: int; pData: Pointer; pSize: PInt; flags: int):
  Error; stdcall;
function KS_postCanoDataObj(hSlave: KSHandle; index: int; subIndex: int; pData: Pointer; size: int; flags: int):
  Error; stdcall;

//------ Auxiliary functions ------
function KS_changeCanoState(hObject: KSHandle; state: int; flags: int):
  Error; stdcall;
function KS_execCanoCommand(hObject: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Profibus Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error Codes ------
  KSERROR_PBUS_CONNECTION_CHANGE                = (KSERROR_CATEGORY_PROFIBUS+$00010000);
  KSERROR_PBUS_TOPOLOGY_CHANGE                  = (KSERROR_CATEGORY_PROFIBUS+$00020000);
  KSERROR_PBUS_SLAVE_ERROR                      = (KSERROR_CATEGORY_PROFIBUS+$00030000);
  KSERROR_PBUS_ALARM_BUFFER_FULL                = (KSERROR_CATEGORY_PROFIBUS+$00040000);
  KSERROR_BAD_PARAMETERIZATION_SET              = (KSERROR_CATEGORY_PROFIBUS+$00050000);
  KSERROR_PBUS_MASTER_ERROR                     = (KSERROR_CATEGORY_PROFIBUS+$00060000);
  KSERROR_HARDWARE_INTERFACE                    = (KSERROR_CATEGORY_PROFIBUS+$00070000);
  KSERROR_AUTO_CLEAR                            = (KSERROR_CATEGORY_PROFIBUS+$00080000);

  //------ Flags ------
  KSF_ACCEPT_OFFLINE                            = $00000001;
  KSF_SLOT                                      = $00000002;

  //------ Master State ------
  KS_PBUS_OFFLINE                               = $00000001;
  KS_PBUS_STOP                                  = $00000002;
  KS_PBUS_CLEAR                                 = $00000003;
  KS_PBUS_OPERATE                               = $00000004;

  //------ Profibus special indices ------
  KS_PBUS_INDEX_ALL                             = Integer($ffffffff);
  KS_PBUS_INDEX_INPUT                           = Integer($fffffffe);
  KS_PBUS_INDEX_OUTPUT                          = Integer($fffffffd);

  //------ Profibus commands ------
  KS_UNFREEZE                                   = $00000001;
  KS_FREEZE                                     = $00000002;
  KS_UNSYNC                                     = $00000004;
  KS_SYNC                                       = $00000008;

  //------ Profibus command params ------
  KS_SELECT_GROUP_1                             = $00000100;
  KS_SELECT_GROUP_2                             = $00000200;
  KS_SELECT_GROUP_3                             = $00000400;
  KS_SELECT_GROUP_4                             = $00000800;
  KS_SELECT_GROUP_5                             = $00001000;
  KS_SELECT_GROUP_6                             = $00002000;
  KS_SELECT_GROUP_7                             = $00004000;
  KS_SELECT_GROUP_8                             = $00008000;

  //------ Profibus bit rates ------
  KS_PBUS_BAUD_12M                              = $00b71b00;
  KS_PBUS_BAUD_6M                               = $005b8d80;
  KS_PBUS_BAUD_3M                               = $002dc6c0;
  KS_PBUS_BAUD_1500K                            = $0016e360;
  KS_PBUS_BAUD_500K                             = $0007a120;
  KS_PBUS_BAUD_187500                           = $0002dc6c;
  KS_PBUS_BAUD_93750                            = $00016e36;
  KS_PBUS_BAUD_45450                            = $0000b18a;
  KS_PBUS_BAUD_31250                            = $00007a12;
  KS_PBUS_BAUD_19200                            = $00004b00;
  KS_PBUS_BAUD_9600                             = $00002580;

  //------ Profibus events ------
  KS_PBUS_ERROR                                 = (PROFIBUS_BASE+$00000000);
  KS_PBUS_DATASET_SIGNAL                        = (PROFIBUS_BASE+$00000001);
  KS_PBUS_TOPOLOGY_CHANGE                       = (PROFIBUS_BASE+$00000002);
  KS_PBUS_ALARM                                 = (PROFIBUS_BASE+$00000003);
  KS_PBUS_DIAGNOSTIC                            = (PROFIBUS_BASE+$00000004);

  //------ Profibus alarm types ------
  KS_PBUS_DIAGNOSTIC_ALARM                      = $00000001;
  KS_PBUS_PROCESS_ALARM                         = $00000002;
  KS_PBUS_PULL_ALARM                            = $00000003;
  KS_PBUS_PLUG_ALARM                            = $00000004;
  KS_PBUS_STATUS_ALARM                          = $00000005;
  KS_PBUS_UPDATE_ALARM                          = $00000006;

  //------ Profibus topology change reasons ------
  KS_PBUS_TOPOLOGY_MASTER_CONNECTED             = $00000000;
  KS_PBUS_TOPOLOGY_MASTER_DISCONNECTED          = $00000001;
  KS_PBUS_TOPOLOGY_SLAVE_COUNT_CHANGED          = $00000002;
  KS_PBUS_TOPOLOGY_SLAVE_ONLINE                 = $00000003;
  KS_PBUS_TOPOLOGY_SLAVE_OFFLINE                = $00000004;

type
  //------ Types & Structures ------
  PbusErrorUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    error: int;
  end;
  PPbusErrorUserContext = ^PbusErrorUserContext;

  PbusDataSetUserContext = packed record
    ctxType: int;
    hDataSet: KSHandle;
  end;
  PPbusDataSetUserContext = ^PbusDataSetUserContext;

  PbusTopologyUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    slavesOnline: int;
    slavesConfigured: int;
    reason: int;
  end;
  PPbusTopologyUserContext = ^PbusTopologyUserContext;

  PbusAlarmUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    slaveAddress: int;
    alarmType: int;
    slotNumber: int;
    specifier: int;
    diagnosticLength: int;
    diagnosticData: array [0..59] of int;
  end;
  PPbusAlarmUserContext = ^PbusAlarmUserContext;

  PbusDiagnosticUserContext = packed record
    ctxType: int;
    hMaster: KSHandle;
    hSlave: KSHandle;
    slaveAddress: int;
  end;
  PPbusDiagnosticUserContext = ^PbusDiagnosticUserContext;

  KSPbusMasterState = packed record
    structSize: int;
    connected: int;
    slavesOnline: int;
    highestStationAddr: int;
    slavesConfigured: int;
    masterState: int;
    busErrorCounter: int;
    timeoutCounter: int;
    lastError: int;
  end;
  PKSPbusMasterState = ^KSPbusMasterState;

  KSPbusSlaveState = packed record
    structSize: int;
    address: int;
    identNumber: int;
    configured: int;
    assigned: int;
    state: int;
    stdStationStatus: array [0..5] of byte;
    extendedDiag: array [0..237] of byte;
  end;
  PKSPbusSlaveState = ^KSPbusSlaveState;

  KSPbusDataVarInfo = packed record
    dataType: int;
    subIndex: int;
    byteLength: int;
    name: PAnsiChar;
  end;
  PKSPbusDataVarInfo = ^KSPbusDataVarInfo;

  PPKSPbusDataVarInfo = ^PKSPbusDataVarInfo;

  KSPbusSlotInfo = packed record
    slotNumber: int;
    varCount: int;
    byteLength: int;
    name: PAnsiChar;
    vars: array [0..0] of PKSPbusDataVarInfo;
  end;
  PKSPbusSlotInfo = ^KSPbusSlotInfo;

  PPKSPbusSlotInfo = ^PKSPbusSlotInfo;

  KSPbusSlaveInfo = packed record
    address: int;
    identNumber: int;
    slotCount: int;
    byteLength: int;
    name: PAnsiChar;
    slots: array [0..0] of PKSPbusSlotInfo;
  end;
  PKSPbusSlaveInfo = ^KSPbusSlaveInfo;

  PPKSPbusSlaveInfo = ^PKSPbusSlaveInfo;

//------ Master functions ------
function KS_createPbusMaster(phMaster: PKSHandle; name: PAnsiChar; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int):
  Error; stdcall;
function KS_closePbusMaster(hMaster: KSHandle):
  Error; stdcall;
function KS_queryPbusMasterState(hMaster: KSHandle; pMasterState: PKSPbusMasterState; flags: int):
  Error; stdcall;
function KS_changePbusMasterState(hMaster: KSHandle; state: int; flags: int):
  Error; stdcall;
function KS_enumPbusSlaves(hMaster: KSHandle; index: int; pSlaveState: PKSPbusSlaveState; flags: int):
  Error; stdcall;
function KS_installPbusMasterHandler(hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_execPbusMasterCommand(hMaster: KSHandle; command: int; pParam: Pointer; flags: int):
  Error; stdcall;

//------ Slave functions ------
function KS_createPbusSlave(hMaster: KSHandle; phSlave: PKSHandle; address: int; flags: int):
  Error; stdcall;
function KS_deletePbusSlave(hSlave: KSHandle):
  Error; stdcall;
function KS_queryPbusSlaveState(hSlave: KSHandle; pSlaveState: PKSPbusSlaveState; flags: int):
  Error; stdcall;
function KS_changePbusSlaveState(hSlave: KSHandle; state: int; flags: int):
  Error; stdcall;
function KS_queryPbusSlaveInfo(hSlave: KSHandle; ppSlaveInfo: PPKSPbusSlaveInfo; flags: int):
  Error; stdcall;
function KS_queryPbusSlotInfo(hSlave: KSHandle; objIndex: int; ppSlotInfo: PPKSPbusSlotInfo; flags: int):
  Error; stdcall;
function KS_queryPbusDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSPbusDataVarInfo; flags: int):
  Error; stdcall;

//------ Data exchange functions ------
function KS_createPbusDataSet(hMaster: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int):
  Error; stdcall;
function KS_deletePbusDataSet(hSet: KSHandle):
  Error; stdcall;
function KS_assignPbusDataSet(hSet: KSHandle; hSlave: KSHandle; slot: int; index: int; placement: int; flags: int):
  Error; stdcall;
function KS_readPbusDataSet(hSet: KSHandle; flags: int):
  Error; stdcall;
function KS_postPbusDataSet(hSet: KSHandle; flags: int):
  Error; stdcall;
function KS_installPbusDataSetHandler(hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;
function KS_getPbusDataObjAddress(hSet: KSHandle; hSlave: KSHandle; slot: int; index: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int):
  Error; stdcall;
function KS_readPbusDataObj(hSlave: KSHandle; slot: int; index: int; pData: Pointer; pSize: PInt; flags: int):
  Error; stdcall;
function KS_postPbusDataObj(hSlave: KSHandle; slot: int; index: int; pData: Pointer; size: int; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// RTL Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Mathematical constants ------
  KS_PI                                         = 3.14159265358979323846;
  KS_E                                          = 2.71828182845904523536;

//------ stdlib.h replacements ------
function KSRTL_calloc(num: uint; size: uint):
  Pointer; stdcall;
procedure KSRTL_free(ptr: Pointer)
  stdcall;
function KSRTL_malloc(size: uint):
  Pointer; stdcall;

//------ math.h replacements ------
function KSRTL_sin(x: Double):
  Double; stdcall;
function KSRTL_cos(x: Double):
  Double; stdcall;
function KSRTL_tan(x: Double):
  Double; stdcall;
function KSRTL_asin(x: Double):
  Double; stdcall;
function KSRTL_acos(x: Double):
  Double; stdcall;
function KSRTL_atan(x: Double):
  Double; stdcall;
function KSRTL_atan2(y: Double; x: Double):
  Double; stdcall;
function KSRTL_sinh(x: Double):
  Double; stdcall;
function KSRTL_cosh(x: Double):
  Double; stdcall;
function KSRTL_tanh(x: Double):
  Double; stdcall;
function KSRTL_exp(x: Double):
  Double; stdcall;
function KSRTL_frexp(x: Double; exp: PInt):
  Double; stdcall;
function KSRTL_ldexp(x: Double; exp: int):
  Double; stdcall;
function KSRTL_log(x: Double):
  Double; stdcall;
function KSRTL_log10(x: Double):
  Double; stdcall;
function KSRTL_modf(x: Double; intpart: PDouble):
  Double; stdcall;
function KSRTL_pow(base: Double; exponent: Double):
  Double; stdcall;
function KSRTL_sqrt(x: Double):
  Double; stdcall;
function KSRTL_fabs(x: Double):
  Double; stdcall;
function KSRTL_ceil(x: Double):
  Double; stdcall;
function KSRTL_floor(x: Double):
  Double; stdcall;
function KSRTL_fmod(numerator: Double; denominator: Double):
  Double; stdcall;

//------ string.h replacements ------
function KSRTL_memchr(ptr: Pointer; value: int; num: uint):
  Pointer; stdcall;
function KSRTL_memcmp(ptr1: Pointer; ptr2: Pointer; num: uint):
  int; stdcall;
function KSRTL_memcpy(destination: Pointer; source: Pointer; num: uint):
  Pointer; stdcall;
function KSRTL_memmove(destination: Pointer; source: Pointer; num: uint):
  Pointer; stdcall;
function KSRTL_memset(ptr: Pointer; value: int; num: uint):
  Pointer; stdcall;
function KSRTL_strlen(str: PAnsiChar):
  uint; stdcall;
function KSRTL_strcmp(str1: PAnsiChar; str2: PAnsiChar):
  int; stdcall;
function KSRTL_strncmp(str1: PAnsiChar; str2: PAnsiChar; num: uint):
  int; stdcall;
function KSRTL_strcpy(destination: PAnsiChar; source: PAnsiChar):
  PAnsiChar; stdcall;
function KSRTL_strncpy(destination: PAnsiChar; source: PAnsiChar; num: uint):
  PAnsiChar; stdcall;
function KSRTL_strcat(destination: PAnsiChar; source: PAnsiChar):
  PAnsiChar; stdcall;
function KSRTL_strncat(destination: PAnsiChar; source: PAnsiChar; num: uint):
  PAnsiChar; stdcall;
function KSRTL_strchr(str: PAnsiChar; character: int):
  PAnsiChar; stdcall;
function KSRTL_strcoll(str1: PAnsiChar; str2: PAnsiChar):
  int; stdcall;
function KSRTL_strrchr(str1: PAnsiChar; character: int):
  PAnsiChar; stdcall;
function KSRTL_strstr(str1: PAnsiChar; str2: PAnsiChar):
  PAnsiChar; stdcall;
function KSRTL_strspn(str1: PAnsiChar; str2: PAnsiChar):
  uint; stdcall;
function KSRTL_strcspn(str1: PAnsiChar; str2: PAnsiChar):
  uint; stdcall;
function KSRTL_strpbrk(str1: PAnsiChar; str2: PAnsiChar):
  PAnsiChar; stdcall;
function KSRTL_strtok(str: PAnsiChar; delimiters: PAnsiChar):
  PAnsiChar; stdcall;
function KSRTL_strxfrm(destination: PAnsiChar; source: PAnsiChar; num: uint):
  uint; stdcall;
function KSRTL_vsprintf(buffer: PAnsiChar; format: PAnsiChar; pArgs: Pointer):
  int; stdcall;
function KSRTL_vsnprintf(buffer: PAnsiChar; n: uint; format: PAnsiChar; pArgs: Pointer):
  int; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Camera Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Flags ------
  KSF_NO_DISCOVERY                              = $00000800;
  KSF_CAMERA_NO_GENICAM                         = $00001000;
  KSF_CAMERA_BROADCAST_DISCOVERY                = $00800000;

type
  //------ Types & Structures ------
  KSCameraInfo = packed record
    structSize: int;
    hardwareId: array [0..79] of AnsiChar;
    vendor: array [0..79] of AnsiChar;
    name: array [0..79] of AnsiChar;
    serialNumber: array [0..79] of AnsiChar;
    userName: array [0..79] of AnsiChar;
  end;
  PKSCameraInfo = ^KSCameraInfo;

  KSCameraTimeouts = packed record
    structSize: int;
    commandTimeout: int;
    commandRetries: int;
    heartbeatTimeout: int;
  end;
  PKSCameraTimeouts = ^KSCameraTimeouts;

  KSCameraForceIp = packed record
    structSize: int;
    macAddress: array [0..17] of AnsiChar;
    address: uint;
    subnetMask: uint;
    gatewayAddress: uint;
  end;
  PKSCameraForceIp = ^KSCameraForceIp;

  KSCameraInfoGev = packed record
    structSize: int;
    vendor: array [0..31] of AnsiChar;
    name: array [0..31] of AnsiChar;
    serialNumber: array [0..15] of AnsiChar;
    userName: array [0..15] of AnsiChar;
    macAddress: array [0..17] of AnsiChar;
    instance: int;
    instanceName: array [0..255] of AnsiChar;
  end;
  PKSCameraInfoGev = ^KSCameraInfoGev;

  KSCameraEvent = packed record
    structSize: int;
    eventId: int;
    timestamp: UInt64;
    blockId: UInt64;
    channel: int;
    size: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
  end;
  PKSCameraEvent = ^KSCameraEvent;

  //------ Block type structures ------
  KSCameraBlock = packed record
    blockType: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
    id: UInt64;
    timestamp: UInt64;
  end;
  PKSCameraBlock = ^KSCameraBlock;

  PPKSCameraBlock = ^PKSCameraBlock;

  KSCameraImage = packed record
    blockType: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
    id: UInt64;
    timestamp: UInt64;
    fieldCount: int;
    fieldId: int;
    pixelFormat: uint;
    width: uint;
    height: uint;
    offsetX: uint;
    offsetY: uint;
    linePadding: uint;
    imagePadding: uint;
    chunkDataPayloadLength: uint;
    chunkLayoutId: uint;
  end;
  PKSCameraImage = ^KSCameraImage;

  KSCameraJpeg = packed record
    blockType: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
    id: UInt64;
    timestamp: UInt64;
    fieldCount: int;
    fieldId: int;
    jpegPayloadSize: UInt64;
    tickFrequency: UInt64;
    dataFormat: uint;
    isColorSpace: int;
    chunkDataPayloadLength: uint;
    chunkLayoutId: uint;
  end;
  PKSCameraJpeg = ^KSCameraJpeg;

  KSCameraChunk = packed record
    blockType: int;
    pAppPtr: Pointer;
    pSysPtr: Pointer;
    id: UInt64;
    timestamp: UInt64;
    chunkDataPayloadLength: uint;
    chunkLayoutId: uint;
  end;
  PKSCameraChunk = ^KSCameraChunk;

const
  //------ Block types ------
  KS_CAMERA_BLOCKTYPE_NONE                      = $00000000;
  KS_CAMERA_BLOCKTYPE_IMAGE                     = $00000001;
  KS_CAMERA_BLOCKTYPE_CHUNK                     = $00000002;
  KS_CAMERA_BLOCKTYPE_JPEG                      = $00000003;
  KS_CAMERA_BLOCKTYPE_JPEG2000                  = $00000004;

  //------ Camera commands ------
  KS_CAMERA_READREG                             = $00000001;
  KS_CAMERA_READMEM                             = $00000002;
  KS_CAMERA_WRITEREG                            = $00000003;
  KS_CAMERA_SET_TIMEOUTS                        = $00000004;
  KS_CAMERA_GET_TIMEOUTS                        = $00000005;
  KS_CAMERA_GET_XML_FILE                        = $00000006;
  KS_CAMERA_GET_XML_INFO                        = $00000007;
  KS_CAMERA_SET_DISCOVERY_INTERVAL              = $00000008;
  KS_CAMERA_RESIZE_BUFFERS                      = $00000009;
  KS_CAMERA_GET_INFO                            = $0000000a;
  KS_CAMERA_SET_CHUNK_DATA                      = $0000000b;
  KS_CAMERA_SEND_TO_SOURCE_PORT                 = $0000000c;
  KS_CAMERA_FORCEIP                             = $0000000d;
  KS_CAMERA_WRITEMEM                            = $0000000e;
  KS_CAMERA_RECV_EVENT                          = $00000011;

  //------ Acquisition modes ------
  KS_CAMERA_CONTINUOUS                          = Integer($ffffffff);
  KS_CAMERA_PRESERVE                            = $00000000;
  KS_CAMERA_SINGLE_FRAME                        = $00000001;
  KS_CAMERA_MULTI_FRAME                         = $00000000;

  //------ Configuration commands ------
  KS_CAMERA_CONFIG_GET                          = $00000001;
  KS_CAMERA_CONFIG_SET                          = $00000002;
  KS_CAMERA_CONFIG_TYPE                         = $00000003;
  KS_CAMERA_CONFIG_ENUMERATE                    = $00000004;
  KS_CAMERA_CONFIG_MIN                          = $00000005;
  KS_CAMERA_CONFIG_MAX                          = $00000006;
  KS_CAMERA_CONFIG_INC                          = $00000007;
  KS_CAMERA_CONFIG_LENGTH                       = $00010000;

  //------ Configuration types ------
  KS_CAMERA_CONFIG_CATEGORY                     = $00000001;
  KS_CAMERA_CONFIG_BOOLEAN                      = $00000002;
  KS_CAMERA_CONFIG_ENUMERATION                  = $00000003;
  KS_CAMERA_CONFIG_INTEGER                      = $00000004;
  KS_CAMERA_CONFIG_COMMAND                      = $00000005;
  KS_CAMERA_CONFIG_FLOAT                        = $00000006;
  KS_CAMERA_CONFIG_STRING                       = $00000007;
  KS_CAMERA_CONFIG_REGISTER                     = $00000008;

  //------ Receive and error handler types ------
  KS_CAMERA_IMAGE_RECEIVED                      = $00000001;
  KS_CAMERA_IMAGE_DROPPED                       = $00000002;
  KS_CAMERA_ERROR                               = $00000003;
  KS_CAMERA_GEV_ATTACHED                        = $00000004;
  KS_CAMERA_GEV_DETACHED                        = $00000005;
  KS_CAMERA_ATTACHED                            = $00000006;
  KS_CAMERA_DETACHED                            = $00000007;
  KS_CAMERA_EVENT                               = $00000008;

  //------ Error codes ------
  KSERROR_CAMERA_COMMAND_ERROR                  = (KSERROR_CATEGORY_CAMERA+$00010000);
  KSERROR_CAMERA_COMMAND_TIMEOUT                = (KSERROR_CATEGORY_CAMERA+$00020000);
  KSERROR_CAMERA_COMMAND_FAILED                 = (KSERROR_CATEGORY_CAMERA+$00030000);
  KSERROR_CAMERA_COMMAND_BAD_ALIGNMENT          = (KSERROR_CATEGORY_CAMERA+$00040000);
  KSERROR_CAMERA_RECONNECTED                    = (KSERROR_CATEGORY_CAMERA+$000f0000);
  KSERROR_CAMERA_STREAM_INVALID_DATA            = (KSERROR_CATEGORY_CAMERA+$00100000);
  KSERROR_CAMERA_STREAM_NO_BUFFER               = (KSERROR_CATEGORY_CAMERA+$00110000);
  KSERROR_CAMERA_STREAM_NOT_TRANSMITTED         = (KSERROR_CATEGORY_CAMERA+$00120000);
  KSERROR_CAMERA_STREAM_INCOMPLETE_TRANSMISSION = (KSERROR_CATEGORY_CAMERA+$00130000);
  KSERROR_CAMERA_STREAM_BUFFER_OVERRUN          = (KSERROR_CATEGORY_CAMERA+$00140000);
  KSERROR_CAMERA_STREAM_BUFFER_QUEUED           = (KSERROR_CATEGORY_CAMERA+$00150000);
  KSERROR_GENICAM_ERROR                         = (KSERROR_CATEGORY_CAMERA+$00800000);
  KSERROR_GENICAM_FEATURE_NOT_IMPLEMENTED       = (KSERROR_CATEGORY_CAMERA+$00810000);
  KSERROR_GENICAM_FEATURE_NOT_AVAILABLE         = (KSERROR_CATEGORY_CAMERA+$00820000);
  KSERROR_GENICAM_FEATURE_ACCESS_DENIED         = (KSERROR_CATEGORY_CAMERA+$00830000);
  KSERROR_GENICAM_FEATURE_NOT_FOUND             = (KSERROR_CATEGORY_CAMERA+$00840000);
  KSERROR_GENICAM_ENUM_ENTRY_NOT_FOUND          = (KSERROR_CATEGORY_CAMERA+$00850000);
  KSERROR_GENICAM_SUBNODE_NOT_REACHABLE         = (KSERROR_CATEGORY_CAMERA+$00860000);
  KSERROR_GENICAM_PORT_NOT_REACHABLE            = (KSERROR_CATEGORY_CAMERA+$00870000);
  KSERROR_GENICAM_FEATURE_NOT_SUPPORTED         = (KSERROR_CATEGORY_CAMERA+$00880000);
  KSERROR_GENICAM_VALUE_TOO_LARGE               = (KSERROR_CATEGORY_CAMERA+$00890000);
  KSERROR_GENICAM_VALUE_TOO_SMALL               = (KSERROR_CATEGORY_CAMERA+$008a0000);
  KSERROR_GENICAM_VALUE_BAD_INCREMENT           = (KSERROR_CATEGORY_CAMERA+$008b0000);
  KSERROR_GENICAM_BAD_REGISTER_DESCRIPTION      = (KSERROR_CATEGORY_CAMERA+$008c0000);

  //------ GigE Vision<sup>®</sup> registers ------
  KS_CAMERA_REGISTER_STREAM_CHANNELS            = $00000904;

  //------ Camera module config ------
  KSCONFIG_OVERRIDE_CAMERA_XML_FILE             = $00000001;

  //------ GigE Vision<sup>®</sup> pixel formats ------
  KS_CAMERA_PIXEL_MONO_8                        = $01080001;
  KS_CAMERA_PIXEL_MONO_8S                       = $01080002;
  KS_CAMERA_PIXEL_MONO_10                       = $01100003;
  KS_CAMERA_PIXEL_MONO_10_PACKED                = $010c0004;
  KS_CAMERA_PIXEL_MONO_12                       = $01100005;
  KS_CAMERA_PIXEL_MONO_12_PACKED                = $010c0006;
  KS_CAMERA_PIXEL_MONO_16                       = $01100007;
  KS_CAMERA_PIXEL_BAYER_GR_8                    = $01080008;
  KS_CAMERA_PIXEL_BAYER_RG_8                    = $01080009;
  KS_CAMERA_PIXEL_BAYER_GB_8                    = $0108000a;
  KS_CAMERA_PIXEL_BAYER_BG_8                    = $0108000b;
  KS_CAMERA_PIXEL_BAYER_GR_10                   = $0110000c;
  KS_CAMERA_PIXEL_BAYER_RG_10                   = $0110000d;
  KS_CAMERA_PIXEL_BAYER_GB_10                   = $0110000e;
  KS_CAMERA_PIXEL_BAYER_BG_10                   = $0110000f;
  KS_CAMERA_PIXEL_BAYER_GR_12                   = $01100010;
  KS_CAMERA_PIXEL_BAYER_RG_12                   = $01100011;
  KS_CAMERA_PIXEL_BAYER_GB_12                   = $01100012;
  KS_CAMERA_PIXEL_BAYER_BG_12                   = $01100013;
  KS_CAMERA_PIXEL_RGB_8                         = $02180014;
  KS_CAMERA_PIXEL_BGR_8                         = $02180015;
  KS_CAMERA_PIXEL_RGBA_8                        = $02200016;
  KS_CAMERA_PIXEL_BGRA_8                        = $02200017;
  KS_CAMERA_PIXEL_RGB_10                        = $02300018;
  KS_CAMERA_PIXEL_BGR_10                        = $02300019;
  KS_CAMERA_PIXEL_RGB_12                        = $0230001a;
  KS_CAMERA_PIXEL_BGR_12                        = $0230001b;
  KS_CAMERA_PIXEL_RGB_10_V1_PACKED              = $0220001c;
  KS_CAMERA_PIXEL_RGB_10_P_32                   = $0220001d;
  KS_CAMERA_PIXEL_YUV_411_8_UYYVYY              = $020c001e;
  KS_CAMERA_PIXEL_YUV_422_8_UYVY                = $0210001f;
  KS_CAMERA_PIXEL_YUV_8_UYV                     = $02180020;
  KS_CAMERA_PIXEL_RGB_8_PLANAR                  = $02180021;
  KS_CAMERA_PIXEL_RGB_10_PLANAR                 = $02300022;
  KS_CAMERA_PIXEL_RGB_12_PLANAR                 = $02300023;
  KS_CAMERA_PIXEL_RGB_16_PLANAR                 = $02300024;
  KS_CAMERA_PIXEL_MONO_14                       = $01100025;
  KS_CAMERA_PIXEL_BAYER_GR_10_PACKED            = $010c0026;
  KS_CAMERA_PIXEL_BAYER_RG_10_PACKED            = $010c0027;
  KS_CAMERA_PIXEL_BAYER_GB_10_PACKED            = $010c0028;
  KS_CAMERA_PIXEL_BAYER_BG_10_PACKED            = $010c0029;
  KS_CAMERA_PIXEL_BAYER_GR_12_PACKED            = $010c002a;
  KS_CAMERA_PIXEL_BAYER_RG_12_PACKED            = $010c002b;
  KS_CAMERA_PIXEL_BAYER_GB_12_PACKED            = $010c002c;
  KS_CAMERA_PIXEL_BAYER_BG_12_PACKED            = $010c002d;
  KS_CAMERA_PIXEL_BAYER_GR_16                   = $0110002e;
  KS_CAMERA_PIXEL_BAYER_RG_16                   = $0110002f;
  KS_CAMERA_PIXEL_BAYER_GB_16                   = $01100030;
  KS_CAMERA_PIXEL_BAYER_BG_16                   = $01100031;
  KS_CAMERA_PIXEL_YUV_422_8                     = $020c0032;
  KS_CAMERA_PIXEL_RGB_16                        = $02300033;
  KS_CAMERA_PIXEL_RGB_12_V1_PACKED              = $02240034;
  KS_CAMERA_PIXEL_RGB_565_P                     = $02100035;
  KS_CAMERA_PIXEL_BGR_565_P                     = $02100036;
  KS_CAMERA_PIXEL_MONO_1P                       = $01010037;
  KS_CAMERA_PIXEL_MONO_2P                       = $01020038;
  KS_CAMERA_PIXEL_MONO_4P                       = $01040039;
  KS_CAMERA_PIXEL_YCBCR_8_CBYCR                 = $0218003a;
  KS_CAMERA_PIXEL_YCBCR_422_8                   = $0210003b;
  KS_CAMERA_PIXEL_YCBCR_411_8_CBYYCRYY          = $020c003c;
  KS_CAMERA_PIXEL_YCBCR_601_8_CBYCR             = $0218003d;
  KS_CAMERA_PIXEL_YCBCR_601_422_8               = $0210003e;
  KS_CAMERA_PIXEL_YCBCR_601_411_8_CBYYCRYY      = $020c003f;
  KS_CAMERA_PIXEL_YCBCR_709_8_CBYCR             = $02180040;
  KS_CAMERA_PIXEL_YCBCR_709_422_8               = $02100041;
  KS_CAMERA_PIXEL_YCBCR_709_411_8_CBYYCRYY      = $020c0042;
  KS_CAMERA_PIXEL_YCBCR_422_8_CBYCRY            = $02100043;
  KS_CAMERA_PIXEL_YCBCR_601_422_8_CBYCRY        = $02100044;
  KS_CAMERA_PIXEL_YCBCR_709_422_8_CBYCRY        = $02100045;

type
  //------ Context structures ------
  KSCameraRecvImageContext = packed record
    ctxType: int;
    hStream: KSHandle;
    hCamera: KSHandle;
  end;
  PKSCameraRecvImageContext = ^KSCameraRecvImageContext;

  KSCameraDroppedImageContext = packed record
    ctxType: int;
    hStream: KSHandle;
    hCamera: KSHandle;
    imageId: UInt64;
  end;
  PKSCameraDroppedImageContext = ^KSCameraDroppedImageContext;

  KSCameraErrorContext = packed record
    ctxType: int;
    hCamera: KSHandle;
    error: Error;
  end;
  PKSCameraErrorContext = ^KSCameraErrorContext;

  KSCameraAttachContext = packed record
    ctxType: int;
    hController: KSHandle;
    cameraInfo: KSCameraInfo;
  end;
  PKSCameraAttachContext = ^KSCameraAttachContext;

  KSCameraGevAttachContext = packed record
    ctxType: int;
    hAdapter: KSHandle;
    cameraInfo: KSCameraInfoGev;
  end;
  PKSCameraGevAttachContext = ^KSCameraGevAttachContext;

  KSCameraEventContext = packed record
    ctxType: int;
    hCamera: KSHandle;
  end;
  PKSCameraEventContext = ^KSCameraEventContext;

//------ Basic functions ------
function KS_enumCameras(hObject: KSHandle; index: int; pCameraInfo: PKSCameraInfo; flags: int):
  Error; stdcall;
function KS_execCameraCommand(hObject: KSHandle; command: int; address: uint; pData: Pointer; pLength: PInt; flags: int):
  Error; stdcall;
function KS_installCameraHandler(hHandle: KSHandle; eventType: int; hSignal: KSHandle; flags: int):
  Error; stdcall;

//------ Camera connection functions ------
function KS_openCamera(phCamera: PKSHandle; hObject: KSHandle; pHardwareId: PAnsiChar; flags: int):
  Error; stdcall;
function KS_openCameraEx(phCamera: PKSHandle; hObject: KSHandle; pCameraInfo: PKSCameraInfo; flags: int):
  Error; stdcall;
function KS_closeCamera(hCamera: KSHandle; flags: int):
  Error; stdcall;
function KS_readCameraMem(hCamera: KSHandle; var address: Int64; pData: Pointer; length: int; flags: int):
  Error; stdcall;
function KS_writeCameraMem(hCamera: KSHandle; var address: Int64; pData: Pointer; length: int; flags: int):
  Error; stdcall;
function KS_configCamera(hCamera: KSHandle; configCommand: int; pName: PAnsiChar; index: int; pData: Pointer; length: int; flags: int):
  Error; stdcall;
function KS_startCameraAcquisition(hCamera: KSHandle; acquisitionMode: int; flags: int):
  Error; stdcall;
function KS_stopCameraAcquisition(hCamera: KSHandle; flags: int):
  Error; stdcall;

//------ Stream functions ------
function KS_createCameraStream(phStream: PKSHandle; hCamera: KSHandle; channelNumber: int; numBuffers: int; bufferSize: int; flags: int):
  Error; stdcall;
function KS_closeCameraStream(hStream: KSHandle; flags: int):
  Error; stdcall;
function KS_recvCameraImage(hStream: KSHandle; ppData: PPointer; ppBlockInfo: PPKSCameraBlock; flags: int):
  Error; stdcall;
function KS_releaseCameraImage(hStream: KSHandle; pData: Pointer; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// PLC Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Error codes ------
  KSERROR_PLC_SCAN_ERROR                        = (KSERROR_CATEGORY_PLC+$00000000);
  KSERROR_PLC_TYPE_ERROR                        = (KSERROR_CATEGORY_PLC+$00010000);
  KSERROR_PLC_BACKEND                           = (KSERROR_CATEGORY_PLC+$00020000);
  KSERROR_PLC_LINKER_ERROR                      = (KSERROR_CATEGORY_PLC+$00030000);
  KSERROR_PLC_INVALID_CONFIG_NUMBER             = (KSERROR_CATEGORY_PLC+$00040000);
  KSERROR_PLC_INVALID_RESOURCE_NUMBER           = (KSERROR_CATEGORY_PLC+$00060000);
  KSERROR_PLC_INVALID_TASK_NUMBER               = (KSERROR_CATEGORY_PLC+$00070000);

  //------ PLC languages ------
  KS_PLC_BYTECODE                               = $00000000;
  KS_PLC_INSTRUCTION_LIST                       = $00000001;
  KS_PLC_STRUCTURED_TEXT                        = $00000002;

  //------ Flags ------
  KSF_USE_KITHARA_PRIORITY                      = $00002000;
  KSF_32BIT                                     = $00000400;
  KSF_64BIT                                     = $00000800;
  KSF_DEBUG                                     = $10000000;
  KSF_PLC_START                                 = $00004000;
  KSF_PLC_STOP                                  = $00008000;
  KSF_IGNORE_CASE                               = $00000010;

  //------ PLC commands ------
  KS_PLC_ABORT_COMPILE                          = $00000001;

  //------ Error levels ------
  KS_ERROR_LEVEL_IGNORE                         = $00000000;
  KS_ERROR_LEVEL_WARNING                        = $00000001;
  KS_ERROR_LEVEL_ERROR                          = $00000002;
  KS_ERROR_LEVEL_FATAL                          = $00000003;

  //------ PLC compiler errors ------
  KS_PLCERROR_NONE                              = $00000000;
  KS_PLCERROR_SYNTAX                            = $00000001;
  KS_PLCERROR_EXPECTED_ID                       = $00000002;
  KS_PLCERROR_EXPECTED_RESCOURCE_ID             = $00000003;
  KS_PLCERROR_EXPECTED_PROG_ID                  = $00000004;
  KS_PLCERROR_EXPECTED_FB_ID                    = $00000005;
  KS_PLCERROR_EXPECTED_FB_INSTANCE              = $00000006;
  KS_PLCERROR_EXPECTED_FUNCTION_ID              = $00000007;
  KS_PLCERROR_EXPECTED_SINGLE_ID                = $00000008;
  KS_PLCERROR_EXPECTED_INTEGER                  = $00000009;
  KS_PLCERROR_EXPECTED_STRUCT                   = $0000000a;
  KS_PLCERROR_EXPECTED_STMT                     = $0000000b;
  KS_PLCERROR_EXPECTED_OPERAND                  = $0000000c;
  KS_PLCERROR_EXPECTED_VAR                      = $0000000d;
  KS_PLCERROR_EXPECTED_TYPE                     = $0000000e;
  KS_PLCERROR_EXPECTED_STRING                   = $0000000f;
  KS_PLCERROR_EXPECTED_VALUE                    = $00000010;
  KS_PLCERROR_EXPECTED_NUMERAL                  = $00000011;
  KS_PLCERROR_EXPECTED_BOOL                     = $00000012;
  KS_PLCERROR_EXPECTED_CONST                    = $00000013;
  KS_PLCERROR_EXPECTED_READ_ONLY                = $00000014;
  KS_PLCERROR_EXPECTED_VAR_INPUT                = $00000015;
  KS_PLCERROR_EXPECTED_VAR_OUTPUT               = $00000016;
  KS_PLCERROR_EXPECTED_SIMPLE_TYPE              = $00000017;
  KS_PLCERROR_EXPECTED_TIME                     = $00000018;
  KS_PLCERROR_EXPECTED_GLOBAL                   = $00000019;
  KS_PLCERROR_EXPECTED_LABEL                    = $0000001a;
  KS_PLCERROR_EXPECTED_INCOMPLETE_DECL          = $0000001b;
  KS_PLCERROR_EXPECTED_VAR_CONFIG               = $0000001c;
  KS_PLCERROR_ID_DEFINED                        = $0000001d;
  KS_PLCERROR_ID_UNKNOWN                        = $0000001e;
  KS_PLCERROR_ID_CONST                          = $0000001f;
  KS_PLCERROR_ID_TOO_MANY                       = $00000020;
  KS_PLCERROR_UNKNOWN_LOCATION                  = $00000021;
  KS_PLCERROR_UNKNOWN_SIZE                      = $00000022;
  KS_PLCERROR_TYPE_MISMATCH                     = $00000023;
  KS_PLCERROR_INDEX_VIOLATION                   = $00000024;
  KS_PLCERROR_FORMAT_MISMATCH                   = $00000025;
  KS_PLCERROR_OVERFLOW                          = $00000026;
  KS_PLCERROR_DATA_LOSS                         = $00000027;
  KS_PLCERROR_NOT_SUPPORTED                     = $00000028;
  KS_PLCERROR_NEGATIVE_SIZE                     = $00000029;
  KS_PLCERROR_MISSING_ARRAY_ELEM_TYPE           = $0000002a;
  KS_PLCERROR_MISSING_RETURN_VALUE              = $0000002b;
  KS_PLCERROR_BAD_VAR_USE                       = $0000002c;
  KS_PLCERROR_BAD_DIRECT_VAR_INOUT              = $0000002d;
  KS_PLCERROR_BAD_LABEL                         = $0000002e;
  KS_PLCERROR_BAD_SUBRANGE                      = $0000002f;
  KS_PLCERROR_BAD_DATETIME                      = $00000030;
  KS_PLCERROR_BAD_ARRAY_SIZE                    = $00000031;
  KS_PLCERROR_BAD_CASE                          = $00000032;
  KS_PLCERROR_DIV_BY_ZERO                       = $00000033;
  KS_PLCERROR_AMBIGUOUS                         = $00000034;
  KS_PLCERROR_INTERNAL                          = $00000035;
  KS_PLCERROR_PARAMETER_MISMATCH                = $00000036;
  KS_PLCERROR_PARAMETER_TOO_MANY                = $00000037;
  KS_PLCERROR_PARAMETER_FEW                     = $00000038;
  KS_PLCERROR_INIT_MANY                         = $00000039;
  KS_PLCERROR_BAD_ACCESS                        = $0000003a;
  KS_PLCERROR_BAD_CONFIG                        = $0000003b;
  KS_PLCERROR_READ_ONLY                         = $0000003c;
  KS_PLCERROR_WRITE_ONLY                        = $0000003d;
  KS_PLCERROR_BAD_EXIT                          = $0000003e;
  KS_PLCERROR_BAD_RECURSION                     = $0000003f;
  KS_PLCERROR_BAD_PRIORITY                      = $00000040;
  KS_PLCERROR_BAD_FB_OPERATOR                   = $00000041;
  KS_PLCERROR_BAD_INIT                          = $00000042;
  KS_PLCERROR_BAD_CONTINUE                      = $00000043;
  KS_PLCERROR_DUPLICATE_CASE                    = $00000044;
  KS_PLCERROR_UNEXPECTED_TYPE                   = $00000045;
  KS_PLCERROR_CASE_MISSING                      = $00000046;
  KS_PLCERROR_DEAD_INSTRUCTION                  = $00000047;
  KS_PLCERROR_FOLD_EXPRESSION                   = $00000048;
  KS_PLCERROR_NOT_IMPLEMENTED                   = $00000049;
  KS_PLCERROR_BAD_CONNECTED_PARAM_VAR           = $0000004a;
  KS_PLCERROR_PARAMETER_MIX                     = $0000004b;
  KS_PLCERROR_EXPECTED_PROGRAM_DECL             = $0000004c;
  KS_PLCERROR_EXPECTED_INTERFACE                = $0000004d;
  KS_PLCERROR_EXPECTED_METHOD                   = $0000004e;
  KS_PLCERROR_ABSTRACT                          = $0000004f;
  KS_PLCERROR_FINAL                             = $00000050;
  KS_PLCERROR_MISSING_ABSTRACT_METHOD           = $00000051;
  KS_PLCERROR_ABSTRACT_INCOMPLETE               = $00000052;
  KS_PLCERROR_BAD_ACCESS_MODIFIER               = $00000053;
  KS_PLCERROR_BAD_METHOD_ACCESS                 = $00000054;
  KS_PLCERROR_ID_INPUT                          = $00000055;
  KS_PLCERROR_BAD_THIS                          = $00000056;
  KS_PLCERROR_MISSING_MODULE                    = $00000057;
  KS_PLCERROR_EXPECTED_DNV                      = $00000058;
  KS_PLCERROR_EXPECTED_DIRECT_VAR               = $00000059;
  KS_PLCERROR_BAD_VAR_DECL                      = $0000005a;
  KS_PLCERROR_HAS_ABSTRACT_BODY                 = $0000005b;
  KS_PLCERROR_BAD_MEMBER_ACCESS                 = $0000005c;
  KS_PLCERROR_EXPECTED_FORMAL_CALL              = $0000005d;
  KS_PLCERROR_EXPECTED_INTERFACE_VAR            = $0000005e;

  //------ PLC runtime errors ------
  KS_PLCERROR_DIVIDED_BY_ZERO                   = $0000012c;
  KS_PLCERROR_ARRAY_INDEX_VIOLATION             = $0000012d;
  KS_PLCERROR_MEMORY_ACCESS_VIOLATION           = $0000012e;
  KS_PLCERROR_IO_VARIABLE_ACCESS                = $0000012f;
  KS_PLCERROR_BAD_VAR_EXTERNAL                  = $00000130;
  KS_PLCERROR_BAD_SUB_RANGE                     = $00000131;

  //------ PLC events ------
  KS_PLC_COMPILE_ERROR                          = (PLC_BASE+$00000001);
  KS_PLC_COMPILE_FINISHED                       = (PLC_BASE+$00000002);
  KS_PLC_RUNTIME_ERROR                          = (PLC_BASE+$00000003);
  KS_PLC_CONFIG_RESSOURCE                       = (PLC_BASE+$00000004);

type
  //------ Context structures ------
  PlcErrorMsgContext = packed record
    ctxType: int;
    hCompiler: KSHandle;
    line: int;
    column: int;
    errorCode: int;
    errorLevel: int;
    flags: int;
    pMsg: array [0..199] of AnsiChar;
  end;
  PPlcErrorMsgContext = ^PlcErrorMsgContext;

  PlcCompileContext = packed record
    ctxType: int;
    hCompiler: KSHandle;
    ksError: int;
    id: int;
  end;
  PPlcCompileContext = ^PlcCompileContext;

  PlcRuntimeErrorContext = packed record
    ctxType: int;
    hPlc: KSHandle;
    line: int;
    column: int;
    errorCode: int;
    errorLevel: int;
    flags: int;
    ksError: int;
  end;
  PPlcRuntimeErrorContext = ^PlcRuntimeErrorContext;

  PlcConfigContext = packed record
    ctxType: int;
    hPlc: KSHandle;
    configIndex: int;
    resourceIndex: int;
    flags: int;
  end;
  PPlcConfigContext = ^PlcConfigContext;

//------ PLC compiler functions ------
function KS_createPlcCompiler(phCompiler: PKSHandle; flags: int):
  Error; stdcall;
function KS_closePlcCompiler(hCompiler: KSHandle; flags: int):
  Error; stdcall;
function KS_compilePlcFile(hCompiler: KSHandle; sourcePath: PAnsiChar; sourceLanguage: int; destinationPath: PAnsiChar; flags: int):
  Error; stdcall;
function KS_compilePlcBuffer(hCompiler: KSHandle; pSourceBuffer: Pointer; sourceLength: int; sourceLang: int; pDestBuffer: Pointer; pDestLength: PInt; flags: int):
  Error; stdcall;

//------ PLC functions ------
function KS_createPlcFromFile(phPlc: PKSHandle; binaryPath: PAnsiChar; flags: int):
  Error; stdcall;
function KS_createPlcFromBuffer(phPlc: PKSHandle; pBinaryBuffer: Pointer; bufferLength: int; flags: int):
  Error; stdcall;
function KS_closePlc(hPlc: KSHandle; flags: int):
  Error; stdcall;
function KS_startPlc(hPlc: KSHandle; configIndex: int; var start: Int64; flags: int):
  Error; stdcall;
function KS_stopPlc(hPlc: KSHandle; flags: int):
  Error; stdcall;

//------ Information functions ------
function KS_enumPlcConfigurations(hPlc: KSHandle; configIndex: int; pNameBuf: PAnsiChar; flags: int):
  Error; stdcall;
function KS_enumPlcResources(hPlc: KSHandle; configIndex: int; resourceIndex: int; pNameBuf: PAnsiChar; flags: int):
  Error; stdcall;
function KS_enumPlcTasks(hPlc: KSHandle; configIndex: int; resourceIndex: int; taskIndex: int; pNameBuf: PAnsiChar; flags: int):
  Error; stdcall;

//------ Generic functions ------
function KS_execPlcCommand(hPlc: KSHandle; command: int; pData: Pointer; flags: int):
  Error; stdcall;
function KS_installPlcHandler(hPlc: KSHandle; eventCode: int; hSignal: KSHandle; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// Vision Module
//--------------------------------------------------------------------------------------------------------------

//------ Basic functions ------
function KS_loadVisionKernel(phKernel: PKSHandle; dllName: PAnsiChar; initProcName: PAnsiChar; pArgs: Pointer; flags: int):
  Error; stdcall;

//------ Control functions ------
function KS_installVisionHandler(eventType: int; hSignal: KSHandle; flags: int):
  Error; stdcall;

type
  //------ Context structures ------
  KSVisionTaskContext = packed record
    ctxType: int;
    hTask: KSHandle;
  end;
  PKSVisionTaskContext = ^KSVisionTaskContext;

const
  //------ Handler events ------
  KS_VISION_TASK_CREATED                        = $00000001;
  KS_VISION_TASK_CLOSED                         = $00000002;

//--------------------------------------------------------------------------------------------------------------
// SigProc Module
//--------------------------------------------------------------------------------------------------------------

const
  //------ Filter types ------
  KS_FIR_LOWPASS_HAMMING                        = $00000000;
  KS_FIR_LOWPASS_HANNING                        = $00000001;
  KS_FIR_LOWPASS_RECTANGULAR                    = $00000002;
  KS_FIR_HIGHPASS_HAMMING                       = $00000003;
  KS_FIR_HIGHPASS_HANNING                       = $00000004;
  KS_FIR_HIGHPASS_RECTANGULAR                   = $00000005;
  KS_FIR_BANDPASS_HAMMING                       = $00000006;
  KS_FIR_BANDPASS_HANNING                       = $00000007;
  KS_FIR_BANDPASS_RECTANGULAR                   = $00000008;
  KS_FIR_BANDSTOP_HAMMING                       = $00000009;
  KS_FIR_BANDSTOP_HANNING                       = $0000000a;
  KS_FIR_BANDSTOP_RECTANGULAR                   = $0000000b;
  KS_IIR_LOWPASS_CHEBYSHEV_I                    = $00000014;
  KS_IIR_LOWPASS_BUTTERWORTH                    = $00000015;
  KS_IIR_LOWPASS_CHEBYSHEV_II                   = $00000016;
  KS_IIR_HIGHPASS_CHEBYSHEV_I                   = $00000017;
  KS_IIR_HIGHPASS_BUTTERWORTH                   = $00000018;
  KS_IIR_HIGHPASS_CHEBYSHEV_II                  = $00000019;
  KS_IIR_BANDPASS_CHEBYSHEV_I                   = $0000001a;
  KS_IIR_BANDPASS_BUTTERWORTH                   = $0000001b;
  KS_IIR_BANDPASS_CHEBYSHEV_II                  = $0000001c;
  KS_IIR_BANDSTOP_CHEBYSHEV_I                   = $0000001d;
  KS_IIR_BANDSTOP_BUTTERWORTH                   = $0000001e;
  KS_IIR_BANDSTOP_CHEBYSHEV_II                  = $0000001f;
  KS_IIR_INDIVIDUAL                             = $00000020;

  //------ Standard ripples ------
  KS_CHEBYSHEV_3DB                              = 0.707106781;
  KS_CHEBYSHEV_1DB                              = 0.891250938;

  //------ Some filter specified errorcodes ------
  KSERROR_SP_SHANNON_THEOREM_MISMATCH           = (KSERROR_CATEGORY_SPECIAL+$00020000);

type
  //------ Paramters structures ------
  KSSignalFilterParams = packed record
    structSize: int;
    filterType: int;
    order: int;
    maxFrequency: Double;
    frequency: Double;
    sampleRate: Double;
    width: Double;
    delta: Double;
  end;
  PKSSignalFilterParams = ^KSSignalFilterParams;

  KSIndividualSignalFilterParams = packed record
    structSize: int;
    pXCoeffs: PDouble;
    xOrder: int;
    pYCoeffs: PDouble;
    yOrder: int;
    maxFrequency: Double;
    sampleRate: Double;
  end;
  PKSIndividualSignalFilterParams = ^KSIndividualSignalFilterParams;

  KSPIDControllerParams = packed record
    structSize: int;
    proportional: Double;
    integral: Double;
    derivative: Double;
    setpoint: Double;
    sampleRate: Double;
  end;
  PKSPIDControllerParams = ^KSPIDControllerParams;

//------ Digital Filter functions ------
function KS_createSignalFilter(phFilter: PKSHandle; pParams: PKSSignalFilterParams; flags: int):
  Error; stdcall;
function KS_createIndividualSignalFilter(phFilter: PKSHandle; pParams: PKSIndividualSignalFilterParams; flags: int):
  Error; stdcall;
function KS_getFrequencyResponse(hFilter: KSHandle; frequency: Double; pAmplitude: PDouble; pPhase: PDouble; flags: int):
  Error; stdcall;
function KS_scaleSignalFilter(hFilter: KSHandle; frequency: Double; value: Double; flags: int):
  Error; stdcall;
function KS_resetSignalFilter(hFilter: KSHandle; flags: int):
  Error; stdcall;

//------ PID Controller functions ------
function KS_createPIDController(phController: PKSHandle; pParams: PKSPIDControllerParams; flags: int):
  Error; stdcall;
function KS_changeSetpoint(hController: KSHandle; setpoint: Double; flags: int):
  Error; stdcall;

//------ General signal processing functions ------
function KS_removeSignalProcessor(hSigProc: KSHandle; flags: int):
  Error; stdcall;
function KS_processSignal(hSigProc: KSHandle; input: Double; pOutput: PDouble; flags: int):
  Error; stdcall;

//--------------------------------------------------------------------------------------------------------------
// implementation
//--------------------------------------------------------------------------------------------------------------

implementation

uses Windows;

//------ Helper functions ------
function KSERROR_CODE(error: Error):  Error;
begin
  KSERROR_CODE := error and $3fff0000;
end;

function mk16(lo, hi : byte):
  word;
begin
  mk16 := word(lo or (word(hi) shl 8));
end;

function mk32(lo, hi : word):
  uint;
begin
  mk32 := uint(lo or (uint(hi) shl 16));
end;

function KS_htons(val : word):
  word;
begin
  KS_htons := mk16(byte(val shr 8), byte(val));
end;

function KS_htonl(val : uint):
  uint;
begin
  KS_htonl := mk32(KS_htons(word(val shr 16)), KS_htons(word(val)));
end;

function KS_ntohs(val : word):
  word;
begin
  KS_ntohs := KS_htons(val);
end;

function KS_ntohl(val : uint):
  uint;
begin
  KS_ntohl := KS_htonl(val);
end;

procedure KS_makeIPv4(pAddr : PUint; b3, b2, b1, b0 : byte);
begin
  pAddr^ := (b0 shl 24) or (b1 shl 16) or (b2 shl 8) or b3;
end;

procedure _registerKernelAddress(pName : PAnsiChar; pProc : Pointer); forward;
procedure _initModules(); forward;

var
  _hKrtsDemoDll : Win32Handle = nil;

const
  _dllToLoad : PAnsiChar = 'KrtsDemo.dll';

//------ KS_openDriver ------
type
  TKS_openDriver = function (customerNumber : PAnsiChar): Error; stdcall;
  PKS_openDriver = ^TKS_openDriver;
var
  FKS_openDriver: TKS_openDriver;

function KS_openDriver(customerNumber : PAnsiChar): Error; stdcall;
var
  ksError : Error;
begin
  // first load the driver library
  if _hKrtsDemoDll = nil then
    _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));

  if _hKrtsDemoDll = nil then
  begin
    KS_openDriver := KSERROR_CANNOT_FIND_LIBRARY or $1000 or GetLastError();
    exit;
  end;

  if @FKS_openDriver = nil then
  begin
    // now query the procedure address
    @FKS_openDriver := PKS_openDriver(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openDriver'));
    if @FKS_openDriver = nil then
    begin
      KS_openDriver := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  // if the function call fails, we close the driver
  ksError := FKS_openDriver(customerNumber);
  if ksError <> KS_OK then
  begin
    FreeLibrary(HMODULE(_hKrtsDemoDll));
    _hKrtsDemoDll := nil;
    KS_openDriver := ksError;
    exit;
  end;

  // now we register some functions for kernel execution
  _initModules();

  KS_openDriver := KS_OK;
end;

//--------------------------------------------------------------------------------------------------------------
// Base Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_startKernel ------
type
  TKS_startKernel = function : Error; stdcall;
  PKS_startKernel = ^TKS_startKernel;
var
  FKS_startKernel: TKS_startKernel;
        
function KS_startKernel: Error; stdcall;
begin
  if @FKS_startKernel = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_startKernel := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_startKernel := PKS_startKernel(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_startKernel'));
    if @FKS_startKernel = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_startKernel not found in DLL');
      KS_startKernel := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_startKernel := FKS_startKernel;
end;

//------ KS_stopKernel ------
type
  TKS_stopKernel = function : Error; stdcall;
  PKS_stopKernel = ^TKS_stopKernel;
var
  FKS_stopKernel: TKS_stopKernel;
        
function KS_stopKernel: Error; stdcall;
begin
  if @FKS_stopKernel = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_stopKernel := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_stopKernel := PKS_stopKernel(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_stopKernel'));
    if @FKS_stopKernel = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_stopKernel not found in DLL');
      KS_stopKernel := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_stopKernel := FKS_stopKernel;
end;

//------ KS_resetKernel ------
type
  TKS_resetKernel = function (flags: int): Error; stdcall;
  PKS_resetKernel = ^TKS_resetKernel;
var
  FKS_resetKernel: TKS_resetKernel;
        
function KS_resetKernel(flags: int): Error; stdcall;
begin
  if @FKS_resetKernel = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_resetKernel := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_resetKernel := PKS_resetKernel(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_resetKernel'));
    if @FKS_resetKernel = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_resetKernel not found in DLL');
      KS_resetKernel := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_resetKernel := FKS_resetKernel(flags);
end;

//------ KS_closeDriver ------
type
  TKS_closeDriver = function : Error; stdcall;
  PKS_closeDriver = ^TKS_closeDriver;
var
  FKS_closeDriver: TKS_closeDriver;
        
function KS_closeDriver: Error; stdcall;
begin
  if @FKS_closeDriver = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeDriver := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeDriver := PKS_closeDriver(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeDriver'));
    if @FKS_closeDriver = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeDriver not found in DLL');
      KS_closeDriver := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeDriver := FKS_closeDriver;
  // WE DON'T RELEASE THE DRIVER LIBRARY, BECAUSE ALL PROCEDURE ADDRESSES WOULD BE INVALID!
  // FreeLibrary(HMODULE(_hKrtsDemoDll));
  // _hKrtsDemoDll := nil;
end;

//------ KS_getDriverVersion ------
type
  TKS_getDriverVersion = function (pVersion: PUInt): Error; stdcall;
  PKS_getDriverVersion = ^TKS_getDriverVersion;
var
  FKS_getDriverVersion: TKS_getDriverVersion;
        
function KS_getDriverVersion(pVersion: PUInt): Error; stdcall;
begin
  if @FKS_getDriverVersion = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getDriverVersion := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getDriverVersion := PKS_getDriverVersion(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getDriverVersion'));
    if @FKS_getDriverVersion = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getDriverVersion not found in DLL');
      KS_getDriverVersion := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getDriverVersion := FKS_getDriverVersion(pVersion);
end;

//------ KS_getErrorString ------
type
  TKS_getErrorString = function (code: Error; var msg: PAnsiChar; language: uint): Error; stdcall;
  PKS_getErrorString = ^TKS_getErrorString;
var
  FKS_getErrorString: TKS_getErrorString;
        
function KS_getErrorString(code: Error; var msg: PAnsiChar; language: uint): Error; stdcall;
begin
  if @FKS_getErrorString = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getErrorString := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getErrorString := PKS_getErrorString(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getErrorString'));
    if @FKS_getErrorString = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getErrorString not found in DLL');
      KS_getErrorString := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getErrorString := FKS_getErrorString(code, msg, language);
end;

//------ KS_addErrorString ------
type
  TKS_addErrorString = function (code: Error; msg: PAnsiChar; language: uint): Error; stdcall;
  PKS_addErrorString = ^TKS_addErrorString;
var
  FKS_addErrorString: TKS_addErrorString;
        
function KS_addErrorString(code: Error; msg: PAnsiChar; language: uint): Error; stdcall;
begin
  if @FKS_addErrorString = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_addErrorString := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_addErrorString := PKS_addErrorString(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_addErrorString'));
    if @FKS_addErrorString = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_addErrorString not found in DLL');
      KS_addErrorString := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_addErrorString := FKS_addErrorString(code, msg, language);
end;

//------ KS_getDriverConfig ------
type
  TKS_getDriverConfig = function (module: PAnsiChar; configType: int; pData: Pointer): Error; stdcall;
  PKS_getDriverConfig = ^TKS_getDriverConfig;
var
  FKS_getDriverConfig: TKS_getDriverConfig;
        
function KS_getDriverConfig(module: PAnsiChar; configType: int; pData: Pointer): Error; stdcall;
begin
  if @FKS_getDriverConfig = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getDriverConfig := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getDriverConfig := PKS_getDriverConfig(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getDriverConfig'));
    if @FKS_getDriverConfig = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getDriverConfig not found in DLL');
      KS_getDriverConfig := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getDriverConfig := FKS_getDriverConfig(module, configType, pData);
end;

//------ KS_setDriverConfig ------
type
  TKS_setDriverConfig = function (module: PAnsiChar; configType: int; pData: Pointer): Error; stdcall;
  PKS_setDriverConfig = ^TKS_setDriverConfig;
var
  FKS_setDriverConfig: TKS_setDriverConfig;
        
function KS_setDriverConfig(module: PAnsiChar; configType: int; pData: Pointer): Error; stdcall;
begin
  if @FKS_setDriverConfig = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setDriverConfig := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setDriverConfig := PKS_setDriverConfig(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setDriverConfig'));
    if @FKS_setDriverConfig = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setDriverConfig not found in DLL');
      KS_setDriverConfig := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setDriverConfig := FKS_setDriverConfig(module, configType, pData);
end;

//------ KS_getSystemInformation ------
type
  TKS_getSystemInformation = function (pSystemInfo: PKSSystemInformation; flags: int): Error; stdcall;
  PKS_getSystemInformation = ^TKS_getSystemInformation;
var
  FKS_getSystemInformation: TKS_getSystemInformation;
        
function KS_getSystemInformation(pSystemInfo: PKSSystemInformation; flags: int): Error; stdcall;
begin
  if @FKS_getSystemInformation = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getSystemInformation := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getSystemInformation := PKS_getSystemInformation(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getSystemInformation'));
    if @FKS_getSystemInformation = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getSystemInformation not found in DLL');
      KS_getSystemInformation := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getSystemInformation := FKS_getSystemInformation(pSystemInfo, flags);
end;

//------ KS_getProcessorInformation ------
type
  TKS_getProcessorInformation = function (index: int; pProcessorInfo: PKSProcessorInformation; flags: int): Error; stdcall;
  PKS_getProcessorInformation = ^TKS_getProcessorInformation;
var
  FKS_getProcessorInformation: TKS_getProcessorInformation;
        
function KS_getProcessorInformation(index: int; pProcessorInfo: PKSProcessorInformation; flags: int): Error; stdcall;
begin
  if @FKS_getProcessorInformation = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getProcessorInformation := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getProcessorInformation := PKS_getProcessorInformation(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getProcessorInformation'));
    if @FKS_getProcessorInformation = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getProcessorInformation not found in DLL');
      KS_getProcessorInformation := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getProcessorInformation := FKS_getProcessorInformation(index, pProcessorInfo, flags);
end;

//------ KS_closeHandle ------
type
  TKS_closeHandle = function (handle: KSHandle; flags: int): Error; stdcall;
  PKS_closeHandle = ^TKS_closeHandle;
var
  FKS_closeHandle: TKS_closeHandle;
        
function KS_closeHandle(handle: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeHandle = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeHandle := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeHandle := PKS_closeHandle(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeHandle'));
    if @FKS_closeHandle = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeHandle not found in DLL');
      KS_closeHandle := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeHandle := FKS_closeHandle(handle, flags);
end;

//------ KS_showMessage ------
type
  TKS_showMessage = function (ksError: Error; msg: PAnsiChar): Error; stdcall;
  PKS_showMessage = ^TKS_showMessage;
var
  FKS_showMessage: TKS_showMessage;
        
function KS_showMessage(ksError: Error; msg: PAnsiChar): Error; stdcall;
begin
  if @FKS_showMessage = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_showMessage := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_showMessage := PKS_showMessage(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_showMessage'));
    if @FKS_showMessage = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_showMessage not found in DLL');
      KS_showMessage := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_showMessage := FKS_showMessage(ksError, msg);
end;

//------ KS_logMessage ------
type
  TKS_logMessage = function (msgType: int; msgText: PAnsiChar): Error; stdcall;
  PKS_logMessage = ^TKS_logMessage;
var
  FKS_logMessage: TKS_logMessage;
        
function KS_logMessage(msgType: int; msgText: PAnsiChar): Error; stdcall;
begin
  if @FKS_logMessage = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_logMessage := PKS_logMessage(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_logMessage'));
    if @FKS_logMessage = nil then
    begin
      KS_logMessage := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_logMessage := FKS_logMessage(msgType, msgText);
end;

//------ KS_bufMessage ------
type
  TKS_bufMessage = function (msgType: int; pBuffer: Pointer; length: int; msgText: PAnsiChar): Error; stdcall;
  PKS_bufMessage = ^TKS_bufMessage;
var
  FKS_bufMessage: TKS_bufMessage;
        
function KS_bufMessage(msgType: int; pBuffer: Pointer; length: int; msgText: PAnsiChar): Error; stdcall;
begin
  if @FKS_bufMessage = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_bufMessage := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_bufMessage := PKS_bufMessage(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_bufMessage'));
    if @FKS_bufMessage = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_bufMessage not found in DLL');
      KS_bufMessage := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_bufMessage := FKS_bufMessage(msgType, pBuffer, length, msgText);
end;

//------ KS_dbgMessage ------
type
  TKS_dbgMessage = function (fileName: PAnsiChar; line: int; msgText: PAnsiChar): Error; stdcall;
  PKS_dbgMessage = ^TKS_dbgMessage;
var
  FKS_dbgMessage: TKS_dbgMessage;
        
function KS_dbgMessage(fileName: PAnsiChar; line: int; msgText: PAnsiChar): Error; stdcall;
begin
  if @FKS_dbgMessage = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_dbgMessage := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_dbgMessage := PKS_dbgMessage(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_dbgMessage'));
    if @FKS_dbgMessage = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_dbgMessage not found in DLL');
      KS_dbgMessage := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_dbgMessage := FKS_dbgMessage(fileName, line, msgText);
end;

//------ KS_beep ------
type
  TKS_beep = function (freq: int; duration: int): Error; stdcall;
  PKS_beep = ^TKS_beep;
var
  FKS_beep: TKS_beep;
        
function KS_beep(freq: int; duration: int): Error; stdcall;
begin
  if @FKS_beep = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_beep := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_beep := PKS_beep(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_beep'));
    if @FKS_beep = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_beep not found in DLL');
      KS_beep := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_beep := FKS_beep(freq, duration);
end;

//------ KS_throwException ------
type
  TKS_throwException = function (error: int; pText: PAnsiChar; pFile: PAnsiChar; line: int): Error; stdcall;
  PKS_throwException = ^TKS_throwException;
var
  FKS_throwException: TKS_throwException;
        
function KS_throwException(error: int; pText: PAnsiChar; pFile: PAnsiChar; line: int): Error; stdcall;
begin
  if @FKS_throwException = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_throwException := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_throwException := PKS_throwException(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_throwException'));
    if @FKS_throwException = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_throwException not found in DLL');
      KS_throwException := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_throwException := FKS_throwException(error, pText, pFile, line);
end;

//------ KS_enumDevices ------
type
  TKS_enumDevices = function (deviceType: PAnsiChar; index: int; pDeviceNameBuf: PAnsiChar; flags: int): Error; stdcall;
  PKS_enumDevices = ^TKS_enumDevices;
var
  FKS_enumDevices: TKS_enumDevices;
        
function KS_enumDevices(deviceType: PAnsiChar; index: int; pDeviceNameBuf: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_enumDevices = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumDevices := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumDevices := PKS_enumDevices(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumDevices'));
    if @FKS_enumDevices = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumDevices not found in DLL');
      KS_enumDevices := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumDevices := FKS_enumDevices(deviceType, index, pDeviceNameBuf, flags);
end;

//------ KS_enumDevicesEx ------
type
  TKS_enumDevicesEx = function (className: PAnsiChar; busType: PAnsiChar; hEnumerator: KSHandle; index: int; pDeviceNameBuf: PAnsiChar; flags: int): Error; stdcall;
  PKS_enumDevicesEx = ^TKS_enumDevicesEx;
var
  FKS_enumDevicesEx: TKS_enumDevicesEx;
        
function KS_enumDevicesEx(className: PAnsiChar; busType: PAnsiChar; hEnumerator: KSHandle; index: int; pDeviceNameBuf: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_enumDevicesEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumDevicesEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumDevicesEx := PKS_enumDevicesEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumDevicesEx'));
    if @FKS_enumDevicesEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumDevicesEx not found in DLL');
      KS_enumDevicesEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumDevicesEx := FKS_enumDevicesEx(className, busType, hEnumerator, index, pDeviceNameBuf, flags);
end;

//------ KS_getDevices ------
type
  TKS_getDevices = function (className: PAnsiChar; busType: PAnsiChar; hEnumerator: KSHandle; pCount: PInt; pBuf: PPointer; size: int; flags: int): Error; stdcall;
  PKS_getDevices = ^TKS_getDevices;
var
  FKS_getDevices: TKS_getDevices;
        
function KS_getDevices(className: PAnsiChar; busType: PAnsiChar; hEnumerator: KSHandle; pCount: PInt; pBuf: PPointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_getDevices = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getDevices := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getDevices := PKS_getDevices(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getDevices'));
    if @FKS_getDevices = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getDevices not found in DLL');
      KS_getDevices := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getDevices := FKS_getDevices(className, busType, hEnumerator, pCount, pBuf, size, flags);
end;

//------ KS_getDeviceInfo ------
type
  TKS_getDeviceInfo = function (deviceName: PAnsiChar; pDeviceInfo: PKSDeviceInfo; flags: int): Error; stdcall;
  PKS_getDeviceInfo = ^TKS_getDeviceInfo;
var
  FKS_getDeviceInfo: TKS_getDeviceInfo;
        
function KS_getDeviceInfo(deviceName: PAnsiChar; pDeviceInfo: PKSDeviceInfo; flags: int): Error; stdcall;
begin
  if @FKS_getDeviceInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getDeviceInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getDeviceInfo := PKS_getDeviceInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getDeviceInfo'));
    if @FKS_getDeviceInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getDeviceInfo not found in DLL');
      KS_getDeviceInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getDeviceInfo := FKS_getDeviceInfo(deviceName, pDeviceInfo, flags);
end;

//------ KS_updateDriver ------
type
  TKS_updateDriver = function (deviceName: PAnsiChar; fileName: PAnsiChar; flags: int): Error; stdcall;
  PKS_updateDriver = ^TKS_updateDriver;
var
  FKS_updateDriver: TKS_updateDriver;
        
function KS_updateDriver(deviceName: PAnsiChar; fileName: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_updateDriver = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_updateDriver := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_updateDriver := PKS_updateDriver(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_updateDriver'));
    if @FKS_updateDriver = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_updateDriver not found in DLL');
      KS_updateDriver := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_updateDriver := FKS_updateDriver(deviceName, fileName, flags);
end;

//------ KS_createThread ------
type
  TKS_createThread = function (procName: Pointer; pArgs: Pointer; phThread: PWin32Handle): Error; stdcall;
  PKS_createThread = ^TKS_createThread;
var
  FKS_createThread: TKS_createThread;
        
function KS_createThread(procName: Pointer; pArgs: Pointer; phThread: PWin32Handle): Error; stdcall;
begin
  if @FKS_createThread = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createThread := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createThread := PKS_createThread(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createThread'));
    if @FKS_createThread = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createThread not found in DLL');
      KS_createThread := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createThread := FKS_createThread(procName, pArgs, phThread);
end;

//------ KS_removeThread ------
type
  TKS_removeThread = function : Error; stdcall;
  PKS_removeThread = ^TKS_removeThread;
var
  FKS_removeThread: TKS_removeThread;
        
function KS_removeThread: Error; stdcall;
begin
  if @FKS_removeThread = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeThread := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeThread := PKS_removeThread(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeThread'));
    if @FKS_removeThread = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeThread not found in DLL');
      KS_removeThread := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeThread := FKS_removeThread;
end;

//------ KS_setThreadPrio ------
type
  TKS_setThreadPrio = function (prio: int): Error; stdcall;
  PKS_setThreadPrio = ^TKS_setThreadPrio;
var
  FKS_setThreadPrio: TKS_setThreadPrio;
        
function KS_setThreadPrio(prio: int): Error; stdcall;
begin
  if @FKS_setThreadPrio = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setThreadPrio := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setThreadPrio := PKS_setThreadPrio(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setThreadPrio'));
    if @FKS_setThreadPrio = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setThreadPrio not found in DLL');
      KS_setThreadPrio := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setThreadPrio := FKS_setThreadPrio(prio);
end;

//------ KS_getThreadPrio ------
type
  TKS_getThreadPrio = function (pPrio: PUInt): Error; stdcall;
  PKS_getThreadPrio = ^TKS_getThreadPrio;
var
  FKS_getThreadPrio: TKS_getThreadPrio;
        
function KS_getThreadPrio(pPrio: PUInt): Error; stdcall;
begin
  if @FKS_getThreadPrio = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getThreadPrio := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getThreadPrio := PKS_getThreadPrio(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getThreadPrio'));
    if @FKS_getThreadPrio = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getThreadPrio not found in DLL');
      KS_getThreadPrio := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getThreadPrio := FKS_getThreadPrio(pPrio);
end;

//------ KS_createSharedMem ------
type
  TKS_createSharedMem = function (ppAppPtr: PPointer; ppSysPtr: PPointer; name: PAnsiChar; size: int; flags: int): Error; stdcall;
  PKS_createSharedMem = ^TKS_createSharedMem;
var
  FKS_createSharedMem: TKS_createSharedMem;
        
function KS_createSharedMem(ppAppPtr: PPointer; ppSysPtr: PPointer; name: PAnsiChar; size: int; flags: int): Error; stdcall;
begin
  if @FKS_createSharedMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createSharedMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createSharedMem := PKS_createSharedMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createSharedMem'));
    if @FKS_createSharedMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createSharedMem not found in DLL');
      KS_createSharedMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createSharedMem := FKS_createSharedMem(ppAppPtr, ppSysPtr, name, size, flags);
end;

//------ KS_freeSharedMem ------
type
  TKS_freeSharedMem = function (pAppPtr: Pointer): Error; stdcall;
  PKS_freeSharedMem = ^TKS_freeSharedMem;
var
  FKS_freeSharedMem: TKS_freeSharedMem;
        
function KS_freeSharedMem(pAppPtr: Pointer): Error; stdcall;
begin
  if @FKS_freeSharedMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_freeSharedMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_freeSharedMem := PKS_freeSharedMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_freeSharedMem'));
    if @FKS_freeSharedMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_freeSharedMem not found in DLL');
      KS_freeSharedMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_freeSharedMem := FKS_freeSharedMem(pAppPtr);
end;

//------ KS_getSharedMem ------
type
  TKS_getSharedMem = function (ppPtr: PPointer; name: PAnsiChar): Error; stdcall;
  PKS_getSharedMem = ^TKS_getSharedMem;
var
  FKS_getSharedMem: TKS_getSharedMem;
        
function KS_getSharedMem(ppPtr: PPointer; name: PAnsiChar): Error; stdcall;
begin
  if @FKS_getSharedMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getSharedMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getSharedMem := PKS_getSharedMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getSharedMem'));
    if @FKS_getSharedMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getSharedMem not found in DLL');
      KS_getSharedMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getSharedMem := FKS_getSharedMem(ppPtr, name);
end;

//------ KS_readMem ------
type
  TKS_readMem = function (pBuffer: Pointer; pAppPtr: Pointer; size: int; offset: int): Error; stdcall;
  PKS_readMem = ^TKS_readMem;
var
  FKS_readMem: TKS_readMem;
        
function KS_readMem(pBuffer: Pointer; pAppPtr: Pointer; size: int; offset: int): Error; stdcall;
begin
  if @FKS_readMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readMem := PKS_readMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readMem'));
    if @FKS_readMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readMem not found in DLL');
      KS_readMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readMem := FKS_readMem(pBuffer, pAppPtr, size, offset);
end;

//------ KS_writeMem ------
type
  TKS_writeMem = function (pBuffer: Pointer; pAppPtr: Pointer; size: int; offset: int): Error; stdcall;
  PKS_writeMem = ^TKS_writeMem;
var
  FKS_writeMem: TKS_writeMem;
        
function KS_writeMem(pBuffer: Pointer; pAppPtr: Pointer; size: int; offset: int): Error; stdcall;
begin
  if @FKS_writeMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_writeMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_writeMem := PKS_writeMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_writeMem'));
    if @FKS_writeMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_writeMem not found in DLL');
      KS_writeMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_writeMem := FKS_writeMem(pBuffer, pAppPtr, size, offset);
end;

//------ KS_createSharedMemEx ------
type
  TKS_createSharedMemEx = function (phHandle: PKSHandle; name: PAnsiChar; size: int; flags: int): Error; stdcall;
  PKS_createSharedMemEx = ^TKS_createSharedMemEx;
var
  FKS_createSharedMemEx: TKS_createSharedMemEx;
        
function KS_createSharedMemEx(phHandle: PKSHandle; name: PAnsiChar; size: int; flags: int): Error; stdcall;
begin
  if @FKS_createSharedMemEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createSharedMemEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createSharedMemEx := PKS_createSharedMemEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createSharedMemEx'));
    if @FKS_createSharedMemEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createSharedMemEx not found in DLL');
      KS_createSharedMemEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createSharedMemEx := FKS_createSharedMemEx(phHandle, name, size, flags);
end;

//------ KS_freeSharedMemEx ------
type
  TKS_freeSharedMemEx = function (hHandle: KSHandle; flags: int): Error; stdcall;
  PKS_freeSharedMemEx = ^TKS_freeSharedMemEx;
var
  FKS_freeSharedMemEx: TKS_freeSharedMemEx;
        
function KS_freeSharedMemEx(hHandle: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_freeSharedMemEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_freeSharedMemEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_freeSharedMemEx := PKS_freeSharedMemEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_freeSharedMemEx'));
    if @FKS_freeSharedMemEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_freeSharedMemEx not found in DLL');
      KS_freeSharedMemEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_freeSharedMemEx := FKS_freeSharedMemEx(hHandle, flags);
end;

//------ KS_getSharedMemEx ------
type
  TKS_getSharedMemEx = function (hHandle: KSHandle; pPtr: PPointer; flags: int): Error; stdcall;
  PKS_getSharedMemEx = ^TKS_getSharedMemEx;
var
  FKS_getSharedMemEx: TKS_getSharedMemEx;
        
function KS_getSharedMemEx(hHandle: KSHandle; pPtr: PPointer; flags: int): Error; stdcall;
begin
  if @FKS_getSharedMemEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getSharedMemEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getSharedMemEx := PKS_getSharedMemEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getSharedMemEx'));
    if @FKS_getSharedMemEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getSharedMemEx not found in DLL');
      KS_getSharedMemEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getSharedMemEx := FKS_getSharedMemEx(hHandle, pPtr, flags);
end;

//------ KS_createEvent ------
type
  TKS_createEvent = function (phEvent: PKSHandle; name: PAnsiChar; flags: int): Error; stdcall;
  PKS_createEvent = ^TKS_createEvent;
var
  FKS_createEvent: TKS_createEvent;
        
function KS_createEvent(phEvent: PKSHandle; name: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_createEvent = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createEvent := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createEvent := PKS_createEvent(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createEvent'));
    if @FKS_createEvent = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createEvent not found in DLL');
      KS_createEvent := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createEvent := FKS_createEvent(phEvent, name, flags);
end;

//------ KS_closeEvent ------
type
  TKS_closeEvent = function (hEvent: KSHandle): Error; stdcall;
  PKS_closeEvent = ^TKS_closeEvent;
var
  FKS_closeEvent: TKS_closeEvent;
        
function KS_closeEvent(hEvent: KSHandle): Error; stdcall;
begin
  if @FKS_closeEvent = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeEvent := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeEvent := PKS_closeEvent(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeEvent'));
    if @FKS_closeEvent = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeEvent not found in DLL');
      KS_closeEvent := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeEvent := FKS_closeEvent(hEvent);
end;

//------ KS_setEvent ------
type
  TKS_setEvent = function (hEvent: KSHandle): Error; stdcall;
  PKS_setEvent = ^TKS_setEvent;
var
  FKS_setEvent: TKS_setEvent;
        
function KS_setEvent(hEvent: KSHandle): Error; stdcall;
begin
  if @FKS_setEvent = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setEvent := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setEvent := PKS_setEvent(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setEvent'));
    if @FKS_setEvent = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setEvent not found in DLL');
      KS_setEvent := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setEvent := FKS_setEvent(hEvent);
end;

//------ KS_resetEvent ------
type
  TKS_resetEvent = function (hEvent: KSHandle): Error; stdcall;
  PKS_resetEvent = ^TKS_resetEvent;
var
  FKS_resetEvent: TKS_resetEvent;
        
function KS_resetEvent(hEvent: KSHandle): Error; stdcall;
begin
  if @FKS_resetEvent = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_resetEvent := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_resetEvent := PKS_resetEvent(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_resetEvent'));
    if @FKS_resetEvent = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_resetEvent not found in DLL');
      KS_resetEvent := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_resetEvent := FKS_resetEvent(hEvent);
end;

//------ KS_pulseEvent ------
type
  TKS_pulseEvent = function (hEvent: KSHandle): Error; stdcall;
  PKS_pulseEvent = ^TKS_pulseEvent;
var
  FKS_pulseEvent: TKS_pulseEvent;
        
function KS_pulseEvent(hEvent: KSHandle): Error; stdcall;
begin
  if @FKS_pulseEvent = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_pulseEvent := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_pulseEvent := PKS_pulseEvent(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_pulseEvent'));
    if @FKS_pulseEvent = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_pulseEvent not found in DLL');
      KS_pulseEvent := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_pulseEvent := FKS_pulseEvent(hEvent);
end;

//------ KS_waitForEvent ------
type
  TKS_waitForEvent = function (hEvent: KSHandle; flags: int; timeout: int): Error; stdcall;
  PKS_waitForEvent = ^TKS_waitForEvent;
var
  FKS_waitForEvent: TKS_waitForEvent;
        
function KS_waitForEvent(hEvent: KSHandle; flags: int; timeout: int): Error; stdcall;
begin
  if @FKS_waitForEvent = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_waitForEvent := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_waitForEvent := PKS_waitForEvent(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_waitForEvent'));
    if @FKS_waitForEvent = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_waitForEvent not found in DLL');
      KS_waitForEvent := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_waitForEvent := FKS_waitForEvent(hEvent, flags, timeout);
end;

//------ KS_getEventState ------
type
  TKS_getEventState = function (hEvent: KSHandle; pState: PUInt): Error; stdcall;
  PKS_getEventState = ^TKS_getEventState;
var
  FKS_getEventState: TKS_getEventState;
        
function KS_getEventState(hEvent: KSHandle; pState: PUInt): Error; stdcall;
begin
  if @FKS_getEventState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getEventState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getEventState := PKS_getEventState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getEventState'));
    if @FKS_getEventState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getEventState not found in DLL');
      KS_getEventState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getEventState := FKS_getEventState(hEvent, pState);
end;

//------ KS_postMessage ------
type
  TKS_postMessage = function (hWnd: Win32Handle; msg: uint; wP: uint; lP: uint): Error; stdcall;
  PKS_postMessage = ^TKS_postMessage;
var
  FKS_postMessage: TKS_postMessage;
        
function KS_postMessage(hWnd: Win32Handle; msg: uint; wP: uint; lP: uint): Error; stdcall;
begin
  if @FKS_postMessage = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_postMessage := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_postMessage := PKS_postMessage(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_postMessage'));
    if @FKS_postMessage = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_postMessage not found in DLL');
      KS_postMessage := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_postMessage := FKS_postMessage(hWnd, msg, wP, lP);
end;

//------ KS_createCallBack ------
type
  TKS_createCallBack = function (phCallBack: PKSHandle; routine: Pointer; pArgs: Pointer; flags: int; prio: int): Error; stdcall;
  PKS_createCallBack = ^TKS_createCallBack;
var
  FKS_createCallBack: TKS_createCallBack;
        
function KS_createCallBack(phCallBack: PKSHandle; routine: Pointer; pArgs: Pointer; flags: int; prio: int): Error; stdcall;
begin
  if @FKS_createCallBack = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createCallBack := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createCallBack := PKS_createCallBack(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createCallBack'));
    if @FKS_createCallBack = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createCallBack not found in DLL');
      KS_createCallBack := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createCallBack := FKS_createCallBack(phCallBack, routine, pArgs, flags, prio);
end;

//------ KS_removeCallBack ------
type
  TKS_removeCallBack = function (hCallBack: KSHandle): Error; stdcall;
  PKS_removeCallBack = ^TKS_removeCallBack;
var
  FKS_removeCallBack: TKS_removeCallBack;
        
function KS_removeCallBack(hCallBack: KSHandle): Error; stdcall;
begin
  if @FKS_removeCallBack = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeCallBack := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeCallBack := PKS_removeCallBack(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeCallBack'));
    if @FKS_removeCallBack = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeCallBack not found in DLL');
      KS_removeCallBack := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeCallBack := FKS_removeCallBack(hCallBack);
end;

//------ KS_execCallBack ------
type
  TKS_execCallBack = function (hCallBack: KSHandle; pContext: Pointer; flags: int): Error; stdcall;
  PKS_execCallBack = ^TKS_execCallBack;
var
  FKS_execCallBack: TKS_execCallBack;
        
function KS_execCallBack(hCallBack: KSHandle; pContext: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execCallBack = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execCallBack := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execCallBack := PKS_execCallBack(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execCallBack'));
    if @FKS_execCallBack = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execCallBack not found in DLL');
      KS_execCallBack := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execCallBack := FKS_execCallBack(hCallBack, pContext, flags);
end;

//------ KS_getCallState ------
type
  TKS_getCallState = function (hSignal: KSHandle; pState: PHandlerState): Error; stdcall;
  PKS_getCallState = ^TKS_getCallState;
var
  FKS_getCallState: TKS_getCallState;
        
function KS_getCallState(hSignal: KSHandle; pState: PHandlerState): Error; stdcall;
begin
  if @FKS_getCallState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getCallState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getCallState := PKS_getCallState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getCallState'));
    if @FKS_getCallState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getCallState not found in DLL');
      KS_getCallState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getCallState := FKS_getCallState(hSignal, pState);
end;

//------ KS_signalObject ------
type
  TKS_signalObject = function (hObject: KSHandle; pContext: Pointer; flags: int): Error; stdcall;
  PKS_signalObject = ^TKS_signalObject;
var
  FKS_signalObject: TKS_signalObject;
        
function KS_signalObject(hObject: KSHandle; pContext: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_signalObject = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_signalObject := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_signalObject := PKS_signalObject(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_signalObject'));
    if @FKS_signalObject = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_signalObject not found in DLL');
      KS_signalObject := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_signalObject := FKS_signalObject(hObject, pContext, flags);
end;

//------ KS_createPipe ------
type
  TKS_createPipe = function (phPipe: PKSHandle; name: PAnsiChar; itemSize: int; itemCount: int; hNotify: KSHandle; flags: int): Error; stdcall;
  PKS_createPipe = ^TKS_createPipe;
var
  FKS_createPipe: TKS_createPipe;
        
function KS_createPipe(phPipe: PKSHandle; name: PAnsiChar; itemSize: int; itemCount: int; hNotify: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_createPipe = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createPipe := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createPipe := PKS_createPipe(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createPipe'));
    if @FKS_createPipe = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createPipe not found in DLL');
      KS_createPipe := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createPipe := FKS_createPipe(phPipe, name, itemSize, itemCount, hNotify, flags);
end;

//------ KS_removePipe ------
type
  TKS_removePipe = function (hPipe: KSHandle): Error; stdcall;
  PKS_removePipe = ^TKS_removePipe;
var
  FKS_removePipe: TKS_removePipe;
        
function KS_removePipe(hPipe: KSHandle): Error; stdcall;
begin
  if @FKS_removePipe = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removePipe := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removePipe := PKS_removePipe(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removePipe'));
    if @FKS_removePipe = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removePipe not found in DLL');
      KS_removePipe := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removePipe := FKS_removePipe(hPipe);
end;

//------ KS_flushPipe ------
type
  TKS_flushPipe = function (hPipe: KSHandle; flags: int): Error; stdcall;
  PKS_flushPipe = ^TKS_flushPipe;
var
  FKS_flushPipe: TKS_flushPipe;
        
function KS_flushPipe(hPipe: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_flushPipe = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_flushPipe := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_flushPipe := PKS_flushPipe(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_flushPipe'));
    if @FKS_flushPipe = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_flushPipe not found in DLL');
      KS_flushPipe := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_flushPipe := FKS_flushPipe(hPipe, flags);
end;

//------ KS_getPipe ------
type
  TKS_getPipe = function (hPipe: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_getPipe = ^TKS_getPipe;
var
  FKS_getPipe: TKS_getPipe;
        
function KS_getPipe(hPipe: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_getPipe = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getPipe := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getPipe := PKS_getPipe(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getPipe'));
    if @FKS_getPipe = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getPipe not found in DLL');
      KS_getPipe := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getPipe := FKS_getPipe(hPipe, pBuffer, length, pLength, flags);
end;

//------ KS_putPipe ------
type
  TKS_putPipe = function (hPipe: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_putPipe = ^TKS_putPipe;
var
  FKS_putPipe: TKS_putPipe;
        
function KS_putPipe(hPipe: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_putPipe = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_putPipe := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_putPipe := PKS_putPipe(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_putPipe'));
    if @FKS_putPipe = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_putPipe not found in DLL');
      KS_putPipe := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_putPipe := FKS_putPipe(hPipe, pBuffer, length, pLength, flags);
end;

//------ KS_createQuickMutex ------
type
  TKS_createQuickMutex = function (phMutex: PKSHandle; level: int; flags: int): Error; stdcall;
  PKS_createQuickMutex = ^TKS_createQuickMutex;
var
  FKS_createQuickMutex: TKS_createQuickMutex;
        
function KS_createQuickMutex(phMutex: PKSHandle; level: int; flags: int): Error; stdcall;
begin
  if @FKS_createQuickMutex = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createQuickMutex := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createQuickMutex := PKS_createQuickMutex(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createQuickMutex'));
    if @FKS_createQuickMutex = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createQuickMutex not found in DLL');
      KS_createQuickMutex := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createQuickMutex := FKS_createQuickMutex(phMutex, level, flags);
end;

//------ KS_removeQuickMutex ------
type
  TKS_removeQuickMutex = function (hMutex: KSHandle): Error; stdcall;
  PKS_removeQuickMutex = ^TKS_removeQuickMutex;
var
  FKS_removeQuickMutex: TKS_removeQuickMutex;
        
function KS_removeQuickMutex(hMutex: KSHandle): Error; stdcall;
begin
  if @FKS_removeQuickMutex = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeQuickMutex := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeQuickMutex := PKS_removeQuickMutex(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeQuickMutex'));
    if @FKS_removeQuickMutex = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeQuickMutex not found in DLL');
      KS_removeQuickMutex := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeQuickMutex := FKS_removeQuickMutex(hMutex);
end;

//------ KS_requestQuickMutex ------
type
  TKS_requestQuickMutex = function (hMutex: KSHandle): Error; stdcall;
  PKS_requestQuickMutex = ^TKS_requestQuickMutex;
var
  FKS_requestQuickMutex: TKS_requestQuickMutex;
        
function KS_requestQuickMutex(hMutex: KSHandle): Error; stdcall;
begin
  if @FKS_requestQuickMutex = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_requestQuickMutex := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_requestQuickMutex := PKS_requestQuickMutex(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_requestQuickMutex'));
    if @FKS_requestQuickMutex = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_requestQuickMutex not found in DLL');
      KS_requestQuickMutex := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_requestQuickMutex := FKS_requestQuickMutex(hMutex);
end;

//------ KS_releaseQuickMutex ------
type
  TKS_releaseQuickMutex = function (hMutex: KSHandle): Error; stdcall;
  PKS_releaseQuickMutex = ^TKS_releaseQuickMutex;
var
  FKS_releaseQuickMutex: TKS_releaseQuickMutex;
        
function KS_releaseQuickMutex(hMutex: KSHandle): Error; stdcall;
begin
  if @FKS_releaseQuickMutex = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_releaseQuickMutex := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_releaseQuickMutex := PKS_releaseQuickMutex(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_releaseQuickMutex'));
    if @FKS_releaseQuickMutex = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_releaseQuickMutex not found in DLL');
      KS_releaseQuickMutex := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_releaseQuickMutex := FKS_releaseQuickMutex(hMutex);
end;

//------ _initBaseModule ------
procedure _initBaseModule();
begin
  _registerKernelAddress('KS_getDriverConfig', @KS_getDriverConfig);
  _registerKernelAddress('KS_setDriverConfig', @KS_setDriverConfig);
  _registerKernelAddress('KS_getProcessorInformation', @KS_getProcessorInformation);
  _registerKernelAddress('KS_closeHandle', @KS_closeHandle);
  _registerKernelAddress('KS_showMessage', @KS_showMessage);
  _registerKernelAddress('KS_logMessage', @KS_logMessage);
  _registerKernelAddress('KS_bufMessage', @KS_bufMessage);
  _registerKernelAddress('KS_dbgMessage', @KS_dbgMessage);
  _registerKernelAddress('KS_beep', @KS_beep);
  _registerKernelAddress('KS_throwException', @KS_throwException);
  _registerKernelAddress('KS_getSharedMem', @KS_getSharedMem);
  _registerKernelAddress('KS_readMem', @KS_readMem);
  _registerKernelAddress('KS_createSharedMemEx', @KS_createSharedMemEx);
  _registerKernelAddress('KS_getSharedMemEx', @KS_getSharedMemEx);
  _registerKernelAddress('KS_setEvent', @KS_setEvent);
  _registerKernelAddress('KS_resetEvent', @KS_resetEvent);
  _registerKernelAddress('KS_pulseEvent', @KS_pulseEvent);
  _registerKernelAddress('KS_waitForEvent', @KS_waitForEvent);
  _registerKernelAddress('KS_postMessage', @KS_postMessage);
  _registerKernelAddress('KS_createCallBack', @KS_createCallBack);
  _registerKernelAddress('KS_removeCallBack', @KS_removeCallBack);
  _registerKernelAddress('KS_execCallBack', @KS_execCallBack);
  _registerKernelAddress('KS_signalObject', @KS_signalObject);
  _registerKernelAddress('KS_flushPipe', @KS_flushPipe);
  _registerKernelAddress('KS_getPipe', @KS_getPipe);
  _registerKernelAddress('KS_putPipe', @KS_putPipe);
  _registerKernelAddress('KS_createQuickMutex', @KS_createQuickMutex);
  _registerKernelAddress('KS_removeQuickMutex', @KS_removeQuickMutex);
  _registerKernelAddress('KS_requestQuickMutex', @KS_requestQuickMutex);
  _registerKernelAddress('KS_releaseQuickMutex', @KS_releaseQuickMutex);
end;

{$DEFINE KS_BASE_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Kernel Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_prepareKernelExec ------
type
  TKS_prepareKernelExec = function (callback: Pointer; pRelocated: PPointer; flags: int): Error; stdcall;
  PKS_prepareKernelExec = ^TKS_prepareKernelExec;
var
  FKS_prepareKernelExec: TKS_prepareKernelExec;
        
function KS_prepareKernelExec(callback: Pointer; pRelocated: PPointer; flags: int): Error; stdcall;
begin
  if @FKS_prepareKernelExec = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_prepareKernelExec := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_prepareKernelExec := PKS_prepareKernelExec(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_prepareKernelExec'));
    if @FKS_prepareKernelExec = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_prepareKernelExec not found in DLL');
      KS_prepareKernelExec := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_prepareKernelExec := FKS_prepareKernelExec(callback, pRelocated, flags);
end;

//------ KS_endKernelExec ------
type
  TKS_endKernelExec = function (relocated: Pointer): Error; stdcall;
  PKS_endKernelExec = ^TKS_endKernelExec;
var
  FKS_endKernelExec: TKS_endKernelExec;
        
function KS_endKernelExec(relocated: Pointer): Error; stdcall;
begin
  if @FKS_endKernelExec = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_endKernelExec := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_endKernelExec := PKS_endKernelExec(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_endKernelExec'));
    if @FKS_endKernelExec = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_endKernelExec not found in DLL');
      KS_endKernelExec := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_endKernelExec := FKS_endKernelExec(relocated);
end;

//------ KS_testKernelExec ------
type
  TKS_testKernelExec = function (relocated: Pointer; pArgs: Pointer): Error; stdcall;
  PKS_testKernelExec = ^TKS_testKernelExec;
var
  FKS_testKernelExec: TKS_testKernelExec;
        
function KS_testKernelExec(relocated: Pointer; pArgs: Pointer): Error; stdcall;
begin
  if @FKS_testKernelExec = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_testKernelExec := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_testKernelExec := PKS_testKernelExec(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_testKernelExec'));
    if @FKS_testKernelExec = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_testKernelExec not found in DLL');
      KS_testKernelExec := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_testKernelExec := FKS_testKernelExec(relocated, pArgs);
end;

//------ KS_registerKernelAddress ------
type
  TKS_registerKernelAddress = function (name: PAnsiChar; appFunction: Pointer; sysFunction: Pointer; flags: int): Error; stdcall;
  PKS_registerKernelAddress = ^TKS_registerKernelAddress;
var
  FKS_registerKernelAddress: TKS_registerKernelAddress;
        
function KS_registerKernelAddress(name: PAnsiChar; appFunction: Pointer; sysFunction: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_registerKernelAddress = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_registerKernelAddress := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_registerKernelAddress := PKS_registerKernelAddress(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_registerKernelAddress'));
    if @FKS_registerKernelAddress = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_registerKernelAddress not found in DLL');
      KS_registerKernelAddress := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_registerKernelAddress := FKS_registerKernelAddress(name, appFunction, sysFunction, flags);
end;

//------ KS_loadKernel ------
type
  TKS_loadKernel = function (phKernel: PKSHandle; dllName: PAnsiChar; initProcName: PAnsiChar; pArgs: Pointer; flags: int): Error; stdcall;
  PKS_loadKernel = ^TKS_loadKernel;
var
  FKS_loadKernel: TKS_loadKernel;
        
function KS_loadKernel(phKernel: PKSHandle; dllName: PAnsiChar; initProcName: PAnsiChar; pArgs: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_loadKernel = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_loadKernel := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_loadKernel := PKS_loadKernel(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_loadKernel'));
    if @FKS_loadKernel = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_loadKernel not found in DLL');
      KS_loadKernel := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_loadKernel := FKS_loadKernel(phKernel, dllName, initProcName, pArgs, flags);
end;

//------ KS_loadKernelFromBuffer ------
type
  TKS_loadKernelFromBuffer = function (phKernel: PKSHandle; dllName: PAnsiChar; pBuffer: Pointer; length: int; initProcName: PAnsiChar; pArgs: Pointer; flags: int): Error; stdcall;
  PKS_loadKernelFromBuffer = ^TKS_loadKernelFromBuffer;
var
  FKS_loadKernelFromBuffer: TKS_loadKernelFromBuffer;
        
function KS_loadKernelFromBuffer(phKernel: PKSHandle; dllName: PAnsiChar; pBuffer: Pointer; length: int; initProcName: PAnsiChar; pArgs: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_loadKernelFromBuffer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_loadKernelFromBuffer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_loadKernelFromBuffer := PKS_loadKernelFromBuffer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_loadKernelFromBuffer'));
    if @FKS_loadKernelFromBuffer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_loadKernelFromBuffer not found in DLL');
      KS_loadKernelFromBuffer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_loadKernelFromBuffer := FKS_loadKernelFromBuffer(phKernel, dllName, pBuffer, length, initProcName, pArgs, flags);
end;

//------ KS_freeKernel ------
type
  TKS_freeKernel = function (hKernel: KSHandle): Error; stdcall;
  PKS_freeKernel = ^TKS_freeKernel;
var
  FKS_freeKernel: TKS_freeKernel;
        
function KS_freeKernel(hKernel: KSHandle): Error; stdcall;
begin
  if @FKS_freeKernel = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_freeKernel := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_freeKernel := PKS_freeKernel(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_freeKernel'));
    if @FKS_freeKernel = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_freeKernel not found in DLL');
      KS_freeKernel := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_freeKernel := FKS_freeKernel(hKernel);
end;

//------ KS_execKernelFunction ------
type
  TKS_execKernelFunction = function (hKernel: KSHandle; name: PAnsiChar; pArgs: Pointer; pContext: Pointer; flags: int): Error; stdcall;
  PKS_execKernelFunction = ^TKS_execKernelFunction;
var
  FKS_execKernelFunction: TKS_execKernelFunction;
        
function KS_execKernelFunction(hKernel: KSHandle; name: PAnsiChar; pArgs: Pointer; pContext: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execKernelFunction = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execKernelFunction := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execKernelFunction := PKS_execKernelFunction(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execKernelFunction'));
    if @FKS_execKernelFunction = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execKernelFunction not found in DLL');
      KS_execKernelFunction := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execKernelFunction := FKS_execKernelFunction(hKernel, name, pArgs, pContext, flags);
end;

//------ KS_getKernelFunction ------
type
  TKS_getKernelFunction = function (hKernel: KSHandle; name: PAnsiChar; pRelocated: PPointer; flags: int): Error; stdcall;
  PKS_getKernelFunction = ^TKS_getKernelFunction;
var
  FKS_getKernelFunction: TKS_getKernelFunction;
        
function KS_getKernelFunction(hKernel: KSHandle; name: PAnsiChar; pRelocated: PPointer; flags: int): Error; stdcall;
begin
  if @FKS_getKernelFunction = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getKernelFunction := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getKernelFunction := PKS_getKernelFunction(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getKernelFunction'));
    if @FKS_getKernelFunction = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getKernelFunction not found in DLL');
      KS_getKernelFunction := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getKernelFunction := FKS_getKernelFunction(hKernel, name, pRelocated, flags);
end;

//------ KS_enumKernelFunctions ------
type
  TKS_enumKernelFunctions = function (hKernel: KSHandle; index: int; pName: PAnsiChar; flags: int): Error; stdcall;
  PKS_enumKernelFunctions = ^TKS_enumKernelFunctions;
var
  FKS_enumKernelFunctions: TKS_enumKernelFunctions;
        
function KS_enumKernelFunctions(hKernel: KSHandle; index: int; pName: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_enumKernelFunctions = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumKernelFunctions := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumKernelFunctions := PKS_enumKernelFunctions(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumKernelFunctions'));
    if @FKS_enumKernelFunctions = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumKernelFunctions not found in DLL');
      KS_enumKernelFunctions := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumKernelFunctions := FKS_enumKernelFunctions(hKernel, index, pName, flags);
end;

//------ KS_execKernelCommand ------
type
  TKS_execKernelCommand = function (hKernel: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execKernelCommand = ^TKS_execKernelCommand;
var
  FKS_execKernelCommand: TKS_execKernelCommand;
        
function KS_execKernelCommand(hKernel: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execKernelCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execKernelCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execKernelCommand := PKS_execKernelCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execKernelCommand'));
    if @FKS_execKernelCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execKernelCommand not found in DLL');
      KS_execKernelCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execKernelCommand := FKS_execKernelCommand(hKernel, command, pParam, flags);
end;

//------ KS_createKernelCallBack ------
type
  TKS_createKernelCallBack = function (phCallBack: PKSHandle; hKernel: KSHandle; name: PAnsiChar; pArgs: Pointer; flags: int; prio: int): Error; stdcall;
  PKS_createKernelCallBack = ^TKS_createKernelCallBack;
var
  FKS_createKernelCallBack: TKS_createKernelCallBack;
        
function KS_createKernelCallBack(phCallBack: PKSHandle; hKernel: KSHandle; name: PAnsiChar; pArgs: Pointer; flags: int; prio: int): Error; stdcall;
begin
  if @FKS_createKernelCallBack = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createKernelCallBack := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createKernelCallBack := PKS_createKernelCallBack(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createKernelCallBack'));
    if @FKS_createKernelCallBack = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createKernelCallBack not found in DLL');
      KS_createKernelCallBack := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createKernelCallBack := FKS_createKernelCallBack(phCallBack, hKernel, name, pArgs, flags, prio);
end;

//------ KS_execSyncFunction ------
type
  TKS_execSyncFunction = function (hHandler: KSHandle; proc: Pointer; pArgs: Pointer; flags: int): Error; stdcall;
  PKS_execSyncFunction = ^TKS_execSyncFunction;
var
  FKS_execSyncFunction: TKS_execSyncFunction;
        
function KS_execSyncFunction(hHandler: KSHandle; proc: Pointer; pArgs: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execSyncFunction = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execSyncFunction := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execSyncFunction := PKS_execSyncFunction(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execSyncFunction'));
    if @FKS_execSyncFunction = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execSyncFunction not found in DLL');
      KS_execSyncFunction := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execSyncFunction := FKS_execSyncFunction(hHandler, proc, pArgs, flags);
end;

//------ KS_execSystemCall ------
type
  TKS_execSystemCall = function (fileName: PAnsiChar; funcName: PAnsiChar; pData: Pointer; flags: int): Error; stdcall;
  PKS_execSystemCall = ^TKS_execSystemCall;
var
  FKS_execSystemCall: TKS_execSystemCall;
        
function KS_execSystemCall(fileName: PAnsiChar; funcName: PAnsiChar; pData: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execSystemCall = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execSystemCall := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execSystemCall := PKS_execSystemCall(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execSystemCall'));
    if @FKS_execSystemCall = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execSystemCall not found in DLL');
      KS_execSystemCall := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execSystemCall := FKS_execSystemCall(fileName, funcName, pData, flags);
end;

//------ KS_memCpy ------
type
  TKS_memCpy = function (pDst: Pointer; pSrc: Pointer; size: int; flags: int): Error; stdcall;
  PKS_memCpy = ^TKS_memCpy;
var
  FKS_memCpy: TKS_memCpy;
        
function KS_memCpy(pDst: Pointer; pSrc: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_memCpy = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_memCpy := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_memCpy := PKS_memCpy(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_memCpy'));
    if @FKS_memCpy = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_memCpy not found in DLL');
      KS_memCpy := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_memCpy := FKS_memCpy(pDst, pSrc, size, flags);
end;

//------ KS_memMove ------
type
  TKS_memMove = function (pDst: Pointer; pSrc: Pointer; size: int; flags: int): Error; stdcall;
  PKS_memMove = ^TKS_memMove;
var
  FKS_memMove: TKS_memMove;
        
function KS_memMove(pDst: Pointer; pSrc: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_memMove = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_memMove := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_memMove := PKS_memMove(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_memMove'));
    if @FKS_memMove = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_memMove not found in DLL');
      KS_memMove := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_memMove := FKS_memMove(pDst, pSrc, size, flags);
end;

//------ KS_memSet ------
type
  TKS_memSet = function (pMem: Pointer; value: int; size: int; flags: int): Error; stdcall;
  PKS_memSet = ^TKS_memSet;
var
  FKS_memSet: TKS_memSet;
        
function KS_memSet(pMem: Pointer; value: int; size: int; flags: int): Error; stdcall;
begin
  if @FKS_memSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_memSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_memSet := PKS_memSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_memSet'));
    if @FKS_memSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_memSet not found in DLL');
      KS_memSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_memSet := FKS_memSet(pMem, value, size, flags);
end;

//------ KS_malloc ------
type
  TKS_malloc = function (ppAllocated: PPointer; bytes: int; flags: int): Error; stdcall;
  PKS_malloc = ^TKS_malloc;
var
  FKS_malloc: TKS_malloc;
        
function KS_malloc(ppAllocated: PPointer; bytes: int; flags: int): Error; stdcall;
begin
  if @FKS_malloc = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_malloc := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_malloc := PKS_malloc(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_malloc'));
    if @FKS_malloc = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_malloc not found in DLL');
      KS_malloc := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_malloc := FKS_malloc(ppAllocated, bytes, flags);
end;

//------ KS_free ------
type
  TKS_free = function (pAllocated: Pointer; flags: int): Error; stdcall;
  PKS_free = ^TKS_free;
var
  FKS_free: TKS_free;
        
function KS_free(pAllocated: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_free = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_free := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_free := PKS_free(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_free'));
    if @FKS_free = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_free not found in DLL');
      KS_free := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_free := FKS_free(pAllocated, flags);
end;

//------ KS_interlocked ------
type
  TKS_interlocked = function (pInterlocked: Pointer; operation: int; pParameter1: Pointer; pParameter2: Pointer; flags: int): Error; stdcall;
  PKS_interlocked = ^TKS_interlocked;
var
  FKS_interlocked: TKS_interlocked;
        
function KS_interlocked(pInterlocked: Pointer; operation: int; pParameter1: Pointer; pParameter2: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_interlocked = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_interlocked := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_interlocked := PKS_interlocked(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_interlocked'));
    if @FKS_interlocked = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_interlocked not found in DLL');
      KS_interlocked := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_interlocked := FKS_interlocked(pInterlocked, operation, pParameter1, pParameter2, flags);
end;

//------ _initKernelModule ------
procedure _initKernelModule();
begin
  _registerKernelAddress('KS_loadKernel', @KS_loadKernel);
  _registerKernelAddress('KS_execKernelFunction', @KS_execKernelFunction);
  _registerKernelAddress('KS_getKernelFunction', @KS_getKernelFunction);
  _registerKernelAddress('KS_enumKernelFunctions', @KS_enumKernelFunctions);
  _registerKernelAddress('KS_createKernelCallBack', @KS_createKernelCallBack);
  _registerKernelAddress('KS_execSyncFunction', @KS_execSyncFunction);
  _registerKernelAddress('KS_execSystemCall', @KS_execSystemCall);
  _registerKernelAddress('KS_memCpy', @KS_memCpy);
  _registerKernelAddress('KS_memMove', @KS_memMove);
  _registerKernelAddress('KS_memSet', @KS_memSet);
  _registerKernelAddress('KS_malloc', @KS_malloc);
  _registerKernelAddress('KS_free', @KS_free);
  _registerKernelAddress('KS_interlocked', @KS_interlocked);
end;

{$DEFINE KS_KERNEL_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// IoPort Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_enableIoRange ------
type
  TKS_enableIoRange = function (ioPortBase: uint; count: int; flags: int): Error; stdcall;
  PKS_enableIoRange = ^TKS_enableIoRange;
var
  FKS_enableIoRange: TKS_enableIoRange;
        
function KS_enableIoRange(ioPortBase: uint; count: int; flags: int): Error; stdcall;
begin
  if @FKS_enableIoRange = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enableIoRange := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enableIoRange := PKS_enableIoRange(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enableIoRange'));
    if @FKS_enableIoRange = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enableIoRange not found in DLL');
      KS_enableIoRange := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enableIoRange := FKS_enableIoRange(ioPortBase, count, flags);
end;

//------ KS_readIoPort ------
type
  TKS_readIoPort = function (ioPort: uint; pValue: PUInt; flags: int): Error; stdcall;
  PKS_readIoPort = ^TKS_readIoPort;
var
  FKS_readIoPort: TKS_readIoPort;
        
function KS_readIoPort(ioPort: uint; pValue: PUInt; flags: int): Error; stdcall;
begin
  if @FKS_readIoPort = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readIoPort := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readIoPort := PKS_readIoPort(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readIoPort'));
    if @FKS_readIoPort = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readIoPort not found in DLL');
      KS_readIoPort := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readIoPort := FKS_readIoPort(ioPort, pValue, flags);
end;

//------ KS_writeIoPort ------
type
  TKS_writeIoPort = function (ioPort: uint; value: uint; flags: int): Error; stdcall;
  PKS_writeIoPort = ^TKS_writeIoPort;
var
  FKS_writeIoPort: TKS_writeIoPort;
        
function KS_writeIoPort(ioPort: uint; value: uint; flags: int): Error; stdcall;
begin
  if @FKS_writeIoPort = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_writeIoPort := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_writeIoPort := PKS_writeIoPort(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_writeIoPort'));
    if @FKS_writeIoPort = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_writeIoPort not found in DLL');
      KS_writeIoPort := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_writeIoPort := FKS_writeIoPort(ioPort, value, flags);
end;

//------ KS_inpb ------
type
  TKS_inpb = function (ioPort: uint): uint; stdcall;
  PKS_inpb = ^TKS_inpb;
var
  FKS_inpb: TKS_inpb;
        
function KS_inpb(ioPort: uint): uint; stdcall;
begin
  if @FKS_inpb = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_inpb := 0;
        exit;
      end;
    end;

    @FKS_inpb := PKS_inpb(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_inpb'));
    if @FKS_inpb = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_inpb not found in DLL');
      KS_inpb := 0;
      exit;
    end;
  end;

  KS_inpb := FKS_inpb(ioPort);
end;

//------ KS_inpw ------
type
  TKS_inpw = function (ioPort: uint): uint; stdcall;
  PKS_inpw = ^TKS_inpw;
var
  FKS_inpw: TKS_inpw;
        
function KS_inpw(ioPort: uint): uint; stdcall;
begin
  if @FKS_inpw = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_inpw := 0;
        exit;
      end;
    end;

    @FKS_inpw := PKS_inpw(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_inpw'));
    if @FKS_inpw = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_inpw not found in DLL');
      KS_inpw := 0;
      exit;
    end;
  end;

  KS_inpw := FKS_inpw(ioPort);
end;

//------ KS_inpd ------
type
  TKS_inpd = function (ioPort: uint): uint; stdcall;
  PKS_inpd = ^TKS_inpd;
var
  FKS_inpd: TKS_inpd;
        
function KS_inpd(ioPort: uint): uint; stdcall;
begin
  if @FKS_inpd = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_inpd := 0;
        exit;
      end;
    end;

    @FKS_inpd := PKS_inpd(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_inpd'));
    if @FKS_inpd = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_inpd not found in DLL');
      KS_inpd := 0;
      exit;
    end;
  end;

  KS_inpd := FKS_inpd(ioPort);
end;

//------ KS_outpb ------
type
  TKS_outpb = procedure (ioPort: uint; value: uint); stdcall;
  PKS_outpb = ^TKS_outpb;
var
  FKS_outpb: TKS_outpb;
        
procedure KS_outpb(ioPort: uint; value: uint); stdcall;
begin
  if @FKS_outpb = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        exit;
      end;
    end;

    @FKS_outpb := PKS_outpb(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_outpb'));
    if @FKS_outpb = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_outpb not found in DLL');
      exit;
    end;
  end;

  FKS_outpb(ioPort, value);
end;

//------ KS_outpw ------
type
  TKS_outpw = procedure (ioPort: uint; value: uint); stdcall;
  PKS_outpw = ^TKS_outpw;
var
  FKS_outpw: TKS_outpw;
        
procedure KS_outpw(ioPort: uint; value: uint); stdcall;
begin
  if @FKS_outpw = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        exit;
      end;
    end;

    @FKS_outpw := PKS_outpw(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_outpw'));
    if @FKS_outpw = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_outpw not found in DLL');
      exit;
    end;
  end;

  FKS_outpw(ioPort, value);
end;

//------ KS_outpd ------
type
  TKS_outpd = procedure (ioPort: uint; value: uint); stdcall;
  PKS_outpd = ^TKS_outpd;
var
  FKS_outpd: TKS_outpd;
        
procedure KS_outpd(ioPort: uint; value: uint); stdcall;
begin
  if @FKS_outpd = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        exit;
      end;
    end;

    @FKS_outpd := PKS_outpd(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_outpd'));
    if @FKS_outpd = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_outpd not found in DLL');
      exit;
    end;
  end;

  FKS_outpd(ioPort, value);
end;

//------ KS_getBusData ------
type
  TKS_getBusData = function (pBusData: PKSPciBusData; busNumber: int; slotNumber: int; flags: int): Error; stdcall;
  PKS_getBusData = ^TKS_getBusData;
var
  FKS_getBusData: TKS_getBusData;
        
function KS_getBusData(pBusData: PKSPciBusData; busNumber: int; slotNumber: int; flags: int): Error; stdcall;
begin
  if @FKS_getBusData = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getBusData := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getBusData := PKS_getBusData(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getBusData'));
    if @FKS_getBusData = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getBusData not found in DLL');
      KS_getBusData := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getBusData := FKS_getBusData(pBusData, busNumber, slotNumber, flags);
end;

//------ KS_setBusData ------
type
  TKS_setBusData = function (pBusData: PKSPciBusData; busNumber: int; slotNumber: int; flags: int): Error; stdcall;
  PKS_setBusData = ^TKS_setBusData;
var
  FKS_setBusData: TKS_setBusData;
        
function KS_setBusData(pBusData: PKSPciBusData; busNumber: int; slotNumber: int; flags: int): Error; stdcall;
begin
  if @FKS_setBusData = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setBusData := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setBusData := PKS_setBusData(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setBusData'));
    if @FKS_setBusData = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setBusData not found in DLL');
      KS_setBusData := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setBusData := FKS_setBusData(pBusData, busNumber, slotNumber, flags);
end;

//------ KS_getResourceInfo ------
type
  TKS_getResourceInfo = function (name: PAnsiChar; pInfo: PKSResourceInfo): Error; stdcall;
  PKS_getResourceInfo = ^TKS_getResourceInfo;
var
  FKS_getResourceInfo: TKS_getResourceInfo;
        
function KS_getResourceInfo(name: PAnsiChar; pInfo: PKSResourceInfo): Error; stdcall;
begin
  if @FKS_getResourceInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getResourceInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getResourceInfo := PKS_getResourceInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getResourceInfo'));
    if @FKS_getResourceInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getResourceInfo not found in DLL');
      KS_getResourceInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getResourceInfo := FKS_getResourceInfo(name, pInfo);
end;

//------ KS_getResourceInfoEx ------
type
  TKS_getResourceInfoEx = function (name: PAnsiChar; pInfo: PKSResourceInfoEx): Error; stdcall;
  PKS_getResourceInfoEx = ^TKS_getResourceInfoEx;
var
  FKS_getResourceInfoEx: TKS_getResourceInfoEx;
        
function KS_getResourceInfoEx(name: PAnsiChar; pInfo: PKSResourceInfoEx): Error; stdcall;
begin
  if @FKS_getResourceInfoEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getResourceInfoEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getResourceInfoEx := PKS_getResourceInfoEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getResourceInfoEx'));
    if @FKS_getResourceInfoEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getResourceInfoEx not found in DLL');
      KS_getResourceInfoEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getResourceInfoEx := FKS_getResourceInfoEx(name, pInfo);
end;

//------ KS_enterQuietSection ------
type
  TKS_enterQuietSection = function (flags: int): Error; stdcall;
  PKS_enterQuietSection = ^TKS_enterQuietSection;
var
  FKS_enterQuietSection: TKS_enterQuietSection;
        
function KS_enterQuietSection(flags: int): Error; stdcall;
begin
  if @FKS_enterQuietSection = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enterQuietSection := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enterQuietSection := PKS_enterQuietSection(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enterQuietSection'));
    if @FKS_enterQuietSection = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enterQuietSection not found in DLL');
      KS_enterQuietSection := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enterQuietSection := FKS_enterQuietSection(flags);
end;

//------ KS_releaseQuietSection ------
type
  TKS_releaseQuietSection = function (flags: int): Error; stdcall;
  PKS_releaseQuietSection = ^TKS_releaseQuietSection;
var
  FKS_releaseQuietSection: TKS_releaseQuietSection;
        
function KS_releaseQuietSection(flags: int): Error; stdcall;
begin
  if @FKS_releaseQuietSection = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_releaseQuietSection := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_releaseQuietSection := PKS_releaseQuietSection(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_releaseQuietSection'));
    if @FKS_releaseQuietSection = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_releaseQuietSection not found in DLL');
      KS_releaseQuietSection := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_releaseQuietSection := FKS_releaseQuietSection(flags);
end;

//------ _initIoPortModule ------
procedure _initIoPortModule();
begin
  _registerKernelAddress('KS_enableIoRange', @KS_enableIoRange);
  _registerKernelAddress('KS_readIoPort', @KS_readIoPort);
  _registerKernelAddress('KS_writeIoPort', @KS_writeIoPort);
  _registerKernelAddress('KS_inpb', @KS_inpb);
  _registerKernelAddress('KS_inpw', @KS_inpw);
  _registerKernelAddress('KS_inpd', @KS_inpd);
  _registerKernelAddress('KS_outpb', @KS_outpb);
  _registerKernelAddress('KS_outpw', @KS_outpw);
  _registerKernelAddress('KS_outpd', @KS_outpd);
end;

{$DEFINE KS_IOPORT_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Memory Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_mapDeviceMem ------
type
  TKS_mapDeviceMem = function (ppAppPtr: PPointer; ppSysPtr: PPointer; pInfo: PKSResourceInfoEx; index: int; flags: int): Error; stdcall;
  PKS_mapDeviceMem = ^TKS_mapDeviceMem;
var
  FKS_mapDeviceMem: TKS_mapDeviceMem;
        
function KS_mapDeviceMem(ppAppPtr: PPointer; ppSysPtr: PPointer; pInfo: PKSResourceInfoEx; index: int; flags: int): Error; stdcall;
begin
  if @FKS_mapDeviceMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_mapDeviceMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_mapDeviceMem := PKS_mapDeviceMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_mapDeviceMem'));
    if @FKS_mapDeviceMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_mapDeviceMem not found in DLL');
      KS_mapDeviceMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_mapDeviceMem := FKS_mapDeviceMem(ppAppPtr, ppSysPtr, pInfo, index, flags);
end;

//------ KS_mapPhysMem ------
type
  TKS_mapPhysMem = function (ppAppPtr: PPointer; ppSysPtr: PPointer; physAddr: uint; size: int; flags: int): Error; stdcall;
  PKS_mapPhysMem = ^TKS_mapPhysMem;
var
  FKS_mapPhysMem: TKS_mapPhysMem;
        
function KS_mapPhysMem(ppAppPtr: PPointer; ppSysPtr: PPointer; physAddr: uint; size: int; flags: int): Error; stdcall;
begin
  if @FKS_mapPhysMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_mapPhysMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_mapPhysMem := PKS_mapPhysMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_mapPhysMem'));
    if @FKS_mapPhysMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_mapPhysMem not found in DLL');
      KS_mapPhysMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_mapPhysMem := FKS_mapPhysMem(ppAppPtr, ppSysPtr, physAddr, size, flags);
end;

//------ KS_unmapPhysMem ------
type
  TKS_unmapPhysMem = function (pAppPtr: Pointer): Error; stdcall;
  PKS_unmapPhysMem = ^TKS_unmapPhysMem;
var
  FKS_unmapPhysMem: TKS_unmapPhysMem;
        
function KS_unmapPhysMem(pAppPtr: Pointer): Error; stdcall;
begin
  if @FKS_unmapPhysMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_unmapPhysMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_unmapPhysMem := PKS_unmapPhysMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_unmapPhysMem'));
    if @FKS_unmapPhysMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_unmapPhysMem not found in DLL');
      KS_unmapPhysMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_unmapPhysMem := FKS_unmapPhysMem(pAppPtr);
end;

//------ KS_allocPhysMem ------
type
  TKS_allocPhysMem = function (ppAppPtr: PPointer; ppSysPtr: PPointer; physAddr: PUInt; size: int; flags: int): Error; stdcall;
  PKS_allocPhysMem = ^TKS_allocPhysMem;
var
  FKS_allocPhysMem: TKS_allocPhysMem;
        
function KS_allocPhysMem(ppAppPtr: PPointer; ppSysPtr: PPointer; physAddr: PUInt; size: int; flags: int): Error; stdcall;
begin
  if @FKS_allocPhysMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_allocPhysMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_allocPhysMem := PKS_allocPhysMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_allocPhysMem'));
    if @FKS_allocPhysMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_allocPhysMem not found in DLL');
      KS_allocPhysMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_allocPhysMem := FKS_allocPhysMem(ppAppPtr, ppSysPtr, physAddr, size, flags);
end;

//------ KS_freePhysMem ------
type
  TKS_freePhysMem = function (pAppPtr: Pointer): Error; stdcall;
  PKS_freePhysMem = ^TKS_freePhysMem;
var
  FKS_freePhysMem: TKS_freePhysMem;
        
function KS_freePhysMem(pAppPtr: Pointer): Error; stdcall;
begin
  if @FKS_freePhysMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_freePhysMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_freePhysMem := PKS_freePhysMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_freePhysMem'));
    if @FKS_freePhysMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_freePhysMem not found in DLL');
      KS_freePhysMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_freePhysMem := FKS_freePhysMem(pAppPtr);
end;

//------ KS_copyPhysMem ------
type
  TKS_copyPhysMem = function (physAddr: uint; pBuffer: Pointer; size: uint; offset: uint; flags: int): Error; stdcall;
  PKS_copyPhysMem = ^TKS_copyPhysMem;
var
  FKS_copyPhysMem: TKS_copyPhysMem;
        
function KS_copyPhysMem(physAddr: uint; pBuffer: Pointer; size: uint; offset: uint; flags: int): Error; stdcall;
begin
  if @FKS_copyPhysMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_copyPhysMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_copyPhysMem := PKS_copyPhysMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_copyPhysMem'));
    if @FKS_copyPhysMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_copyPhysMem not found in DLL');
      KS_copyPhysMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_copyPhysMem := FKS_copyPhysMem(physAddr, pBuffer, size, offset, flags);
end;

//------ KS_getPhysAddr ------
type
  TKS_getPhysAddr = function (pPhysAddr: PPointer; pDosLinAddr: Pointer): Error; stdcall;
  PKS_getPhysAddr = ^TKS_getPhysAddr;
var
  FKS_getPhysAddr: TKS_getPhysAddr;
        
function KS_getPhysAddr(pPhysAddr: PPointer; pDosLinAddr: Pointer): Error; stdcall;
begin
  if @FKS_getPhysAddr = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getPhysAddr := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getPhysAddr := PKS_getPhysAddr(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getPhysAddr'));
    if @FKS_getPhysAddr = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getPhysAddr not found in DLL');
      KS_getPhysAddr := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getPhysAddr := FKS_getPhysAddr(pPhysAddr, pDosLinAddr);
end;

//------ _initMemoryModule ------
procedure _initMemoryModule();
begin
  _registerKernelAddress('KS_allocPhysMem', @KS_allocPhysMem);
end;

{$DEFINE KS_MEMORY_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Clock Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_calibrateMachineTime ------
type
  TKS_calibrateMachineTime = function (pFrequency: PUInt; flags: int): Error; stdcall;
  PKS_calibrateMachineTime = ^TKS_calibrateMachineTime;
var
  FKS_calibrateMachineTime: TKS_calibrateMachineTime;
        
function KS_calibrateMachineTime(pFrequency: PUInt; flags: int): Error; stdcall;
begin
  if @FKS_calibrateMachineTime = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_calibrateMachineTime := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_calibrateMachineTime := PKS_calibrateMachineTime(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_calibrateMachineTime'));
    if @FKS_calibrateMachineTime = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_calibrateMachineTime not found in DLL');
      KS_calibrateMachineTime := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_calibrateMachineTime := FKS_calibrateMachineTime(pFrequency, flags);
end;

//------ KS_getSystemTicks ------
type
  TKS_getSystemTicks = function (pSystemTicks: PUInt64): Error; stdcall;
  PKS_getSystemTicks = ^TKS_getSystemTicks;
var
  FKS_getSystemTicks: TKS_getSystemTicks;
        
function KS_getSystemTicks(pSystemTicks: PUInt64): Error; stdcall;
begin
  if @FKS_getSystemTicks = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getSystemTicks := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getSystemTicks := PKS_getSystemTicks(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getSystemTicks'));
    if @FKS_getSystemTicks = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getSystemTicks not found in DLL');
      KS_getSystemTicks := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getSystemTicks := FKS_getSystemTicks(pSystemTicks);
end;

//------ KS_getSystemTime ------
type
  TKS_getSystemTime = function (pSystemTime: PUInt64): Error; stdcall;
  PKS_getSystemTime = ^TKS_getSystemTime;
var
  FKS_getSystemTime: TKS_getSystemTime;
        
function KS_getSystemTime(pSystemTime: PUInt64): Error; stdcall;
begin
  if @FKS_getSystemTime = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getSystemTime := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getSystemTime := PKS_getSystemTime(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getSystemTime'));
    if @FKS_getSystemTime = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getSystemTime not found in DLL');
      KS_getSystemTime := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getSystemTime := FKS_getSystemTime(pSystemTime);
end;

//------ KS_getMachineTime ------
type
  TKS_getMachineTime = function (pMachineTime: PUInt64): Error; stdcall;
  PKS_getMachineTime = ^TKS_getMachineTime;
var
  FKS_getMachineTime: TKS_getMachineTime;
        
function KS_getMachineTime(pMachineTime: PUInt64): Error; stdcall;
begin
  if @FKS_getMachineTime = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getMachineTime := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getMachineTime := PKS_getMachineTime(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getMachineTime'));
    if @FKS_getMachineTime = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getMachineTime not found in DLL');
      KS_getMachineTime := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getMachineTime := FKS_getMachineTime(pMachineTime);
end;

//------ KS_getAbsTime ------
type
  TKS_getAbsTime = function (pAbsTime: PUInt64): Error; stdcall;
  PKS_getAbsTime = ^TKS_getAbsTime;
var
  FKS_getAbsTime: TKS_getAbsTime;
        
function KS_getAbsTime(pAbsTime: PUInt64): Error; stdcall;
begin
  if @FKS_getAbsTime = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getAbsTime := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getAbsTime := PKS_getAbsTime(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getAbsTime'));
    if @FKS_getAbsTime = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getAbsTime not found in DLL');
      KS_getAbsTime := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getAbsTime := FKS_getAbsTime(pAbsTime);
end;

//------ KS_getClock ------
type
  TKS_getClock = function (pClock: PUInt64; clockId: int): Error; stdcall;
  PKS_getClock = ^TKS_getClock;
var
  FKS_getClock: TKS_getClock;
        
function KS_getClock(pClock: PUInt64; clockId: int): Error; stdcall;
begin
  if @FKS_getClock = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getClock := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getClock := PKS_getClock(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getClock'));
    if @FKS_getClock = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getClock not found in DLL');
      KS_getClock := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getClock := FKS_getClock(pClock, clockId);
end;

//------ KS_getClockSource ------
type
  TKS_getClockSource = function (pClockSource: PKSClockSource; clockId: int): Error; stdcall;
  PKS_getClockSource = ^TKS_getClockSource;
var
  FKS_getClockSource: TKS_getClockSource;
        
function KS_getClockSource(pClockSource: PKSClockSource; clockId: int): Error; stdcall;
begin
  if @FKS_getClockSource = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getClockSource := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getClockSource := PKS_getClockSource(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getClockSource'));
    if @FKS_getClockSource = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getClockSource not found in DLL');
      KS_getClockSource := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getClockSource := FKS_getClockSource(pClockSource, clockId);
end;

//------ KS_setClockSource ------
type
  TKS_setClockSource = function (pClockSource: PKSClockSource; clockId: int): Error; stdcall;
  PKS_setClockSource = ^TKS_setClockSource;
var
  FKS_setClockSource: TKS_setClockSource;
        
function KS_setClockSource(pClockSource: PKSClockSource; clockId: int): Error; stdcall;
begin
  if @FKS_setClockSource = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setClockSource := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setClockSource := PKS_setClockSource(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setClockSource'));
    if @FKS_setClockSource = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setClockSource not found in DLL');
      KS_setClockSource := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setClockSource := FKS_setClockSource(pClockSource, clockId);
end;

//------ KS_microDelay ------
type
  TKS_microDelay = function (delay: int): Error; stdcall;
  PKS_microDelay = ^TKS_microDelay;
var
  FKS_microDelay: TKS_microDelay;
        
function KS_microDelay(delay: int): Error; stdcall;
begin
  if @FKS_microDelay = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_microDelay := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_microDelay := PKS_microDelay(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_microDelay'));
    if @FKS_microDelay = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_microDelay not found in DLL');
      KS_microDelay := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_microDelay := FKS_microDelay(delay);
end;

//------ KS_convertClock ------
type
  TKS_convertClock = function (pValue: PUInt64; clockIdFrom: int; clockIdTo: int; flags: int): Error; stdcall;
  PKS_convertClock = ^TKS_convertClock;
var
  FKS_convertClock: TKS_convertClock;
        
function KS_convertClock(pValue: PUInt64; clockIdFrom: int; clockIdTo: int; flags: int): Error; stdcall;
begin
  if @FKS_convertClock = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_convertClock := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_convertClock := PKS_convertClock(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_convertClock'));
    if @FKS_convertClock = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_convertClock not found in DLL');
      KS_convertClock := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_convertClock := FKS_convertClock(pValue, clockIdFrom, clockIdTo, flags);
end;

//------ _initClockModule ------
procedure _initClockModule();
begin
  _registerKernelAddress('KS_calibrateMachineTime', @KS_calibrateMachineTime);
  _registerKernelAddress('KS_getSystemTicks', @KS_getSystemTicks);
  _registerKernelAddress('KS_getSystemTime', @KS_getSystemTime);
  _registerKernelAddress('KS_getMachineTime', @KS_getMachineTime);
  _registerKernelAddress('KS_getAbsTime', @KS_getAbsTime);
  _registerKernelAddress('KS_getClock', @KS_getClock);
  _registerKernelAddress('KS_getClockSource', @KS_getClockSource);
  _registerKernelAddress('KS_setClockSource', @KS_setClockSource);
  _registerKernelAddress('KS_microDelay', @KS_microDelay);
  _registerKernelAddress('KS_convertClock', @KS_convertClock);
end;

{$DEFINE KS_CLOCK_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// System Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_installSystemHandler ------
type
  TKS_installSystemHandler = function (eventMask: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installSystemHandler = ^TKS_installSystemHandler;
var
  FKS_installSystemHandler: TKS_installSystemHandler;
        
function KS_installSystemHandler(eventMask: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installSystemHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installSystemHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installSystemHandler := PKS_installSystemHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installSystemHandler'));
    if @FKS_installSystemHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installSystemHandler not found in DLL');
      KS_installSystemHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installSystemHandler := FKS_installSystemHandler(eventMask, hSignal, flags);
end;

//------ KS_removeSystemHandler ------
type
  TKS_removeSystemHandler = function (eventMask: int): Error; stdcall;
  PKS_removeSystemHandler = ^TKS_removeSystemHandler;
var
  FKS_removeSystemHandler: TKS_removeSystemHandler;
        
function KS_removeSystemHandler(eventMask: int): Error; stdcall;
begin
  if @FKS_removeSystemHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeSystemHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeSystemHandler := PKS_removeSystemHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeSystemHandler'));
    if @FKS_removeSystemHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeSystemHandler not found in DLL');
      KS_removeSystemHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeSystemHandler := FKS_removeSystemHandler(eventMask);
end;

//--------------------------------------------------------------------------------------------------------------
// Device Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createDevice ------
type
  TKS_createDevice = function (phDevice: PKSHandle; name: PAnsiChar; pFunctions: PKSDeviceFunctions; flags: int): Error; stdcall;
  PKS_createDevice = ^TKS_createDevice;
var
  FKS_createDevice: TKS_createDevice;
        
function KS_createDevice(phDevice: PKSHandle; name: PAnsiChar; pFunctions: PKSDeviceFunctions; flags: int): Error; stdcall;
begin
  if @FKS_createDevice = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createDevice := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createDevice := PKS_createDevice(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createDevice'));
    if @FKS_createDevice = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createDevice not found in DLL');
      KS_createDevice := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createDevice := FKS_createDevice(phDevice, name, pFunctions, flags);
end;

//------ KS_closeDevice ------
type
  TKS_closeDevice = function (hDevice: KSHandle): Error; stdcall;
  PKS_closeDevice = ^TKS_closeDevice;
var
  FKS_closeDevice: TKS_closeDevice;
        
function KS_closeDevice(hDevice: KSHandle): Error; stdcall;
begin
  if @FKS_closeDevice = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeDevice := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeDevice := PKS_closeDevice(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeDevice'));
    if @FKS_closeDevice = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeDevice not found in DLL');
      KS_closeDevice := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeDevice := FKS_closeDevice(hDevice);
end;

//------ KS_resumeDevice ------
type
  TKS_resumeDevice = function (hDevice: KSHandle; hRequest: KSHandle; pBuffer: PByte; size: int; returnCode: int; flags: int): Error; stdcall;
  PKS_resumeDevice = ^TKS_resumeDevice;
var
  FKS_resumeDevice: TKS_resumeDevice;
        
function KS_resumeDevice(hDevice: KSHandle; hRequest: KSHandle; pBuffer: PByte; size: int; returnCode: int; flags: int): Error; stdcall;
begin
  if @FKS_resumeDevice = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_resumeDevice := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_resumeDevice := PKS_resumeDevice(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_resumeDevice'));
    if @FKS_resumeDevice = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_resumeDevice not found in DLL');
      KS_resumeDevice := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_resumeDevice := FKS_resumeDevice(hDevice, hRequest, pBuffer, size, returnCode, flags);
end;

//------ _initDeviceModule ------
procedure _initDeviceModule();
begin
  _registerKernelAddress('KS_resumeDevice', @KS_resumeDevice);
end;

{$DEFINE KS_DEVICE_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Keyboard Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createKeyHandler ------
type
  TKS_createKeyHandler = function (mask: uint; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_createKeyHandler = ^TKS_createKeyHandler;
var
  FKS_createKeyHandler: TKS_createKeyHandler;
        
function KS_createKeyHandler(mask: uint; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_createKeyHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createKeyHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createKeyHandler := PKS_createKeyHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createKeyHandler'));
    if @FKS_createKeyHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createKeyHandler not found in DLL');
      KS_createKeyHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createKeyHandler := FKS_createKeyHandler(mask, hSignal, flags);
end;

//------ KS_removeKeyHandler ------
type
  TKS_removeKeyHandler = function (mask: uint): Error; stdcall;
  PKS_removeKeyHandler = ^TKS_removeKeyHandler;
var
  FKS_removeKeyHandler: TKS_removeKeyHandler;
        
function KS_removeKeyHandler(mask: uint): Error; stdcall;
begin
  if @FKS_removeKeyHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeKeyHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeKeyHandler := PKS_removeKeyHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeKeyHandler'));
    if @FKS_removeKeyHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeKeyHandler not found in DLL');
      KS_removeKeyHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeKeyHandler := FKS_removeKeyHandler(mask);
end;

//------ KS_simulateKeyStroke ------
type
  TKS_simulateKeyStroke = function (pKeys: PUInt): Error; stdcall;
  PKS_simulateKeyStroke = ^TKS_simulateKeyStroke;
var
  FKS_simulateKeyStroke: TKS_simulateKeyStroke;
        
function KS_simulateKeyStroke(pKeys: PUInt): Error; stdcall;
begin
  if @FKS_simulateKeyStroke = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_simulateKeyStroke := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_simulateKeyStroke := PKS_simulateKeyStroke(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_simulateKeyStroke'));
    if @FKS_simulateKeyStroke = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_simulateKeyStroke not found in DLL');
      KS_simulateKeyStroke := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_simulateKeyStroke := FKS_simulateKeyStroke(pKeys);
end;

//------ _initKeyboardModule ------
procedure _initKeyboardModule();
begin
  _registerKernelAddress('KS_simulateKeyStroke', @KS_simulateKeyStroke);
end;

{$DEFINE KS_KEYBOARD_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Interrupt Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createDeviceInterrupt ------
type
  TKS_createDeviceInterrupt = function (phInterrupt: PKSHandle; pInfo: PKSResourceInfoEx; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_createDeviceInterrupt = ^TKS_createDeviceInterrupt;
var
  FKS_createDeviceInterrupt: TKS_createDeviceInterrupt;
        
function KS_createDeviceInterrupt(phInterrupt: PKSHandle; pInfo: PKSResourceInfoEx; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_createDeviceInterrupt = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createDeviceInterrupt := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createDeviceInterrupt := PKS_createDeviceInterrupt(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createDeviceInterrupt'));
    if @FKS_createDeviceInterrupt = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createDeviceInterrupt not found in DLL');
      KS_createDeviceInterrupt := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createDeviceInterrupt := FKS_createDeviceInterrupt(phInterrupt, pInfo, hSignal, flags);
end;

//------ KS_createDeviceInterruptEx ------
type
  TKS_createDeviceInterruptEx = function (phInterrupt: PKSHandle; deviceName: PAnsiChar; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_createDeviceInterruptEx = ^TKS_createDeviceInterruptEx;
var
  FKS_createDeviceInterruptEx: TKS_createDeviceInterruptEx;
        
function KS_createDeviceInterruptEx(phInterrupt: PKSHandle; deviceName: PAnsiChar; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_createDeviceInterruptEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createDeviceInterruptEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createDeviceInterruptEx := PKS_createDeviceInterruptEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createDeviceInterruptEx'));
    if @FKS_createDeviceInterruptEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createDeviceInterruptEx not found in DLL');
      KS_createDeviceInterruptEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createDeviceInterruptEx := FKS_createDeviceInterruptEx(phInterrupt, deviceName, hSignal, flags);
end;

//------ KS_createInterrupt ------
type
  TKS_createInterrupt = function (phInterrupt: PKSHandle; irq: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_createInterrupt = ^TKS_createInterrupt;
var
  FKS_createInterrupt: TKS_createInterrupt;
        
function KS_createInterrupt(phInterrupt: PKSHandle; irq: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_createInterrupt = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createInterrupt := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createInterrupt := PKS_createInterrupt(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createInterrupt'));
    if @FKS_createInterrupt = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createInterrupt not found in DLL');
      KS_createInterrupt := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createInterrupt := FKS_createInterrupt(phInterrupt, irq, hSignal, flags);
end;

//------ KS_removeInterrupt ------
type
  TKS_removeInterrupt = function (hInterrupt: KSHandle): Error; stdcall;
  PKS_removeInterrupt = ^TKS_removeInterrupt;
var
  FKS_removeInterrupt: TKS_removeInterrupt;
        
function KS_removeInterrupt(hInterrupt: KSHandle): Error; stdcall;
begin
  if @FKS_removeInterrupt = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeInterrupt := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeInterrupt := PKS_removeInterrupt(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeInterrupt'));
    if @FKS_removeInterrupt = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeInterrupt not found in DLL');
      KS_removeInterrupt := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeInterrupt := FKS_removeInterrupt(hInterrupt);
end;

//------ KS_disableInterrupt ------
type
  TKS_disableInterrupt = function (hInterrupt: KSHandle): Error; stdcall;
  PKS_disableInterrupt = ^TKS_disableInterrupt;
var
  FKS_disableInterrupt: TKS_disableInterrupt;
        
function KS_disableInterrupt(hInterrupt: KSHandle): Error; stdcall;
begin
  if @FKS_disableInterrupt = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_disableInterrupt := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_disableInterrupt := PKS_disableInterrupt(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_disableInterrupt'));
    if @FKS_disableInterrupt = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_disableInterrupt not found in DLL');
      KS_disableInterrupt := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_disableInterrupt := FKS_disableInterrupt(hInterrupt);
end;

//------ KS_enableInterrupt ------
type
  TKS_enableInterrupt = function (hInterrupt: KSHandle): Error; stdcall;
  PKS_enableInterrupt = ^TKS_enableInterrupt;
var
  FKS_enableInterrupt: TKS_enableInterrupt;
        
function KS_enableInterrupt(hInterrupt: KSHandle): Error; stdcall;
begin
  if @FKS_enableInterrupt = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enableInterrupt := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enableInterrupt := PKS_enableInterrupt(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enableInterrupt'));
    if @FKS_enableInterrupt = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enableInterrupt not found in DLL');
      KS_enableInterrupt := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enableInterrupt := FKS_enableInterrupt(hInterrupt);
end;

//------ KS_simulateInterrupt ------
type
  TKS_simulateInterrupt = function (hInterrupt: KSHandle): Error; stdcall;
  PKS_simulateInterrupt = ^TKS_simulateInterrupt;
var
  FKS_simulateInterrupt: TKS_simulateInterrupt;
        
function KS_simulateInterrupt(hInterrupt: KSHandle): Error; stdcall;
begin
  if @FKS_simulateInterrupt = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_simulateInterrupt := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_simulateInterrupt := PKS_simulateInterrupt(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_simulateInterrupt'));
    if @FKS_simulateInterrupt = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_simulateInterrupt not found in DLL');
      KS_simulateInterrupt := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_simulateInterrupt := FKS_simulateInterrupt(hInterrupt);
end;

//------ KS_getInterruptState ------
type
  TKS_getInterruptState = function (hInterrupt: KSHandle; pState: PHandlerState): Error; stdcall;
  PKS_getInterruptState = ^TKS_getInterruptState;
var
  FKS_getInterruptState: TKS_getInterruptState;
        
function KS_getInterruptState(hInterrupt: KSHandle; pState: PHandlerState): Error; stdcall;
begin
  if @FKS_getInterruptState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getInterruptState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getInterruptState := PKS_getInterruptState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getInterruptState'));
    if @FKS_getInterruptState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getInterruptState not found in DLL');
      KS_getInterruptState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getInterruptState := FKS_getInterruptState(hInterrupt, pState);
end;

//--------------------------------------------------------------------------------------------------------------
// UART Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openUart ------
type
  TKS_openUart = function (phUart: PKSHandle; name: PAnsiChar; port: int; pProperties: PKSUartProperties; recvBufferLength: int; xmitBufferLength: int; flags: int): Error; stdcall;
  PKS_openUart = ^TKS_openUart;
var
  FKS_openUart: TKS_openUart;
        
function KS_openUart(phUart: PKSHandle; name: PAnsiChar; port: int; pProperties: PKSUartProperties; recvBufferLength: int; xmitBufferLength: int; flags: int): Error; stdcall;
begin
  if @FKS_openUart = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openUart := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openUart := PKS_openUart(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openUart'));
    if @FKS_openUart = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openUart not found in DLL');
      KS_openUart := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openUart := FKS_openUart(phUart, name, port, pProperties, recvBufferLength, xmitBufferLength, flags);
end;

//------ KS_openUartEx ------
type
  TKS_openUartEx = function (phUart: PKSHandle; hConnection: KSHandle; port: int; pProperties: PKSUartProperties; recvBufferLength: int; xmitBufferLength: int; flags: int): Error; stdcall;
  PKS_openUartEx = ^TKS_openUartEx;
var
  FKS_openUartEx: TKS_openUartEx;
        
function KS_openUartEx(phUart: PKSHandle; hConnection: KSHandle; port: int; pProperties: PKSUartProperties; recvBufferLength: int; xmitBufferLength: int; flags: int): Error; stdcall;
begin
  if @FKS_openUartEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openUartEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openUartEx := PKS_openUartEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openUartEx'));
    if @FKS_openUartEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openUartEx not found in DLL');
      KS_openUartEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openUartEx := FKS_openUartEx(phUart, hConnection, port, pProperties, recvBufferLength, xmitBufferLength, flags);
end;

//------ KS_closeUart ------
type
  TKS_closeUart = function (hUart: KSHandle; flags: int): Error; stdcall;
  PKS_closeUart = ^TKS_closeUart;
var
  FKS_closeUart: TKS_closeUart;
        
function KS_closeUart(hUart: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeUart = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeUart := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeUart := PKS_closeUart(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeUart'));
    if @FKS_closeUart = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeUart not found in DLL');
      KS_closeUart := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeUart := FKS_closeUart(hUart, flags);
end;

//------ KS_xmitUart ------
type
  TKS_xmitUart = function (hUart: KSHandle; value: int; flags: int): Error; stdcall;
  PKS_xmitUart = ^TKS_xmitUart;
var
  FKS_xmitUart: TKS_xmitUart;
        
function KS_xmitUart(hUart: KSHandle; value: int; flags: int): Error; stdcall;
begin
  if @FKS_xmitUart = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitUart := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitUart := PKS_xmitUart(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitUart'));
    if @FKS_xmitUart = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitUart not found in DLL');
      KS_xmitUart := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitUart := FKS_xmitUart(hUart, value, flags);
end;

//------ KS_recvUart ------
type
  TKS_recvUart = function (hUart: KSHandle; pValue: PInt; pLineError: PInt; flags: int): Error; stdcall;
  PKS_recvUart = ^TKS_recvUart;
var
  FKS_recvUart: TKS_recvUart;
        
function KS_recvUart(hUart: KSHandle; pValue: PInt; pLineError: PInt; flags: int): Error; stdcall;
begin
  if @FKS_recvUart = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvUart := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvUart := PKS_recvUart(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvUart'));
    if @FKS_recvUart = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvUart not found in DLL');
      KS_recvUart := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvUart := FKS_recvUart(hUart, pValue, pLineError, flags);
end;

//------ KS_xmitUartBlock ------
type
  TKS_xmitUartBlock = function (hUart: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_xmitUartBlock = ^TKS_xmitUartBlock;
var
  FKS_xmitUartBlock: TKS_xmitUartBlock;
        
function KS_xmitUartBlock(hUart: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_xmitUartBlock = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitUartBlock := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitUartBlock := PKS_xmitUartBlock(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitUartBlock'));
    if @FKS_xmitUartBlock = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitUartBlock not found in DLL');
      KS_xmitUartBlock := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitUartBlock := FKS_xmitUartBlock(hUart, pBytes, length, pLength, flags);
end;

//------ KS_recvUartBlock ------
type
  TKS_recvUartBlock = function (hUart: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_recvUartBlock = ^TKS_recvUartBlock;
var
  FKS_recvUartBlock: TKS_recvUartBlock;
        
function KS_recvUartBlock(hUart: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_recvUartBlock = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvUartBlock := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvUartBlock := PKS_recvUartBlock(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvUartBlock'));
    if @FKS_recvUartBlock = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvUartBlock not found in DLL');
      KS_recvUartBlock := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvUartBlock := FKS_recvUartBlock(hUart, pBytes, length, pLength, flags);
end;

//------ KS_installUartHandler ------
type
  TKS_installUartHandler = function (hUart: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installUartHandler = ^TKS_installUartHandler;
var
  FKS_installUartHandler: TKS_installUartHandler;
        
function KS_installUartHandler(hUart: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installUartHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installUartHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installUartHandler := PKS_installUartHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installUartHandler'));
    if @FKS_installUartHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installUartHandler not found in DLL');
      KS_installUartHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installUartHandler := FKS_installUartHandler(hUart, eventCode, hSignal, flags);
end;

//------ KS_getUartState ------
type
  TKS_getUartState = function (hUart: KSHandle; pUartState: PKSUartState; flags: int): Error; stdcall;
  PKS_getUartState = ^TKS_getUartState;
var
  FKS_getUartState: TKS_getUartState;
        
function KS_getUartState(hUart: KSHandle; pUartState: PKSUartState; flags: int): Error; stdcall;
begin
  if @FKS_getUartState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getUartState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getUartState := PKS_getUartState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getUartState'));
    if @FKS_getUartState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getUartState not found in DLL');
      KS_getUartState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getUartState := FKS_getUartState(hUart, pUartState, flags);
end;

//------ KS_execUartCommand ------
type
  TKS_execUartCommand = function (hUart: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execUartCommand = ^TKS_execUartCommand;
var
  FKS_execUartCommand: TKS_execUartCommand;
        
function KS_execUartCommand(hUart: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execUartCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execUartCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execUartCommand := PKS_execUartCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execUartCommand'));
    if @FKS_execUartCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execUartCommand not found in DLL');
      KS_execUartCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execUartCommand := FKS_execUartCommand(hUart, command, pParam, flags);
end;

//------ _initUARTModule ------
procedure _initUARTModule();
begin
  _registerKernelAddress('KS_xmitUart', @KS_xmitUart);
  _registerKernelAddress('KS_recvUart', @KS_recvUart);
  _registerKernelAddress('KS_xmitUartBlock', @KS_xmitUartBlock);
  _registerKernelAddress('KS_recvUartBlock', @KS_recvUartBlock);
  _registerKernelAddress('KS_installUartHandler', @KS_installUartHandler);
  _registerKernelAddress('KS_getUartState', @KS_getUartState);
  _registerKernelAddress('KS_execUartCommand', @KS_execUartCommand);
end;

{$DEFINE KS_UART_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// COMM Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openComm ------
type
  TKS_openComm = function (phComm: PKSHandle; name: PAnsiChar; pProperties: PKSCommProperties; recvBufferLength: int; xmitBufferLength: int; flags: int): Error; stdcall;
  PKS_openComm = ^TKS_openComm;
var
  FKS_openComm: TKS_openComm;
        
function KS_openComm(phComm: PKSHandle; name: PAnsiChar; pProperties: PKSCommProperties; recvBufferLength: int; xmitBufferLength: int; flags: int): Error; stdcall;
begin
  if @FKS_openComm = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openComm := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openComm := PKS_openComm(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openComm'));
    if @FKS_openComm = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openComm not found in DLL');
      KS_openComm := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openComm := FKS_openComm(phComm, name, pProperties, recvBufferLength, xmitBufferLength, flags);
end;

//------ KS_closeComm ------
type
  TKS_closeComm = function (hComm: KSHandle; flags: int): Error; stdcall;
  PKS_closeComm = ^TKS_closeComm;
var
  FKS_closeComm: TKS_closeComm;
        
function KS_closeComm(hComm: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeComm = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeComm := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeComm := PKS_closeComm(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeComm'));
    if @FKS_closeComm = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeComm not found in DLL');
      KS_closeComm := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeComm := FKS_closeComm(hComm, flags);
end;

//------ KS_xmitComm ------
type
  TKS_xmitComm = function (hComm: KSHandle; value: int; flags: int): Error; stdcall;
  PKS_xmitComm = ^TKS_xmitComm;
var
  FKS_xmitComm: TKS_xmitComm;
        
function KS_xmitComm(hComm: KSHandle; value: int; flags: int): Error; stdcall;
begin
  if @FKS_xmitComm = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitComm := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitComm := PKS_xmitComm(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitComm'));
    if @FKS_xmitComm = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitComm not found in DLL');
      KS_xmitComm := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitComm := FKS_xmitComm(hComm, value, flags);
end;

//------ KS_recvComm ------
type
  TKS_recvComm = function (hComm: KSHandle; pValue: PInt; pLineError: PInt; flags: int): Error; stdcall;
  PKS_recvComm = ^TKS_recvComm;
var
  FKS_recvComm: TKS_recvComm;
        
function KS_recvComm(hComm: KSHandle; pValue: PInt; pLineError: PInt; flags: int): Error; stdcall;
begin
  if @FKS_recvComm = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvComm := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvComm := PKS_recvComm(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvComm'));
    if @FKS_recvComm = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvComm not found in DLL');
      KS_recvComm := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvComm := FKS_recvComm(hComm, pValue, pLineError, flags);
end;

//------ KS_xmitCommBlock ------
type
  TKS_xmitCommBlock = function (hComm: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_xmitCommBlock = ^TKS_xmitCommBlock;
var
  FKS_xmitCommBlock: TKS_xmitCommBlock;
        
function KS_xmitCommBlock(hComm: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_xmitCommBlock = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitCommBlock := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitCommBlock := PKS_xmitCommBlock(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitCommBlock'));
    if @FKS_xmitCommBlock = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitCommBlock not found in DLL');
      KS_xmitCommBlock := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitCommBlock := FKS_xmitCommBlock(hComm, pBytes, length, pLength, flags);
end;

//------ KS_recvCommBlock ------
type
  TKS_recvCommBlock = function (hComm: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_recvCommBlock = ^TKS_recvCommBlock;
var
  FKS_recvCommBlock: TKS_recvCommBlock;
        
function KS_recvCommBlock(hComm: KSHandle; pBytes: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_recvCommBlock = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvCommBlock := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvCommBlock := PKS_recvCommBlock(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvCommBlock'));
    if @FKS_recvCommBlock = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvCommBlock not found in DLL');
      KS_recvCommBlock := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvCommBlock := FKS_recvCommBlock(hComm, pBytes, length, pLength, flags);
end;

//------ KS_installCommHandler ------
type
  TKS_installCommHandler = function (hComm: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installCommHandler = ^TKS_installCommHandler;
var
  FKS_installCommHandler: TKS_installCommHandler;
        
function KS_installCommHandler(hComm: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installCommHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installCommHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installCommHandler := PKS_installCommHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installCommHandler'));
    if @FKS_installCommHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installCommHandler not found in DLL');
      KS_installCommHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installCommHandler := FKS_installCommHandler(hComm, eventCode, hSignal, flags);
end;

//------ KS_getCommState ------
type
  TKS_getCommState = function (hComm: KSHandle; pCommState: PKSCommState; flags: int): Error; stdcall;
  PKS_getCommState = ^TKS_getCommState;
var
  FKS_getCommState: TKS_getCommState;
        
function KS_getCommState(hComm: KSHandle; pCommState: PKSCommState; flags: int): Error; stdcall;
begin
  if @FKS_getCommState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getCommState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getCommState := PKS_getCommState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getCommState'));
    if @FKS_getCommState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getCommState not found in DLL');
      KS_getCommState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getCommState := FKS_getCommState(hComm, pCommState, flags);
end;

//------ KS_execCommCommand ------
type
  TKS_execCommCommand = function (hComm: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execCommCommand = ^TKS_execCommCommand;
var
  FKS_execCommCommand: TKS_execCommCommand;
        
function KS_execCommCommand(hComm: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execCommCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execCommCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execCommCommand := PKS_execCommCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execCommCommand'));
    if @FKS_execCommCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execCommCommand not found in DLL');
      KS_execCommCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execCommCommand := FKS_execCommCommand(hComm, command, pParam, flags);
end;

//--------------------------------------------------------------------------------------------------------------
// USB Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openUsb ------
type
  TKS_openUsb = function (phUsbDevice: PKSHandle; deviceName: PAnsiChar; flags: int): Error; stdcall;
  PKS_openUsb = ^TKS_openUsb;
var
  FKS_openUsb: TKS_openUsb;
        
function KS_openUsb(phUsbDevice: PKSHandle; deviceName: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_openUsb = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openUsb := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openUsb := PKS_openUsb(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openUsb'));
    if @FKS_openUsb = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openUsb not found in DLL');
      KS_openUsb := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openUsb := FKS_openUsb(phUsbDevice, deviceName, flags);
end;

//------ KS_openUsbEndPoint ------
type
  TKS_openUsbEndPoint = function (hUsbDevice: KSHandle; phUsbEndPoint: PKSHandle; endPointAddress: int; packetSize: int; packetCount: int; flags: int): Error; stdcall;
  PKS_openUsbEndPoint = ^TKS_openUsbEndPoint;
var
  FKS_openUsbEndPoint: TKS_openUsbEndPoint;
        
function KS_openUsbEndPoint(hUsbDevice: KSHandle; phUsbEndPoint: PKSHandle; endPointAddress: int; packetSize: int; packetCount: int; flags: int): Error; stdcall;
begin
  if @FKS_openUsbEndPoint = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openUsbEndPoint := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openUsbEndPoint := PKS_openUsbEndPoint(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openUsbEndPoint'));
    if @FKS_openUsbEndPoint = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openUsbEndPoint not found in DLL');
      KS_openUsbEndPoint := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openUsbEndPoint := FKS_openUsbEndPoint(hUsbDevice, phUsbEndPoint, endPointAddress, packetSize, packetCount, flags);
end;

//------ KS_closeUsb ------
type
  TKS_closeUsb = function (hUsb: KSHandle; flags: int): Error; stdcall;
  PKS_closeUsb = ^TKS_closeUsb;
var
  FKS_closeUsb: TKS_closeUsb;
        
function KS_closeUsb(hUsb: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeUsb = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeUsb := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeUsb := PKS_closeUsb(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeUsb'));
    if @FKS_closeUsb = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeUsb not found in DLL');
      KS_closeUsb := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeUsb := FKS_closeUsb(hUsb, flags);
end;

//------ KS_xmitUsb ------
type
  TKS_xmitUsb = function (hUsb: KSHandle; pData: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_xmitUsb = ^TKS_xmitUsb;
var
  FKS_xmitUsb: TKS_xmitUsb;
        
function KS_xmitUsb(hUsb: KSHandle; pData: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_xmitUsb = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitUsb := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitUsb := PKS_xmitUsb(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitUsb'));
    if @FKS_xmitUsb = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitUsb not found in DLL');
      KS_xmitUsb := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitUsb := FKS_xmitUsb(hUsb, pData, length, pLength, flags);
end;

//------ KS_recvUsb ------
type
  TKS_recvUsb = function (hUsb: KSHandle; pData: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_recvUsb = ^TKS_recvUsb;
var
  FKS_recvUsb: TKS_recvUsb;
        
function KS_recvUsb(hUsb: KSHandle; pData: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_recvUsb = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvUsb := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvUsb := PKS_recvUsb(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvUsb'));
    if @FKS_recvUsb = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvUsb not found in DLL');
      KS_recvUsb := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvUsb := FKS_recvUsb(hUsb, pData, length, pLength, flags);
end;

//------ KS_installUsbHandler ------
type
  TKS_installUsbHandler = function (hUsb: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installUsbHandler = ^TKS_installUsbHandler;
var
  FKS_installUsbHandler: TKS_installUsbHandler;
        
function KS_installUsbHandler(hUsb: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installUsbHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installUsbHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installUsbHandler := PKS_installUsbHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installUsbHandler'));
    if @FKS_installUsbHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installUsbHandler not found in DLL');
      KS_installUsbHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installUsbHandler := FKS_installUsbHandler(hUsb, eventCode, hSignal, flags);
end;

//------ KS_getUsbState ------
type
  TKS_getUsbState = function (hUsb: KSHandle; pUsbState: PKSUsbState; flags: int): Error; stdcall;
  PKS_getUsbState = ^TKS_getUsbState;
var
  FKS_getUsbState: TKS_getUsbState;
        
function KS_getUsbState(hUsb: KSHandle; pUsbState: PKSUsbState; flags: int): Error; stdcall;
begin
  if @FKS_getUsbState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getUsbState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getUsbState := PKS_getUsbState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getUsbState'));
    if @FKS_getUsbState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getUsbState not found in DLL');
      KS_getUsbState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getUsbState := FKS_getUsbState(hUsb, pUsbState, flags);
end;

//------ KS_execUsbCommand ------
type
  TKS_execUsbCommand = function (hUsb: KSHandle; command: int; index: int; pData: Pointer; size: int; flags: int): Error; stdcall;
  PKS_execUsbCommand = ^TKS_execUsbCommand;
var
  FKS_execUsbCommand: TKS_execUsbCommand;
        
function KS_execUsbCommand(hUsb: KSHandle; command: int; index: int; pData: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_execUsbCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execUsbCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execUsbCommand := PKS_execUsbCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execUsbCommand'));
    if @FKS_execUsbCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execUsbCommand not found in DLL');
      KS_execUsbCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execUsbCommand := FKS_execUsbCommand(hUsb, command, index, pData, size, flags);
end;

//------ _initUSBModule ------
procedure _initUSBModule();
begin
  _registerKernelAddress('KS_xmitUsb', @KS_xmitUsb);
  _registerKernelAddress('KS_recvUsb', @KS_recvUsb);
  _registerKernelAddress('KS_installUsbHandler', @KS_installUsbHandler);
  _registerKernelAddress('KS_getUsbState', @KS_getUsbState);
  _registerKernelAddress('KS_execUsbCommand', @KS_execUsbCommand);
end;

{$DEFINE KS_USB_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// XHCI Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openXhciController ------
type
  TKS_openXhciController = function (phXhciController: PKSHandle; deviceName: PAnsiChar; flags: int): Error; stdcall;
  PKS_openXhciController = ^TKS_openXhciController;
var
  FKS_openXhciController: TKS_openXhciController;
        
function KS_openXhciController(phXhciController: PKSHandle; deviceName: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_openXhciController = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openXhciController := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openXhciController := PKS_openXhciController(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openXhciController'));
    if @FKS_openXhciController = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openXhciController not found in DLL');
      KS_openXhciController := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openXhciController := FKS_openXhciController(phXhciController, deviceName, flags);
end;

//------ KS_openXhciDevice ------
type
  TKS_openXhciDevice = function (phXhciDevice: PKSHandle; deviceName: PAnsiChar; flags: int): Error; stdcall;
  PKS_openXhciDevice = ^TKS_openXhciDevice;
var
  FKS_openXhciDevice: TKS_openXhciDevice;
        
function KS_openXhciDevice(phXhciDevice: PKSHandle; deviceName: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_openXhciDevice = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openXhciDevice := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openXhciDevice := PKS_openXhciDevice(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openXhciDevice'));
    if @FKS_openXhciDevice = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openXhciDevice not found in DLL');
      KS_openXhciDevice := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openXhciDevice := FKS_openXhciDevice(phXhciDevice, deviceName, flags);
end;

//------ KS_openXhciEndPoint ------
type
  TKS_openXhciEndPoint = function (hXhciDevice: KSHandle; phXhciEndPoint: PKSHandle; endPointAddress: int; packetSize: int; packetCount: int; flags: int): Error; stdcall;
  PKS_openXhciEndPoint = ^TKS_openXhciEndPoint;
var
  FKS_openXhciEndPoint: TKS_openXhciEndPoint;
        
function KS_openXhciEndPoint(hXhciDevice: KSHandle; phXhciEndPoint: PKSHandle; endPointAddress: int; packetSize: int; packetCount: int; flags: int): Error; stdcall;
begin
  if @FKS_openXhciEndPoint = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openXhciEndPoint := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openXhciEndPoint := PKS_openXhciEndPoint(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openXhciEndPoint'));
    if @FKS_openXhciEndPoint = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openXhciEndPoint not found in DLL');
      KS_openXhciEndPoint := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openXhciEndPoint := FKS_openXhciEndPoint(hXhciDevice, phXhciEndPoint, endPointAddress, packetSize, packetCount, flags);
end;

//------ KS_closeXhci ------
type
  TKS_closeXhci = function (hXhci: KSHandle; flags: int): Error; stdcall;
  PKS_closeXhci = ^TKS_closeXhci;
var
  FKS_closeXhci: TKS_closeXhci;
        
function KS_closeXhci(hXhci: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeXhci = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeXhci := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeXhci := PKS_closeXhci(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeXhci'));
    if @FKS_closeXhci = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeXhci not found in DLL');
      KS_closeXhci := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeXhci := FKS_closeXhci(hXhci, flags);
end;

//------ KS_requestXhciPacket ------
type
  TKS_requestXhciPacket = function (hXhciEndPoint: KSHandle; ppXhciPacket: PPointer; flags: int): Error; stdcall;
  PKS_requestXhciPacket = ^TKS_requestXhciPacket;
var
  FKS_requestXhciPacket: TKS_requestXhciPacket;
        
function KS_requestXhciPacket(hXhciEndPoint: KSHandle; ppXhciPacket: PPointer; flags: int): Error; stdcall;
begin
  if @FKS_requestXhciPacket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_requestXhciPacket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_requestXhciPacket := PKS_requestXhciPacket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_requestXhciPacket'));
    if @FKS_requestXhciPacket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_requestXhciPacket not found in DLL');
      KS_requestXhciPacket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_requestXhciPacket := FKS_requestXhciPacket(hXhciEndPoint, ppXhciPacket, flags);
end;

//------ KS_releaseXhciPacket ------
type
  TKS_releaseXhciPacket = function (hXhciEndPoint: KSHandle; pXhciPacket: Pointer; flags: int): Error; stdcall;
  PKS_releaseXhciPacket = ^TKS_releaseXhciPacket;
var
  FKS_releaseXhciPacket: TKS_releaseXhciPacket;
        
function KS_releaseXhciPacket(hXhciEndPoint: KSHandle; pXhciPacket: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_releaseXhciPacket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_releaseXhciPacket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_releaseXhciPacket := PKS_releaseXhciPacket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_releaseXhciPacket'));
    if @FKS_releaseXhciPacket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_releaseXhciPacket not found in DLL');
      KS_releaseXhciPacket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_releaseXhciPacket := FKS_releaseXhciPacket(hXhciEndPoint, pXhciPacket, flags);
end;

//------ KS_xmitXhciPacket ------
type
  TKS_xmitXhciPacket = function (hXhciEndPoint: KSHandle; pXhciPacket: Pointer; size: int; flags: int): Error; stdcall;
  PKS_xmitXhciPacket = ^TKS_xmitXhciPacket;
var
  FKS_xmitXhciPacket: TKS_xmitXhciPacket;
        
function KS_xmitXhciPacket(hXhciEndPoint: KSHandle; pXhciPacket: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_xmitXhciPacket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitXhciPacket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitXhciPacket := PKS_xmitXhciPacket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitXhciPacket'));
    if @FKS_xmitXhciPacket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitXhciPacket not found in DLL');
      KS_xmitXhciPacket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitXhciPacket := FKS_xmitXhciPacket(hXhciEndPoint, pXhciPacket, size, flags);
end;

//------ KS_recvXhciPacket ------
type
  TKS_recvXhciPacket = function (hXhciEndPoint: KSHandle; ppXhciPacket: PPointer; pSize: PInt; flags: int): Error; stdcall;
  PKS_recvXhciPacket = ^TKS_recvXhciPacket;
var
  FKS_recvXhciPacket: TKS_recvXhciPacket;
        
function KS_recvXhciPacket(hXhciEndPoint: KSHandle; ppXhciPacket: PPointer; pSize: PInt; flags: int): Error; stdcall;
begin
  if @FKS_recvXhciPacket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvXhciPacket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvXhciPacket := PKS_recvXhciPacket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvXhciPacket'));
    if @FKS_recvXhciPacket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvXhciPacket not found in DLL');
      KS_recvXhciPacket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvXhciPacket := FKS_recvXhciPacket(hXhciEndPoint, ppXhciPacket, pSize, flags);
end;

//------ KS_sendXhciControlRequest ------
type
  TKS_sendXhciControlRequest = function (hXhciDevice: KSHandle; pRequestData: PKSXhciControlRequest; pData: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_sendXhciControlRequest = ^TKS_sendXhciControlRequest;
var
  FKS_sendXhciControlRequest: TKS_sendXhciControlRequest;
        
function KS_sendXhciControlRequest(hXhciDevice: KSHandle; pRequestData: PKSXhciControlRequest; pData: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_sendXhciControlRequest = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_sendXhciControlRequest := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_sendXhciControlRequest := PKS_sendXhciControlRequest(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_sendXhciControlRequest'));
    if @FKS_sendXhciControlRequest = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_sendXhciControlRequest not found in DLL');
      KS_sendXhciControlRequest := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_sendXhciControlRequest := FKS_sendXhciControlRequest(hXhciDevice, pRequestData, pData, length, pLength, flags);
end;

//------ KS_installXhciHandler ------
type
  TKS_installXhciHandler = function (hXhci: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installXhciHandler = ^TKS_installXhciHandler;
var
  FKS_installXhciHandler: TKS_installXhciHandler;
        
function KS_installXhciHandler(hXhci: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installXhciHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installXhciHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installXhciHandler := PKS_installXhciHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installXhciHandler'));
    if @FKS_installXhciHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installXhciHandler not found in DLL');
      KS_installXhciHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installXhciHandler := FKS_installXhciHandler(hXhci, eventCode, hSignal, flags);
end;

//------ KS_getXhciEndPointState ------
type
  TKS_getXhciEndPointState = function (hXhciEndPoint: KSHandle; pState: PKSXhciEndPointState; flags: int): Error; stdcall;
  PKS_getXhciEndPointState = ^TKS_getXhciEndPointState;
var
  FKS_getXhciEndPointState: TKS_getXhciEndPointState;
        
function KS_getXhciEndPointState(hXhciEndPoint: KSHandle; pState: PKSXhciEndPointState; flags: int): Error; stdcall;
begin
  if @FKS_getXhciEndPointState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getXhciEndPointState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getXhciEndPointState := PKS_getXhciEndPointState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getXhciEndPointState'));
    if @FKS_getXhciEndPointState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getXhciEndPointState not found in DLL');
      KS_getXhciEndPointState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getXhciEndPointState := FKS_getXhciEndPointState(hXhciEndPoint, pState, flags);
end;

//------ KS_execXhciCommand ------
type
  TKS_execXhciCommand = function (hXhci: KSHandle; command: int; index: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execXhciCommand = ^TKS_execXhciCommand;
var
  FKS_execXhciCommand: TKS_execXhciCommand;
        
function KS_execXhciCommand(hXhci: KSHandle; command: int; index: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execXhciCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execXhciCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execXhciCommand := PKS_execXhciCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execXhciCommand'));
    if @FKS_execXhciCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execXhciCommand not found in DLL');
      KS_execXhciCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execXhciCommand := FKS_execXhciCommand(hXhci, command, index, pParam, flags);
end;

//------ _initXHCIModule ------
procedure _initXHCIModule();
begin
  _registerKernelAddress('KS_requestXhciPacket', @KS_requestXhciPacket);
  _registerKernelAddress('KS_releaseXhciPacket', @KS_releaseXhciPacket);
  _registerKernelAddress('KS_xmitXhciPacket', @KS_xmitXhciPacket);
  _registerKernelAddress('KS_recvXhciPacket', @KS_recvXhciPacket);
  _registerKernelAddress('KS_sendXhciControlRequest', @KS_sendXhciControlRequest);
  _registerKernelAddress('KS_installXhciHandler', @KS_installXhciHandler);
  _registerKernelAddress('KS_getXhciEndPointState', @KS_getXhciEndPointState);
  _registerKernelAddress('KS_execXhciCommand', @KS_execXhciCommand);
end;

{$DEFINE KS_XHCI_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// RealTime Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createTimer ------
type
  TKS_createTimer = function (phTimer: PKSHandle; delay: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_createTimer = ^TKS_createTimer;
var
  FKS_createTimer: TKS_createTimer;
        
function KS_createTimer(phTimer: PKSHandle; delay: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_createTimer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createTimer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createTimer := PKS_createTimer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createTimer'));
    if @FKS_createTimer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createTimer not found in DLL');
      KS_createTimer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createTimer := FKS_createTimer(phTimer, delay, hSignal, flags);
end;

//------ KS_removeTimer ------
type
  TKS_removeTimer = function (hTimer: KSHandle): Error; stdcall;
  PKS_removeTimer = ^TKS_removeTimer;
var
  FKS_removeTimer: TKS_removeTimer;
        
function KS_removeTimer(hTimer: KSHandle): Error; stdcall;
begin
  if @FKS_removeTimer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeTimer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeTimer := PKS_removeTimer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeTimer'));
    if @FKS_removeTimer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeTimer not found in DLL');
      KS_removeTimer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeTimer := FKS_removeTimer(hTimer);
end;

//------ KS_cancelTimer ------
type
  TKS_cancelTimer = function (hTimer: KSHandle): Error; stdcall;
  PKS_cancelTimer = ^TKS_cancelTimer;
var
  FKS_cancelTimer: TKS_cancelTimer;
        
function KS_cancelTimer(hTimer: KSHandle): Error; stdcall;
begin
  if @FKS_cancelTimer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_cancelTimer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_cancelTimer := PKS_cancelTimer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_cancelTimer'));
    if @FKS_cancelTimer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_cancelTimer not found in DLL');
      KS_cancelTimer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_cancelTimer := FKS_cancelTimer(hTimer);
end;

//------ KS_startTimer ------
type
  TKS_startTimer = function (hTimer: KSHandle; flags: int; delay: int): Error; stdcall;
  PKS_startTimer = ^TKS_startTimer;
var
  FKS_startTimer: TKS_startTimer;
        
function KS_startTimer(hTimer: KSHandle; flags: int; delay: int): Error; stdcall;
begin
  if @FKS_startTimer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_startTimer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_startTimer := PKS_startTimer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_startTimer'));
    if @FKS_startTimer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_startTimer not found in DLL');
      KS_startTimer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_startTimer := FKS_startTimer(hTimer, flags, delay);
end;

//------ KS_stopTimer ------
type
  TKS_stopTimer = function (hTimer: KSHandle): Error; stdcall;
  PKS_stopTimer = ^TKS_stopTimer;
var
  FKS_stopTimer: TKS_stopTimer;
        
function KS_stopTimer(hTimer: KSHandle): Error; stdcall;
begin
  if @FKS_stopTimer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_stopTimer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_stopTimer := PKS_stopTimer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_stopTimer'));
    if @FKS_stopTimer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_stopTimer not found in DLL');
      KS_stopTimer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_stopTimer := FKS_stopTimer(hTimer);
end;

//------ KS_startTimerDelayed ------
type
  TKS_startTimerDelayed = function (hTimer: KSHandle; var start: Int64; period: int; flags: int): Error; stdcall;
  PKS_startTimerDelayed = ^TKS_startTimerDelayed;
var
  FKS_startTimerDelayed: TKS_startTimerDelayed;
        
function KS_startTimerDelayed(hTimer: KSHandle; var start: Int64; period: int; flags: int): Error; stdcall;
begin
  if @FKS_startTimerDelayed = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_startTimerDelayed := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_startTimerDelayed := PKS_startTimerDelayed(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_startTimerDelayed'));
    if @FKS_startTimerDelayed = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_startTimerDelayed not found in DLL');
      KS_startTimerDelayed := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_startTimerDelayed := FKS_startTimerDelayed(hTimer, start, period, flags);
end;

//------ KS_adjustTimer ------
type
  TKS_adjustTimer = function (hTimer: KSHandle; period: int; flags: int): Error; stdcall;
  PKS_adjustTimer = ^TKS_adjustTimer;
var
  FKS_adjustTimer: TKS_adjustTimer;
        
function KS_adjustTimer(hTimer: KSHandle; period: int; flags: int): Error; stdcall;
begin
  if @FKS_adjustTimer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_adjustTimer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_adjustTimer := PKS_adjustTimer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_adjustTimer'));
    if @FKS_adjustTimer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_adjustTimer not found in DLL');
      KS_adjustTimer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_adjustTimer := FKS_adjustTimer(hTimer, period, flags);
end;

//------ KS_disableTimer ------
type
  TKS_disableTimer = function (hTimer: KSHandle): Error; stdcall;
  PKS_disableTimer = ^TKS_disableTimer;
var
  FKS_disableTimer: TKS_disableTimer;
        
function KS_disableTimer(hTimer: KSHandle): Error; stdcall;
begin
  if @FKS_disableTimer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_disableTimer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_disableTimer := PKS_disableTimer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_disableTimer'));
    if @FKS_disableTimer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_disableTimer not found in DLL');
      KS_disableTimer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_disableTimer := FKS_disableTimer(hTimer);
end;

//------ KS_enableTimer ------
type
  TKS_enableTimer = function (hTimer: KSHandle): Error; stdcall;
  PKS_enableTimer = ^TKS_enableTimer;
var
  FKS_enableTimer: TKS_enableTimer;
        
function KS_enableTimer(hTimer: KSHandle): Error; stdcall;
begin
  if @FKS_enableTimer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enableTimer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enableTimer := PKS_enableTimer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enableTimer'));
    if @FKS_enableTimer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enableTimer not found in DLL');
      KS_enableTimer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enableTimer := FKS_enableTimer(hTimer);
end;

//------ KS_getTimerState ------
type
  TKS_getTimerState = function (hTimer: KSHandle; pState: PHandlerState): Error; stdcall;
  PKS_getTimerState = ^TKS_getTimerState;
var
  FKS_getTimerState: TKS_getTimerState;
        
function KS_getTimerState(hTimer: KSHandle; pState: PHandlerState): Error; stdcall;
begin
  if @FKS_getTimerState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getTimerState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getTimerState := PKS_getTimerState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getTimerState'));
    if @FKS_getTimerState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getTimerState not found in DLL');
      KS_getTimerState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getTimerState := FKS_getTimerState(hTimer, pState);
end;

//------ KS_setTimerResolution ------
type
  TKS_setTimerResolution = function (resolution: uint; flags: int): Error; stdcall;
  PKS_setTimerResolution = ^TKS_setTimerResolution;
var
  FKS_setTimerResolution: TKS_setTimerResolution;
        
function KS_setTimerResolution(resolution: uint; flags: int): Error; stdcall;
begin
  if @FKS_setTimerResolution = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setTimerResolution := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setTimerResolution := PKS_setTimerResolution(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setTimerResolution'));
    if @FKS_setTimerResolution = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setTimerResolution not found in DLL');
      KS_setTimerResolution := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setTimerResolution := FKS_setTimerResolution(resolution, flags);
end;

//------ KS_getTimerResolution ------
type
  TKS_getTimerResolution = function (pResolution: PUInt; flags: int): Error; stdcall;
  PKS_getTimerResolution = ^TKS_getTimerResolution;
var
  FKS_getTimerResolution: TKS_getTimerResolution;
        
function KS_getTimerResolution(pResolution: PUInt; flags: int): Error; stdcall;
begin
  if @FKS_getTimerResolution = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getTimerResolution := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getTimerResolution := PKS_getTimerResolution(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getTimerResolution'));
    if @FKS_getTimerResolution = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getTimerResolution not found in DLL');
      KS_getTimerResolution := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getTimerResolution := FKS_getTimerResolution(pResolution, flags);
end;

//------ _initRealTimeModule ------
procedure _initRealTimeModule();
begin
  _registerKernelAddress('KS_createTimer', @KS_createTimer);
  _registerKernelAddress('KS_removeTimer', @KS_removeTimer);
  _registerKernelAddress('KS_cancelTimer', @KS_cancelTimer);
  _registerKernelAddress('KS_startTimer', @KS_startTimer);
  _registerKernelAddress('KS_stopTimer', @KS_stopTimer);
  _registerKernelAddress('KS_startTimerDelayed', @KS_startTimerDelayed);
  _registerKernelAddress('KS_adjustTimer', @KS_adjustTimer);
  _registerKernelAddress('KS_disableTimer', @KS_disableTimer);
  _registerKernelAddress('KS_enableTimer', @KS_enableTimer);
  _registerKernelAddress('KS_getTimerState', @KS_getTimerState);
  _registerKernelAddress('KS_setTimerResolution', @KS_setTimerResolution);
  _registerKernelAddress('KS_getTimerResolution', @KS_getTimerResolution);
end;

{$DEFINE KS_REALTIME_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Task Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createTask ------
type
  TKS_createTask = function (phTask: PKSHandle; hCallBack: KSHandle; priority: int; flags: int): Error; stdcall;
  PKS_createTask = ^TKS_createTask;
var
  FKS_createTask: TKS_createTask;
        
function KS_createTask(phTask: PKSHandle; hCallBack: KSHandle; priority: int; flags: int): Error; stdcall;
begin
  if @FKS_createTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createTask := PKS_createTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createTask'));
    if @FKS_createTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createTask not found in DLL');
      KS_createTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createTask := FKS_createTask(phTask, hCallBack, priority, flags);
end;

//------ KS_removeTask ------
type
  TKS_removeTask = function (hTask: KSHandle): Error; stdcall;
  PKS_removeTask = ^TKS_removeTask;
var
  FKS_removeTask: TKS_removeTask;
        
function KS_removeTask(hTask: KSHandle): Error; stdcall;
begin
  if @FKS_removeTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeTask := PKS_removeTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeTask'));
    if @FKS_removeTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeTask not found in DLL');
      KS_removeTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeTask := FKS_removeTask(hTask);
end;

//------ KS_exitTask ------
type
  TKS_exitTask = function (hTask: KSHandle; exitCode: int): Error; stdcall;
  PKS_exitTask = ^TKS_exitTask;
var
  FKS_exitTask: TKS_exitTask;
        
function KS_exitTask(hTask: KSHandle; exitCode: int): Error; stdcall;
begin
  if @FKS_exitTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_exitTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_exitTask := PKS_exitTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_exitTask'));
    if @FKS_exitTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_exitTask not found in DLL');
      KS_exitTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_exitTask := FKS_exitTask(hTask, exitCode);
end;

//------ KS_killTask ------
type
  TKS_killTask = function (hTask: KSHandle; exitCode: int): Error; stdcall;
  PKS_killTask = ^TKS_killTask;
var
  FKS_killTask: TKS_killTask;
        
function KS_killTask(hTask: KSHandle; exitCode: int): Error; stdcall;
begin
  if @FKS_killTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_killTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_killTask := PKS_killTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_killTask'));
    if @FKS_killTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_killTask not found in DLL');
      KS_killTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_killTask := FKS_killTask(hTask, exitCode);
end;

//------ KS_terminateTask ------
type
  TKS_terminateTask = function (hTask: KSHandle; timeout: int; flags: int): Error; stdcall;
  PKS_terminateTask = ^TKS_terminateTask;
var
  FKS_terminateTask: TKS_terminateTask;
        
function KS_terminateTask(hTask: KSHandle; timeout: int; flags: int): Error; stdcall;
begin
  if @FKS_terminateTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_terminateTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_terminateTask := PKS_terminateTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_terminateTask'));
    if @FKS_terminateTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_terminateTask not found in DLL');
      KS_terminateTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_terminateTask := FKS_terminateTask(hTask, timeout, flags);
end;

//------ KS_suspendTask ------
type
  TKS_suspendTask = function (hTask: KSHandle): Error; stdcall;
  PKS_suspendTask = ^TKS_suspendTask;
var
  FKS_suspendTask: TKS_suspendTask;
        
function KS_suspendTask(hTask: KSHandle): Error; stdcall;
begin
  if @FKS_suspendTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_suspendTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_suspendTask := PKS_suspendTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_suspendTask'));
    if @FKS_suspendTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_suspendTask not found in DLL');
      KS_suspendTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_suspendTask := FKS_suspendTask(hTask);
end;

//------ KS_resumeTask ------
type
  TKS_resumeTask = function (hTask: KSHandle): Error; stdcall;
  PKS_resumeTask = ^TKS_resumeTask;
var
  FKS_resumeTask: TKS_resumeTask;
        
function KS_resumeTask(hTask: KSHandle): Error; stdcall;
begin
  if @FKS_resumeTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_resumeTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_resumeTask := PKS_resumeTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_resumeTask'));
    if @FKS_resumeTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_resumeTask not found in DLL');
      KS_resumeTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_resumeTask := FKS_resumeTask(hTask);
end;

//------ KS_triggerTask ------
type
  TKS_triggerTask = function (hTask: KSHandle): Error; stdcall;
  PKS_triggerTask = ^TKS_triggerTask;
var
  FKS_triggerTask: TKS_triggerTask;
        
function KS_triggerTask(hTask: KSHandle): Error; stdcall;
begin
  if @FKS_triggerTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_triggerTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_triggerTask := PKS_triggerTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_triggerTask'));
    if @FKS_triggerTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_triggerTask not found in DLL');
      KS_triggerTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_triggerTask := FKS_triggerTask(hTask);
end;

//------ KS_yieldTask ------
type
  TKS_yieldTask = function : Error; stdcall;
  PKS_yieldTask = ^TKS_yieldTask;
var
  FKS_yieldTask: TKS_yieldTask;
        
function KS_yieldTask: Error; stdcall;
begin
  if @FKS_yieldTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_yieldTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_yieldTask := PKS_yieldTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_yieldTask'));
    if @FKS_yieldTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_yieldTask not found in DLL');
      KS_yieldTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_yieldTask := FKS_yieldTask;
end;

//------ KS_sleepTask ------
type
  TKS_sleepTask = function (delay: int): Error; stdcall;
  PKS_sleepTask = ^TKS_sleepTask;
var
  FKS_sleepTask: TKS_sleepTask;
        
function KS_sleepTask(delay: int): Error; stdcall;
begin
  if @FKS_sleepTask = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_sleepTask := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_sleepTask := PKS_sleepTask(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_sleepTask'));
    if @FKS_sleepTask = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_sleepTask not found in DLL');
      KS_sleepTask := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_sleepTask := FKS_sleepTask(delay);
end;

//------ KS_setTaskPrio ------
type
  TKS_setTaskPrio = function (hTask: KSHandle; newPrio: int; pOldPrio: PInt): Error; stdcall;
  PKS_setTaskPrio = ^TKS_setTaskPrio;
var
  FKS_setTaskPrio: TKS_setTaskPrio;
        
function KS_setTaskPrio(hTask: KSHandle; newPrio: int; pOldPrio: PInt): Error; stdcall;
begin
  if @FKS_setTaskPrio = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setTaskPrio := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setTaskPrio := PKS_setTaskPrio(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setTaskPrio'));
    if @FKS_setTaskPrio = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setTaskPrio not found in DLL');
      KS_setTaskPrio := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setTaskPrio := FKS_setTaskPrio(hTask, newPrio, pOldPrio);
end;

//------ KS_setTaskPriority ------
type
  TKS_setTaskPriority = function (hObject: KSHandle; priority: int; flags: int): Error; stdcall;
  PKS_setTaskPriority = ^TKS_setTaskPriority;
var
  FKS_setTaskPriority: TKS_setTaskPriority;
        
function KS_setTaskPriority(hObject: KSHandle; priority: int; flags: int): Error; stdcall;
begin
  if @FKS_setTaskPriority = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setTaskPriority := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setTaskPriority := PKS_setTaskPriority(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setTaskPriority'));
    if @FKS_setTaskPriority = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setTaskPriority not found in DLL');
      KS_setTaskPriority := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setTaskPriority := FKS_setTaskPriority(hObject, priority, flags);
end;

//------ KS_setTaskPriorityRelative ------
type
  TKS_setTaskPriorityRelative = function (hObject: KSHandle; hRelative: KSHandle; distance: int; flags: int): Error; stdcall;
  PKS_setTaskPriorityRelative = ^TKS_setTaskPriorityRelative;
var
  FKS_setTaskPriorityRelative: TKS_setTaskPriorityRelative;
        
function KS_setTaskPriorityRelative(hObject: KSHandle; hRelative: KSHandle; distance: int; flags: int): Error; stdcall;
begin
  if @FKS_setTaskPriorityRelative = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setTaskPriorityRelative := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setTaskPriorityRelative := PKS_setTaskPriorityRelative(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setTaskPriorityRelative'));
    if @FKS_setTaskPriorityRelative = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setTaskPriorityRelative not found in DLL');
      KS_setTaskPriorityRelative := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setTaskPriorityRelative := FKS_setTaskPriorityRelative(hObject, hRelative, distance, flags);
end;

//------ KS_getTaskPriority ------
type
  TKS_getTaskPriority = function (hObject: KSHandle; pPriority: PInt; flags: int): Error; stdcall;
  PKS_getTaskPriority = ^TKS_getTaskPriority;
var
  FKS_getTaskPriority: TKS_getTaskPriority;
        
function KS_getTaskPriority(hObject: KSHandle; pPriority: PInt; flags: int): Error; stdcall;
begin
  if @FKS_getTaskPriority = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getTaskPriority := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getTaskPriority := PKS_getTaskPriority(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getTaskPriority'));
    if @FKS_getTaskPriority = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getTaskPriority not found in DLL');
      KS_getTaskPriority := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getTaskPriority := FKS_getTaskPriority(hObject, pPriority, flags);
end;

//------ KS_getTaskState ------
type
  TKS_getTaskState = function (hTask: KSHandle; pTaskState: PUInt; pExitCode: PInt): Error; stdcall;
  PKS_getTaskState = ^TKS_getTaskState;
var
  FKS_getTaskState: TKS_getTaskState;
        
function KS_getTaskState(hTask: KSHandle; pTaskState: PUInt; pExitCode: PInt): Error; stdcall;
begin
  if @FKS_getTaskState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getTaskState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getTaskState := PKS_getTaskState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getTaskState'));
    if @FKS_getTaskState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getTaskState not found in DLL');
      KS_getTaskState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getTaskState := FKS_getTaskState(hTask, pTaskState, pExitCode);
end;

//------ KS_setTaskStackSize ------
type
  TKS_setTaskStackSize = function (size: int; flags: int): Error; stdcall;
  PKS_setTaskStackSize = ^TKS_setTaskStackSize;
var
  FKS_setTaskStackSize: TKS_setTaskStackSize;
        
function KS_setTaskStackSize(size: int; flags: int): Error; stdcall;
begin
  if @FKS_setTaskStackSize = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setTaskStackSize := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setTaskStackSize := PKS_setTaskStackSize(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setTaskStackSize'));
    if @FKS_setTaskStackSize = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setTaskStackSize not found in DLL');
      KS_setTaskStackSize := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setTaskStackSize := FKS_setTaskStackSize(size, flags);
end;

//------ KS_setTargetProcessor ------
type
  TKS_setTargetProcessor = function (processor: int; flags: int): Error; stdcall;
  PKS_setTargetProcessor = ^TKS_setTargetProcessor;
var
  FKS_setTargetProcessor: TKS_setTargetProcessor;
        
function KS_setTargetProcessor(processor: int; flags: int): Error; stdcall;
begin
  if @FKS_setTargetProcessor = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setTargetProcessor := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setTargetProcessor := PKS_setTargetProcessor(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setTargetProcessor'));
    if @FKS_setTargetProcessor = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setTargetProcessor not found in DLL');
      KS_setTargetProcessor := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setTargetProcessor := FKS_setTargetProcessor(processor, flags);
end;

//------ KS_getCurrentProcessor ------
type
  TKS_getCurrentProcessor = function (pProcessor: PInt; flags: int): Error; stdcall;
  PKS_getCurrentProcessor = ^TKS_getCurrentProcessor;
var
  FKS_getCurrentProcessor: TKS_getCurrentProcessor;
        
function KS_getCurrentProcessor(pProcessor: PInt; flags: int): Error; stdcall;
begin
  if @FKS_getCurrentProcessor = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getCurrentProcessor := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getCurrentProcessor := PKS_getCurrentProcessor(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getCurrentProcessor'));
    if @FKS_getCurrentProcessor = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getCurrentProcessor not found in DLL');
      KS_getCurrentProcessor := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getCurrentProcessor := FKS_getCurrentProcessor(pProcessor, flags);
end;

//------ KS_getCurrentContext ------
type
  TKS_getCurrentContext = function (pContextInfo: PKSContextInformation; flags: int): Error; stdcall;
  PKS_getCurrentContext = ^TKS_getCurrentContext;
var
  FKS_getCurrentContext: TKS_getCurrentContext;
        
function KS_getCurrentContext(pContextInfo: PKSContextInformation; flags: int): Error; stdcall;
begin
  if @FKS_getCurrentContext = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getCurrentContext := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getCurrentContext := PKS_getCurrentContext(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getCurrentContext'));
    if @FKS_getCurrentContext = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getCurrentContext not found in DLL');
      KS_getCurrentContext := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getCurrentContext := FKS_getCurrentContext(pContextInfo, flags);
end;

//------ KS_createSemaphore ------
type
  TKS_createSemaphore = function (phSemaphore: PKSHandle; maxCount: int; initCount: int; flags: int): Error; stdcall;
  PKS_createSemaphore = ^TKS_createSemaphore;
var
  FKS_createSemaphore: TKS_createSemaphore;
        
function KS_createSemaphore(phSemaphore: PKSHandle; maxCount: int; initCount: int; flags: int): Error; stdcall;
begin
  if @FKS_createSemaphore = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createSemaphore := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createSemaphore := PKS_createSemaphore(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createSemaphore'));
    if @FKS_createSemaphore = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createSemaphore not found in DLL');
      KS_createSemaphore := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createSemaphore := FKS_createSemaphore(phSemaphore, maxCount, initCount, flags);
end;

//------ KS_removeSemaphore ------
type
  TKS_removeSemaphore = function (hSemaphore: KSHandle): Error; stdcall;
  PKS_removeSemaphore = ^TKS_removeSemaphore;
var
  FKS_removeSemaphore: TKS_removeSemaphore;
        
function KS_removeSemaphore(hSemaphore: KSHandle): Error; stdcall;
begin
  if @FKS_removeSemaphore = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeSemaphore := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeSemaphore := PKS_removeSemaphore(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeSemaphore'));
    if @FKS_removeSemaphore = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeSemaphore not found in DLL');
      KS_removeSemaphore := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeSemaphore := FKS_removeSemaphore(hSemaphore);
end;

//------ KS_requestSemaphore ------
type
  TKS_requestSemaphore = function (hSemaphore: KSHandle; timeout: int): Error; stdcall;
  PKS_requestSemaphore = ^TKS_requestSemaphore;
var
  FKS_requestSemaphore: TKS_requestSemaphore;
        
function KS_requestSemaphore(hSemaphore: KSHandle; timeout: int): Error; stdcall;
begin
  if @FKS_requestSemaphore = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_requestSemaphore := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_requestSemaphore := PKS_requestSemaphore(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_requestSemaphore'));
    if @FKS_requestSemaphore = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_requestSemaphore not found in DLL');
      KS_requestSemaphore := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_requestSemaphore := FKS_requestSemaphore(hSemaphore, timeout);
end;

//------ KS_releaseSemaphore ------
type
  TKS_releaseSemaphore = function (hSemaphore: KSHandle): Error; stdcall;
  PKS_releaseSemaphore = ^TKS_releaseSemaphore;
var
  FKS_releaseSemaphore: TKS_releaseSemaphore;
        
function KS_releaseSemaphore(hSemaphore: KSHandle): Error; stdcall;
begin
  if @FKS_releaseSemaphore = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_releaseSemaphore := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_releaseSemaphore := PKS_releaseSemaphore(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_releaseSemaphore'));
    if @FKS_releaseSemaphore = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_releaseSemaphore not found in DLL');
      KS_releaseSemaphore := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_releaseSemaphore := FKS_releaseSemaphore(hSemaphore);
end;

//------ _initTaskModule ------
procedure _initTaskModule();
begin
  _registerKernelAddress('KS_createTask', @KS_createTask);
  _registerKernelAddress('KS_removeTask', @KS_removeTask);
  _registerKernelAddress('KS_exitTask', @KS_exitTask);
  _registerKernelAddress('KS_killTask', @KS_killTask);
  _registerKernelAddress('KS_terminateTask', @KS_terminateTask);
  _registerKernelAddress('KS_suspendTask', @KS_suspendTask);
  _registerKernelAddress('KS_resumeTask', @KS_resumeTask);
  _registerKernelAddress('KS_triggerTask', @KS_triggerTask);
  _registerKernelAddress('KS_yieldTask', @KS_yieldTask);
  _registerKernelAddress('KS_sleepTask', @KS_sleepTask);
  _registerKernelAddress('KS_setTaskPrio', @KS_setTaskPrio);
  _registerKernelAddress('KS_setTaskPriority', @KS_setTaskPriority);
  _registerKernelAddress('KS_setTaskPriorityRelative', @KS_setTaskPriorityRelative);
  _registerKernelAddress('KS_getTaskPriority', @KS_getTaskPriority);
  _registerKernelAddress('KS_getTaskState', @KS_getTaskState);
  _registerKernelAddress('KS_setTaskStackSize', @KS_setTaskStackSize);
  _registerKernelAddress('KS_setTargetProcessor', @KS_setTargetProcessor);
  _registerKernelAddress('KS_getCurrentProcessor', @KS_getCurrentProcessor);
  _registerKernelAddress('KS_getCurrentContext', @KS_getCurrentContext);
  _registerKernelAddress('KS_createSemaphore', @KS_createSemaphore);
  _registerKernelAddress('KS_removeSemaphore', @KS_removeSemaphore);
  _registerKernelAddress('KS_requestSemaphore', @KS_requestSemaphore);
  _registerKernelAddress('KS_releaseSemaphore', @KS_releaseSemaphore);
end;

{$DEFINE KS_TASK_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Dedicated Module
//--------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------
// Packet Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openAdapter ------
type
  TKS_openAdapter = function (phAdapter: PKSHandle; name: PAnsiChar; recvPoolLength: int; sendPoolLength: int; flags: int): Error; stdcall;
  PKS_openAdapter = ^TKS_openAdapter;
var
  FKS_openAdapter: TKS_openAdapter;
        
function KS_openAdapter(phAdapter: PKSHandle; name: PAnsiChar; recvPoolLength: int; sendPoolLength: int; flags: int): Error; stdcall;
begin
  if @FKS_openAdapter = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openAdapter := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openAdapter := PKS_openAdapter(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openAdapter'));
    if @FKS_openAdapter = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openAdapter not found in DLL');
      KS_openAdapter := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openAdapter := FKS_openAdapter(phAdapter, name, recvPoolLength, sendPoolLength, flags);
end;

//------ KS_openAdapterEx ------
type
  TKS_openAdapterEx = function (phAdapter: PKSHandle; hConnection: KSHandle; recvPoolLength: int; sendPoolLength: int; flags: int): Error; stdcall;
  PKS_openAdapterEx = ^TKS_openAdapterEx;
var
  FKS_openAdapterEx: TKS_openAdapterEx;
        
function KS_openAdapterEx(phAdapter: PKSHandle; hConnection: KSHandle; recvPoolLength: int; sendPoolLength: int; flags: int): Error; stdcall;
begin
  if @FKS_openAdapterEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openAdapterEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openAdapterEx := PKS_openAdapterEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openAdapterEx'));
    if @FKS_openAdapterEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openAdapterEx not found in DLL');
      KS_openAdapterEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openAdapterEx := FKS_openAdapterEx(phAdapter, hConnection, recvPoolLength, sendPoolLength, flags);
end;

//------ KS_closeAdapter ------
type
  TKS_closeAdapter = function (hAdapter: KSHandle): Error; stdcall;
  PKS_closeAdapter = ^TKS_closeAdapter;
var
  FKS_closeAdapter: TKS_closeAdapter;
        
function KS_closeAdapter(hAdapter: KSHandle): Error; stdcall;
begin
  if @FKS_closeAdapter = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeAdapter := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeAdapter := PKS_closeAdapter(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeAdapter'));
    if @FKS_closeAdapter = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeAdapter not found in DLL');
      KS_closeAdapter := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeAdapter := FKS_closeAdapter(hAdapter);
end;

//------ KS_getAdapterState ------
type
  TKS_getAdapterState = function (hAdapter: KSHandle; pAdapterState: PKSAdapterState; flags: int): Error; stdcall;
  PKS_getAdapterState = ^TKS_getAdapterState;
var
  FKS_getAdapterState: TKS_getAdapterState;
        
function KS_getAdapterState(hAdapter: KSHandle; pAdapterState: PKSAdapterState; flags: int): Error; stdcall;
begin
  if @FKS_getAdapterState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getAdapterState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getAdapterState := PKS_getAdapterState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getAdapterState'));
    if @FKS_getAdapterState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getAdapterState not found in DLL');
      KS_getAdapterState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getAdapterState := FKS_getAdapterState(hAdapter, pAdapterState, flags);
end;

//------ KS_execAdapterCommand ------
type
  TKS_execAdapterCommand = function (hAdapter: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execAdapterCommand = ^TKS_execAdapterCommand;
var
  FKS_execAdapterCommand: TKS_execAdapterCommand;
        
function KS_execAdapterCommand(hAdapter: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execAdapterCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execAdapterCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execAdapterCommand := PKS_execAdapterCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execAdapterCommand'));
    if @FKS_execAdapterCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execAdapterCommand not found in DLL');
      KS_execAdapterCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execAdapterCommand := FKS_execAdapterCommand(hAdapter, command, pParam, flags);
end;

//------ KS_aggregateAdapter ------
type
  TKS_aggregateAdapter = function (hAdapter: KSHandle; hLink: KSHandle; flags: int): Error; stdcall;
  PKS_aggregateAdapter = ^TKS_aggregateAdapter;
var
  FKS_aggregateAdapter: TKS_aggregateAdapter;
        
function KS_aggregateAdapter(hAdapter: KSHandle; hLink: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_aggregateAdapter = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_aggregateAdapter := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_aggregateAdapter := PKS_aggregateAdapter(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_aggregateAdapter'));
    if @FKS_aggregateAdapter = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_aggregateAdapter not found in DLL');
      KS_aggregateAdapter := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_aggregateAdapter := FKS_aggregateAdapter(hAdapter, hLink, flags);
end;

//------ KS_requestPacket ------
type
  TKS_requestPacket = function (hAdapter: KSHandle; ppPacket: PPointer; flags: int): Error; stdcall;
  PKS_requestPacket = ^TKS_requestPacket;
var
  FKS_requestPacket: TKS_requestPacket;
        
function KS_requestPacket(hAdapter: KSHandle; ppPacket: PPointer; flags: int): Error; stdcall;
begin
  if @FKS_requestPacket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_requestPacket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_requestPacket := PKS_requestPacket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_requestPacket'));
    if @FKS_requestPacket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_requestPacket not found in DLL');
      KS_requestPacket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_requestPacket := FKS_requestPacket(hAdapter, ppPacket, flags);
end;

//------ KS_releasePacket ------
type
  TKS_releasePacket = function (hAdapter: KSHandle; pPacket: Pointer; flags: int): Error; stdcall;
  PKS_releasePacket = ^TKS_releasePacket;
var
  FKS_releasePacket: TKS_releasePacket;
        
function KS_releasePacket(hAdapter: KSHandle; pPacket: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_releasePacket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_releasePacket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_releasePacket := PKS_releasePacket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_releasePacket'));
    if @FKS_releasePacket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_releasePacket not found in DLL');
      KS_releasePacket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_releasePacket := FKS_releasePacket(hAdapter, pPacket, flags);
end;

//------ KS_sendPacket ------
type
  TKS_sendPacket = function (hAdapter: KSHandle; pPacket: Pointer; size: uint; flags: int): Error; stdcall;
  PKS_sendPacket = ^TKS_sendPacket;
var
  FKS_sendPacket: TKS_sendPacket;
        
function KS_sendPacket(hAdapter: KSHandle; pPacket: Pointer; size: uint; flags: int): Error; stdcall;
begin
  if @FKS_sendPacket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_sendPacket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_sendPacket := PKS_sendPacket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_sendPacket'));
    if @FKS_sendPacket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_sendPacket not found in DLL');
      KS_sendPacket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_sendPacket := FKS_sendPacket(hAdapter, pPacket, size, flags);
end;

//------ KS_recvPacket ------
type
  TKS_recvPacket = function (hAdapter: KSHandle; ppPacket: PPointer; flags: int): Error; stdcall;
  PKS_recvPacket = ^TKS_recvPacket;
var
  FKS_recvPacket: TKS_recvPacket;
        
function KS_recvPacket(hAdapter: KSHandle; ppPacket: PPointer; flags: int): Error; stdcall;
begin
  if @FKS_recvPacket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvPacket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvPacket := PKS_recvPacket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvPacket'));
    if @FKS_recvPacket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvPacket not found in DLL');
      KS_recvPacket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvPacket := FKS_recvPacket(hAdapter, ppPacket, flags);
end;

//------ KS_recvPacketEx ------
type
  TKS_recvPacketEx = function (hAdapter: KSHandle; ppPacket: PPointer; pLength: PInt; flags: int): Error; stdcall;
  PKS_recvPacketEx = ^TKS_recvPacketEx;
var
  FKS_recvPacketEx: TKS_recvPacketEx;
        
function KS_recvPacketEx(hAdapter: KSHandle; ppPacket: PPointer; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_recvPacketEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvPacketEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvPacketEx := PKS_recvPacketEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvPacketEx'));
    if @FKS_recvPacketEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvPacketEx not found in DLL');
      KS_recvPacketEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvPacketEx := FKS_recvPacketEx(hAdapter, ppPacket, pLength, flags);
end;

//------ KS_installPacketHandler ------
type
  TKS_installPacketHandler = function (hAdapter: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installPacketHandler = ^TKS_installPacketHandler;
var
  FKS_installPacketHandler: TKS_installPacketHandler;
        
function KS_installPacketHandler(hAdapter: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installPacketHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installPacketHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installPacketHandler := PKS_installPacketHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installPacketHandler'));
    if @FKS_installPacketHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installPacketHandler not found in DLL');
      KS_installPacketHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installPacketHandler := FKS_installPacketHandler(hAdapter, eventCode, hSignal, flags);
end;

//------ _initPacketModule ------
procedure _initPacketModule();
begin
  _registerKernelAddress('KS_openAdapter', @KS_openAdapter);
  _registerKernelAddress('KS_closeAdapter', @KS_closeAdapter);
  _registerKernelAddress('KS_getAdapterState', @KS_getAdapterState);
  _registerKernelAddress('KS_execAdapterCommand', @KS_execAdapterCommand);
  _registerKernelAddress('KS_aggregateAdapter', @KS_aggregateAdapter);
  _registerKernelAddress('KS_requestPacket', @KS_requestPacket);
  _registerKernelAddress('KS_releasePacket', @KS_releasePacket);
  _registerKernelAddress('KS_sendPacket', @KS_sendPacket);
  _registerKernelAddress('KS_recvPacket', @KS_recvPacket);
  _registerKernelAddress('KS_recvPacketEx', @KS_recvPacketEx);
  _registerKernelAddress('KS_installPacketHandler', @KS_installPacketHandler);
end;

{$DEFINE KS_PACKET_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Socket Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openSocket ------
type
  TKS_openSocket = function (phSocket: PKSHandle; hAdapter: KSHandle; pSocketAddr: PKSSocketAddr; protocol: int; flags: int): Error; stdcall;
  PKS_openSocket = ^TKS_openSocket;
var
  FKS_openSocket: TKS_openSocket;
        
function KS_openSocket(phSocket: PKSHandle; hAdapter: KSHandle; pSocketAddr: PKSSocketAddr; protocol: int; flags: int): Error; stdcall;
begin
  if @FKS_openSocket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openSocket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openSocket := PKS_openSocket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openSocket'));
    if @FKS_openSocket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openSocket not found in DLL');
      KS_openSocket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openSocket := FKS_openSocket(phSocket, hAdapter, pSocketAddr, protocol, flags);
end;

//------ KS_closeSocket ------
type
  TKS_closeSocket = function (hSocket: KSHandle): Error; stdcall;
  PKS_closeSocket = ^TKS_closeSocket;
var
  FKS_closeSocket: TKS_closeSocket;
        
function KS_closeSocket(hSocket: KSHandle): Error; stdcall;
begin
  if @FKS_closeSocket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeSocket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeSocket := PKS_closeSocket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeSocket'));
    if @FKS_closeSocket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeSocket not found in DLL');
      KS_closeSocket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeSocket := FKS_closeSocket(hSocket);
end;

//------ KS_recvFromSocket ------
type
  TKS_recvFromSocket = function (hSocket: KSHandle; pSourceAddr: PKSSocketAddr; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_recvFromSocket = ^TKS_recvFromSocket;
var
  FKS_recvFromSocket: TKS_recvFromSocket;
        
function KS_recvFromSocket(hSocket: KSHandle; pSourceAddr: PKSSocketAddr; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_recvFromSocket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvFromSocket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvFromSocket := PKS_recvFromSocket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvFromSocket'));
    if @FKS_recvFromSocket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvFromSocket not found in DLL');
      KS_recvFromSocket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvFromSocket := FKS_recvFromSocket(hSocket, pSourceAddr, pBuffer, length, pLength, flags);
end;

//------ KS_sendToSocket ------
type
  TKS_sendToSocket = function (hSocket: KSHandle; pTargetAddr: PKSSocketAddr; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_sendToSocket = ^TKS_sendToSocket;
var
  FKS_sendToSocket: TKS_sendToSocket;
        
function KS_sendToSocket(hSocket: KSHandle; pTargetAddr: PKSSocketAddr; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_sendToSocket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_sendToSocket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_sendToSocket := PKS_sendToSocket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_sendToSocket'));
    if @FKS_sendToSocket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_sendToSocket not found in DLL');
      KS_sendToSocket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_sendToSocket := FKS_sendToSocket(hSocket, pTargetAddr, pBuffer, length, pLength, flags);
end;

//------ KS_recvSocket ------
type
  TKS_recvSocket = function (hSocket: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_recvSocket = ^TKS_recvSocket;
var
  FKS_recvSocket: TKS_recvSocket;
        
function KS_recvSocket(hSocket: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_recvSocket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvSocket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvSocket := PKS_recvSocket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvSocket'));
    if @FKS_recvSocket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvSocket not found in DLL');
      KS_recvSocket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvSocket := FKS_recvSocket(hSocket, pBuffer, length, pLength, flags);
end;

//------ KS_sendSocket ------
type
  TKS_sendSocket = function (hSocket: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_sendSocket = ^TKS_sendSocket;
var
  FKS_sendSocket: TKS_sendSocket;
        
function KS_sendSocket(hSocket: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_sendSocket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_sendSocket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_sendSocket := PKS_sendSocket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_sendSocket'));
    if @FKS_sendSocket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_sendSocket not found in DLL');
      KS_sendSocket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_sendSocket := FKS_sendSocket(hSocket, pBuffer, length, pLength, flags);
end;

//------ KS_installSocketHandler ------
type
  TKS_installSocketHandler = function (hSocket: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installSocketHandler = ^TKS_installSocketHandler;
var
  FKS_installSocketHandler: TKS_installSocketHandler;
        
function KS_installSocketHandler(hSocket: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installSocketHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installSocketHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installSocketHandler := PKS_installSocketHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installSocketHandler'));
    if @FKS_installSocketHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installSocketHandler not found in DLL');
      KS_installSocketHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installSocketHandler := FKS_installSocketHandler(hSocket, eventCode, hSignal, flags);
end;

//------ KS_connectSocket ------
type
  TKS_connectSocket = function (hClient: KSHandle; pServerAddr: PKSSocketAddr; flags: int): Error; stdcall;
  PKS_connectSocket = ^TKS_connectSocket;
var
  FKS_connectSocket: TKS_connectSocket;
        
function KS_connectSocket(hClient: KSHandle; pServerAddr: PKSSocketAddr; flags: int): Error; stdcall;
begin
  if @FKS_connectSocket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_connectSocket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_connectSocket := PKS_connectSocket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_connectSocket'));
    if @FKS_connectSocket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_connectSocket not found in DLL');
      KS_connectSocket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_connectSocket := FKS_connectSocket(hClient, pServerAddr, flags);
end;

//------ KS_acceptSocket ------
type
  TKS_acceptSocket = function (hServer: KSHandle; pClientAddr: PKSSocketAddr; flags: int): Error; stdcall;
  PKS_acceptSocket = ^TKS_acceptSocket;
var
  FKS_acceptSocket: TKS_acceptSocket;
        
function KS_acceptSocket(hServer: KSHandle; pClientAddr: PKSSocketAddr; flags: int): Error; stdcall;
begin
  if @FKS_acceptSocket = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_acceptSocket := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_acceptSocket := PKS_acceptSocket(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_acceptSocket'));
    if @FKS_acceptSocket = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_acceptSocket not found in DLL');
      KS_acceptSocket := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_acceptSocket := FKS_acceptSocket(hServer, pClientAddr, flags);
end;

//------ KS_execSocketCommand ------
type
  TKS_execSocketCommand = function (hSocket: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execSocketCommand = ^TKS_execSocketCommand;
var
  FKS_execSocketCommand: TKS_execSocketCommand;
        
function KS_execSocketCommand(hSocket: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execSocketCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execSocketCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execSocketCommand := PKS_execSocketCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execSocketCommand'));
    if @FKS_execSocketCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execSocketCommand not found in DLL');
      KS_execSocketCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execSocketCommand := FKS_execSocketCommand(hSocket, command, pParam, flags);
end;

//------ _initSocketModule ------
procedure _initSocketModule();
begin
  _registerKernelAddress('KS_recvFromSocket', @KS_recvFromSocket);
  _registerKernelAddress('KS_sendToSocket', @KS_sendToSocket);
  _registerKernelAddress('KS_recvSocket', @KS_recvSocket);
  _registerKernelAddress('KS_sendSocket', @KS_sendSocket);
  _registerKernelAddress('KS_connectSocket', @KS_connectSocket);
  _registerKernelAddress('KS_acceptSocket', @KS_acceptSocket);
  _registerKernelAddress('KS_execSocketCommand', @KS_execSocketCommand);
end;

{$DEFINE KS_SOCKET_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// EtherCAT Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createEcatMaster ------
type
  TKS_createEcatMaster = function (phMaster: PKSHandle; hConnection: KSHandle; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int): Error; stdcall;
  PKS_createEcatMaster = ^TKS_createEcatMaster;
var
  FKS_createEcatMaster: TKS_createEcatMaster;
        
function KS_createEcatMaster(phMaster: PKSHandle; hConnection: KSHandle; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_createEcatMaster = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createEcatMaster := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createEcatMaster := PKS_createEcatMaster(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createEcatMaster'));
    if @FKS_createEcatMaster = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createEcatMaster not found in DLL');
      KS_createEcatMaster := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createEcatMaster := FKS_createEcatMaster(phMaster, hConnection, libraryPath, topologyFile, flags);
end;

//------ KS_closeEcatMaster ------
type
  TKS_closeEcatMaster = function (hMaster: KSHandle): Error; stdcall;
  PKS_closeEcatMaster = ^TKS_closeEcatMaster;
var
  FKS_closeEcatMaster: TKS_closeEcatMaster;
        
function KS_closeEcatMaster(hMaster: KSHandle): Error; stdcall;
begin
  if @FKS_closeEcatMaster = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeEcatMaster := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeEcatMaster := PKS_closeEcatMaster(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeEcatMaster'));
    if @FKS_closeEcatMaster = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeEcatMaster not found in DLL');
      KS_closeEcatMaster := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeEcatMaster := FKS_closeEcatMaster(hMaster);
end;

//------ KS_installEcatMasterHandler ------
type
  TKS_installEcatMasterHandler = function (hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installEcatMasterHandler = ^TKS_installEcatMasterHandler;
var
  FKS_installEcatMasterHandler: TKS_installEcatMasterHandler;
        
function KS_installEcatMasterHandler(hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installEcatMasterHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installEcatMasterHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installEcatMasterHandler := PKS_installEcatMasterHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installEcatMasterHandler'));
    if @FKS_installEcatMasterHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installEcatMasterHandler not found in DLL');
      KS_installEcatMasterHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installEcatMasterHandler := FKS_installEcatMasterHandler(hMaster, eventCode, hSignal, flags);
end;

//------ KS_queryEcatMasterState ------
type
  TKS_queryEcatMasterState = function (hMaster: KSHandle; pMasterState: PKSEcatMasterState; flags: int): Error; stdcall;
  PKS_queryEcatMasterState = ^TKS_queryEcatMasterState;
var
  FKS_queryEcatMasterState: TKS_queryEcatMasterState;
        
function KS_queryEcatMasterState(hMaster: KSHandle; pMasterState: PKSEcatMasterState; flags: int): Error; stdcall;
begin
  if @FKS_queryEcatMasterState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryEcatMasterState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryEcatMasterState := PKS_queryEcatMasterState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryEcatMasterState'));
    if @FKS_queryEcatMasterState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryEcatMasterState not found in DLL');
      KS_queryEcatMasterState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryEcatMasterState := FKS_queryEcatMasterState(hMaster, pMasterState, flags);
end;

//------ KS_changeEcatMasterState ------
type
  TKS_changeEcatMasterState = function (hMaster: KSHandle; state: int; flags: int): Error; stdcall;
  PKS_changeEcatMasterState = ^TKS_changeEcatMasterState;
var
  FKS_changeEcatMasterState: TKS_changeEcatMasterState;
        
function KS_changeEcatMasterState(hMaster: KSHandle; state: int; flags: int): Error; stdcall;
begin
  if @FKS_changeEcatMasterState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_changeEcatMasterState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_changeEcatMasterState := PKS_changeEcatMasterState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_changeEcatMasterState'));
    if @FKS_changeEcatMasterState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_changeEcatMasterState not found in DLL');
      KS_changeEcatMasterState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_changeEcatMasterState := FKS_changeEcatMasterState(hMaster, state, flags);
end;

//------ KS_addEcatRedundancy ------
type
  TKS_addEcatRedundancy = function (hMaster: KSHandle; hConnection: KSHandle; flags: int): Error; stdcall;
  PKS_addEcatRedundancy = ^TKS_addEcatRedundancy;
var
  FKS_addEcatRedundancy: TKS_addEcatRedundancy;
        
function KS_addEcatRedundancy(hMaster: KSHandle; hConnection: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_addEcatRedundancy = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_addEcatRedundancy := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_addEcatRedundancy := PKS_addEcatRedundancy(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_addEcatRedundancy'));
    if @FKS_addEcatRedundancy = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_addEcatRedundancy not found in DLL');
      KS_addEcatRedundancy := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_addEcatRedundancy := FKS_addEcatRedundancy(hMaster, hConnection, flags);
end;

//------ KS_createEcatSlave ------
type
  TKS_createEcatSlave = function (hMaster: KSHandle; phSlave: PKSHandle; id: int; position: int; vendor: int; product: int; revision: int; flags: int): Error; stdcall;
  PKS_createEcatSlave = ^TKS_createEcatSlave;
var
  FKS_createEcatSlave: TKS_createEcatSlave;
        
function KS_createEcatSlave(hMaster: KSHandle; phSlave: PKSHandle; id: int; position: int; vendor: int; product: int; revision: int; flags: int): Error; stdcall;
begin
  if @FKS_createEcatSlave = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createEcatSlave := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createEcatSlave := PKS_createEcatSlave(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createEcatSlave'));
    if @FKS_createEcatSlave = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createEcatSlave not found in DLL');
      KS_createEcatSlave := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createEcatSlave := FKS_createEcatSlave(hMaster, phSlave, id, position, vendor, product, revision, flags);
end;

//------ KS_createEcatSlaveIndirect ------
type
  TKS_createEcatSlaveIndirect = function (hMaster: KSHandle; phSlave: PKSHandle; pSlaveState: PKSEcatSlaveState; flags: int): Error; stdcall;
  PKS_createEcatSlaveIndirect = ^TKS_createEcatSlaveIndirect;
var
  FKS_createEcatSlaveIndirect: TKS_createEcatSlaveIndirect;
        
function KS_createEcatSlaveIndirect(hMaster: KSHandle; phSlave: PKSHandle; pSlaveState: PKSEcatSlaveState; flags: int): Error; stdcall;
begin
  if @FKS_createEcatSlaveIndirect = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createEcatSlaveIndirect := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createEcatSlaveIndirect := PKS_createEcatSlaveIndirect(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createEcatSlaveIndirect'));
    if @FKS_createEcatSlaveIndirect = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createEcatSlaveIndirect not found in DLL');
      KS_createEcatSlaveIndirect := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createEcatSlaveIndirect := FKS_createEcatSlaveIndirect(hMaster, phSlave, pSlaveState, flags);
end;

//------ KS_enumEcatSlaves ------
type
  TKS_enumEcatSlaves = function (hMaster: KSHandle; index: int; pSlaveState: PKSEcatSlaveState; flags: int): Error; stdcall;
  PKS_enumEcatSlaves = ^TKS_enumEcatSlaves;
var
  FKS_enumEcatSlaves: TKS_enumEcatSlaves;
        
function KS_enumEcatSlaves(hMaster: KSHandle; index: int; pSlaveState: PKSEcatSlaveState; flags: int): Error; stdcall;
begin
  if @FKS_enumEcatSlaves = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumEcatSlaves := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumEcatSlaves := PKS_enumEcatSlaves(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumEcatSlaves'));
    if @FKS_enumEcatSlaves = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumEcatSlaves not found in DLL');
      KS_enumEcatSlaves := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumEcatSlaves := FKS_enumEcatSlaves(hMaster, index, pSlaveState, flags);
end;

//------ KS_deleteEcatSlave ------
type
  TKS_deleteEcatSlave = function (hSlave: KSHandle): Error; stdcall;
  PKS_deleteEcatSlave = ^TKS_deleteEcatSlave;
var
  FKS_deleteEcatSlave: TKS_deleteEcatSlave;
        
function KS_deleteEcatSlave(hSlave: KSHandle): Error; stdcall;
begin
  if @FKS_deleteEcatSlave = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_deleteEcatSlave := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_deleteEcatSlave := PKS_deleteEcatSlave(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_deleteEcatSlave'));
    if @FKS_deleteEcatSlave = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_deleteEcatSlave not found in DLL');
      KS_deleteEcatSlave := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_deleteEcatSlave := FKS_deleteEcatSlave(hSlave);
end;

//------ KS_writeEcatSlaveId ------
type
  TKS_writeEcatSlaveId = function (hSlave: KSHandle; id: int; flags: int): Error; stdcall;
  PKS_writeEcatSlaveId = ^TKS_writeEcatSlaveId;
var
  FKS_writeEcatSlaveId: TKS_writeEcatSlaveId;
        
function KS_writeEcatSlaveId(hSlave: KSHandle; id: int; flags: int): Error; stdcall;
begin
  if @FKS_writeEcatSlaveId = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_writeEcatSlaveId := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_writeEcatSlaveId := PKS_writeEcatSlaveId(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_writeEcatSlaveId'));
    if @FKS_writeEcatSlaveId = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_writeEcatSlaveId not found in DLL');
      KS_writeEcatSlaveId := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_writeEcatSlaveId := FKS_writeEcatSlaveId(hSlave, id, flags);
end;

//------ KS_queryEcatSlaveState ------
type
  TKS_queryEcatSlaveState = function (hSlave: KSHandle; pSlaveState: PKSEcatSlaveState; flags: int): Error; stdcall;
  PKS_queryEcatSlaveState = ^TKS_queryEcatSlaveState;
var
  FKS_queryEcatSlaveState: TKS_queryEcatSlaveState;
        
function KS_queryEcatSlaveState(hSlave: KSHandle; pSlaveState: PKSEcatSlaveState; flags: int): Error; stdcall;
begin
  if @FKS_queryEcatSlaveState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryEcatSlaveState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryEcatSlaveState := PKS_queryEcatSlaveState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryEcatSlaveState'));
    if @FKS_queryEcatSlaveState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryEcatSlaveState not found in DLL');
      KS_queryEcatSlaveState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryEcatSlaveState := FKS_queryEcatSlaveState(hSlave, pSlaveState, flags);
end;

//------ KS_changeEcatSlaveState ------
type
  TKS_changeEcatSlaveState = function (hSlave: KSHandle; state: int; flags: int): Error; stdcall;
  PKS_changeEcatSlaveState = ^TKS_changeEcatSlaveState;
var
  FKS_changeEcatSlaveState: TKS_changeEcatSlaveState;
        
function KS_changeEcatSlaveState(hSlave: KSHandle; state: int; flags: int): Error; stdcall;
begin
  if @FKS_changeEcatSlaveState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_changeEcatSlaveState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_changeEcatSlaveState := PKS_changeEcatSlaveState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_changeEcatSlaveState'));
    if @FKS_changeEcatSlaveState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_changeEcatSlaveState not found in DLL');
      KS_changeEcatSlaveState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_changeEcatSlaveState := FKS_changeEcatSlaveState(hSlave, state, flags);
end;

//------ KS_queryEcatSlaveInfo ------
type
  TKS_queryEcatSlaveInfo = function (hSlave: KSHandle; ppSlaveInfo: PPKSEcatSlaveInfo; flags: int): Error; stdcall;
  PKS_queryEcatSlaveInfo = ^TKS_queryEcatSlaveInfo;
var
  FKS_queryEcatSlaveInfo: TKS_queryEcatSlaveInfo;
        
function KS_queryEcatSlaveInfo(hSlave: KSHandle; ppSlaveInfo: PPKSEcatSlaveInfo; flags: int): Error; stdcall;
begin
  if @FKS_queryEcatSlaveInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryEcatSlaveInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryEcatSlaveInfo := PKS_queryEcatSlaveInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryEcatSlaveInfo'));
    if @FKS_queryEcatSlaveInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryEcatSlaveInfo not found in DLL');
      KS_queryEcatSlaveInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryEcatSlaveInfo := FKS_queryEcatSlaveInfo(hSlave, ppSlaveInfo, flags);
end;

//------ KS_queryEcatDataObjInfo ------
type
  TKS_queryEcatDataObjInfo = function (hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSEcatDataObjInfo; flags: int): Error; stdcall;
  PKS_queryEcatDataObjInfo = ^TKS_queryEcatDataObjInfo;
var
  FKS_queryEcatDataObjInfo: TKS_queryEcatDataObjInfo;
        
function KS_queryEcatDataObjInfo(hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSEcatDataObjInfo; flags: int): Error; stdcall;
begin
  if @FKS_queryEcatDataObjInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryEcatDataObjInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryEcatDataObjInfo := PKS_queryEcatDataObjInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryEcatDataObjInfo'));
    if @FKS_queryEcatDataObjInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryEcatDataObjInfo not found in DLL');
      KS_queryEcatDataObjInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryEcatDataObjInfo := FKS_queryEcatDataObjInfo(hSlave, objIndex, ppDataObjInfo, flags);
end;

//------ KS_queryEcatDataVarInfo ------
type
  TKS_queryEcatDataVarInfo = function (hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSEcatDataVarInfo; flags: int): Error; stdcall;
  PKS_queryEcatDataVarInfo = ^TKS_queryEcatDataVarInfo;
var
  FKS_queryEcatDataVarInfo: TKS_queryEcatDataVarInfo;
        
function KS_queryEcatDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSEcatDataVarInfo; flags: int): Error; stdcall;
begin
  if @FKS_queryEcatDataVarInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryEcatDataVarInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryEcatDataVarInfo := PKS_queryEcatDataVarInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryEcatDataVarInfo'));
    if @FKS_queryEcatDataVarInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryEcatDataVarInfo not found in DLL');
      KS_queryEcatDataVarInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryEcatDataVarInfo := FKS_queryEcatDataVarInfo(hSlave, objIndex, varIndex, ppDataVarInfo, flags);
end;

//------ KS_enumEcatDataObjInfo ------
type
  TKS_enumEcatDataObjInfo = function (hSlave: KSHandle; objEnumIndex: int; ppDataObjInfo: PPKSEcatDataObjInfo; flags: int): Error; stdcall;
  PKS_enumEcatDataObjInfo = ^TKS_enumEcatDataObjInfo;
var
  FKS_enumEcatDataObjInfo: TKS_enumEcatDataObjInfo;
        
function KS_enumEcatDataObjInfo(hSlave: KSHandle; objEnumIndex: int; ppDataObjInfo: PPKSEcatDataObjInfo; flags: int): Error; stdcall;
begin
  if @FKS_enumEcatDataObjInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumEcatDataObjInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumEcatDataObjInfo := PKS_enumEcatDataObjInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumEcatDataObjInfo'));
    if @FKS_enumEcatDataObjInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumEcatDataObjInfo not found in DLL');
      KS_enumEcatDataObjInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumEcatDataObjInfo := FKS_enumEcatDataObjInfo(hSlave, objEnumIndex, ppDataObjInfo, flags);
end;

//------ KS_enumEcatDataVarInfo ------
type
  TKS_enumEcatDataVarInfo = function (hSlave: KSHandle; objEnumIndex: int; varEnumIndex: int; ppDataVarInfo: PPKSEcatDataVarInfo; flags: int): Error; stdcall;
  PKS_enumEcatDataVarInfo = ^TKS_enumEcatDataVarInfo;
var
  FKS_enumEcatDataVarInfo: TKS_enumEcatDataVarInfo;
        
function KS_enumEcatDataVarInfo(hSlave: KSHandle; objEnumIndex: int; varEnumIndex: int; ppDataVarInfo: PPKSEcatDataVarInfo; flags: int): Error; stdcall;
begin
  if @FKS_enumEcatDataVarInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumEcatDataVarInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumEcatDataVarInfo := PKS_enumEcatDataVarInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumEcatDataVarInfo'));
    if @FKS_enumEcatDataVarInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumEcatDataVarInfo not found in DLL');
      KS_enumEcatDataVarInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumEcatDataVarInfo := FKS_enumEcatDataVarInfo(hSlave, objEnumIndex, varEnumIndex, ppDataVarInfo, flags);
end;

//------ KS_loadEcatInitCommands ------
type
  TKS_loadEcatInitCommands = function (hSlave: KSHandle; filename: PAnsiChar; flags: int): Error; stdcall;
  PKS_loadEcatInitCommands = ^TKS_loadEcatInitCommands;
var
  FKS_loadEcatInitCommands: TKS_loadEcatInitCommands;
        
function KS_loadEcatInitCommands(hSlave: KSHandle; filename: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_loadEcatInitCommands = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_loadEcatInitCommands := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_loadEcatInitCommands := PKS_loadEcatInitCommands(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_loadEcatInitCommands'));
    if @FKS_loadEcatInitCommands = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_loadEcatInitCommands not found in DLL');
      KS_loadEcatInitCommands := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_loadEcatInitCommands := FKS_loadEcatInitCommands(hSlave, filename, flags);
end;

//------ KS_createEcatDataSet ------
type
  TKS_createEcatDataSet = function (hObject: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int): Error; stdcall;
  PKS_createEcatDataSet = ^TKS_createEcatDataSet;
var
  FKS_createEcatDataSet: TKS_createEcatDataSet;
        
function KS_createEcatDataSet(hObject: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int): Error; stdcall;
begin
  if @FKS_createEcatDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createEcatDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createEcatDataSet := PKS_createEcatDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createEcatDataSet'));
    if @FKS_createEcatDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createEcatDataSet not found in DLL');
      KS_createEcatDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createEcatDataSet := FKS_createEcatDataSet(hObject, phSet, ppAppPtr, ppSysPtr, flags);
end;

//------ KS_deleteEcatDataSet ------
type
  TKS_deleteEcatDataSet = function (hSet: KSHandle): Error; stdcall;
  PKS_deleteEcatDataSet = ^TKS_deleteEcatDataSet;
var
  FKS_deleteEcatDataSet: TKS_deleteEcatDataSet;
        
function KS_deleteEcatDataSet(hSet: KSHandle): Error; stdcall;
begin
  if @FKS_deleteEcatDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_deleteEcatDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_deleteEcatDataSet := PKS_deleteEcatDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_deleteEcatDataSet'));
    if @FKS_deleteEcatDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_deleteEcatDataSet not found in DLL');
      KS_deleteEcatDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_deleteEcatDataSet := FKS_deleteEcatDataSet(hSet);
end;

//------ KS_assignEcatDataSet ------
type
  TKS_assignEcatDataSet = function (hSet: KSHandle; hObject: KSHandle; syncIndex: int; placement: int; flags: int): Error; stdcall;
  PKS_assignEcatDataSet = ^TKS_assignEcatDataSet;
var
  FKS_assignEcatDataSet: TKS_assignEcatDataSet;
        
function KS_assignEcatDataSet(hSet: KSHandle; hObject: KSHandle; syncIndex: int; placement: int; flags: int): Error; stdcall;
begin
  if @FKS_assignEcatDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_assignEcatDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_assignEcatDataSet := PKS_assignEcatDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_assignEcatDataSet'));
    if @FKS_assignEcatDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_assignEcatDataSet not found in DLL');
      KS_assignEcatDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_assignEcatDataSet := FKS_assignEcatDataSet(hSet, hObject, syncIndex, placement, flags);
end;

//------ KS_readEcatDataSet ------
type
  TKS_readEcatDataSet = function (hSet: KSHandle; flags: int): Error; stdcall;
  PKS_readEcatDataSet = ^TKS_readEcatDataSet;
var
  FKS_readEcatDataSet: TKS_readEcatDataSet;
        
function KS_readEcatDataSet(hSet: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_readEcatDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readEcatDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readEcatDataSet := PKS_readEcatDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readEcatDataSet'));
    if @FKS_readEcatDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readEcatDataSet not found in DLL');
      KS_readEcatDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readEcatDataSet := FKS_readEcatDataSet(hSet, flags);
end;

//------ KS_postEcatDataSet ------
type
  TKS_postEcatDataSet = function (hSet: KSHandle; flags: int): Error; stdcall;
  PKS_postEcatDataSet = ^TKS_postEcatDataSet;
var
  FKS_postEcatDataSet: TKS_postEcatDataSet;
        
function KS_postEcatDataSet(hSet: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_postEcatDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_postEcatDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_postEcatDataSet := PKS_postEcatDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_postEcatDataSet'));
    if @FKS_postEcatDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_postEcatDataSet not found in DLL');
      KS_postEcatDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_postEcatDataSet := FKS_postEcatDataSet(hSet, flags);
end;

//------ KS_installEcatDataSetHandler ------
type
  TKS_installEcatDataSetHandler = function (hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installEcatDataSetHandler = ^TKS_installEcatDataSetHandler;
var
  FKS_installEcatDataSetHandler: TKS_installEcatDataSetHandler;
        
function KS_installEcatDataSetHandler(hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installEcatDataSetHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installEcatDataSetHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installEcatDataSetHandler := PKS_installEcatDataSetHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installEcatDataSetHandler'));
    if @FKS_installEcatDataSetHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installEcatDataSetHandler not found in DLL');
      KS_installEcatDataSetHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installEcatDataSetHandler := FKS_installEcatDataSetHandler(hSet, eventCode, hSignal, flags);
end;

//------ KS_changeEcatState ------
type
  TKS_changeEcatState = function (hObject: KSHandle; state: int; flags: int): Error; stdcall;
  PKS_changeEcatState = ^TKS_changeEcatState;
var
  FKS_changeEcatState: TKS_changeEcatState;
        
function KS_changeEcatState(hObject: KSHandle; state: int; flags: int): Error; stdcall;
begin
  if @FKS_changeEcatState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_changeEcatState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_changeEcatState := PKS_changeEcatState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_changeEcatState'));
    if @FKS_changeEcatState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_changeEcatState not found in DLL');
      KS_changeEcatState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_changeEcatState := FKS_changeEcatState(hObject, state, flags);
end;

//------ KS_installEcatHandler ------
type
  TKS_installEcatHandler = function (hObject: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installEcatHandler = ^TKS_installEcatHandler;
var
  FKS_installEcatHandler: TKS_installEcatHandler;
        
function KS_installEcatHandler(hObject: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installEcatHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installEcatHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installEcatHandler := PKS_installEcatHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installEcatHandler'));
    if @FKS_installEcatHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installEcatHandler not found in DLL');
      KS_installEcatHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installEcatHandler := FKS_installEcatHandler(hObject, eventCode, hSignal, flags);
end;

//------ KS_getEcatDataObjAddress ------
type
  TKS_getEcatDataObjAddress = function (hSet: KSHandle; hObject: KSHandle; pdoIndex: int; varIndex: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int): Error; stdcall;
  PKS_getEcatDataObjAddress = ^TKS_getEcatDataObjAddress;
var
  FKS_getEcatDataObjAddress: TKS_getEcatDataObjAddress;
        
function KS_getEcatDataObjAddress(hSet: KSHandle; hObject: KSHandle; pdoIndex: int; varIndex: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_getEcatDataObjAddress = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getEcatDataObjAddress := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getEcatDataObjAddress := PKS_getEcatDataObjAddress(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getEcatDataObjAddress'));
    if @FKS_getEcatDataObjAddress = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getEcatDataObjAddress not found in DLL');
      KS_getEcatDataObjAddress := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getEcatDataObjAddress := FKS_getEcatDataObjAddress(hSet, hObject, pdoIndex, varIndex, ppAppPtr, ppSysPtr, pBitOffset, pBitLength, flags);
end;

//------ KS_readEcatDataObj ------
type
  TKS_readEcatDataObj = function (hObject: KSHandle; objIndex: int; varIndex: int; pData: Pointer; pSize: PInt; flags: int): Error; stdcall;
  PKS_readEcatDataObj = ^TKS_readEcatDataObj;
var
  FKS_readEcatDataObj: TKS_readEcatDataObj;
        
function KS_readEcatDataObj(hObject: KSHandle; objIndex: int; varIndex: int; pData: Pointer; pSize: PInt; flags: int): Error; stdcall;
begin
  if @FKS_readEcatDataObj = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readEcatDataObj := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readEcatDataObj := PKS_readEcatDataObj(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readEcatDataObj'));
    if @FKS_readEcatDataObj = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readEcatDataObj not found in DLL');
      KS_readEcatDataObj := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readEcatDataObj := FKS_readEcatDataObj(hObject, objIndex, varIndex, pData, pSize, flags);
end;

//------ KS_postEcatDataObj ------
type
  TKS_postEcatDataObj = function (hObject: KSHandle; objIndex: int; varIndex: int; pData: Pointer; size: int; flags: int): Error; stdcall;
  PKS_postEcatDataObj = ^TKS_postEcatDataObj;
var
  FKS_postEcatDataObj: TKS_postEcatDataObj;
        
function KS_postEcatDataObj(hObject: KSHandle; objIndex: int; varIndex: int; pData: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_postEcatDataObj = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_postEcatDataObj := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_postEcatDataObj := PKS_postEcatDataObj(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_postEcatDataObj'));
    if @FKS_postEcatDataObj = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_postEcatDataObj not found in DLL');
      KS_postEcatDataObj := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_postEcatDataObj := FKS_postEcatDataObj(hObject, objIndex, varIndex, pData, size, flags);
end;

//------ KS_setEcatPdoAssign ------
type
  TKS_setEcatPdoAssign = function (hObject: KSHandle; syncIndex: int; pdoIndex: int; flags: int): Error; stdcall;
  PKS_setEcatPdoAssign = ^TKS_setEcatPdoAssign;
var
  FKS_setEcatPdoAssign: TKS_setEcatPdoAssign;
        
function KS_setEcatPdoAssign(hObject: KSHandle; syncIndex: int; pdoIndex: int; flags: int): Error; stdcall;
begin
  if @FKS_setEcatPdoAssign = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setEcatPdoAssign := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setEcatPdoAssign := PKS_setEcatPdoAssign(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setEcatPdoAssign'));
    if @FKS_setEcatPdoAssign = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setEcatPdoAssign not found in DLL');
      KS_setEcatPdoAssign := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setEcatPdoAssign := FKS_setEcatPdoAssign(hObject, syncIndex, pdoIndex, flags);
end;

//------ KS_setEcatPdoMapping ------
type
  TKS_setEcatPdoMapping = function (hObject: KSHandle; pdoIndex: int; objIndex: int; varIndex: int; bitLength: int; flags: int): Error; stdcall;
  PKS_setEcatPdoMapping = ^TKS_setEcatPdoMapping;
var
  FKS_setEcatPdoMapping: TKS_setEcatPdoMapping;
        
function KS_setEcatPdoMapping(hObject: KSHandle; pdoIndex: int; objIndex: int; varIndex: int; bitLength: int; flags: int): Error; stdcall;
begin
  if @FKS_setEcatPdoMapping = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setEcatPdoMapping := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setEcatPdoMapping := PKS_setEcatPdoMapping(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setEcatPdoMapping'));
    if @FKS_setEcatPdoMapping = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setEcatPdoMapping not found in DLL');
      KS_setEcatPdoMapping := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setEcatPdoMapping := FKS_setEcatPdoMapping(hObject, pdoIndex, objIndex, varIndex, bitLength, flags);
end;

//------ KS_uploadEcatFile ------
type
  TKS_uploadEcatFile = function (hObject: KSHandle; fileName: PAnsiChar; password: int; pData: Pointer; pSize: PInt; flags: int): Error; stdcall;
  PKS_uploadEcatFile = ^TKS_uploadEcatFile;
var
  FKS_uploadEcatFile: TKS_uploadEcatFile;
        
function KS_uploadEcatFile(hObject: KSHandle; fileName: PAnsiChar; password: int; pData: Pointer; pSize: PInt; flags: int): Error; stdcall;
begin
  if @FKS_uploadEcatFile = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_uploadEcatFile := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_uploadEcatFile := PKS_uploadEcatFile(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_uploadEcatFile'));
    if @FKS_uploadEcatFile = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_uploadEcatFile not found in DLL');
      KS_uploadEcatFile := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_uploadEcatFile := FKS_uploadEcatFile(hObject, fileName, password, pData, pSize, flags);
end;

//------ KS_downloadEcatFile ------
type
  TKS_downloadEcatFile = function (hObject: KSHandle; fileName: PAnsiChar; password: int; pData: Pointer; size: int; flags: int): Error; stdcall;
  PKS_downloadEcatFile = ^TKS_downloadEcatFile;
var
  FKS_downloadEcatFile: TKS_downloadEcatFile;
        
function KS_downloadEcatFile(hObject: KSHandle; fileName: PAnsiChar; password: int; pData: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_downloadEcatFile = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_downloadEcatFile := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_downloadEcatFile := PKS_downloadEcatFile(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_downloadEcatFile'));
    if @FKS_downloadEcatFile = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_downloadEcatFile not found in DLL');
      KS_downloadEcatFile := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_downloadEcatFile := FKS_downloadEcatFile(hObject, fileName, password, pData, size, flags);
end;

//------ KS_readEcatSlaveMem ------
type
  TKS_readEcatSlaveMem = function (hSlave: KSHandle; address: int; pData: Pointer; size: int; flags: int): Error; stdcall;
  PKS_readEcatSlaveMem = ^TKS_readEcatSlaveMem;
var
  FKS_readEcatSlaveMem: TKS_readEcatSlaveMem;
        
function KS_readEcatSlaveMem(hSlave: KSHandle; address: int; pData: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_readEcatSlaveMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readEcatSlaveMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readEcatSlaveMem := PKS_readEcatSlaveMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readEcatSlaveMem'));
    if @FKS_readEcatSlaveMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readEcatSlaveMem not found in DLL');
      KS_readEcatSlaveMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readEcatSlaveMem := FKS_readEcatSlaveMem(hSlave, address, pData, size, flags);
end;

//------ KS_writeEcatSlaveMem ------
type
  TKS_writeEcatSlaveMem = function (hSlave: KSHandle; address: int; pData: Pointer; size: int; flags: int): Error; stdcall;
  PKS_writeEcatSlaveMem = ^TKS_writeEcatSlaveMem;
var
  FKS_writeEcatSlaveMem: TKS_writeEcatSlaveMem;
        
function KS_writeEcatSlaveMem(hSlave: KSHandle; address: int; pData: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_writeEcatSlaveMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_writeEcatSlaveMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_writeEcatSlaveMem := PKS_writeEcatSlaveMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_writeEcatSlaveMem'));
    if @FKS_writeEcatSlaveMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_writeEcatSlaveMem not found in DLL');
      KS_writeEcatSlaveMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_writeEcatSlaveMem := FKS_writeEcatSlaveMem(hSlave, address, pData, size, flags);
end;

//------ KS_activateEcatDcMode ------
type
  TKS_activateEcatDcMode = function (hObject: KSHandle; var startTime: Int64; cycleTime: int; shiftTime: int; hTimer: KSHandle; flags: int): Error; stdcall;
  PKS_activateEcatDcMode = ^TKS_activateEcatDcMode;
var
  FKS_activateEcatDcMode: TKS_activateEcatDcMode;
        
function KS_activateEcatDcMode(hObject: KSHandle; var startTime: Int64; cycleTime: int; shiftTime: int; hTimer: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_activateEcatDcMode = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_activateEcatDcMode := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_activateEcatDcMode := PKS_activateEcatDcMode(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_activateEcatDcMode'));
    if @FKS_activateEcatDcMode = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_activateEcatDcMode not found in DLL');
      KS_activateEcatDcMode := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_activateEcatDcMode := FKS_activateEcatDcMode(hObject, startTime, cycleTime, shiftTime, hTimer, flags);
end;

//------ KS_enumEcatDcOpModes ------
type
  TKS_enumEcatDcOpModes = function (hSlave: KSHandle; index: int; pDcOpModeBuf: PAnsiChar; pDescriptionBuf: PAnsiChar; flags: int): Error; stdcall;
  PKS_enumEcatDcOpModes = ^TKS_enumEcatDcOpModes;
var
  FKS_enumEcatDcOpModes: TKS_enumEcatDcOpModes;
        
function KS_enumEcatDcOpModes(hSlave: KSHandle; index: int; pDcOpModeBuf: PAnsiChar; pDescriptionBuf: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_enumEcatDcOpModes = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumEcatDcOpModes := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumEcatDcOpModes := PKS_enumEcatDcOpModes(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumEcatDcOpModes'));
    if @FKS_enumEcatDcOpModes = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumEcatDcOpModes not found in DLL');
      KS_enumEcatDcOpModes := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumEcatDcOpModes := FKS_enumEcatDcOpModes(hSlave, index, pDcOpModeBuf, pDescriptionBuf, flags);
end;

//------ KS_lookupEcatDcOpMode ------
type
  TKS_lookupEcatDcOpMode = function (hSlave: KSHandle; dcOpMode: PAnsiChar; pDcParams: PKSEcatDcParams; flags: int): Error; stdcall;
  PKS_lookupEcatDcOpMode = ^TKS_lookupEcatDcOpMode;
var
  FKS_lookupEcatDcOpMode: TKS_lookupEcatDcOpMode;
        
function KS_lookupEcatDcOpMode(hSlave: KSHandle; dcOpMode: PAnsiChar; pDcParams: PKSEcatDcParams; flags: int): Error; stdcall;
begin
  if @FKS_lookupEcatDcOpMode = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_lookupEcatDcOpMode := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_lookupEcatDcOpMode := PKS_lookupEcatDcOpMode(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_lookupEcatDcOpMode'));
    if @FKS_lookupEcatDcOpMode = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_lookupEcatDcOpMode not found in DLL');
      KS_lookupEcatDcOpMode := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_lookupEcatDcOpMode := FKS_lookupEcatDcOpMode(hSlave, dcOpMode, pDcParams, flags);
end;

//------ KS_configEcatDcOpMode ------
type
  TKS_configEcatDcOpMode = function (hSlave: KSHandle; dcOpMode: PAnsiChar; pDcParams: PKSEcatDcParams; flags: int): Error; stdcall;
  PKS_configEcatDcOpMode = ^TKS_configEcatDcOpMode;
var
  FKS_configEcatDcOpMode: TKS_configEcatDcOpMode;
        
function KS_configEcatDcOpMode(hSlave: KSHandle; dcOpMode: PAnsiChar; pDcParams: PKSEcatDcParams; flags: int): Error; stdcall;
begin
  if @FKS_configEcatDcOpMode = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_configEcatDcOpMode := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_configEcatDcOpMode := PKS_configEcatDcOpMode(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_configEcatDcOpMode'));
    if @FKS_configEcatDcOpMode = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_configEcatDcOpMode not found in DLL');
      KS_configEcatDcOpMode := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_configEcatDcOpMode := FKS_configEcatDcOpMode(hSlave, dcOpMode, pDcParams, flags);
end;

//------ KS_execEcatCommand ------
type
  TKS_execEcatCommand = function (hObject: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execEcatCommand = ^TKS_execEcatCommand;
var
  FKS_execEcatCommand: TKS_execEcatCommand;
        
function KS_execEcatCommand(hObject: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execEcatCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execEcatCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execEcatCommand := PKS_execEcatCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execEcatCommand'));
    if @FKS_execEcatCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execEcatCommand not found in DLL');
      KS_execEcatCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execEcatCommand := FKS_execEcatCommand(hObject, command, pParam, flags);
end;

//------ KS_getEcatAlias ------
type
  TKS_getEcatAlias = function (hObject: KSHandle; objIndex: int; varIndex: int; pAliasBuf: PAnsiChar; flags: int): Error; stdcall;
  PKS_getEcatAlias = ^TKS_getEcatAlias;
var
  FKS_getEcatAlias: TKS_getEcatAlias;
        
function KS_getEcatAlias(hObject: KSHandle; objIndex: int; varIndex: int; pAliasBuf: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_getEcatAlias = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getEcatAlias := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getEcatAlias := PKS_getEcatAlias(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getEcatAlias'));
    if @FKS_getEcatAlias = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getEcatAlias not found in DLL');
      KS_getEcatAlias := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getEcatAlias := FKS_getEcatAlias(hObject, objIndex, varIndex, pAliasBuf, flags);
end;

//------ KS_setEcatAlias ------
type
  TKS_setEcatAlias = function (hObject: KSHandle; objIndex: int; varIndex: int; alias: PAnsiChar; flags: int): Error; stdcall;
  PKS_setEcatAlias = ^TKS_setEcatAlias;
var
  FKS_setEcatAlias: TKS_setEcatAlias;
        
function KS_setEcatAlias(hObject: KSHandle; objIndex: int; varIndex: int; alias: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_setEcatAlias = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setEcatAlias := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setEcatAlias := PKS_setEcatAlias(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setEcatAlias'));
    if @FKS_setEcatAlias = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setEcatAlias not found in DLL');
      KS_setEcatAlias := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setEcatAlias := FKS_setEcatAlias(hObject, objIndex, varIndex, alias, flags);
end;

//------ KS_openEcatSlaveDevice ------
type
  TKS_openEcatSlaveDevice = function (phSlaveDevice: PKSHandle; name: PAnsiChar; flags: int): Error; stdcall;
  PKS_openEcatSlaveDevice = ^TKS_openEcatSlaveDevice;
var
  FKS_openEcatSlaveDevice: TKS_openEcatSlaveDevice;
        
function KS_openEcatSlaveDevice(phSlaveDevice: PKSHandle; name: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_openEcatSlaveDevice = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openEcatSlaveDevice := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openEcatSlaveDevice := PKS_openEcatSlaveDevice(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openEcatSlaveDevice'));
    if @FKS_openEcatSlaveDevice = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openEcatSlaveDevice not found in DLL');
      KS_openEcatSlaveDevice := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openEcatSlaveDevice := FKS_openEcatSlaveDevice(phSlaveDevice, name, flags);
end;

//------ KS_closeEcatSlaveDevice ------
type
  TKS_closeEcatSlaveDevice = function (hSlaveDevice: KSHandle; flags: int): Error; stdcall;
  PKS_closeEcatSlaveDevice = ^TKS_closeEcatSlaveDevice;
var
  FKS_closeEcatSlaveDevice: TKS_closeEcatSlaveDevice;
        
function KS_closeEcatSlaveDevice(hSlaveDevice: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeEcatSlaveDevice = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeEcatSlaveDevice := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeEcatSlaveDevice := PKS_closeEcatSlaveDevice(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeEcatSlaveDevice'));
    if @FKS_closeEcatSlaveDevice = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeEcatSlaveDevice not found in DLL');
      KS_closeEcatSlaveDevice := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeEcatSlaveDevice := FKS_closeEcatSlaveDevice(hSlaveDevice, flags);
end;

//------ KS_queryEcatSlaveDeviceState ------
type
  TKS_queryEcatSlaveDeviceState = function (hSlaveDevice: KSHandle; pSlaveState: PKSEcatSlaveDeviceState; flags: int): Error; stdcall;
  PKS_queryEcatSlaveDeviceState = ^TKS_queryEcatSlaveDeviceState;
var
  FKS_queryEcatSlaveDeviceState: TKS_queryEcatSlaveDeviceState;
        
function KS_queryEcatSlaveDeviceState(hSlaveDevice: KSHandle; pSlaveState: PKSEcatSlaveDeviceState; flags: int): Error; stdcall;
begin
  if @FKS_queryEcatSlaveDeviceState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryEcatSlaveDeviceState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryEcatSlaveDeviceState := PKS_queryEcatSlaveDeviceState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryEcatSlaveDeviceState'));
    if @FKS_queryEcatSlaveDeviceState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryEcatSlaveDeviceState not found in DLL');
      KS_queryEcatSlaveDeviceState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryEcatSlaveDeviceState := FKS_queryEcatSlaveDeviceState(hSlaveDevice, pSlaveState, flags);
end;

//------ KS_createEcatEapNode ------
type
  TKS_createEcatEapNode = function (phNode: PKSHandle; hConnection: KSHandle; networkFile: PAnsiChar; flags: int): Error; stdcall;
  PKS_createEcatEapNode = ^TKS_createEcatEapNode;
var
  FKS_createEcatEapNode: TKS_createEcatEapNode;
        
function KS_createEcatEapNode(phNode: PKSHandle; hConnection: KSHandle; networkFile: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_createEcatEapNode = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createEcatEapNode := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createEcatEapNode := PKS_createEcatEapNode(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createEcatEapNode'));
    if @FKS_createEcatEapNode = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createEcatEapNode not found in DLL');
      KS_createEcatEapNode := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createEcatEapNode := FKS_createEcatEapNode(phNode, hConnection, networkFile, flags);
end;

//------ KS_closeEcatEapNode ------
type
  TKS_closeEcatEapNode = function (hNode: KSHandle; flags: int): Error; stdcall;
  PKS_closeEcatEapNode = ^TKS_closeEcatEapNode;
var
  FKS_closeEcatEapNode: TKS_closeEcatEapNode;
        
function KS_closeEcatEapNode(hNode: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeEcatEapNode = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeEcatEapNode := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeEcatEapNode := PKS_closeEcatEapNode(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeEcatEapNode'));
    if @FKS_closeEcatEapNode = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeEcatEapNode not found in DLL');
      KS_closeEcatEapNode := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeEcatEapNode := FKS_closeEcatEapNode(hNode, flags);
end;

//------ KS_queryEcatEapNodeState ------
type
  TKS_queryEcatEapNodeState = function (hNode: KSHandle; pNodeState: PKSEcatEapNodeState; flags: int): Error; stdcall;
  PKS_queryEcatEapNodeState = ^TKS_queryEcatEapNodeState;
var
  FKS_queryEcatEapNodeState: TKS_queryEcatEapNodeState;
        
function KS_queryEcatEapNodeState(hNode: KSHandle; pNodeState: PKSEcatEapNodeState; flags: int): Error; stdcall;
begin
  if @FKS_queryEcatEapNodeState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryEcatEapNodeState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryEcatEapNodeState := PKS_queryEcatEapNodeState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryEcatEapNodeState'));
    if @FKS_queryEcatEapNodeState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryEcatEapNodeState not found in DLL');
      KS_queryEcatEapNodeState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryEcatEapNodeState := FKS_queryEcatEapNodeState(hNode, pNodeState, flags);
end;

//------ KS_createEcatEapProxy ------
type
  TKS_createEcatEapProxy = function (hNode: KSHandle; phProxy: PKSHandle; pNodeId: PKSEcatEapNodeId; flags: int): Error; stdcall;
  PKS_createEcatEapProxy = ^TKS_createEcatEapProxy;
var
  FKS_createEcatEapProxy: TKS_createEcatEapProxy;
        
function KS_createEcatEapProxy(hNode: KSHandle; phProxy: PKSHandle; pNodeId: PKSEcatEapNodeId; flags: int): Error; stdcall;
begin
  if @FKS_createEcatEapProxy = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createEcatEapProxy := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createEcatEapProxy := PKS_createEcatEapProxy(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createEcatEapProxy'));
    if @FKS_createEcatEapProxy = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createEcatEapProxy not found in DLL');
      KS_createEcatEapProxy := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createEcatEapProxy := FKS_createEcatEapProxy(hNode, phProxy, pNodeId, flags);
end;

//------ KS_deleteEcatEapProxy ------
type
  TKS_deleteEcatEapProxy = function (hProxy: KSHandle; flags: int): Error; stdcall;
  PKS_deleteEcatEapProxy = ^TKS_deleteEcatEapProxy;
var
  FKS_deleteEcatEapProxy: TKS_deleteEcatEapProxy;
        
function KS_deleteEcatEapProxy(hProxy: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_deleteEcatEapProxy = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_deleteEcatEapProxy := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_deleteEcatEapProxy := PKS_deleteEcatEapProxy(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_deleteEcatEapProxy'));
    if @FKS_deleteEcatEapProxy = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_deleteEcatEapProxy not found in DLL');
      KS_deleteEcatEapProxy := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_deleteEcatEapProxy := FKS_deleteEcatEapProxy(hProxy, flags);
end;

//------ KS_queryEcatEapProxyState ------
type
  TKS_queryEcatEapProxyState = function (hProxy: KSHandle; pProxyState: PKSEcatEapProxyState; flags: int): Error; stdcall;
  PKS_queryEcatEapProxyState = ^TKS_queryEcatEapProxyState;
var
  FKS_queryEcatEapProxyState: TKS_queryEcatEapProxyState;
        
function KS_queryEcatEapProxyState(hProxy: KSHandle; pProxyState: PKSEcatEapProxyState; flags: int): Error; stdcall;
begin
  if @FKS_queryEcatEapProxyState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryEcatEapProxyState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryEcatEapProxyState := PKS_queryEcatEapProxyState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryEcatEapProxyState'));
    if @FKS_queryEcatEapProxyState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryEcatEapProxyState not found in DLL');
      KS_queryEcatEapProxyState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryEcatEapProxyState := FKS_queryEcatEapProxyState(hProxy, pProxyState, flags);
end;

//------ _initEtherCATModule ------
procedure _initEtherCATModule();
begin
  _registerKernelAddress('KS_queryEcatMasterState', @KS_queryEcatMasterState);
  _registerKernelAddress('KS_changeEcatMasterState', @KS_changeEcatMasterState);
  _registerKernelAddress('KS_addEcatRedundancy', @KS_addEcatRedundancy);
  _registerKernelAddress('KS_enumEcatSlaves', @KS_enumEcatSlaves);
  _registerKernelAddress('KS_writeEcatSlaveId', @KS_writeEcatSlaveId);
  _registerKernelAddress('KS_queryEcatSlaveState', @KS_queryEcatSlaveState);
  _registerKernelAddress('KS_changeEcatSlaveState', @KS_changeEcatSlaveState);
  _registerKernelAddress('KS_queryEcatSlaveInfo', @KS_queryEcatSlaveInfo);
  _registerKernelAddress('KS_queryEcatDataObjInfo', @KS_queryEcatDataObjInfo);
  _registerKernelAddress('KS_queryEcatDataVarInfo', @KS_queryEcatDataVarInfo);
  _registerKernelAddress('KS_enumEcatDataObjInfo', @KS_enumEcatDataObjInfo);
  _registerKernelAddress('KS_enumEcatDataVarInfo', @KS_enumEcatDataVarInfo);
  _registerKernelAddress('KS_assignEcatDataSet', @KS_assignEcatDataSet);
  _registerKernelAddress('KS_readEcatDataSet', @KS_readEcatDataSet);
  _registerKernelAddress('KS_postEcatDataSet', @KS_postEcatDataSet);
  _registerKernelAddress('KS_installEcatDataSetHandler', @KS_installEcatDataSetHandler);
  _registerKernelAddress('KS_changeEcatState', @KS_changeEcatState);
  _registerKernelAddress('KS_installEcatHandler', @KS_installEcatHandler);
  _registerKernelAddress('KS_getEcatDataObjAddress', @KS_getEcatDataObjAddress);
  _registerKernelAddress('KS_readEcatDataObj', @KS_readEcatDataObj);
  _registerKernelAddress('KS_postEcatDataObj', @KS_postEcatDataObj);
  _registerKernelAddress('KS_readEcatSlaveMem', @KS_readEcatSlaveMem);
  _registerKernelAddress('KS_writeEcatSlaveMem', @KS_writeEcatSlaveMem);
  _registerKernelAddress('KS_activateEcatDcMode', @KS_activateEcatDcMode);
  _registerKernelAddress('KS_enumEcatDcOpModes', @KS_enumEcatDcOpModes);
  _registerKernelAddress('KS_lookupEcatDcOpMode', @KS_lookupEcatDcOpMode);
  _registerKernelAddress('KS_configEcatDcOpMode', @KS_configEcatDcOpMode);
  _registerKernelAddress('KS_execEcatCommand', @KS_execEcatCommand);
  _registerKernelAddress('KS_getEcatAlias', @KS_getEcatAlias);
  _registerKernelAddress('KS_setEcatAlias', @KS_setEcatAlias);
  _registerKernelAddress('KS_openEcatSlaveDevice', @KS_openEcatSlaveDevice);
  _registerKernelAddress('KS_closeEcatSlaveDevice', @KS_closeEcatSlaveDevice);
  _registerKernelAddress('KS_queryEcatSlaveDeviceState', @KS_queryEcatSlaveDeviceState);
  _registerKernelAddress('KS_createEcatEapNode', @KS_createEcatEapNode);
  _registerKernelAddress('KS_closeEcatEapNode', @KS_closeEcatEapNode);
  _registerKernelAddress('KS_queryEcatEapNodeState', @KS_queryEcatEapNodeState);
  _registerKernelAddress('KS_createEcatEapProxy', @KS_createEcatEapProxy);
  _registerKernelAddress('KS_deleteEcatEapProxy', @KS_deleteEcatEapProxy);
  _registerKernelAddress('KS_queryEcatEapProxyState', @KS_queryEcatEapProxyState);
end;

{$DEFINE KS_ETHERCAT_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// MultiFunction Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openBoard ------
type
  TKS_openBoard = function (phBoard: PKSHandle; name: PAnsiChar; hKernel: KSHandle; flags: int): Error; stdcall;
  PKS_openBoard = ^TKS_openBoard;
var
  FKS_openBoard: TKS_openBoard;
        
function KS_openBoard(phBoard: PKSHandle; name: PAnsiChar; hKernel: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_openBoard = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openBoard := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openBoard := PKS_openBoard(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openBoard'));
    if @FKS_openBoard = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openBoard not found in DLL');
      KS_openBoard := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openBoard := FKS_openBoard(phBoard, name, hKernel, flags);
end;

//------ KS_closeBoard ------
type
  TKS_closeBoard = function (hBoard: KSHandle; flags: int): Error; stdcall;
  PKS_closeBoard = ^TKS_closeBoard;
var
  FKS_closeBoard: TKS_closeBoard;
        
function KS_closeBoard(hBoard: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeBoard = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeBoard := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeBoard := PKS_closeBoard(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeBoard'));
    if @FKS_closeBoard = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeBoard not found in DLL');
      KS_closeBoard := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeBoard := FKS_closeBoard(hBoard, flags);
end;

//------ KS_getBoardInfo ------
type
  TKS_getBoardInfo = function (hBoard: KSHandle; pBoardInfo: PKSMfBoardInfo; flags: int): Error; stdcall;
  PKS_getBoardInfo = ^TKS_getBoardInfo;
var
  FKS_getBoardInfo: TKS_getBoardInfo;
        
function KS_getBoardInfo(hBoard: KSHandle; pBoardInfo: PKSMfBoardInfo; flags: int): Error; stdcall;
begin
  if @FKS_getBoardInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getBoardInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getBoardInfo := PKS_getBoardInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getBoardInfo'));
    if @FKS_getBoardInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getBoardInfo not found in DLL');
      KS_getBoardInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getBoardInfo := FKS_getBoardInfo(hBoard, pBoardInfo, flags);
end;

//------ KS_setBoardObject ------
type
  TKS_setBoardObject = function (hBoard: KSHandle; pAppAddr: Pointer; pSysAddr: Pointer): Error; stdcall;
  PKS_setBoardObject = ^TKS_setBoardObject;
var
  FKS_setBoardObject: TKS_setBoardObject;
        
function KS_setBoardObject(hBoard: KSHandle; pAppAddr: Pointer; pSysAddr: Pointer): Error; stdcall;
begin
  if @FKS_setBoardObject = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setBoardObject := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setBoardObject := PKS_setBoardObject(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setBoardObject'));
    if @FKS_setBoardObject = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setBoardObject not found in DLL');
      KS_setBoardObject := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setBoardObject := FKS_setBoardObject(hBoard, pAppAddr, pSysAddr);
end;

//------ KS_getBoardObject ------
type
  TKS_getBoardObject = function (hBoard: KSHandle; ppAddr: PPointer): Error; stdcall;
  PKS_getBoardObject = ^TKS_getBoardObject;
var
  FKS_getBoardObject: TKS_getBoardObject;
        
function KS_getBoardObject(hBoard: KSHandle; ppAddr: PPointer): Error; stdcall;
begin
  if @FKS_getBoardObject = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getBoardObject := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getBoardObject := PKS_getBoardObject(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getBoardObject'));
    if @FKS_getBoardObject = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getBoardObject not found in DLL');
      KS_getBoardObject := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getBoardObject := FKS_getBoardObject(hBoard, ppAddr);
end;

//------ KS_installBoardHandler ------
type
  TKS_installBoardHandler = function (hBoard: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installBoardHandler = ^TKS_installBoardHandler;
var
  FKS_installBoardHandler: TKS_installBoardHandler;
        
function KS_installBoardHandler(hBoard: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installBoardHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installBoardHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installBoardHandler := PKS_installBoardHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installBoardHandler'));
    if @FKS_installBoardHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installBoardHandler not found in DLL');
      KS_installBoardHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installBoardHandler := FKS_installBoardHandler(hBoard, eventCode, hSignal, flags);
end;

//------ KS_execBoardCommand ------
type
  TKS_execBoardCommand = function (hBoard: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execBoardCommand = ^TKS_execBoardCommand;
var
  FKS_execBoardCommand: TKS_execBoardCommand;
        
function KS_execBoardCommand(hBoard: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execBoardCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execBoardCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execBoardCommand := PKS_execBoardCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execBoardCommand'));
    if @FKS_execBoardCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execBoardCommand not found in DLL');
      KS_execBoardCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execBoardCommand := FKS_execBoardCommand(hBoard, command, pParam, flags);
end;

//------ KS_readBoardRegister ------
type
  TKS_readBoardRegister = function (hBoard: KSHandle; rangeIndex: int; offset: int; pValue: PInt; flags: int): Error; stdcall;
  PKS_readBoardRegister = ^TKS_readBoardRegister;
var
  FKS_readBoardRegister: TKS_readBoardRegister;
        
function KS_readBoardRegister(hBoard: KSHandle; rangeIndex: int; offset: int; pValue: PInt; flags: int): Error; stdcall;
begin
  if @FKS_readBoardRegister = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readBoardRegister := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readBoardRegister := PKS_readBoardRegister(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readBoardRegister'));
    if @FKS_readBoardRegister = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readBoardRegister not found in DLL');
      KS_readBoardRegister := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readBoardRegister := FKS_readBoardRegister(hBoard, rangeIndex, offset, pValue, flags);
end;

//------ KS_writeBoardRegister ------
type
  TKS_writeBoardRegister = function (hBoard: KSHandle; rangeIndex: int; offset: int; value: int; flags: int): Error; stdcall;
  PKS_writeBoardRegister = ^TKS_writeBoardRegister;
var
  FKS_writeBoardRegister: TKS_writeBoardRegister;
        
function KS_writeBoardRegister(hBoard: KSHandle; rangeIndex: int; offset: int; value: int; flags: int): Error; stdcall;
begin
  if @FKS_writeBoardRegister = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_writeBoardRegister := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_writeBoardRegister := PKS_writeBoardRegister(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_writeBoardRegister'));
    if @FKS_writeBoardRegister = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_writeBoardRegister not found in DLL');
      KS_writeBoardRegister := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_writeBoardRegister := FKS_writeBoardRegister(hBoard, rangeIndex, offset, value, flags);
end;

//------ KS_getDigitalBit ------
type
  TKS_getDigitalBit = function (hBoard: KSHandle; channel: int; pValue: PUInt; flags: int): Error; stdcall;
  PKS_getDigitalBit = ^TKS_getDigitalBit;
var
  FKS_getDigitalBit: TKS_getDigitalBit;
        
function KS_getDigitalBit(hBoard: KSHandle; channel: int; pValue: PUInt; flags: int): Error; stdcall;
begin
  if @FKS_getDigitalBit = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getDigitalBit := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getDigitalBit := PKS_getDigitalBit(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getDigitalBit'));
    if @FKS_getDigitalBit = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getDigitalBit not found in DLL');
      KS_getDigitalBit := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getDigitalBit := FKS_getDigitalBit(hBoard, channel, pValue, flags);
end;

//------ KS_getDigitalPort ------
type
  TKS_getDigitalPort = function (hBoard: KSHandle; port: int; pValue: PUInt; flags: int): Error; stdcall;
  PKS_getDigitalPort = ^TKS_getDigitalPort;
var
  FKS_getDigitalPort: TKS_getDigitalPort;
        
function KS_getDigitalPort(hBoard: KSHandle; port: int; pValue: PUInt; flags: int): Error; stdcall;
begin
  if @FKS_getDigitalPort = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getDigitalPort := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getDigitalPort := PKS_getDigitalPort(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getDigitalPort'));
    if @FKS_getDigitalPort = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getDigitalPort not found in DLL');
      KS_getDigitalPort := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getDigitalPort := FKS_getDigitalPort(hBoard, port, pValue, flags);
end;

//------ KS_setDigitalBit ------
type
  TKS_setDigitalBit = function (hBoard: KSHandle; channel: int; value: uint; flags: int): Error; stdcall;
  PKS_setDigitalBit = ^TKS_setDigitalBit;
var
  FKS_setDigitalBit: TKS_setDigitalBit;
        
function KS_setDigitalBit(hBoard: KSHandle; channel: int; value: uint; flags: int): Error; stdcall;
begin
  if @FKS_setDigitalBit = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setDigitalBit := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setDigitalBit := PKS_setDigitalBit(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setDigitalBit'));
    if @FKS_setDigitalBit = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setDigitalBit not found in DLL');
      KS_setDigitalBit := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setDigitalBit := FKS_setDigitalBit(hBoard, channel, value, flags);
end;

//------ KS_setDigitalPort ------
type
  TKS_setDigitalPort = function (hBoard: KSHandle; port: int; value: uint; mask: uint; flags: int): Error; stdcall;
  PKS_setDigitalPort = ^TKS_setDigitalPort;
var
  FKS_setDigitalPort: TKS_setDigitalPort;
        
function KS_setDigitalPort(hBoard: KSHandle; port: int; value: uint; mask: uint; flags: int): Error; stdcall;
begin
  if @FKS_setDigitalPort = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setDigitalPort := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setDigitalPort := PKS_setDigitalPort(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setDigitalPort'));
    if @FKS_setDigitalPort = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setDigitalPort not found in DLL');
      KS_setDigitalPort := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setDigitalPort := FKS_setDigitalPort(hBoard, port, value, mask, flags);
end;

//------ KS_configAnalogChannel ------
type
  TKS_configAnalogChannel = function (hBoard: KSHandle; channel: int; gain: int; mode: int; flags: int): Error; stdcall;
  PKS_configAnalogChannel = ^TKS_configAnalogChannel;
var
  FKS_configAnalogChannel: TKS_configAnalogChannel;
        
function KS_configAnalogChannel(hBoard: KSHandle; channel: int; gain: int; mode: int; flags: int): Error; stdcall;
begin
  if @FKS_configAnalogChannel = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_configAnalogChannel := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_configAnalogChannel := PKS_configAnalogChannel(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_configAnalogChannel'));
    if @FKS_configAnalogChannel = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_configAnalogChannel not found in DLL');
      KS_configAnalogChannel := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_configAnalogChannel := FKS_configAnalogChannel(hBoard, channel, gain, mode, flags);
end;

//------ KS_getAnalogValue ------
type
  TKS_getAnalogValue = function (hBoard: KSHandle; channel: int; pValue: PUInt; flags: int): Error; stdcall;
  PKS_getAnalogValue = ^TKS_getAnalogValue;
var
  FKS_getAnalogValue: TKS_getAnalogValue;
        
function KS_getAnalogValue(hBoard: KSHandle; channel: int; pValue: PUInt; flags: int): Error; stdcall;
begin
  if @FKS_getAnalogValue = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getAnalogValue := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getAnalogValue := PKS_getAnalogValue(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getAnalogValue'));
    if @FKS_getAnalogValue = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getAnalogValue not found in DLL');
      KS_getAnalogValue := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getAnalogValue := FKS_getAnalogValue(hBoard, channel, pValue, flags);
end;

//------ KS_initAnalogStream ------
type
  TKS_initAnalogStream = function (hBoard: KSHandle; pChannelSequence: PInt; sequenceLength: int; sequenceCount: int; period: int; flags: int): Error; stdcall;
  PKS_initAnalogStream = ^TKS_initAnalogStream;
var
  FKS_initAnalogStream: TKS_initAnalogStream;
        
function KS_initAnalogStream(hBoard: KSHandle; pChannelSequence: PInt; sequenceLength: int; sequenceCount: int; period: int; flags: int): Error; stdcall;
begin
  if @FKS_initAnalogStream = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_initAnalogStream := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_initAnalogStream := PKS_initAnalogStream(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_initAnalogStream'));
    if @FKS_initAnalogStream = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_initAnalogStream not found in DLL');
      KS_initAnalogStream := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_initAnalogStream := FKS_initAnalogStream(hBoard, pChannelSequence, sequenceLength, sequenceCount, period, flags);
end;

//------ KS_startAnalogStream ------
type
  TKS_startAnalogStream = function (hBoard: KSHandle; flags: int): Error; stdcall;
  PKS_startAnalogStream = ^TKS_startAnalogStream;
var
  FKS_startAnalogStream: TKS_startAnalogStream;
        
function KS_startAnalogStream(hBoard: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_startAnalogStream = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_startAnalogStream := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_startAnalogStream := PKS_startAnalogStream(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_startAnalogStream'));
    if @FKS_startAnalogStream = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_startAnalogStream not found in DLL');
      KS_startAnalogStream := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_startAnalogStream := FKS_startAnalogStream(hBoard, flags);
end;

//------ KS_getAnalogStream ------
type
  TKS_getAnalogStream = function (hBoard: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
  PKS_getAnalogStream = ^TKS_getAnalogStream;
var
  FKS_getAnalogStream: TKS_getAnalogStream;
        
function KS_getAnalogStream(hBoard: KSHandle; pBuffer: Pointer; length: int; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_getAnalogStream = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getAnalogStream := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getAnalogStream := PKS_getAnalogStream(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getAnalogStream'));
    if @FKS_getAnalogStream = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getAnalogStream not found in DLL');
      KS_getAnalogStream := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getAnalogStream := FKS_getAnalogStream(hBoard, pBuffer, length, pLength, flags);
end;

//------ KS_valueToVolt ------
type
  TKS_valueToVolt = function (hBoard: KSHandle; gain: int; mode: int; value: uint; pVolt: PSingle; flags: int): Error; stdcall;
  PKS_valueToVolt = ^TKS_valueToVolt;
var
  FKS_valueToVolt: TKS_valueToVolt;
        
function KS_valueToVolt(hBoard: KSHandle; gain: int; mode: int; value: uint; pVolt: PSingle; flags: int): Error; stdcall;
begin
  if @FKS_valueToVolt = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_valueToVolt := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_valueToVolt := PKS_valueToVolt(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_valueToVolt'));
    if @FKS_valueToVolt = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_valueToVolt not found in DLL');
      KS_valueToVolt := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_valueToVolt := FKS_valueToVolt(hBoard, gain, mode, value, pVolt, flags);
end;

//------ KS_setAnalogValue ------
type
  TKS_setAnalogValue = function (hBoard: KSHandle; channel: int; mode: int; value: int; flags: int): Error; stdcall;
  PKS_setAnalogValue = ^TKS_setAnalogValue;
var
  FKS_setAnalogValue: TKS_setAnalogValue;
        
function KS_setAnalogValue(hBoard: KSHandle; channel: int; mode: int; value: int; flags: int): Error; stdcall;
begin
  if @FKS_setAnalogValue = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_setAnalogValue := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_setAnalogValue := PKS_setAnalogValue(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_setAnalogValue'));
    if @FKS_setAnalogValue = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_setAnalogValue not found in DLL');
      KS_setAnalogValue := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_setAnalogValue := FKS_setAnalogValue(hBoard, channel, mode, value, flags);
end;

//------ KS_voltToValue ------
type
  TKS_voltToValue = function (hBoard: KSHandle; mode: int; volt: Single; pValue: PUInt; flags: int): Error; stdcall;
  PKS_voltToValue = ^TKS_voltToValue;
var
  FKS_voltToValue: TKS_voltToValue;
        
function KS_voltToValue(hBoard: KSHandle; mode: int; volt: Single; pValue: PUInt; flags: int): Error; stdcall;
begin
  if @FKS_voltToValue = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_voltToValue := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_voltToValue := PKS_voltToValue(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_voltToValue'));
    if @FKS_voltToValue = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_voltToValue not found in DLL');
      KS_voltToValue := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_voltToValue := FKS_voltToValue(hBoard, mode, volt, pValue, flags);
end;

//------ _initMultiFunctionModule ------
procedure _initMultiFunctionModule();
begin
  _registerKernelAddress('KS_getBoardInfo', @KS_getBoardInfo);
  _registerKernelAddress('KS_setBoardObject', @KS_setBoardObject);
  _registerKernelAddress('KS_getBoardObject', @KS_getBoardObject);
  _registerKernelAddress('KS_installBoardHandler', @KS_installBoardHandler);
  _registerKernelAddress('KS_execBoardCommand', @KS_execBoardCommand);
  _registerKernelAddress('KS_readBoardRegister', @KS_readBoardRegister);
  _registerKernelAddress('KS_writeBoardRegister', @KS_writeBoardRegister);
  _registerKernelAddress('KS_getDigitalBit', @KS_getDigitalBit);
  _registerKernelAddress('KS_getDigitalPort', @KS_getDigitalPort);
  _registerKernelAddress('KS_setDigitalBit', @KS_setDigitalBit);
  _registerKernelAddress('KS_setDigitalPort', @KS_setDigitalPort);
  _registerKernelAddress('KS_configAnalogChannel', @KS_configAnalogChannel);
  _registerKernelAddress('KS_getAnalogValue', @KS_getAnalogValue);
  _registerKernelAddress('KS_initAnalogStream', @KS_initAnalogStream);
  _registerKernelAddress('KS_startAnalogStream', @KS_startAnalogStream);
  _registerKernelAddress('KS_getAnalogStream', @KS_getAnalogStream);
  _registerKernelAddress('KS_valueToVolt', @KS_valueToVolt);
  _registerKernelAddress('KS_setAnalogValue', @KS_setAnalogValue);
  _registerKernelAddress('KS_voltToValue', @KS_voltToValue);
end;

{$DEFINE KS_MULTIFUNCTION_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// CAN Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openCanFd ------
type
  TKS_openCanFd = function (phCanFd: PKSHandle; name: PAnsiChar; port: int; pCanFdConfig: PKSCanFdConfig; flags: int): Error; stdcall;
  PKS_openCanFd = ^TKS_openCanFd;
var
  FKS_openCanFd: TKS_openCanFd;
        
function KS_openCanFd(phCanFd: PKSHandle; name: PAnsiChar; port: int; pCanFdConfig: PKSCanFdConfig; flags: int): Error; stdcall;
begin
  if @FKS_openCanFd = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openCanFd := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openCanFd := PKS_openCanFd(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openCanFd'));
    if @FKS_openCanFd = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openCanFd not found in DLL');
      KS_openCanFd := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openCanFd := FKS_openCanFd(phCanFd, name, port, pCanFdConfig, flags);
end;

//------ KS_closeCanFd ------
type
  TKS_closeCanFd = function (hCanFd: KSHandle; flags: int): Error; stdcall;
  PKS_closeCanFd = ^TKS_closeCanFd;
var
  FKS_closeCanFd: TKS_closeCanFd;
        
function KS_closeCanFd(hCanFd: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeCanFd = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeCanFd := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeCanFd := PKS_closeCanFd(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeCanFd'));
    if @FKS_closeCanFd = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeCanFd not found in DLL');
      KS_closeCanFd := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeCanFd := FKS_closeCanFd(hCanFd, flags);
end;

//------ KS_xmitCanFdMsg ------
type
  TKS_xmitCanFdMsg = function (hCanFd: KSHandle; pCanFdMsg: PKSCanFdMsg; flags: int): Error; stdcall;
  PKS_xmitCanFdMsg = ^TKS_xmitCanFdMsg;
var
  FKS_xmitCanFdMsg: TKS_xmitCanFdMsg;
        
function KS_xmitCanFdMsg(hCanFd: KSHandle; pCanFdMsg: PKSCanFdMsg; flags: int): Error; stdcall;
begin
  if @FKS_xmitCanFdMsg = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitCanFdMsg := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitCanFdMsg := PKS_xmitCanFdMsg(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitCanFdMsg'));
    if @FKS_xmitCanFdMsg = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitCanFdMsg not found in DLL');
      KS_xmitCanFdMsg := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitCanFdMsg := FKS_xmitCanFdMsg(hCanFd, pCanFdMsg, flags);
end;

//------ KS_recvCanFdMsg ------
type
  TKS_recvCanFdMsg = function (hCanFd: KSHandle; pCanFdMsg: PKSCanFdMsg; flags: int): Error; stdcall;
  PKS_recvCanFdMsg = ^TKS_recvCanFdMsg;
var
  FKS_recvCanFdMsg: TKS_recvCanFdMsg;
        
function KS_recvCanFdMsg(hCanFd: KSHandle; pCanFdMsg: PKSCanFdMsg; flags: int): Error; stdcall;
begin
  if @FKS_recvCanFdMsg = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvCanFdMsg := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvCanFdMsg := PKS_recvCanFdMsg(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvCanFdMsg'));
    if @FKS_recvCanFdMsg = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvCanFdMsg not found in DLL');
      KS_recvCanFdMsg := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvCanFdMsg := FKS_recvCanFdMsg(hCanFd, pCanFdMsg, flags);
end;

//------ KS_installCanFdHandler ------
type
  TKS_installCanFdHandler = function (hCanFd: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installCanFdHandler = ^TKS_installCanFdHandler;
var
  FKS_installCanFdHandler: TKS_installCanFdHandler;
        
function KS_installCanFdHandler(hCanFd: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installCanFdHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installCanFdHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installCanFdHandler := PKS_installCanFdHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installCanFdHandler'));
    if @FKS_installCanFdHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installCanFdHandler not found in DLL');
      KS_installCanFdHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installCanFdHandler := FKS_installCanFdHandler(hCanFd, eventCode, hSignal, flags);
end;

//------ KS_getCanFdState ------
type
  TKS_getCanFdState = function (hCanFd: KSHandle; pCanFdState: PKSCanFdState; flags: int): Error; stdcall;
  PKS_getCanFdState = ^TKS_getCanFdState;
var
  FKS_getCanFdState: TKS_getCanFdState;
        
function KS_getCanFdState(hCanFd: KSHandle; pCanFdState: PKSCanFdState; flags: int): Error; stdcall;
begin
  if @FKS_getCanFdState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getCanFdState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getCanFdState := PKS_getCanFdState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getCanFdState'));
    if @FKS_getCanFdState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getCanFdState not found in DLL');
      KS_getCanFdState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getCanFdState := FKS_getCanFdState(hCanFd, pCanFdState, flags);
end;

//------ KS_execCanFdCommand ------
type
  TKS_execCanFdCommand = function (hCanFd: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execCanFdCommand = ^TKS_execCanFdCommand;
var
  FKS_execCanFdCommand: TKS_execCanFdCommand;
        
function KS_execCanFdCommand(hCanFd: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execCanFdCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execCanFdCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execCanFdCommand := PKS_execCanFdCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execCanFdCommand'));
    if @FKS_execCanFdCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execCanFdCommand not found in DLL');
      KS_execCanFdCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execCanFdCommand := FKS_execCanFdCommand(hCanFd, command, pParam, flags);
end;

//------ KS_openCan ------
type
  TKS_openCan = function (phCan: PKSHandle; name: PAnsiChar; port: int; baudRate: int; flags: int): Error; stdcall;
  PKS_openCan = ^TKS_openCan;
var
  FKS_openCan: TKS_openCan;
        
function KS_openCan(phCan: PKSHandle; name: PAnsiChar; port: int; baudRate: int; flags: int): Error; stdcall;
begin
  if @FKS_openCan = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openCan := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openCan := PKS_openCan(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openCan'));
    if @FKS_openCan = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openCan not found in DLL');
      KS_openCan := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openCan := FKS_openCan(phCan, name, port, baudRate, flags);
end;

//------ KS_openCanEx ------
type
  TKS_openCanEx = function (phCan: PKSHandle; hConnection: KSHandle; port: int; baudRate: int; flags: int): Error; stdcall;
  PKS_openCanEx = ^TKS_openCanEx;
var
  FKS_openCanEx: TKS_openCanEx;
        
function KS_openCanEx(phCan: PKSHandle; hConnection: KSHandle; port: int; baudRate: int; flags: int): Error; stdcall;
begin
  if @FKS_openCanEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openCanEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openCanEx := PKS_openCanEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openCanEx'));
    if @FKS_openCanEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openCanEx not found in DLL');
      KS_openCanEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openCanEx := FKS_openCanEx(phCan, hConnection, port, baudRate, flags);
end;

//------ KS_closeCan ------
type
  TKS_closeCan = function (hCan: KSHandle): Error; stdcall;
  PKS_closeCan = ^TKS_closeCan;
var
  FKS_closeCan: TKS_closeCan;
        
function KS_closeCan(hCan: KSHandle): Error; stdcall;
begin
  if @FKS_closeCan = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeCan := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeCan := PKS_closeCan(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeCan'));
    if @FKS_closeCan = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeCan not found in DLL');
      KS_closeCan := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeCan := FKS_closeCan(hCan);
end;

//------ KS_xmitCanMsg ------
type
  TKS_xmitCanMsg = function (hCan: KSHandle; pCanMsg: PKSCanMsg; flags: int): Error; stdcall;
  PKS_xmitCanMsg = ^TKS_xmitCanMsg;
var
  FKS_xmitCanMsg: TKS_xmitCanMsg;
        
function KS_xmitCanMsg(hCan: KSHandle; pCanMsg: PKSCanMsg; flags: int): Error; stdcall;
begin
  if @FKS_xmitCanMsg = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitCanMsg := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitCanMsg := PKS_xmitCanMsg(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitCanMsg'));
    if @FKS_xmitCanMsg = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitCanMsg not found in DLL');
      KS_xmitCanMsg := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitCanMsg := FKS_xmitCanMsg(hCan, pCanMsg, flags);
end;

//------ KS_recvCanMsg ------
type
  TKS_recvCanMsg = function (hCan: KSHandle; pCanMsg: PKSCanMsg; flags: int): Error; stdcall;
  PKS_recvCanMsg = ^TKS_recvCanMsg;
var
  FKS_recvCanMsg: TKS_recvCanMsg;
        
function KS_recvCanMsg(hCan: KSHandle; pCanMsg: PKSCanMsg; flags: int): Error; stdcall;
begin
  if @FKS_recvCanMsg = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvCanMsg := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvCanMsg := PKS_recvCanMsg(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvCanMsg'));
    if @FKS_recvCanMsg = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvCanMsg not found in DLL');
      KS_recvCanMsg := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvCanMsg := FKS_recvCanMsg(hCan, pCanMsg, flags);
end;

//------ KS_installCanHandler ------
type
  TKS_installCanHandler = function (hCan: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installCanHandler = ^TKS_installCanHandler;
var
  FKS_installCanHandler: TKS_installCanHandler;
        
function KS_installCanHandler(hCan: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installCanHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installCanHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installCanHandler := PKS_installCanHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installCanHandler'));
    if @FKS_installCanHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installCanHandler not found in DLL');
      KS_installCanHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installCanHandler := FKS_installCanHandler(hCan, eventCode, hSignal, flags);
end;

//------ KS_getCanState ------
type
  TKS_getCanState = function (hCan: KSHandle; pCanState: PKSCanState): Error; stdcall;
  PKS_getCanState = ^TKS_getCanState;
var
  FKS_getCanState: TKS_getCanState;
        
function KS_getCanState(hCan: KSHandle; pCanState: PKSCanState): Error; stdcall;
begin
  if @FKS_getCanState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getCanState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getCanState := PKS_getCanState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getCanState'));
    if @FKS_getCanState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getCanState not found in DLL');
      KS_getCanState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getCanState := FKS_getCanState(hCan, pCanState);
end;

//------ KS_execCanCommand ------
type
  TKS_execCanCommand = function (hCan: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execCanCommand = ^TKS_execCanCommand;
var
  FKS_execCanCommand: TKS_execCanCommand;
        
function KS_execCanCommand(hCan: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execCanCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execCanCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execCanCommand := PKS_execCanCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execCanCommand'));
    if @FKS_execCanCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execCanCommand not found in DLL');
      KS_execCanCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execCanCommand := FKS_execCanCommand(hCan, command, pParam, flags);
end;

//------ _initCANModule ------
procedure _initCANModule();
begin
  _registerKernelAddress('KS_xmitCanFdMsg', @KS_xmitCanFdMsg);
  _registerKernelAddress('KS_recvCanFdMsg', @KS_recvCanFdMsg);
  _registerKernelAddress('KS_installCanFdHandler', @KS_installCanFdHandler);
  _registerKernelAddress('KS_getCanFdState', @KS_getCanFdState);
  _registerKernelAddress('KS_execCanFdCommand', @KS_execCanFdCommand);
  _registerKernelAddress('KS_xmitCanMsg', @KS_xmitCanMsg);
  _registerKernelAddress('KS_recvCanMsg', @KS_recvCanMsg);
  _registerKernelAddress('KS_installCanHandler', @KS_installCanHandler);
  _registerKernelAddress('KS_getCanState', @KS_getCanState);
  _registerKernelAddress('KS_execCanCommand', @KS_execCanCommand);
end;

{$DEFINE KS_CAN_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// LIN Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_openLin ------
type
  TKS_openLin = function (phLin: PKSHandle; pDeviceName: PAnsiChar; port: int; pProperties: PKSLinProperties; flags: int): Error; stdcall;
  PKS_openLin = ^TKS_openLin;
var
  FKS_openLin: TKS_openLin;
        
function KS_openLin(phLin: PKSHandle; pDeviceName: PAnsiChar; port: int; pProperties: PKSLinProperties; flags: int): Error; stdcall;
begin
  if @FKS_openLin = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openLin := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openLin := PKS_openLin(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openLin'));
    if @FKS_openLin = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openLin not found in DLL');
      KS_openLin := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openLin := FKS_openLin(phLin, pDeviceName, port, pProperties, flags);
end;

//------ KS_openLinEx ------
type
  TKS_openLinEx = function (phLin: PKSHandle; hLinDevice: KSHandle; pProperties: PKSLinProperties; flags: int): Error; stdcall;
  PKS_openLinEx = ^TKS_openLinEx;
var
  FKS_openLinEx: TKS_openLinEx;
        
function KS_openLinEx(phLin: PKSHandle; hLinDevice: KSHandle; pProperties: PKSLinProperties; flags: int): Error; stdcall;
begin
  if @FKS_openLinEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openLinEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openLinEx := PKS_openLinEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openLinEx'));
    if @FKS_openLinEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openLinEx not found in DLL');
      KS_openLinEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openLinEx := FKS_openLinEx(phLin, hLinDevice, pProperties, flags);
end;

//------ KS_closeLin ------
type
  TKS_closeLin = function (hLin: KSHandle; flags: int): Error; stdcall;
  PKS_closeLin = ^TKS_closeLin;
var
  FKS_closeLin: TKS_closeLin;
        
function KS_closeLin(hLin: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeLin = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeLin := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeLin := PKS_closeLin(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeLin'));
    if @FKS_closeLin = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeLin not found in DLL');
      KS_closeLin := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeLin := FKS_closeLin(hLin, flags);
end;

//------ KS_installLinHandler ------
type
  TKS_installLinHandler = function (hLin: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installLinHandler = ^TKS_installLinHandler;
var
  FKS_installLinHandler: TKS_installLinHandler;
        
function KS_installLinHandler(hLin: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installLinHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installLinHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installLinHandler := PKS_installLinHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installLinHandler'));
    if @FKS_installLinHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installLinHandler not found in DLL');
      KS_installLinHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installLinHandler := FKS_installLinHandler(hLin, eventCode, hSignal, flags);
end;

//------ KS_execLinCommand ------
type
  TKS_execLinCommand = function (hLin: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execLinCommand = ^TKS_execLinCommand;
var
  FKS_execLinCommand: TKS_execLinCommand;
        
function KS_execLinCommand(hLin: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execLinCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execLinCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execLinCommand := PKS_execLinCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execLinCommand'));
    if @FKS_execLinCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execLinCommand not found in DLL');
      KS_execLinCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execLinCommand := FKS_execLinCommand(hLin, command, pParam, flags);
end;

//------ KS_xmitLinHeader ------
type
  TKS_xmitLinHeader = function (hLin: KSHandle; identifier: int; flags: int): Error; stdcall;
  PKS_xmitLinHeader = ^TKS_xmitLinHeader;
var
  FKS_xmitLinHeader: TKS_xmitLinHeader;
        
function KS_xmitLinHeader(hLin: KSHandle; identifier: int; flags: int): Error; stdcall;
begin
  if @FKS_xmitLinHeader = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitLinHeader := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitLinHeader := PKS_xmitLinHeader(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitLinHeader'));
    if @FKS_xmitLinHeader = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitLinHeader not found in DLL');
      KS_xmitLinHeader := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitLinHeader := FKS_xmitLinHeader(hLin, identifier, flags);
end;

//------ KS_xmitLinResponse ------
type
  TKS_xmitLinResponse = function (hLin: KSHandle; pData: Pointer; size: int; flags: int): Error; stdcall;
  PKS_xmitLinResponse = ^TKS_xmitLinResponse;
var
  FKS_xmitLinResponse: TKS_xmitLinResponse;
        
function KS_xmitLinResponse(hLin: KSHandle; pData: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_xmitLinResponse = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_xmitLinResponse := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_xmitLinResponse := PKS_xmitLinResponse(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_xmitLinResponse'));
    if @FKS_xmitLinResponse = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_xmitLinResponse not found in DLL');
      KS_xmitLinResponse := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_xmitLinResponse := FKS_xmitLinResponse(hLin, pData, size, flags);
end;

//------ KS_recvLinMsg ------
type
  TKS_recvLinMsg = function (hLin: KSHandle; pLinMsg: PKSLinMsg; flags: int): Error; stdcall;
  PKS_recvLinMsg = ^TKS_recvLinMsg;
var
  FKS_recvLinMsg: TKS_recvLinMsg;
        
function KS_recvLinMsg(hLin: KSHandle; pLinMsg: PKSLinMsg; flags: int): Error; stdcall;
begin
  if @FKS_recvLinMsg = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvLinMsg := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvLinMsg := PKS_recvLinMsg(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvLinMsg'));
    if @FKS_recvLinMsg = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvLinMsg not found in DLL');
      KS_recvLinMsg := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvLinMsg := FKS_recvLinMsg(hLin, pLinMsg, flags);
end;

//------ KS_getLinState ------
type
  TKS_getLinState = function (hLin: KSHandle; pLinState: PKSLinState; flags: int): Error; stdcall;
  PKS_getLinState = ^TKS_getLinState;
var
  FKS_getLinState: TKS_getLinState;
        
function KS_getLinState(hLin: KSHandle; pLinState: PKSLinState; flags: int): Error; stdcall;
begin
  if @FKS_getLinState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getLinState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getLinState := PKS_getLinState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getLinState'));
    if @FKS_getLinState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getLinState not found in DLL');
      KS_getLinState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getLinState := FKS_getLinState(hLin, pLinState, flags);
end;

//------ _initLINModule ------
procedure _initLINModule();
begin
  _registerKernelAddress('KS_execLinCommand', @KS_execLinCommand);
  _registerKernelAddress('KS_xmitLinHeader', @KS_xmitLinHeader);
  _registerKernelAddress('KS_xmitLinResponse', @KS_xmitLinResponse);
  _registerKernelAddress('KS_recvLinMsg', @KS_recvLinMsg);
end;

{$DEFINE KS_LIN_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// CANopen Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createCanoMaster ------
type
  TKS_createCanoMaster = function (phMaster: PKSHandle; hConnection: KSHandle; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int): Error; stdcall;
  PKS_createCanoMaster = ^TKS_createCanoMaster;
var
  FKS_createCanoMaster: TKS_createCanoMaster;
        
function KS_createCanoMaster(phMaster: PKSHandle; hConnection: KSHandle; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_createCanoMaster = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createCanoMaster := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createCanoMaster := PKS_createCanoMaster(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createCanoMaster'));
    if @FKS_createCanoMaster = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createCanoMaster not found in DLL');
      KS_createCanoMaster := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createCanoMaster := FKS_createCanoMaster(phMaster, hConnection, libraryPath, topologyFile, flags);
end;

//------ KS_closeCanoMaster ------
type
  TKS_closeCanoMaster = function (hMaster: KSHandle): Error; stdcall;
  PKS_closeCanoMaster = ^TKS_closeCanoMaster;
var
  FKS_closeCanoMaster: TKS_closeCanoMaster;
        
function KS_closeCanoMaster(hMaster: KSHandle): Error; stdcall;
begin
  if @FKS_closeCanoMaster = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeCanoMaster := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeCanoMaster := PKS_closeCanoMaster(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeCanoMaster'));
    if @FKS_closeCanoMaster = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeCanoMaster not found in DLL');
      KS_closeCanoMaster := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeCanoMaster := FKS_closeCanoMaster(hMaster);
end;

//------ KS_installCanoMasterHandler ------
type
  TKS_installCanoMasterHandler = function (hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installCanoMasterHandler = ^TKS_installCanoMasterHandler;
var
  FKS_installCanoMasterHandler: TKS_installCanoMasterHandler;
        
function KS_installCanoMasterHandler(hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installCanoMasterHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installCanoMasterHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installCanoMasterHandler := PKS_installCanoMasterHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installCanoMasterHandler'));
    if @FKS_installCanoMasterHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installCanoMasterHandler not found in DLL');
      KS_installCanoMasterHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installCanoMasterHandler := FKS_installCanoMasterHandler(hMaster, eventCode, hSignal, flags);
end;

//------ KS_queryCanoMasterState ------
type
  TKS_queryCanoMasterState = function (hMaster: KSHandle; pMasterState: PKSCanoMasterState; flags: int): Error; stdcall;
  PKS_queryCanoMasterState = ^TKS_queryCanoMasterState;
var
  FKS_queryCanoMasterState: TKS_queryCanoMasterState;
        
function KS_queryCanoMasterState(hMaster: KSHandle; pMasterState: PKSCanoMasterState; flags: int): Error; stdcall;
begin
  if @FKS_queryCanoMasterState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryCanoMasterState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryCanoMasterState := PKS_queryCanoMasterState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryCanoMasterState'));
    if @FKS_queryCanoMasterState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryCanoMasterState not found in DLL');
      KS_queryCanoMasterState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryCanoMasterState := FKS_queryCanoMasterState(hMaster, pMasterState, flags);
end;

//------ KS_changeCanoMasterState ------
type
  TKS_changeCanoMasterState = function (hMaster: KSHandle; state: int; flags: int): Error; stdcall;
  PKS_changeCanoMasterState = ^TKS_changeCanoMasterState;
var
  FKS_changeCanoMasterState: TKS_changeCanoMasterState;
        
function KS_changeCanoMasterState(hMaster: KSHandle; state: int; flags: int): Error; stdcall;
begin
  if @FKS_changeCanoMasterState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_changeCanoMasterState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_changeCanoMasterState := PKS_changeCanoMasterState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_changeCanoMasterState'));
    if @FKS_changeCanoMasterState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_changeCanoMasterState not found in DLL');
      KS_changeCanoMasterState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_changeCanoMasterState := FKS_changeCanoMasterState(hMaster, state, flags);
end;

//------ KS_createCanoSlave ------
type
  TKS_createCanoSlave = function (hMaster: KSHandle; phSlave: PKSHandle; id: int; position: int; vendor: uint; product: uint; revision: uint; flags: int): Error; stdcall;
  PKS_createCanoSlave = ^TKS_createCanoSlave;
var
  FKS_createCanoSlave: TKS_createCanoSlave;
        
function KS_createCanoSlave(hMaster: KSHandle; phSlave: PKSHandle; id: int; position: int; vendor: uint; product: uint; revision: uint; flags: int): Error; stdcall;
begin
  if @FKS_createCanoSlave = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createCanoSlave := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createCanoSlave := PKS_createCanoSlave(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createCanoSlave'));
    if @FKS_createCanoSlave = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createCanoSlave not found in DLL');
      KS_createCanoSlave := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createCanoSlave := FKS_createCanoSlave(hMaster, phSlave, id, position, vendor, product, revision, flags);
end;

//------ KS_createCanoSlaveIndirect ------
type
  TKS_createCanoSlaveIndirect = function (hMaster: KSHandle; phSlave: PKSHandle; pSlaveState: PKSCanoSlaveState; flags: int): Error; stdcall;
  PKS_createCanoSlaveIndirect = ^TKS_createCanoSlaveIndirect;
var
  FKS_createCanoSlaveIndirect: TKS_createCanoSlaveIndirect;
        
function KS_createCanoSlaveIndirect(hMaster: KSHandle; phSlave: PKSHandle; pSlaveState: PKSCanoSlaveState; flags: int): Error; stdcall;
begin
  if @FKS_createCanoSlaveIndirect = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createCanoSlaveIndirect := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createCanoSlaveIndirect := PKS_createCanoSlaveIndirect(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createCanoSlaveIndirect'));
    if @FKS_createCanoSlaveIndirect = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createCanoSlaveIndirect not found in DLL');
      KS_createCanoSlaveIndirect := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createCanoSlaveIndirect := FKS_createCanoSlaveIndirect(hMaster, phSlave, pSlaveState, flags);
end;

//------ KS_enumCanoSlaves ------
type
  TKS_enumCanoSlaves = function (hMaster: KSHandle; index: int; pSlaveState: PKSCanoSlaveState; flags: int): Error; stdcall;
  PKS_enumCanoSlaves = ^TKS_enumCanoSlaves;
var
  FKS_enumCanoSlaves: TKS_enumCanoSlaves;
        
function KS_enumCanoSlaves(hMaster: KSHandle; index: int; pSlaveState: PKSCanoSlaveState; flags: int): Error; stdcall;
begin
  if @FKS_enumCanoSlaves = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumCanoSlaves := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumCanoSlaves := PKS_enumCanoSlaves(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumCanoSlaves'));
    if @FKS_enumCanoSlaves = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumCanoSlaves not found in DLL');
      KS_enumCanoSlaves := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumCanoSlaves := FKS_enumCanoSlaves(hMaster, index, pSlaveState, flags);
end;

//------ KS_deleteCanoSlave ------
type
  TKS_deleteCanoSlave = function (hSlave: KSHandle): Error; stdcall;
  PKS_deleteCanoSlave = ^TKS_deleteCanoSlave;
var
  FKS_deleteCanoSlave: TKS_deleteCanoSlave;
        
function KS_deleteCanoSlave(hSlave: KSHandle): Error; stdcall;
begin
  if @FKS_deleteCanoSlave = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_deleteCanoSlave := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_deleteCanoSlave := PKS_deleteCanoSlave(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_deleteCanoSlave'));
    if @FKS_deleteCanoSlave = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_deleteCanoSlave not found in DLL');
      KS_deleteCanoSlave := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_deleteCanoSlave := FKS_deleteCanoSlave(hSlave);
end;

//------ KS_queryCanoSlaveState ------
type
  TKS_queryCanoSlaveState = function (hSlave: KSHandle; pSlaveState: PKSCanoSlaveState; flags: int): Error; stdcall;
  PKS_queryCanoSlaveState = ^TKS_queryCanoSlaveState;
var
  FKS_queryCanoSlaveState: TKS_queryCanoSlaveState;
        
function KS_queryCanoSlaveState(hSlave: KSHandle; pSlaveState: PKSCanoSlaveState; flags: int): Error; stdcall;
begin
  if @FKS_queryCanoSlaveState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryCanoSlaveState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryCanoSlaveState := PKS_queryCanoSlaveState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryCanoSlaveState'));
    if @FKS_queryCanoSlaveState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryCanoSlaveState not found in DLL');
      KS_queryCanoSlaveState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryCanoSlaveState := FKS_queryCanoSlaveState(hSlave, pSlaveState, flags);
end;

//------ KS_changeCanoSlaveState ------
type
  TKS_changeCanoSlaveState = function (hSlave: KSHandle; state: int; flags: int): Error; stdcall;
  PKS_changeCanoSlaveState = ^TKS_changeCanoSlaveState;
var
  FKS_changeCanoSlaveState: TKS_changeCanoSlaveState;
        
function KS_changeCanoSlaveState(hSlave: KSHandle; state: int; flags: int): Error; stdcall;
begin
  if @FKS_changeCanoSlaveState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_changeCanoSlaveState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_changeCanoSlaveState := PKS_changeCanoSlaveState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_changeCanoSlaveState'));
    if @FKS_changeCanoSlaveState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_changeCanoSlaveState not found in DLL');
      KS_changeCanoSlaveState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_changeCanoSlaveState := FKS_changeCanoSlaveState(hSlave, state, flags);
end;

//------ KS_queryCanoSlaveInfo ------
type
  TKS_queryCanoSlaveInfo = function (hSlave: KSHandle; ppSlaveInfo: PPKSCanoSlaveInfo; flags: int): Error; stdcall;
  PKS_queryCanoSlaveInfo = ^TKS_queryCanoSlaveInfo;
var
  FKS_queryCanoSlaveInfo: TKS_queryCanoSlaveInfo;
        
function KS_queryCanoSlaveInfo(hSlave: KSHandle; ppSlaveInfo: PPKSCanoSlaveInfo; flags: int): Error; stdcall;
begin
  if @FKS_queryCanoSlaveInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryCanoSlaveInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryCanoSlaveInfo := PKS_queryCanoSlaveInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryCanoSlaveInfo'));
    if @FKS_queryCanoSlaveInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryCanoSlaveInfo not found in DLL');
      KS_queryCanoSlaveInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryCanoSlaveInfo := FKS_queryCanoSlaveInfo(hSlave, ppSlaveInfo, flags);
end;

//------ KS_queryCanoDataObjInfo ------
type
  TKS_queryCanoDataObjInfo = function (hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSCanoDataObjInfo; flags: int): Error; stdcall;
  PKS_queryCanoDataObjInfo = ^TKS_queryCanoDataObjInfo;
var
  FKS_queryCanoDataObjInfo: TKS_queryCanoDataObjInfo;
        
function KS_queryCanoDataObjInfo(hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSCanoDataObjInfo; flags: int): Error; stdcall;
begin
  if @FKS_queryCanoDataObjInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryCanoDataObjInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryCanoDataObjInfo := PKS_queryCanoDataObjInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryCanoDataObjInfo'));
    if @FKS_queryCanoDataObjInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryCanoDataObjInfo not found in DLL');
      KS_queryCanoDataObjInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryCanoDataObjInfo := FKS_queryCanoDataObjInfo(hSlave, objIndex, ppDataObjInfo, flags);
end;

//------ KS_queryCanoDataVarInfo ------
type
  TKS_queryCanoDataVarInfo = function (hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSCanoDataVarInfo; flags: int): Error; stdcall;
  PKS_queryCanoDataVarInfo = ^TKS_queryCanoDataVarInfo;
var
  FKS_queryCanoDataVarInfo: TKS_queryCanoDataVarInfo;
        
function KS_queryCanoDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSCanoDataVarInfo; flags: int): Error; stdcall;
begin
  if @FKS_queryCanoDataVarInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryCanoDataVarInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryCanoDataVarInfo := PKS_queryCanoDataVarInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryCanoDataVarInfo'));
    if @FKS_queryCanoDataVarInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryCanoDataVarInfo not found in DLL');
      KS_queryCanoDataVarInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryCanoDataVarInfo := FKS_queryCanoDataVarInfo(hSlave, objIndex, varIndex, ppDataVarInfo, flags);
end;

//------ KS_enumCanoDataObjInfo ------
type
  TKS_enumCanoDataObjInfo = function (hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSCanoDataObjInfo; flags: int): Error; stdcall;
  PKS_enumCanoDataObjInfo = ^TKS_enumCanoDataObjInfo;
var
  FKS_enumCanoDataObjInfo: TKS_enumCanoDataObjInfo;
        
function KS_enumCanoDataObjInfo(hSlave: KSHandle; objIndex: int; ppDataObjInfo: PPKSCanoDataObjInfo; flags: int): Error; stdcall;
begin
  if @FKS_enumCanoDataObjInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumCanoDataObjInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumCanoDataObjInfo := PKS_enumCanoDataObjInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumCanoDataObjInfo'));
    if @FKS_enumCanoDataObjInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumCanoDataObjInfo not found in DLL');
      KS_enumCanoDataObjInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumCanoDataObjInfo := FKS_enumCanoDataObjInfo(hSlave, objIndex, ppDataObjInfo, flags);
end;

//------ KS_enumCanoDataVarInfo ------
type
  TKS_enumCanoDataVarInfo = function (hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSCanoDataVarInfo; flags: int): Error; stdcall;
  PKS_enumCanoDataVarInfo = ^TKS_enumCanoDataVarInfo;
var
  FKS_enumCanoDataVarInfo: TKS_enumCanoDataVarInfo;
        
function KS_enumCanoDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSCanoDataVarInfo; flags: int): Error; stdcall;
begin
  if @FKS_enumCanoDataVarInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumCanoDataVarInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumCanoDataVarInfo := PKS_enumCanoDataVarInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumCanoDataVarInfo'));
    if @FKS_enumCanoDataVarInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumCanoDataVarInfo not found in DLL');
      KS_enumCanoDataVarInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumCanoDataVarInfo := FKS_enumCanoDataVarInfo(hSlave, objIndex, varIndex, ppDataVarInfo, flags);
end;

//------ KS_createCanoDataSet ------
type
  TKS_createCanoDataSet = function (hMaster: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int): Error; stdcall;
  PKS_createCanoDataSet = ^TKS_createCanoDataSet;
var
  FKS_createCanoDataSet: TKS_createCanoDataSet;
        
function KS_createCanoDataSet(hMaster: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int): Error; stdcall;
begin
  if @FKS_createCanoDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createCanoDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createCanoDataSet := PKS_createCanoDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createCanoDataSet'));
    if @FKS_createCanoDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createCanoDataSet not found in DLL');
      KS_createCanoDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createCanoDataSet := FKS_createCanoDataSet(hMaster, phSet, ppAppPtr, ppSysPtr, flags);
end;

//------ KS_deleteCanoDataSet ------
type
  TKS_deleteCanoDataSet = function (hSet: KSHandle): Error; stdcall;
  PKS_deleteCanoDataSet = ^TKS_deleteCanoDataSet;
var
  FKS_deleteCanoDataSet: TKS_deleteCanoDataSet;
        
function KS_deleteCanoDataSet(hSet: KSHandle): Error; stdcall;
begin
  if @FKS_deleteCanoDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_deleteCanoDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_deleteCanoDataSet := PKS_deleteCanoDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_deleteCanoDataSet'));
    if @FKS_deleteCanoDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_deleteCanoDataSet not found in DLL');
      KS_deleteCanoDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_deleteCanoDataSet := FKS_deleteCanoDataSet(hSet);
end;

//------ KS_assignCanoDataSet ------
type
  TKS_assignCanoDataSet = function (hSet: KSHandle; hSlave: KSHandle; pdoIndex: int; placement: int; flags: int): Error; stdcall;
  PKS_assignCanoDataSet = ^TKS_assignCanoDataSet;
var
  FKS_assignCanoDataSet: TKS_assignCanoDataSet;
        
function KS_assignCanoDataSet(hSet: KSHandle; hSlave: KSHandle; pdoIndex: int; placement: int; flags: int): Error; stdcall;
begin
  if @FKS_assignCanoDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_assignCanoDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_assignCanoDataSet := PKS_assignCanoDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_assignCanoDataSet'));
    if @FKS_assignCanoDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_assignCanoDataSet not found in DLL');
      KS_assignCanoDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_assignCanoDataSet := FKS_assignCanoDataSet(hSet, hSlave, pdoIndex, placement, flags);
end;

//------ KS_readCanoDataSet ------
type
  TKS_readCanoDataSet = function (hSet: KSHandle; flags: int): Error; stdcall;
  PKS_readCanoDataSet = ^TKS_readCanoDataSet;
var
  FKS_readCanoDataSet: TKS_readCanoDataSet;
        
function KS_readCanoDataSet(hSet: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_readCanoDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readCanoDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readCanoDataSet := PKS_readCanoDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readCanoDataSet'));
    if @FKS_readCanoDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readCanoDataSet not found in DLL');
      KS_readCanoDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readCanoDataSet := FKS_readCanoDataSet(hSet, flags);
end;

//------ KS_postCanoDataSet ------
type
  TKS_postCanoDataSet = function (hSet: KSHandle; flags: int): Error; stdcall;
  PKS_postCanoDataSet = ^TKS_postCanoDataSet;
var
  FKS_postCanoDataSet: TKS_postCanoDataSet;
        
function KS_postCanoDataSet(hSet: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_postCanoDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_postCanoDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_postCanoDataSet := PKS_postCanoDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_postCanoDataSet'));
    if @FKS_postCanoDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_postCanoDataSet not found in DLL');
      KS_postCanoDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_postCanoDataSet := FKS_postCanoDataSet(hSet, flags);
end;

//------ KS_installCanoDataSetHandler ------
type
  TKS_installCanoDataSetHandler = function (hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installCanoDataSetHandler = ^TKS_installCanoDataSetHandler;
var
  FKS_installCanoDataSetHandler: TKS_installCanoDataSetHandler;
        
function KS_installCanoDataSetHandler(hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installCanoDataSetHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installCanoDataSetHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installCanoDataSetHandler := PKS_installCanoDataSetHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installCanoDataSetHandler'));
    if @FKS_installCanoDataSetHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installCanoDataSetHandler not found in DLL');
      KS_installCanoDataSetHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installCanoDataSetHandler := FKS_installCanoDataSetHandler(hSet, eventCode, hSignal, flags);
end;

//------ KS_getCanoDataObjAddress ------
type
  TKS_getCanoDataObjAddress = function (hSet: KSHandle; hSlave: KSHandle; pdoIndex: int; subIndex: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int): Error; stdcall;
  PKS_getCanoDataObjAddress = ^TKS_getCanoDataObjAddress;
var
  FKS_getCanoDataObjAddress: TKS_getCanoDataObjAddress;
        
function KS_getCanoDataObjAddress(hSet: KSHandle; hSlave: KSHandle; pdoIndex: int; subIndex: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_getCanoDataObjAddress = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getCanoDataObjAddress := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getCanoDataObjAddress := PKS_getCanoDataObjAddress(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getCanoDataObjAddress'));
    if @FKS_getCanoDataObjAddress = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getCanoDataObjAddress not found in DLL');
      KS_getCanoDataObjAddress := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getCanoDataObjAddress := FKS_getCanoDataObjAddress(hSet, hSlave, pdoIndex, subIndex, ppAppPtr, ppSysPtr, pBitOffset, pBitLength, flags);
end;

//------ KS_readCanoDataObj ------
type
  TKS_readCanoDataObj = function (hSlave: KSHandle; index: int; subIndex: int; pData: Pointer; pSize: PInt; flags: int): Error; stdcall;
  PKS_readCanoDataObj = ^TKS_readCanoDataObj;
var
  FKS_readCanoDataObj: TKS_readCanoDataObj;
        
function KS_readCanoDataObj(hSlave: KSHandle; index: int; subIndex: int; pData: Pointer; pSize: PInt; flags: int): Error; stdcall;
begin
  if @FKS_readCanoDataObj = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readCanoDataObj := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readCanoDataObj := PKS_readCanoDataObj(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readCanoDataObj'));
    if @FKS_readCanoDataObj = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readCanoDataObj not found in DLL');
      KS_readCanoDataObj := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readCanoDataObj := FKS_readCanoDataObj(hSlave, index, subIndex, pData, pSize, flags);
end;

//------ KS_postCanoDataObj ------
type
  TKS_postCanoDataObj = function (hSlave: KSHandle; index: int; subIndex: int; pData: Pointer; size: int; flags: int): Error; stdcall;
  PKS_postCanoDataObj = ^TKS_postCanoDataObj;
var
  FKS_postCanoDataObj: TKS_postCanoDataObj;
        
function KS_postCanoDataObj(hSlave: KSHandle; index: int; subIndex: int; pData: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_postCanoDataObj = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_postCanoDataObj := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_postCanoDataObj := PKS_postCanoDataObj(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_postCanoDataObj'));
    if @FKS_postCanoDataObj = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_postCanoDataObj not found in DLL');
      KS_postCanoDataObj := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_postCanoDataObj := FKS_postCanoDataObj(hSlave, index, subIndex, pData, size, flags);
end;

//------ KS_changeCanoState ------
type
  TKS_changeCanoState = function (hObject: KSHandle; state: int; flags: int): Error; stdcall;
  PKS_changeCanoState = ^TKS_changeCanoState;
var
  FKS_changeCanoState: TKS_changeCanoState;
        
function KS_changeCanoState(hObject: KSHandle; state: int; flags: int): Error; stdcall;
begin
  if @FKS_changeCanoState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_changeCanoState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_changeCanoState := PKS_changeCanoState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_changeCanoState'));
    if @FKS_changeCanoState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_changeCanoState not found in DLL');
      KS_changeCanoState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_changeCanoState := FKS_changeCanoState(hObject, state, flags);
end;

//------ KS_execCanoCommand ------
type
  TKS_execCanoCommand = function (hObject: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execCanoCommand = ^TKS_execCanoCommand;
var
  FKS_execCanoCommand: TKS_execCanoCommand;
        
function KS_execCanoCommand(hObject: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execCanoCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execCanoCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execCanoCommand := PKS_execCanoCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execCanoCommand'));
    if @FKS_execCanoCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execCanoCommand not found in DLL');
      KS_execCanoCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execCanoCommand := FKS_execCanoCommand(hObject, command, pParam, flags);
end;

//------ _initCANopenModule ------
procedure _initCANopenModule();
begin
  _registerKernelAddress('KS_queryCanoMasterState', @KS_queryCanoMasterState);
  _registerKernelAddress('KS_changeCanoMasterState', @KS_changeCanoMasterState);
  _registerKernelAddress('KS_queryCanoSlaveState', @KS_queryCanoSlaveState);
  _registerKernelAddress('KS_changeCanoSlaveState', @KS_changeCanoSlaveState);
  _registerKernelAddress('KS_readCanoDataSet', @KS_readCanoDataSet);
  _registerKernelAddress('KS_postCanoDataSet', @KS_postCanoDataSet);
  _registerKernelAddress('KS_getCanoDataObjAddress', @KS_getCanoDataObjAddress);
  _registerKernelAddress('KS_readCanoDataObj', @KS_readCanoDataObj);
  _registerKernelAddress('KS_postCanoDataObj', @KS_postCanoDataObj);
  _registerKernelAddress('KS_changeCanoState', @KS_changeCanoState);
  _registerKernelAddress('KS_execCanoCommand', @KS_execCanoCommand);
end;

{$DEFINE KS_CANOPEN_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Profibus Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createPbusMaster ------
type
  TKS_createPbusMaster = function (phMaster: PKSHandle; name: PAnsiChar; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int): Error; stdcall;
  PKS_createPbusMaster = ^TKS_createPbusMaster;
var
  FKS_createPbusMaster: TKS_createPbusMaster;
        
function KS_createPbusMaster(phMaster: PKSHandle; name: PAnsiChar; libraryPath: PAnsiChar; topologyFile: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_createPbusMaster = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createPbusMaster := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createPbusMaster := PKS_createPbusMaster(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createPbusMaster'));
    if @FKS_createPbusMaster = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createPbusMaster not found in DLL');
      KS_createPbusMaster := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createPbusMaster := FKS_createPbusMaster(phMaster, name, libraryPath, topologyFile, flags);
end;

//------ KS_closePbusMaster ------
type
  TKS_closePbusMaster = function (hMaster: KSHandle): Error; stdcall;
  PKS_closePbusMaster = ^TKS_closePbusMaster;
var
  FKS_closePbusMaster: TKS_closePbusMaster;
        
function KS_closePbusMaster(hMaster: KSHandle): Error; stdcall;
begin
  if @FKS_closePbusMaster = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closePbusMaster := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closePbusMaster := PKS_closePbusMaster(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closePbusMaster'));
    if @FKS_closePbusMaster = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closePbusMaster not found in DLL');
      KS_closePbusMaster := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closePbusMaster := FKS_closePbusMaster(hMaster);
end;

//------ KS_queryPbusMasterState ------
type
  TKS_queryPbusMasterState = function (hMaster: KSHandle; pMasterState: PKSPbusMasterState; flags: int): Error; stdcall;
  PKS_queryPbusMasterState = ^TKS_queryPbusMasterState;
var
  FKS_queryPbusMasterState: TKS_queryPbusMasterState;
        
function KS_queryPbusMasterState(hMaster: KSHandle; pMasterState: PKSPbusMasterState; flags: int): Error; stdcall;
begin
  if @FKS_queryPbusMasterState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryPbusMasterState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryPbusMasterState := PKS_queryPbusMasterState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryPbusMasterState'));
    if @FKS_queryPbusMasterState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryPbusMasterState not found in DLL');
      KS_queryPbusMasterState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryPbusMasterState := FKS_queryPbusMasterState(hMaster, pMasterState, flags);
end;

//------ KS_changePbusMasterState ------
type
  TKS_changePbusMasterState = function (hMaster: KSHandle; state: int; flags: int): Error; stdcall;
  PKS_changePbusMasterState = ^TKS_changePbusMasterState;
var
  FKS_changePbusMasterState: TKS_changePbusMasterState;
        
function KS_changePbusMasterState(hMaster: KSHandle; state: int; flags: int): Error; stdcall;
begin
  if @FKS_changePbusMasterState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_changePbusMasterState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_changePbusMasterState := PKS_changePbusMasterState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_changePbusMasterState'));
    if @FKS_changePbusMasterState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_changePbusMasterState not found in DLL');
      KS_changePbusMasterState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_changePbusMasterState := FKS_changePbusMasterState(hMaster, state, flags);
end;

//------ KS_enumPbusSlaves ------
type
  TKS_enumPbusSlaves = function (hMaster: KSHandle; index: int; pSlaveState: PKSPbusSlaveState; flags: int): Error; stdcall;
  PKS_enumPbusSlaves = ^TKS_enumPbusSlaves;
var
  FKS_enumPbusSlaves: TKS_enumPbusSlaves;
        
function KS_enumPbusSlaves(hMaster: KSHandle; index: int; pSlaveState: PKSPbusSlaveState; flags: int): Error; stdcall;
begin
  if @FKS_enumPbusSlaves = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumPbusSlaves := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumPbusSlaves := PKS_enumPbusSlaves(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumPbusSlaves'));
    if @FKS_enumPbusSlaves = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumPbusSlaves not found in DLL');
      KS_enumPbusSlaves := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumPbusSlaves := FKS_enumPbusSlaves(hMaster, index, pSlaveState, flags);
end;

//------ KS_installPbusMasterHandler ------
type
  TKS_installPbusMasterHandler = function (hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installPbusMasterHandler = ^TKS_installPbusMasterHandler;
var
  FKS_installPbusMasterHandler: TKS_installPbusMasterHandler;
        
function KS_installPbusMasterHandler(hMaster: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installPbusMasterHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installPbusMasterHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installPbusMasterHandler := PKS_installPbusMasterHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installPbusMasterHandler'));
    if @FKS_installPbusMasterHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installPbusMasterHandler not found in DLL');
      KS_installPbusMasterHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installPbusMasterHandler := FKS_installPbusMasterHandler(hMaster, eventCode, hSignal, flags);
end;

//------ KS_execPbusMasterCommand ------
type
  TKS_execPbusMasterCommand = function (hMaster: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
  PKS_execPbusMasterCommand = ^TKS_execPbusMasterCommand;
var
  FKS_execPbusMasterCommand: TKS_execPbusMasterCommand;
        
function KS_execPbusMasterCommand(hMaster: KSHandle; command: int; pParam: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execPbusMasterCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execPbusMasterCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execPbusMasterCommand := PKS_execPbusMasterCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execPbusMasterCommand'));
    if @FKS_execPbusMasterCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execPbusMasterCommand not found in DLL');
      KS_execPbusMasterCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execPbusMasterCommand := FKS_execPbusMasterCommand(hMaster, command, pParam, flags);
end;

//------ KS_createPbusSlave ------
type
  TKS_createPbusSlave = function (hMaster: KSHandle; phSlave: PKSHandle; address: int; flags: int): Error; stdcall;
  PKS_createPbusSlave = ^TKS_createPbusSlave;
var
  FKS_createPbusSlave: TKS_createPbusSlave;
        
function KS_createPbusSlave(hMaster: KSHandle; phSlave: PKSHandle; address: int; flags: int): Error; stdcall;
begin
  if @FKS_createPbusSlave = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createPbusSlave := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createPbusSlave := PKS_createPbusSlave(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createPbusSlave'));
    if @FKS_createPbusSlave = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createPbusSlave not found in DLL');
      KS_createPbusSlave := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createPbusSlave := FKS_createPbusSlave(hMaster, phSlave, address, flags);
end;

//------ KS_deletePbusSlave ------
type
  TKS_deletePbusSlave = function (hSlave: KSHandle): Error; stdcall;
  PKS_deletePbusSlave = ^TKS_deletePbusSlave;
var
  FKS_deletePbusSlave: TKS_deletePbusSlave;
        
function KS_deletePbusSlave(hSlave: KSHandle): Error; stdcall;
begin
  if @FKS_deletePbusSlave = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_deletePbusSlave := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_deletePbusSlave := PKS_deletePbusSlave(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_deletePbusSlave'));
    if @FKS_deletePbusSlave = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_deletePbusSlave not found in DLL');
      KS_deletePbusSlave := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_deletePbusSlave := FKS_deletePbusSlave(hSlave);
end;

//------ KS_queryPbusSlaveState ------
type
  TKS_queryPbusSlaveState = function (hSlave: KSHandle; pSlaveState: PKSPbusSlaveState; flags: int): Error; stdcall;
  PKS_queryPbusSlaveState = ^TKS_queryPbusSlaveState;
var
  FKS_queryPbusSlaveState: TKS_queryPbusSlaveState;
        
function KS_queryPbusSlaveState(hSlave: KSHandle; pSlaveState: PKSPbusSlaveState; flags: int): Error; stdcall;
begin
  if @FKS_queryPbusSlaveState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryPbusSlaveState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryPbusSlaveState := PKS_queryPbusSlaveState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryPbusSlaveState'));
    if @FKS_queryPbusSlaveState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryPbusSlaveState not found in DLL');
      KS_queryPbusSlaveState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryPbusSlaveState := FKS_queryPbusSlaveState(hSlave, pSlaveState, flags);
end;

//------ KS_changePbusSlaveState ------
type
  TKS_changePbusSlaveState = function (hSlave: KSHandle; state: int; flags: int): Error; stdcall;
  PKS_changePbusSlaveState = ^TKS_changePbusSlaveState;
var
  FKS_changePbusSlaveState: TKS_changePbusSlaveState;
        
function KS_changePbusSlaveState(hSlave: KSHandle; state: int; flags: int): Error; stdcall;
begin
  if @FKS_changePbusSlaveState = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_changePbusSlaveState := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_changePbusSlaveState := PKS_changePbusSlaveState(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_changePbusSlaveState'));
    if @FKS_changePbusSlaveState = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_changePbusSlaveState not found in DLL');
      KS_changePbusSlaveState := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_changePbusSlaveState := FKS_changePbusSlaveState(hSlave, state, flags);
end;

//------ KS_queryPbusSlaveInfo ------
type
  TKS_queryPbusSlaveInfo = function (hSlave: KSHandle; ppSlaveInfo: PPKSPbusSlaveInfo; flags: int): Error; stdcall;
  PKS_queryPbusSlaveInfo = ^TKS_queryPbusSlaveInfo;
var
  FKS_queryPbusSlaveInfo: TKS_queryPbusSlaveInfo;
        
function KS_queryPbusSlaveInfo(hSlave: KSHandle; ppSlaveInfo: PPKSPbusSlaveInfo; flags: int): Error; stdcall;
begin
  if @FKS_queryPbusSlaveInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryPbusSlaveInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryPbusSlaveInfo := PKS_queryPbusSlaveInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryPbusSlaveInfo'));
    if @FKS_queryPbusSlaveInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryPbusSlaveInfo not found in DLL');
      KS_queryPbusSlaveInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryPbusSlaveInfo := FKS_queryPbusSlaveInfo(hSlave, ppSlaveInfo, flags);
end;

//------ KS_queryPbusSlotInfo ------
type
  TKS_queryPbusSlotInfo = function (hSlave: KSHandle; objIndex: int; ppSlotInfo: PPKSPbusSlotInfo; flags: int): Error; stdcall;
  PKS_queryPbusSlotInfo = ^TKS_queryPbusSlotInfo;
var
  FKS_queryPbusSlotInfo: TKS_queryPbusSlotInfo;
        
function KS_queryPbusSlotInfo(hSlave: KSHandle; objIndex: int; ppSlotInfo: PPKSPbusSlotInfo; flags: int): Error; stdcall;
begin
  if @FKS_queryPbusSlotInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryPbusSlotInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryPbusSlotInfo := PKS_queryPbusSlotInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryPbusSlotInfo'));
    if @FKS_queryPbusSlotInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryPbusSlotInfo not found in DLL');
      KS_queryPbusSlotInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryPbusSlotInfo := FKS_queryPbusSlotInfo(hSlave, objIndex, ppSlotInfo, flags);
end;

//------ KS_queryPbusDataVarInfo ------
type
  TKS_queryPbusDataVarInfo = function (hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSPbusDataVarInfo; flags: int): Error; stdcall;
  PKS_queryPbusDataVarInfo = ^TKS_queryPbusDataVarInfo;
var
  FKS_queryPbusDataVarInfo: TKS_queryPbusDataVarInfo;
        
function KS_queryPbusDataVarInfo(hSlave: KSHandle; objIndex: int; varIndex: int; ppDataVarInfo: PPKSPbusDataVarInfo; flags: int): Error; stdcall;
begin
  if @FKS_queryPbusDataVarInfo = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_queryPbusDataVarInfo := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_queryPbusDataVarInfo := PKS_queryPbusDataVarInfo(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_queryPbusDataVarInfo'));
    if @FKS_queryPbusDataVarInfo = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_queryPbusDataVarInfo not found in DLL');
      KS_queryPbusDataVarInfo := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_queryPbusDataVarInfo := FKS_queryPbusDataVarInfo(hSlave, objIndex, varIndex, ppDataVarInfo, flags);
end;

//------ KS_createPbusDataSet ------
type
  TKS_createPbusDataSet = function (hMaster: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int): Error; stdcall;
  PKS_createPbusDataSet = ^TKS_createPbusDataSet;
var
  FKS_createPbusDataSet: TKS_createPbusDataSet;
        
function KS_createPbusDataSet(hMaster: KSHandle; phSet: PKSHandle; ppAppPtr: PPointer; ppSysPtr: PPointer; flags: int): Error; stdcall;
begin
  if @FKS_createPbusDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createPbusDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createPbusDataSet := PKS_createPbusDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createPbusDataSet'));
    if @FKS_createPbusDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createPbusDataSet not found in DLL');
      KS_createPbusDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createPbusDataSet := FKS_createPbusDataSet(hMaster, phSet, ppAppPtr, ppSysPtr, flags);
end;

//------ KS_deletePbusDataSet ------
type
  TKS_deletePbusDataSet = function (hSet: KSHandle): Error; stdcall;
  PKS_deletePbusDataSet = ^TKS_deletePbusDataSet;
var
  FKS_deletePbusDataSet: TKS_deletePbusDataSet;
        
function KS_deletePbusDataSet(hSet: KSHandle): Error; stdcall;
begin
  if @FKS_deletePbusDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_deletePbusDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_deletePbusDataSet := PKS_deletePbusDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_deletePbusDataSet'));
    if @FKS_deletePbusDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_deletePbusDataSet not found in DLL');
      KS_deletePbusDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_deletePbusDataSet := FKS_deletePbusDataSet(hSet);
end;

//------ KS_assignPbusDataSet ------
type
  TKS_assignPbusDataSet = function (hSet: KSHandle; hSlave: KSHandle; slot: int; index: int; placement: int; flags: int): Error; stdcall;
  PKS_assignPbusDataSet = ^TKS_assignPbusDataSet;
var
  FKS_assignPbusDataSet: TKS_assignPbusDataSet;
        
function KS_assignPbusDataSet(hSet: KSHandle; hSlave: KSHandle; slot: int; index: int; placement: int; flags: int): Error; stdcall;
begin
  if @FKS_assignPbusDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_assignPbusDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_assignPbusDataSet := PKS_assignPbusDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_assignPbusDataSet'));
    if @FKS_assignPbusDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_assignPbusDataSet not found in DLL');
      KS_assignPbusDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_assignPbusDataSet := FKS_assignPbusDataSet(hSet, hSlave, slot, index, placement, flags);
end;

//------ KS_readPbusDataSet ------
type
  TKS_readPbusDataSet = function (hSet: KSHandle; flags: int): Error; stdcall;
  PKS_readPbusDataSet = ^TKS_readPbusDataSet;
var
  FKS_readPbusDataSet: TKS_readPbusDataSet;
        
function KS_readPbusDataSet(hSet: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_readPbusDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readPbusDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readPbusDataSet := PKS_readPbusDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readPbusDataSet'));
    if @FKS_readPbusDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readPbusDataSet not found in DLL');
      KS_readPbusDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readPbusDataSet := FKS_readPbusDataSet(hSet, flags);
end;

//------ KS_postPbusDataSet ------
type
  TKS_postPbusDataSet = function (hSet: KSHandle; flags: int): Error; stdcall;
  PKS_postPbusDataSet = ^TKS_postPbusDataSet;
var
  FKS_postPbusDataSet: TKS_postPbusDataSet;
        
function KS_postPbusDataSet(hSet: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_postPbusDataSet = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_postPbusDataSet := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_postPbusDataSet := PKS_postPbusDataSet(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_postPbusDataSet'));
    if @FKS_postPbusDataSet = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_postPbusDataSet not found in DLL');
      KS_postPbusDataSet := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_postPbusDataSet := FKS_postPbusDataSet(hSet, flags);
end;

//------ KS_installPbusDataSetHandler ------
type
  TKS_installPbusDataSetHandler = function (hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installPbusDataSetHandler = ^TKS_installPbusDataSetHandler;
var
  FKS_installPbusDataSetHandler: TKS_installPbusDataSetHandler;
        
function KS_installPbusDataSetHandler(hSet: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installPbusDataSetHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installPbusDataSetHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installPbusDataSetHandler := PKS_installPbusDataSetHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installPbusDataSetHandler'));
    if @FKS_installPbusDataSetHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installPbusDataSetHandler not found in DLL');
      KS_installPbusDataSetHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installPbusDataSetHandler := FKS_installPbusDataSetHandler(hSet, eventCode, hSignal, flags);
end;

//------ KS_getPbusDataObjAddress ------
type
  TKS_getPbusDataObjAddress = function (hSet: KSHandle; hSlave: KSHandle; slot: int; index: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int): Error; stdcall;
  PKS_getPbusDataObjAddress = ^TKS_getPbusDataObjAddress;
var
  FKS_getPbusDataObjAddress: TKS_getPbusDataObjAddress;
        
function KS_getPbusDataObjAddress(hSet: KSHandle; hSlave: KSHandle; slot: int; index: int; ppAppPtr: PPointer; ppSysPtr: PPointer; pBitOffset: PInt; pBitLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_getPbusDataObjAddress = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getPbusDataObjAddress := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getPbusDataObjAddress := PKS_getPbusDataObjAddress(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getPbusDataObjAddress'));
    if @FKS_getPbusDataObjAddress = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getPbusDataObjAddress not found in DLL');
      KS_getPbusDataObjAddress := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getPbusDataObjAddress := FKS_getPbusDataObjAddress(hSet, hSlave, slot, index, ppAppPtr, ppSysPtr, pBitOffset, pBitLength, flags);
end;

//------ KS_readPbusDataObj ------
type
  TKS_readPbusDataObj = function (hSlave: KSHandle; slot: int; index: int; pData: Pointer; pSize: PInt; flags: int): Error; stdcall;
  PKS_readPbusDataObj = ^TKS_readPbusDataObj;
var
  FKS_readPbusDataObj: TKS_readPbusDataObj;
        
function KS_readPbusDataObj(hSlave: KSHandle; slot: int; index: int; pData: Pointer; pSize: PInt; flags: int): Error; stdcall;
begin
  if @FKS_readPbusDataObj = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readPbusDataObj := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readPbusDataObj := PKS_readPbusDataObj(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readPbusDataObj'));
    if @FKS_readPbusDataObj = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readPbusDataObj not found in DLL');
      KS_readPbusDataObj := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readPbusDataObj := FKS_readPbusDataObj(hSlave, slot, index, pData, pSize, flags);
end;

//------ KS_postPbusDataObj ------
type
  TKS_postPbusDataObj = function (hSlave: KSHandle; slot: int; index: int; pData: Pointer; size: int; flags: int): Error; stdcall;
  PKS_postPbusDataObj = ^TKS_postPbusDataObj;
var
  FKS_postPbusDataObj: TKS_postPbusDataObj;
        
function KS_postPbusDataObj(hSlave: KSHandle; slot: int; index: int; pData: Pointer; size: int; flags: int): Error; stdcall;
begin
  if @FKS_postPbusDataObj = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_postPbusDataObj := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_postPbusDataObj := PKS_postPbusDataObj(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_postPbusDataObj'));
    if @FKS_postPbusDataObj = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_postPbusDataObj not found in DLL');
      KS_postPbusDataObj := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_postPbusDataObj := FKS_postPbusDataObj(hSlave, slot, index, pData, size, flags);
end;

//------ _initProfibusModule ------
procedure _initProfibusModule();
begin
  _registerKernelAddress('KS_queryPbusMasterState', @KS_queryPbusMasterState);
  _registerKernelAddress('KS_changePbusMasterState', @KS_changePbusMasterState);
  _registerKernelAddress('KS_execPbusMasterCommand', @KS_execPbusMasterCommand);
  _registerKernelAddress('KS_queryPbusSlaveState', @KS_queryPbusSlaveState);
  _registerKernelAddress('KS_changePbusSlaveState', @KS_changePbusSlaveState);
  _registerKernelAddress('KS_readPbusDataSet', @KS_readPbusDataSet);
  _registerKernelAddress('KS_postPbusDataSet', @KS_postPbusDataSet);
  _registerKernelAddress('KS_getPbusDataObjAddress', @KS_getPbusDataObjAddress);
  _registerKernelAddress('KS_readPbusDataObj', @KS_readPbusDataObj);
  _registerKernelAddress('KS_postPbusDataObj', @KS_postPbusDataObj);
end;

{$DEFINE KS_PROFIBUS_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// RTL Module
//--------------------------------------------------------------------------------------------------------------

//------ KSRTL_calloc ------
type
  TKSRTL_calloc = function (num: uint; size: uint): Pointer; stdcall;
  PKSRTL_calloc = ^TKSRTL_calloc;
var
  FKSRTL_calloc: TKSRTL_calloc;
        
function KSRTL_calloc(num: uint; size: uint): Pointer; stdcall;
begin
  if @FKSRTL_calloc = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_calloc := nil;
        exit;
      end;
    end;

    @FKSRTL_calloc := PKSRTL_calloc(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_calloc'));
    if @FKSRTL_calloc = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_calloc not found in DLL');
      KSRTL_calloc := nil;
      exit;
    end;
  end;

  KSRTL_calloc := FKSRTL_calloc(num, size);
end;

//------ KSRTL_free ------
type
  TKSRTL_free = procedure (ptr: Pointer); stdcall;
  PKSRTL_free = ^TKSRTL_free;
var
  FKSRTL_free: TKSRTL_free;
        
procedure KSRTL_free(ptr: Pointer); stdcall;
begin
  if @FKSRTL_free = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        exit;
      end;
    end;

    @FKSRTL_free := PKSRTL_free(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_free'));
    if @FKSRTL_free = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_free not found in DLL');
      exit;
    end;
  end;

  FKSRTL_free(ptr);
end;

//------ KSRTL_malloc ------
type
  TKSRTL_malloc = function (size: uint): Pointer; stdcall;
  PKSRTL_malloc = ^TKSRTL_malloc;
var
  FKSRTL_malloc: TKSRTL_malloc;
        
function KSRTL_malloc(size: uint): Pointer; stdcall;
begin
  if @FKSRTL_malloc = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_malloc := nil;
        exit;
      end;
    end;

    @FKSRTL_malloc := PKSRTL_malloc(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_malloc'));
    if @FKSRTL_malloc = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_malloc not found in DLL');
      KSRTL_malloc := nil;
      exit;
    end;
  end;

  KSRTL_malloc := FKSRTL_malloc(size);
end;

//------ KSRTL_sin ------
type
  TKSRTL_sin = function (x: Double): Double; stdcall;
  PKSRTL_sin = ^TKSRTL_sin;
var
  FKSRTL_sin: TKSRTL_sin;
        
function KSRTL_sin(x: Double): Double; stdcall;
begin
  if @FKSRTL_sin = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_sin := 0.0;
        exit;
      end;
    end;

    @FKSRTL_sin := PKSRTL_sin(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_sin'));
    if @FKSRTL_sin = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_sin not found in DLL');
      KSRTL_sin := 0.0;
      exit;
    end;
  end;

  KSRTL_sin := FKSRTL_sin(x);
end;

//------ KSRTL_cos ------
type
  TKSRTL_cos = function (x: Double): Double; stdcall;
  PKSRTL_cos = ^TKSRTL_cos;
var
  FKSRTL_cos: TKSRTL_cos;
        
function KSRTL_cos(x: Double): Double; stdcall;
begin
  if @FKSRTL_cos = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_cos := 0.0;
        exit;
      end;
    end;

    @FKSRTL_cos := PKSRTL_cos(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_cos'));
    if @FKSRTL_cos = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_cos not found in DLL');
      KSRTL_cos := 0.0;
      exit;
    end;
  end;

  KSRTL_cos := FKSRTL_cos(x);
end;

//------ KSRTL_tan ------
type
  TKSRTL_tan = function (x: Double): Double; stdcall;
  PKSRTL_tan = ^TKSRTL_tan;
var
  FKSRTL_tan: TKSRTL_tan;
        
function KSRTL_tan(x: Double): Double; stdcall;
begin
  if @FKSRTL_tan = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_tan := 0.0;
        exit;
      end;
    end;

    @FKSRTL_tan := PKSRTL_tan(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_tan'));
    if @FKSRTL_tan = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_tan not found in DLL');
      KSRTL_tan := 0.0;
      exit;
    end;
  end;

  KSRTL_tan := FKSRTL_tan(x);
end;

//------ KSRTL_asin ------
type
  TKSRTL_asin = function (x: Double): Double; stdcall;
  PKSRTL_asin = ^TKSRTL_asin;
var
  FKSRTL_asin: TKSRTL_asin;
        
function KSRTL_asin(x: Double): Double; stdcall;
begin
  if @FKSRTL_asin = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_asin := 0.0;
        exit;
      end;
    end;

    @FKSRTL_asin := PKSRTL_asin(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_asin'));
    if @FKSRTL_asin = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_asin not found in DLL');
      KSRTL_asin := 0.0;
      exit;
    end;
  end;

  KSRTL_asin := FKSRTL_asin(x);
end;

//------ KSRTL_acos ------
type
  TKSRTL_acos = function (x: Double): Double; stdcall;
  PKSRTL_acos = ^TKSRTL_acos;
var
  FKSRTL_acos: TKSRTL_acos;
        
function KSRTL_acos(x: Double): Double; stdcall;
begin
  if @FKSRTL_acos = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_acos := 0.0;
        exit;
      end;
    end;

    @FKSRTL_acos := PKSRTL_acos(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_acos'));
    if @FKSRTL_acos = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_acos not found in DLL');
      KSRTL_acos := 0.0;
      exit;
    end;
  end;

  KSRTL_acos := FKSRTL_acos(x);
end;

//------ KSRTL_atan ------
type
  TKSRTL_atan = function (x: Double): Double; stdcall;
  PKSRTL_atan = ^TKSRTL_atan;
var
  FKSRTL_atan: TKSRTL_atan;
        
function KSRTL_atan(x: Double): Double; stdcall;
begin
  if @FKSRTL_atan = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_atan := 0.0;
        exit;
      end;
    end;

    @FKSRTL_atan := PKSRTL_atan(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_atan'));
    if @FKSRTL_atan = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_atan not found in DLL');
      KSRTL_atan := 0.0;
      exit;
    end;
  end;

  KSRTL_atan := FKSRTL_atan(x);
end;

//------ KSRTL_atan2 ------
type
  TKSRTL_atan2 = function (y: Double; x: Double): Double; stdcall;
  PKSRTL_atan2 = ^TKSRTL_atan2;
var
  FKSRTL_atan2: TKSRTL_atan2;
        
function KSRTL_atan2(y: Double; x: Double): Double; stdcall;
begin
  if @FKSRTL_atan2 = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_atan2 := 0.0;
        exit;
      end;
    end;

    @FKSRTL_atan2 := PKSRTL_atan2(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_atan2'));
    if @FKSRTL_atan2 = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_atan2 not found in DLL');
      KSRTL_atan2 := 0.0;
      exit;
    end;
  end;

  KSRTL_atan2 := FKSRTL_atan2(y, x);
end;

//------ KSRTL_sinh ------
type
  TKSRTL_sinh = function (x: Double): Double; stdcall;
  PKSRTL_sinh = ^TKSRTL_sinh;
var
  FKSRTL_sinh: TKSRTL_sinh;
        
function KSRTL_sinh(x: Double): Double; stdcall;
begin
  if @FKSRTL_sinh = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_sinh := 0.0;
        exit;
      end;
    end;

    @FKSRTL_sinh := PKSRTL_sinh(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_sinh'));
    if @FKSRTL_sinh = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_sinh not found in DLL');
      KSRTL_sinh := 0.0;
      exit;
    end;
  end;

  KSRTL_sinh := FKSRTL_sinh(x);
end;

//------ KSRTL_cosh ------
type
  TKSRTL_cosh = function (x: Double): Double; stdcall;
  PKSRTL_cosh = ^TKSRTL_cosh;
var
  FKSRTL_cosh: TKSRTL_cosh;
        
function KSRTL_cosh(x: Double): Double; stdcall;
begin
  if @FKSRTL_cosh = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_cosh := 0.0;
        exit;
      end;
    end;

    @FKSRTL_cosh := PKSRTL_cosh(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_cosh'));
    if @FKSRTL_cosh = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_cosh not found in DLL');
      KSRTL_cosh := 0.0;
      exit;
    end;
  end;

  KSRTL_cosh := FKSRTL_cosh(x);
end;

//------ KSRTL_tanh ------
type
  TKSRTL_tanh = function (x: Double): Double; stdcall;
  PKSRTL_tanh = ^TKSRTL_tanh;
var
  FKSRTL_tanh: TKSRTL_tanh;
        
function KSRTL_tanh(x: Double): Double; stdcall;
begin
  if @FKSRTL_tanh = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_tanh := 0.0;
        exit;
      end;
    end;

    @FKSRTL_tanh := PKSRTL_tanh(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_tanh'));
    if @FKSRTL_tanh = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_tanh not found in DLL');
      KSRTL_tanh := 0.0;
      exit;
    end;
  end;

  KSRTL_tanh := FKSRTL_tanh(x);
end;

//------ KSRTL_exp ------
type
  TKSRTL_exp = function (x: Double): Double; stdcall;
  PKSRTL_exp = ^TKSRTL_exp;
var
  FKSRTL_exp: TKSRTL_exp;
        
function KSRTL_exp(x: Double): Double; stdcall;
begin
  if @FKSRTL_exp = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_exp := 0.0;
        exit;
      end;
    end;

    @FKSRTL_exp := PKSRTL_exp(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_exp'));
    if @FKSRTL_exp = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_exp not found in DLL');
      KSRTL_exp := 0.0;
      exit;
    end;
  end;

  KSRTL_exp := FKSRTL_exp(x);
end;

//------ KSRTL_frexp ------
type
  TKSRTL_frexp = function (x: Double; exp: PInt): Double; stdcall;
  PKSRTL_frexp = ^TKSRTL_frexp;
var
  FKSRTL_frexp: TKSRTL_frexp;
        
function KSRTL_frexp(x: Double; exp: PInt): Double; stdcall;
begin
  if @FKSRTL_frexp = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_frexp := 0.0;
        exit;
      end;
    end;

    @FKSRTL_frexp := PKSRTL_frexp(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_frexp'));
    if @FKSRTL_frexp = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_frexp not found in DLL');
      KSRTL_frexp := 0.0;
      exit;
    end;
  end;

  KSRTL_frexp := FKSRTL_frexp(x, exp);
end;

//------ KSRTL_ldexp ------
type
  TKSRTL_ldexp = function (x: Double; exp: int): Double; stdcall;
  PKSRTL_ldexp = ^TKSRTL_ldexp;
var
  FKSRTL_ldexp: TKSRTL_ldexp;
        
function KSRTL_ldexp(x: Double; exp: int): Double; stdcall;
begin
  if @FKSRTL_ldexp = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_ldexp := 0.0;
        exit;
      end;
    end;

    @FKSRTL_ldexp := PKSRTL_ldexp(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_ldexp'));
    if @FKSRTL_ldexp = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_ldexp not found in DLL');
      KSRTL_ldexp := 0.0;
      exit;
    end;
  end;

  KSRTL_ldexp := FKSRTL_ldexp(x, exp);
end;

//------ KSRTL_log ------
type
  TKSRTL_log = function (x: Double): Double; stdcall;
  PKSRTL_log = ^TKSRTL_log;
var
  FKSRTL_log: TKSRTL_log;
        
function KSRTL_log(x: Double): Double; stdcall;
begin
  if @FKSRTL_log = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_log := 0.0;
        exit;
      end;
    end;

    @FKSRTL_log := PKSRTL_log(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_log'));
    if @FKSRTL_log = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_log not found in DLL');
      KSRTL_log := 0.0;
      exit;
    end;
  end;

  KSRTL_log := FKSRTL_log(x);
end;

//------ KSRTL_log10 ------
type
  TKSRTL_log10 = function (x: Double): Double; stdcall;
  PKSRTL_log10 = ^TKSRTL_log10;
var
  FKSRTL_log10: TKSRTL_log10;
        
function KSRTL_log10(x: Double): Double; stdcall;
begin
  if @FKSRTL_log10 = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_log10 := 0.0;
        exit;
      end;
    end;

    @FKSRTL_log10 := PKSRTL_log10(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_log10'));
    if @FKSRTL_log10 = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_log10 not found in DLL');
      KSRTL_log10 := 0.0;
      exit;
    end;
  end;

  KSRTL_log10 := FKSRTL_log10(x);
end;

//------ KSRTL_modf ------
type
  TKSRTL_modf = function (x: Double; intpart: PDouble): Double; stdcall;
  PKSRTL_modf = ^TKSRTL_modf;
var
  FKSRTL_modf: TKSRTL_modf;
        
function KSRTL_modf(x: Double; intpart: PDouble): Double; stdcall;
begin
  if @FKSRTL_modf = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_modf := 0.0;
        exit;
      end;
    end;

    @FKSRTL_modf := PKSRTL_modf(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_modf'));
    if @FKSRTL_modf = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_modf not found in DLL');
      KSRTL_modf := 0.0;
      exit;
    end;
  end;

  KSRTL_modf := FKSRTL_modf(x, intpart);
end;

//------ KSRTL_pow ------
type
  TKSRTL_pow = function (base: Double; exponent: Double): Double; stdcall;
  PKSRTL_pow = ^TKSRTL_pow;
var
  FKSRTL_pow: TKSRTL_pow;
        
function KSRTL_pow(base: Double; exponent: Double): Double; stdcall;
begin
  if @FKSRTL_pow = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_pow := 0.0;
        exit;
      end;
    end;

    @FKSRTL_pow := PKSRTL_pow(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_pow'));
    if @FKSRTL_pow = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_pow not found in DLL');
      KSRTL_pow := 0.0;
      exit;
    end;
  end;

  KSRTL_pow := FKSRTL_pow(base, exponent);
end;

//------ KSRTL_sqrt ------
type
  TKSRTL_sqrt = function (x: Double): Double; stdcall;
  PKSRTL_sqrt = ^TKSRTL_sqrt;
var
  FKSRTL_sqrt: TKSRTL_sqrt;
        
function KSRTL_sqrt(x: Double): Double; stdcall;
begin
  if @FKSRTL_sqrt = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_sqrt := 0.0;
        exit;
      end;
    end;

    @FKSRTL_sqrt := PKSRTL_sqrt(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_sqrt'));
    if @FKSRTL_sqrt = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_sqrt not found in DLL');
      KSRTL_sqrt := 0.0;
      exit;
    end;
  end;

  KSRTL_sqrt := FKSRTL_sqrt(x);
end;

//------ KSRTL_fabs ------
type
  TKSRTL_fabs = function (x: Double): Double; stdcall;
  PKSRTL_fabs = ^TKSRTL_fabs;
var
  FKSRTL_fabs: TKSRTL_fabs;
        
function KSRTL_fabs(x: Double): Double; stdcall;
begin
  if @FKSRTL_fabs = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_fabs := 0.0;
        exit;
      end;
    end;

    @FKSRTL_fabs := PKSRTL_fabs(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_fabs'));
    if @FKSRTL_fabs = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_fabs not found in DLL');
      KSRTL_fabs := 0.0;
      exit;
    end;
  end;

  KSRTL_fabs := FKSRTL_fabs(x);
end;

//------ KSRTL_ceil ------
type
  TKSRTL_ceil = function (x: Double): Double; stdcall;
  PKSRTL_ceil = ^TKSRTL_ceil;
var
  FKSRTL_ceil: TKSRTL_ceil;
        
function KSRTL_ceil(x: Double): Double; stdcall;
begin
  if @FKSRTL_ceil = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_ceil := 0.0;
        exit;
      end;
    end;

    @FKSRTL_ceil := PKSRTL_ceil(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_ceil'));
    if @FKSRTL_ceil = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_ceil not found in DLL');
      KSRTL_ceil := 0.0;
      exit;
    end;
  end;

  KSRTL_ceil := FKSRTL_ceil(x);
end;

//------ KSRTL_floor ------
type
  TKSRTL_floor = function (x: Double): Double; stdcall;
  PKSRTL_floor = ^TKSRTL_floor;
var
  FKSRTL_floor: TKSRTL_floor;
        
function KSRTL_floor(x: Double): Double; stdcall;
begin
  if @FKSRTL_floor = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_floor := 0.0;
        exit;
      end;
    end;

    @FKSRTL_floor := PKSRTL_floor(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_floor'));
    if @FKSRTL_floor = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_floor not found in DLL');
      KSRTL_floor := 0.0;
      exit;
    end;
  end;

  KSRTL_floor := FKSRTL_floor(x);
end;

//------ KSRTL_fmod ------
type
  TKSRTL_fmod = function (numerator: Double; denominator: Double): Double; stdcall;
  PKSRTL_fmod = ^TKSRTL_fmod;
var
  FKSRTL_fmod: TKSRTL_fmod;
        
function KSRTL_fmod(numerator: Double; denominator: Double): Double; stdcall;
begin
  if @FKSRTL_fmod = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_fmod := 0.0;
        exit;
      end;
    end;

    @FKSRTL_fmod := PKSRTL_fmod(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_fmod'));
    if @FKSRTL_fmod = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_fmod not found in DLL');
      KSRTL_fmod := 0.0;
      exit;
    end;
  end;

  KSRTL_fmod := FKSRTL_fmod(numerator, denominator);
end;

//------ KSRTL_memchr ------
type
  TKSRTL_memchr = function (ptr: Pointer; value: int; num: uint): Pointer; stdcall;
  PKSRTL_memchr = ^TKSRTL_memchr;
var
  FKSRTL_memchr: TKSRTL_memchr;
        
function KSRTL_memchr(ptr: Pointer; value: int; num: uint): Pointer; stdcall;
begin
  if @FKSRTL_memchr = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_memchr := nil;
        exit;
      end;
    end;

    @FKSRTL_memchr := PKSRTL_memchr(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_memchr'));
    if @FKSRTL_memchr = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_memchr not found in DLL');
      KSRTL_memchr := nil;
      exit;
    end;
  end;

  KSRTL_memchr := FKSRTL_memchr(ptr, value, num);
end;

//------ KSRTL_memcmp ------
type
  TKSRTL_memcmp = function (ptr1: Pointer; ptr2: Pointer; num: uint): int; stdcall;
  PKSRTL_memcmp = ^TKSRTL_memcmp;
var
  FKSRTL_memcmp: TKSRTL_memcmp;
        
function KSRTL_memcmp(ptr1: Pointer; ptr2: Pointer; num: uint): int; stdcall;
begin
  if @FKSRTL_memcmp = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_memcmp := 0;
        exit;
      end;
    end;

    @FKSRTL_memcmp := PKSRTL_memcmp(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_memcmp'));
    if @FKSRTL_memcmp = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_memcmp not found in DLL');
      KSRTL_memcmp := 0;
      exit;
    end;
  end;

  KSRTL_memcmp := FKSRTL_memcmp(ptr1, ptr2, num);
end;

//------ KSRTL_memcpy ------
type
  TKSRTL_memcpy = function (destination: Pointer; source: Pointer; num: uint): Pointer; stdcall;
  PKSRTL_memcpy = ^TKSRTL_memcpy;
var
  FKSRTL_memcpy: TKSRTL_memcpy;
        
function KSRTL_memcpy(destination: Pointer; source: Pointer; num: uint): Pointer; stdcall;
begin
  if @FKSRTL_memcpy = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_memcpy := nil;
        exit;
      end;
    end;

    @FKSRTL_memcpy := PKSRTL_memcpy(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_memcpy'));
    if @FKSRTL_memcpy = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_memcpy not found in DLL');
      KSRTL_memcpy := nil;
      exit;
    end;
  end;

  KSRTL_memcpy := FKSRTL_memcpy(destination, source, num);
end;

//------ KSRTL_memmove ------
type
  TKSRTL_memmove = function (destination: Pointer; source: Pointer; num: uint): Pointer; stdcall;
  PKSRTL_memmove = ^TKSRTL_memmove;
var
  FKSRTL_memmove: TKSRTL_memmove;
        
function KSRTL_memmove(destination: Pointer; source: Pointer; num: uint): Pointer; stdcall;
begin
  if @FKSRTL_memmove = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_memmove := nil;
        exit;
      end;
    end;

    @FKSRTL_memmove := PKSRTL_memmove(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_memmove'));
    if @FKSRTL_memmove = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_memmove not found in DLL');
      KSRTL_memmove := nil;
      exit;
    end;
  end;

  KSRTL_memmove := FKSRTL_memmove(destination, source, num);
end;

//------ KSRTL_memset ------
type
  TKSRTL_memset = function (ptr: Pointer; value: int; num: uint): Pointer; stdcall;
  PKSRTL_memset = ^TKSRTL_memset;
var
  FKSRTL_memset: TKSRTL_memset;
        
function KSRTL_memset(ptr: Pointer; value: int; num: uint): Pointer; stdcall;
begin
  if @FKSRTL_memset = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_memset := nil;
        exit;
      end;
    end;

    @FKSRTL_memset := PKSRTL_memset(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_memset'));
    if @FKSRTL_memset = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_memset not found in DLL');
      KSRTL_memset := nil;
      exit;
    end;
  end;

  KSRTL_memset := FKSRTL_memset(ptr, value, num);
end;

//------ KSRTL_strlen ------
type
  TKSRTL_strlen = function (str: PAnsiChar): uint; stdcall;
  PKSRTL_strlen = ^TKSRTL_strlen;
var
  FKSRTL_strlen: TKSRTL_strlen;
        
function KSRTL_strlen(str: PAnsiChar): uint; stdcall;
begin
  if @FKSRTL_strlen = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strlen := 0;
        exit;
      end;
    end;

    @FKSRTL_strlen := PKSRTL_strlen(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strlen'));
    if @FKSRTL_strlen = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strlen not found in DLL');
      KSRTL_strlen := 0;
      exit;
    end;
  end;

  KSRTL_strlen := FKSRTL_strlen(str);
end;

//------ KSRTL_strcmp ------
type
  TKSRTL_strcmp = function (str1: PAnsiChar; str2: PAnsiChar): int; stdcall;
  PKSRTL_strcmp = ^TKSRTL_strcmp;
var
  FKSRTL_strcmp: TKSRTL_strcmp;
        
function KSRTL_strcmp(str1: PAnsiChar; str2: PAnsiChar): int; stdcall;
begin
  if @FKSRTL_strcmp = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strcmp := 0;
        exit;
      end;
    end;

    @FKSRTL_strcmp := PKSRTL_strcmp(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strcmp'));
    if @FKSRTL_strcmp = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strcmp not found in DLL');
      KSRTL_strcmp := 0;
      exit;
    end;
  end;

  KSRTL_strcmp := FKSRTL_strcmp(str1, str2);
end;

//------ KSRTL_strncmp ------
type
  TKSRTL_strncmp = function (str1: PAnsiChar; str2: PAnsiChar; num: uint): int; stdcall;
  PKSRTL_strncmp = ^TKSRTL_strncmp;
var
  FKSRTL_strncmp: TKSRTL_strncmp;
        
function KSRTL_strncmp(str1: PAnsiChar; str2: PAnsiChar; num: uint): int; stdcall;
begin
  if @FKSRTL_strncmp = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strncmp := 0;
        exit;
      end;
    end;

    @FKSRTL_strncmp := PKSRTL_strncmp(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strncmp'));
    if @FKSRTL_strncmp = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strncmp not found in DLL');
      KSRTL_strncmp := 0;
      exit;
    end;
  end;

  KSRTL_strncmp := FKSRTL_strncmp(str1, str2, num);
end;

//------ KSRTL_strcpy ------
type
  TKSRTL_strcpy = function (destination: PAnsiChar; source: PAnsiChar): PAnsiChar; stdcall;
  PKSRTL_strcpy = ^TKSRTL_strcpy;
var
  FKSRTL_strcpy: TKSRTL_strcpy;
        
function KSRTL_strcpy(destination: PAnsiChar; source: PAnsiChar): PAnsiChar; stdcall;
begin
  if @FKSRTL_strcpy = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strcpy := nil;
        exit;
      end;
    end;

    @FKSRTL_strcpy := PKSRTL_strcpy(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strcpy'));
    if @FKSRTL_strcpy = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strcpy not found in DLL');
      KSRTL_strcpy := nil;
      exit;
    end;
  end;

  KSRTL_strcpy := FKSRTL_strcpy(destination, source);
end;

//------ KSRTL_strncpy ------
type
  TKSRTL_strncpy = function (destination: PAnsiChar; source: PAnsiChar; num: uint): PAnsiChar; stdcall;
  PKSRTL_strncpy = ^TKSRTL_strncpy;
var
  FKSRTL_strncpy: TKSRTL_strncpy;
        
function KSRTL_strncpy(destination: PAnsiChar; source: PAnsiChar; num: uint): PAnsiChar; stdcall;
begin
  if @FKSRTL_strncpy = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strncpy := nil;
        exit;
      end;
    end;

    @FKSRTL_strncpy := PKSRTL_strncpy(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strncpy'));
    if @FKSRTL_strncpy = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strncpy not found in DLL');
      KSRTL_strncpy := nil;
      exit;
    end;
  end;

  KSRTL_strncpy := FKSRTL_strncpy(destination, source, num);
end;

//------ KSRTL_strcat ------
type
  TKSRTL_strcat = function (destination: PAnsiChar; source: PAnsiChar): PAnsiChar; stdcall;
  PKSRTL_strcat = ^TKSRTL_strcat;
var
  FKSRTL_strcat: TKSRTL_strcat;
        
function KSRTL_strcat(destination: PAnsiChar; source: PAnsiChar): PAnsiChar; stdcall;
begin
  if @FKSRTL_strcat = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strcat := nil;
        exit;
      end;
    end;

    @FKSRTL_strcat := PKSRTL_strcat(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strcat'));
    if @FKSRTL_strcat = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strcat not found in DLL');
      KSRTL_strcat := nil;
      exit;
    end;
  end;

  KSRTL_strcat := FKSRTL_strcat(destination, source);
end;

//------ KSRTL_strncat ------
type
  TKSRTL_strncat = function (destination: PAnsiChar; source: PAnsiChar; num: uint): PAnsiChar; stdcall;
  PKSRTL_strncat = ^TKSRTL_strncat;
var
  FKSRTL_strncat: TKSRTL_strncat;
        
function KSRTL_strncat(destination: PAnsiChar; source: PAnsiChar; num: uint): PAnsiChar; stdcall;
begin
  if @FKSRTL_strncat = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strncat := nil;
        exit;
      end;
    end;

    @FKSRTL_strncat := PKSRTL_strncat(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strncat'));
    if @FKSRTL_strncat = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strncat not found in DLL');
      KSRTL_strncat := nil;
      exit;
    end;
  end;

  KSRTL_strncat := FKSRTL_strncat(destination, source, num);
end;

//------ KSRTL_strchr ------
type
  TKSRTL_strchr = function (str: PAnsiChar; character: int): PAnsiChar; stdcall;
  PKSRTL_strchr = ^TKSRTL_strchr;
var
  FKSRTL_strchr: TKSRTL_strchr;
        
function KSRTL_strchr(str: PAnsiChar; character: int): PAnsiChar; stdcall;
begin
  if @FKSRTL_strchr = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strchr := nil;
        exit;
      end;
    end;

    @FKSRTL_strchr := PKSRTL_strchr(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strchr'));
    if @FKSRTL_strchr = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strchr not found in DLL');
      KSRTL_strchr := nil;
      exit;
    end;
  end;

  KSRTL_strchr := FKSRTL_strchr(str, character);
end;

//------ KSRTL_strcoll ------
type
  TKSRTL_strcoll = function (str1: PAnsiChar; str2: PAnsiChar): int; stdcall;
  PKSRTL_strcoll = ^TKSRTL_strcoll;
var
  FKSRTL_strcoll: TKSRTL_strcoll;
        
function KSRTL_strcoll(str1: PAnsiChar; str2: PAnsiChar): int; stdcall;
begin
  if @FKSRTL_strcoll = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strcoll := 0;
        exit;
      end;
    end;

    @FKSRTL_strcoll := PKSRTL_strcoll(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strcoll'));
    if @FKSRTL_strcoll = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strcoll not found in DLL');
      KSRTL_strcoll := 0;
      exit;
    end;
  end;

  KSRTL_strcoll := FKSRTL_strcoll(str1, str2);
end;

//------ KSRTL_strrchr ------
type
  TKSRTL_strrchr = function (str1: PAnsiChar; character: int): PAnsiChar; stdcall;
  PKSRTL_strrchr = ^TKSRTL_strrchr;
var
  FKSRTL_strrchr: TKSRTL_strrchr;
        
function KSRTL_strrchr(str1: PAnsiChar; character: int): PAnsiChar; stdcall;
begin
  if @FKSRTL_strrchr = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strrchr := nil;
        exit;
      end;
    end;

    @FKSRTL_strrchr := PKSRTL_strrchr(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strrchr'));
    if @FKSRTL_strrchr = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strrchr not found in DLL');
      KSRTL_strrchr := nil;
      exit;
    end;
  end;

  KSRTL_strrchr := FKSRTL_strrchr(str1, character);
end;

//------ KSRTL_strstr ------
type
  TKSRTL_strstr = function (str1: PAnsiChar; str2: PAnsiChar): PAnsiChar; stdcall;
  PKSRTL_strstr = ^TKSRTL_strstr;
var
  FKSRTL_strstr: TKSRTL_strstr;
        
function KSRTL_strstr(str1: PAnsiChar; str2: PAnsiChar): PAnsiChar; stdcall;
begin
  if @FKSRTL_strstr = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strstr := nil;
        exit;
      end;
    end;

    @FKSRTL_strstr := PKSRTL_strstr(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strstr'));
    if @FKSRTL_strstr = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strstr not found in DLL');
      KSRTL_strstr := nil;
      exit;
    end;
  end;

  KSRTL_strstr := FKSRTL_strstr(str1, str2);
end;

//------ KSRTL_strspn ------
type
  TKSRTL_strspn = function (str1: PAnsiChar; str2: PAnsiChar): uint; stdcall;
  PKSRTL_strspn = ^TKSRTL_strspn;
var
  FKSRTL_strspn: TKSRTL_strspn;
        
function KSRTL_strspn(str1: PAnsiChar; str2: PAnsiChar): uint; stdcall;
begin
  if @FKSRTL_strspn = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strspn := 0;
        exit;
      end;
    end;

    @FKSRTL_strspn := PKSRTL_strspn(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strspn'));
    if @FKSRTL_strspn = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strspn not found in DLL');
      KSRTL_strspn := 0;
      exit;
    end;
  end;

  KSRTL_strspn := FKSRTL_strspn(str1, str2);
end;

//------ KSRTL_strcspn ------
type
  TKSRTL_strcspn = function (str1: PAnsiChar; str2: PAnsiChar): uint; stdcall;
  PKSRTL_strcspn = ^TKSRTL_strcspn;
var
  FKSRTL_strcspn: TKSRTL_strcspn;
        
function KSRTL_strcspn(str1: PAnsiChar; str2: PAnsiChar): uint; stdcall;
begin
  if @FKSRTL_strcspn = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strcspn := 0;
        exit;
      end;
    end;

    @FKSRTL_strcspn := PKSRTL_strcspn(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strcspn'));
    if @FKSRTL_strcspn = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strcspn not found in DLL');
      KSRTL_strcspn := 0;
      exit;
    end;
  end;

  KSRTL_strcspn := FKSRTL_strcspn(str1, str2);
end;

//------ KSRTL_strpbrk ------
type
  TKSRTL_strpbrk = function (str1: PAnsiChar; str2: PAnsiChar): PAnsiChar; stdcall;
  PKSRTL_strpbrk = ^TKSRTL_strpbrk;
var
  FKSRTL_strpbrk: TKSRTL_strpbrk;
        
function KSRTL_strpbrk(str1: PAnsiChar; str2: PAnsiChar): PAnsiChar; stdcall;
begin
  if @FKSRTL_strpbrk = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strpbrk := nil;
        exit;
      end;
    end;

    @FKSRTL_strpbrk := PKSRTL_strpbrk(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strpbrk'));
    if @FKSRTL_strpbrk = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strpbrk not found in DLL');
      KSRTL_strpbrk := nil;
      exit;
    end;
  end;

  KSRTL_strpbrk := FKSRTL_strpbrk(str1, str2);
end;

//------ KSRTL_strtok ------
type
  TKSRTL_strtok = function (str: PAnsiChar; delimiters: PAnsiChar): PAnsiChar; stdcall;
  PKSRTL_strtok = ^TKSRTL_strtok;
var
  FKSRTL_strtok: TKSRTL_strtok;
        
function KSRTL_strtok(str: PAnsiChar; delimiters: PAnsiChar): PAnsiChar; stdcall;
begin
  if @FKSRTL_strtok = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strtok := nil;
        exit;
      end;
    end;

    @FKSRTL_strtok := PKSRTL_strtok(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strtok'));
    if @FKSRTL_strtok = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strtok not found in DLL');
      KSRTL_strtok := nil;
      exit;
    end;
  end;

  KSRTL_strtok := FKSRTL_strtok(str, delimiters);
end;

//------ KSRTL_strxfrm ------
type
  TKSRTL_strxfrm = function (destination: PAnsiChar; source: PAnsiChar; num: uint): uint; stdcall;
  PKSRTL_strxfrm = ^TKSRTL_strxfrm;
var
  FKSRTL_strxfrm: TKSRTL_strxfrm;
        
function KSRTL_strxfrm(destination: PAnsiChar; source: PAnsiChar; num: uint): uint; stdcall;
begin
  if @FKSRTL_strxfrm = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_strxfrm := 0;
        exit;
      end;
    end;

    @FKSRTL_strxfrm := PKSRTL_strxfrm(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_strxfrm'));
    if @FKSRTL_strxfrm = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_strxfrm not found in DLL');
      KSRTL_strxfrm := 0;
      exit;
    end;
  end;

  KSRTL_strxfrm := FKSRTL_strxfrm(destination, source, num);
end;

//------ KSRTL_vsprintf ------
type
  TKSRTL_vsprintf = function (buffer: PAnsiChar; format: PAnsiChar; pArgs: Pointer): int; stdcall;
  PKSRTL_vsprintf = ^TKSRTL_vsprintf;
var
  FKSRTL_vsprintf: TKSRTL_vsprintf;
        
function KSRTL_vsprintf(buffer: PAnsiChar; format: PAnsiChar; pArgs: Pointer): int; stdcall;
begin
  if @FKSRTL_vsprintf = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_vsprintf := 0;
        exit;
      end;
    end;

    @FKSRTL_vsprintf := PKSRTL_vsprintf(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_vsprintf'));
    if @FKSRTL_vsprintf = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_vsprintf not found in DLL');
      KSRTL_vsprintf := 0;
      exit;
    end;
  end;

  KSRTL_vsprintf := FKSRTL_vsprintf(buffer, format, pArgs);
end;

//------ KSRTL_vsnprintf ------
type
  TKSRTL_vsnprintf = function (buffer: PAnsiChar; n: uint; format: PAnsiChar; pArgs: Pointer): int; stdcall;
  PKSRTL_vsnprintf = ^TKSRTL_vsnprintf;
var
  FKSRTL_vsnprintf: TKSRTL_vsnprintf;
        
function KSRTL_vsnprintf(buffer: PAnsiChar; n: uint; format: PAnsiChar; pArgs: Pointer): int; stdcall;
begin
  if @FKSRTL_vsnprintf = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KSRTL_vsnprintf := 0;
        exit;
      end;
    end;

    @FKSRTL_vsnprintf := PKSRTL_vsnprintf(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KSRTL_vsnprintf'));
    if @FKSRTL_vsnprintf = nil then
    begin
      KS_logMessage(0, 'ERROR: KSRTL_vsnprintf not found in DLL');
      KSRTL_vsnprintf := 0;
      exit;
    end;
  end;

  KSRTL_vsnprintf := FKSRTL_vsnprintf(buffer, n, format, pArgs);
end;

//------ _initRTLModule ------
procedure _initRTLModule();
begin
  _registerKernelAddress('KSRTL_calloc', @KSRTL_calloc);
  _registerKernelAddress('KSRTL_free', @KSRTL_free);
  _registerKernelAddress('KSRTL_malloc', @KSRTL_malloc);
  _registerKernelAddress('KSRTL_sin', @KSRTL_sin);
  _registerKernelAddress('KSRTL_cos', @KSRTL_cos);
  _registerKernelAddress('KSRTL_tan', @KSRTL_tan);
  _registerKernelAddress('KSRTL_asin', @KSRTL_asin);
  _registerKernelAddress('KSRTL_acos', @KSRTL_acos);
  _registerKernelAddress('KSRTL_atan', @KSRTL_atan);
  _registerKernelAddress('KSRTL_atan2', @KSRTL_atan2);
  _registerKernelAddress('KSRTL_sinh', @KSRTL_sinh);
  _registerKernelAddress('KSRTL_cosh', @KSRTL_cosh);
  _registerKernelAddress('KSRTL_tanh', @KSRTL_tanh);
  _registerKernelAddress('KSRTL_exp', @KSRTL_exp);
  _registerKernelAddress('KSRTL_frexp', @KSRTL_frexp);
  _registerKernelAddress('KSRTL_ldexp', @KSRTL_ldexp);
  _registerKernelAddress('KSRTL_log', @KSRTL_log);
  _registerKernelAddress('KSRTL_log10', @KSRTL_log10);
  _registerKernelAddress('KSRTL_modf', @KSRTL_modf);
  _registerKernelAddress('KSRTL_pow', @KSRTL_pow);
  _registerKernelAddress('KSRTL_sqrt', @KSRTL_sqrt);
  _registerKernelAddress('KSRTL_fabs', @KSRTL_fabs);
  _registerKernelAddress('KSRTL_ceil', @KSRTL_ceil);
  _registerKernelAddress('KSRTL_floor', @KSRTL_floor);
  _registerKernelAddress('KSRTL_fmod', @KSRTL_fmod);
  _registerKernelAddress('KSRTL_memchr', @KSRTL_memchr);
  _registerKernelAddress('KSRTL_memcmp', @KSRTL_memcmp);
  _registerKernelAddress('KSRTL_memcpy', @KSRTL_memcpy);
  _registerKernelAddress('KSRTL_memmove', @KSRTL_memmove);
  _registerKernelAddress('KSRTL_memset', @KSRTL_memset);
  _registerKernelAddress('KSRTL_strlen', @KSRTL_strlen);
  _registerKernelAddress('KSRTL_strcmp', @KSRTL_strcmp);
  _registerKernelAddress('KSRTL_strncmp', @KSRTL_strncmp);
  _registerKernelAddress('KSRTL_strcpy', @KSRTL_strcpy);
  _registerKernelAddress('KSRTL_strncpy', @KSRTL_strncpy);
  _registerKernelAddress('KSRTL_strcat', @KSRTL_strcat);
  _registerKernelAddress('KSRTL_strncat', @KSRTL_strncat);
  _registerKernelAddress('KSRTL_strchr', @KSRTL_strchr);
  _registerKernelAddress('KSRTL_strcoll', @KSRTL_strcoll);
  _registerKernelAddress('KSRTL_strrchr', @KSRTL_strrchr);
  _registerKernelAddress('KSRTL_strstr', @KSRTL_strstr);
  _registerKernelAddress('KSRTL_strspn', @KSRTL_strspn);
  _registerKernelAddress('KSRTL_strcspn', @KSRTL_strcspn);
  _registerKernelAddress('KSRTL_strpbrk', @KSRTL_strpbrk);
  _registerKernelAddress('KSRTL_strtok', @KSRTL_strtok);
  _registerKernelAddress('KSRTL_strxfrm', @KSRTL_strxfrm);
  _registerKernelAddress('KSRTL_vsprintf', @KSRTL_vsprintf);
  _registerKernelAddress('KSRTL_vsnprintf', @KSRTL_vsnprintf);
end;

{$DEFINE KS_RTL_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Camera Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_enumCameras ------
type
  TKS_enumCameras = function (hObject: KSHandle; index: int; pCameraInfo: PKSCameraInfo; flags: int): Error; stdcall;
  PKS_enumCameras = ^TKS_enumCameras;
var
  FKS_enumCameras: TKS_enumCameras;
        
function KS_enumCameras(hObject: KSHandle; index: int; pCameraInfo: PKSCameraInfo; flags: int): Error; stdcall;
begin
  if @FKS_enumCameras = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumCameras := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumCameras := PKS_enumCameras(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumCameras'));
    if @FKS_enumCameras = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumCameras not found in DLL');
      KS_enumCameras := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumCameras := FKS_enumCameras(hObject, index, pCameraInfo, flags);
end;

//------ KS_execCameraCommand ------
type
  TKS_execCameraCommand = function (hObject: KSHandle; command: int; address: uint; pData: Pointer; pLength: PInt; flags: int): Error; stdcall;
  PKS_execCameraCommand = ^TKS_execCameraCommand;
var
  FKS_execCameraCommand: TKS_execCameraCommand;
        
function KS_execCameraCommand(hObject: KSHandle; command: int; address: uint; pData: Pointer; pLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_execCameraCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execCameraCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execCameraCommand := PKS_execCameraCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execCameraCommand'));
    if @FKS_execCameraCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execCameraCommand not found in DLL');
      KS_execCameraCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execCameraCommand := FKS_execCameraCommand(hObject, command, address, pData, pLength, flags);
end;

//------ KS_installCameraHandler ------
type
  TKS_installCameraHandler = function (hHandle: KSHandle; eventType: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installCameraHandler = ^TKS_installCameraHandler;
var
  FKS_installCameraHandler: TKS_installCameraHandler;
        
function KS_installCameraHandler(hHandle: KSHandle; eventType: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installCameraHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installCameraHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installCameraHandler := PKS_installCameraHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installCameraHandler'));
    if @FKS_installCameraHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installCameraHandler not found in DLL');
      KS_installCameraHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installCameraHandler := FKS_installCameraHandler(hHandle, eventType, hSignal, flags);
end;

//------ KS_openCamera ------
type
  TKS_openCamera = function (phCamera: PKSHandle; hObject: KSHandle; pHardwareId: PAnsiChar; flags: int): Error; stdcall;
  PKS_openCamera = ^TKS_openCamera;
var
  FKS_openCamera: TKS_openCamera;
        
function KS_openCamera(phCamera: PKSHandle; hObject: KSHandle; pHardwareId: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_openCamera = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openCamera := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openCamera := PKS_openCamera(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openCamera'));
    if @FKS_openCamera = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openCamera not found in DLL');
      KS_openCamera := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openCamera := FKS_openCamera(phCamera, hObject, pHardwareId, flags);
end;

//------ KS_openCameraEx ------
type
  TKS_openCameraEx = function (phCamera: PKSHandle; hObject: KSHandle; pCameraInfo: PKSCameraInfo; flags: int): Error; stdcall;
  PKS_openCameraEx = ^TKS_openCameraEx;
var
  FKS_openCameraEx: TKS_openCameraEx;
        
function KS_openCameraEx(phCamera: PKSHandle; hObject: KSHandle; pCameraInfo: PKSCameraInfo; flags: int): Error; stdcall;
begin
  if @FKS_openCameraEx = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_openCameraEx := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_openCameraEx := PKS_openCameraEx(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_openCameraEx'));
    if @FKS_openCameraEx = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_openCameraEx not found in DLL');
      KS_openCameraEx := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_openCameraEx := FKS_openCameraEx(phCamera, hObject, pCameraInfo, flags);
end;

//------ KS_closeCamera ------
type
  TKS_closeCamera = function (hCamera: KSHandle; flags: int): Error; stdcall;
  PKS_closeCamera = ^TKS_closeCamera;
var
  FKS_closeCamera: TKS_closeCamera;
        
function KS_closeCamera(hCamera: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeCamera = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeCamera := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeCamera := PKS_closeCamera(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeCamera'));
    if @FKS_closeCamera = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeCamera not found in DLL');
      KS_closeCamera := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeCamera := FKS_closeCamera(hCamera, flags);
end;

//------ KS_readCameraMem ------
type
  TKS_readCameraMem = function (hCamera: KSHandle; var address: Int64; pData: Pointer; length: int; flags: int): Error; stdcall;
  PKS_readCameraMem = ^TKS_readCameraMem;
var
  FKS_readCameraMem: TKS_readCameraMem;
        
function KS_readCameraMem(hCamera: KSHandle; var address: Int64; pData: Pointer; length: int; flags: int): Error; stdcall;
begin
  if @FKS_readCameraMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_readCameraMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_readCameraMem := PKS_readCameraMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_readCameraMem'));
    if @FKS_readCameraMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_readCameraMem not found in DLL');
      KS_readCameraMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_readCameraMem := FKS_readCameraMem(hCamera, address, pData, length, flags);
end;

//------ KS_writeCameraMem ------
type
  TKS_writeCameraMem = function (hCamera: KSHandle; var address: Int64; pData: Pointer; length: int; flags: int): Error; stdcall;
  PKS_writeCameraMem = ^TKS_writeCameraMem;
var
  FKS_writeCameraMem: TKS_writeCameraMem;
        
function KS_writeCameraMem(hCamera: KSHandle; var address: Int64; pData: Pointer; length: int; flags: int): Error; stdcall;
begin
  if @FKS_writeCameraMem = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_writeCameraMem := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_writeCameraMem := PKS_writeCameraMem(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_writeCameraMem'));
    if @FKS_writeCameraMem = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_writeCameraMem not found in DLL');
      KS_writeCameraMem := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_writeCameraMem := FKS_writeCameraMem(hCamera, address, pData, length, flags);
end;

//------ KS_configCamera ------
type
  TKS_configCamera = function (hCamera: KSHandle; configCommand: int; pName: PAnsiChar; index: int; pData: Pointer; length: int; flags: int): Error; stdcall;
  PKS_configCamera = ^TKS_configCamera;
var
  FKS_configCamera: TKS_configCamera;
        
function KS_configCamera(hCamera: KSHandle; configCommand: int; pName: PAnsiChar; index: int; pData: Pointer; length: int; flags: int): Error; stdcall;
begin
  if @FKS_configCamera = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_configCamera := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_configCamera := PKS_configCamera(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_configCamera'));
    if @FKS_configCamera = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_configCamera not found in DLL');
      KS_configCamera := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_configCamera := FKS_configCamera(hCamera, configCommand, pName, index, pData, length, flags);
end;

//------ KS_startCameraAcquisition ------
type
  TKS_startCameraAcquisition = function (hCamera: KSHandle; acquisitionMode: int; flags: int): Error; stdcall;
  PKS_startCameraAcquisition = ^TKS_startCameraAcquisition;
var
  FKS_startCameraAcquisition: TKS_startCameraAcquisition;
        
function KS_startCameraAcquisition(hCamera: KSHandle; acquisitionMode: int; flags: int): Error; stdcall;
begin
  if @FKS_startCameraAcquisition = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_startCameraAcquisition := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_startCameraAcquisition := PKS_startCameraAcquisition(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_startCameraAcquisition'));
    if @FKS_startCameraAcquisition = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_startCameraAcquisition not found in DLL');
      KS_startCameraAcquisition := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_startCameraAcquisition := FKS_startCameraAcquisition(hCamera, acquisitionMode, flags);
end;

//------ KS_stopCameraAcquisition ------
type
  TKS_stopCameraAcquisition = function (hCamera: KSHandle; flags: int): Error; stdcall;
  PKS_stopCameraAcquisition = ^TKS_stopCameraAcquisition;
var
  FKS_stopCameraAcquisition: TKS_stopCameraAcquisition;
        
function KS_stopCameraAcquisition(hCamera: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_stopCameraAcquisition = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_stopCameraAcquisition := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_stopCameraAcquisition := PKS_stopCameraAcquisition(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_stopCameraAcquisition'));
    if @FKS_stopCameraAcquisition = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_stopCameraAcquisition not found in DLL');
      KS_stopCameraAcquisition := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_stopCameraAcquisition := FKS_stopCameraAcquisition(hCamera, flags);
end;

//------ KS_createCameraStream ------
type
  TKS_createCameraStream = function (phStream: PKSHandle; hCamera: KSHandle; channelNumber: int; numBuffers: int; bufferSize: int; flags: int): Error; stdcall;
  PKS_createCameraStream = ^TKS_createCameraStream;
var
  FKS_createCameraStream: TKS_createCameraStream;
        
function KS_createCameraStream(phStream: PKSHandle; hCamera: KSHandle; channelNumber: int; numBuffers: int; bufferSize: int; flags: int): Error; stdcall;
begin
  if @FKS_createCameraStream = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createCameraStream := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createCameraStream := PKS_createCameraStream(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createCameraStream'));
    if @FKS_createCameraStream = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createCameraStream not found in DLL');
      KS_createCameraStream := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createCameraStream := FKS_createCameraStream(phStream, hCamera, channelNumber, numBuffers, bufferSize, flags);
end;

//------ KS_closeCameraStream ------
type
  TKS_closeCameraStream = function (hStream: KSHandle; flags: int): Error; stdcall;
  PKS_closeCameraStream = ^TKS_closeCameraStream;
var
  FKS_closeCameraStream: TKS_closeCameraStream;
        
function KS_closeCameraStream(hStream: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closeCameraStream = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closeCameraStream := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closeCameraStream := PKS_closeCameraStream(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closeCameraStream'));
    if @FKS_closeCameraStream = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closeCameraStream not found in DLL');
      KS_closeCameraStream := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closeCameraStream := FKS_closeCameraStream(hStream, flags);
end;

//------ KS_recvCameraImage ------
type
  TKS_recvCameraImage = function (hStream: KSHandle; ppData: PPointer; ppBlockInfo: PPKSCameraBlock; flags: int): Error; stdcall;
  PKS_recvCameraImage = ^TKS_recvCameraImage;
var
  FKS_recvCameraImage: TKS_recvCameraImage;
        
function KS_recvCameraImage(hStream: KSHandle; ppData: PPointer; ppBlockInfo: PPKSCameraBlock; flags: int): Error; stdcall;
begin
  if @FKS_recvCameraImage = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_recvCameraImage := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_recvCameraImage := PKS_recvCameraImage(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_recvCameraImage'));
    if @FKS_recvCameraImage = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_recvCameraImage not found in DLL');
      KS_recvCameraImage := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_recvCameraImage := FKS_recvCameraImage(hStream, ppData, ppBlockInfo, flags);
end;

//------ KS_releaseCameraImage ------
type
  TKS_releaseCameraImage = function (hStream: KSHandle; pData: Pointer; flags: int): Error; stdcall;
  PKS_releaseCameraImage = ^TKS_releaseCameraImage;
var
  FKS_releaseCameraImage: TKS_releaseCameraImage;
        
function KS_releaseCameraImage(hStream: KSHandle; pData: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_releaseCameraImage = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_releaseCameraImage := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_releaseCameraImage := PKS_releaseCameraImage(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_releaseCameraImage'));
    if @FKS_releaseCameraImage = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_releaseCameraImage not found in DLL');
      KS_releaseCameraImage := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_releaseCameraImage := FKS_releaseCameraImage(hStream, pData, flags);
end;

//------ _initCameraModule ------
procedure _initCameraModule();
begin
  _registerKernelAddress('KS_execCameraCommand', @KS_execCameraCommand);
  _registerKernelAddress('KS_readCameraMem', @KS_readCameraMem);
  _registerKernelAddress('KS_writeCameraMem', @KS_writeCameraMem);
  _registerKernelAddress('KS_configCamera', @KS_configCamera);
  _registerKernelAddress('KS_recvCameraImage', @KS_recvCameraImage);
  _registerKernelAddress('KS_releaseCameraImage', @KS_releaseCameraImage);
end;

{$DEFINE KS_CAMERA_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// PLC Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createPlcCompiler ------
type
  TKS_createPlcCompiler = function (phCompiler: PKSHandle; flags: int): Error; stdcall;
  PKS_createPlcCompiler = ^TKS_createPlcCompiler;
var
  FKS_createPlcCompiler: TKS_createPlcCompiler;
        
function KS_createPlcCompiler(phCompiler: PKSHandle; flags: int): Error; stdcall;
begin
  if @FKS_createPlcCompiler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createPlcCompiler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createPlcCompiler := PKS_createPlcCompiler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createPlcCompiler'));
    if @FKS_createPlcCompiler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createPlcCompiler not found in DLL');
      KS_createPlcCompiler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createPlcCompiler := FKS_createPlcCompiler(phCompiler, flags);
end;

//------ KS_closePlcCompiler ------
type
  TKS_closePlcCompiler = function (hCompiler: KSHandle; flags: int): Error; stdcall;
  PKS_closePlcCompiler = ^TKS_closePlcCompiler;
var
  FKS_closePlcCompiler: TKS_closePlcCompiler;
        
function KS_closePlcCompiler(hCompiler: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closePlcCompiler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closePlcCompiler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closePlcCompiler := PKS_closePlcCompiler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closePlcCompiler'));
    if @FKS_closePlcCompiler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closePlcCompiler not found in DLL');
      KS_closePlcCompiler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closePlcCompiler := FKS_closePlcCompiler(hCompiler, flags);
end;

//------ KS_compilePlcFile ------
type
  TKS_compilePlcFile = function (hCompiler: KSHandle; sourcePath: PAnsiChar; sourceLanguage: int; destinationPath: PAnsiChar; flags: int): Error; stdcall;
  PKS_compilePlcFile = ^TKS_compilePlcFile;
var
  FKS_compilePlcFile: TKS_compilePlcFile;
        
function KS_compilePlcFile(hCompiler: KSHandle; sourcePath: PAnsiChar; sourceLanguage: int; destinationPath: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_compilePlcFile = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_compilePlcFile := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_compilePlcFile := PKS_compilePlcFile(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_compilePlcFile'));
    if @FKS_compilePlcFile = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_compilePlcFile not found in DLL');
      KS_compilePlcFile := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_compilePlcFile := FKS_compilePlcFile(hCompiler, sourcePath, sourceLanguage, destinationPath, flags);
end;

//------ KS_compilePlcBuffer ------
type
  TKS_compilePlcBuffer = function (hCompiler: KSHandle; pSourceBuffer: Pointer; sourceLength: int; sourceLang: int; pDestBuffer: Pointer; pDestLength: PInt; flags: int): Error; stdcall;
  PKS_compilePlcBuffer = ^TKS_compilePlcBuffer;
var
  FKS_compilePlcBuffer: TKS_compilePlcBuffer;
        
function KS_compilePlcBuffer(hCompiler: KSHandle; pSourceBuffer: Pointer; sourceLength: int; sourceLang: int; pDestBuffer: Pointer; pDestLength: PInt; flags: int): Error; stdcall;
begin
  if @FKS_compilePlcBuffer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_compilePlcBuffer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_compilePlcBuffer := PKS_compilePlcBuffer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_compilePlcBuffer'));
    if @FKS_compilePlcBuffer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_compilePlcBuffer not found in DLL');
      KS_compilePlcBuffer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_compilePlcBuffer := FKS_compilePlcBuffer(hCompiler, pSourceBuffer, sourceLength, sourceLang, pDestBuffer, pDestLength, flags);
end;

//------ KS_createPlcFromFile ------
type
  TKS_createPlcFromFile = function (phPlc: PKSHandle; binaryPath: PAnsiChar; flags: int): Error; stdcall;
  PKS_createPlcFromFile = ^TKS_createPlcFromFile;
var
  FKS_createPlcFromFile: TKS_createPlcFromFile;
        
function KS_createPlcFromFile(phPlc: PKSHandle; binaryPath: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_createPlcFromFile = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createPlcFromFile := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createPlcFromFile := PKS_createPlcFromFile(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createPlcFromFile'));
    if @FKS_createPlcFromFile = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createPlcFromFile not found in DLL');
      KS_createPlcFromFile := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createPlcFromFile := FKS_createPlcFromFile(phPlc, binaryPath, flags);
end;

//------ KS_createPlcFromBuffer ------
type
  TKS_createPlcFromBuffer = function (phPlc: PKSHandle; pBinaryBuffer: Pointer; bufferLength: int; flags: int): Error; stdcall;
  PKS_createPlcFromBuffer = ^TKS_createPlcFromBuffer;
var
  FKS_createPlcFromBuffer: TKS_createPlcFromBuffer;
        
function KS_createPlcFromBuffer(phPlc: PKSHandle; pBinaryBuffer: Pointer; bufferLength: int; flags: int): Error; stdcall;
begin
  if @FKS_createPlcFromBuffer = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createPlcFromBuffer := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createPlcFromBuffer := PKS_createPlcFromBuffer(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createPlcFromBuffer'));
    if @FKS_createPlcFromBuffer = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createPlcFromBuffer not found in DLL');
      KS_createPlcFromBuffer := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createPlcFromBuffer := FKS_createPlcFromBuffer(phPlc, pBinaryBuffer, bufferLength, flags);
end;

//------ KS_closePlc ------
type
  TKS_closePlc = function (hPlc: KSHandle; flags: int): Error; stdcall;
  PKS_closePlc = ^TKS_closePlc;
var
  FKS_closePlc: TKS_closePlc;
        
function KS_closePlc(hPlc: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_closePlc = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_closePlc := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_closePlc := PKS_closePlc(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_closePlc'));
    if @FKS_closePlc = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_closePlc not found in DLL');
      KS_closePlc := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_closePlc := FKS_closePlc(hPlc, flags);
end;

//------ KS_startPlc ------
type
  TKS_startPlc = function (hPlc: KSHandle; configIndex: int; var start: Int64; flags: int): Error; stdcall;
  PKS_startPlc = ^TKS_startPlc;
var
  FKS_startPlc: TKS_startPlc;
        
function KS_startPlc(hPlc: KSHandle; configIndex: int; var start: Int64; flags: int): Error; stdcall;
begin
  if @FKS_startPlc = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_startPlc := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_startPlc := PKS_startPlc(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_startPlc'));
    if @FKS_startPlc = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_startPlc not found in DLL');
      KS_startPlc := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_startPlc := FKS_startPlc(hPlc, configIndex, start, flags);
end;

//------ KS_stopPlc ------
type
  TKS_stopPlc = function (hPlc: KSHandle; flags: int): Error; stdcall;
  PKS_stopPlc = ^TKS_stopPlc;
var
  FKS_stopPlc: TKS_stopPlc;
        
function KS_stopPlc(hPlc: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_stopPlc = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_stopPlc := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_stopPlc := PKS_stopPlc(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_stopPlc'));
    if @FKS_stopPlc = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_stopPlc not found in DLL');
      KS_stopPlc := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_stopPlc := FKS_stopPlc(hPlc, flags);
end;

//------ KS_enumPlcConfigurations ------
type
  TKS_enumPlcConfigurations = function (hPlc: KSHandle; configIndex: int; pNameBuf: PAnsiChar; flags: int): Error; stdcall;
  PKS_enumPlcConfigurations = ^TKS_enumPlcConfigurations;
var
  FKS_enumPlcConfigurations: TKS_enumPlcConfigurations;
        
function KS_enumPlcConfigurations(hPlc: KSHandle; configIndex: int; pNameBuf: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_enumPlcConfigurations = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumPlcConfigurations := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumPlcConfigurations := PKS_enumPlcConfigurations(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumPlcConfigurations'));
    if @FKS_enumPlcConfigurations = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumPlcConfigurations not found in DLL');
      KS_enumPlcConfigurations := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumPlcConfigurations := FKS_enumPlcConfigurations(hPlc, configIndex, pNameBuf, flags);
end;

//------ KS_enumPlcResources ------
type
  TKS_enumPlcResources = function (hPlc: KSHandle; configIndex: int; resourceIndex: int; pNameBuf: PAnsiChar; flags: int): Error; stdcall;
  PKS_enumPlcResources = ^TKS_enumPlcResources;
var
  FKS_enumPlcResources: TKS_enumPlcResources;
        
function KS_enumPlcResources(hPlc: KSHandle; configIndex: int; resourceIndex: int; pNameBuf: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_enumPlcResources = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumPlcResources := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumPlcResources := PKS_enumPlcResources(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumPlcResources'));
    if @FKS_enumPlcResources = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumPlcResources not found in DLL');
      KS_enumPlcResources := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumPlcResources := FKS_enumPlcResources(hPlc, configIndex, resourceIndex, pNameBuf, flags);
end;

//------ KS_enumPlcTasks ------
type
  TKS_enumPlcTasks = function (hPlc: KSHandle; configIndex: int; resourceIndex: int; taskIndex: int; pNameBuf: PAnsiChar; flags: int): Error; stdcall;
  PKS_enumPlcTasks = ^TKS_enumPlcTasks;
var
  FKS_enumPlcTasks: TKS_enumPlcTasks;
        
function KS_enumPlcTasks(hPlc: KSHandle; configIndex: int; resourceIndex: int; taskIndex: int; pNameBuf: PAnsiChar; flags: int): Error; stdcall;
begin
  if @FKS_enumPlcTasks = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_enumPlcTasks := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_enumPlcTasks := PKS_enumPlcTasks(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_enumPlcTasks'));
    if @FKS_enumPlcTasks = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_enumPlcTasks not found in DLL');
      KS_enumPlcTasks := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_enumPlcTasks := FKS_enumPlcTasks(hPlc, configIndex, resourceIndex, taskIndex, pNameBuf, flags);
end;

//------ KS_execPlcCommand ------
type
  TKS_execPlcCommand = function (hPlc: KSHandle; command: int; pData: Pointer; flags: int): Error; stdcall;
  PKS_execPlcCommand = ^TKS_execPlcCommand;
var
  FKS_execPlcCommand: TKS_execPlcCommand;
        
function KS_execPlcCommand(hPlc: KSHandle; command: int; pData: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_execPlcCommand = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_execPlcCommand := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_execPlcCommand := PKS_execPlcCommand(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_execPlcCommand'));
    if @FKS_execPlcCommand = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_execPlcCommand not found in DLL');
      KS_execPlcCommand := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_execPlcCommand := FKS_execPlcCommand(hPlc, command, pData, flags);
end;

//------ KS_installPlcHandler ------
type
  TKS_installPlcHandler = function (hPlc: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installPlcHandler = ^TKS_installPlcHandler;
var
  FKS_installPlcHandler: TKS_installPlcHandler;
        
function KS_installPlcHandler(hPlc: KSHandle; eventCode: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installPlcHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installPlcHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installPlcHandler := PKS_installPlcHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installPlcHandler'));
    if @FKS_installPlcHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installPlcHandler not found in DLL');
      KS_installPlcHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installPlcHandler := FKS_installPlcHandler(hPlc, eventCode, hSignal, flags);
end;

//------ _initPLCModule ------
procedure _initPLCModule();
begin
  _registerKernelAddress('KS_closePlc', @KS_closePlc);
  _registerKernelAddress('KS_startPlc', @KS_startPlc);
  _registerKernelAddress('KS_stopPlc', @KS_stopPlc);
  _registerKernelAddress('KS_enumPlcConfigurations', @KS_enumPlcConfigurations);
  _registerKernelAddress('KS_enumPlcResources', @KS_enumPlcResources);
  _registerKernelAddress('KS_enumPlcTasks', @KS_enumPlcTasks);
  _registerKernelAddress('KS_execPlcCommand', @KS_execPlcCommand);
  _registerKernelAddress('KS_installPlcHandler', @KS_installPlcHandler);
end;

{$DEFINE KS_PLC_MODULE_HAS_RTX}

//--------------------------------------------------------------------------------------------------------------
// Vision Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_loadVisionKernel ------
type
  TKS_loadVisionKernel = function (phKernel: PKSHandle; dllName: PAnsiChar; initProcName: PAnsiChar; pArgs: Pointer; flags: int): Error; stdcall;
  PKS_loadVisionKernel = ^TKS_loadVisionKernel;
var
  FKS_loadVisionKernel: TKS_loadVisionKernel;
        
function KS_loadVisionKernel(phKernel: PKSHandle; dllName: PAnsiChar; initProcName: PAnsiChar; pArgs: Pointer; flags: int): Error; stdcall;
begin
  if @FKS_loadVisionKernel = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_loadVisionKernel := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_loadVisionKernel := PKS_loadVisionKernel(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_loadVisionKernel'));
    if @FKS_loadVisionKernel = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_loadVisionKernel not found in DLL');
      KS_loadVisionKernel := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_loadVisionKernel := FKS_loadVisionKernel(phKernel, dllName, initProcName, pArgs, flags);
end;

//------ KS_installVisionHandler ------
type
  TKS_installVisionHandler = function (eventType: int; hSignal: KSHandle; flags: int): Error; stdcall;
  PKS_installVisionHandler = ^TKS_installVisionHandler;
var
  FKS_installVisionHandler: TKS_installVisionHandler;
        
function KS_installVisionHandler(eventType: int; hSignal: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_installVisionHandler = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_installVisionHandler := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_installVisionHandler := PKS_installVisionHandler(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_installVisionHandler'));
    if @FKS_installVisionHandler = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_installVisionHandler not found in DLL');
      KS_installVisionHandler := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_installVisionHandler := FKS_installVisionHandler(eventType, hSignal, flags);
end;

//--------------------------------------------------------------------------------------------------------------
// SigProc Module
//--------------------------------------------------------------------------------------------------------------

//------ KS_createSignalFilter ------
type
  TKS_createSignalFilter = function (phFilter: PKSHandle; pParams: PKSSignalFilterParams; flags: int): Error; stdcall;
  PKS_createSignalFilter = ^TKS_createSignalFilter;
var
  FKS_createSignalFilter: TKS_createSignalFilter;
        
function KS_createSignalFilter(phFilter: PKSHandle; pParams: PKSSignalFilterParams; flags: int): Error; stdcall;
begin
  if @FKS_createSignalFilter = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createSignalFilter := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createSignalFilter := PKS_createSignalFilter(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createSignalFilter'));
    if @FKS_createSignalFilter = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createSignalFilter not found in DLL');
      KS_createSignalFilter := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createSignalFilter := FKS_createSignalFilter(phFilter, pParams, flags);
end;

//------ KS_createIndividualSignalFilter ------
type
  TKS_createIndividualSignalFilter = function (phFilter: PKSHandle; pParams: PKSIndividualSignalFilterParams; flags: int): Error; stdcall;
  PKS_createIndividualSignalFilter = ^TKS_createIndividualSignalFilter;
var
  FKS_createIndividualSignalFilter: TKS_createIndividualSignalFilter;
        
function KS_createIndividualSignalFilter(phFilter: PKSHandle; pParams: PKSIndividualSignalFilterParams; flags: int): Error; stdcall;
begin
  if @FKS_createIndividualSignalFilter = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createIndividualSignalFilter := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createIndividualSignalFilter := PKS_createIndividualSignalFilter(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createIndividualSignalFilter'));
    if @FKS_createIndividualSignalFilter = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createIndividualSignalFilter not found in DLL');
      KS_createIndividualSignalFilter := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createIndividualSignalFilter := FKS_createIndividualSignalFilter(phFilter, pParams, flags);
end;

//------ KS_getFrequencyResponse ------
type
  TKS_getFrequencyResponse = function (hFilter: KSHandle; frequency: Double; pAmplitude: PDouble; pPhase: PDouble; flags: int): Error; stdcall;
  PKS_getFrequencyResponse = ^TKS_getFrequencyResponse;
var
  FKS_getFrequencyResponse: TKS_getFrequencyResponse;
        
function KS_getFrequencyResponse(hFilter: KSHandle; frequency: Double; pAmplitude: PDouble; pPhase: PDouble; flags: int): Error; stdcall;
begin
  if @FKS_getFrequencyResponse = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_getFrequencyResponse := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_getFrequencyResponse := PKS_getFrequencyResponse(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_getFrequencyResponse'));
    if @FKS_getFrequencyResponse = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_getFrequencyResponse not found in DLL');
      KS_getFrequencyResponse := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_getFrequencyResponse := FKS_getFrequencyResponse(hFilter, frequency, pAmplitude, pPhase, flags);
end;

//------ KS_scaleSignalFilter ------
type
  TKS_scaleSignalFilter = function (hFilter: KSHandle; frequency: Double; value: Double; flags: int): Error; stdcall;
  PKS_scaleSignalFilter = ^TKS_scaleSignalFilter;
var
  FKS_scaleSignalFilter: TKS_scaleSignalFilter;
        
function KS_scaleSignalFilter(hFilter: KSHandle; frequency: Double; value: Double; flags: int): Error; stdcall;
begin
  if @FKS_scaleSignalFilter = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_scaleSignalFilter := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_scaleSignalFilter := PKS_scaleSignalFilter(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_scaleSignalFilter'));
    if @FKS_scaleSignalFilter = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_scaleSignalFilter not found in DLL');
      KS_scaleSignalFilter := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_scaleSignalFilter := FKS_scaleSignalFilter(hFilter, frequency, value, flags);
end;

//------ KS_resetSignalFilter ------
type
  TKS_resetSignalFilter = function (hFilter: KSHandle; flags: int): Error; stdcall;
  PKS_resetSignalFilter = ^TKS_resetSignalFilter;
var
  FKS_resetSignalFilter: TKS_resetSignalFilter;
        
function KS_resetSignalFilter(hFilter: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_resetSignalFilter = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_resetSignalFilter := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_resetSignalFilter := PKS_resetSignalFilter(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_resetSignalFilter'));
    if @FKS_resetSignalFilter = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_resetSignalFilter not found in DLL');
      KS_resetSignalFilter := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_resetSignalFilter := FKS_resetSignalFilter(hFilter, flags);
end;

//------ KS_createPIDController ------
type
  TKS_createPIDController = function (phController: PKSHandle; pParams: PKSPIDControllerParams; flags: int): Error; stdcall;
  PKS_createPIDController = ^TKS_createPIDController;
var
  FKS_createPIDController: TKS_createPIDController;
        
function KS_createPIDController(phController: PKSHandle; pParams: PKSPIDControllerParams; flags: int): Error; stdcall;
begin
  if @FKS_createPIDController = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_createPIDController := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_createPIDController := PKS_createPIDController(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_createPIDController'));
    if @FKS_createPIDController = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_createPIDController not found in DLL');
      KS_createPIDController := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_createPIDController := FKS_createPIDController(phController, pParams, flags);
end;

//------ KS_changeSetpoint ------
type
  TKS_changeSetpoint = function (hController: KSHandle; setpoint: Double; flags: int): Error; stdcall;
  PKS_changeSetpoint = ^TKS_changeSetpoint;
var
  FKS_changeSetpoint: TKS_changeSetpoint;
        
function KS_changeSetpoint(hController: KSHandle; setpoint: Double; flags: int): Error; stdcall;
begin
  if @FKS_changeSetpoint = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_changeSetpoint := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_changeSetpoint := PKS_changeSetpoint(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_changeSetpoint'));
    if @FKS_changeSetpoint = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_changeSetpoint not found in DLL');
      KS_changeSetpoint := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_changeSetpoint := FKS_changeSetpoint(hController, setpoint, flags);
end;

//------ KS_removeSignalProcessor ------
type
  TKS_removeSignalProcessor = function (hSigProc: KSHandle; flags: int): Error; stdcall;
  PKS_removeSignalProcessor = ^TKS_removeSignalProcessor;
var
  FKS_removeSignalProcessor: TKS_removeSignalProcessor;
        
function KS_removeSignalProcessor(hSigProc: KSHandle; flags: int): Error; stdcall;
begin
  if @FKS_removeSignalProcessor = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_removeSignalProcessor := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_removeSignalProcessor := PKS_removeSignalProcessor(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_removeSignalProcessor'));
    if @FKS_removeSignalProcessor = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_removeSignalProcessor not found in DLL');
      KS_removeSignalProcessor := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_removeSignalProcessor := FKS_removeSignalProcessor(hSigProc, flags);
end;

//------ KS_processSignal ------
type
  TKS_processSignal = function (hSigProc: KSHandle; input: Double; pOutput: PDouble; flags: int): Error; stdcall;
  PKS_processSignal = ^TKS_processSignal;
var
  FKS_processSignal: TKS_processSignal;
        
function KS_processSignal(hSigProc: KSHandle; input: Double; pOutput: PDouble; flags: int): Error; stdcall;
begin
  if @FKS_processSignal = nil then
  begin
    if _hKrtsDemoDll = nil then
    begin
      _hKrtsDemoDll := Win32Handle(LoadLibraryA(_dllToLoad));
      if _hKrtsDemoDll = nil then
      begin
        KS_logMessage(0, 'ERROR: Failed to load DLL');
        KS_processSignal := KSERROR_CANNOT_FIND_LIBRARY or GetLastError();
        exit;
      end;
    end;

    @FKS_processSignal := PKS_processSignal(GetProcAddress(HMODULE(_hKrtsDemoDll), 'KS_processSignal'));
    if @FKS_processSignal = nil then
    begin
      KS_logMessage(0, 'ERROR: KS_processSignal not found in DLL');
      KS_processSignal := KSERROR_CANNOT_FIND_ADDRESS or $1000 or GetLastError();
      exit;
    end;
  end;

  KS_processSignal := FKS_processSignal(hSigProc, input, pOutput, flags);
end;

//------ _initSigProcModule ------
procedure _initSigProcModule();
begin
  _registerKernelAddress('KS_createSignalFilter', @KS_createSignalFilter);
  _registerKernelAddress('KS_createIndividualSignalFilter', @KS_createIndividualSignalFilter);
  _registerKernelAddress('KS_getFrequencyResponse', @KS_getFrequencyResponse);
  _registerKernelAddress('KS_scaleSignalFilter', @KS_scaleSignalFilter);
  _registerKernelAddress('KS_resetSignalFilter', @KS_resetSignalFilter);
  _registerKernelAddress('KS_createPIDController', @KS_createPIDController);
  _registerKernelAddress('KS_changeSetpoint', @KS_changeSetpoint);
  _registerKernelAddress('KS_removeSignalProcessor', @KS_removeSignalProcessor);
  _registerKernelAddress('KS_processSignal', @KS_processSignal);
end;

{$DEFINE KS_SIGPROC_MODULE_HAS_RTX}

//------ _registerKernelAddress ------
procedure _registerKernelAddress(pName : PAnsiChar; pProc : Pointer);
begin
{$IFDEF KS_KERNEL_MODULE_HAS_RTX}
  KS_registerKernelAddress(pName, pProc, nil, 0);
{$ELSE}
  // nothing's done, because there is no Kernel Module
{$ENDIF}
end;

//------ _initModules ------
procedure _initModules();
begin
{$IFDEF KS_BASE_MODULE_HAS_RTX}
  _initBaseModule();
{$ENDIF}
{$IFDEF KS_KERNEL_MODULE_HAS_RTX}
  _initKernelModule();
{$ENDIF}
{$IFDEF KS_IOPORT_MODULE_HAS_RTX}
  _initIoPortModule();
{$ENDIF}
{$IFDEF KS_MEMORY_MODULE_HAS_RTX}
  _initMemoryModule();
{$ENDIF}
{$IFDEF KS_CLOCK_MODULE_HAS_RTX}
  _initClockModule();
{$ENDIF}
{$IFDEF KS_SYSTEM_MODULE_HAS_RTX}
  _initSystemModule();
{$ENDIF}
{$IFDEF KS_DEVICE_MODULE_HAS_RTX}
  _initDeviceModule();
{$ENDIF}
{$IFDEF KS_KEYBOARD_MODULE_HAS_RTX}
  _initKeyboardModule();
{$ENDIF}
{$IFDEF KS_INTERRUPT_MODULE_HAS_RTX}
  _initInterruptModule();
{$ENDIF}
{$IFDEF KS_UART_MODULE_HAS_RTX}
  _initUARTModule();
{$ENDIF}
{$IFDEF KS_COMM_MODULE_HAS_RTX}
  _initCOMMModule();
{$ENDIF}
{$IFDEF KS_USB_MODULE_HAS_RTX}
  _initUSBModule();
{$ENDIF}
{$IFDEF KS_XHCI_MODULE_HAS_RTX}
  _initXHCIModule();
{$ENDIF}
{$IFDEF KS_TIMER_MODULE_HAS_RTX}
  _initTimerModule();
{$ENDIF}
{$IFDEF KS_REALTIME_MODULE_HAS_RTX}
  _initRealTimeModule();
{$ENDIF}
{$IFDEF KS_TASK_MODULE_HAS_RTX}
  _initTaskModule();
{$ENDIF}
{$IFDEF KS_PACKET_MODULE_HAS_RTX}
  _initPacketModule();
{$ENDIF}
{$IFDEF KS_SOCKET_MODULE_HAS_RTX}
  _initSocketModule();
{$ENDIF}
{$IFDEF KS_ETHERCAT_MODULE_HAS_RTX}
  _initEtherCATModule();
{$ENDIF}
{$IFDEF KS_MULTIFUNCTION_MODULE_HAS_RTX}
  _initMultiFunctionModule();
{$ENDIF}
{$IFDEF KS_LIN_MODULE_HAS_RTX}
  _initLINModule();
{$ENDIF}
{$IFDEF KS_CAN_MODULE_HAS_RTX}
  _initCANModule();
{$ENDIF}
{$IFDEF KS_CANOPEN_MODULE_HAS_RTX}
  _initCANopenModule();
{$ENDIF}
{$IFDEF KS_PROFIBUS_MODULE_HAS_RTX}
  _initProfibusModule();
{$ENDIF}
{$IFDEF KS_PLC_MODULE_HAS_RTX}
  _initPLCModule();
{$ENDIF}
{$IFDEF KS_FLEXRAY_MODULE_HAS_RTX}
  _initFlexRayModule();
{$ENDIF}
{$IFDEF KS_CAMERA_MODULE_HAS_RTX}
  _initCameraModule();
{$ENDIF}
{$IFDEF KS_RTL_MODULE_HAS_RTX}
  _initRTLModule();
{$ENDIF}
{$IFDEF KS_DEDICATED_MODULE_HAS_RTX}
  _initDedicatedModule();
{$ENDIF}
{$IFDEF KS_SIGPROC_MODULE_HAS_RTX}
  _initSigProcModule();
{$ENDIF}
{$IFDEF KS_VISION_MODULE_HAS_RTX}
  _initVisionModule();
{$ENDIF}
{$IFDEF KS_DISK_MODULE_HAS_RTX}
  _initDiskModule();
{$ENDIF}
{$IFDEF KS_FILE_MODULE_HAS_RTX}
  _initFileModule();
{$ENDIF}
{$IFDEF KS_VARIABLE_MODULE_HAS_RTX}
  _initVariableModule();
{$ENDIF}
end;

end.
